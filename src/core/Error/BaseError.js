'use strict' ;

/*
 * src/core/Error/BaseError.js
 */

class BaseError extends Error {

  constructor (message, meta, app, original) {

    super(message) ;

    original = original ? original : message ;

    this.name = this.constructor.name ;
    this.originalMessage = original ;
    this.meta = meta ;
  }
}

module.exports = BaseError ;

