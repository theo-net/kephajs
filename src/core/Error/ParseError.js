'use strict' ;

/*
 * src/core/Error/ParseError.js
 */

const BaseError = require('./BaseError') ;

class ParseError extends BaseError {
}

module.exports = ParseError ;

