'use strict' ;

/**
 * src/core/Entity.js
 */

const Utils        = require('./Utils'),
      KernelAccess = require('./KernelAccess') ;

/**
 * Représente une entité d'un modèle
 */
class Entity extends KernelAccess {

  /**
   * Construit un objet
   * @param {Object} [values] Valeurs au départ de l'initialisation
   * @param {Boolean} [isNew] C'est une nouvelle entité
   */
  constructor (values, isNew = false) {

    super() ;

    this._isNew = true ;
    this._hasModifications = false ;
    this._properties = {} ;
    this._validators = {} ;
    this._alias = {} ;

    if (Utils.isObject(values)) {

      this._isNew = isNew ;
      Utils.forEach(values, (value, property) => {

        this._properties[property] = value ;
      }) ;
    }

    this._setProperties() ;
    this.toString = this.toString ;
  }


  /**
   * Indique s'il s'agit d'une nouvelle entité
   * @returns {Boolean}
   */
  isNew () {

    return this._isNew ;
  }


  /**
   * Indique si les propriétés de l'entité ont été modifiées
   * @returns {Boolean}
   */
  hasModifications () {

    return this._hasModifications ;
  }


  /**
   * Indique que l'entité a été sauvée (isNew et hasModifications deviennent
   * false
   */
  saved () {

    this._isNew = false ;
    this._hasModifications = false ;
  }


  /**
   * Définie une propriété
   * @param {String} name Nom de la propriété
   * @param {String|RegExp|Function|Number|Array} [validator] Validateur
   * @param {String} [aliasOf] Alias vers lequel pointe la propriété
   * @param {String} [defaultValue] Valeur par défaut. Sera utilisée si
   *                 propriété définie à `undefined`
   */
  _setProperty (name, validator, aliasOf, defaultValue = null) {

    this._validators[name] = this.kernel().get('$validate').make(validator) ;

    if (aliasOf)
      this._alias[name] = aliasOf ;
    else
      aliasOf = name ;

    if (!this._properties.hasOwnProperty(aliasOf)) {
      this._properties[aliasOf] = defaultValue !== null ? defaultValue
                                                        : undefined ;
    }

    Object.defineProperty(this, name, {
      configurable: false,
      enumerable: true,
      get: () => {
        return this._properties[aliasOf] ;
      },
      set: value => {

        if (value === undefined && defaultValue !== null)
          value = defaultValue ;

        if (!this._validators[name].isValid(value))
          throw new Error('Value of ' + name + ' invalid') ;

        this._hasModifications = true ;
        this._properties[aliasOf] = value ;
      }
    }) ;
  }
}

module.exports = Entity ;

