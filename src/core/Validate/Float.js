'use strict' ;

/**
 * src/core/Validate/Float.js
 */

const Validator = require('./Validator') ;

/**
 * Teste si la valeur est un entier et est comprise dans un interval
 *
 *    min = Infinity:  valeur minimum
 *    max = -Infinity: valeur maximum
 */
class FloatValidator extends Validator {

  _setArguments () {

    this._arguments = {
      min: 'Valeur minimum',
      max: 'Valeur maximum'
    } ;
    this._defaultArguments = {
      min: -Infinity,
      max: Infinity
    } ;
  }


  isValid (value = null) {

    this.reset() ;

    let test = true ;

    if (typeof value != 'number' || isNaN(value))
      test = this._addOccurredError('Doit être un nombre flottant.') ;
    else {

      if (value < this._validatorArguments.min) {
        test = this._addOccurredError('Doit être supérieur à '
             + this._validatorArguments.min + '.') ;
      }

      if (value > this._validatorArguments.max) {
        test = this._addOccurredError('Doit être inférieur à '
             + this._validatorArguments.max + '.') ;
      }
    }

    return test ;
  }
}

module.exports = FloatValidator ;

