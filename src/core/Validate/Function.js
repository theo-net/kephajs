'use strict' ;

/**
 * src/core/Validate/Function.js
 */

const Validator = require('./Validator') ;

/**
 * Teste si la valeur avec une fonction
 *
 *    fct:  Fonction de test fct (value)
 *    [message]: Fonction générant le message fct (value, testReturn)
 */
class FunctionValidator extends Validator {

  _setArguments () {

    this._arguments = {
      fct: 'Fonction de test',
      message: 'Fonction générant le message d\'erreur'
    } ;
    this._defaultArguments = {
      message: function () {
        return 'La valeur n\'est pas valide.' ;
      }
    } ;
  }


  isValid (value = null) {

    this.reset() ;

    let test = true ;

    test = this._validatorArguments.fct(value) ;

    if (test !== true) {
      test = this._addOccurredError(
        this._validatorArguments.message(value, test)
      ) ;
    }

    return test ;
  }
}

module.exports = FunctionValidator ;

