'use strict' ;

/**
 * src/core/Validate/Integer.js
 */

const FloatValidator = require('./Float') ;

/**
 * Teste si la valeur est un entier et est comprise dans un interval
 *
 *    min = Infinity:  valeur minimum
 *    max = -Infinity: valeur maximum
 */
class IntegerValidator extends FloatValidator {

  isValid (value = null) {

    this.reset() ;

    let test = true ;

    if (!Number.isInteger(value))
      test = this._addOccurredError('Doit être un entier.') ;
    else
      test = super.isValid(value) ;

    return test ;
  }
}

module.exports = IntegerValidator ;

