'use strict' ;

/**
 * src/core/Validate/List.test.js
 */

const expect = require('chai').expect ;

const ListValidator = require('./List') ;

// buddy ignore:start
describe('ListValidator', function () {

  it('check if value is in a list', function () {

    let validator = new ListValidator({list: [1, 'lol', 2.2]}) ;

    expect(validator.isValid('lol')).to.be.true ;
    expect(validator.isValid(2.2)).to.be.true ;
    expect(validator.isValid(3)).to.be.false ;
    expect(validator.isValid('test')).to.be.false ;
    expect(validator.isValid({a: 1})).to.be.false ;
    expect(validator.isValid([])).to.be.false ;
    expect(validator.getOccurredErrors()[0])
      .to.be.equal('Doit avoir une des valeurs suivantes: 1,lol,2.2.') ;
    expect(validator.isValid(1)).to.be.true ;
    expect(validator.getOccurredErrors().length).to.be.equal(0) ;
  }) ;

  it('fonctionne avec une liste fournie par un `String`', function () {

    let validator = new ListValidator({list: 'a,b,c,lol'}) ;

    expect(validator.isValid('a')).to.be.true ;
    expect(validator.isValid('b')).to.be.true ;
    expect(validator.isValid('c')).to.be.true ;
    expect(validator.isValid('lol')).to.be.true ;
    expect(validator.isValid(1)).to.be.false ;
    expect(validator.getOccurredErrors()[0])
      .to.be.equal('Doit avoir une des valeurs suivantes: a,b,c,lol.') ;
  }) ;
}) ;
// buddy ignore:end

