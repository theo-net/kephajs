'use strict' ;

/**
 * src/core/Services/Validator.js
 */

//const util = require('util') ;

const Utils = require('../Utils'),
      BaseError = require('../Error/BaseError') ;

/**
 * Objet permettant de valider des données. On ne doit pas l'instancier
 * directement, mais les différents Validator doivent étendre cette classe.
 *
 * Chaque validateur peut définir this._arguments avec les arguments nécessaire
 * à la validation des données :
 *
 *    this._arguments = {
 *      min: 'Valeur minimum',
 *      max: 'Valeur maximum'
 *    }
 */
class Validator {

  /**
   * Construit le validateur
   * @param {Object} [args={}] Arguments nécessaires à la construction du
   *                           validateur
   */
  constructor (args = {}) {

    // Messages d'erreurs
    this._errors    = new Array() ;
    // Erreurs retournées
    this._occurredErrors = new Array() ;

    // Arguments du validateur
    this._arguments = {} ;
    this._defaultArguments = {} ;
    this._setArguments() ;
    this._validatorArguments = Utils.extend(this._defaultArguments, args) ;
    this._checkArguments() ;
  }


  /**
   * Regarde si une valeur a été détectée
   * @returns {Boolean}
   */
  hasError () {

    return !Utils.isEmpty(this._occurredErrors) ;
  }


  /**
   * Renvoit la liste des erreurs si la validation ne passe pas, un tableau
   * vide sinon.
   * @returns {Array}
   */
  getOccurredErrors () {

    return this._occurredErrors ;
  }


  /**
   * À chaque exécution de la méthode, la liste des erreurs est réinitialisée
   * @param {*} [value=null] Valeur à tester
   * @returns {Boolean}
   */
  isValid (value = null) {

    this.reset() ;

    return value ? true : true ;
  }


  /**
   * Remet à zéro la liste d'erreurs
   */
  reset () {

    this._occurredErrors = [] ;
  }


  /**
   * Définit les arguments. C'est ici que l'on doit définir les arguments.
   * Doit contenir :
   *   this._arguments = {
   *     nom: 'description',
   *     ...
   *    } ;
   *    this._defaultArguments = {
   *      nom: valeur par défaut
   *      ...
   *    }
   */
  _setArguments () {}


  /**
   * Check que les arguments obligatoires soient bien transmis. Envoi une
   * erreur sinon
   * @returns {Boolean}
   * @throws {BaseError}
   * @private
   */
  _checkArguments () {

    let needed = [] ;

    Object.keys(this._arguments).forEach(name => {
      if (!this._validatorArguments.hasOwnProperty(name))
        needed.push(name + ': ' + this._arguments[name]) ;
    }) ;

    if (Utils.isEmpty(needed))
      return true ;

    let message = '' ;
    needed.forEach(msg => { message += '\n  - ' + msg ; }) ;

    throw new BaseError('Needs parameters:' + message) ;
  }


  /**
   * Ajoute un message d'erreur. Renvoit toujours false pour faciliter
   * l'écriture des tests.
   * @param {String} message Message à ajouter
   * @returns {false}
   * @private
   */
  _addOccurredError (message) {

    this._occurredErrors.push(message) ;
    return false ;
  }
}

module.exports = Validator ;

