'use strict' ;

/**
 * src/core/Validate/Null.js
 */

const Validator = require('./Validator') ;

/**
 * Renvoit toujours `true`.
 */
class NullValidator extends Validator {

  isValid () {

    return true ;
  }
}

module.exports = NullValidator ;

