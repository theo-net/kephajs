'use strict' ;

/**
 * src/core/Services/Validator.test.js
 */

const expect = require('chai').expect ;

const Validator = require('./Validator') ;

describe('Validator', function () {

  let validator = null ;
  const args = {
    min: 2,
    max: 5
  } ;

  beforeEach(function () {

    validator = new Validator(args) ;
  }) ;

  it('sauve les arguments', function () {

    expect(validator._validatorArguments).to.be.deep.equal(args) ;
  }) ;

  it('prend, si besoin, les valeurs par défaut des arguments', function () {

    class Test extends Validator {

      _setArguments () {

        this._arguments = {min: 'test', max: 'test'} ;
        this._defaultArguments = {max: 3} ;
      }
    }
    validator = new Test({min: 1}) ;
    expect(validator._validatorArguments).to.be.deep.equal({
      min: 1, max: 3
    }) ;
  }) ;


  describe('_checkArguments', function () {

    it('renvoit `true` si tout est ok', function () {

      validator._arguments = {
        min: 'Valeur minimum',
        max: 'Valeur maximum'
      } ;
      expect(validator._checkArguments()).to.be.equal(true) ;
    }) ;

    it('lance une erreur si un argument est manquant', function () {

      validator = new Validator({min: 0}) ;
      validator._arguments = {
        min: 'Valeur minimum',
        max: 'Valeur maximum'
      } ;

      expect(validator._checkArguments.bind(validator))
        .to.throw('Needs parameters:\n  - max: Valeur maximum') ;
    }) ;
  }) ;


  describe('hasError', function () {

    it('renvoit `false` si aucune erreur', function () {

      expect(validator.hasError()).to.be.equal(false) ;
    }) ;

    it('renvoit `true` s\'il y a une/des erreur(s)', function () {

      validator._occurredErrors[0] = 1 ;
      expect(validator.hasError()).to.be.equal(true) ;
    }) ;
  }) ;


  describe('getOccurredErrors', function () {

    it('Renvoit un tableau vide si aucune erreur', function () {

      expect(validator.getOccurredErrors()).to.be.deep.equal([]) ;
    }) ;

    it('Renvoit les messages d\'erreur', function () {

      validator._occurredErrors[0] = 'un message' ;
      validator._occurredErrors[1] = 'un message' ;

      expect(validator.getOccurredErrors()).to.be.deep.equal([
        'un message', 'un message'
      ]) ;
    }) ;
  }) ;


  describe('isValid', function () {

    it('Réinitialise les erreurs avant chaque test', function () {

      validator._occurredErrors[0] = 'un message' ;
      validator.isValid(1) ;

      expect(validator.hasError()).to.be.false ;
    }) ;
  }) ;


  describe('_addOccurredError', function () {

    it('retourne `false`', function () {

      expect(validator._addOccurredError('test')).to.be.equal(false) ;
    }) ;

    it('ajoute le message', function () {

      validator._addOccurredError('test') ;
      expect(validator.getOccurredErrors()).to.be.deep.equal(['test']) ;
    }) ;
  }) ;
}) ;

