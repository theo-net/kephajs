'use strict' ;

/**
 * src/core/Validate/Boolean.test.js
 */

const expect = require('chai').expect ;

const BooleanValidator = require('./Boolean') ;

// buddy ignore:start
describe('BooleanValidator', function () {

  it('check if value is an boolean', function () {

    let validator = new BooleanValidator() ;

    expect(validator.isValid(0)).to.be.false ;
    expect(validator.isValid(1)).to.be.false ;
    expect(validator.isValid('1')).to.be.false ;
    expect(validator.isValid(true)).to.be.true ;
    expect(validator.isValid('foobar')).to.be.false ;
    expect(validator.isValid(/a/)).to.be.false ;
    expect(validator.isValid({truc: 1})).to.be.false ;
    expect(validator.isValid([1, 2, 3])).to.be.false ;
    expect(validator.isValid(-1.1)).to.be.false ;
    expect(validator.isValid(2.1)).to.be.false ;

    expect(validator.getOccurredErrors()[0])
      .to.be.equal('Doit être un boolean.') ;

    expect(validator.isValid(false)).to.be.true ;
    expect(validator.getOccurredErrors().length).to.be.equal(0) ;
  }) ;
}) ;
// buddy ignore:end

