'use strict' ;

/**
 * src/core/Validate/NotEmpty.test.js
 */

const expect = require('chai').expect ;

const NotEmptyValidator = require('./NotEmpty') ;

// buddy ignore:start
describe('NotEmptyValidator', function () {

  it('check if value is not empty', function () {

    let validator = new NotEmptyValidator() ;

    expect(validator.isValid(0)).to.be.false ;
    expect(validator.isValid(-1)).to.be.false ;
    expect(validator.isValid(21)).to.be.false ;
    expect(validator.isValid('')).to.be.false ;
    expect(validator.isValid(true)).to.be.false ;
    expect(validator.isValid('foobar')).to.be.true ;
    expect(validator.isValid(/a/)).to.be.false ;
    expect(validator.isValid({truc: 1})).to.be.true ;
    expect(validator.isValid([])).to.be.false ;
    expect(validator.isValid({})).to.be.false ;
    expect(validator.isValid()).to.be.false ;
    expect(validator.isValid(2.1)).to.be.false ;
    expect(validator.getOccurredErrors()[0])
      .to.be.equal('Ne doit pas être vide.') ;
    expect(validator.isValid([1, 2, 3])).to.be.true ;
    expect(validator.getOccurredErrors().length).to.be.equal(0) ;
  }) ;
}) ;
// buddy ignore:end

