'use strict' ;

/**
 * src/core/Validate/Boolean.js
 */

const Validator = require('./Validator'),
      Utils     = require('../Utils') ;

/**
 * Teste si la valeur est un `Boolean`
 */
class BooleanValidator extends Validator {

  isValid (value = null) {

    this.reset() ;

    let test = true ;

    if (!Utils.isBoolean(value))
      test = this._addOccurredError('Doit être un boolean.') ;

    return test ;
  }
}

module.exports = BooleanValidator ;

