'use strict' ;

/**
 * src/core/Validate/RegExp.test.js
 */

const expect = require('chai').expect,
      sinon  = require('sinon') ;

const RegExpValidator = require('./RegExp') ;

describe('RegExpValidator', function () {

  it('execute the function to generate the message', function () {

    let msg   = sinon.spy() ;
    let validator = new RegExpValidator({
      regExp: /a/, message: msg
    }) ;

    validator.isValid('b') ;

    expect(msg.calledWith('b')).to.be.true ;
  }) ;

  it('return `true` si le test retourne `true`', function () {

    let validator = new RegExpValidator({regExp: /a/}) ;

    expect(validator.isValid('a')).to.be.equal(true) ;
  }) ;

  it('return `false` si la fonction retourne `false`', function () {

    let validator = new RegExpValidator({regExp: /a/}) ;

    expect(validator.isValid('b')).to.be.equal(false) ;
  }) ;

  it('remise à zéro des erreurs', function () {

    let validator = new RegExpValidator({regExp: /a/}) ;
    validator.isValid('b') ;
    validator.isValid('a') ;
    expect(validator.getOccurredErrors().length).to.be.equal(0) ;
  }) ;
}) ;

