'use strict' ;

/**
 * src/core/Validate/Null.test.js
 */

const expect = require('chai').expect ;

const NullValidator = require('./Null') ;

// buddy ignore:start
describe('NullValidator', function () {

  it('retourne toujours `true`', function () {

    let validator = new NullValidator() ;

    expect(validator.isValid(0)).to.be.true ;
    expect(validator.isValid(-1)).to.be.true ;
    expect(validator.isValid(21)).to.be.true ;
    expect(validator.isValid(NaN)).to.be.true ;
    expect(validator.isValid(true)).to.be.true ;
    expect(validator.isValid('foobar')).to.be.true ;
    expect(validator.isValid(/a/)).to.be.true ;
    expect(validator.isValid({truc: 1})).to.be.true ;
    expect(validator.isValid([1, 2, 3])).to.be.true ;
    expect(validator.isValid(-1.1)).to.be.true ;
    expect(validator.isValid(2.1)).to.be.true ;
    expect(validator.getOccurredErrors().length).to.be.equal(0) ;
  }) ;
}) ;
// buddy ignore:end

