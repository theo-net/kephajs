'use strict' ;

/**
 * src/core/Validate/RegExp.js
 */

const Validator = require('./Validator') ;

/**
 * Teste si la valeur avec une expression régulière
 *
 *    regExp:  Expression régulière
 *    [message]: Fonction générant le message fct (value)
 */
class RegExpValidator extends Validator {

  _setArguments () {

    this._arguments = {
      regExp:  'Expression régulière',
      message: 'Fonction générant le message d\'erreur'
    } ;
    this._defaultArguments = {
      message: function () {
        return 'La valeur n\'est pas valide.' ;
      }
    } ;
  }


  isValid (value = null) {

    this.reset() ;

    let test = this._validatorArguments.regExp.test(value) ;

    if (test !== true)
      this._addOccurredError(this._validatorArguments.message(value)) ;

    return test ;
  }
}

module.exports = RegExpValidator ;

