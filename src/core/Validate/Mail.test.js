'use strict' ;

/**
 * src/core/Validate/Mail.test.js
 */

const expect = require('chai').expect ;

const MailValidator = require('./Mail') ;

// buddy ignore:start
describe('MailValidator', function () {

  it('check if value is a email', function () {

    let validator = new MailValidator() ;

    expect(validator.isValid(0)).to.be.false ;
    expect(validator.isValid(-1)).to.be.false ;
    expect(validator.isValid(21)).to.be.false ;
    expect(validator.isValid(NaN)).to.be.false ;
    expect(validator.isValid(true)).to.be.false ;
    expect(validator.isValid('foobar')).to.be.false ;
    expect(validator.isValid('')).to.be.false ;
    expect(validator.isValid(/a/)).to.be.false ;
    expect(validator.isValid({truc: 1})).to.be.false ;
    expect(validator.isValid([1, 2, 3])).to.be.false ;
    expect(validator.isValid(-1.1)).to.be.false ;
    expect(validator.isValid(2.1)).to.be.false ;
    expect(validator.isValid('test@test')).to.be.false ;
    expect(validator.getOccurredErrors()[0])
      .to.be.equal('Doit être une adresse mail.') ;

    expect(validator.isValid('test@test.test')).to.be.true ;
    expect(validator.isValid('te.st@test.test')).to.be.true ;
    expect(validator.isValid('te.st@test.te.st')).to.be.true ;
    expect(validator.isValid('test1@t2est.test')).to.be.true ;
    expect(validator.getOccurredErrors().length).to.be.equal(0) ;
  }) ;
}) ;
// buddy ignore:end

