'use strict' ;

/**
 * src/core/Validate/Validate.js
 */

const Validator         = require('./Validator'),
      FunctionValidator = require('./Function'),
      ListValidator     = require('./List'),
      RegExpValidator   = require('./RegExp'),
      Utils             = require('../Utils'),
      BaseError         = require('../Error/BaseError') ;

/**
 * Créé une collection de validateurs qui seront tous testés/
 */
class ValidateValidator extends Validator {

  _setArguments () {

    this._arguments = {
      service: 'Service validate'
    } ;
    this._defaultArguments = {
      service: null
    } ;
  }


  /**
   * Ajoute un validateur à la collection
   *
   * @param {Validator} validator Validateur à ajouter
   * @returns {ValidateValidator} this
   * @throws {BaseError}
   */
  addValidator (validator) {

    if (!(validator instanceof Validator)) {
      throw new BaseError(
        'La validateur ajouté n\'est pas une instance de Validator !') ;
    }

    // On initialise si besoin la liste des validators
    if (!Array.isArray(this._validators))
      this._validators = [] ;

    this._validators.push(validator) ;

    return this ;
  }


  /**
   * Ajoute un nouveau validateur en le construisant grâce au service
   * `validate`.
   *
   * @param {String|Function|RegExp|Object|Array} validator Validateur à
   *    ajouter. Si `validator` est un `String`, récupère le validateur
   *    correspondant depuis la service, s'il s'agit d'une fonction, construit
   *    un nouveau validateur, de même s'il s'agit d'une `RegExp`. S'il s'agit
   *    d'un objet, on récupère le service correspondant:
   *      { name: args } ex: {integer: {min: 0, max: 5}}
   *    S'il s'agit d'un tableau : soit on construit une liste (ListValidator),
   *    soit il s'agit d'une série de validateurs décrits par des objets comme
   *    ci-dessus.
   * @param {Object|String} [args={}] Arguments qui seront passés au validateur.
   *    S'il s'agit d'un `String`, ce sera le message d'erreur (utilisé si on
   *    construit le validateur à partir d'une fonction ou d'une `RegExp`).
   * @returns {ValidateValidator} this
   * @throws {BaseError}
   */
  add (validator, args = {}) {

    // On initialise si besoin la liste des validators
    if (!Array.isArray(this._validators))
      this._validators = [] ;

    // Si args est en fait le message
    let message ;
    if (Utils.isString(args)) {
      message = function () {
        return args ;
      } ;
    }

    // Validator est une RegExp
    if (Utils.isRegExp(validator)) {
      if (message !== undefined) {
        this._validators.push(new RegExpValidator({
          regExp: validator, message: message
        })) ;
      }
      else
        this._validators.push(new RegExpValidator({regExp: validator})) ;
    }
    // Validator est une Function
    else if (Utils.isFunction(validator)) {
      if (message !== undefined) {
        this._validators.push(new FunctionValidator({
          fct: validator, message: message
        })) ;
      }
      else
        this._validators.push(new FunctionValidator({fct: validator})) ;
    }
    // On construit le validateur à partir du service
    else if (Utils.isString(validator))
      this._validators.push(this._getFromService(validator, args)) ;
    // Array
    else if (Array.isArray(validator)) {
      // Object
      if (Utils.isObject(validator[0])) {
        validator.forEach(val => {
          let valName = Object.getOwnPropertyNames(val) ;
          let args = val[valName] ;
          this._validators.push(this._getFromService(valName, args)) ;
        }) ;
      }
      // List
      else
        this._validators.push(new ListValidator({list: validator})) ;
    }
    // Object
    else if (Utils.isObject(validator)) {

      let valName = Object.getOwnPropertyNames(validator)[0] ;
      args = validator[valName] ;
      this._validators.push(this._getFromService(valName, args)) ;
    }
    else {
      throw new BaseError(
        'La validateur ajouté n\'est pas valide !') ;
    }

    return this ;
  }


  /**
   * Exécute tous les validateurs. Renvoit `false` si un ne passe pas.
   * @param {*} value Valeur à tester
   * @returns {Boolean}
   */
  isValid (value = null) {

    this.reset() ;

    let test = true ;

    this._validators.forEach(validator => {

      test = validator.isValid(value) && test ;

      if (validator.hasError()) {
        this._occurredErrors =
          this._occurredErrors.concat(validator.getOccurredErrors()) ;
      }
    }) ;

    return test ;
  }


  /**
   * Récupère un validateur depuis le service validate
   *
   * @param {String} name Nom du validateur
   * @param {Object} args Arguments
   * @returns {Validator}
   * @private
   */
  _getFromService (name, args) {

    // On vérifie que Validate est lié au service validate
    if (this._validatorArguments.service === null) {
      throw new BaseError(
        'Le validateur n\'est pas lié au service `validate` !') ;
    }

    return this._validatorArguments.service.getWithName(name, args) ;
  }
}

module.exports = ValidateValidator ;

