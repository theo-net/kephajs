'use strict' ;

/**
 * src/core/Validate/String.test.js
 */

const expect = require('chai').expect ;

const StringValidator = require('./String') ;

// buddy ignore:start
describe('StringValidator', function () {

  it('check if value is a string', function () {

    let validator = new StringValidator() ;

    expect(validator.isValid(0)).to.be.false ;
    expect(validator.isValid(-1)).to.be.false ;
    expect(validator.isValid(21)).to.be.false ;
    expect(validator.isValid(NaN)).to.be.false ;
    expect(validator.isValid(true)).to.be.false ;
    expect(validator.isValid('foobar')).to.be.true ;
    expect(validator.isValid(/a/)).to.be.false ;
    expect(validator.isValid({truc: 1})).to.be.false ;
    expect(validator.isValid([1, 2, 3])).to.be.false ;
    expect(validator.isValid(-1.1)).to.be.false ;
    expect(validator.isValid(2.1)).to.be.false ;
    expect(validator.getOccurredErrors()[0])
      .to.be.equal('Doit être une chaîne de caractères.') ;
    expect(validator.isValid('')).to.be.true ;
    expect(validator.getOccurredErrors().length).to.be.equal(0) ;
  }) ;

  it('check length min', function () {

    let validator = new StringValidator({
      min: 3,
      max: 8
    }) ;

    expect(validator.isValid()).to.be.false ;
    expect(validator.isValid('abc')).to.be.true ;
    expect(validator.isValid('abcd')).to.be.true ;
    expect(validator.isValid('ab')).to.be.false ;
    expect(validator.getOccurredErrors()[0])
      .to.be.equal('La valeur est trop courte, le minimum est 3 et vous avez '
        + 'entré 2 caractère(s).') ;
  }) ;

  it('check length max', function () {

    let validator = new StringValidator({
      min: 3,
      max: 8
    }) ;

    expect(validator.isValid('abc')).to.be.true ;
    expect(validator.isValid('abcdefgh')).to.be.true ;
    expect(validator.isValid('abcdefghi')).to.be.false ;
    expect(validator.getOccurredErrors()[0])
      .to.be.equal('La valeur est trop longue, le maximum est 8 et vous avez '
        + 'entré 9 caractère(s).') ;
  }) ;

  it('check just max', function () {

    let validator = new StringValidator({
      max: 4
    }) ;

    expect(validator.isValid('')).to.be.true ;
    expect(validator.isValid('ac')).to.be.true ;
    expect(validator.isValid('abcdefgh')).to.be.false ;
  }) ;

  it('check just min', function () {

    let validator = new StringValidator({
      min: 2
    }) ;

    expect(validator.isValid('')).to.be.false ;
    expect(validator.isValid('ac')).to.be.true ;
    expect(validator.isValid('abc')).to.be.true ;
  }) ;
}) ;
// buddy ignore:end

