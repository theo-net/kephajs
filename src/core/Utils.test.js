'use strict' ;

/**
 * src/core/Utils.test.js
 */

const expect = require('chai').expect ;

const Utils = require('./Utils.js') ;

// buddy ignore:start

describe('Utils', function () {

  let root = (typeof global == 'object' && global) || this ;

  // Used to provide falsey values to methods.
  let falsey = [null, undefined, false, 0, NaN, ''] ;
  let empties = [[], {}].concat(falsey.slice(1)) ;

  // Pour les tests dans le navigateur
  let document = root.document,
      body = root.document && root.document.body ;

  // Array typés
  let typedArrays = [
    'Float32Array',
    'Float64Array',
    'Int8Array',
    'Int16Array',
    'Int32Array',
    'Uint8Array',
    'Uint8ClampedArray',
    'Uint16Array',
    'Uint32Array'
  ] ;
  let arrayViews = typedArrays.concat('DataView') ;


  describe('.isString', function () {

    it('should return `true` for strings', function () {

      expect(Utils.isString('a')).to.equal(true) ;
      expect(Utils.isString(Object('a'))).to.equal(true) ;
    }) ;

    it('should return `false` for non-strings', function () {

      let expected = falsey.map(value => value === '') ;
      let actual   = falsey.map(function (value, index) {
        return index ? Utils.isString(value) : Utils.isString() ;
      }) ;

      expect(actual).to.deep.equal(expected) ;

      expect(Utils.isString([1, 2, 3])).to.equal(false) ;
      expect(Utils.isString(true)).to.equal(false) ;
      expect(Utils.isString(new Date)).to.equal(false) ;
      expect(Utils.isString(new Error)).to.equal(false) ;
      expect(Utils.isString(Utils)).to.equal(false) ;
      expect(Utils.isString(Utils.noop)).to.equal(false) ;
      expect(Utils.isString({ '0': 1, 'length': 1 })).to.equal(false) ;
      expect(Utils.isString(1)).to.equal(false) ;
      expect(Utils.isString(/x/)).to.equal(false) ;
      expect(Utils.isString(Symbol('a'))).to.equal(false) ;
    }) ;
  }) ;


  describe('.isObjectLike', function () {

    it('should return `true` for objects', function () {

      expect(Utils.isObjectLike([1, 2, 3])).to.equal(true) ;
      expect(Utils.isObjectLike(Object(false))).to.equal(true) ;
      expect(Utils.isObjectLike(new Date)).to.equal(true) ;
      expect(Utils.isObjectLike(new Error)).to.equal(true) ;
      expect(Utils.isObjectLike({ 'a': 1 })).to.equal(true) ;
      expect(Utils.isObjectLike(Object(0))).to.equal(true) ;
      expect(Utils.isObjectLike(/x/)).to.equal(true) ;
      expect(Utils.isObjectLike(Object('a'))).to.equal(true) ;
    }) ;

    it('should return `false` for non-objects', function () {

      let values = falsey.concat(true, Array.prototype.slice,
                                 1, 'a', Symbol('a')),
          expected = values.map(function () { return false ; }) ;

      let actual = values.map(function (value, index) {
        return index ? Utils.isObjectLike(value) : Utils.isObjectLike() ;
      }) ;

      expect(actual).to.deep.equal(expected) ;
    }) ;
  }) ;


  describe('.isObject', function () {

    it('should return `true` for objects', function () {

      expect(Utils.isObject([1, 2, 3])).to.equal(true) ;
      expect(Utils.isObject(Object(false))).to.equal(true) ;
      expect(Utils.isObject(new Date)).to.equal(true) ;
      expect(Utils.isObject(new Error)).to.equal(true) ;
      expect(Utils.isObject(Utils)).to.equal(true) ;
      expect(Utils.isObject(Array.prototype.slice)).to.equal(true) ;
      expect(Utils.isObject({ 'a': 1 })).to.equal(true) ;
      expect(Utils.isObject(Object(0))).to.equal(true) ;
      expect(Utils.isObject(/x/)).to.equal(true) ;
      expect(Utils.isObject(Object('a'))).to.equal(true) ;

      if (document)
        expect(Utils.isObject(body)).to.equal(true) ;
      if (Symbol)
        expect(Utils.isObject(Object(Symbol('a')))).to.equal(true) ;
    }) ;

    it('should return `false` for non-objects', function () {

      let values = falsey.concat(true, 1, 'a', Symbol('a')),
          expected = values.map(function () { return false ; }) ;

      let actual = values.map(function (value, index) {
        return index ? Utils.isObject(value) : Utils.isObject() ;
      }) ;

      expect(actual).to.deep.equal(expected) ;
    }) ;
  }) ;


  describe('.isDate', function () {

    it('should return `true` for dates', function () {

      expect(Utils.isDate(new Date)).to.equal(true) ;
    }) ;

    it('should return `false` for non-dates', function () {

      let expected = falsey.map(function () { return false ; }) ;
      let actual = falsey.map(function (value, index) {
        return index ? Utils.isDate(value) : Utils.isDate() ;
      }) ;
      expect(actual).to.deep.equal(expected) ;

      expect(Utils.isDate([1, 2, 3])).to.equal(false) ;
      expect(Utils.isDate(true)).to.equal(false) ;
      expect(Utils.isDate(new Error)).to.equal(false) ;
      expect(Utils.isDate(Utils)).to.equal(false) ;
      expect(Utils.isDate(Array.prototype.slice)).to.equal(false) ;
      expect(Utils.isDate({ 'a': 1 })).to.equal(false) ;
      expect(Utils.isDate(1)).to.equal(false) ;
      expect(Utils.isDate(/x/)).to.equal(false) ;
      expect(Utils.isDate('a')).to.equal(false) ;
      expect(Utils.isDate(Symbol('a'))).to.equal(false) ;
    }) ;
  }) ;


  describe('.isRegExp', function () {

    it('should return `true` for regexes', function () {

      expect(Utils.isRegExp(/x/)).to.equal(true) ;
      expect(Utils.isRegExp(RegExp('x'))).to.equal(true) ;
    }) ;

    it('should return `false` for non-regexes', function () {

      let expected = falsey.map(function () { return false ; }) ;
      let actual = falsey.map(function (value, index) {
        return index ? Utils.isRegExp(value) : Utils.isRegExp() ;
      }) ;
      expect(actual).to.deep.equal(expected) ;

      expect(Utils.isRegExp([1, 2, 3])).to.equal(false) ;
      expect(Utils.isRegExp(true)).to.equal(false) ;
      expect(Utils.isRegExp(new Date)).to.equal(false) ;
      expect(Utils.isRegExp(new Error)).to.equal(false) ;
      expect(Utils.isRegExp(Utils)).to.equal(false) ;
      expect(Utils.isRegExp(Array.prototype.slice)).to.equal(false) ;
      expect(Utils.isRegExp({ 'a': 1 })).to.equal(false) ;
      expect(Utils.isRegExp(1)).to.equal(false) ;
      expect(Utils.isRegExp('a')).to.equal(false) ;
      expect(Utils.isRegExp(Symbol('a'))).to.equal(false) ;
    }) ;
  }) ;


  describe('.isNumber', function () {

    it('should return `true` for numbers', function () {

      expect(Utils.isNumber(0)).to.equal(true) ;
      expect(Utils.isNumber(Object(0))).to.equal(true) ;
      expect(Utils.isNumber(NaN)).to.equal(true) ;
    }) ;

    it('should return `false` for non-numbers', function () {

      let expected = falsey.map(function (value) {
        return typeof value == 'number' ;
      }) ;

      let actual = falsey.map(function (value, index) {
        return index ? Utils.isNumber(value) : Utils.isNumber() ;
      }) ;

      expect(actual).to.deep.equal(expected) ;

      expect(Utils.isNumber([1, 2, 3])).to.equal(false) ;
      expect(Utils.isNumber(true)).to.equal(false) ;
      expect(Utils.isNumber(new Date)).to.equal(false) ;
      expect(Utils.isNumber(new Error)).to.equal(false) ;
      expect(Utils.isNumber(Utils)).to.equal(false) ;
      expect(Utils.isNumber(Array.prototype.slice)).to.equal(false) ;
      expect(Utils.isNumber({ 'a': 1 })).to.equal(false) ;
      expect(Utils.isNumber(/x/)).to.equal(false) ;
      expect(Utils.isNumber('a')).to.equal(false) ;
      expect(Utils.isNumber(Symbol('a'))).to.equal(false) ;
    }) ;
  }) ;


  describe('.isBoolean', function () {

    it('should return `true` for booleans', function () {

      expect(Utils.isBoolean(true)).to.equal(true) ;
      expect(Utils.isBoolean(false)).to.equal(true) ;
      expect(Utils.isBoolean(Object(true))).to.equal(true) ;
      expect(Utils.isBoolean(Object(false))).to.equal(true) ;
    }) ;

    it('should return `false` for non-booleans', function () {

      let expected = falsey.map(function (value) {
        return value === false ;
      }) ;

      let actual = falsey.map(function (value, index) {
        return index ? Utils.isBoolean(value) : Utils.isBoolean() ;
      }) ;

      expect(actual).to.deep.equal(expected) ;

      expect(Utils.isBoolean([1, 2, 3])).to.equal(false) ;
      expect(Utils.isBoolean(new Date)).to.equal(false) ;
      expect(Utils.isBoolean(new Error)).to.equal(false) ;
      expect(Utils.isBoolean(Utils)).to.equal(false) ;
      expect(Utils.isBoolean(Array.prototype.slice)).to.equal(false) ;
      expect(Utils.isBoolean({ 'a': 1 })).to.equal(false) ;
      expect(Utils.isBoolean(1)).to.equal(false) ;
      expect(Utils.isBoolean(/x/)).to.equal(false) ;
      expect(Utils.isBoolean('a')).to.equal(false) ;
      expect(Utils.isBoolean(Symbol('a'))).to.equal(false) ;
    }) ;
  }) ;


  describe('.isFunction', function () {

    it('should return `true` for functions', function () {

      expect(Utils.isFunction(Utils.isNumber)).to.equal(true) ;
      expect(Utils.isFunction(Array.isArray)).to.equal(true) ;
    }) ;

    it('should return `true` for array view constructors', function () {

      let expected = arrayViews.map((type) => {
        return Object.prototype.toString
          .call(root[type]) == '[object Function]' ;
      }) ;

      let actual = arrayViews.map((type) => {
        return Utils.isFunction(root[type]) ;
      }) ;

      expect(actual).to.deep.equal(expected) ;
    }) ;

    it('should return `false` for non-functions', function () {

      let expected = falsey.map(() => { return false ; }) ;

      let actual = falsey.map((value, index) => {
        return index ? Utils.isFunction(value) : Utils.isFunction() ;
      }) ;

      expect(actual).to.deep.equal(expected) ;

      expect(Utils.isFunction([1, 2, 3])).to.equal(false) ;
      expect(Utils.isFunction(true)).to.equal(false) ;
      expect(Utils.isFunction(new Date)).to.equal(false) ;
      expect(Utils.isFunction(new Error)).to.equal(false) ;
      expect(Utils.isFunction({ 'a': 1 })).to.equal(false) ;
      expect(Utils.isFunction(1)).to.equal(false) ;
      expect(Utils.isFunction(/x/)).to.equal(false) ;
      expect(Utils.isFunction('a')).to.equal(false) ;
      expect(Utils.isFunction(Symbol('a'))).to.equal(false) ;

      if (document) {
        expect(Utils.isFunction(document.getElementsByTagName('body')))
          .to.equal(false) ;
      }
    }) ;
  }) ;


  describe('.isNull', function () {

    it('should return `true` for `null` values', function () {

      expect(Utils.isNull(null)).to.equal(true) ;
    }) ;

    it('should return `false` for non `null` values', function () {

      let expected = falsey.map(value => {
        return value === null ;
      }) ;

      let actual = falsey.map(value => {
        return Utils.isNull(value) ;
      }) ;

      expect(actual).to.deep.equal(expected) ;

      expect(Utils.isNull([1, 2, 3])).to.equal(false) ;
      expect(Utils.isNull(true)).to.equal(false) ;
      expect(Utils.isNull(new Date)).to.equal(false) ;
      expect(Utils.isNull(new Error)).to.equal(false) ;
      expect(Utils.isNull(Utils)).to.equal(false) ;
      expect(Utils.isNull(Array.slice)).to.equal(false) ;
      expect(Utils.isNull({ 'a': 1 })).to.equal(false) ;
      expect(Utils.isNull(1)).to.equal(false) ;
      expect(Utils.isNull(/x/)).to.equal(false) ;
      expect(Utils.isNull('a')).to.equal(false) ;
      expect(Utils.isNull(Symbol('a'))).to.equal(false) ;
    }) ;
  }) ;


  describe('.isJsonLike', function () {

    it('should return `true` for JsonLike values', function () {

      expect(Utils.isJsonLike('{...}')).to.equal(true) ;
      expect(Utils.isJsonLike('[...]')).to.equal(true) ;
      expect(Utils.isJsonLike({'a': 1})).to.equal(true) ;
    }) ;

    it('should return `false` for non JsonLike values', function () {

      expect(Utils.isJsonLike('{{expr}}')).to.equal(false) ;
      expect(Utils.isJsonLike('{expr')).to.equal(false) ;
      expect(Utils.isJsonLike('[expr')).to.equal(false) ;
      expect(Utils.isJsonLike([1, 2, 3])).to.equal(false) ;
      expect(Utils.isJsonLike(true)).to.equal(false) ;
      expect(Utils.isJsonLike(new Date)).to.equal(false) ;
      expect(Utils.isJsonLike(new Error)).to.equal(false) ;
      expect(Utils.isJsonLike(Utils)).to.equal(false) ;
      expect(Utils.isJsonLike(Array.slice)).to.equal(false) ;
      expect(Utils.isJsonLike(1)).to.equal(false) ;
      expect(Utils.isJsonLike(/x/)).to.equal(false) ;
      expect(Utils.isJsonLike('a')).to.equal(false) ;
      expect(Utils.isJsonLike(Symbol('a'))).to.equal(false) ;
    }) ;
  }) ;


  describe('.isUndefined', function () {

    it('should return `true` for `undefined` values', function () {

      expect(Utils.isUndefined(undefined)).to.equal(true) ;
    }) ;

    it('should return `false` for non `undefined` values', function () {

      let expected = falsey.map(value => {
        return value === undefined ;
      }) ;

      let actual = falsey.map(value => {
        return Utils.isUndefined(value) ;
      }) ;

      expect(actual).to.deep.equal(expected) ;

      expect(Utils.isUndefined([1, 2, 3])).to.equal(false) ;
      expect(Utils.isUndefined(true)).to.equal(false) ;
      expect(Utils.isUndefined(new Date)).to.equal(false) ;
      expect(Utils.isUndefined(new Error)).to.equal(false) ;
      expect(Utils.isUndefined(Utils)).to.equal(false) ;
      expect(Utils.isUndefined(Number.isNaN)).to.equal(false) ;
      expect(Utils.isUndefined({ 'a': 1 })).to.equal(false) ;
      expect(Utils.isUndefined(1)).to.equal(false) ;
      expect(Utils.isUndefined(/x/)).to.equal(false) ;
      expect(Utils.isUndefined('a')).to.equal(false) ;
      expect(Utils.isUndefined(Symbol('a'))).to.equal(false) ;
    }) ;

  }) ;


  describe('.isEmpty', function () {

    it('should return `true` for empty values', function () {

      let expected = empties.map(() => { return true ; }),
          actual = empties.map(Utils.isEmpty) ;

      expect(actual).to.deep.equal(expected) ;

      expect(Utils.isEmpty(true)).to.equal(true) ;
      expect(Utils.isEmpty(1)).to.equal(true) ;
      expect(Utils.isEmpty(NaN)).to.equal(true) ;
      expect(Utils.isEmpty(/x/)).to.equal(true) ;
      expect(Utils.isEmpty(Symbol('a'))).to.equal(true) ;
      expect(Utils.isEmpty()).to.equal(true) ;

      if (Buffer) {
        expect(Utils.isEmpty(new Buffer(0))).to.equal(true) ;
        expect(Utils.isEmpty(new Buffer(1))).to.equal(false) ;
      }
    }) ;

    it('should return `false` for non-empty values', function () {

      expect(Utils.isEmpty([0])).to.equal(false) ;
      expect(Utils.isEmpty({ 'a': 0 })).to.equal(false) ;
      expect(Utils.isEmpty('a')).to.equal(false) ;
    }) ;

    it('should work with an object that has a `length` property', function () {

      expect(Utils.isEmpty({ 'length': 0 })).to.equal(false) ;
    }) ;

    it('should not treat objects with negative lengths as array-like',
    function () {

      function Foo () {}
      Foo.prototype.length = -1 ;

      expect(Utils.isEmpty(new Foo)).to.equal(true) ;
    }) ;

    it('should not treat objects with non-number lengths as array-like',
    function () {

      expect(Utils.isEmpty({ 'length': '0' })).to.equal(false) ;
    }) ;
  }) ;


  describe('.noop', function () {

    it('should return `undefined`', function () {

      let values = empties.concat(true, new Date, Utils, 1, /x/, 'a'),
          expected = values.map(function () {}) ;

      let actual = values.map(function (value, index) {
        return index ? Utils.noop(value) : Utils.noop() ;
      }) ;

      expect(actual).to.deep.equal(expected) ;
    }) ;
  }) ;


  describe('.times', function () {

    it('should coerce non-finite `n` values to `0`', function () {

      [-Infinity, NaN, Infinity].forEach(n => {
        expect(Utils.times(n)).to.deep.equal([]) ;
      }) ;
    }) ;

    it('should coerce `n` to an integer', function () {

      let actual = Utils.times(2.6, n => n) ;
      expect(actual).to.be.deep.equal([0, 1]) ;
    }) ;

    it('should provide the correct `iteratee` arguments', function () {

      let args ;

      Utils.times(1, function () {
        args || (args = Array.prototype.slice.call(arguments)) ;
      }) ;

      expect(args).to.be.deep.equal([0]) ;
    }) ;

    it('should use `identity` when `iteratee` is nullish', function () {

      let values = [null, undefined],
          expected = [0, 1, 2] ;

      let actual = values.map((value, index) => {
        return index ? Utils.times(3, value) : Utils.times(3) ;
      }) ;

      expect(actual[0]).to.be.deep.equal(expected) ;
      expect(actual[1]).to.be.deep.equal(expected) ;
    }) ;

    it('should return an array of the results of each `iteratee` execution',
    function () {

      expect(Utils.times(3, n => n * 2)).to.be.deep.equal([0, 2, 4]) ;
    }) ;

    it('should return an empty array for falsey and negative `n` arguments',
    function () {

      let values = falsey.concat(-1, -Infinity),
          expected = values.map(() => []) ;

      let actual = values.map((value, index) => {
        return index ? Utils.times(value) : Utils.times() ;
      }) ;

      expect(actual).to.be.deep.equal(expected) ;
    }) ;
  }) ;


  describe('.isEqual', function () {

    let symbol1 = Symbol ? Symbol('a') : true,
        symbol2 = Symbol ? Symbol('b') : false ;

    it('should compare primitives', function () {

      let pairs = [
        [1, 1, true], [1, '1', false], [1, 2, false],
        [-0, -0, true], [0, 0, true], [Object(0), Object(0), true],
        [-0, 0, true], [0, '0', false], [0, null, false], [NaN, NaN, true],
        [Object(NaN), Object(NaN), true], [NaN, 'a', false],
        [NaN, Infinity, false], ['a', 'a', true], ['a', 'b', false],
        ['a', ['a'], false], [true, true, true],
        [Object(true), Object(true), true], [true, 1, false],
        [true, 'a', false], [false, false, true],
        [Object(false), Object(false), true], [false, 0, false],
        [false, '', false], [symbol1, symbol1, true],
        [symbol1, Object(symbol1), false],
        [Object(symbol1), Object(symbol1), true], [symbol1, symbol2, false],
        [null, null, true], [null, undefined, false], [null, {}, false],
        [null, '', false], [undefined, undefined, true],
        [undefined, null, false], [undefined, '', false]
      ] ;

      let expected = pairs.map(pair => {
        return pair[2] ;
      }) ;

      let actual = pairs.map(pair => {
        return Utils.isEqual(pair[0], pair[1]) ;
      }) ;

      expect(actual).to.be.deep.equal(expected) ;
    }) ;

    it('should compare arrays', function () {

      let array1 = [true, null, 1, 'a', undefined],
          array2 = [true, null, 1, 'a', undefined] ;

      expect(Utils.isEqual(array1, array2)).to.be.equal(true) ;

      array1 = [[1, 2, 3], new Date(2012, 4, 23), /x/, { 'e': 1 }] ;
      array2 = [[1, 2, 3], new Date(2012, 4, 23), /x/, { 'e': 1 }] ;

      expect(Utils.isEqual(array1, array2)).to.be.equal(true) ;

      array1 = [1] ;
      array1[2] = 3 ;

      array2 = [1] ;
      array2[1] = undefined ;
      array2[2] = 3 ;

      expect(Utils.isEqual(array1, array2)).to.be.true ;

      array1 = [1, 2, 3] ;
      array2 = [3, 2, 1] ;

      expect(Utils.isEqual(array1, array2)).to.be.false ;

      array1 = [1, 2] ;
      array2 = [1, 2, 3] ;

      expect(Utils.isEqual(array1, array2)).to.be.false ;
    }) ;

    it('should treat arrays with identical values but different non-index '
        + 'properties as equal', function () {

      let array1 = [1, 2, 3],
          array2 = [1, 2, 3] ;

      array1.filter = array1.forEach = array1.indexOf =
      array1.lastIndexOf = array1.map = array1.some =
      array1.reduce = array1.reduceRight = null ;

      array2.concat = array2.join = array2.pop =
      array2.reverse = array2.shift = array2.slice =
      array2.sort = array2.splice = array2.unshift = null ;

      expect(Utils.isEqual(array1, array2)).to.be.true ;

      array1 = [1, 2, 3] ;
      array1.a = 1 ;

      array2 = [1, 2, 3] ;
      array2.b = 1 ;

      expect(Utils.isEqual(array1, array2)).to.be.true ;

      array1 = /c/.exec('abcde') ;
      array2 = ['c'] ;

      expect(Utils.isEqual(array1, array2)).to.be.true ;
    }) ;

    it('should compare sparse arrays', function () {

      let array = Array(1) ;

      expect(Utils.isEqual(array, Array(1))).to.be.true ;
      expect(Utils.isEqual(array, [undefined])).to.be.true ;
      expect(Utils.isEqual(array, Array(2))).to.be.false ;
    }) ;

    it('should compare plain objects', function () {

      let object1 = {'a': true, 'b': null, 'c': 1, 'd': 'a', 'e': undefined},
          object2 = {'a': true, 'b': null, 'c': 1, 'd': 'a', 'e': undefined} ;

      expect(Utils.isEqual(object1, object2)).to.be.true ;

      object1 = {'a': [1, 2, 3], 'b': new Date(2012, 4, 23), 'c': /x/,
        'd': {'e': 1}} ;
      object2 = {'a': [1, 2, 3], 'b': new Date(2012, 4, 23), 'c': /x/,
        'd': {'e': 1}} ;

      expect(Utils.isEqual(object1, object2)).to.be.true ;

      object1 = {'a': 1, 'b': 2, 'c': 3} ;
      object2 = {'a': 3, 'b': 2, 'c': 1} ;

      expect(Utils.isEqual(object1, object2)).to.be.false ;

      object1 = {'a': 1, 'b': 2, 'c': 3} ;
      object2 = {'d': 1, 'e': 2, 'f': 3} ;

      expect(Utils.isEqual(object1, object2)).to.be.false ;

      object1 = {'a': 1, 'b': 2} ;
      object2 = {'a': 1, 'b': 2, 'c': 3} ;

      expect(Utils.isEqual(object1, object2)).to.be.false ;
    }) ;

    it('should compare objects regardless of key order', function () {

      let object1 = {'a': 1, 'b': 2, 'c': 3 },
          object2 = {'c': 3, 'a': 1, 'b': 2 } ;

      expect(Utils.isEqual(object1, object2)).to.be.true  ;
    }) ;

    it('should compare nested objects', function () {

      let object1 = {
        'a': [1, 2, 3],
        'b': true,
        'c': 1,
        'd': 'a',
        'e': {
          'f': ['a', 'b', 'c'],
          'g': false,
          'h': new Date(2012, 4, 23),
          'i': Utils.noop,
          'j': 'a'
        }
      } ;

      let object2 = {
        'a': [1, 2, 3],
        'b': true,
        'c': 1,
        'd': 'a',
        'e': {
          'f': ['a', 'b', 'c'],
          'g': false,
          'h': new Date(2012, 4, 23),
          'i': Utils.noop,
          'j': 'a'
        }
      } ;

      expect(Utils.isEqual(object1, object2)).to.be.true ;
    }) ;

    it('should compare object instances', function () {

      function Foo () {
        this.a = 1 ;
      }
      Foo.prototype.a = 1 ;

      function Bar () {
        this.a = 1 ;
      }
      Bar.prototype.a = 2 ;

      expect(Utils.isEqual(new Foo(), new Foo())).to.be.true ;
      expect(Utils.isEqual(new Foo(), new Bar())).to.be.false ;
      expect(Utils.isEqual({'a': 1}, new Foo())).to.be.false ;
      expect(Utils.isEqual({'a': 2}, new Bar())).to.be.false ;
    }) ;

    it('should compare objects with constructor properties', function () {

      expect(Utils.isEqual({'constructor': 1}, {'constructor': 1}))
        .to.be.true ;
      expect(Utils.isEqual({'constructor': 1}, {'constructor': '1'}))
        .to.be.false ;
      expect(Utils.isEqual({'constructor': [1]}, {'constructor': [1]}))
        .to.be.true ;
      expect(Utils.isEqual({'constructor': [1]}, {'constructor': ['1']}))
        .to.be.false ;
      expect(Utils.isEqual({'constructor': Object}, {})).to.be.false ;
    }) ;

    it('should compare arrays with circular references', function () {

      let array1 = [],
          array2 = [] ;

      array1.push(array1) ;
      array2.push(array2) ;

      expect(Utils.isEqual(array1, array2)).to.be.true ;

      array1.push('b') ;
      array2.push('b') ;

      expect(Utils.isEqual(array1, array2)).to.be.true ;

      array1.push('c') ;
      array2.push('d') ;

      expect(Utils.isEqual(array1, array2)).to.be.false ;

      array1 = ['a', 'b', 'c'] ;
      array1[1] = array1 ;
      array2 = ['a', ['a', 'b', 'c'], 'c'] ;

      expect(Utils.isEqual(array1, array2)).to.be.false ;
    }) ;

    it('should compare objects with circular references', function () {

      let object1 = {},
          object2 = {} ;

      object1.a = object1 ;
      object2.a = object2 ;

      expect(Utils.isEqual(object1, object2)).to.be.true ;

      object1 = { 'a': 1, 'b': 2, 'c': 3 } ;
      object1.b = object1 ;
      object2 = { 'a': 1, 'b': { 'a': 1, 'b': 2, 'c': 3 }, 'c': 3 } ;

      expect(Utils.isEqual(object1, object2)).to.be.false ;
    }) ;

    it('should compare objects with multiple circular references',
    function () {

      let array1 = [{}],
          array2 = [{}] ;

      (array1[0].a = array1).push(array1) ;
      (array2[0].a = array2).push(array2) ;

      expect(Utils.isEqual(array1, array2)).to.be.true ;
    }) ;

    it('should compare objects with complex circular references', function () {

      let object1 = {
        'foo': {'b': {'c': {'d': {}}}},
        'bar': {'a': 2}
      } ;
      let object2 = {
        'foo': {'b': {'c': {'d': {}}}},
        'bar': {'a': 2}
      } ;

      object1.foo.b.c.d = object1 ;
      object1.bar.b = object1.foo.b ;

      object2.foo.b.c.d = object2 ;
      object2.bar.b = object2.foo.b ;

      expect(Utils.isEqual(object1, object2)).to.be.true ;
    }) ;

    it('should compare objects with shared property values', function () {

      let object1 = {
        'a': [1, 2]
      } ;
      let object2 = {
        'a': [1, 2],
        'b': [1, 2]
      } ;
      object1.b = object1.a ;

      expect(Utils.isEqual(object1, object2)).to.be.true ;
    }) ;

    it('should return `false` for objects with custom `toString` methods',
    function () {

      let primitive,
          object = {'toString': function () { return primitive ; } },
          values = [true, null, 1, 'a', undefined],
          expected = values.map(() => false) ;

      let actual = values.map(value => {
        primitive = value ;
        return Utils.isEqual(object, value) ;
      }) ;

      expect(actual).to.be.deep.equal(expected) ;
    }) ;

    it('should avoid common type coercions', function () {

      expect(Utils.isEqual(true, Object(false))).to.be.false ;
      expect(Utils.isEqual(Object(false), Object(0))).to.be.false ;
      expect(Utils.isEqual(false, Object(''))).to.be.false ;
      expect(Utils.isEqual(Object(36), Object('36'))).to.be.false ;
      expect(Utils.isEqual(0, '')).to.be.false ;
      expect(Utils.isEqual(1, true)).to.be.false ;
      expect(Utils.isEqual(1337756400000, new Date(2012, 4, 23))).to.be.false ;
      expect(Utils.isEqual('36', 36)).to.be.false ;
      expect(Utils.isEqual(36, '36')).to.be.false ;
    }) ;

    it('should compare date objects', function () {

      let date = new Date(2012, 4, 23) ;

      expect(Utils.isEqual(date, new Date(2012, 4, 23))).to.be.true ;
      expect(Utils.isEqual(date, new Date(2013, 3, 25))).to.be.false ;
      expect(Utils.isEqual(date, {'getTime': +date})).to.be.false ;
      expect(Utils.isEqual(new Date('a'), new Date('a'))).to.be.false ;
    }) ;

    it('should compare error objects', function () {

      let pairs = [
        'Error',
        'EvalError',
        'RangeError',
        'ReferenceError',
        'SyntaxError',
        'TypeError',
        'URIError'
      ].map((type, index, errorTypes) => {
        let otherType = errorTypes[++index % errorTypes.length],
            CtorA = root[type],
            CtorB = root[otherType] ;

        return [
          new CtorA('a'), new CtorA('a'),
          new CtorB('a'), new CtorB('b')
        ] ;
      }) ;

      let expected = pairs.map(() => [true, false, false]) ;

      let actual = pairs.map(pair => {
        return [
          Utils.isEqual(pair[0], pair[1]),
          Utils.isEqual(pair[0], pair[2]),
          Utils.isEqual(pair[2], pair[3])
        ] ;
      }) ;

      expect(actual).to.be.deep.equal(expected) ;
    }) ;

    it('should compare functions', function () {

      function a () { return 1 + 2 ; }
      function b () { return 1 + 2 ; }

      expect(Utils.isEqual(a, a)).to.be.true ;
      expect(Utils.isEqual(a, b)).to.be.false ;
    }) ;

    it('should compare regexes', function () {

      expect(Utils.isEqual(/x/gim, /x/gim)).to.be.true ;
      expect(Utils.isEqual(/x/gim, /x/mgi)).to.be.true ;
      expect(Utils.isEqual(/x/gi, /x/g)).to.be.false ;
      expect(Utils.isEqual(/x/, /y/)).to.be.false ;
      expect(Utils.isEqual(/x/g, {
        'global': true, 'ignoreCase': false,
        'multiline': false, 'source': 'x'
      })).to.be.false ;
    }) ;
  }) ;


  describe('iteration method', function () {

    let methods = [
      //'filter',
      'forEach',
      'forEachRight',
      'forOwn',
      //'forOwnRight',
      //'map',
      'some'
    ] ;
    let collectionMethods = [
      //'filter',
      'forEach',
      'forEachRight',
      //'map',
      'some'
    ] ;
    let iterationMethods = [
      'forEach',
      'forEachRight',
      'forOwn',
      //'forOwnRight'
    ] ;
    let objectMethods = [
      'forOwn',
      //'forOwnRight',
    ] ;
    let rightMethods = [
      'forEachRight',
      //'forOwnRight'
    ] ;
    let exitMethods = [
      'forEach',
      'forEachRight',
      'forOwn',
      //'forOwnRight',
      'transform'
    ] ;


    methods.forEach((methodName) => {

      let array = [1, 2, 3],
          func  = Utils[methodName] ;


      it(methodName + ' should provide the correct iteratee arguments',
      function () {

        let args = [],
            expected = [
              [1, 0, true], [2, 1, true], [3, 2, true]
            ] ;

        func(array, (value, index, collection) => {
          args.push([
            value, index, collection == array
          ]) ;
        }) ;

        if (rightMethods.indexOf(methodName) > -1) {
          let tmp = expected[0] ;
          expected[0] = expected[2] ;
          expected[2] = tmp ;
        }

        expect(args).to.be.deep.equal(expected) ;
      }) ;

      it(methodName + ' should treat sparse arrays as dense',
      function () {

        let array = [1] ;
        array[2] = 3 ;

        let expected = objectMethods.indexOf(methodName) > -1 ?
           [[1, '0', array], [undefined, '1', array], [3, '2', array]]
          : [[1,  0, array],  [undefined,  1,  array], [3,  2,  array]] ;

        if (rightMethods.indexOf(methodName) > -1)
          expected.reverse() ;

        let argsList = [] ;
        func(array, (value, index, collection) => {
          argsList.push([value, index, collection]) ;
        }) ;

        if (objectMethods.indexOf(methodName) === -1)
          expect(argsList).to.be.deep.equal(expected) ;
      }) ;
    }) ;

    methods.filter(method => objectMethods.indexOf(method) === -1).forEach(
    methodName => {

      let array = [1, 2, 3],
          func = Utils[methodName] ;

      array.a = 1 ;

      it(methodName + ' should not iterate custom properties on arrays',
      function () {

        let keys = [] ;
        func(array, (value, key) => {
          keys.push(key) ;
        }) ;

        expect(keys.indexOf('a')).to.be.equal(-1) ;
      }) ;
    }) ;


    methods.forEach(methodName => {

      let func = Utils[methodName] ;

      it(methodName + ' iterates over own string keyed properties of objects',
      function () {

        function Foo () {
          this.a = 1 ;
        }
        Foo.prototype.b = 2 ;

        let values = [] ;
        func(new Foo, value => { values.push(value) ; }) ;
        expect(values).to.be.deep.equal([1]) ;
      }) ;
    }) ;

    iterationMethods.forEach(methodName => {

      let array = [1, 2, 3],
          func = Utils[methodName] ;

      it(methodName + ' should return the collection', function () {

        expect(func(array, Boolean)).to.be.equal(array) ;
      }) ;
    }) ;

    methods.forEach(methodName => {

      let func = Utils[methodName],
          isSome = methodName == 'some' ;

      it(methodName + ' should ignore changes to `array.length`', function () {

        let count = 0,
            array = [1] ;

        func(array, () => {
          if (++count == 1)
            array.push(2) ;
          return !(isSome) ;
        }, null) ;

        expect(count).to.be.equal(1) ;
      }) ;
    }) ;

    methods.concat(collectionMethods).forEach(methodName => {

      let func = Utils[methodName],
          isSome = methodName == 'some' ;

      it(methodName + ' should ignore added `object` properties', function () {

        let count = 0,
            object = {'a': 1} ;

        func(object, function () {
          if (++count == 1)
            object.b = 2 ;
          return !(isSome) ;
        }, null) ;

        expect(count).to.be.equal(1) ;
      }) ;
    }) ;

    exitMethods.forEach(methodName => {

      let func = Utils[methodName] ;

      it(methodName + ' can exit early when iterating arrays', function () {

        let array = [1, 2, 3],
            values = [] ;

        func(array, (value, other) => {
          values.push(Array.isArray(value) ? other : value) ;
          return false ;
        }) ;

        expect(values)
          .to.be.deep.equal([rightMethods.indexOf(methodName) > -1 ? 3 : 1]) ;
      }) ;

      it(methodName + ' can exit early when iterating objects', function () {

        let object = {'a': 1, 'b': 2, 'c': 3},
            values = [] ;

        func(object, (value, other) => {
          values.push(Array.isArray(value) ? other : value) ;
          return false ;
        }) ;

        expect(values.length).to.be.equal(1) ;
      }) ;


    }) ;
  }) ;


  describe('extend', function () {

    let object ;
    let source1 = {d: 'bar', e: 2} ;
    let source2 = {f: 'lol'} ;
    let source3 = {b: false, g: 'mdr'} ;
    let source4 = {f: ':-)'} ;

    beforeEach(function () {

      object = {a: 1, b: 2, c: 'foo'} ;
    }) ;

    it('assign source\'s properties to object', function () {

      let expected = {
        a: 1, b: 2, c: 'foo', d: 'bar', e: 2
      } ;

      expect(Utils.extend(object, source1)).to.be.deep.equal(expected) ;
    }) ;

    it('assign multiple source\'s properties to object', function () {

      let expected = {
        a: 1, b: 2, c: 'foo', d: 'bar', e: 2, f: 'lol'
      } ;

      expect(Utils.extend(object, source1, source2))
        .to.be.deep.equal(expected) ;
    }) ;

    it('a source ecrase object\'s equivalent properties', function () {

      let expected = {
        a: 1, b: false, c: 'foo', g: 'mdr'
      } ;

      expect(Utils.extend(object, source3))
        .to.be.deep.equal(expected) ;
    }) ;

    it('a source ecrase previous source\' properties to object', function () {

      let expected = {
        a: 1, b: 2, c: 'foo', f: ':-)'
      } ;

      expect(Utils.extend(object, source2, source4))
        .to.be.deep.equal(expected) ;
    }) ;

  }) ;


  describe('.camelCase', function () {

    it('should work with numbers', function () {

      expect(Utils.camelCase('12 feet')).to.equal('12Feet') ;
      expect(Utils.camelCase('enable 6h format')).to.equal('enable6HFormat') ;
      expect(Utils.camelCase('enable 24H format')).to.equal('enable24HFormat') ;
      expect(Utils.camelCase('too legit 2 quit')).to.equal('tooLegit2Quit') ;
      expect(Utils.camelCase('walk 500 miles')).to.equal('walk500Miles') ;
      expect(Utils.camelCase('xhr2 request')).to.equal('xhr2Request') ;
    }) ;

    it('should handle acronyms', function () {

      ['safe HTML', 'safeHTML'].forEach(function (string) {
        expect(Utils.camelCase(string)).to.equal('safeHtml') ;
      }) ;

      ['escape HTML entities', 'escapeHTMLEntities'].forEach(function (string) {
        expect(Utils.camelCase(string)).to.equal('escapeHtmlEntities') ;
      }) ;

      ['XMLHttpRequest', 'XmlHTTPRequest'].forEach(function (string) {
        expect(Utils.camelCase(string)).to.equal('xmlHttpRequest') ;
      }) ;
    }) ;
  }) ;


  describe('.kebabCase', function () {

    it('ne change pas un élément déjà en kebabCase', function () {

      expect(Utils.kebabCase('foo-bar')).to.be.equal('foo-bar') ;
    }) ;

    it('transforme des éléments espacés', function () {

      expect(Utils.kebabCase('Foo bar')).to.be.equal('foo-bar') ;
      expect(Utils.kebabCase('Foo Bar')).to.be.equal('foo-bar') ;
    }) ;

    it('transforme du camelCase', function () {

      expect(Utils.kebabCase('fooBar')).to.be.equal('foo-bar') ;
    }) ;

    it('transforme de la notation constante `__FOO_BAR__`', function () {

      expect(Utils.kebabCase('__FOO_BAR__')).to.be.equal('foo-bar') ;
    }) ;
  }) ;

  describe('levenshtein', function () {

    it('renvoit la longueur de str2 si str1 vide', function () {

      expect(Utils.levenshtein('', 'foo')).to.be.equal(3) ;
    }) ;

    it('renvoit la longueur de str1 si str2 vide', function () {

      expect(Utils.levenshtein('bar!', '')).to.be.equal(4) ;
    }) ;

    it('Test avec des valeurs', function () {

      expect(Utils.levenshtein('niche', 'chien')).to.be.equal(4) ;
      expect(Utils.levenshtein('chat', 'chien')).to.be.equal(3) ;
      expect(Utils.levenshtein('chat', 'poule')).to.be.equal(5) ;
      expect(Utils.levenshtein('poue', 'poule')).to.be.equal(1) ;
    }) ;
  }) ;


  describe('clone methods', function () {

    class Foo {

      constructor () {

        this.a = 1 ;
        this.b = 1 ;
      }

      c () {}
    }
    Foo.prototype.b = 1 ;

    let objects = {
      'arrays': ['a', '', 1],
      'array-like-objects': {'0': 'a', '1': '', 'length': 3},
      'booleans': false,
      'date objects': new Date,
      'Foo instances': new Foo,
      'objects': {'a': 0, 'b': 1, 'c': 2},
      'objects with object values': {'a': /a/, 'b': ['B'], 'c': {'C': 1}},
      'null values': null,
      'numbers': 0,
      'regexes': /a/gim,
      'strings': 'a',
      'undefined values': undefined
    } ;

    objects.arrays.length = 3 ;

    let uncloneable = {
      'DOM elements': body,
      'functions': Foo
    } ;


    it('.clone should perform a shallow clone', function () {

      let array = [{'a': 0}, {'b': 1}],
          actual = Utils.clone(array) ;

      expect(actual).to.be.deep.equal(array) ;
      expect(actual !== array && actual[0] === array[0]).to.be.true ;
    }) ;

    it('cloneDeep should deep clone objects with circular references',
    function () {

      let object = {
        'foo': {'b': {'c': {'d': {}}}},
        'bar': {}
      } ;

      object.foo.b.c.d = object ;
      object.bar.b = object.foo.b ;

      let actual = Utils.cloneDeep(object) ;
      expect(actual.bar.b === actual.foo.b &&
             actual === actual.foo.b.c.d &&
             actual !== object).to.be.true ;
    }) ;

    it('cloneDeep should deep clone objects with lots of circular references',
    function () {

      let cyclical = {} ;
      Utils.times(200 + 1, index => {
        cyclical['v' + index] =
          [index ? cyclical['v' + (index - 1)] : cyclical] ;
      }) ;

      let clone  = Utils.cloneDeep(cyclical),
          actual = clone['v' + 200][0] ;

      expect(actual).to.be.equal(clone['v' + (200 - 1)]) ;
      expect(actual).to.be.not.equal(cyclical['v' + (200 - 1)]) ;
    }) ;

    ['clone', 'cloneDeep'].forEach(methodName => {
      let func   = Utils[methodName],
          isDeep = methodName == 'cloneDeep' ;

      Utils.forOwn(objects, (object, key) => {

        it(methodName + ' should clone ' + key, function () {

          let actual  = func(object) ;

          expect(actual).to.be.deep.equal(object) ;

          if (Utils.isObject(object))
            expect(actual).to.be.not.equal(object) ;
          else
            expect(actual).to.be.equal(object) ;
        }) ;
      }) ;

      it(methodName + ' should clone array buffers', function () {

        if (ArrayBuffer) {
          let arrayBuffer = new ArrayBuffer(2) ;
          let actual = func(arrayBuffer) ;
          expect(actual.byteLength).to.be.equal(arrayBuffer.byteLength) ;
          expect(actual).to.be.not.equal(arrayBuffer) ;
        }
      }) ;

      it(methodName + ' should clone buffers', function () {

        if (Buffer) {
          let buffer = new Buffer([1, 2]),
              actual = func(buffer) ;

          expect(actual.byteLength).to.be.equal(buffer.byteLength) ;
          expect(actual.inspect()).to.be.equal(buffer.inspect()) ;
          expect(actual).to.be.not.equal(buffer) ;

          buffer[0] = 2 ;
          expect(actual[0]).to.be.equal(isDeep ? 1 : 2) ;
        }
      }) ;


      it(methodName + ' should clone `index` and `input` array properties',
      function () {

        let array = /c/.exec('abcde'),
            actual = func(array) ;

        expect(actual.index).to.be.equal(2) ;
        expect(actual.input).to.be.equal('abcde') ;
      }) ;

      it(methodName + ' should clone `lastIndex` regexp property',
      function () {

        let regexp = /c/g ;
        regexp.exec('abcde') ;

        expect(func(regexp).lastIndex).to.be.equal(3) ;
      }) ;

      it(methodName + ' should clone prototype objects', function () {

        let actual = func(Foo.prototype) ;

        expect(actual instanceof Foo).to.be.false ;
        expect(actual).to.be.deep.equal({'b': 1}) ;
      }) ;

      it(methodName + ' should ensure `value` constructor is a function '
         + 'before using its `[[Prototype]]`',
      function () {

        Foo.prototype.constructor = null ;
        expect(func(new Foo) instanceof Foo).to.be.false ;
        Foo.prototype.constructor = Foo ;
      }) ;

      it(methodName + ' should clone properties that shadow those on '
        + '`Object.prototype`',
      function () {

        let object = {
          'constructor':          Object.prototype.constructor,
          'hasOwnProperty':       Object.prototype.hasOwnProperty,
          'isPrototypeOf':        Object.prototype.isPrototypeOf,
          'propertyIsEnumerable': Object.prototype.propertyIsEnumerable,
          'toLocaleString':       Object.prototype.toLocaleString,
          'toString':             Object.prototype.toString,
          'valueOf':              Object.prototype.valueOf
        } ;

        let actual = func(object) ;

        expect(actual).to.be.deep.equal(object) ;
        expect(actual).to.be.not.equal(object) ;
      }) ;

      it(methodName + ' should clone symbol objects', function () {

        let symbol = Symbol ? Symbol('a') : undefined ;
        if (Symbol) {
          expect(func(symbol)).to.be.equal(symbol) ;

          let object = Object(symbol),
              actual = func(object) ;

          expect(typeof actual).to.be.equal('object') ;
          expect(actual).to.be.not.equal(object) ;
        }
      }) ;

      it(methodName + ' should not clone symbol primitives', function () {

        let symbol = Symbol ? Symbol('a') : undefined ;
        if (Symbol)
          expect(func(symbol)).to.be.equal(symbol) ;
      }) ;

      it(methodName + ' should not error on DOM elements', function () {

        if (document) {
          let element = document.createElement('div') ;

          try {
            expect(func(element)).to.be.deep.equal({}) ;
          } catch (e) {
            expect.ok(false, e.message) ;
          }
        }
      }) ;

      Utils.forOwn(uncloneable, (value, key) => {
        it(methodName + ' should not clone ' + key, function () {

          if (value) {
            let object = {'a': value, 'b': {'c': value}},
                actual = func(object),
                expected = (typeof value == 'function' && !!value.c) ?
                           {'c': Foo.c} : {} ;

            expect(actual).to.be.deep.equal(object) ;
            expect(actual).to.be.not.equal(object) ;
            expect(func(value)).to.be.deep.equal(expected) ;
          }
        }) ;
      }) ;
    }) ;
  }) ;

  describe('.some', function () {

    it('should return `true` if `predicate` returns truthy for any element',
    function () {

      expect(Utils.some([false, 1, ''], value => { return value ; }))
        .to.be.equal(true) ;
      expect(Utils.some([null, 'a', 0], value => { return value ; }))
        .to.be.equal(true) ;
    }) ;

    it('should return `false` for empty collections', function () {

      let expected = empties.map(() => { return false ; }),
          actual   = empties.map(value => {
            try {
              return Utils.some(value, value => { return value ; }) ;
            } catch (e) { e.message = '' ; }
          }) ;

      expect(actual).to.be.deep.equal(expected) ;
    }) ;

    it('should return `true` as soon as `predicate` returns truthy',
    function () {

      let count = 0 ;

      expect(Utils.some([null, true, null], value => {
        count++ ;
        return value ;
      })).to.be.true ;

      expect(count).to.be.equal(2) ;
    }) ;

    it('should return `false` if `predicate` returns falsey for all elements',
    function () {

      expect(Utils.some([false, false, false], value => { return value ; }))
        .to.be.false ;
      expect(Utils.some([null, 0, ''], value => { return value ; }))
        .to.be.false ;
    }) ;

    it('should use identity when `predicate` is nullish', function () {

      let values   = [null, undefined],
          expected = values.map(() => { return false ; }) ;

      let actual = values.map((value, index) => {
        let array = [0, 0] ;
        return index ? Utils.some(array, value) : Utils.some(array) ;
      }) ;

      expect(actual).to.be.deep.equal(expected) ;

      expected = values.map(() => { return true ; }) ;
      actual = values.map((value, index) => {
        let array = [0, 1] ;
        return index ? Utils.some(array, value) : Utils.some(array) ;
      }) ;

      expect(actual).to.be.deep.equal(expected) ;
    }) ;
  }) ;


  describe('.transform', function () {

    function Foo () {
      this.a = 1 ;
      this.b = 2 ;
      this.c = 3 ;
    }

    it('should create an object with the same `[[Prototype]]` as `object` '
       + 'when `accumulator` is nullish', function () {

      let accumulators = [null, undefined],
          object       = new Foo,
          expected     = accumulators.map(() => { return true ; }) ;

      let iteratee = (result, value, key) => {
        result[key] = value * value ;
      } ;

      let mapper = (accumulator, index) => {
        return index ? Utils.transform(object, iteratee, accumulator)
          : Utils.transform(object, iteratee) ;
      } ;

      let results = accumulators.map(mapper) ;
      let actual = results.map(result => {
        return result instanceof Foo ;
      }) ;

      expect(actual).to.be.deep.equal(expected) ;

      expected = accumulators.map(() => {
        return {'a': 1, 'b': 4, 'c': 9} ;
      }) ;
      actual = results.map(res => {
        return {
          a: res.a,
          b: res.b,
          c: res.c
        } ;
      }) ;
      expect(actual).to.be.deep.equal(expected) ;

      object = {'a': 1, 'b': 2, 'c': 3} ;
      actual = accumulators.map(mapper) ;
      expect(actual).to.be.deep.equal(expected) ;

      object = [1, 2, 3] ;
      expected = accumulators.map(() => [1, 4, 9]) ;
      actual = accumulators.map(mapper) ;
      expect(actual).to.be.deep.equal(expected) ;
    }) ;

    it('should support an `accumulator` value', function () {

      let values   = [new Foo, [1, 2, 3], {'a': 1, 'b': 2, 'c': 3}],
          expected = values.map(() => [1, 4, 9]) ;

      let actual = values.map(value => {
        return Utils.transform(value, (result, value) => {
          result.push(value * value) ;
        }, []) ;
      }) ;
      expect(actual).to.be.deep.equal(expected) ;

      let object   = {'a': 1, 'b': 4, 'c': 9} ;
      expected = [object, {'0': 1, '1': 4, '2': 9}, object] ;

      actual = values.map(value => {
        return Utils.transform(value, (result, value, key) => {
          result[key] = value * value ;
        }, {}) ;
      }) ;
      expect(actual).to.be.deep.equal(expected) ;
    }) ;

    it('should treat sparse arrays as dense', function () {

      let actual = Utils.transform(Array(1), (result, value, index) => {
        result[index] = String(value) ;
      }) ;

      expect(actual).to.be.deep.equal(['undefined']) ;
    }) ;

    it('should work without an `iteratee` argument', function () {

      expect(Utils.transform(new Foo)).to.be.instanceOf(Foo) ;
    }) ;

    it('should ensure `object` is an object before using its `[[Prototype]]`',
    function () {

      let Ctors    = [Boolean, Boolean, Number, Number, Number, String, String],
          values   = [false, true, 0, 1, NaN, '', 'a'],
          expected = values.map(() => { return {} ; }) ;

      let results = values.map(value => {
        return Utils.transform(value) ;
      }) ;
      expect(results).to.be.deep.equal(expected) ;

      expected = values.map(() => { return false ; }) ;
      let actual = results.map((value, index) => {
        return value instanceof Ctors[index] ;
      }) ;
      expect(actual).to.be.deep.equal(expected) ;
    }) ;

    it('should ensure `object` constructor is a function before using its '
      + '`[[Prototype]]`', function () {

      Foo.prototype.constructor = null ;
      expect(Utils.transform(new Foo)).to.be.not.instanceOf(Foo) ;
      Foo.prototype.constructor = Foo ;
    }) ;

    it('should create an empty object when given a falsey `object` argument',
    function () {

      let expected = falsey.map(() => { return {} ; }) ;

      let actual = falsey.map((object, index) => {
        return index ? Utils.transform(object) : Utils.transform() ;
      }) ;

      expect(actual).to.be.deep.equal(expected) ;
    }) ;
  }) ;


  describe('.remove', function () {

    it('should modify the array and return removed elements', function () {

      let array  = [1, 2, 3, 4] ;
      let actual = Utils.remove(array, n => {
        return n % 2 == 0 ;
      }) ;

      expect(array).to.be.deep.equal([1, 3]) ;
      expect(actual).to.be.deep.equal([2, 4]) ;
    }) ;

    it('should provide the correct `predicate` arguments', function () {

      let argsList = [],
          array    = [1, 2, 3],
          clone    = array.slice() ;

      Utils.remove(array, function (n, index) {

        let args = [].slice.call(arguments) ;
        args[2] = args[2].slice() ;
        argsList.push(args) ;
        return index % 2 == 0 ;
      }) ;

      expect(argsList)
        .to.be.deep.equal([[1, 0, clone], [2, 1, clone], [3, 2, clone]]) ;
    }) ;

    it('should work with `_.matches` shorthands', function () {

      let objects = [{'a': 0, 'b': 1}, {'a': 1, 'b': 2}] ;
      Utils.remove(objects, {'a': 1}) ;
      expect(objects).to.be.deep.equal([{'a': 0, 'b': 1}]) ;
    }) ;

    it('should preserve holes in arrays', function () {

      let array = [1, 2, 3, 4] ;
      delete array[1] ;
      delete array[3] ;

      Utils.remove(array, n => {
        return n === 1 ;
      }) ;

      expect('0' in array).to.be.false ;
      expect('2' in array).to.be.false ;
    }) ;

    it('should treat holes as `undefined`', function () {

      let array = [1, 2, 3] ;
      delete array[1] ;

      Utils.remove(array, n => {
        return n == null ;
      }) ;

      expect(array).to.be.deep.equal([1, 3]) ;
    }) ;

    it('should not mutate the array until all elements to remove '
      + 'are determined', function () {

      let array = [1, 2, 3] ;

      Utils.remove(array, (n, index) => {
        return index % 2 == 0 ;
      }) ;

      expect(array).to.be.deep.equal([2]) ;
    }) ;
  }) ;


  describe('.ucfirst', function () {

    it('premier caractère en majuscule', function () {

      expect(Utils.ucfirst('foobar')).to.be.equal('Foobar') ;
    }) ;

    it('Ok accents', function () {

      expect(Utils.ucfirst('école')).to.be.equal('École') ;
    }) ;

    it('Ok si caractères autre que lettre', function () {

      expect(Utils.ucfirst('1foobar')).to.be.equal('1foobar') ;
      expect(Utils.ucfirst('_foobar')).to.be.equal('_foobar') ;
    }) ;

    it('pas de pb avec autres types', function () {

      expect(Utils.ucfirst(2)).to.be.equal(2) ;
      expect(Utils.ucfirst(true)).to.be.equal(true) ;
      expect(Utils.ucfirst([1, 2])).to.be.deep.equal([1, 2]) ;
    }) ;
  }) ;


  describe('.uuid', function () {

    it('retourne un uuid v4', function () {

      let reg = RegExp(
        '^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$',
        'i'
      ) ;
      expect(reg.test(Utils.uuid())).to.be.true ;
    }) ;

    it('toujours différent', function () {

      expect(Utils.uuid()).to.be.not.equal(Utils.uuid()) ;
    }) ;
  }) ;


  describe('.uuidRegExp', function () {

    it('retourne une RegExp', function () {

      expect(Utils.isRegExp(Utils.uuidRegExp())).to.be.true ;
    }) ;

    it('retourne la RegExp pour valider un uuid v4', function () {

      let reg = RegExp(
        '^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$',
        'i'
      ) ;
      expect(Utils.uuidRegExp()).to.be.deep.equal(reg) ;
    }) ;
  }) ;
}) ;

// buddy ignore:end

