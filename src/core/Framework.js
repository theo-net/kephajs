'use strict' ;

/**
 * src/core/Framework.js
 */

const Utils  = require('./Utils'),
      Kernel = require('./Kernel'),
      Entity = require('./Entity') ;

/**
 * Réunit tous les outils nécessaires
 */
class Framework extends Utils {

  /**
   * Retourne le Kernel
   * @returns {Kernel}
   * @static
   */
  static kernel () {

    return new Kernel() ;
  }
}

Framework.Entity = Entity ;

module.exports = Framework ;

