'use strict' ;

/**
 * src/core/Application.js
 */

const KernelAccess = require('./KernelAccess') ;

/**
 * Chaque script/application réalisés avec le framework doit prendre comme
 * racine cette classe (même s'il passe par une autre dérivée, Cli, Server, ...)
 *
 * Celle-ci fournit un accès aux Kernel, s'enregistre comme service, et fournit
 * une serie d'outils disponibles directement avec Application.kjs
 */
class Application extends KernelAccess {

  constructor () {

    super() ;

    // S'enregistre comme service
    this.kernel().container().set('$application', this) ;

    // On ajoute une série de méthodes utilitaires
    this.kjs = require('./Framework') ;
  }


  /**
   * Définit ou donne le nom de notre application
   *
   * @param {String} [name] Nom de l'application
   * @returns {*} Le nom, si aucun paramètre, `this` sinon
   */
  name (name) {

    if (name) {
      this._name = name ;
      return this ;
    }
    else
      return this._name ;
  }


  /**
   * Définit ou donne la version de notre application
   *
   * @param {String} [version] Version de l'application
   * @returns {*} La version, si aucun paramètre, `this` sinon
   */
  version (version) {

    if (version) {
      this._version = version ;
      return this ;
    }
    else
      return this._version ;
  }
}

module.exports = Application ;

