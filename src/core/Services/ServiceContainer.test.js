'use strict' ;

/**
 * src/core/Services/ServiceContainer.test.js
 */

let expect = require('chai').expect,
    sinon  = require('sinon') ;

const ServiceContainer = require('./ServiceContainer'),
      Kernel = require('../Kernel') ;
const kernel = new Kernel() ;

describe('ServiceContainer', function () {

  // On définit des faux services
  class A {}
  class B {}
  class C {}

  it('peut enregistrer le kernel pour le transmettre aux services',
  function () {

    let services = new ServiceContainer(kernel) ;
    expect(services._kernel).to.be.equal(kernel) ;
  }) ;

  describe('set', function () {

    it('defini un service', function () {

      let services = new ServiceContainer() ;
      services.set('test', new A()) ;

      expect(services._services['test']).to.be.instanceOf(A) ;
    }) ;

    it('peut définir une fonction comme service', function () {

      let fct = function () {} ;
      let services = new ServiceContainer() ;
      services.set('test', fct) ;

      expect(services._services['test']).to.be.equal(fct) ;
    }) ;
  }) ;


  describe('get', function () {

    it('Retourne un service', function () {

      let a = new A() ;
      let aBis = new A() ;
      let services = new ServiceContainer() ;
      services.set('test', a) ;

      expect(services.get('test')).to.equal(a) ;
      expect(services.get('test')).to.not.equal(aBis) ;
      expect(services.get('test2')).to.equal(undefined) ;
    }) ;

    it('si le service est une fonction, l\'exécute avec le Kernel',
    function () {

      let spy = sinon.spy() ;
      let services = new ServiceContainer() ;
      services.set('test', spy) ;
      services.get('test') ;

      expect(spy.calledOnce).to.equal(true) ;
      expect(spy.calledWithExactly(services._kernel)).to.be.true ;
    }) ;

    it('si le service est une fonction, retourne le résultat', function () {

      let services = new ServiceContainer() ;
      services.set('test', () => 'foobar') ;

      expect(services.get('test')).to.be.equal('foobar') ;
    }) ;
  }) ;


  describe('has', function () {

    it('indique si un service est defini', function () {

      let services = new ServiceContainer() ;
      services.set('test', new A()) ;

      expect(services.has('test')).to.equal(true) ;
      expect(services.has('test2')).to.equal(false) ;
    }) ;
  }) ;


  describe('getAll', function () {

    it('retourne tous les services', function () {

      let services = new ServiceContainer() ;
      services.set('serviceA', new A()) ;
      services.set('serviceB', new B()) ;
      services.set('serviceC', new C()) ;

      expect(services.getAll()).to.deep.equal(services._services) ;
    }) ;
  }) ;
}) ;

