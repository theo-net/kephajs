'use strict' ;

/**
 * src/core/Services/Filter.test.js
 */

const expect = require('chai').expect ;

const Filter       = require('./Filter'),
      filterFilter = require('./filterFilter') ;

describe('Filter', function () {

  let filter,
      f1 = () => {},
      f2 = () => {} ;

  beforeEach(function () {

    filter = new Filter() ;
    filter.register('f1', f1) ;
  }) ;

  it('le filtre `filter` est défini', function () {

    expect(filter.get('filter')).to.be.equal(filterFilter) ;
  }) ;

  it('indique si un filtre est disponible', function () {

    expect(filter.has('f1')).to.be.equal(true) ;
    expect(filter.has('f2')).to.be.equal(false) ;
  }) ;

  it('enregistre un filtre', function () {

    filter.register('test', f2) ;
    expect(filter.has('test')).to.be.true ;
  }) ;

  it('récupère un filtre', function () {

    filter.register('test', f2) ;
    expect(filter.get('test')).to.be.equal(f2) ;
  }) ;
}) ;

