'use strict' ;

/**
 * src/core/Services/interpolate.test.js
 */

const expect = require('chai').expect,
      sinon  = require('sinon') ;

const Kernel = require('../Kernel') ;
const kernel = new Kernel(),
      interpolate = kernel.get('$interpolate') ;

describe('interpolate', function () {

  /**
   * Interpolating String
   */

  it('produces an identity function for static content', function () {

    let interp = interpolate('hello') ;
    expect(interp instanceof Function).to.be.true ;
    expect(interp()).to.be.equal('hello') ;
  }) ;

  it('evaluates a single expression', function () {

    let interp = interpolate('{{anAttr}}') ;
    expect(interp({anAttr: '2016'})).to.be.equal('2016') ;
  }) ;

  it('evaluates many expressions', function () {

    let interp = interpolate('First {{anAttr}}, then {{anotherAttr}}!') ;
    expect(interp({anAttr: 'january', anotherAttr: '2016'}))
      .to.be.equal('First january, then 2016!') ;
  }) ;

  it('passes through ill-defined interpolations', function () {

    let interp = interpolate('why u no }}work{{') ;
    expect(interp({})).to.be.equal('why u no }}work{{') ;
  }) ;

  it('turns nulls into empty strings', function () {

    let interp = interpolate('{{aNull}}') ;
    expect(interp({aNull: null})).to.be.equal('') ;
  }) ;

  it('turns undefineds into empty strings', function () {

    let interp = interpolate('{{anUndefined}}') ;
    expect(interp({})).to.be.equal('') ;
  }) ;

  it('turns numbers into strings', function () {

    let interp = interpolate('{{aNumber}}') ;
    expect(interp({aNumber: 2016})).to.be.equal('2016') ;
  }) ;

  it('turns booleans into strings', function () {

    let interp = interpolate('{{aBoolean}}') ;
    expect(interp({aBoolean: true})).to.be.equal('true') ;
  }) ;

  it('turns arrays into JSON strings', function () {

    let interp = interpolate('{{anArray}}') ;
    expect(interp({
      anArray: [1, 2, [3]]          // buddy ignore:line
    })).to.be.equal('[1,2,[3]]') ;
  }) ;

  it('turns objects into JSON strings', function () {

    let interp = interpolate('{{anObject}}') ;
    expect(interp({anObject: {a: 1, b: '2'}})).to.be.equal('{"a":1,"b":"2"}') ;
  }) ;


  /**
   * Supporting Escaped Interpolation Symbols
   */

  it('unescapes escaped sequences', function () {

    let interp = interpolate('\\{\\{expr\\}\\} {{expr}} \\{\\{expr\\}\\}') ;
    expect(interp({expr: 'value'})).to.be.equal('{{expr}} value {{expr}}') ;
  }) ;


  /**
   * Skipping Interpolation When There are No Expressions
   */

  it('does not return function when flagged and no expressions', function () {

    let interp = interpolate('static content only', true) ;
    expect(interp).to.be.undefined ;
  }) ;

  it('returns function when flagged and has expressions', function () {

    let interp = interpolate('has an {{expr}}', true) ;
    expect(interp).not.to.be.false ;
  }) ;


  /**
   * Optimizing Interpolation Watches With A Watch Delegate
   */

  it('uses a watch delegate', function () {

    let interp = interpolate('has an {{expr}}') ;
    expect(interp._watchDelegate).to.be.not.undefined ;
  }) ;

  it('correctly returns new and old value when watched', function () {

    let rootScope = kernel.get('$rootScope') ;

    let interp = interpolate('{{expr}}') ;
    let listenerSpy = sinon.spy() ;

    rootScope.$watch(interp, listenerSpy) ;
    rootScope.expr = 2015 ;

    rootScope.$apply() ;
    expect(listenerSpy.getCall(0).args[0]).to.be.equal('2015') ;
    expect(listenerSpy.getCall(0).args[1]).to.be.equal('2015') ;

    rootScope.expr++ ;
    rootScope.$apply() ;
    expect(listenerSpy.getCall(1).args[0]).to.be.equal('2016') ;
    expect(listenerSpy.getCall(1).args[1]).to.be.equal('2015') ;
  }) ;


  /**
   * Promise
   */
  it('return promise si une expression est une promise', function () {

    let interp = interpolate('{{a}} {{b}} {{c}}') ;

    expect(interp({a: new Promise(resolve => { resolve() ; }), b: 1, c: 'C'}))
      .to.be.instanceOf(Promise) ;
  }) ;

  it('résout toutes les promises', function (done) {

    let interp = interpolate('{{a}} {{b}} {{c}}') ;

    interp({
      a: new Promise(resolve => { resolve('A') ; }),
      b: new Promise(resolve => { resolve('B') ; }),
      c: 'C'
    }).then(expr => {
      expect(expr).to.be.equal('A B C') ;
      done() ;
    }) ;
  }) ;



  /**
   * Comments
   */
  it('do not parse comments', function () {

    let interp = interpolate('foobar {{!-- comment --}}') ;
    expect(interp({comment: 'test'})).to.be.equal('foobar ') ;
  }) ;

  it('but all are parsed well', function () {

    let interp = interpolate('{{start}}{{!-- Hello --}}{{end}}') ;
    expect(interp({start: 'foo', end: 'bar'})).to.be.equal('foobar') ;
  }) ;

  it('fonctionne avec plusieurs commentaires', function () {

    let interp = interpolate('{{!-- Hello--}} foobar {{!-- comment --}}') ;
    expect(interp({comment: 'test'})).to.be.equal(' foobar ') ;
  }) ;

  /**
   * Block expressions
   */
  it('parse block expressions', function () {

    let interp = interpolate('a {{#list foo}}Hello {{bar}}{{/list}} b') ;
    expect(interp({
      list: function (items, content) {
        let out = '<ul>' ;
        items.forEach(item => {
          out += '<li>' + content(item) + '</li>' ;
        }) ;
        return out + '</ul>' ;
      },
      foo: [{bar: 'Jésus'}, {bar: 'Marie'}, {bar: 'Joseph'}]
    })).to.be.equal('a <ul>'
      + '<li>Hello Jésus</li><li>Hello Marie</li><li>Hello Joseph</li>'
      + '</ul> b'
    ) ;
  }) ;

  it('passe correct arguments', function () {

    let context = {
      list: sinon.spy(),
      foo: [{bar: 'Jésus'}, {bar: 'Marie'}, {bar: 'Joseph'}]
    } ;
    let interp = interpolate('{{#list foo}}Hello {{bar}}{{/list}}') ;
    interp(context) ;

    expect(context.list.getCall(0).args[0]).to.be.equal(context.foo) ;
    expect(typeof context.list.getCall(0).args[1])
      .to.be.equal('function') ;
    expect(context.list.getCall(0).args[2]).to.be.equal(context) ;
  }) ;

  it('mix of block expressions et normal expressions', function () {

    let interp = interpolate(
      'a {{hello}} b {{#if !foo}}Hello {{bar}}{{/if}} c {{nb + 2}} d') ;
    expect(interp({hello: 'Coucou', foo: false, bar: 'Jésus', nb: 3}))
      .to.be.equal('a Coucou b Hello Jésus c 5 d') ;
  }) ;

  it('ok with multilines', function () {

    let interp = interpolate(
      'a {{hello}} b {{#if !foo}}Hel\nlo {{bar}}{{/if}} c {{nb + 2}} d') ;
    expect(interp({hello: 'Coucou', foo: false, bar: 'Jésus', nb: 3}))
      .to.be.equal('a Coucou b Hel\nlo Jésus c 5 d') ;
  }) ;

  it('multiples non imbriqued block', function () {

    let interp = interpolate(
      'a {{#list foo}}Hello {{bar}}{{/list}} b '
      + '{{#list bar}}Hello {{bar}}{{/list}} c'
    ) ;
    expect(interp({
      list: function (items, content) {
        let out = '<ul>' ;
        items.forEach(item => {
          out += '<li>' + content(item) + '</li>' ;
        }) ;
        return out + '</ul>' ;
      },
      foo: [{bar: 'Jésus'}, {bar: 'Marie'}, {bar: 'Joseph'}],
      bar: [{bar: 'Pierre'}, {bar: 'Paul'}, {bar: 'Jacque'}]
    })).to.be.equal('a <ul>'
      + '<li>Hello Jésus</li><li>Hello Marie</li><li>Hello Joseph</li>'
      + '</ul> b <ul>'
      + '<li>Hello Pierre</li><li>Hello Paul</li><li>Hello Jacque</li>'
      + '</ul> c'
    ) ;
  }) ;

  it('parse if block expressions', function () {

    let interp = interpolate('a {{#if !foo}}Hello {{bar}}{{/if}} b') ;
    expect(interp({foo: false, bar: 'Jésus'}))
      .to.be.equal('a Hello Jésus b') ;
    expect(interp({foo: true, bar: 'Juda'}))
      .to.be.equal('a  b') ;
  }) ;

  it('parse each block expressions', function () {

    let interp = interpolate(
      'a <ul>{{#each foo}}<li>Hello {{bar}}</li>{{/each}}</ul> b'
    ) ;
    expect(interp({foo: [{bar: 'Jésus'}, {bar: 'Marie'}, {bar: 'Joseph'}]}))
      .to.be.equal('a <ul>'
        + '<li>Hello Jésus</li><li>Hello Marie</li><li>Hello Joseph</li>'
        + '</ul> b'
      ) ;
  }) ;

  it('each block, item\'s context merged with global ', function () {

    let interp = interpolate(
      'a <ul>{{#each foo}}<li>{{hello}} {{bar}}</li>{{/each}}</ul> b'
    ) ;
    expect(interp({
      hello: 'Hi',
      foo: [{bar: 'Jésus'}, {bar: 'Marie', hello: 'Hey'}, {bar: 'Joseph'}]
    })).to.be.equal('a <ul>'
      + '<li>Hi Jésus</li><li>Hey Marie</li><li>Hi Joseph</li>'
      + '</ul> b'
    ) ;
  }) ;

  it('parse blocks imbriqued', function () {

    let interp = interpolate(
      'a {{#if test}}'
      + '<ul>{{#each foo}}<li>Hello {{bar}}</li>{{/each}}</ul> '
      + '{{/if}}b'
    ) ;
    expect(interp({
      foo: [{bar: 'Jésus'}, {bar: 'Marie'}, {bar: 'Joseph'}],
      test: true
    })).to.be.equal('a <ul>'
      + '<li>Hello Jésus</li><li>Hello Marie</li><li>Hello Joseph</li>'
      + '</ul> b'
    ) ;
    expect(interp({
      foo: [{bar: 'Jésus'}, {bar: 'Marie'}, {bar: 'Joseph'}],
      test: false
    })).to.be.equal('a b') ;
  }) ;

  it('parse blocks imbriqued, same block', function () {

    let interp = interpolate(
      'a {{#if foo}}'
      + '<p>Foo{{#if bar}}bar{{/if}}</p> '
      + '{{/if}}b'
    ) ;
    expect(interp({foo: true, bar: false})).to.be.equal('a <p>Foo</p> b') ;
    expect(interp({foo: true, bar: true})).to.be.equal('a <p>Foobar</p> b') ;
  }) ;

  it('parse blocks imbriqued, multiple same block', function () {

    let interp = interpolate(
      '{{#if a}}a{{#if b}}b{{#if c}}c{{/if}}{{/if}}{{/if}}'
    ) ;

    expect(interp({a: true, b: true, c: true})).to.be.equal('abc') ;
    expect(interp({a: true, b: true, c: false})).to.be.equal('ab') ;
    expect(interp({a: true, b: false, c: true})).to.be.equal('a') ;
    expect(interp({a: false, b: true, c: true})).to.be.equal('') ;
  }) ;

  it('parse blocks imbriqued, same and multiple block', function () {

    let interp = interpolate(
      'a {{#if foo}}<p>Foo{{#if bar}}bar{{/if}}</p>{{/if}} '
      + '<ul>'
      + '{{#each truc}}<li>Hello {{bar}}{{#if excl}}!{{/if}}</li>{{/each}}'
      + '</ul> '
      + '<p>{{#if a}}a{{#if b}}b{{#if c}}c{{/if}}{{/if}}{{/if}}</p>'
      + ' b'
    ) ;
    expect(interp({
      foo: true, bar: true,
      truc: [{bar: 'Marie', excl: false}, {bar: 'Joseph', excl: false}],
      a: true, b: true, c: true
    })).to.be.equal(
      'a <p>Foobar</p> '
      + '<ul><li>Hello Marie</li><li>Hello Joseph</li></ul> '
      + '<p>abc</p> b') ;
    expect(interp({
      foo: false, bar: true,
      truc: [{bar: 'Marie', excl: true}, {bar: 'Joseph', excl: false}],
      a: true, b: false, c: true
    })).to.be.equal(
      'a  '
      + '<ul><li>Hello Marie!</li><li>Hello Joseph</li></ul> '
      + '<p>a</p> b') ;
    expect(interp({
      foo: true, bar: true,
      truc: [],
      a: false, b: true, c: true
    })).to.be.equal(
      'a <p>Foobar</p> '
      + '<ul></ul> '
      + '<p></p> b') ;
  }) ;

  it('parse blocks imbriqued, multiple block', function () {

    let interp = interpolate(
      '<ul>'
      + '{{#each truc}}<li>Hello {{#if all}}'
      + '{{#each name}}{{ok}} {{/each}}'
      + '{{/if}}</li>{{/each}}'
      + '</ul>'
    ) ;
    expect(interp({
      truc: [
        {all: true, name: [{ok: 'Jésus'}, {ok: 'Marie'}, {ok: 'Joseph'}]},
        {all: false, name: [{ok: 'Jésus'}, {ok: 'Marie'}, {ok: 'Joseph'}]},
        {all: true, name: [{ok: 'J'}, {ok: 'M'}, {ok: 'J'}]}
      ]
    })).to.be.equal(
      '<ul>'
      + '<li>Hello Jésus Marie Joseph </li><li>Hello </li><li>Hello J M J </li>'
      + '</ul>'
    ) ;
  }) ;
}) ;

