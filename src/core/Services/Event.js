'use strict' ;

/**
 * src/core/Services/Event.js
 */

const Utils = require('../Utils') ;

/**
 * Canal d'évènement
 */
class Event {

  /**
   * Initialise le canal
   * @param {String} id Identifiant du canal
   */
  constructor (id) {

    this._id = id ;
    this._observers = {} ;
  }


  /**
   * Attache un observer, écrasera un précédent portant le même nom.
   * @param {String} id Identifiant, nécessaire pour désenregister l'observer
   * @param {function} callback Fonction de callback à exécuter
   */
  attach (id, callback) {

    this._observers[id] = callback ;
  }


  /**
   * Détache un observer
   * @param {String} id Identifiant de l'observer à détacher
   */
  detach (id) {

    this._observers[id] = null ;
  }


  /**
   * Notifie tous les observers
   * @param {*} data Données à transmettre
   */
  notify (data) {

    Utils.forEach(this._observers, observer => {
      if (Utils.isFunction(observer))
        observer(data) ;
    }) ;
  }
}

module.exports = Event ;

