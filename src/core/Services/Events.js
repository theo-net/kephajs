'use strict' ;

/**
 * src/core/Services/Events.js
 */

const Event = require('./Event') ;

/**
 * Gestion des évènements
 *
 * Il s'agit d'un canal publique : n'importe qui peut y déclencher un évènement
 * et écouter les autres évènements.
 *
 * Il s'agit du service donnant l'accès aux différents canaux d'évènements.
 */
class Events {

  constructor () {

    this._events = {} ;
  }


  /**
   * Enregistre un nouveau canal d'évènement. Si l'id est déjà défini, on
   * effacera l'accès au canal précédent.
   * @param {String} id Identifiant du canal
   */
  register (id) {

    this._events[id] = new Event(id) ;
  }


  /**
   * Attache un callback à un canal
   * @param {String} canal Identifiant du canal
   * @param {String} id Identifiant de l'observer canal
   * @param {Function} callback Le callback
   */
  attach (canal, id, callback) {

    if (this._events[canal])
      this._events[canal].attach(id, callback) ;
  }


  /**
   * Notifie un évènement
   * @param {String} canal Identifiant du canal à notifier
   * @param {*} data Données à transmettre
   */
  notify (canal, data) {

    if (this._events[canal])
      this._events[canal].notify(data) ;
  }


  /**
   * Indique si un canal est enregistré
   * @param {String} canal Identidiant du canal à tester
   * @returns {Boolean}
   */
  has (canal) {

    return Object.keys(this._events).indexOf(canal) > -1 ;
  }
}

module.exports = Events ;

