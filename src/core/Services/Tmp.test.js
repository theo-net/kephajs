'use strict' ;

/**
 * src/core/Services/Tmp.test.js
 */

let expect = require('chai').expect ;

const Tmp    = require('./Tmp'),
      Config = require('./Config') ;

describe('Tmp', function () {

  it('étend Config', function () {

    let tmp = new Tmp() ;
    expect(tmp).to.be.instanceOf(Config) ;
  }) ;
}) ;
