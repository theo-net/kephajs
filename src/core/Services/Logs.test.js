'use strict' ;

/**
 * src/core/Services/Logs.test.js
 */

const expect = require('chai').expect,
      sinon  = require('sinon') ;

const Logs  = require('./Logs') ;

// Cli ou browser ?
let logs = new Logs() ;
let cli = global.process !== undefined ;

describe('Logs', function () {

  let consoleLog, consoleError ;

  // Pour la couleur
  let green = (text) => {
    if (cli) return '\u001b[32m' + text + '\u001b[39m' ;
    else return text ;
  } ;
  let yellow = (text) => {
    if (cli) return '\u001b[33m' + text + '\u001b[39m' ;
    else return text ;
  } ;
  let blue = (text) => {
    if (cli) return '\u001b[34m' + text + '\u001b[39m' ;
    else return text ;
  } ;
  let cyan = (text) => {
    if (cli) return '\u001b[36m' + text + '\u001b[39m' ;
    else return text ;
  } ;
  let magenta = (text) => {
    if (cli) return '\u001b[35m' + text + '\u001b[39m' ;
    else return text ;
  } ;

  before(function () {

    consoleLog = sinon.stub(console, 'log') ;
    consoleError = sinon.stub(console, 'error') ;

    if (cli) {
      let Color = require('../../cli/Color') ;
      logs = new Logs() ;
      logs.kernel().container().set('$color', new Color()) ;
    }
  }) ;

  after(function () {

    console.log.restore() ;
    console.error.restore() ;
  }) ;

  beforeEach(function () {

    logs = new Logs() ;
  }) ;


  it('_getLogLevel retourne le bon log level', function () {

    expect(logs._getLogLevel()).to.be.equal('normal') ;

    logs.kernel().get('$config').set('logLevel', 'warn', true) ;
    expect(logs._getLogLevel()).to.be.equal('warn') ;
  }) ;

  it('log normal', function () {

    logs.kernel().get('$config').set('logLevel', 'normal', true) ;

    logs.log('msg info', 'info') ;
    expect(consoleLog.calledWith(blue('Info: msg info'))).to.be.true ;

    logs.log('msg success', 'success') ;
    expect(consoleLog.calledWith(green('msg success'))).to.be.true ;

    logs.log('msg warning', 'warning') ;
    expect(consoleLog.calledWith(yellow('Warning: msg warning'))).to.be.false ;

    logs.log('msg error', 'error') ;
    expect(consoleError.calledWith('msg error')).to.be.true ;

    logs.log('msg debug', 'debug') ;
    expect(consoleLog.calledWith(cyan('msg debug'))).to.be.false ;

    logs.log('msg data', 'data') ;
    expect(consoleLog.calledWith(magenta('msg data'))).to.be.false ;
  }) ;

  it('log warn', function () {

    logs.kernel().get('$config').set('logLevel', 'warn', true) ;

    logs.log('msg info', 'info') ;
    expect(consoleLog.calledWith(blue('Info: msg info'))).to.be.true ;

    logs.log('msg success', 'success') ;
    expect(consoleLog.calledWith(green('msg success'))).to.be.true ;

    logs.log('msg warning', 'warning') ;
    expect(consoleLog.calledWith(yellow('Warning: msg warning'))).to.be.true ;

    logs.log('msg error', 'error') ;
    expect(consoleError.calledWith('msg error')).to.be.true ;

    logs.log('msg debug', 'debug') ;
    expect(consoleLog.calledWith(cyan('msg debug'))).to.be.false ;

    logs.log('msg data', 'data') ;
    expect(consoleLog.calledWith(magenta('msg data'))).to.be.false ;
  }) ;

  it('log debug', function () {

    logs.kernel().get('$config').set('logLevel', 'debug', true) ;

    logs.log('msg info', 'info') ;
    expect(consoleLog.calledWith(blue('Info: msg info'))).to.be.true ;

    logs.log('msg success', 'success') ;
    expect(consoleLog.calledWith(green('msg success'))).to.be.true ;

    logs.log('msg warning', 'warning') ;
    expect(consoleLog.calledWith(yellow('Warning: msg warning'))).to.be.true ;

    logs.log('msg error', 'error') ;
    expect(consoleError.calledWith('msg error')).to.be.true ;

    logs.log('msg debug', 'debug') ;
    expect(consoleLog.calledWith(cyan('msg debug'))).to.be.true ;

    logs.log('msg data', 'data') ;
    expect(consoleLog.calledWith(magenta('msg data'))).to.be.true ;
  }) ;
}) ;

