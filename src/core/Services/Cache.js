'use strict' ;

/**
 * src/core/Services/Cache.js
 */

const Config = require('./Config'),
      Utils  = require('../Utils') ;

/**
 * Service gérant le cache de l'application
 * Chaque élément peut être associé à une date d'expiration à partir de
 * laquelle l'élément sera effacé.
 */
class Cache extends Config {

  constructor () {

    super() ;

    this._enabled = true ;
    this._exp = {} ;
  }


  /**
   * Indique si le cache est activé ou non
   * @returns {Boolean}
   */
  isEnabled () {

    return this._enabled ;
  }


  /**
   * Active ou désactive le cache
   * @param {Boolean} enabled `true` pour activer, `false` sinon
   */
  enable (enabled) {

    this._enabled = enabled ? true : false ;
  }


  /**
   * Définit une donnée à mettre en cache. Un valeur précédente sera
   * toujours écrasée.
   * @param {String} id Identifiant
   * @param {*} value Valeur
   * @param {Integer|Null} [exp] Expiration (en seconde depuis maintenant),
   *    mettre `null` (valeur par défaut) pour aucune expiration.
   */
  set (id, value, exp = null) {

    this._vars[id] = value ;
    this._exp[id] = exp !== null ? Date.now() + exp * 1000 : null ;
  }


  /**
   * Retourne la valeur contenue dans le cache. S'il est absent ou expiré, on
   * retournera la valeur par défaut qui sera mis en cache.
   *
   * @param {String} id Identifiant
   * @param {*} [dflt=null] Valeur par défaut
   * @param {Integer|Null} [exp] Expiration (en seconde depuis maintenant),
   *    mettre `null` (valeur par défaut) pour aucune expiration.
   * @returns {*}
   */
  get (id, dflt = null, exp = null) {

    if (/.+\.\*$/.test(id)) {

      let root    = id.substr(0, id.length - 1),
          results = {} ;

      Utils.forEach(this._vars, (value, id) => {

        if (id.substr(0, root.length) == root && this.has(id))
          results[id.substr(root.length)] = super.get(id) ;
      }) ;

      if (Utils.isObject(dflt))
        results = Utils.extend({}, dflt, results) ;

      return results ;
    }
    else {
      if (this.has(id))
        return super.get(id, dflt, false) ;
      else if (dflt !== null) {
        this.set(id, Utils.isFunction(dflt) ? dflt() : dflt, exp) ;
        return super.get(id, null, false) ;
      }
      else
        return null ;
    }
  }


  /**
   * Indique si un élément est en cache ou non. Retournera également
   * `false` si l'élément à expiré.
   * @param {String} id Identifiant de l'élément à tester
   * @returns {Boolean}
   */
  has (id) {

    let has = false ;

    if (this._enabled) {

      if (super.has(id))
        has = this._exp[id] === null ? true : this._exp[id] > Date.now() ;

      if (!has)
        this.delete(id) ;
    }

    return has ;
  }


  /**
   * Supprime un élément
   * @param {String} id Identifiant de l'élément à supprimer
   */
  delete (id) {

    super.delete(id, elmt => {
      delete this._vars[elmt] ;
      delete this._exp[elmt] ;
    }) ;
  }
}

module.exports = Cache ;

