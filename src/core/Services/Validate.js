'use strict' ;

/**
 * src/core/Services/Validate.js
 */

const ValidateValidator = require('../Validate/Validate'),
      BooleanValidator  = require('../Validate/Boolean'),
      FloatValidator    = require('../Validate/Float'),
      FunctionValidator = require('../Validate/Function'),
      IntegerValidator  = require('../Validate/Integer'),
      ListValidator     = require('../Validate/List'),
      NotEmptyValidator = require('../Validate/NotEmpty'),
      NullValidator     = require('../Validate/Null'),
      RegExpValidator   = require('../Validate/RegExp'),
      StringValidator   = require('../Validate/String'),
      MailValidator     = require('../Validate/Mail'),
      BaseError         = require('../Error/BaseError') ;

/**
 * Créé un service permettant de générer des objets validant des données.
 */
class Validate {

  constructor () {

    this._validators = {
      boolean:  {object: new BooleanValidator()},
      float:    {class: FloatValidator},
      function: {class: FunctionValidator},
      integer:  {class: IntegerValidator},
      list:     {class: ListValidator},
      notEmpty: {class: NotEmptyValidator},
      'null':   {object: new NullValidator()},
      regExp:   {class: RegExpValidator},
      string:   {class: StringValidator},
      mail:     {class: MailValidator}
    } ;
  }


  /**
   * Construit un nouveau validateur
   *
   * @param {String|Function|RegExp} validator Voir ValidateValidator.add()
   * @param {Object|String} args Voir ValidateValidator.add()
   * @returns {Validator}
   */
  make (validator, args = {}) {

    let val = new ValidateValidator({service: this}) ;

    validator = validator ? validator : 'null' ;

    return val.add(validator, args) ;
  }


  /**
   * Retourne un nouveau validateur à partir de son nom
   * @param {String} name Nom du validateur
   * @param {Object} [args] Arguments
   * @returns {Validator}
   */
  getWithName (name, args = {}) {

    if (!this._validators.hasOwnProperty(name)) {
      throw new BaseError('Le service `validate` ne possède pas de validateur '
        + name + ' !') ;
    }

    if (this._validators[name].hasOwnProperty('object'))
      return this._validators[name].object ;

    return new this._validators[name].class(args) ;
  }


  /**
   * Définit un nouveau validateur
   *
   *   validator = {
   *     class: require('..'),   // créer à partir d'une class
   *     object: new *Validator  // on s'occupe nous-même de la création
   *   }
   *
   * @param {String} name Nom du validateur
   * @param {Object} validator Description du validateur
   */
  setValidator (name, validator) {

    this._validators[name] = validator ;
  }
}

module.exports = Validate ;

