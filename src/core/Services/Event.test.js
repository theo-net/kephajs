'use strict' ;

/**
 * src/core/Services/Event.test.js
 */


const expect = require('chai').expect,
      sinon  = require('sinon') ;
const Event = require('./Event') ;

describe('core/Services/Event', function () {

  it('Sauve son identifiant', function () {

    let event = new Event('foobar') ;
    expect(event._id).to.be.equal('foobar') ;
  }) ;

  it('Sait attacher un observer', function () {

    let event = new Event('foobar') ;
    let callback = function () {} ;

    event.attach('test', callback) ;
    expect(event._observers.test).to.be.equal(callback) ;
  }) ;

  it('Sait détacher un observer', function () {

    let event = new Event('foobar') ;
    let callback = function () {} ;

    event.attach('test', callback) ;
    event.detach('test') ;
    expect(event._observers.test).to.be.equal(null) ;
  }) ;

  it('Notifie tous les observers et transmet les données', function () {

    let event = new Event('foobar') ;
    let spy1 = sinon.spy() ;
    let spy2 = sinon.spy() ;
    let spy3 = sinon.spy() ;
    let data = {a: 1, b: 2} ;

    event.attach('spy1', spy1) ;
    event.attach('spy2', spy2) ;
    event.attach('spy3', spy3) ;
    event.detach('spy3') ;

    event.notify(data) ;

    expect(spy1.calledOnce).to.be.true ;
    expect(spy1.calledWith(data)).to.be.true ;
    expect(spy2.calledOnce).to.be.true ;
    expect(spy2.calledWith(data)).to.be.true ;
    expect(spy3.calledOnce).to.be.false ;
  }) ;
}) ;

