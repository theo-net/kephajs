'use strict' ;

/**
 * src/core/Services/Filter.js
 */


/**
 * Gère les filtres disponibles dans notre application
 */
class Filter {

  constructor () {

    this._filters = {} ;

    // On enregistre le filtre filter
    this.register('filter', require('./filterFilter')) ;
  }


  /**
   * Indique si un filtre est enregistré
   * @param {String} name Nom du filtre
   * @returns {Boolean}
   */
  has (name) {

    return Object.keys(this._filters).indexOf(name) > -1 ;
  }


  /**
   * Enregistre le filtre.
   * ATTENTION : `filter` doit être de la forme `function () {}` et non de la
   *             forme `() => {}` pour que le parser puisse fonctionner.
   * @param {String} name Nom du filtre
   * @param {Function} filter Filtre à enregistrer
   */
  register (name, filter) {

    this._filters[name] = filter ;
  }


  /**
   * Retourne un filtre
   * @param {String} name Nom du filtre
   * @returns {Function}
   */
  get (name) {

    return this._filters[name] ;
  }
}

module.exports = Filter ;

