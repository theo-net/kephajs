'use strict' ;

/**
 * src/core/Services/filter_filter.test.js
 */

const expect = require('chai').expect ;

const createParse  = require('./parse'),
      Kernel       = require('../Kernel') ;
const kernel = new Kernel() ;

let parse = createParse(kernel) ;

describe('Filter filter', function () {

  it('can filter an array with a predicate function', function () {

    let fn = parse('[1, 2, 3, 4] | filter:isOdd') ;
    let scope = {
      isOdd: function (n) {
        return n % 2 !== 0 ;
      }
    } ;
    expect(fn(scope)).to.be.deep.equal([1, 3]) ; // buddy ignore:line
  }) ;

  it('can filter an array of strings with a string', function () {

    let fn = parse('arr | filter:"a"') ;
    expect(fn({arr: ['a', 'b', 'a']})).to.be.deep.equal(['a', 'a']) ;
  }) ;

  it('filters an array of strings with substring matching', function () {

    let fn = parse('arr | filter:"o"') ;
    expect(fn({arr: ['quick', 'brown', 'fox']}))
      .to.be.deep.equal(['brown', 'fox']) ;
  }) ;

  it('filters an array of strings ignoring case', function () {

    let fn = parse('arr | filter:"o"') ;
    expect(fn({arr: ['quick', 'BROWN', 'fox']}))
      .to.be.deep.equal(['BROWN', 'fox']) ;
  }) ;

  it('filters an array of objects where any value matches', function () {

    let fn = parse('arr | filter:"o"') ;
    expect(fn({arr: [
      {firstName: 'John', lastName: 'Brown'},
      {firstName: 'Jane', lastName: 'Fox'},
      {firstName: 'Mary', lastName: 'Quick'}
    ]})).to.be.deep.equal([
      {firstName: 'John', lastName: 'Brown'},
      {firstName: 'Jane', lastName: 'Fox'}
    ]) ;
  }) ;

  it('filters an array of objects where a nested value matches', function () {

    let fn = parse('arr | filter:"o"') ;
    expect(fn({arr: [
      {name: {first: 'John', last: 'Brown'}},
      {name: {first: 'Jane', last: 'Fox'}},
      {name: {first: 'Mary', last: 'Quick'}}
    ]})).to.be.deep.equal([
      {name: {first: 'John', last: 'Brown'}},
      {name: {first: 'Jane', last: 'Fox'}}
    ]) ;
  }) ;

  it('filters an array of arrays where a nested value matches', function () {

    let fn = parse('arr | filter:"o"') ;
    expect(fn({arr: [
      [{name: 'John'}, {name: 'Mary'}],
      [{name: 'Jane'}]
    ]})).to.be.deep.equal([
      [{name: 'John'}, {name: 'Mary'}]
    ]) ;
  }) ;

  it('filers with a number', function () {

    let fn = parse('arr | filter:42') ;
    expect(fn({arr: [
      {name: 'Mary', age: 42},
      {name: 'John', age: 43},
      {name: 'Jane', age: 44}
    ]})).to.be.deep.equal([
      {name: 'Mary', age: 42}
    ]) ;
  }) ;

  it('filers with a boolean value', function () {

    let fn = parse('arr | filter:true') ;
    expect(fn({arr: [
      {name: 'Mary', admin: true},
      {name: 'John', admin: true},
      {name: 'Jane', admin: false}
    ]})).to.be.deep.equal([
      {name: 'Mary', admin: true},
      {name: 'John', admin: true}
    ]) ;
  }) ;

  it('filters with a substring numeric value', function () {

    let fn = parse('arr | filter:42') ;
    expect(fn({arr: ['contains 42']})).to.be.deep.equal(['contains 42']) ;
  }) ;

  it('filters matching null', function () {

    let fn = parse('arr | filter:null') ;
    expect(fn({arr: [null, 'not null']})).to.be.deep.equal([null]) ;
  }) ;

  it('does not match null value with the string null', function () {

    let fn = parse('arr | filter:"null"') ;
    expect(fn({arr: [null, 'not null']})).to.be.deep.equal(['not null']) ;
  }) ;

  it('does not match undefined values', function () {

    let fn = parse('arr | filter:"undefined"') ;
    expect(fn({arr: [undefined, 'undefined']}))
      .to.be.deep.equal(['undefined']) ;
  }) ;

  it('allows negating string filter', function () {

    let fn = parse('arr | filter:"!o"') ;
    expect(fn({arr: ['quick', 'brown', 'fox']})).to.be.deep.equal(['quick']) ;
  }) ;

  it('filers with an object', function () {

    let fn = parse('arr | filter:{name: "o"}') ;
    expect(fn({arr: [
      {name: 'Joe',  role: 'admin'},
      {name: 'Jane', role: 'moderator'}
    ]})).to.be.deep.equal([
      {name: 'Joe',  role: 'admin'}
    ]) ;
  }) ;

  it('must match all criteria in an object', function () {

    let fn = parse('arr | filter:{name: "o", role: "m"}') ;
    expect(fn({arr: [
      {name: 'Joe',  role: 'admin'},
      {name: 'Jane', role: 'moderator'}
    ]})).to.be.deep.equal([
      {name: 'Joe',  role: 'admin'}
    ]) ;
  }) ;

  it('matches everything when filtered with an empty object', function () {

    let fn = parse('arr | filter:{}') ;
    expect(fn({arr: [
      {name: 'Joe',  role: 'admin'},
      {name: 'Jane', role: 'moderator'}
    ]})).to.be.deep.equal([
      {name: 'Joe',  role: 'admin'},
      {name: 'Jane', role: 'moderator'}
    ]) ;
  }) ;

  it('filters with a nested object', function () {

    let fn = parse('arr | filter:{name: {first: "o"}}') ;
    expect(fn({arr: [
      {name: {first: 'Joe'},  role: 'admin'},
      {name: {first: 'Jane'}, role: 'moderator'}
    ]})).to.be.deep.equal([
      {name: {first: 'Joe'},  role: 'admin'}
    ]) ;
  }) ;

  it('allows negation when filtering with an object', function () {

    let fn = parse('arr | filter:{name: {first: "!o"}}') ;
    expect(fn({arr: [
      {name: {first: 'Joe'},  role: 'admin'},
      {name: {first: 'Jane'}, role: 'moderator'}
    ]})).to.be.deep.equal([
      {name: {first: 'Jane'}, role: 'moderator'}
    ]) ;
  }) ;

  it('ignores undefined values in expectation object', function () {

    let fn = parse('arr | filter:{name: thisIsUndefined}') ;
    expect(fn({arr: [
      {name: 'Joe',  role: 'admin'},
      {name: 'Jane', role: 'moderator'}
    ]})).to.be.deep.equal([
      {name: 'Joe',  role: 'admin'},
      {name: 'Jane', role: 'moderator'}
    ]) ;
  }) ;

  it('filters with a nested object in array', function () {

    let fn = parse('arr | filter:{users: {name: {first: "o"}}}') ;
    expect(fn({arr: [
      {users: [
        {name: {first: 'Joe'},  role: 'admin'},
        {name: {first: 'Jane'}, role: 'moderator'}
      ]},
      {users: [{name: {first: 'Mary'},  role: 'admin'}]}
    ]})).to.be.deep.equal([
      {users: [
        {name: {first: 'Joe'},  role: 'admin'},
        {name: {first: 'Jane'}, role: 'moderator'}
      ]}
    ]) ;
  }) ;

  it('filters with nested objects on the same level only', function () {

    let items = [
      {user: 'Bob'},
      {user: {name: 'Bob'}},
      {user: {name: {first: 'Bob', last: 'Fox'}}}
    ] ;
    let fn = parse('arr | filter:{user: {name: "Bob"}}') ;
    expect(fn({arr: items})).to.be.deep.equal([{user: {name: 'Bob'}}]) ;
  }) ;

  it('filters with a wildcard property', function () {

    let fn = parse('arr | filter:{$: "o"}') ;
    expect(fn({arr: [
      {name: 'Joe',  role: 'admin'},
      {name: 'Jane', role: 'moderator'},
      {name: 'Mary', role: 'admin'}
    ]})).to.be.deep.equal([
      {name: 'Joe',  role: 'admin'},
      {name: 'Jane', role: 'moderator'}
    ]) ;
  }) ;

  it('filters nested objects with a wildcard property', function () {

    let fn = parse('arr | filter:{$: "o"}') ;
    expect(fn({arr: [
      {name: {first: 'Joe'},  role: 'admin'},
      {name: {first: 'Jane'}, role: 'moderator'},
      {name: {first: 'Mary'}, role: 'admin'}
    ]})).to.be.deep.equal([
      {name: {first: 'Joe'},  role: 'admin'},
      {name: {first: 'Jane'}, role: 'moderator'}
    ]) ;
  }) ;

  it('filters wildcard properties scoped to parent', function () {

    let fn = parse('arr | filter:{name: {$: "o"}}') ;
    expect(fn({arr: [
      {name: {first: 'Joe',  last: 'Fox'},   role: 'admin'},
      {name: {first: 'Jane', last: 'Quick'}, role: 'moderator'},
      {name: {first: 'Mary', last: 'Brown'}, role: 'admin'}
    ]})).to.be.deep.equal([
      {name: {first: 'Joe',  last: 'Fox'},   role: 'admin'},
      {name: {first: 'Mary', last: 'Brown'}, role: 'admin'}
    ]) ;
  }) ;

  it('filters primitives with a wildcard property', function () {

    let fn = parse('arr | filter:{$: "o"}') ;
    expect(fn({arr: ['Joe', 'Jane', 'Marry']})).to.be.deep.equal(['Joe']) ;
  }) ;

  it('filters with a nested wildcard property', function () {

    let fn = parse('arr | filter:{$: {$: "o"}}') ;
    expect(fn({arr: [
      {name: {first: 'Joe'},  role: 'admin'},
      {name: {first: 'Jane'}, role: 'moderator'},
      {name: {first: 'Mary'}, role: 'admin'}
    ]})).to.be.deep.equal([
      {name: {first: 'Joe'},  role: 'admin'},
    ]) ;
  }) ;

  it('allows using a custom comparator', function () {

    let fn = parse('arr | filter:{$: "o"}:myComparator') ;
    expect(fn({
      arr: ['o', 'oo', 'ao', 'aa'],
      myComparator: function (left, right) {
        return left === right ;
      }
    })).to.be.deep.equal(['o']) ;
  }) ;

  it('allows using an equality comparator', function () {

    let fn = parse('arr | filter:{name: "Jo"}:true') ;
    expect(fn({arr: [
      {name: 'Jo'},
      {name: 'Joe'}
    ]})).to.be.deep.equal([
      {name: 'Jo'}
    ]) ;
  }) ;


}) ;


