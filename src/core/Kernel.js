'use strict' ;

/**
 * src/core/Kernel.js
 */

let ServiceContainer = require('./Services/ServiceContainer'),
    Config           = require('./Services/Config'),
    Filter           = require('./Services/Filter'),
    Validate         = require('./Services/Validate'),
    Scope            = require('./Services/Scope'),
    Events           = require('./Services/Events'),
    Cache            = require('./Services/Cache'),
    Tmp              = require('./Services/Tmp') ;

let _instance = null ;

/**
 * Cœur de l'application
 * Initialise les services et l'environnement. Il s'agit d'un singleton
 */
class Kernel {

  /**
   * Initialisation du Kernel
   *
   * @param {Boolean} [force = false] Force la création d'une nouvelle instance
   * @returns {Kernel} L'instance
   */
  constructor (force = false) {

    // On n'initialise qu'une fois le Kernel
    if (!force && _instance !== null)
      return _instance ;

    _instance = this ;

    // Initialisation des services de base
    this._container = new ServiceContainer(this) ;
    this._container.set('$config', new Config()) ;
    this._container.set('$validate', new Validate()) ;
    this._container.set('$parser', require('./Services/parse')) ;
    this._container.set('$filter', new Filter()) ;
    this._container.set('$interpolate', require('./Services/interpolate')) ;
    this._container.set('$rootScope', new Scope()) ;
    this._container.set('$events', new Events()) ;
    this._container.set('$cache', new Cache()) ;
    this._container.set('$tmp', new Tmp()) ;

    // Initialisation config
    this.get('$config').set('logLevel', 'normal') ;

    return _instance ;
  }


  /**
   * Retourne le ServiceContainer
   * @returns {ServiceContainer}
   */
  container () {

    return this._container ;
  }


  /**
   * Retourne le service $event
   * @returns {Events}
   */
  events () {

    return this.get('$events') ;
  }
  /**
   * Alias pour `this.container().get()`
   *
   * @param {String} name Nom du service à récupérer
   * @returns {Object}
   */
  get (name) {

    return this._container.get(name) ;
  }
}

module.exports = Kernel ;

