'use strict' ;

/**
 * src/cli/Error/InvalidArgumentValue.js
 */

const BaseError = require('./BaseError') ;

class InvalidArgumentValueError extends BaseError {

  constructor (arg, value, command, cli) {

    let msg = 'Invalid value \'' + value + '\' for argument '
            + cli.color().italic(arg.name()) + '.' ;
    arg._validator.getOccurredErrors().forEach(err => {
      msg += '\n  - ' + err ;
    }) ;

    super(msg, {arg, command, value}, cli) ;
  }
}

module.exports = InvalidArgumentValueError ;

