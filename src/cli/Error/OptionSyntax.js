'use strict' ;

/**
 * src/cli/Error/OptionSyntax.js
 */

const BaseError = require('./BaseError') ;

class OptionSyntaxError extends BaseError {

  constructor (synopsis, cli) {

    let msg = 'Syntax error in option synopsis: ' + synopsis ;

    super(msg, {synopsis}, cli) ;
  }
}

module.exports = OptionSyntaxError ;

