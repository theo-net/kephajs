'use strict' ;

/**
 * src/cli/error/UnknownOption.js
 */

const BaseError = require('./BaseError') ;
const getSuggestions    = require('../Suggest').getSuggestions,
      getBoldDiffString = require('../Suggest').getBoldDiffString ;

class UnknownOptionError extends BaseError {

  constructor (option, command, cli) {

    const suggestions = getSuggestions(option, command._getLongOptions()) ;

    let msg = 'Unknown option '
            + cli.color().italic(BaseError.getDashedOption(option)) + '.' ;

    if (suggestions.length) {
      msg += ' Did you mean ' + suggestions.map(
          s => '--' + getBoldDiffString(option, s)
      ).join(' or maybe ') + ' ?' ;
    }

    super(msg, {option, command}, cli) ;
  }
}

module.exports = UnknownOptionError ;

