'use strict' ;

/**
 * src/cli/Error/InvalidOptionValueError.ks
 */

const BaseError = require('./BaseError') ;

class InvalidOptionValueError extends BaseError {

  constructor (option, value, command, cli) {

    let msg = 'Invalid value \'' + value + '\' for option '
            + cli.color().italic(BaseError.getDashedOption(option.name()))
            + '.' ;
    option._validator.getOccurredErrors().forEach(err => {
      msg += '\n  - ' + err ;
    }) ;

    super(msg, {option, command, value}, cli) ;
  }
}

module.exports = InvalidOptionValueError ;

