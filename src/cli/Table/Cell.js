'use strict' ;

/**
 * src/cli/Table/Cell.js
 */

/*
 Inspiré par cli-table2
 */

const Utils        = require('../../core/Utils'),
      Color        = require('../Color'),
      KernelAccess = require('../../core/KernelAccess'),
      Table        = require('./Table'),
      ColSpanCell  = require('./ColSpanCell'),
      RowSpanCell  = require('./RowSpanCell') ;

const kAccess = new KernelAccess() ;
const color = kAccess.kernel().get('$application').color() ;

/**
 * Classe représentant la cellule d'un tableau à afficher
 */
class Cell {

  /**
   * Construit une nouvelle cellule, le paramètre option permet de la
   * configurer :
   *   - `rowSpan` Espacement ligne
   *   - `colSpan` Espacement colonne
   *   - `content` Contenu (converti tout en `string`)
   *
   * Si autre chose qu'un objet est transmis, alors il s'agira du contenu
   *
   * @param {*} options Options à transmettre
   * @constructor
   */
  constructor (options) {

    // C'est le contenu qui a été transmis (converti en string)
    if (Utils.isString(options) || Utils.isNumber(options) ||
        Utils.isBoolean(options))
      options = {content: '' + options} ;

    // Aucune options de transmise
    options = options || {} ;

    this._options = options ;

    // On récupère le contenu
    let content = options.content ;
    if (Utils.isString(content) || Utils.isNumber(content) ||
        Utils.isBoolean(content))
      this.content = String(content) ;
    else if (!content)
      this.content = '' ;
    else {
      throw new Error('Content needs to be a primitive, got: '
          + (typeof content)) ;
    }

    // On définit colSpan et rowSpan
    this.colSpan = options.colSpan || 1 ;
    this.rowSpan = options.rowSpan || 1 ;

    // Propriété x et y (fixées par le layout manager)
    this.x = this.y = null ;

  }


  /**
   * Transmet les options à notre cellule à partir du tableau
   *
   * @param {Object} tableOptions Options du tableau
   * @param {Array} cells Cellules du tableau
   */
  mergeTableOptions (tableOptions, cells) {

    this._cells = cells ;

    // Caractères de séparation
    let optionsChars = this._options.chars || {} ;
    let tableChars = tableOptions.chars ;
    this.chars = {} ;
    ['top', 'top-mid', 'top-left', 'top-right',
      'bottom', 'bottom-mid', 'bottom-left', 'bottom-right',
      'left', 'left-mid', 'mid', 'mid-mid', 'right', 'right-mid', 'middle'
    ].forEach(name => {
      let nameCamel = Utils.camelCase(name) ;
      this.chars[nameCamel] = optionsChars[nameCamel] || optionsChars[name] ||
        tableChars[nameCamel] || tableChars[name] ;
    }) ;

    // Truncate
    this.truncate = this._options.truncate || tableOptions.truncate ;

    // Stylisation
    this._options.style = this._options.style || {} ;
    let tableStyle = tableOptions.style ;
    this.paddingLeft =
      this._options.style['paddingLeft'] ||
      this._options.style['padding-left'] ||
      tableStyle['paddingLeft'] || tableStyle['padding-left'] ;
    this.paddingRight =
      this._options.style['paddingRight'] ||
      this._options.style['padding-right'] ||
      tableStyle['paddingRight'] || tableStyle['padding-right'] ;
    this.head = this._options.style.head || tableStyle.head ;
    this.border = this._options.style.border || tableStyle.border ;

    // width et height
    let fixedWidth = tableOptions.colWidths[this.x] ;
    if (tableOptions.wordWrap && fixedWidth) {
      fixedWidth -= this.paddingLeft + this.paddingRight ;
      this.lines = Color.colorizeLines(
          Table.wordWrap(fixedWidth, this.content)) ;
    }
    else
      this.lines = Color.colorizeLines(this.content.split('\n')) ;

    this.desiredWidth = Table.strlen(this.content)
                      + this.paddingLeft + this.paddingRight ;
    this.desiredHeight = this.lines.length ;
  }


  /**
   * Initialise la structure des données de notre cellule.
   *
   * @param {Object} tableOptions Un objet complètement renseigné
   *        En plus des valeurs standards par défaut, il doit avoir les éléments
   *        `colWidths` et `rowWidths` de renseignés. Ces éléments donnent le
   *        nombre de colonnes et de lignes du tableau (doivent contenir un
   *        `Number`).
   */
  init (tableOptions) {

    this.widths = tableOptions.colWidths
                              .slice(this.x, this.x + this.colSpan) ;
    this.heights = tableOptions.rowHeights
                               .slice(this.y, this.y + this.rowSpan) ;

    this.width = this.widths.reduce(
        (a, b) => { return a + b + 1 ; },
        this.widths.length ? -1 : 0) ;
    this.height = this.heights.reduce(
        (a, b) => { return a + b + 1 ; },
        this.heights.length ? -1 : 0) ;

    this.hAlign = this._options.hAlign || tableOptions.colAligns[this.x] ;
    this.vAlign = this._options.vAlign || tableOptions.rowAligns[this.y] ;

    this.drawRight = this.x + this.colSpan == tableOptions.colWidths.length ;
  }


  /**
   * Dessine la ligne demandée de la cellule
   *
   * @param {*} lineNum Peut être `top`n `bottom` ou un numéro de ligne
   * @param {Number} [spanningCell] Doit être un nombre si doit être appelé
   *        depuis `RowSpanCell` et doit représenter le nombre de ligne
   *        d'espace avant le contenu. Sinon est `undefined`.
   * @returns {String} LA représentation de la ligne
   */
  draw (lineNum, spanningCell) {

    // Top et Bottom
    if (lineNum == 'top')
      return this.drawTop(this.drawRight) ;
    if (lineNum == 'bottom')
      return this.drawBottom(this.drawRight) ;

    // Longueur du padding
    let padLen = Math.max(this.height - this.lines.length, 0) ;

    // Padding appliqué
    let padTop ;
    switch (this.vAlign) {
      case 'center':
        padTop = Math.ceil(padLen / 2) ;
        break ;
      case 'bottom':
        padTop = padLen ;
        break ;
      default:
        padTop = 0 ;
    }

    // Ligne vide
    if ((lineNum < padTop) || (lineNum >= (padTop + this.lines.length)))
      return this.drawEmpty(this.drawRight, spanningCell) ;

    // Trunc
    let forceTruncation = (this.lines.length > this.height) &&
                          (lineNum + 1 >= this.height) ;

    return this.drawLine(lineNum - padTop, this.drawRight,
                         forceTruncation, spanningCell) ;
  }


  /**
   * Rendu de la bordure supérieure de la cellule
   *
   * @param {Boolean} drawRight `true` si la méthode doit rendre la bordure
   *                            droite de la cellule
   * @return {String}
   */
  drawTop (drawRight) {

    let content = [] ;

    if (this.cells) {
      this.widths.forEach((width, index) => {

        content.push(this._topLeftChar(index)) ;
        content.push(
            this.chars[this.y == 0 ? 'top' : 'mid'].repeat(width)
        ) ;
      }) ;
    }
    else {
      content.push(this._topLeftChar(0)) ;
      content.push(
          this.chars[this.y == 0 ? 'top' : 'mid'].repeat(this.width)
      ) ;
    }

    // On dessine la bordure de droite
    if (drawRight)
      content.push(this.chars[this.y == 0 ? 'topRight' : 'rightMid']) ;

    return this._wrapWithStyleColor('border', content.join('')) ;
  }


  /**
   * Dessine la bordure inférieur de la cellule
   * @param {Booelan} drawRight `true` si on doit dessiner la bordure droite de
   *                            la cellule
   * @return {String}
   */
  drawBottom (drawRight) {

    let left = this.chars[this.x == 0 ? 'bottomLeft' : 'bottomMid'] ;
    let content = this.chars.bottom.repeat(this.width) ;
    let right = drawRight ? this.chars['bottomRight'] : '' ;

    return this._wrapWithStyleColor('border', left + content + right) ;
  }


  /**
   * Dessine une ligne vide pour la cellule. Utilisé pour le padding-top/bottom.
   *
   * @param {Boolean} drawRight `true` si on doit dessiner la bordure droite de
   *                            la cellule
   * @param {Number} spanningCell Espacemment
   * @returns {String}
   */
  drawEmpty (drawRight, spanningCell) {

    let left = this.chars[this.x == 0 ? 'left' : 'middle'] ;

    if (this.x && spanningCell && this.cells) {

      let cellLeft = this.cells[this.y + spanningCell][this.x - 1] ;
      while (cellLeft instanceof ColSpanCell)
        cellLeft = this.cells[cellLeft.y][cellLeft.x - 1] ;

      if (!(cellLeft instanceof RowSpanCell))
        left = this.chars['rightMid'] ;
    }

    let right = drawRight ? this.chars['right'] : '' ;
    let content = ' '.repeat(this.width) ;

    return this._stylizeLine(left, content, right) ;
  }


  /**
   * Rendu d'une ligne de texte
   *
   * @param {Number} lineNum Numéro de la ligne du texte à rendre. Ne correspond
   *                         pas forcément au contenu, peut être le padding
   * @param {Boolean} drawRight `true` pour le rendu de la bordure droite
   * @param {Boolean} forceTruncationSymbol `true` si on doit insérer le symbole
   *        de troncature si le texte a été tronqué verticalement. Sinon, ne le
   *        sera que lors d'une toncature horizontale
   * @param {Number} spanningCell Padding
   * @returns {String}
   */
  drawLine (lineNum, drawRight, forceTruncationSymbol, spanningCell) {

    let left = this.chars[this.x == 0 ? 'left' : 'middle'] ;

    if (this.x && spanningCell && this.cells) {

      let cellLeft = this.cells[this.y + spanningCell][this.x - 1] ;
      while(cellLeft instanceof ColSpanCell)
        cellLeft = this.cells[cellLeft.y][cellLeft.x - 1] ;

      if (!(cellLeft instanceof RowSpanCell))
        left = this.chars['rightMid'] ;
    }

    let leftPadding = ' '.repeat(this.paddingLeft) ;
    let right = drawRight ? this.chars['right'] : '' ;
    let rightPadding = ' '.repeat(this.paddingRight) ;
    let line = this.lines[lineNum] ;
    let len = this.width - (this.paddingLeft + this.paddingRight) ;
    if (forceTruncationSymbol) line += this.truncate || '…' ;

    let content = Table.truncate(line, len, this.truncate) ;
    let contentLength = Table.strlen(content) ;

    // On ajoute le padding
    if (len + 1 >= contentLength) {

      let padlen = len - contentLength ;
      switch (this.hAlign) {
        case 'right':
          content = ' '.repeat(padlen) + content ;
          break ;
        case 'center':
          content = ' '.repeat(padlen - Math.ceil(padlen / 2)) + content
                  + ' '.repeat(Math.ceil(padlen / 2)) ;
          break ;
        default:
          content = content + ' '.repeat(padlen) ;
          break ;
      }
    }

    content = leftPadding + content + rightPadding ;

    return this._stylizeLine(left, content, right) ;
  }


  /**
   * Renvoit le caractère correspondant à l'angle supérieur gauche de la
   * cellule.
   *
   * @param {Number} offset ?
   * @returns {String}
   * @private
   */
  _topLeftChar (offset) {

    let x = this.x + offset ;
    let leftChar ;

    // Nous somme sur la première ligne
    if (this.y == 0)
      leftChar = x == 0 ? 'topLeft' : (offset == 0 ? 'topMid' : 'top') ;
    else  {

      if (x == 0)
        leftChar = 'leftMid' ;
      else {

        leftChar = offset == 0 ? 'midMid' : 'bottomMid' ;

        if (this.cells) {

          let spanAbove =
            this.cells[this.y - 1][x] instanceof Cell.ColSpanCell ;
          if (spanAbove)
            leftChar = offset == 0 ? 'topMid' : 'mid' ;

          if (offset == 0) {

            let i = 1 ;
            while (this.cells[this.y][x - 1] instanceof Cell.ColSpanCell)
              i++ ;

            if (this.cells[this.y][x - i] instanceof Cell.RowSpanCell)
              leftChar = 'leftMid' ;
          }
        }
      }
    }

    return this.chars[leftChar] ;
  }


  /**
   * Renvoit `content` formaté avec le bon style.
   *
   * @param {String} styleProperty Style
   * @param {String} content contenu
   * @returns {String}
   * @private
   */
  _wrapWithStyleColor (styleProperty, content) {

    if (this[styleProperty] && this[styleProperty].length) {

      try {
        this[styleProperty].forEach(style => {
          content = color[style](content) ;
        }) ;
        return content ;
      } catch (e) {
        return content ;
      }
    }
    else
      return content ;
  }


  /**
   * Applique les bons styles à une ligne
   *
   * @param {String} left Bordure gauche
   * @param {String} content Contenu de la cellule
   * @param {String} right Bordure droite
   * @returns {String}
   * @private
   */
  _stylizeLine (left, content, right) {

    left = this._wrapWithStyleColor('border', left) ;
    right = this._wrapWithStyleColor('border', right) ;

    if (this.y === 0)
      content = this._wrapWithStyleColor('head', content) ;

    return left + content + right ;
  }
}

module.exports = Cell ;

