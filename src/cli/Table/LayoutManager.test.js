'use strict' ;

/**
 * src/cli/Table/LayoutManager.test.js
 */

const expect = require('chai').expect ;

const LayoutManager = require('./LayoutManager'),
      Utils         = require('../../core/Utils'),
      Cell          = require('./Cell'),
      RowSpanCell   = require('./RowSpanCell') ;

const layoutTable = LayoutManager.layoutTable ;
const addRowSpanCells = LayoutManager.addRowSpanCells ;
const maxWidth = LayoutManager.maxWidth ;


// buddy ignore:start
describe('LayoutManager', function () {

  describe('layoutTable', function () {

    it('sets x and y', function () {

      let table = [
        [{}, {}],
        [{}, {}]
      ] ;

      layoutTable(table) ;

      expect(table).to.eql([
        [{x: 0, y: 0}, {x: 1, y: 0}],
        [{x: 0, y: 1}, {x: 1, y: 1}]
      ]) ;

      let w = maxWidth(table) ;
      expect(w).to.equal(2) ;
    }) ;

    it('colSpan will push x values to the right', function () {

      let table = [
        [{colSpan: 2}, {}],
        [{}, {colSpan: 2}]
      ] ;

      layoutTable(table) ;

      expect(table).to.eql([
        [{x: 0, y: 0, colSpan: 2},{x: 2, y: 0}],
        [{x: 0, y: 1}, {x: 1, y: 1, colSpan: 2}]
      ]) ;

      expect(maxWidth(table)).to.equal(3) ;
    }) ;

    it('rowSpan will push x values on cells below', function () {

      let table = [
        [{rowSpan: 2}, {}],
        [{}]
      ] ;

      layoutTable(table) ;

      expect(table).to.eql([
        [{x: 0, y: 0, rowSpan: 2}, {x: 1, y: 0}],
        [{x: 1, y: 1}]
      ]) ;

      expect(maxWidth(table)).to.equal(2) ;
    }) ;

    it('colSpan and rowSpan together', function () {

      let table = [
        [{rowSpan: 2, colSpan: 2}, {}],
        [{}]
      ] ;

      layoutTable(table) ;

      expect(table).to.eql([
        [{x: 0, y: 0, rowSpan: 2, colSpan: 2}, {x: 2, y: 0}],
        [{x: 2, y: 1}]
      ]) ;

      expect(maxWidth(table)).to.equal(3) ;
    }) ;

    it('complex layout', function () {

      let table = [
        [{c: 'a'}, {c: 'b'},      {c: 'c', rowSpan: 3, colSpan: 2}, {c: 'd'}],
        [{c: 'e', rowSpan: 2, colSpan: 2},                          {c: 'f'}],
        [{c: 'g'}]
      ] ;

      layoutTable(table) ;

      expect(table).to.eql([
        [
          {c: 'a', y: 0, x: 0}, {c: 'b', y: 0, x: 1},
          {c: 'c', y: 0, x: 2, rowSpan: 3, colSpan: 2}, {c: 'd', y: 0, x: 4}
        ],
        [{c: 'e', rowSpan: 2, colSpan: 2, y: 1, x: 0}, {c: 'f', y: 1, x: 4}],
        [{c: 'g', y: 2, x: 4}]
      ]) ;
    }) ;

    it('maxWidth of single element', function () {

      let table = [[{}]] ;
      layoutTable(table) ;
      expect(maxWidth(table)).to.equal(1) ;
    }) ;
  }) ;

  describe('addRowSpanCells', function () {

    it('will insert a rowSpan cell - beginning of line', function () {

      let table = [
        [{x: 0, y: 0, rowSpan: 2}, {x: 1, y: 0}],
        [{x: 1, y: 1}]
      ] ;

      addRowSpanCells(table) ;

      expect(table[0]).to.eql([{x: 0, y: 0, rowSpan: 2}, {x: 1, y: 0}]) ;
      expect(table[1].length).to.equal(2) ;
      expect(table[1][0]).to.be.instanceOf(RowSpanCell) ;
      expect(table[1][1]).to.eql({x: 1, y: 1}) ;
    }) ;

    it('will insert a rowSpan cell - end of line', function () {

      let table = [
        [{x: 0, y: 0}, {x: 1, y: 0, rowSpan: 2}],
        [{x: 0, y: 1}]
      ] ;

      addRowSpanCells(table) ;

      expect(table[0]).to.eql([{x: 0, y: 0}, {rowSpan: 2, x: 1, y: 0}]) ;
      expect(table[1].length).to.equal(2) ;
      expect(table[1][0]).to.eql({x: 0, y: 1}) ;
      expect(table[1][1]).to.be.instanceOf(RowSpanCell) ;
    }) ;

    it('will insert a rowSpan cell - middle of line', function () {

      let table = [
        [{x: 0, y: 0}, {x: 1, y: 0, rowSpan: 2}, {x: 2, y: 0}],
        [{x: 0, y: 1}, {x: 2, y: 1}]
      ] ;

      addRowSpanCells(table) ;

      expect(table[0]).to.eql(
          [{x: 0, y: 0}, {rowSpan: 2, x: 1, y: 0}, {x: 2, y: 0}]
      ) ;
      expect(table[1].length).to.equal(3) ;
      expect(table[1][0]).to.eql({x: 0, y: 1}) ;
      expect(table[1][1]).to.be.instanceOf(RowSpanCell) ;
      expect(table[1][2]).to.eql({x: 2, y: 1}) ;
    }) ;

    it('will insert a rowSpan cell - multiple on the same line', function () {

      let table = [
        [
          {x: 0, y: 0}, {x: 1, y: 0, rowSpan: 2},
          {x: 2, y: 0, rowSpan: 2}, {x: 3, y: 0}
        ],
        [{x: 0, y: 1}, {x: 3, y: 1}]
      ] ;

      addRowSpanCells(table) ;

      expect(table[0]).to.eql(
        [{x: 0, y: 0}, {rowSpan: 2, x: 1, y: 0},
          {rowSpan: 2, x: 2, y: 0}, {x: 3, y: 0}]) ;
      expect(table[1].length).to.equal(4) ;
      expect(table[1][0]).to.eql({x: 0, y: 1}) ;
      expect(table[1][1]).to.be.instanceOf(RowSpanCell) ;
      expect(table[1][2]).to.be.instanceOf(RowSpanCell) ;
      expect(table[1][3]).to.eql({x: 3, y: 1}) ;
    }) ;
  }) ;

  it('simple 2x2 layout', function () {

    let actual = LayoutManager.makeTableLayout([
      ['hello','goodbye'],
      ['hola','adios']
    ]) ;

    let expected = [
      ['hello','goodbye'],
      ['hola','adios']
    ] ;

    checkLayout(actual,expected) ;
  }) ;

  it('cross table', function () {

    let actual = LayoutManager.makeTableLayout([
      {'1.0': ['yes','no']},
      {'2.0': ['hello','goodbye']}
    ]) ;

    let expected = [
      ['1.0', 'yes', 'no'],
      ['2.0', 'hello', 'goodbye']
    ] ;

    checkLayout(actual, expected) ;
  }) ;

  it('vertical table', function () {

    let actual = LayoutManager.makeTableLayout([
      {'1.0': 'yes'},
      {'2.0': 'hello'}
    ]) ;

    let expected = [
      ['1.0', 'yes'],
      ['2.0', 'hello']
    ] ;

    checkLayout(actual, expected) ;
  }) ;

  it('colSpan adds RowSpanCells to the right', function () {

    let actual = LayoutManager.makeTableLayout([
      [{content: 'hello', colSpan: 2}],
      ['hola', 'adios']
    ]) ;

    let expected = [
      [{content: 'hello', colSpan: 2}, null],
      ['hola', 'adios']
    ] ;

    checkLayout(actual, expected) ;
  }) ;

  it('rowSpan adds RowSpanCell below', function () {

    let actual = LayoutManager.makeTableLayout([
      [{content: 'hello', rowSpan: 2}, 'goodbye'],
      ['adios']
    ]) ;

    let expected = [
      ['hello'            , 'goodbye'],
      [{spannerFor: [0,0]}, 'adios']
    ] ;

    checkLayout(actual, expected) ;
  }) ;

  it('rowSpan and cellSpan together', function () {

    let actual = LayoutManager.makeTableLayout([
      [{content: 'hello', rowSpan: 2, colSpan: 2}, 'goodbye'],
      ['adios']
    ]) ;

    let expected = [
      ['hello'            , null, 'goodbye'],
      [{spannerFor: [0,0]}, null, 'adios']
    ] ;

    checkLayout(actual, expected) ;
  }) ;

  it('complex layout', function () {

    let actual = LayoutManager.makeTableLayout([
      [
        {content: 'hello', rowSpan: 2, colSpan: 2},
        {content: 'yo', rowSpan: 2, colSpan: 2},'goodbye'
      ],
      ['adios']
    ]) ;

    let expected = [
      ['hello'            , null, 'yo'               , null, 'goodbye'],
      [{spannerFor: [0,0]}, null, {spannerFor: [0,2]}, null, 'adios']
    ] ;

    checkLayout(actual, expected) ;
  }) ;

  it('complex layout2', function () {

    let actual = LayoutManager.makeTableLayout([
      ['a', 'b', {content: 'c', rowSpan: 3, colSpan: 2}, 'd'],
      [{content: 'e', rowSpan: 2, colSpan: 2}, 'f'],
      ['g']
    ]) ;

    let expected = [
      ['a', 'b', 'c', null, 'd'],
      ['e', null, {spannerFor: [0, 2]}, null, 'f'],
      [{spannerFor: [1, 0]}, null, {spannerFor: [0, 2]}, null, 'g']
    ] ;

    checkLayout(actual, expected) ;
  }) ;

  it('stairstep spans', function () {

    let actual = LayoutManager.makeTableLayout([
      [{content: '', rowSpan: 2}, ''],
      [{content: '', rowSpan: 2}],
      ['']
    ]) ;

    let expected = [
      [{content: '', rowSpan: 2}, ''],
      [{spannerFor: [0, 0]}, {content: '', rowSpan: 2}],
      ['', {spannerFor: [1, 1]}]
    ] ;

    checkLayout(actual, expected) ;
  }) ;

  describe('fillInTable', function () {

    function mc (opts, y, x) {

      let cell = new Cell(opts) ;

      cell.x = x ;
      cell.y = y ;

      return cell ;
    }

    it('will blank out individual cells', function () {

      let cells = [
        [mc('a', 0, 1)],
        [mc('b', 1, 0)]
      ] ;
      LayoutManager.fillInTable(cells) ;

      checkLayout(cells, [
        ['', 'a'],
        ['b', '']
      ]) ;
    }) ;

    it('will autospan to the right', function () {

      let cells = [
        [],
        [mc('a', 1, 1)]
      ] ;
      LayoutManager.fillInTable(cells) ;

      checkLayout(cells, [
        [{content: '', colSpan: 2}, null],
        ['', 'a']
      ]) ;
    }) ;

    it('will autospan down', function () {

      let cells = [
        [mc('a', 0, 1)],
        []
      ] ;
      LayoutManager.fillInTable(cells) ;
      addRowSpanCells(cells) ;

      checkLayout(cells, [
        [{content: '', rowSpan: 2}, 'a'],
        [{spannerFor: [0, 0]}, '']
      ]) ;
    }) ;

    it('will autospan right and down', function () {

      let cells = [
        [mc('a', 0, 2)],
        [],
        [mc('b', 2, 1)]
      ] ;
      LayoutManager.fillInTable(cells) ;
      addRowSpanCells(cells) ;

      checkLayout(cells, [
        [{content: '', colSpan: 2, rowSpan: 2}, null, 'a'],
        [{spannerFor: [0, 0]}, null, {content: '', colSpan: 1, rowSpan: 2}],
        ['', 'b',{spannerFor: [1, 2]}]
      ]) ;
    }) ;
  }) ;

  describe('computeWidths', function () {

    function mc (y, x, desiredWidth, colSpan) {

      return {x: x, y: y, desiredWidth: desiredWidth, colSpan: colSpan} ;
    }

    it('finds the maximum desired width of each column', function () {

      let widths = [] ;
      let cells = [
        [mc(0, 0, 7), mc(0, 1, 3), mc(0, 2, 5)],
        [mc(1, 0, 8), mc(1, 1, 5), mc(1, 2, 2)],
        [mc(2, 0, 6), mc(2, 1, 9), mc(2, 2, 1)]
      ] ;

      LayoutManager.computeWidths(widths, cells) ;

      expect(widths).to.eql([8, 9, 5]) ;
    }) ;

    it('won\'t touch hard coded values', function () {

      let widths = [null, 3] ;
      let cells = [
        [mc(0, 0, 7), mc(0, 1, 3), mc(0, 2, 5)],
        [mc(1, 0, 8), mc(1, 1, 5), mc(1, 2, 2)],
        [mc(2, 0, 6), mc(2, 1, 9), mc(2, 2, 1)]
      ] ;

      LayoutManager.computeWidths(widths, cells) ;

      expect(widths).to.eql([8, 3, 5]) ;
    }) ;

    it('assumes undefined desiredWidth is 1', function () {

      let widths = [] ;
      let cells = [[{x: 0, y: 0}], [{x: 0, y: 1}], [{x: 0, y: 2}]] ;
      LayoutManager.computeWidths(widths, cells) ;
      expect(widths).to.eql([1]) ;
    }) ;

    it('takes into account colSpan and wont over expand', function () {

      let widths = [] ;
      let cells = [
        [mc(0, 0, 10, 2), mc(0, 2, 5)],
        [mc(1, 0, 5), mc(1, 1, 5), mc(1, 2, 2)],
        [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)]
      ] ;
      LayoutManager.computeWidths(widths, cells) ;
      expect(widths).to.eql([5, 5, 5]) ;
    }) ;

    it('will expand rows involved in colSpan in a balanced way', function () {

      let widths = [] ;
      let cells = [
        [mc(0, 0, 13, 2), mc(0, 2, 5)],
        [mc(1, 0, 5), mc(1, 1, 5), mc(1, 2, 2)],
        [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)]
      ] ;
      LayoutManager.computeWidths(widths, cells) ;
      expect(widths).to.eql([6, 6, 5]) ;
    }) ;

    it('expands across 3 cols', function () {

      let widths = [] ;
      let cells = [
        [mc(0, 0, 25, 3)],
        [mc(1, 0, 5), mc(1, 1, 5), mc(1, 2, 2)],
        [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)]
      ] ;
      LayoutManager.computeWidths(widths, cells) ;
      expect(widths).to.eql([9, 9, 5]) ;
    }) ;

    it('multiple spans in same table', function () {

      let widths = [] ;
      let cells = [
        [mc(0, 0, 25, 3)],
        [mc(1, 0, 30, 3)],
        [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)]
      ] ;
      LayoutManager.computeWidths(widths, cells) ;
      expect(widths).to.eql([11, 9, 8]) ;
    }) ;

    it('spans will only edit uneditable tables', function () {

      let widths = [null, 3] ;
      let cells = [
        [mc(0, 0, 20, 3)],
        [mc(1, 0, 4), mc(1, 1, 20), mc(1, 2, 5)]
      ] ;
      LayoutManager.computeWidths(widths, cells) ;
      expect(widths).to.eql([7, 3, 8]) ;
    }) ;

    it('spans will only edit uneditable tables - first column uneditable',
    function () {

      let widths = [3] ;
      let cells = [
        [mc(0, 0, 20, 3)],
        [mc(1, 0, 4), mc(1, 1, 3), mc(1, 2, 5)]
      ] ;
      LayoutManager.computeWidths(widths, cells) ;
      expect(widths).to.eql([3, 7, 8]) ;
    }) ;
  }) ;

  describe('computeHeights', function () {

    function mc (y, x, desiredHeight, colSpan) {

      return {x: x, y: y, desiredHeight: desiredHeight, rowSpan: colSpan} ;
    }

    it('finds the maximum desired height of each row', function () {

      let heights = [] ;
      let cells = [
        [mc(0, 0, 7), mc(0, 1, 3), mc(0, 2, 5)],
        [mc(1, 0, 8), mc(1, 1, 5), mc(1, 2, 2)],
        [mc(2, 0, 6), mc(2, 1, 9), mc(2, 2, 1)]
      ] ;

      LayoutManager.computeHeights(heights, cells) ;

      expect(heights).to.eql([7, 8, 9]) ;
    }) ;

    it('won\'t touch hard coded values', function () {

      let heights = [null, 3] ;
      let cells = [
        [mc(0, 0, 7), mc(0, 1, 3), mc(0, 2, 5)],
        [mc(1, 0, 8), mc(1, 1, 5), mc(1, 2, 2)],
        [mc(2, 0, 6), mc(2, 1, 9), mc(2, 2, 1)]
      ] ;

      LayoutManager.computeHeights(heights, cells) ;

      expect(heights).to.eql([7, 3, 9]) ;
    }) ;

    it('assumes undefined desiredHeight is 1', function () {

      let heights = [] ;
      let cells = [[{x: 0, y: 0}, {x: 1, y: 0}, {x: 2, y: 0}]] ;
      LayoutManager.computeHeights(heights, cells) ;
      expect(heights).to.eql([1]) ;
    }) ;

    it('takes into account rowSpan and wont over expand', function () {

      let heights = [] ;
      let cells = [
        [mc(0, 0, 10, 2), mc(0, 1, 5), mc(0, 2, 2)],
        [mc(1, 1, 5), mc(1, 2, 2)],
        [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)]
      ] ;
      LayoutManager.computeHeights(heights, cells) ;
      expect(heights).to.eql([5, 5, 4]) ;
    }) ;

    it('will expand rows involved in rowSpan in a balanced way', function () {

      let heights = [] ;
      let cells = [
        [mc(0, 0, 13, 2), mc(0, 1, 5), mc(0, 2, 5)],
        [mc(1, 1, 5), mc(1, 2, 2)],
        [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)]
      ] ;
      LayoutManager.computeHeights(heights, cells) ;
      expect(heights).to.eql([6, 6, 4]) ;
    }) ;

    it('expands across 3 rows', function () {

      let heights = [] ;
      let cells = [
        [mc(0, 0, 25, 3), mc(0, 1, 5), mc(0, 2, 4)],
        [mc(1, 1, 5), mc(1, 2, 2)],
        [mc(2, 1, 2), mc(2, 2, 1)]
      ] ;
      LayoutManager.computeHeights(heights, cells) ;
      expect(heights).to.eql([9, 9, 5]) ;
    }) ;

    it('multiple spans in same table', function () {

      let heights = [] ;
      let cells = [
        [mc(0, 0, 25, 3), mc(0, 1, 30, 3), mc(0, 2, 4)],
        [mc(1, 2, 2)],
        [mc(2, 2, 1)]
      ] ;
      LayoutManager.computeHeights(heights, cells) ;
      expect(heights).to.eql([11, 9, 8]) ;
    }) ;
  }) ;


  function checkLayout (actualTable, expectedTable) {

    expectedTable.forEach((expectedRow, y) => {
      expectedRow.forEach((expectedCell,x) => {

        if (expectedCell !== null) {
          let actualCell = findCell(actualTable, x, y) ;
          checkExpectation(actualCell, expectedCell, x, y, actualTable) ;
        }
      }) ;
    }) ;
  }

  function findCell (table, x, y) {

    for (let i = 0 ; i < table.length ; i++) {

      let row = table[i] ;

      for (let j = 0 ; j < row.length ; j++) {

        let cell = row[j] ;
        if (cell.x === x && cell.y === y)
          return cell ;
      }
    }
  }

  function checkExpectation (actualCell, expectedCell, x, y, actualTable) {

    if (Utils.isString(expectedCell))
      expectedCell = {content: expectedCell} ;

    let address = '(' + y + ',' + x + ')' ;

    if (expectedCell.hasOwnProperty('content')) {
      expect(actualCell, address).to.be.instanceOf(Cell) ;
      expect(actualCell.content,'content of ' + address)
        .to.equal(expectedCell.content) ;
    }

    if(expectedCell.hasOwnProperty('rowSpan')) {
      expect(actualCell, address).to.be.instanceOf(Cell) ;
      expect(actualCell.rowSpan, 'rowSpan of ' + address)
        .to.equal(expectedCell.rowSpan) ;
    }

    if(expectedCell.hasOwnProperty('colSpan')) {
      expect(actualCell, address).to.be.instanceOf(Cell) ;
      expect(actualCell.colSpan, 'colSpan of ' + address)
        .to.equal(expectedCell.colSpan) ;
    }

    if(expectedCell.hasOwnProperty('spannerFor')) {

      expect(actualCell, address).to.be.instanceOf(RowSpanCell) ;
      expect(actualCell.originalCell, address
             + 'originalCell should be a cell').to.be.instanceOf(Cell) ;
      expect(actualCell.originalCell, address + 'originalCell not right')
        .to.equal(findCell(actualTable, expectedCell.spannerFor[1],
          expectedCell.spannerFor[0]
        )) ;
    }
  }

}) ;
// buddy ignore:end

