'use strict' ;

/**
 * src/cli/Table/LayoutManager.js
 */

/*
 Inspiré par cli-table2
 */

const Utils       = require('../../core/Utils'),
      RowSpanCell = require('./RowSpanCell'),
      ColSpanCell = require('./ColSpanCell') ;

/**
 * Gère le layout du tableau
 */
class LayoutManager {


  /**
   * Créé le layout du tableau (array)
   *
   * @param {Array} rows lignes du tableau
   * @returns {Array}
   * @static
   */
  static makeTableLayout (rows) {

    let cellRows = LayoutManager._generateCells(rows) ;

    LayoutManager.layoutTable(cellRows) ;
    LayoutManager.fillInTable(cellRows) ;
    LayoutManager.addRowSpanCells(cellRows) ;
    LayoutManager._addColSpanCells(cellRows) ;

    return cellRows ;
  }


  /**
   * Génère le layout du tableau
   * Renseigne cell.x et cell.y
   *
   * @param {Table} table Tableau
   * @static
   */
  static layoutTable (table) {

    table.forEach((row, rowIndex) => {
      row.forEach((cell, columnIndex) => {

        cell.y = rowIndex ;
        cell.x = columnIndex ;

        for (let y = rowIndex ; y >= 0 ; y--) {

          let row2 = table[y] ;
          let xMax = (y === rowIndex) ? columnIndex : row2.length ;

          for (let x = 0 ; x < xMax ; x++) {

            let cell2 = row2[x] ;
            while(LayoutManager._cellsConflict(cell, cell2))
              cell.x++ ;
          }
        }
      }) ;
    }) ;
  }


  /**
   * Ajoute des cellules pour le rowSpan
   * @param {Table} table Tableau
   * @static
   */
  static addRowSpanCells (table) {

    table.forEach((row, rowIndex) => {
      row.forEach(cell => {

        for (let i = 1 ; i < cell.rowSpan ; i++) {

          let rowSpanCell = new RowSpanCell(cell) ;
          rowSpanCell.x = cell.x ;
          rowSpanCell.y = cell.y + i ;
          rowSpanCell.colSpan = cell.colSpan ;
          LayoutManager._insertCell(rowSpanCell, table[rowIndex + i]) ;
        }
      }) ;
    }) ;
  }


  /**
   * Retourne la largeur maximum du tableau
   *
   * @param {Table} table Le tableau
   * @returns {Integer}
   * @static
   */
  static maxWidth (table) {

    let mw = 0 ;

    table.forEach(row => {
      row.forEach(cell => {
        mw = Math.max(mw, cell.x + (cell.colSpan || 1)) ;
      }) ;
    }) ;

    return mw ;
  }


  /**
   * @param {Table} table Tableau
   * @static
   */
  static fillInTable (table) {

    let h_max = table.length ;
    let w_max = LayoutManager.maxWidth(table) ;

    for (let y = 0 ; y < h_max ; y++) {
      for (let x = 0 ; x < w_max ; x++) {

        if(!LayoutManager._conflictExists(table, x, y)) {

          let opts = {x: x, y: y, colSpan: 1, rowSpan: 1} ;
          x++ ;
          while (x < w_max &&
                 !LayoutManager._conflictExists(table, x, y)) {
            opts.colSpan++ ;
            x++ ;
          }
          let y2 = y + 1 ;
          while (y2 < h_max && LayoutManager._allBlank(
            table, y2, opts.x, opts.x + opts.colSpan)) {
            opts.rowSpan++ ;
            y2++ ;
          }

          let cell = require('./Cell') ;
          cell = new cell(opts) ;
          cell.x = opts.x ;
          cell.y = opts.y ;
          LayoutManager._insertCell(cell, table[y]) ;
        }
      }
    }
  }


  static _makeComputeWidths (colSpan, desiredWidth, x, forcedMin) {

    // Renvoit une fonction
    // {Array} vals : valeurs forcées ([null, 2] : la deuxième colonne à 2
    //                                 [2, 3, 4] : 1ère col 2, 2e 3, 3e 4)
    // {Array} table : le tableau à traiter
    return (vals, table) => {

      let result = [] ;
      let spanners = [] ;

      table.forEach(row => {
        row.forEach(cell => {

          // S'il y a un col/rowSpan, on récupère la valeur
          if ((cell[colSpan] || 1) > 1)
            spanners.push(cell) ;
          // Sinon on prend la valeur correspondant à la cellule
          else {
            result[cell[x]] = Math.max(result[cell[x]] || 0,
                cell[desiredWidth] || 0, forcedMin) ;
          }
        }) ;
      }) ;

      // Si une valeur est précisée, on prend celle-ci
      vals.forEach((val, index) => {

        if(Utils.isNumber(val))
          result[index] = val ;
      }) ;

      // On parcours tous les sapnners enregistrés
      for (let k = spanners.length - 1 ; k >= 0 ; k--) {

        let cell = spanners[k] ;
        let span = cell[colSpan] ;
        let col = cell[x] ;
        let existingWidth = result[col] ;
        let editableCols = Utils.isNumber(vals[col]) ? 0 : 1 ;

        for (let i = 1 ; i < span ; i++) {

          existingWidth += 1 + result[col + i] ;
          if (!Utils.isNumber(vals[col + i]))
            editableCols++ ;
        }

        if (cell[desiredWidth] > existingWidth) {

          let i = 0 ;
          while (editableCols > 0 && cell[desiredWidth] > existingWidth) {

            if (!Utils.isNumber(vals[col + i])) {

              let dif = Math.round(
                  (cell[desiredWidth] - existingWidth) / editableCols) ;

              existingWidth += dif ;
              result[col + i] += dif ;
              editableCols-- ;
            }
            i++ ;
          }
        }
      }

      Utils.extend(vals, result) ;
      for (let j = 0 ; j < vals.length ; j++)
        vals[j] = Math.max(forcedMin, vals[j] || 0) ;
    } ;
  }


  /**
   * Détecte s'il y a un conflit entre deux cellules à cause d'une propriété
   * colSpan ou rowSpan qui vient décaler leur placement x et y.
   *
   * @param {Cell} cell1 Première cellule
   * @param {Cell} cell2 Deuxième cellule
   * @returns {Boolean}
   * @private
   * @static
   */
  static _cellsConflict (cell1, cell2) {

    let yMin1 = cell1.y ;
    let yMax1 = cell1.y - 1 + (cell2.rowSpan || 1) ;
    let yMin2 = cell2.y ;
    let yMax2 = cell2.y - 1 + (cell2.rowSpan || 1) ;
    let yConflict = !(yMin1 > yMax2 || yMin2 > yMax1) ;

    let xMin1 = cell1.x ;
    let xMax1 = cell1.x - 1 + (cell2.colSpan || 1) ;
    let xMin2 = cell2.x ;
    let xMax2 = cell2.x - 1 + (cell2.colSpan || 1) ;
    let xConflict = !(xMin1 > xMax2 || xMin2 > xMax1) ;

    return yConflict && xConflict ;
  }


  /**
   * Insert une cellule
   *
   * @param {Cell} cell Cellule à insérer
   * @param {Array} row Ligne dans laquelle l'insérer
   * @static
   * @private
   */
  static _insertCell (cell, row) {

    let x = 0 ;
    while (x < row.length && (row[x].x < cell.x))
      x++ ;

    row.splice(x, 0, cell) ;
  }


  /**
   * Génère les cellules de plusieurs lignes
   *
   * @param {Array} rows Lignes du tableau
   * @returns {Array}
   * @static
   * @private
   */
  static _generateCells (rows) {

    return rows.map(row => {

      if (!Array.isArray(row)) {

        let key = Object.keys(row)[0] ;
        row = row[key] ;

        if (Array.isArray(row)) {
          row = row.slice() ;
          row.unshift(key) ;
        }
        else
          row = [key, row] ;
      }

      return row.map(cell => {
        let Cell = require('./Cell') ;
        return new Cell(cell) ;
      }) ;
    }) ;
  }


  /**
   * @param {Array} rows Lignes
   * @param {Integer} y Position y
   * @param {Integer} xMin Valeur minimale de x
   * @param {Integer} xMax Valeur maximale de x
   * @returns {Boolean}
   * @static
   * @private
   */
  static _allBlank (rows, y, xMin, xMax) {

    for (let x = xMin ; x < xMax ; x++) {
      if (LayoutManager._conflictExists(rows, x, y))
        return false ;
    }
    return true ;
  }


  /**
   * @param {Array} rows Lignes
   * @param {Integer} x Position x
   * @param {Integer} y Position y
   * @returns {Boolean}
   * @static
   * @private
   */
  static _conflictExists (rows, x, y) {

    let i_max = Math.min(rows.length - 1, y) ;
    let cell = {x: x, y: y} ;

    for (let i = 0 ; i <= i_max ; i++) {

      let row = rows[i] ;
      for (let j = 0 ; j < row.length ; j++) {
        if (LayoutManager._cellsConflict(cell, row[j]))
          return true ;
      }
    }

    return false ;
  }


  /**
   * @param {Array} cellRows Cellule d'une ligne
   * @static
   * @private
   */
  static _addColSpanCells (cellRows) {

    for (let rowIndex = cellRows.length - 1 ; rowIndex >= 0 ; rowIndex--) {

      let cellColumns = cellRows[rowIndex] ;

      for (let columnIndex = 0 ; columnIndex < cellColumns.length ;
        columnIndex++) {

        let cell = cellColumns[columnIndex] ;

        for (let k = 1 ; k < cell.colSpan ; k++) {

          let colSpanCell = new ColSpanCell() ;
          colSpanCell.x = cell.x + k ;
          colSpanCell.y = cell.y ;
          cellColumns.splice(columnIndex + 1, 0, colSpanCell) ;
        }
      }
    }
  }
}


module.exports = LayoutManager ;
module.exports.computeWidths =
  LayoutManager._makeComputeWidths('colSpan', 'desiredWidth', 'x', 1) ;
module.exports.computeHeights =
  LayoutManager._makeComputeWidths('rowSpan', 'desiredHeight', 'y', 1) ;

