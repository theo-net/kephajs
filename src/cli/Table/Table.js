'use strict' ;

/**
 * src/cli/Table/Table.js
 */

/*
 Inspiré par `cli-table2` James Talmage <james.talmage@jrtechnical.com>
*/

const Utils         = require('../../core/Utils'),
      Color         = require('../Color'),
      LayoutManager = require('./LayoutManager') ;

/**
 * Utilitaire pour le rendu de tableau dans la ligne de commande à partir de
 * vos scripts node.js. Cet utilitaire est basé sur `cli-table2` dont il
 * reprend la plupart des fonctionnalités.
 *
 *    let table = new Table() ;
 *    table.push(
 *      {'a': 'b'},
 *      {'c': 'd'}
 *    ) ;
 *    console.log(table.toString()) ;
 *
 *
 *    table.push(
 *      {'Left header': 'valeur1'},
 *      {'Left header': 'valeur2'}
 *    ) ;
 *
 *
 *    table.push(
 *      ['l1 c1', 'l1 c2', 'l1 c3'],
 *      ['l2 c1', 'l2 c2', 'l2 c3']
 *    ) ;
 *
 *    new Table({
 *      head: ['TH 1 label', 'TH 2 label']
 *    }) ;
 */
class Table extends Array {

  /**
   * Initialise notre tableau
   *
   * @param {Array} [options] Options du tableau
   */
  constructor (options) {

    super() ;

    this._options = Table._mergeOptions(options) ;
  }


  /**
   * Rendu du tableau
   *
   * @returns {String}
   */
  toString () {

    let array = this ;
    let headersPresent = this._options.head && this._options.head.length ;

    // On ajoute le head
    if (headersPresent) {

      array = [this._options.head] ;
      if (this.length)
        array.push.apply(array, this) ;
    }
    else
      this._options.style.head = [] ;

    // On créé les cellules
    let cells = LayoutManager.makeTableLayout(array) ;

    Utils.forEach(cells, row => {
      Utils.forEach(row, cell => {
        cell.mergeTableOptions(this._options, cells) ;
      }, this) ;
    }, this) ;

    // On calcule la taille des cellules
    LayoutManager.computeWidths(this._options.colWidths, cells) ;
    LayoutManager.computeHeights(this._options.rowHeights, cells) ;

    // On initialise le rendus des cellules
    Utils.forEach(cells, row => {
      Utils.forEach(row, cell => {
        cell.init(this._options) ;
      }, this) ;
    }, this) ;

    // On passe au rendu du tableau, qui se fera par ligne
    let result = [] ;

    for (let rowIndex = 0 ; rowIndex < cells.length ; rowIndex++) {

      let row = cells[rowIndex] ;
      let heightOfRow = this._options.rowHeights[rowIndex] ;

      // Haut du tableau
      if (rowIndex === 0 || !this._options.style.compact ||
          (rowIndex == 1 && headersPresent))
        this._doDraw(row, 'top', result) ;

      // Toutes les lignes intermédiaires
      for (let lineNum = 0 ; lineNum < heightOfRow ; lineNum++)
        this._doDraw(row, lineNum, result) ;

      // Bas du tableau
      if (rowIndex + 1 == cells.length)
        this._doDraw(row, 'bottom', result) ;
    }

    // On fusionne le résultat que l'on retourne
    return result.join('\n') ;
  }


  /**
   * Getter pour `width`
   * @returns {Integer}
   */
  get width () {

    let str = this.toString().split('\n') ;

    return str[0].length ;
  }


  /**
   * Retourne la longueur de la chaîne, en supprimant les caractères de
   * contrôle de style et couleurs. Il s'agit de la longueur maximum d'une
   * ligne si `str` est composé de plusieurs lignes. De plus, les caractères
   * unicodes ne sont pas tous traités ?
   *
   * @param {String} str Chaîne dont on cherche la longueur
   * @return {Integer}
   * @static
   */
  static strlen (str) {

    // On supprime les codes de controle
    let stripped = ('' + str).replace(Color.codeRegex(), '') ;

    // On découpe pour traiter par ligne
    let split = stripped.split('\n') ;

    function stringWidth (s) {

      // buddy ignore:start

      let width = 0 ;

      for (let i = 0 ; i < s.length ; i++) {

        let code = s.charCodeAt(i) ;
        if (code >= 0xD800 && code <= 0xDBFF && s.length > i + 1) {
          let second = s.charCodeAt(i + 1) ;
          if (second >= 0xDC00 && second <= 0xDFFF)
            return ((code - 0xD800) * 0x400) + second - 0xDC00 + 0x10000 ;
        }

        // ignore control characters
        if (code <= 0x1f || (code >= 0x7f && code <= 0x9f))
          continue ;

        // is fullwidth code point
        if (code >= 0x1100 &&
          (code <= 0x115f ||  // Hangul Jamo
           0x2329 === code || // LEFT-POINTING ANGLE BRACKET
           0x232a === code || // RIGHT-POINTING ANGLE BRACKET
           // CJK Radicals Supplement .. Enclosed CJK Letters and Months
           (0x2e80 <= code && code <= 0x3247 && code !== 0x303f) ||
           // Enclosed CJK Letters and Months .. CJK Unified Ideographs Ext A
           0x3250 <= code && code <= 0x4dbf ||
           // CJK Unified Ideographs .. Yi Radicals
           0x4e00 <= code && code <= 0xa4c6 ||
           // Hangul Jamo Extended-A
           0xa960 <= code && code <= 0xa97c ||
           // Hangul Syllables
           0xac00 <= code && code <= 0xd7a3 ||
           // CJK Compatibility Ideographs
           0xf900 <= code && code <= 0xfaff ||
           // Vertical Forms
           0xfe10 <= code && code <= 0xfe19 ||
           // CJK Compatibility Forms .. Small Form Variants
           0xfe30 <= code && code <= 0xfe6b ||
           // Halfwidth and Fullwidth Forms
           0xff01 <= code && code <= 0xff60 ||
           0xffe0 <= code && code <= 0xffe6 ||
           // Kana Supplement
           0x1b000 <= code && code <= 0x1b001 ||
           // Enclosed Ideographic Supplement
           0x1f200 <= code && code <= 0x1f251 ||
           // CJK Unified Ideographs Extension B .. Tertiary Ideographic Plane
           0x20000 <= code && code <= 0x3fffd))
          width += 2 ;
        else
          width++ ;
      }

      return width ;
    }

    return split.reduce((memo, s) => {
      return (stringWidth(s) > memo) ? stringWidth(s) : memo ;
    }, 0) ;

    // buddy ignore:end
  }


  /**
   * Tronque une chaîne de caractères.
   *
   * @param {String} str Chaîne à tronquer
   * @param {Integer} desiredLength Longueur désirée
   * @param {String} [truncateChar] Caractère indiquant la troncature
   * @returns {String}
   * @static
   */
  static truncate (str, desiredLength, truncateChar = '…') {

    let lengthOfStr = this.strlen(str) ;
    if (lengthOfStr <= desiredLength)
      return str ;

    desiredLength -= this.strlen(truncateChar) ;


    function truncateWidth (str, desiredLength) {

      if (str.length === Table.strlen(str))
        return str.substr(0, desiredLength) ;

      while (Table.strlen(str) > desiredLength)
        str = str.slice(0, -1) ;

      return str ;
    }

    function truncateWidthWithAnsi (str, desiredLength) {

      let code = Color.codeRegex(true) ;
      let split = str.split(Color.codeRegex()) ;
      let splitIndex = 0 ;
      let retLen = 0 ;
      let ret = '' ;
      let myArray ;
      let state = {} ;

      while(retLen < desiredLength) {

        myArray = code.exec(str) ;
        let toAdd = split[splitIndex] ;
        splitIndex++ ;

        if (retLen + Table.strlen(toAdd) > desiredLength)
          toAdd = truncateWidth(toAdd, desiredLength - retLen) ;

        ret += toAdd ;
        retLen += Table.strlen(toAdd) ;

        if (retLen < desiredLength) {

          if (!myArray) break ;
          ret += myArray[0] ;
          Color._updateState(state, myArray) ;
        }
      }

      return Color._unwindState(state, ret) ;
    }

    return truncateWidthWithAnsi(str, desiredLength) + truncateChar ;
  }


  /**
   * Découpe une chaîne de caractère sur plusieurs lignes. Celle-ci le sera
   * sur les caractères d'espacements.
   *
   * @param {Integer} maxLength Taille maximum d'une ligne
   * @param {String} input Chaîne à traiter
   * @return {String}
   * @static
   */
  static wordWrap (maxLength, input) {

    let output = [] ;
    input = input.split('\n') ;

    for (let i = 0 ; i < input.length ; i++) {

      let lines = [] ;
      let split = input[i].split(/(\s+)/g) ;
      let line = [] ;
      let lineLength = 0 ;
      let whitespace ;

      // On parcours chaque mot (un élément sur deux, le seuxième étant un ou
      // plusieurs caractères d'espacement.
      for (let k = 0 ; k < split.length ; k += 2) {

        let word = split[k] ;
        let newLength = lineLength + Table.strlen(word) ;

        // Notre mot est précédé d'un caractère d'esapcement
        if (lineLength > 0 && whitespace)
          newLength += whitespace.length ;

        // Si la taille est trop grande, on devra couper la ligne
        if (newLength > maxLength) {

          if (lineLength !== 0)
            lines.push(line.join('')) ;
          line = [word] ;
          lineLength = Table.strlen(word) ;
        } else {
          line.push(whitespace || '', word) ;
          lineLength = newLength ;
        }

        // Sauvegarde du caractère d'espacement
        whitespace = split[k + 1] ;
      }

      if (lineLength)
        lines.push(line.join('')) ;

      // Utiliser `apply` permet de récupérer individuellement chaque lignes
      // pas insérer le tableau.
      output.push.apply(output, lines) ;
    }

    return output ;
  }


  /**
   * Merge les options transmises avec les valeurs par défauts pour compléter
   * les premières si besoin.
   *
   * @param {Object} options Options à traiter
   * @param {Object} defaults Valeurs par défaut des options
   * @returns {Object} Options mergées avec les valeurs par défaut
   * @private
   */
  static _mergeOptions (options, defaults) {

    // Options par défaut
    defaults = defaults || {
      chars: {
        'top': '─',
        'top-mid': '┬',
        'top-left': '┌',
        'top-right': '┐',
        'bottom': '─',
        'bottom-mid': '┴',
        'bottom-left': '└',
        'bottom-right': '┘',
        'left': '│',
        'left-mid': '├',
        'mid': '─',
        'mid-mid': '┼',
        'right': '│',
        'right-mid': '┤',
        'middle': '│'
      },
      truncate: '…',
      'colWidths': [],
      'rowHeights': [],
      'colAligns': [],
      'rowAligns': [],
      'style': {
        'padding-left': 1,
        'padding-right': 1,
        'head': ['red'],
        'border': ['grey'],
        'compact': false
      },
      head: []
    } ;

    // On merge nos options
    options = options || {} ;
    let ret = Utils.extend({}, defaults, options) ;
    ret.chars = Utils.extend({}, defaults.chars, options.chars) ;
    ret.style = Utils.extend({}, defaults.style, options.style) ;

    return ret ;
  }


  /**
   * Retourne un tableau sans bordures
   *
   * @param {Object} [opts={}] Options à appliquer
   * @returns {Table}
   * @static
   */
  static getOptionsNoBorder (opts = {}) {

    let defaults = Table._mergeOptions({
      chars: {
        bottom: '', 'bottom-left': '', 'bottom-mid': '', 'bottom-right': '',
        left: '', 'left-mid': '', mid: '', 'mid-mid': '', middle: '',
        right: '', 'right-mid': '',
        top: '', 'top-left': '', 'top-mid': '', 'top-right': ''
      }
    }) ;

    return Table._mergeOptions(opts, defaults) ;
  }


  /**
   * Dessine une ligne de cellule
   * @param {Array} row Rangée de cellules à dessiner
   * @param {Integer} lineNum Numéro de la ligne à dessiner
   * @param {Array} result Tableau contenant le résultat
   * @private
   */
  _doDraw (row, lineNum, result) {

    let line = [] ;

    Utils.forEach(row, cell => {
      line.push(cell.draw(lineNum)) ;
    }) ;

    let str = line.join('') ;
    if (str.length) result.push(str) ;
  }
}

module.exports = Table ;

