'use strict' ;

/**
 * src/cli/Suggest.test.js
 */

const expect = require('chai').expect ;
const Suggest = require('./Suggest') ;

describe('Suggest', function () {

  let possibilities = ['test', 'start', 'foo', 'bar', 'for'] ;

  describe('getSuggestions', function () {

    it('returns a possibilitie', function () {

      expect(Suggest.getSuggestions('test', possibilities))
        .to.be.deep.equal(['test']) ;
      expect(Suggest.getSuggestions('tet', possibilities))
        .to.be.deep.equal(['test']) ;
      expect(Suggest.getSuggestions('star', possibilities))
        .to.be.deep.equal(['start', 'bar']) ;
      expect(Suggest.getSuggestions('fo', possibilities))
        .to.be.deep.equal(['foo', 'for']) ;
      expect(Suggest.getSuggestions('bart', possibilities))
        .to.be.deep.equal(['bar', 'start']) ;
      expect(Suggest.getSuggestions('rien', possibilities))
        .to.be.deep.equal([]) ;
    }) ;
  }) ;

  describe('getBoldDiffString', function () {

    it('returns difference', function () {

      expect(Suggest.getBoldDiffString('test', 'test'))
        .to.be.equal('test') ;
      expect(Suggest.getBoldDiffString('test', 'tet'))
        .to.be.equal('te\u001b[1mt\u001b[22m') ;
      expect(Suggest.getBoldDiffString('test', 'tester'))
        .to.be.equal('test\u001b[1me\u001b[22m\u001b[1mr\u001b[22m') ;
      expect(Suggest.getBoldDiffString('test', 'foo'))
        .to.be.equal(
          '\u001b[1mf\u001b[22m\u001b[1mo\u001b[22m\u001b[1mo\u001b[22m'
        ) ;
      expect(Suggest.getBoldDiffString('test', 'est'))
        .to.be.equal(
          '\u001b[1me\u001b[22m\u001b[1ms\u001b[22m\u001b[1mt\u001b[22m'
        ) ;
    }) ;
  }) ;
}) ;

