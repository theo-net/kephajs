'use strict' ;

/**
 * src/cli/Utils.js
 */

const fs     = require('fs'),
      expect = require('chai').expect ;

const Utils     = require('./Utils'),
      Framework = require('../core/Framework') ;



describe('Utils', function () {

  it('extends framework', function () {

    const framMethods = Reflect.ownKeys(Framework) ;
    let expected = true ;

    framMethods.forEach(method => {

      if (!Reflect.has(Utils, method))
        expected = false ;
    }) ;

    expect(expected).to.be.true ;
  }) ;


  describe('Files methods', function () {

    before(function () {

      // On prépare le terrain
      if (!Utils.fileExists('./tests'))
        fs.mkdirSync('./tests') ;

      fs.mkdirSync('./tests/empty') ;
      fs.mkdirSync('./tests/simple') ;
      fs.mkdirSync('./tests/complex') ;
      fs.mkdirSync('./tests/complex/empty') ;
      fs.mkdirSync('./tests/complex/sub') ;
      fs.mkdirSync('./tests/complex/sub/sub') ;

      fs.writeFileSync('./tests/file', 'foobar') ;
      fs.writeFileSync('./tests/simple/file1', 'foobar') ;
      fs.writeFileSync('./tests/simple/file2', 'foobar') ;
      fs.writeFileSync('./tests/complex/file1', 'foobar') ;
      fs.writeFileSync('./tests/complex/file2', 'foobar') ;
      fs.writeFileSync('./tests/complex/sub/file', 'foobar') ;
      fs.writeFileSync('./tests/complex/sub/sub/file1', 'foobar') ;
      fs.writeFileSync('./tests/complex/sub/sub/file2', 'foobar') ;
    }) ;

    after(function (done) {

      Utils.rmrf('./tests', done) ;
    }) ;


    describe('fileExists', function () {

      it('return `true` if the file exists', function () {

        expect(Utils.fileExists('./tests/file')).to.be.equal(true) ;
      }) ;

      it('return `false` if the file doesn\'t exist', function () {

        expect(Utils.fileExists('./tests/fil')).to.be.equal(false) ;
      }) ;

      it('return `true` if the dir exists', function () {

        expect(Utils.fileExists('./tests/empty/')).to.be.equal(true) ;
      }) ;

      it('return `false` if the dir doesn\'t exist', function () {

        expect(Utils.fileExists('./tests/dir/')).to.be.equal(false) ;
      }) ;
    }) ;


    describe('cpr', function () {

      it('copy a simple file', function (done) {

        Utils.cpr('./tests/file', './tests/file_copy', () => {
          expect(Utils.fileExists('./tests/file_copy')).to.be.true ;
          expect(fs.readFileSync('./tests/file'))
            .to.be.deep.equal(fs.readFileSync('./tests/file_copy')) ;
          done() ;
        }) ;
      }) ;

      it('copy an empty directory', function (done) {

        Utils.cpr('./tests/empty', './tests/empty_copy', () => {
          expect(Utils.fileExists('./tests/empty_copy')).to.be.true ;
          done() ;
        }) ;
      }) ;

      it('copy a directory with files', function (done) {

        Utils.cpr('./tests/simple', './tests/simple_copy', () => {
          expect(Utils.fileExists('./tests/simple_copy/file1')).to.be.true ;
          expect(Utils.fileExists('./tests/simple_copy/file2')).to.be.true ;
          expect(fs.readFileSync('./tests/simple/file1'))
            .to.be.deep.equal(fs.readFileSync('./tests/simple_copy/file1')) ;
          done() ;
        }) ;
      }) ;

      it('copy a complex directory', function (done) {

        Utils.cpr('./tests/complex', './tests/complex_copy', () => {
          expect(Utils.fileExists('./tests/complex_copy/file1')).to.be.true ;
          expect(Utils.fileExists('./tests/complex_copy/sub/file')).to.be.true ;
          expect(Utils.fileExists('./tests/complex_copy/sub/sub/file1'))
            .to.be.true ;
          done() ;
        }) ;
      }) ;
    }) ;


    describe('rmrf', function () {

      it('remove a file', function (done) {

        Utils.rmrf('./tests/file', () => {
          expect(Utils.fileExists('./tests/file')).to.be.false ;
          done() ;
        }) ;
      }) ;

      it('remove an empty dir', function (done) {

        Utils.rmrf('./tests/empty', () => {
          expect(Utils.fileExists('./tests/empty')).to.be.false ;
          done() ;
        }) ;
      }) ;

      it('remove a dir with files', function (done) {

        Utils.rmrf('./tests/simple', () => {
          expect(Utils.fileExists('./tests/simple')).to.be.false ;
          done() ;
        }) ;
      }) ;

      it('remove a dir with subdir and not empty subdir', function (done) {

        Utils.rmrf('./tests/complex', () => {
          expect(Utils.fileExists('./tests/complex')).to.be.false ;
          done() ;
        }) ;
      }) ;
    }) ;
  }) ;

}) ;

