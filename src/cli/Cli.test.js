'use strict' ;

/**
 * src/cli/Cli.test.js
 */

const expect = require('chai').expect,
      sinon  = require('sinon') ;

const Cli         = require('./Cli'),
      Command     = require('./Cli/Command'),
      Color       = require('./Color'),
      Table       = require('./Table/Table'),
      Application = require('../core/Application'),
      ParseArgs   = require('./Cli/ParseArgs'),
      Help        = require('./Cli/Help'),
      K           = require('./Utils'),
      Logs        = require('../core/Services/Logs') ;

let cli, exit, log ;

// buddy ignore:start
describe('Cli', function () {

  before(function () {

    exit = sinon.stub(process, 'exit').withArgs(2) ;
  }) ;

  after(function () {

    process.exit.restore() ;
  }) ;

  beforeEach(function () {

    cli = new Cli() ;
  }) ;


  it('étend Application', function () {

    expect(cli).to.be.instanceof(Application) ;
  }) ;

  it('rend disponible le framework', function () {

    expect(cli.K).to.be.equal(K) ;
  }) ;

  it('enregistre des services', function () {

    expect(cli.kernel().container().has('$color')).to.be.true ;
    expect(cli.kernel().container().get('$color')).to.be.instanceOf(Color) ;
    expect(cli.kernel().container().has('$logs')).to.be.true ;
    expect(cli.kernel().container().get('$logs')).to.be.instanceOf(Logs) ;
  }) ;

  it('renvoit le service color', function () {

    expect(cli.color()).to.be.an.instanceof(Color) ;
  }) ;

  it('gestion de l\'aide', function () {

    expect(cli._help).to.be.instanceof(Help) ;
    expect(cli._help._cli).to.be.equal(cli) ;
  }) ;

  it('parser enregistré', function () {

    expect(cli.parseArgs).to.be.instanceof(ParseArgs) ;
  }) ;

  describe('newTable', function () {

    it('retourne une nouvelle instance de `Table`', function () {

      expect(cli.newTable()).to.be.an.instanceof(Table) ;
    }) ;

    it('transmet les options au tableau', function () {

      let table = cli.newTable({style: {border: ['foo'], head: ['bar']}}) ;

      expect(table._options.style.border[0]).to.equal('foo') ;
      expect(table._options.style.head[0]).to.equal('bar') ;
    }) ;

    it('Demande le style `noBorder`', function () {

      let table = cli.newTable(null, 'noBorder') ;

      expect(table._options.chars).to.be.deep.equal({
        bottom: '', 'bottom-left': '', 'bottom-mid': '', 'bottom-right': '',
        left: '', 'left-mid': '', mid: '', 'mid-mid': '', middle: '',
        right: '', 'right-mid': '',
        top: '', 'top-left': '', 'top-mid': '', 'top-right': ''
      }) ;
    }) ;
  }) ;


  describe('description', function () {

    it('définit la description de l\'application', function () {

      cli.description('Ma superbe app') ;
      expect(cli._description).to.be.equal('Ma superbe app') ;
    }) ;

    it('si une description est définit, retourne l\'app', function () {

      expect(cli.description('Une app')).to.be.equal(cli) ;
    }) ;

    it('retourne la description de l\'application', function () {

      cli.description('Une app') ;
      expect(cli.description()).to.be.equal('Une app') ;
    }) ;
  }) ;

  describe('command', function () {

    it('créé une nouvelle commande et la retourne', function () {

      let command = cli.command('test', 'un test') ;

      expect(cli._commands.slice()[0]).to.be.equal(command) ;
    }) ;
  }) ;


  describe('action', function () {

    beforeEach(function () {

      cli = new Cli() ;
    }) ;

    it('ajoute l\'action à la commande par défaut', function () {

      cli._defaultCommand = cli.command('test', 'lol') ;
      let fct = function () {} ;
      cli.action(fct) ;

      expect(cli._defaultCommand._action).to.be.equal(fct) ;
    }) ;

    it('créé une commande par défaut si besoin', function () {

      let fct = function () {} ;
      cli.action(fct) ;

      expect(cli._defaultCommand).to.be.instanceof(Command) ;
      expect(cli._defaultCommand._action).to.be.equal(fct) ;
    }) ;

    it('retourne l\'application', function () {

      expect(cli.action(function () {})).to.be.equal(cli) ;
    }) ;
  }) ;


  describe('run', function () {

    before(function () {

      log = sinon.stub(console, 'log') ;
    }) ;

    after(function () {

      console.log.restore() ;
    }) ;

    beforeEach(function () {

      cli = new Cli() ;
      cli.command('test') ;
    }) ;

    it('appel help si demandé ou si la commande demandée n\'existe pas',
    function () {

      sinon.spy(cli, 'help') ;

      cli.run(['truc']) ;
      expect(cli.help.calledOnce).to.be.true ;
      cli.command('order') ;
      cli.run(['order']) ;
      expect(cli.help.calledOnce).to.be.true ;
      cli.run(['--help']) ;
      expect(cli.help.calledTwice).to.be.true ;
      cli.run(['-h']) ;
      expect(cli.help.calledThrice).to.be.true ;
      cli.run(['help', 'arg1', 'arg2', '-o']) ;
      expect(cli.help.calledWith('arg1 arg2')).to.be.true ;
    }) ;

    it('print version of script', function () {

      cli.run(['-V']) ;
      expect(log.calledWith(cli.version())).to.be.true ;
      cli.run(['--version']) ;
      expect(log.calledWith(cli.version())).to.be.true ;
    }) ;

    it('set quiet mode', function () {

      cli.run(['test', '--quiet']) ;
      expect(cli.kernel().get('$config').get('logLevel')).to.be.equal('warn') ;
      cli.run(['test', '--silent']) ;
      expect(cli.kernel().get('$config').get('logLevel')).to.be.equal('warn') ;
    }) ;

    it('set verbose mode', function () {

      cli.run(['test', '-v']) ;
      expect(cli.kernel().get('$config').get('logLevel')).to.be.equal('debug') ;
      cli.run(['test', '--verbose']) ;
      expect(cli.kernel().get('$config').get('logLevel')).to.be.equal('debug') ;
    }) ;

    it('set no-color mode', function () {

      cli.run(['test']) ;
      expect(cli.color().blue('test')).to.be.equal('\u001b[34mtest\u001b[39m') ;
      cli.run(['test', '--no-color']) ;
      expect(cli.color().blue('test')).to.be.equal('test') ;
      // On rétabli les couleurs
      cli.color().enableColor(true) ;
      expect(cli.color().blue('test')).to.be.equal('\u001b[34mtest\u001b[39m') ;
    }) ;
  }) ;

  describe('help', function () {

    before(function () {

      sinon.stub(console, 'log') ;
    }) ;

    after(function () {

      console.log.restore() ;
    }) ;

    beforeEach(function () {

      cli = new Cli() ;
      sinon.spy(cli._help, 'display') ;
    }) ;

    it('appel Help.display() avec la bonne commande', function () {

      let cmd = cli.command('test', 'un test') ;

      cli.run(['help', 'test']) ;
      expect(cli._help.display.calledWithExactly(cmd)).to.be.true ;
    }) ;

    it('fonctionne avec un alias', function () {

      let cmd = cli.command('test', 'un test') ;
      cmd.alias('lol') ;

      cli.run(['help', 'lol']) ;
      expect(cli._help.display.lastCall.calledWithExactly(cmd)).to.be.true ;
    }) ;

    it('si la commande n\'existe pas : undefined', function () {

      cli.run(['help', 'mince']) ;
      expect(cli._help.display.lastCall.calledWithExactly(undefined))
        .to.be.true ;
    }) ;

    it('si aucune commande n\'est demandée : undefined', function () {

      cli.run(['help']) ;
      expect(cli._help.display.lastCall.calledWithExactly(undefined))
        .to.be.true ;
    }) ;
  }) ;


  describe('fatalError', function () {

    before(function () {

      sinon.stub(console, 'log') ;
    }) ;

    after(function () {

      console.log.restore() ;
    }) ;

    it('called `process.exit(2)`', function () {

      exit.reset() ;
      cli.fatalError(new Error('test')) ;
      expect(exit.called).to.be.true ;
    }) ;
  }) ;
}) ;

// buddy ignore:end

