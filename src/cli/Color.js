'use strict' ;

/**
 * src/cli/Color.js
 */

/*
 Inspiré par la librairie `chalk 1.1.3`
   Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)
*/

const Utils = require('../core/Utils') ;


/**
 * Objet permettant personnaliser le rendu de notre sortie dans la console.
 *
 * L'utilisation est très simple :
 *
 *     const color = new Color() ;
 *
 *     console.log(color.blue.bold('Hello World!') ;
 *
 * Lorsque des styles sont combinés, on commence toujours par appliquer le
 * dernier de la liste.
 *
 * Vous pouvez ajouter des styles :
 *
 *     color.addStyle('test', text => { return text + '!' ;}) ;
 *     color.addStyle('theme', text => { return color.yellow.bold(text) ;});
 *
 *     console.log(color.theme.text('Brice de Nice')) ;
 */
class Color {

  // buddy ignore:start

  /**
   * Créé un nouvel objet
   * Si `enable` n'est pas donné, les colors seront appliqués si la console
   * les supporte.
   *
   * @param {boolean} [enable] On désactive le support des couleurs
   * @return {void}
   */
  constructor (enable) {

    this._enabled = this.supportsColor(enable) ;
    this._styles = {} ;
    this._proto = function () {} ;

    let styles = {
      reset: [0, 0],
      bold: [1, 22],
      dim: [2, 22],
      italic: [3, 23],
      underline: [4, 24],
      inverse: [7, 27],
      hidden: [8, 28],
      strikethrough: [9, 29],
      black: [30, 39],
      red: [31, 39],
      green: [32, 39],
      yellow: [33, 39],
      blue: [34, 39],
      magenta: [35, 39],
      cyan: [36, 39],
      white: [37, 39],
      grey: [90, 39],
      bgBlack: [40, 49],
      bgRed: [41, 49],
      bgGreen: [42, 49],
      bgYellow: [43, 49],
      bgBlue: [44, 49],
      bgMagenta: [45, 49],
      bgCyan: [46, 49],
      bgWhite: [47, 49]
    } ;
    // On créé tous les styles
    Utils.forEach(styles, (style, name) => {
      this._styles[name] = this._createStyle(style[0], style[1]) ;
    }, this) ;


    // On ajoute les styles comme des méthodes
    // Il y a deux manière de construire notre méthode :
    //   - comme ici quand nous travaillons avec Color
    //   - avec _setProto() pour les objets retournés par les styles
    let ret = {} ;
    Utils.forEach(this._styles, (style, name) => {
      ret[name] = {
        get: () => {
          return this._build([name]) ;
        }
      } ;
    }) ;

    // On rend accessible les styles
    Object.defineProperties(this, ret) ;
    this._setProto(this._styles) ;
  }


  /**
   * Indique si le terminal supporte les couleurs
   * Le paramètre `enable` permet de forcer le résultat. On peut également
   * forcer le support en définissant la variable d'environnement `FORCE_COLOR`
   * ou avec `COLORTERM`
   *
   * @param {boolean} enable Force le résultat
   * @return {boolean}
   */
  supportsColor (enable) {

    if (enable === false)
      return false ;

    if ('FORCE_COLOR' in process.env)
      return true ;

    if (enable === true)
      return true ;

    if (process.stdout && !process.stdout.isTTY)
      return false ;

    if (process.platform === 'win32')
      return true ;

    if ('COLORTERM' in process.env)
      return true ;

    if (process.env.TERM === 'dumb')
      return false ;

    if (
      /^screen|^xterm|^vt100|color|ansi|cygwin|linux/i.test(process.env.TERM)
    )
      return true ;

    return false ;
  }


  /**
   * Active ou désactive le support des couleurs
   *
   * @param {Boolean} enable .
   */
  enableColor (enable) {

    this._enabled = enable ;
  }


  /**
   * Ajoute un style
   *
   * @param {String} name Nom du style
   * @param {Function} style Le style à ajouter
   * @throws Lève une exception si `name` correspond à un style existant ou
   *         une méthode de notre objet.
   * }
   */
  addStyle (name, style) {

    if (this.hasOwnProperty(name) || Color.prototype.hasOwnProperty(name)) {
      throw 'Can\'t has `' + name + '` style because a style already exist '
        + 'or is a forbidden name' ;
    }
    else {

      this._styles[name] = style ;

      // On rend accessible le style
      let ret = {
        get: () => {
          return this._build([name]) ;
        }
      } ;
      Object.defineProperty(this, name, ret) ;
      this._setProto({[name]: style}) ;
    }
  }


  /**
   * Helper pour la coloration sur plusieurs lignes
   * En effet, lors de l'affichage, une cellule sur plusieurs lignes doit avoir
   * une unique coloration, sans cet helper, celle-ci serait perturbée par les
   * cellules voisines.
   *
   * @param {Array} input Lignes à coloriser
   * @returns {Array}
   * @static
   */
  static colorizeLines (input) {

    let output = [] ;
    let state = {} ;


    input.forEach(row => {
      let line = this._rewindState(state, row) ;
      state = this._readState(line) ;
      let temp = Utils.extend({}, state) ;
      output.push(this._unwindState(temp, line)) ;
    }) ;

    return output ;
  }


  /**
   * Retourne une RegExp pour détecter les codes couleurs
   *
   * @param {Boolean} [capture] Capture le code
   * @returns {RegExp}
   * @static
   */
  static codeRegex (capture) {

    return capture ?
      /\u001b\[((?:\d*;){0,5}\d*)m/g : /\u001b\[(?:\d*;){0,5}\d*m/g ; // eslint-disable-line no-control-regex
  }


  /**
   * Créé un style que l'on pourra ajouter
   *
   * @param {Integer} open Code du début
   * @param {Integer} close Code de fin
   * @private
   * @returns {Function}
   */
  _createStyle (open, close) {

    let style = (text) => {

      if (this._enabled)
        text = '\u001b[' + open + 'm' + text + '\u001b[' + close + 'm' ;
      return text ;
    } ;

    return style ;
  }

  /**
   * Construit une fonction de stylisation, c'est celle-ci qui permet d'accéder
   * aux styles à la fois comme des fonctions (pour les appliquer à un texte)
   * et comme des méthode (pour chaîner des styles).
   *
   * @param {Array} styles Styles appliqués
   * @returns {Function}
   * @private
   */
  _build (styles) {

    let self = this ;

    let builder = function builder (text) {
      return self._applyStyle.call(builder, text, self._styles, self._enabled) ;
    } ;
    builder._styles = styles ;
    builder.__proto__ = self._proto ;

    return builder ;
  }

  /**
   * Applique le(s) style(s)
   *
   * @param {String} text Texte à formater
   * @param {Object} styles Styles à appliquer
   * @param {Boolean} enabled Couleurs activées
   * @returns {String}
   * @private
   */
  _applyStyle (text, styles, enabled) {

    if (!enabled || !text)
      return text ;

    Utils.forEachRight(this._styles, name => {
      text = styles[name](text) ;
    }) ;

    return text ;
  }


  /**
   * Défini le prototype que sera utilisé par les styles
   *
   * @param {Object} styles Liste des styles à ajouter
   */
  _setProto (styles) {

    let self = this ;

    let stylesProto = (() => {
      let ret = {} ;
      Object.keys(styles).forEach(name => {
        ret[name] = {
          get: function () {
            return self._build(this._styles.concat(name)) ;
          },
        } ;
      }) ;
      return ret ;
    })() ;

    Object.defineProperties(this._proto, stylesProto) ;
  }


  static _getCodeCache () {

    if (this._codeCache !== undefined)
      return this._codeCache ;

    // Code cache pour certains utilitaires
    let codeCache = {} ;
    function addToCodeCache (name, on, off) {
      on = '\u001b[' + on + 'm' ;
      off = '\u001b[' + off + 'm' ;
      codeCache[on] = {set: name, to: true} ;
      codeCache[off] = {set: name, to: false} ;
      codeCache[name] = {on: on, off: off} ;
    }
    addToCodeCache('bold', 1, 22) ;
    addToCodeCache('italics', 3, 23) ;
    addToCodeCache('underline', 4, 24) ;
    addToCodeCache('inverse', 7, 27) ;
    addToCodeCache('strikethrough', 9, 29) ;

    return this._codeCache = codeCache ;
  }

  static _rewindState (state, ret) {

    let lastBackgroundAdded = state.lastBackgroundAdded ;
    let lastForegroundAdded = state.lastForegroundAdded ;

    delete state.lastBackgroundAdded ;
    delete state.lastForegroundAdded ;

    Utils.forEach(state, (value, key) => {
      if (value)
        ret = this._getCodeCache()[key].on + ret ;
    }) ;

    if (lastBackgroundAdded && (lastBackgroundAdded != '\u001b[49m'))
      ret = lastBackgroundAdded + ret ;
    if (lastForegroundAdded && (lastForegroundAdded != '\u001b[39m'))
      ret = lastForegroundAdded + ret ;

    return ret ;
  }

  static _readState (line) {

    let code = this.codeRegex(true) ;
    let controlChars = code.exec(line) ;
    let state = {} ;

    while (controlChars !== null) {
      this._updateState(state, controlChars) ;
      controlChars = code.exec(line) ;
    }

    return state ;
  }

  static _updateState (state, controlChars) {

    let controlCode = controlChars[1] ?
      parseInt(controlChars[1].split(';')[0]) : 0 ;

    if ((controlCode >= 30 && controlCode <= 39) ||
        (controlCode >= 90 && controlCode <= 97)) {

      state.lastForegroundAdded = controlChars[0] ;
      return ;
    }
    if ((controlCode >= 40 && controlCode <= 49) ||
        (controlCode >= 100 && controlCode <= 107)) {

      state.lastBackgroundAdded = controlChars[0] ;
      return ;
    }

    if (controlCode === 0) {
      for (let i in state) {
        if (state.hasOwnProperty(i))
          delete state[i] ;
      }
      return ;
    }

    let info = this._getCodeCache()[controlChars[0]] ;
    if (info)
      state[info.set] = info.to ;
  }

  static _unwindState (state, ret) {

    let lastBackgroundAdded = state.lastBackgroundAdded ;
    let lastForegroundAdded = state.lastForegroundAdded ;

    delete state.lastBackgroundAdded ;
    delete state.lastForegroundAdded ;

    Utils.forEach(state, (value, key) => {
      if (value)
        ret += this._getCodeCache()[key].off ;
    }) ;

    if (lastBackgroundAdded && (lastBackgroundAdded != '\u001b[49m'))
      ret += '\u001b[49m' ;
    if (lastForegroundAdded && (lastForegroundAdded != '\u001b[39m'))
      ret += '\u001b[39m' ;

    return ret ;
  }

  // buddy ignore:end
}

module.exports = Color ;

