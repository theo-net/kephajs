'use strict' ;

/**
 * src/cli/Color.test.js
 */

const expect = require('chai').expect,
      sinon  = require('sinon') ;

const Color = require('./Color') ;
const Utils = require('../core/Utils') ;

let color = new Color() ;

// buddy ignore:start
describe('Color', function () {

  describe('supportsColor', function () {

    it('peut forcer le résultat à false', function () {

      expect(color.supportsColor(false)).to.equal(false) ;
    }) ;

    it('peut forcer le résultat à true', function () {

      expect(color.supportsColor(true)).to.equal(true) ;
    }) ;
  }) ;

  describe('enableColor', function () {

    it('peut désactiver le support des couleurs', function () {

      color.enableColor(false) ;
      expect(color._enabled).to.equal(false) ;
    }) ;

    it('peut activer le support des couleurs', function () {

      color.enableColor(true) ;
      expect(color._enabled).to.equal(true) ;
    }) ;
  }) ;

  describe('addStyle', function () {

    it('déclenche une erreur si `name` existe déjà', function () {

      expect(color.addStyle.bind(color, 'black', () => {})).to.throw() ;
    }) ;

    it('déclenche une erreur si `name` n\'est pas valide', function () {

      expect(color.addStyle.bind(color, 'addStyle', () => {})).to.throw() ;
    }) ;

    it('définie une méthode permettant d\'appliquer le style',
    function () {

      let spy = sinon.spy() ;
      color.addStyle('aStyle', spy) ;
      color.aStyle('a text') ;
      expect(spy.calledOnce).to.equal(true) ;
    }) ;

    it('la fonction définie sera appelée avec le paramètre passé à la méthode',
    function () {

      let spy = sinon.spy() ;
      color.addStyle('secondStyle', spy) ;
      color.secondStyle('a text') ;
      expect(spy.calledWith('a text')).to.equal(true) ;
    }) ;
  }) ;

  describe('_createStyle', function () {

    it('retourne une fonction', function () {

      expect(typeof color._createStyle()).to.equal('function') ;
    }) ;

    it('fonction qui transforme un texte en ajoutant des balises', function () {

      let spy = sinon.spy(color._createStyle('azerty', 'qwerty')) ;
      let result = spy('a text') ;

      expect(result).to.equal('\u001b[azertyma text\u001b[qwertym') ;
      expect(spy.withArgs('a text').calledOnce).to.true ;
    }) ;
  }) ;

  describe('styles', function () {

    let styles = {
      reset: [0, 0],
      bold: [1, 22],
      dim: [2, 22],
      italic: [3, 23],
      underline: [4, 24],
      inverse: [7, 27],
      hidden: [8, 28],
      strikethrough: [9, 29],
      black: [30, 39],
      red: [31, 39],
      green: [32, 39],
      yellow: [33, 39],
      blue: [34, 39],
      magenta: [35, 39],
      cyan: [36, 39],
      white: [37, 39],
      grey: [90, 39],
      bgBlack: [40, 49],
      bgRed: [41, 49],
      bgGreen: [42, 49],
      bgYellow: [43, 49],
      bgBlue: [44, 49],
      bgMagenta: [45, 49],
      bgCyan: [46, 49],
      bgWhite: [47, 49]
    } ;

    Utils.forEach(styles, (style, name) => {
      it(name, function () {
        expect(color[name]('a text'))
          .to.equal('\u001b[' + style[0] + 'ma text\u001b[' + style[1] + 'm') ;
      }) ;
    }) ;

    it('disable', function () {

      color.enableColor(false) ;
      expect(color.blue('a text')).to.equal('a text') ;
      color.enableColor(true) ;
    }) ;

    it('combined', function () {

      expect(color.bold.inverse('a text'))
        .to.equal('\u001b[1m\u001b[7ma text\u001b[27m\u001b[22m') ;
    }) ;

    it('combined (multiple)', function () {

      expect(color.bold.inverse.blue('a text'))
        .to.equal(
          '\u001b[1m\u001b[7m\u001b[34ma text\u001b[39m\u001b[27m\u001b[22m'
        ) ;
    }) ;

    it('combined (with perso style)', function () {

      color.addStyle('perso', text => { return text + '!' ; }) ;
      expect(color.bold.perso('a text'))
        .to.equal('\u001b[1ma text!\u001b[22m') ;
    }) ;
  }) ;

  describe('colorizeLines', function () {

    it('foreground colors continue on each line', function () {

      let input = color.red('Hello\nHi').split('\n') ;

      expect(Color.colorizeLines(input)).to.eql([
        color.red('Hello'),
        color.red('Hi')
      ]) ;
    }) ;

    it('background colors continue on each line', function () {

      let input = color.bgRed('Hello\nHi').split('\n') ;

      expect(Color.colorizeLines(input)).to.eql([
        color.bgRed('Hello'),
        color.bgRed('Hi')
      ]) ;
    }) ;

    it('styles will continue on each line', function () {

      let input = color.underline('Hello\nHi').split('\n') ;

      expect(Color.colorizeLines(input)).to.eql([
        color.underline('Hello'),
        color.underline('Hi')
      ]) ;
    }) ;

    it('styles that end before the break will not be applied to the next line',
    function () {

      let input = (color.underline('Hello') + '\nHi').split('\n') ;

      expect(Color.colorizeLines(input)).to.eql([
        color.underline('Hello'),
        'Hi'
      ]) ;
    }) ;

    it('the reset code can be used to drop styles', function () {

      let input = '\x1b[31mHello\x1b[0m\nHi'.split('\n') ;
      expect(Color.colorizeLines(input)).to.eql([
        '\x1b[31mHello\x1b[0m',
        'Hi'
      ]) ;
    }) ;

    it('handles aixterm 16-color foreground', function () {

      let input = '\x1b[90mHello\nHi\x1b[0m'.split('\n') ;
      expect(Color.colorizeLines(input)).to.eql([
        '\x1b[90mHello\x1b[39m',
        '\x1b[90mHi\x1b[0m'
      ]) ;
    }) ;

    it('handles aixterm 16-color background', function () {

      let input = '\x1b[100mHello\nHi\x1b[m\nHowdy'.split('\n') ;
      expect(Color.colorizeLines(input)).to.eql([
        '\x1b[100mHello\x1b[49m',
        '\x1b[100mHi\x1b[m',
        'Howdy'
      ]) ;
    }) ;

    it('handles aixterm 256-color foreground', function () {

      let input = '\x1b[48;5;8mHello\nHi\x1b[0m\nHowdy'.split('\n') ;
      expect(Color.colorizeLines(input)).to.eql([
        '\x1b[48;5;8mHello\x1b[49m',
        '\x1b[48;5;8mHi\x1b[0m',
        'Howdy'
      ]) ;
    }) ;

    it('handles CJK chars', function () {

      let input = color.red('漢字\nテスト').split('\n') ;

      expect(Color.colorizeLines(input)).to.eql([
        color.red('漢字'),
        color.red('テスト')
      ]) ;
    }) ;
  }) ;

}) ;
// buddy ignore:end

