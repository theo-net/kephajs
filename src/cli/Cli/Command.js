'use strict' ;

/**
 * src/cli/Cli/Command.js
 */

const Argument = require('./Argument'),
      Option = require('./Option'),
      Utils = require('../../core/Utils'),
      InvalidArgumentValueError  = require('../Error/InvalidArgumentValue'),
      InvalidOptionValueError    = require('../Error/InvalidOptionValue'),
      MissingOptionError         = require('../Error/MissingOption'),
      NoActionError              = require('../Error/NoAction'),
      UnknownOptionError         = require('../Error/UnknownOption'),
      WrongNumberOfArgumentError = require('../Error/WrongNumberOfArgument') ;

class Command {

  /**
   * Créé une nouvelle commande
   *
   * @param {String|null} name Nom de la commande
   * @param {String} description Description de la commande
   * @param {Application} application Instance de l'application.
   */
  constructor (name, description, application) {


    this._name = name ;
    this._description = description ;
    this._application = application ;
    this._alias = null ;

    this._args = [] ;
    this._options = [] ;
  }

  /**
   * Défini un alias pour cette commande. Un seul alias est possible par
   * commande.
   *
   * @param {String} alias Alias
   * @return {Command}
   * @public
   */
  alias (alias) {

    this._alias = alias ;

    return this ;
  }

  /**
   * Ajoute un argument
   *
   * @param {String} synopsis Synopsis de l'argument comme `<mon-argument>` ou
   *        `[mon-argument]`. `<>` indiquent un argument requis et `[]` un
   *        argument optionnel.
   * @param {String} description Description de l'option ou de l'argument
   * @param {String|RegExp|Function|Number} [validator] Option validator, used
   *        for checking or casting.
   * @param {*} [defaultValue] Valeur par défaut
   * @public
   * @returns {Command}
   */
  argument (synopsis, description, validator, defaultValue) {

    const arg = new Argument(synopsis, description, validator, defaultValue,
                             this._application) ;

    this._args.push(arg) ;

    return this ;
  }


  /**
   * Ajoute une option
   *
   * @param {String} synopsis Synopsis de l'option comme `-f, --force`
   *        ou `-f, --file <file>` ou `--hello [path]`
   * @param {String} description Description de l'option
   * @param {String|RegExp|Function|Number} [validator] Validateur
   * @param {*} defaultValue Valeur par défaut
   * @param {Boolean} [required] L'option est-elle nécessaire
   * @public
   * @returns {Command}
   */
  option (synopsis, description, validator, defaultValue, required) {

    const opt = new Option(synopsis, description, validator, defaultValue,
                           required, this._application) ;
    this._options.push(opt) ;

    return this ;
  }


  /**
   * Retourne les options
   * @returns {Array}
   */
  getOptions () {

    return this._options ;
  }


  /**
   * Définie l'action correspondante à la commande
   *
   * @param {Function} action Action à exécuter
   * @returns {Command}
   * @public
   */
  action (action) {

    this._action = action ;
    return this ;
  }


  /**
   * Permet d'enchaîner une méthode `command()` avec une autre `.command()`
   * @returns {Command}
   */
  command () {

    return this._application.command.apply(this._application, arguments) ;
  }

  /**
   * Retourne le nom de la commande ou une chaîne vide s'il s'agit de la
   * commande par défaut.
   * @returns {String}
   */
  name () {

    return this._name === '_default' ? '' : this._name ;
  }

  /**
   * Retourne l'alias de la commande
   * @returns {String}
   * @private
   */
  getAlias () {

    return this._alias ;
  }


  /**
   * Retourne le synopsis
   * @returns {String}
   * @private
   */
  getSynopsis () {

    return this.name() + ' ' + (this.args().map(a => a.synopsis).join(' ')) ;
  }


  /**
   * @param {Integer} index Index de l'argument
   * @private
   * @returns {Argument[]}
   */
  args (index) {

    return typeof index !== 'undefined' ? this._args[index] : this._args ;
  }


  /**
   * Valide les arguments et les options
   * @param {Array} args Arguments à valider
   * @param {Object} options Options à valider
   * @returns {*}
   * @private
   */
  _validateCall (args, options) {

    // vérifie que le bon nombre d'arguments est transmis
    this._checkArgsRange(args) ;

    // Récupère les arguments dans un tableau : chaque élément est un arguments
    // ou un tableau contenant la liste des valeurs (variadic)
    args = this.args().reduce((acc, arg) => {
      if (arg.isVariadic())
        acc.push(args.slice()) ;
      else
        acc.push(args.shift()) ;
      return acc ;
    }, []) ;

    // Transfrome le tableau en objet, et récupère si besoin les valeurs par
    // défauts
    args = this.args().reduce((acc, arg, index) => {
      if (typeof args[index] !== 'undefined')
        acc[arg.name()] = args[index] ;
      else if (arg.hasDefault())
        acc[arg.name()] = args.default() ;
      return acc ;
    }, {}) ;

    // Vérifie la valeur des arguments
    args = this._validateArgs(args) ;

    // On valide les options
    options = this._checkRequiredOptions(options) ;
    options = this._validateOptions(options) ;

    // On nettoie les options
    options = this._addLongNotationToOptions(options) ;
    options = this._camelCaseOptions(options) ;


    return {args, options} ;
  }


  /**
   * Vérifie si le bon nombre d'arguments est présent. Lance une exception
   * sinon
   *
   * @param {Array} args Arguments à tester
   * @throws {WrongNumberOfArgumentError}
   * @private
   */
  _checkArgsRange (args) {

    // On récupère le nombre minimum et maximum d'arguments requis
    const min = this.args().filter(a => a.isRequired()).length ;
    const max = (this.args().find(a => a.isVariadic()) !== undefined) ?
      Infinity : (min + this.args().filter(a => a.isOptional()).length) ;

    // Nombre actuel d'argument
    const count = args.length ;

    if (count < min || count > max) {

      throw new WrongNumberOfArgumentError(
        'Wrong number of argument(s)'
        + (this.name() ? ' for command ' + this.name() : '')
        + '. Got ' + count + ', expected '
        + (min === max ? 'exactly ' + min : 'between ' + min + ' and ' + max)
        + '.',
        {}, this._application
      ) ;
    }
  }


  /**
   * Valide les arguments
   *
   * @param {Object} args Arguments à tester
   * @returns {Object} Arguments qui seront transmis à la commande
   * @throws {InvalidArgumentValueError}
   * @private
   */
  _validateArgs (args) {

    return Object.keys(args).reduce((acc, key) => {

      const arg = this._args.find(a => a.name() === key) ;
      const value = args[key] ;

      if (!arg.isValid(value)) {
        throw new InvalidArgumentValueError(arg, value, this,
          this._application) ;
      }
      else
        acc[key] = value ;

      return acc ;
    }, {}) ;
  }


  /**
   * Vérifie si les options requises ont été transmises. Si une option est
   * requise, mais n'a pas été transmise : si une valeur par défaut existe, on
   * prendra cette valeur, sinon une exception sera levée.
   *
   * @param {Object} options Options transmises
   * @throws {MissingOptionError}
   * @returns {Array}
   * @private
   */
  _checkRequiredOptions (options) {

    return this._options.reduce((acc, opt) => {

      if (typeof acc[opt.getLongCleanName()] === 'undefined' &&
          typeof acc[opt.getShortCleanName()] === 'undefined') {

        if (opt.hasDefault())
          acc[opt.getLongCleanName()] = opt.default ;
        else if (opt.isRequired()) {
          throw new MissingOptionError(opt.getLongOrShortCleanName(),
            this, this._application) ;
        }
      }

      return acc ;
    }, options) ;
  }


  /**
   * Vérifie la valeur des options
   * @param {Object} options Options à vérifier
   * @throws UnknownOptionError
   * @throws InvalidOptionError
   * @returns {Object} Options traitées
   * @private
   */
  _validateOptions (options) {

    return Object.keys(options).reduce((acc, key) => {

      if (Command.NATIVE_OPTIONS.indexOf(key) !== -1)
        return acc ;

      const value = acc[key] ;
      const opt = this._findOption(key) ;

      if (!opt)
        throw new UnknownOptionError(key, this, this._application) ;

      if (!opt.isValid(value))
        throw new InvalidOptionValueError(opt, value, this, this._application) ;
      else
        acc[key] = value ;

      return acc ;
    }, options) ;
  }


  /**
   * Ajoute la notation longue si elle existe.
   * @param {Object} options Options transmises
   * @returns {Object}
   * @private
   */
  _addLongNotationToOptions (options) {

    return Object.keys(options).reduce((acc, key) => {

      if (key.length === 1) {

        const value = acc[key] ;
        const opt = this._findOption(key) ;
        if (opt && opt.getLongCleanName())
          acc[opt.getLongCleanName()] = value ;
      }
      return acc ;
    }, options) ;
  }


  /**
   * On prend le nom en camelCase pour les options longues et on supprime leur
   * version courte. Celles qui n'ont que la version courte sont conservées.
   * @param {Object} options Options transmises
   * @returns {Object}
   * @private
   */
  _camelCaseOptions (options) {

    return this._options.reduce((acc, opt) => {

      if (!Utils.isUndefined(options[opt.getLongCleanName()]))
        acc[opt.name()] = options[opt.getLongCleanName()] ;
      else if (!Utils.isUndefined(options[opt.getShortCleanName()]))
        acc[opt.name()] = options[opt.getShortCleanName()] ;

      return acc ;
    }, {}) ;
  }


  /**
   * Retourne l'option `name`
   * @param {String} name Nom de l'option (court ou long)
   * @returns {Option}
   * @private
   */
  _findOption (name) {

    return this._options.find(
      o => (o.getShortCleanName() === name || o.getLongCleanName() === name)
    ) ;
  }


  /**
   * @returns {Array}
   * @private
   */
  _getLongOptions () {

    return this._options.map(option => option.getLongCleanName())
                        .filter(option => typeof option !== 'undefined') ;
  }



  /**
   * Exécute la commande
   *
   * @param {Object} args Arguments
   * @param {Object} options Options
   * @throws {NoActionError} Si aucune action n'est associée à la commande
   * @returns {Promise}
   * @private
   */
  _run (args, options) {

    if (!this._action) {

      return this._application.fatalError(new NoActionError(
        'KephaJs Setup Error: You don\'t have defined an action for your '
        + 'program/command. Use .action()', {}, this._application)) ;
    }

    const actionResults = this._action.apply(this, [args, options]) ;
    const response = Promise.resolve(actionResults) ;

    response
      .catch(error => {
        if (!(error instanceof Error))
          error = new Error(error) ;
      }) ;
  }
}

Object.defineProperties(Command, {
  'NATIVE_OPTIONS': {
    value: [
      'h', 'help', 'V', 'version', 'no-color', 'quiet', 'v', 'verbose'
    ]
  }
}) ;

module.exports = Command ;

