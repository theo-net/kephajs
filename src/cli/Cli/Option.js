'use strict' ;

/**
 * src/cli/Cli/Option.js
 */

const ArgOpt = require('./ArgOpt'),
      Utils  = require('../../core/Utils'),
      OptionSyntaxError = require('../Error/OptionSyntax') ;

class Option extends ArgOpt {

  /**
   * @param {String} synopsis Synopsis de l'option
   * @param {String} description Description de l'option
   * @param {String|RegExp|Function|Number} [validator] Validateur
   * @param {*} [defaultValue] Valeur par défaut (undefined si aucune)
   * @param {Boolean} [required] L'option est-elle nécessaire ou facultative
   * @param {Program} [application] Instance de l'application
   */
  constructor (synopsis, description, validator, defaultValue,
    required = false, application) {

    super(synopsis, description, validator, defaultValue, application) ;

    this._required = required ;
    const analysis = this._analyseSynopsis() ;
    this._valueType = analysis.valueType ;
    this._variadic = analysis.variadic ;
    this._longCleanName = analysis.longCleanName ;
    this._shortCleanName = analysis.shortCleanName ;
    this._short = analysis.short ;
    this._long = analysis.long ;

    this._name = this._longCleanName || this._shortCleanName ;
    this._name = Utils.camelCase(
        this._name.replace(/([[\]<>]+)/g, '').replace('...', '')
    ) ;
  }


  /**
   * Analyse le synopsis pour déterminer certaines informations
   *
   * @returns {*}
   * @private
   */
  _analyseSynopsis () {

    // buddy ignore:start

    const info = this.synopsis.split(/[\s\t,]+/).reduce((acc, value) => {

      if (value.substring(0, 2) === '--') {

        acc.long = value ;
        acc.longCleanName = value.substring(2) ;
      } else if (value.substring(0, 1) === '-') {

        acc.short = value ;
        acc.shortCleanName = value.substring(1) ;
      } else if (value.substring(0, 1) === '[') {

        acc.valueType = Option.OPTIONAL_VALUE ;
        acc.variadic = value.substr(-4, 3) === '...' ;
      }
      else if (value.substring(0, 1) === '<') {

        acc.valueType = Option.REQUIRED_VALUE ;
        acc.variadic = value.substr(-4, 3) === '...' ;
      }
      else
        throw new OptionSyntaxError(this.synopsis, this._application) ;

      return acc ;
    }, {}) ;

    return info ;

    // buddy ignore:end
  }


  /**
   * Indique si l'argument est requis
   * @returns {Boolean}
   */
  isRequired () {

    return this._required ;
  }

  getLongCleanName () {

    return this._longCleanName ;
  }

  getShortCleanName () {

    return this._shortCleanName ;
  }

  getLongOrShortCleanName () {

    return this.getLongCleanName() || this.getShortCleanName() ;
  }
}

module.exports = Option ;
module.exports.OPTIONAL_VALUE = 1 ;
module.exports.REQUIRED_VALUE = 2 ;

