'use strict' ;

/**
 * src/cli/Cli/ArgOpt.js
 */

class ArgOpt {

  /**
   * @param {String} synopsis Synopsis de l'argument/option
   * @param {String} description Description de l'option/argument
   * @param {String|RegExp|Function|Number} [validator] Validateur
   * @param {*} [defaultValue] Valeur par défaut
   * @param {Application} application Instance du programme
   */
  constructor (synopsis, description, validator, defaultValue, application) {

    this.synopsis = synopsis ;
    this.description = description ;
    this.default = defaultValue ;
    this._application = application ;

    // Ajout du validateur
    this._validator = validator ?
      application.kernel().get('$validate').make(validator) : null ;
  }


  /**
   * Indique si l'option/argument possède une valeur par défaut
   * @returns {Boolean}
   */
  hasDefault () {

    return this.default !== undefined ;
  }


  /**
   * Retourne le nom de l'option/argument
   * @returns {String}
   */
  name () {

    return this._name ;
  }


  /**
   * Valide la valeur de l'option/argument
   *
   * @param {*} value Valeur à tester
   * @returns {Boolean}
   */
  isValid (value) {

    if (!this._validator)
      return value ;

    return this._validator.isValid(value) ;
  }
}

module.exports = ArgOpt ;

