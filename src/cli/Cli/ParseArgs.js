'use strict' ;

/**
 * src/cli/Cli/ParseArgs.js
 */

const Utils = require('../../core/Utils') ;

/**
 * Parse une série d'argument
 * ['-a', 'foo', '-b', 'bar'] -> {_: [], a: 'foo', b: 'bar'}
 * ['-x', '3', '-y', '4', '-n5', '-abc', '--beep=boop', 'foo', 'bar']
 *   -> {_: ['foo', 'bar'],
 *       x: 3, y: 4, n: 5, a: true, b: true, c: true,
 *       beep: 'boop' }
 *
 * Les options peuvent êtres:
 *  - `opts.stopEarly` si `true` remplie `argv._` avec tout ce qu'il y a
 *    après le premier élément qui n'est pas une option
 *  - `opts['--']` si `true`, tout ce qui sera après `--` sera dans
 *    `argv['--']`
 *
 * Repris de minimist James Halliday <mail@substack.net>
 */
class ParseArgs {


  constructor () {

    this._opts = {} ;
  }


  /**
   * Définie des options, écrase toutes les options déjà enregistrées.
   *
   * @param {Object} opts Options à ajouter
   * @throws {TypeError} Si `opts` n'est pas un objet
   */
  setOpts (opts) {

    if (!Utils.isObject(opts))
      throw new TypeError('`opts` is not `Object`') ;

    this._opts = opts ;
  }


  /**
   * Parse des arguments
   *
   * @param {Array} args Arguments
   * @return {Array}
   */
  parse (args) {

    this._argv = {_: []} ;

    // On ignore tout après '--'
    let notFlags = [] ;
    if (args.indexOf('--') !== -1) {
      notFlags = args.slice(args.indexOf('--') + 1) ;
      args = args.slice(0, args.indexOf('--')) ;
    }


    let key = '' ;
    // On parcours la liste des arguments
    for (let i = 0 ; i < args.length ; i++) {

      let arg = args[i] ;
      let nextArg = args[i + 1] ;
      let broken = true ;

      // --*=*
      if (/^--.+=/.test(arg)) {

        let match = arg.match(/^--([^=]+)=([\s\S]*)$/) ;
        key = match[1] ;
        let value = match[2] ;

        this._setArg(key, value) ;
        continue ;
      }
      // --no-*
      else if (/^--no-.+/.test(arg)) {
        key = arg.match(/^--no-(.+)/)[1] ;
        this._setArg(key, false) ;
        continue ;
      }
      // --*
      else if (/^--.+/.test(arg)) {

        key = arg.match(/^--(.+)/)[1] ;
        let next = args[i + 1] ;

        if (next !== undefined && !/^-/.test(next)) {
          this._setArg(key, next) ;
          i++ ;
        }
        else if (/^(true|false)$/.test(next)) {
          this._setArg(key, next === 'true') ;
          i++ ;
        }
        else
          this._setArg(key, true) ;

        continue ;
      }
      // -*
      else if (/^-[^-]+/.test(arg)) {

        broken = false ;
        let letters = arg.slice(1,-1).split('') ;
        Utils.forEach(letters, (letter, index) => {

          let next = arg.slice(index + 2) ;
          if (next === '-')
            this._setArg(letter, next) ;
          else {
            if (/[A-Za-z]/.test(letter) && /=/.test(next)) {
              this._setArg(letter, next.split('=')[1]) ;
              broken = true ;
              return false ;
            }
            if (/[A-Za-z]/.test(letter) &&
                /-?\d+(\.\d*)?(e-?\d+)?$/.test(next)) {
              this._setArg(letter, next) ;
              broken = true ;
              return false ;
            }
            if (letters[index + 1] && letters[index + 1].match(/\W/)) {
              this._setArg(letter, next) ;
              broken = true ;
              return false ;
            }
            else
              this._setArg(letter, true) ;
          }
        }) ;

        // On s'occupe de tout ce qui n'est pas --*
        let key = arg.slice(-1)[0] ;
        if (!broken && key !== '-') {

          // L'arg suiv est la valeur du paramètre courant
          if (nextArg && !/^(-|--)[^-]/.test(nextArg)) {
            this._setArg(key, nextArg) ;
            i++ ;
          }
          else
            this._setArg(key, true) ;
        }
      }
      else {

        this._argv._.push(!Utils.isNumber(arg, true) ? arg : Number(arg)) ;

        if (this._opts.stopEarly) {
          // merge args dans argv
          Array.prototype.push.apply(this._argv._, args.slice(i + 1)) ;
          break ;
        }
      }
    }
    // Fin du parcours des args

    // On a demandé de stocker ce qu'il y a après `--`
    if (this._opts['--'])
      this._argv['--'] = notFlags ;
    else {
      notFlags.forEach(key => {
        this._argv._.push(key) ;
      }) ;
    }

    // On retourne le résultat ! (enfin)
    return this._argv ;
  }

  /**
   * Défini un argument
   * @param {String} key Nom de la clé
   * @param {*} val Valeur de l'argument
   * @private
   */
  _setArg (key, val) {

    let value = Utils.isNumber(val, true) ? Number(val) : val ;
    this._setKey(this._argv, key.split('.'), value) ;
  }

  /**
   * Enregistre des clés
   * @param {Object} obj Objet stockant les clés (argv)
   * @param {Array} keys Clés à enregistrer
   * @param {*} value Valeur de la clé
   * @private
   */
  _setKey (obj, keys, value) {

    keys.slice(0,-1).forEach(key => {
      if (obj[key] === undefined)
        obj[key] = {} ;
      obj = obj[key] ;
    }) ;

    let key = keys[keys.length - 1] ;

    // Clé pas encore définie, ou devant être un boolean
    if (obj[key] === undefined)
      obj[key] = value ;
    // La clé contient déjà un tableau, on ajoute la nouvelle valeur
    else if (Array.isArray(obj[key]))
      obj[key].push(value) ;
    // Sinon on transforme la valeur courante en tableau avec la nvlle valeur
    else
      obj[key] = [obj[key], value] ;
  }
}

module.exports = ParseArgs ;

