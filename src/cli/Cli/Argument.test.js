'use strict' ;

/**
 * src/cli/Cli/Argument.test.js
 */

const expect = require('chai').expect ;

const Argument = require('./Argument'),
      Cli      = require('../Cli') ;


// buddy ignore:start
describe('Argument', function () {

  let app = new Cli() ;

  describe('constructor', function () {

    let arg = null ;

    beforeEach(function () {

      arg = new Argument('<test>', 'une description', /[0-9]/, 0, app) ;
    }) ;

    it('détermine si l\'argument est obligatoire ou optionnel', function () {

      expect(Argument.REQUIRED).to.be.not.equal(Argument.OPTIONAL) ;
      expect(arg._type).to.be.equal(Argument.REQUIRED) ;
      arg = new Argument('[test]', 'une description', /[0-9]/, 0, app) ;
      expect(arg._type).to.be.equal(Argument.OPTIONAL) ;
    }) ;

    it('détermine si l\'argument peut contenir un nombre variable d\'éléments',
    function () {

      expect(arg._variadic).to.be.false ;
      arg = new Argument('[test...]', 'une description', /[0-9]/, 0, app) ;
      expect(arg._variadic).to.be.true ;
    }) ;

    it('définit le nom de l\'argument', function () {

      expect(arg._name).to.be.equal('test') ;
      arg = new Argument('[test...]', 'une description', /[0-9]/, 0, app) ;
      expect(arg._name).to.be.equal('test') ;
      arg = new Argument('<salut-toi>', 'une description', /[0-9]/, 0, app) ;
      expect(arg._name).to.be.equal('salutToi') ;
      arg = new Argument('[HelloWorld3]', 'une description', /[0-9]/, 0, app) ;
      expect(arg._name).to.be.equal('helloWorld3') ;
      arg = new Argument('[my-HTTP]', 'une description', /[0-9]/, 0, app) ;
      expect(arg._name).to.be.equal('myHttp') ;
    }) ;
  }) ;


  describe('isRequired', function () {

    it('return true if argument is required', function () {

      let arg = new Argument('<test>', 'une description', /[0-9]/, 2, app) ;
      expect(arg.isRequired()).to.be.true ;
    }) ;

    it('return false if argument is not required', function () {

      let arg = new Argument('[test]', 'une description', /[0-9]/, 2, app) ;
      expect(arg.isRequired()).to.be.false ;
    }) ;
  }) ;


  describe('isVariadic', function () {

    it('return true if arg can have variadic number value', function () {

      let arg = new Argument('[test...]', '', /a/, 'a', app) ;
      expect(arg.isVariadic()).to.be.equal(true) ;
    }) ;

    it('return false if arg can\'t have variadic number value', function () {

      let arg = new Argument('[test]', '', /a/, 'a', app) ;
      expect(arg.isVariadic()).to.be.equal(false) ;
    }) ;
  }) ;


  describe('isOptional', function () {

    it('return true if arg is optional', function () {

      let arg = new Argument('[test]', '', /a/, 'a', app) ;
      expect(arg.isOptional()).to.be.equal(true) ;
    }) ;

    it('return false if arg isn\'t optional', function () {

      let arg = new Argument('<test>', '', /a/, 'a', app) ;
      expect(arg.isOptional()).to.be.equal(false) ;
    }) ;
  }) ;
}) ;
// buddy ignore:end

