'use strict' ;

/**
 * src/cli/Cli/Argument.js
 */

const ArgOpt = require('./ArgOpt'),
      Utils  = require('../../core/Utils') ;

class Argument extends ArgOpt {

  /**
   * @param {String} synopsis Synopsis de l'argument/de l'option
   *        Si commence par [, l'argument sera optionel. Si vaut ... alors
   *        correspond à un nombre variable de valeurs
   * @param {String} description Description de l'option/ de l'argument
   * @param {String|RegExp|Function|Number} [validator] Validateur
   * @param {*} [defaultValue] Valeur par défaut
   * @param {Application} application Instance du programme
   */
  constructor (synopsis, description, validator, defaultValue, application) {

    super(synopsis, description, validator, defaultValue, application) ;

    this._type = synopsis.substring(0, 1) === '[' ?
        Argument.OPTIONAL : Argument.REQUIRED ;
    // buddy ignore:start
    this._variadic = synopsis.substr(-4, 3) === '...' ;
    // buddy ignore:end

    this._name = Utils.camelCase(
       synopsis.replace(/([[\]<>]+)/g, '').replace('...', '')
    ) ;
  }


  /**
   * Indique si l'argument est requis
   * @returns {Boolean}
   */
  isRequired () {

    return this._type === Argument.REQUIRED ;
  }


  /**
   * Indique si l'argument peut posséder un nombre variable de valeur
   * @returns {Boolean}
   */
  isVariadic () {

    return this._variadic === true ;
  }


  /**
   * Indique si l'argument est optionnel
   * @returns {Boolean}
   */
  isOptional () {

    return this._type === Argument.OPTIONAL ;
  }
}

module.exports = Argument ;
module.exports.REQUIRED = 1 ;
module.exports.OPTIONAL = 2 ;

