'use strict' ;

/**
 * src/index.test.js
 */

const expect = require('chai').expect ;
const Index       = require('./index'),
      Cli         = require('./cli/Cli'),
      Framework   = require('./core/Framework'),
      Server      = require('./server/Server'),
      FormBuilder = require('./server/Form/FormBuilder'),
      Field       = require('./server/Form/Field') ;

describe('index', function () {

  it('is instance of Core/Framework', function () {

    expect(new Index()).instanceOf(Framework) ;
  }) ;


  describe('cli', function () {

    it('returns a new instance of Cli', function () {

      expect(Index.cli()).instanceOf(Cli) ;
    }) ;
  }) ;

  describe('server', function () {

    it('returns a new instance of Server', function () {

      expect(Index.server()).instanceOf(Server) ;
    }) ;

    it('returns FormBuilder constructor', function () {

      expect(Index.FormBuilder).to.be.equal(FormBuilder) ;
    }) ;

    it('returns Field constructor', function () {

      expect(Index.Field).to.be.equal(Field) ;
    }) ;
  }) ;
}) ;
