'use strict' ;

/**
 * src/browser/Utils.test.js
 */

const expect = require('chai').expect,
      sinon  = require('sinon') ;

const Utils     = require('./Utils'),
      coreUtils = require('../core/Utils') ;

// buddy ignore:start
describe('browser Utils', function () {

  it('extends core Utils', function () {

    expect(new Utils()).to.be.instanceOf(coreUtils) ;
  }) ;


  describe('.isBlob', function () {

    it('true for Blob object', function () {

      expect(Utils.isBlob(new Blob(['a', 'b']))).to.be.equal(true) ;
    }) ;

    it('false for non Blob object', function () {

      expect(Utils.isBlob(['a', 'b'])).to.be.equal(false) ;
      expect(Utils.isBlob({'a': 1, 'b': 2})).to.be.equal(false) ;
      expect(Utils.isBlob('a')).to.be.equal(false) ;
      expect(Utils.isBlob(1)).to.be.equal(false) ;
      expect(Utils.isBlob(true)).to.be.equal(false) ;
    }) ;
  }) ;


  describe('.isFile', function () {

    it('true for File object', function () {

      expect(Utils.isFile(new File(['foo'], 'foo.txt', {type: 'text/plain'})))
        .to.be.equal(true) ;
    }) ;

    it('false for non File object', function () {

      expect(Utils.isFile(['a', 'b'])).to.be.equal(false) ;
      expect(Utils.isFile({'a': 1, 'b': 2})).to.be.equal(false) ;
      expect(Utils.isFile('a')).to.be.equal(false) ;
      expect(Utils.isFile(1)).to.be.equal(false) ;
      expect(Utils.isFile(true)).to.be.equal(false) ;
    }) ;
  }) ;


  describe('.isFormData', function () {

    it('true for FormData object', function () {

      expect(Utils.isFormData(new FormData())).to.be.equal(true) ;
    }) ;

    it('false for non FormData object', function () {

      expect(Utils.isFormData(['a', 'b'])).to.be.equal(false) ;
      expect(Utils.isFormData({'a': 1, 'b': 2})).to.be.equal(false) ;
      expect(Utils.isFormData('a')).to.be.equal(false) ;
      expect(Utils.isFormData(1)).to.be.equal(false) ;
      expect(Utils.isFormData(true)).to.be.equal(false) ;
    }) ;
  }) ;


  describe('.str2DomElement', function () {

    it('returns an Dom element', function () {

      expect(Utils.str2DomElement('hello')[0]).instanceOf(Node) ;
    }) ;

    it('si pas de tag <>, on retourne du texte', function () {

      expect(Utils.str2DomElement('hello')[0].nodeName).to.be.equal('#text') ;
    }) ;

    it('sinon, retourne le bon élément', function () {

      expect(Utils.str2DomElement('<tag>hello</tag>')[0].nodeName)
        .to.be.equal('TAG') ;
      expect(Utils.str2DomElement('<div>hello</div>')[0].nodeName)
        .to.be.equal('DIV') ;
      expect(Utils.str2DomElement('<td>hello</td>')[0].nodeName)
        .to.be.equal('TD') ;
    }) ;

    it('s\'il y a plusieurs éléments, on retourne une liste', function () {

      let els = Utils.str2DomElement('<tag>Hello</tag> <p>World</p>!') ;
      expect(els).to.be.instanceOf(NodeList) ;
      expect(els.length).to.be.equal(4) ;
      expect(els[0].nodeName).to.be.equal('TAG') ;
      expect(els[1].nodeName).to.be.equal('#text') ;
      expect(els[2].nodeName).to.be.equal('P') ;
      expect(els[3].nodeName).to.be.equal('#text') ;
    }) ;
  }) ;


  describe('.debounce', function () {

    it('Le code ne sera exécuté qu\'au bout du delai', function (done) {

      let spy = sinon.spy() ;
      let debounce = Utils.debounce(spy, 350) ;

      debounce() ;
      expect(spy.getCalls().length).to.be.equal(0) ;

      setTimeout(() => {
        expect(spy.getCalls().length).to.be.equal(1) ;
        done() ;
      }, 350) ;
    }) ;

    it('réinitialise le timer si rappellée', function (done) {

      let spy = sinon.spy() ;
      let debounce = Utils.debounce(spy, 350) ;

      debounce() ;
      expect(spy.getCalls().length).to.be.equal(0) ;

      setTimeout(() => {
        debounce() ;
        expect(spy.getCalls().length).to.be.equal(0) ;
        setTimeout(() => {
          debounce() ;
          expect(spy.getCalls().length).to.be.equal(1) ;
          done() ;
        }, 350) ;
      }, 100) ;
    }) ;
  }) ;

  describe('.throttle', function () {

    it('exécute une fois par délais le callback', function () {

      let spy = sinon.spy() ;
      let throttle = Utils.throttle(spy, 350) ;

      throttle() ;
      throttle() ;

      expect(spy.getCalls().length).to.be.equal(1) ;
    }) ;

    it('une fois le délai écoulé, on peut réexécuter la fonction',
    function (done) {

      let spy = sinon.spy() ;
      let throttle = Utils.throttle(spy, 350) ;

      throttle() ;

      setTimeout(() => {
        throttle() ;
        expect(spy.getCalls().length).to.be.equal(2) ;
        done() ;
      }, 350) ;
    }) ;
  }) ;


  describe('.absolutePath', function () {

    it('absolute path for absolute href', function () {

      expect(Utils.absolutePath('https://www.theo-net.org/test'))
        .to.be.equal('https://www.theo-net.org/test') ;
    }) ;

    it('absolute path for relative href', function () {

      expect(Utils.absolutePath('./test'))
        .to.be.equal('http://localhost:9876/test') ;
    }) ;
  }) ;
}) ;
// buddy ignore:end

