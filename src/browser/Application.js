'use strict' ;

/**
 * src/browser/Application.js
 */

const CoreApplication = require('../core/Application'),
      Assets          = require('./Services/Assets'),
      Compile         = require('./Services/Compile'),
      Controller      = require('./Services/Controller'),
      Http            = require('./Services/Http'),
      Logs            = require('../core/Services/Logs') ;

class Application extends CoreApplication {

  constructor () {

    super() ;

    const compile = new Compile() ;

    this.kernel().container().set('$http', new Http()) ;
    this.kernel().container().set('$controller', new Controller()) ;
    this.kernel().container().set('$compile', compile) ;
    this.kernel().container().set('$assets', new Assets()) ;
    this.kernel().container().set('$logs', new Logs()) ;

    // On ajoute les directives
    compile.directive('kjsController', require('./directives/kjs_controller')) ;
    compile.directive('kjsClick', require('./directives/kjs_click')) ;
    compile.directive('kjsTransclude', require('./directives/kjs_transclude')) ;
    compile.directive('kjsShow', require('./directives/kjs_show')) ;
    compile.directive('kjsHide', require('./directives/kjs_hide')) ;
  }


  /**
   * Alias pour enregistrer un controller
   * @param {String|Object} name Nom du controller ou objet
   *      `{nom1: ctrl1, nom2: ctrl2, ...}`
   * @param {Object} controller Controller à ajouter
   */
  controller (name, controller) {

    this.kernel().get('$controller').register(name, controller) ;
  }
}

module.exports = Application ;

