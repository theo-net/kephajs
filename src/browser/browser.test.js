'use strict' ;

/**
 * src/browser/browser.test.js
 */

const expect = require('chai').expect ;

const Application = require('../core/Application'),
      Utils       = require('./Utils'),
      Kernel    = require('../core/Kernel'),
      Entity    = require('../core/Entity') ;

require('./browser.js') ;
const K = window.K ;

describe('browser', function () {

  it('KephaJs est disponible', function () {

    expect(window.K).to.be.not.undefined ;
  }) ;


  describe('K', function () {

    it('fournit toutes les méthodes de browser/Utils', function () {

      const utilsMethods = Reflect.ownKeys(Utils) ;
      let expected = true ;

      utilsMethods.forEach(method => {

        if (!Reflect.has(K, method)) {
          expected = false ;
          console.error('`' + method + '` absente') ;
        }
      }) ;

      expect(expected).to.be.true ;
    }) ;

    it('donne accès au Kernel', function () {

      expect(K.kernel()).to.be.instanceOf(Kernel) ;
    }) ;

    it('donne accès à la class Entity', function () {

      expect(K.Entity).to.be.equal(Entity) ;
    }) ;

    describe('app', function () {

      it('returns a new Application', function () {

        const A = K.app() ;
        const B = K.app() ;

        expect(A).to.be.instanceOf(Application) ;
        expect(B).to.be.not.equal(A) ;
      }) ;
    }) ;
  }) ;
}) ;

