'use strict' ;

/**
 * src/browser.js
 *
 * Point d'entrée pour le framework lorsqu'il est chargé dans le navigateur
 */

const Application = require('./Application'),
      Utils       = require('./Utils'),
      Kernel      = require('../core/Kernel'),
      Entity      = require('../core/Entity') ;

/**
 * Framework de base
 */
class Framework extends Utils {

  /**
   * Retourne le Kernel
   * @returns {Kernel}
   * @static
   */
  static kernel () {

    return new Kernel() ;
  }


  /**
   * Retourne une nouvelle application et compile le DOM
   * @returns {Application}
   * @static
   */
  static app () {

    let app = new Application() ;

    // Compile le DOM à la recherche d'élément possédant un des attributs
    // suivants : kjs-app, data-kjs-app, kjs:app, x-kjs-app
    // La valeur de l'attribut correspondra au nom du module à charger
    // (si définit)
    let kjsAttr = ['kjs-app', 'data-kjs-app', 'kjs:app', 'x-kjs-app'] ;
    document.addEventListener('DOMContentLoaded', () => {

      kjsAttr.forEach(attr => {

        let foundAppElement, element ;
        let selector = '[' + attr.replace(':', '\\:') + ']' ;

        if (!foundAppElement &&
          (element = document.querySelector(selector))) {

          foundAppElement = element ;
          console.log('Application trouvée : ' + element.getAttribute(attr)) ;

          let rootScope = app.kernel().get('$rootScope') ;
          rootScope.$apply(() => {
            app.kernel().get('$compile').run(Array(element))(rootScope) ;
          }) ;
        }
      }) ;
    }) ;

    return app ;
  }
}

Framework.Entity = Entity ;


// On publie l'API
window.K = Framework ;

