'use strict' ;

/**
 * src/browser/Services/Controller.js
 */

const KernelAccess = require('../../core/KernelAccess'),
      Utils        = require('../Utils') ;

class Controller extends KernelAccess {

  constructor () {

    super() ;

    this._controllers = {} ;
    this._globals = false ;
  }


  /**
   * Retourne un controller
   *
   * @param {String} ctrl Retourne un controller (ira le cherché si déjà def)
   * @param {Object} locals Scope local
   * @param {Boolean} later Quand instancie t-on le controller ?
   * @param {String} identifier Identifiant du controller
   * @returns {Object}
   */
  get (ctrl, locals, later, identifier) {

    if (Utils.isString(ctrl)) {

      // Récupère le nom du controller et l'identifier
      // (myController as ctrl)
      let match = ctrl.match(/^(\S+)(\s+as\s+(\w+))?/) ;
      ctrl = match[1] ;
      identifier = identifier || match[3] ; // buddy ignore:line

      if (this._controllers.hasOwnProperty(ctrl))
        ctrl = this._controllers[ctrl] ;
      else {
        ctrl = (locals && locals.$scope && locals.$scope[ctrl]) ||
               (this._globals && window[ctrl]) ;
      }
    }

    // On instantie le controller en lui passant les bons params
    let instance = Object.create(ctrl.prototype) ;
    if (identifier)
      addToScope(locals, identifier, instance) ;
    if (later) {

      let thisCtrl = this ;
      return Utils.extend(function () {
        thisCtrl._invoke(ctrl, instance, locals) ;
        return instance ;
      }, {
        instance: instance
      }) ;
    }
    else {
      this._invoke(ctrl, instance, locals) ;
      return instance ;
    }
  }


  /**
   * Autorise l'existance de controllers déclarer directement dans l'objet
   * global window.
   */
  allowGlobals () {

    this._globals = true ;
  }


  /**
   * Enregistre un controller
   * @param {String|Object} name Nom du controller ou objet
   *      `{nom1: ctrl1, nom2: ctrl2, ...}`
   * @param {Object} controller Controller à ajouter
   */
  register (name, controller) {

    if (Utils.isObject(name))
      Utils.extend(this._controllers, name) ;
    else
      this._controllers[name] = controller ;
  }

  /**
   * Exécute le controller `ctrl`
   * @param {Object} ctrl Constructeur du controlleur
   * @param {Object} instance Instance du controlleur que l'on invoque
   * @param {Object} locals Variables que l'on transmet au controller
   *   (si une clé correspond à un des paramètres du constructeurs, elle sera
   *   utilisée, sinon on ira chercher le service dans le container)
   * @private
   */
  _invoke (ctrl, instance, locals) {

    let args = [] ;
    if (ctrl.length) {
      // Strip comment
      let source = ctrl.toString().replace(/(\/\/.*$)|(\/\*.*?\*\/)/mg, '') ;
      // Les args
      let argDeclaration = source.match(/^function\s*[^(]*\(\s*([^)]*)\)/m) ;
      args = argDeclaration[1].split(',').map(argName => {
        return argName.match(/^\s*(_?)(\S+)\1\s*/)[2] ;
      }) ;
      args = args.map(token => {
        return locals && locals.hasOwnProperty(token) ?
          locals[token] : this.kernel().get(token) ;
      }) ;
    }
    ctrl.apply(instance, args) ;
  }
}

module.exports = Controller ;

/**
 * Ajoute instance à locals.$scope[identifier]
 * @param {Object} locals Scope local ?
 * @param {String} identifier Identifiant du controller
 * @param {Object} instance Instance du controller
 */
function addToScope (locals, identifier, instance) {

  if (locals && Utils.isObject(locals.$scope))
    locals.$scope[identifier] = instance ;
  else {
    throw 'Cannot export controller as ' + identifier
      + '! No $scope object provided via locals' ;
  }
}

