'use strict' ;

/**
 * src/browser/Services/Http.test.js
 */

const expect = require('chai').expect,
      sinon  = require('sinon') ;

const Http  = require('./Http'),
      Utils = require('../Utils') ;

let xhr, clock, requests, http ;


// buddy ignore:start
describe('Http', function () {

  beforeEach(function () {

    xhr = sinon.useFakeXMLHttpRequest() ;
    requests = [] ;
    xhr.onCreate = function (req) {
      requests.push(req) ;
    } ;
    clock = sinon.useFakeTimers() ;
    http = new Http() ;
  }) ;

  afterEach(function () {

    xhr.restore() ;
    clock.restore() ;
  }) ;

  describe('request', function () {

    it('returns a Promise', function () {

      let result = http.request({}) ;
      expect(result).to.be.not.undefined ;
      expect(result.then).to.be.not.undefined ;
    }) ;

    it('makes an XMLHttpRequest to given URL', function () {

      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: 'hello'
      }) ;

      expect(requests.length).to.be.equal(1) ;
      expect(requests[0].method).to.be.equal('POST') ;
      expect(requests[0].url).to.be.equal('http://theo-net.org') ;
      expect(requests[0].async).to.be.equal(true) ;
      expect(requests[0].requestBody).to.be.equal('hello') ;
    }) ;

    it('resolves promise when XHR result received', function (done) {

      http.request({
        method: 'GET',
        url: 'http://theo-net.org'
      }).then(r => {
        let response ;
        response = r ;

        expect(response).to.be.not.undefined ;
        expect(response.status).to.be.equal(200) ;
        expect(response.statusText).to.be.equal('OK') ;
        expect(response.data).to.be.equal('Hello') ;
        expect(response.config.url).to.be.equal('http://theo-net.org') ;
      }).then(done, done) ;

      requests[0].respond(200, {}, 'Hello') ;
    }) ;

    it('resolves promise when XHR result received with error status',
    function (done) {

      http.request({
        method: 'GET',
        url: 'http://theo-net.org'
      }).catch(r => {
        let response ;
        response = r ;

        expect(response).to.be.not.undefined ;
        expect(response.status).to.be.equal(401) ;
        expect(response.statusText).to.be.equal('Unauthorized') ;
        expect(response.data).to.be.equal('Fail') ;
        expect(response.config.url).to.be.equal('http://theo-net.org') ;
        done() ;
      }) ;

      requests[0].respond(401, {}, 'Fail') ;
    }) ;

    it('rejects promise when XHR result error/aborts', function (done) {

      http.request({
        method: 'GET',
        url: 'http://theo-net.org'
      }).catch(r => {
        let response ;
        response = r ;

        expect(response).to.be.not.undefined ;
        expect(response.status).to.be.equal(0) ;
        expect(response.data).to.be.equal(null) ;
        expect(response.config.url).to.be.equal('http://theo-net.org') ;
        done() ;
      }) ;

      requests[0].onerror() ;
    }) ;

    it('uses GET method by default', function () {

      http.request({
        url: 'http://theo-net.org'
      }) ;

      expect(requests.length).to.be.equal(1) ;
      expect(requests[0].method).to.be.equal('GET') ;
    }) ;
  }) ;

  describe('Request Headers', function () {

    it('sets headers on request', function () {

      http.request({
        url: 'http://theo-net.org',
        headers: {
          'Accept': 'text/plain',
          'Cache-Control': 'no-cache'
        }
      }) ;

      expect(requests.length).to.be.equal(1) ;
      expect(requests[0].requestHeaders.Accept).to.be.equal('text/plain') ;
      expect(requests[0].requestHeaders['Cache-Control'])
        .to.be.equal('no-cache') ;
    }) ;

    it('sets default headers on request', function () {

      http.request({
        url: 'http://theo-net.org'
      }) ;

      expect(requests.length).to.be.equal(1) ;
      expect(requests[0].requestHeaders.Accept)
        .to.be.equal('application/json, text/plain, */*') ;
    }) ;

    it('sets method-specific default headers on request', function () {

      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: '42'
      }) ;

      expect(requests.length).to.be.equal(1) ;
      expect(requests[0].requestHeaders['Content-Type'])
        .to.be.equal('application/json;charset=utf-8') ;
    }) ;

    it('exposes default headers for overriding', function () {

      http.defaults.headers.post['Content-Type'] = 'text/plain;charset=utf-8' ;
      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: '42'
      }) ;

      expect(requests.length).to.be.equal(1) ;
      expect(requests[0].requestHeaders['Content-Type'])
        .to.be.equal('text/plain;charset=utf-8') ;
    }) ;

    it('merges default headers case-insensitively', function () {

      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: '42',
        headers: {
          'content-type': 'text/plain;charset=utf-8'
        }
      }) ;

      expect(requests.length).to.be.equal(1) ;
      expect(requests[0].requestHeaders['content-type'])
        .to.be.equal('text/plain;charset=utf-8') ;
      expect(requests[0].requestHeaders['Content-Type']).to.be.undefined ;
    }) ;

    it('does not send content-type header when no data', function () {

      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        }
      }) ;

      expect(requests.length).to.be.equal(1) ;
      expect(requests[0].requestHeaders['Content-Type'])
        .to.be.not.equal('application/json;charset=utf-8') ;
    }) ;

    it('supports functions as header values', function () {

      function contentType () {
        return 'text/plain;charset=utf-8' ;
      }
      let contentTypeSpy = sinon.spy(contentType) ;
      http.defaults.headers.post['Content-Type'] = contentTypeSpy ;

      let request = {
        method: 'POST',
        url: 'http://theo-net.org',
        data: '42'
      } ;
      http.request(request) ;

      expect(contentTypeSpy.calledWith(request)).to.be.true ;
      expect(requests[0].requestHeaders['Content-Type'])
        .to.be.equal('text/plain;charset=utf-8') ;
    }) ;

    it('ignores header function value when null/undefined', function () {

      function cacheControl () { return null ; }
      let cacheControlSpy = sinon.spy(cacheControl) ;
      http.defaults.headers.post['Cache-Control'] = cacheControlSpy ;

      let request = {
        method: 'POST',
        url: 'http://theo-net.org',
        data: '42'
      } ;
      http.request(request) ;

      expect(cacheControlSpy.calledWith(request)).to.be.true ;
      expect(requests[0].requestHeaders['Cache-Control']).to.be.undefined ;
    }) ;
  }) ;


  describe('Response Headers', function () {

    it('makes response headers available', function (done) {

      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: 42
      }).then(r => {
        let response ;
        response = r ;

        expect(response.headers).to.be.not.undefined ;
        expect(response.headers).to.be.instanceOf(Function) ;
        expect(response.headers('Content-Type')).to.be.equal('text/plain') ;
        expect(response.headers('content-type')).to.be.equal('text/plain') ;
      }).then(done, done) ;

      requests[0].respond(200, {'Content-Type': 'text/plain'}, 'Hello') ;
    }) ;

    it('may returns all response headers', function (done) {

      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: 42
      }).then(r => {
        let response ;
        response = r ;

        expect(response.headers())
          .to.be.deep.equal({'content-type': 'text/plain'}) ;
      }).then(done, done) ;

      requests[0].respond(200, {'Content-Type': 'text/plain'}, 'Hello') ;
    }) ;
  }) ;


  describe('CORS Authorization', function () {

    it('allows setting withCredentials', function () {

      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: '42',
        withCredentials: true
      }) ;

      expect(requests[0].withCredentials).to.be.equal(true) ;
    }) ;

    it('allows setting withCredentials from defaults', function () {

      http.defaults.withCredentials = true ;

      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: '42'
      }) ;

      expect(requests[0].withCredentials).to.be.equal(true) ;
    }) ;
  }) ;


  describe('Request Transforms', function () {

    it('allows transforming requests with functions', function () {

      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: '42',
        transformRequest: function (data) {
          return '*' + data + '*' ;
        }
      }) ;

      expect(requests[0].requestBody).to.be.equal('*42*') ;
    }) ;

    it('allows multiple request transform functions', function () {

      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: '42',
        transformRequest: [function (data) {
          return '*' + data + '*' ;
        }, function (data) {
          return '-' + data + '-' ;
        }]
      }) ;

      expect(requests[0].requestBody).to.be.equal('-*42*-') ;
    }) ;

    it('allows settings transform in defaults', function () {

      http.defaults.transformRequest = [function (data) {
        return '*' + data + '*' ;
      }] ;
      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: '42'
      }) ;

      expect(requests[0].requestBody).to.be.equal('*42*') ;
    }) ;

    it('passes request headers getter to transforms', function () {

      http.defaults.transformRequest = [function (data, headers) {
        if (headers('Content-Type') === 'text/emphasized')
          return '*' + data + '*' ;
        else
          return data ;
      }] ;
      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: '42',
        headers: {
          'content-type': 'text/emphasized'
        }
      }) ;

      expect(requests[0].requestBody).to.be.equal('*42*') ;
    }) ;
  }) ;


  describe('Reponse Transforms', function () {

    it('allows transforming responses with functions', function (done) {

      http.request({
        url: 'http://theo-net.org',
        transformResponse: function (data) {
          return '*' + data + '*' ;
        }
      }).then(r => {
        let response ;
        response = r ;

        expect(response.data).to.be.equal('*Hello*') ;
      }).then(done, done) ;

      requests[0].respond(200, {'Content-Type': 'text/plain'}, 'Hello') ;
    }) ;

    it('passes reponse headers to transform functions', function (done) {

      http.request({
        url: 'http://theo-net.org',
        transformResponse: function (data, headers) {
          if (headers('content-type') === 'text/decorated')
            return '*' + data + '*' ;
          else
            return data ;
        }
      }).then(r => {
        let response ;
        response = r ;

        expect(response.data).to.be.equal('*Hello*') ;
      }).then(done, done) ;

      requests[0].respond(200, {'Content-Type': 'text/decorated'}, 'Hello') ;
    }) ;

    it('allows setting default response transforms', function (done) {

      http.defaults.transformResponse = [function (data) {
        return '*' + data + '*' ;
      }] ;
      http.request({
        url: 'http://theo-net.org'
      }).then(r => {
        let response ;
        response = r ;

        expect(response.data).to.be.equal('*Hello*') ;
      }).then(done, done) ;

      requests[0].respond(200, {'Content-Type': 'text/plain'}, 'Hello') ;
    }) ;

    it('transforms error responses also', function (done) {

      http.request({
        url: 'http://theo-net.org',
        transformResponse: function (data) {
          return '*' + data + '*' ;
        }
      }).catch(r => {
        let response ;
        response = r ;

        expect(response.data).to.be.equal('*Fail*') ;
        done() ;
      }) ;

      requests[0].respond(401, {'Content-Type': 'text/plain'}, 'Fail') ;
    }) ;

    it('passes HTTP status to response transformers', function (done) {

      http.request({
        url: 'http://theo-net.org',
        transformResponse: function (data, headers, status) {
          if (status === 401)
            return 'unauthorized' ;
          else
            return data ;
        }
      }).catch(r => {
        let response ;
        response = r ;

        expect(response.data).to.be.equal('unauthorized') ;
        done() ;
      }) ;

      requests[0].respond(401, {'Content-Type': 'text/plain'}, 'Fail') ;
    }) ;
  }) ;


  describe('JSON Serialization and Parsing', function () {

    it('serializes object data to JSON for requests', function () {

      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: {aKey: 42}
      }) ;

      expect(requests[0].requestBody).to.be.equal('{"aKey":42}') ;
    }) ;

    it('serializes array data to JSON for requests', function () {

      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: [1, 'two', 3]
      }) ;

      expect(requests[0].requestBody).to.be.equal('[1,"two",3]') ;
    }) ;

    it('does not serialize blobs for requests', function () {

      let blob ;
      if (window.Blob)
        blob = new Blob(['hello']) ;
      else {
        let BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder ||
          window.MozBlobBuilder || window.MSBlobBuilder ;
        let bb = new BlobBuilder() ;
        bb.append('hello') ;
        blob = bb.getBlob('text/plain') ;
      }

      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: blob
      }) ;

      expect(requests[0].requestBody).to.be.equal(blob) ;
    }) ;

    it('does not serialize form data for requests', function () {

      let formData = new FormData() ;
      formData.append('aFiled', 'aValue') ;

      http.request({
        method: 'POST',
        url: 'http://theo-net.org',
        data: formData
      }) ;

      expect(requests[0].requestBody).to.be.equal(formData) ;
    }) ;

    it('parses JSON data for JSON responses', function (done) {

      http.request({
        method: 'GET',
        url: 'http://theo-net.org'
      }).then(r => {
        let response ;
        response = r ;

        expect(Utils.isObject(response.data)).to.be.true ;
        expect(response.data.message).to.be.equal('hello') ;
      }).then(done, done) ;

      requests[0].respond(
        200,
        {'Content-Type': 'application/json'},
        '{"message":"hello"}') ;
    }) ;

    it('parses a JSON object response without content type', function (done) {

      http.request({
        method: 'GET',
        url: 'http://theo-net.org'
      }).then(r => {
        let response ;
        response = r ;

        expect(Utils.isObject(response.data)).to.be.true ;
        expect(response.data.message).to.be.equal('hello') ;
      }).then(done, done) ;

      requests[0].respond(200, {}, '{"message":"hello"}') ;
    }) ;

    it('parses a JSON array response without content type', function (done) {

      http.request({
        method: 'GET',
        url: 'http://theo-net.org'
      }).then(r => {
        let response ;
        response = r ;

        expect(Array.isArray(response.data)).to.be.true ;
        expect(response.data).to.be.deep.equal([1, 2, 3]) ;
      }).then(done, done) ;

      requests[0].respond(200, {}, '[1, 2, 3]') ;
    }) ;

    it('does not choke on response resembling JSON but not valid',
    function (done) {

      http.request({
        method: 'GET',
        url: 'http://theo-net.org'
      }).then(r => {
        let response ;
        response = r ;

        expect(response.data).to.be.equal('{1, 2, 3]') ;
      }).then(done, done) ;

      requests[0].respond(200, {}, '{1, 2, 3]') ;
    }) ;

    it('does not try to parse interpolation expr as JSON', function (done) {

      http.request({
        method: 'GET',
        url: 'http://theo-net.org'
      }).then(r => {
        let response ;
        response = r ;

        expect(response.data).to.be.equal('{{expr}}') ;
      }).then(done, done) ;

      requests[0].respond(200, {}, '{{expr}}') ;
    }) ;
  }) ;


  describe('URL Parameters', function () {

    it('adds params to URL', function () {

      http.request({
        url: 'http://theo-net.org',
        params: {
          a: 42
        }
      }) ;

      expect(requests[0].url).to.be.equal('http://theo-net.org?a=42') ;
    }) ;

    it('adds additional params to URL', function () {

      http.request({
        url: 'http://theo-net.org?a=42',
        params: {
          b: 42
        }
      }) ;

      expect(requests[0].url).to.be.equal('http://theo-net.org?a=42&b=42') ;
    }) ;

    it('escapes url characters in params', function () {

      http.request({
        url: 'http://theo-net.org',
        params: {
          '==': '&&'
        }
      }) ;

      expect(requests[0].url).to.be.equal('http://theo-net.org?%3D%3D=%26%26') ;
    }) ;

    it('does not attach null or undefined params', function () {

      http.request({
        url: 'http://theo-net.org',
        params: {
          a: null,
          b: undefined
        }
      }) ;

      expect(requests[0].url).to.be.equal('http://theo-net.org') ;
    }) ;

    it('attaches multiple params from arrays', function () {

      http.request({
        url: 'http://theo-net.org',
        params: {
          a: [42, 43]
        }
      }) ;

      expect(requests[0].url).to.be.equal('http://theo-net.org?a=42&a=43') ;
    }) ;

    it('serializes objects to JSON', function () {

      http.request({
        url: 'http://theo-net.org',
        params: {
          a: {b: 42}
        }
      }) ;

      expect(requests[0].url)
        .to.be.equal('http://theo-net.org?a=%7B%22b%22%3A42%7D') ;
    }) ;

    it('serializes dates to ISO strings', function () {

      http.request({
        url: 'http://theo-net.org',
        params: {
          a: new Date(2017, 5, 15, 9, 55, 55)
        }
      }) ;

      expect(/\d{4}-\d{2}-\d{2}T\d{2}%3A\d{2}%3A\d{2}/.test(requests[0].url))
        .to.be.true ;
    }) ;

    it('allows substituting param serializer', function () {

      http.request({
        url: 'http://theo-net.org',
        params: {
          a: 42,
          b: 43
        },
        paramSerializer: function (params) {
          let param = [] ;
          Utils.forEach(params, (v, k) => {
            param.push(k + '=' + v + 'lol') ;
          }) ;
          return param.join('&') ;
        }
      }) ;

      expect(requests[0].url)
        .to.be.equal('http://theo-net.org?a=42lol&b=43lol') ;
    }) ;


    describe('JQ-like param serialization', function () {

      it('is possible', function () {

        http.request({
          url: 'http://theo-net.org',
          params: {
            a: 42,
            b: 43
          },
          paramSerializer: http.paramSerializerJQLike
        }) ;

        expect(requests[0].url)
          .to.be.equal('http://theo-net.org?a=42&b=43') ;
      }) ;

      it('uses square brackets in arrays', function () {

        http.request({
          url: 'http://theo-net.org',
          params: {
            a: [42, 43]
          },
          paramSerializer: http.paramSerializerJQLike
        }) ;

        expect(requests[0].url)
          .to.be.equal('http://theo-net.org?a%5B%5D=42&a%5B%5D=43') ;
      }) ;

      it('uses square brackets in objets', function () {

        http.request({
          url: 'http://theo-net.org',
          params: {
            a: {b: 42, c: 43}
          },
          paramSerializer: http.paramSerializerJQLike
        }) ;

        expect(requests[0].url)
          .to.be.equal('http://theo-net.org?a%5Bb%5D=42&a%5Bc%5D=43') ;
      }) ;

      it('supports nesting in objets', function () {

        http.request({
          url: 'http://theo-net.org',
          params: {
            a: {b: {c: 43}}
          },
          paramSerializer: http.paramSerializerJQLike
        }) ;

        expect(requests[0].url)
          .to.be.equal('http://theo-net.org?a%5Bb%5D%5Bc%5D=43') ;
      }) ;

      it('appends array indexes when items are objects', function () {

        http.request({
          url: 'http://theo-net.org',
          params: {
            a: [{b: 42}]
          },
          paramSerializer: http.paramSerializerJQLike
        }) ;

        expect(requests[0].url)
          .to.be.equal('http://theo-net.org?a%5B0%5D%5Bb%5D=42') ;
      }) ;
    }) ;
  }) ;


  describe('Shortand Methods', function () {

    it('supports shortand method for GET', function () {

      http.get('http://theo-net.org', {
        params: {q: 42}
      }) ;

      expect(requests[0].url).to.be.equal('http://theo-net.org?q=42') ;
      expect(requests[0].method).to.be.equal('GET') ;
    }) ;

    it('supports shortand method for HEAD', function () {

      http.head('http://theo-net.org', {
        params: {q: 42}
      }) ;

      expect(requests[0].url).to.be.equal('http://theo-net.org?q=42') ;
      expect(requests[0].method).to.be.equal('HEAD') ;
    }) ;

    it('supports shortand method for DELETE', function () {

      http.delete('http://theo-net.org', {
        params: {q: 42}
      }) ;

      expect(requests[0].url).to.be.equal('http://theo-net.org?q=42') ;
      expect(requests[0].method).to.be.equal('DELETE') ;
    }) ;

    it('supports shortand method for POST with data', function () {

      http.post('http://theo-net.org', 'data', {
        params: {q: 42}
      }) ;

      expect(requests[0].url).to.be.equal('http://theo-net.org?q=42') ;
      expect(requests[0].method).to.be.equal('POST') ;
      expect(requests[0].requestBody).to.be.equal('data') ;
    }) ;

    it('supports shortand method for PUT with data', function () {

      http.put('http://theo-net.org', 'data', {
        params: {q: 42}
      }) ;

      expect(requests[0].url).to.be.equal('http://theo-net.org?q=42') ;
      expect(requests[0].method).to.be.equal('PUT') ;
      expect(requests[0].requestBody).to.be.equal('data') ;
    }) ;

    it('supports shortand method for PATCH with data', function () {

      http.patch('http://theo-net.org', 'data', {
        params: {q: 42}
      }) ;

      expect(requests[0].url).to.be.equal('http://theo-net.org?q=42') ;
      expect(requests[0].method).to.be.equal('PATCH') ;
      expect(requests[0].requestBody).to.be.equal('data') ;
    }) ;
  }) ;


  describe('Promise Extension', function () {

    it('allows attaching success handlers', function (done) {

      http.get('http://theo-net.org').success((d, s, h, c) => {

        expect(d).to.be.equal('Hello') ;
        expect(s).to.be.equal(200) ;
        expect(h('Cache-Control')).to.be.equal('no-cache') ;
        expect(c.method).to.be.equal('GET') ;
        done() ;
      }) ;

      requests[0].respond(200, {'Cache-Control': 'no-cache'}, 'Hello') ;
    }) ;

    it('allows attaching error handlers', function (done) {

      http.get('http://theo-net.org').error((d, s, h, c) => {

        expect(d).to.be.equal('Fail') ;
        expect(s).to.be.equal(401) ;
        expect(h('Cache-Control')).to.be.equal('no-cache') ;
        expect(c.method).to.be.equal('GET') ;
        done() ;
      }) ;

      requests[0].respond(401, {'Cache-Control': 'no-cache'}, 'Fail') ;
    }) ;
  }) ;


  describe('Request Timeouts', function () {

    it('allows aborting a request with a Promise', function (done) {

      let timeoutResolve ;
      let timeout = new Promise(resolve => {
        timeoutResolve = resolve ;
      }) ;

      http.get('http://theo-net.org', {
        timeout: timeout
      }) ;

      timeoutResolve() ;
      timeout.then(() => {

        expect(requests[0].aborted).to.be.true ;
      }).then(done, done) ;
    }) ;

    it('allows aborting a request after a timeout', function () {

      http.get('http://theo-net.org', {
        timeout: 5000
      }) ;

      clock.tick(5001) ;

      expect(requests[0].aborted).to.be.true ;
    }) ;
  }) ;


  describe('Pending Requests', function () {

    it('are in the collection while pending', function (done) {

      http.get('http://theo-net.org').then(() => {
        expect(http.pendingRequests.length).to.be.equal(0) ;
        done() ;
      }) ;

      expect(http.pendingRequests).to.be.not.undefined ;
      expect(http.pendingRequests.length).to.be.equal(1) ;
      expect(http.pendingRequests[0].url).to.be.equal('http://theo-net.org') ;

      requests[0].respond(200, {}, 'OK') ;
    }) ;

    it('are alse cleared on failure', function (done) {

      http.get('http://theo-net.org').catch(() => {
        expect(http.pendingRequests.length).to.be.equal(0) ;
        done() ;
      }) ;

      requests[0].respond(404, {}, 'Not found') ;
    }) ;
  }) ;
}) ;
// buddy ignore:end

