'use strict' ;

/**
 * src/browser/Services/Compile.test.js
 */

const expect = require('chai').expect,
      sinon = require('sinon') ;

const Compile = require('./Compile'),
      Application = require('../Application'),
      Scope   = require('../../core/Services/Scope'),
      Utils   = require('../Utils') ;

let compile ;

// Initialise les services devant se trouver dans le navigateur
new Application() ;


// buddy ignore:start
describe('Compile', function () {

  beforeEach(function () {

    compile = new Compile() ;
  }) ;


  describe('Registering Directives', function () {

    it('allows creating directives', function () {

      compile.directive('testing', function () {}) ;
      expect(compile.has('testing')).to.be.equal(true) ;
    }) ;

    it('allows creating many directives with the same name', function () {

      compile.directive('testing', function () { return {d: 'one'} ; }) ;
      compile.directive('testing', function () { return {d: 'two'} ; }) ;

      let result = compile.get('testing') ;
      expect(result.length).to.be.equal(2) ;
      expect(result[0].d).to.be.equal('one') ;
      expect(result[1].d).to.be.equal('two') ;
    }) ;

    it('does not allow a directive called hasOwnProperty', function () {

      expect(function () {
        compile.directive('hasOwnProperty', function () {}) ;
      }).to.throw() ;
    }) ;

    it('allows creating directives with object notation', function () {

      compile.directive({
        a: function () {},
        b: function () {},
        c: function () {}
      }) ;

      expect(compile.has('a')).to.be.true ;
      expect(compile.has('b')).to.be.true ;
      expect(compile.has('c')).to.be.true ;
    }) ;
  }) ;


  describe('Compiling The DOM with Element Directives', function () {

    it('compiles element directives from a single element', function () {

      compile.directive('myDirective', function () {
        return {
          restrict: 'EA',
          compile: element => {
            element.hasCompiled = true ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement('<my-directive></my-directive>') ;
      compile.run(el) ;
      expect(el[0].hasCompiled).to.be.equal(true) ;
    }) ;

    it('compiles element directives found from several elements', function () {

      let idx = 1 ;
      compile.directive('myDirective', function () {
        return {
          restrict: 'EA',
          compile: element => {
            element.hasCompiled = idx++ ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<my-directive></my-directive><my-directive></my-directive>') ;
      compile.run(el) ;
      expect(el[0].hasCompiled).to.be.equal(1) ;
      expect(el[1].hasCompiled).to.be.equal(2) ;
    }) ;
  }) ;


  describe('Recursing to Child Elements', function () {

    it('compiles element directives from child elements', function () {

      let idx = 1 ;
      compile.directive('myDirective', function () {
        return {
          restrict: 'EA',
          compile: element => {
            element.hasCompiled = idx++ ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div><my-directive></my-directive></div>') ;
      compile.run(el) ;
      expect(el[0].hasCompiled).to.be.undefined ;
      expect(el[0].lastChild.hasCompiled).to.be.equal(1) ;
    }) ;

    it('compiles nested directives', function () {

      let idx = 1 ;
      compile.directive('myDir', function () {
        return {
          restrict: 'EA',
          compile: element => {
            element.hasCompiled = idx++ ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<my-dir><my-dir><my-dir /></my-dir></my-dir>') ;
      compile.run(el) ;
      expect(el[0].hasCompiled).to.be.equal(1) ;
      expect(el[0].getElementsByTagName('my-dir')[0].hasCompiled)
        .to.be.equal(2) ;
      expect(el[0].getElementsByTagName('my-dir')[0]
               .getElementsByTagName('my-dir')[0].hasCompiled).to.be.equal(3) ;
    }) ;
  }) ;


  describe('Using Prefixes with Element Directives', function () {

    ['x', 'data'].forEach(prefix => {
      [':', '-', '_'].forEach(delim => {

        it('compiles element directives with ' + prefix + delim + ' prefix',
        function () {

          compile.directive('myDir', function () {
            return {
              restrict: 'EA',
              compile: element => {
                element.hasCompiled = true ;
              }
            } ;
          }) ;
          let el = Utils.str2DomElement(
              '<' + prefix + delim + 'my-dir></' + prefix + delim + 'my-dir>'
          ) ;
          compile.run(el) ;
          expect(el[0].hasCompiled).to.be.equal(true) ;
        }) ;
      }) ;
    }) ;
  }) ;


  describe('Applying Directives to Attributes', function () {

    it('compiles attribute directives', function () {

      compile.directive('myDirective', function () {
        return {
          restrict: 'EA',
          compile: element => {
            element.hasCompiled =  true ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el) ;
      expect(el[0].hasCompiled).to.be.true ;
    }) ;

    it('compiles attribute directives with prefixes', function () {

      compile.directive('myDirective', function () {
        return {
          restrict: 'EA',
          compile: element => {
            element.hasCompiled = true ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement('<div x:my-directive></div>') ;
      compile.run(el) ;
      expect(el[0].hasCompiled).to.be.true ;
    }) ;

    it('compiles several attribute directives in an element', function () {

      compile.directive('myDirective', function () {
        return {
          restrict: 'EA',
          compile: element => {
            element.hasCompiled = true ;
          }
        } ;
      }) ;
      compile.directive('mySecondDirective', function () {
        return {
          restrict: 'EA',
          compile: element => {
            element.secondCompiled = true ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div my-directive my-second-directive></div>') ;
      compile.run(el) ;
      expect(el[0].hasCompiled).to.be.true ;
      expect(el[0].secondCompiled).to.be.true ;
    }) ;

    it('compiles both element and attributes directives in an element',
    function () {

      compile.directive('myDirective', function () {
        return {
          restrict: 'EA',
          compile: element => {
            element.hasCompiled = true ;
          }
        } ;
      }) ;
      compile.directive('mySecondDirective', function () {
        return {
          restrict: 'EA',
          compile: element => {
            element.secondCompiled = true ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<my-directive my-second-directive></my-directive>') ;
      compile.run(el) ;
      expect(el[0].hasCompiled).to.be.true ;
      expect(el[0].secondCompiled).to.be.true ;
    }) ;

    it('compiles attribute directives with kjs-attr prefix', function () {

      compile.directive('myDirective', function () {
        return {
          restrict: 'EA',
          compile: element => {
            element.hasCompiled = true ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement('<div kjs-attr-my-directive></div>') ;
      compile.run(el) ;
      expect(el[0].hasCompiled).to.be.true ;
    }) ;

    it('compiles attribute directives with data:kjs-attr prefix', function () {

      compile.directive('myDirective', function () {
        return {
          restrict: 'EA',
          compile: element => {
            element.hasCompiled = true ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement('<div data:kjs-attr-my-directive></div>') ;
      compile.run(el) ;
      expect(el[0].hasCompiled).to.be.true ;
    }) ;
  }) ;


  describe('Restricting Directive Application', function () {

    Utils.forEach({
      E:    {element: true,  attribute: false},
      A:    {element: false, attribute: true },
      EA:   {element: true,  attribute: true },
    }, function (expected, restrict) {

      describe('restricted to ' + restrict, function () {

        Utils.forEach({
          element:   '<my-directive></my-directive>',
          attribute: '<div my-directive></div>',
        }, function (dom, type) {

          it((expected[type] ? 'matches' : 'does not match') + ' on ' + type,
          function () {

            let hasCompiled = false ;
            compile.directive('myDirective', function () {
              return {
                restrict: restrict,
                compile: () => {
                  hasCompiled = true ;
                }
              } ;
            }) ;
            let el = Utils.str2DomElement(dom) ;
            compile.run(el) ;
            expect(hasCompiled).to.be.equal(expected[type]) ;
          }) ;
        }) ;
      }) ;
    }) ;

    it('applies to attributes when no restrict given', function () {

      let hasCompiled = false ;
      compile.directive('myDirective', function () {
        return {
          compile: () => {
            hasCompiled = true ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el) ;
      expect(hasCompiled).to.be.true ;
    }) ;

    it('applies to element when no restrict given', function () {

      let hasCompiled = false ;
      compile.directive('myDirective', function () {
        return {
          compile: () => {
            hasCompiled = true ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement('<my-directive></my-directive>') ;
      compile.run(el) ;
      expect(hasCompiled).to.be.true ;
    }) ;

    it('does not apply to classes when no restrict given', function () {

      let hasCompiled = false ;
      compile.directive('myDirective', function () {
        return {
          compile: () => {
            hasCompiled = true ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement('<div class="my-directive"></div>') ;
      compile.run(el) ;
      expect(hasCompiled).to.be.false ;
    }) ;
  }) ;


  describe('Prioritizing Directives', function () {

    it('applies in priority order', function () {

      let compilations = [] ;
      compile.directive({
        lowerDirective: function () {
          return {
            priority: 1,
            compile: () => {
              compilations.push('lower') ;
            }
          } ;
        },
        higherDirective: function () {
          return {
            priority: 2,
            compile: () => {
              compilations.push('higher') ;
            }
          } ;
        }
      }) ;

      let el = Utils.str2DomElement(
        '<div lower-directive higher-directive></div>') ;
      compile.run(el) ;
      expect(compilations).to.be.deep.equal(['higher', 'lower']) ;
    }) ;

    it('applies in name order when priorities are the same', function () {

      let compilations = [] ;
      compile.directive({
        firstDirective: function () {
          return {
            priority: 1,
            compile: () => {
              compilations.push('first') ;
            }
          } ;
        },
        secondDirective: function () {
          return {
            priority: 1,
            compile: () => {
              compilations.push('second') ;
            }
          } ;
        }
      }) ;

      let el = Utils.str2DomElement(
        '<div second-directive first-directive></div>') ;
      compile.run(el) ;
      expect(compilations).to.be.deep.equal(['first', 'second']) ;
    }) ;

    it('applies in registration order when names are the same', function () {

      let compilations = [] ;
      compile.directive('aDirective', function () {
        return {
          priority: 1,
          compile: () => {
            compilations.push('first') ;
          }
        } ;
      }) ;
      compile.directive('aDirective', function () {
        return {
          priority: 1,
          compile: () => {
            compilations.push('second') ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement('<div a-directive></div>') ;
      compile.run(el) ;
      expect(compilations).to.be.deep.equal(['first', 'second']) ;
    }) ;

    it('uses default priority when one not given', function () {

      let compilations = [] ;
      compile.directive('firstDirective', function () {
        return {
          priority: 1,
          compile: () => {
            compilations.push('first') ;
          }
        } ;
      }) ;
      compile.directive('secondDirective', function () {
        return {
          compile: () => {
            compilations.push('second') ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div second-directive first-directive></div>') ;
      compile.run(el) ;
      expect(compilations).to.be.deep.equal(['first', 'second']) ;
    }) ;
  }) ;


  describe('Terminating Compilation', function () {

    it('stops compiling at a terminal directive', function () {

      let compilations = [] ;
      compile.directive('firstDirective', function () {
        return {
          priority: 1,
          terminal: true,
          compile: () => {
            compilations.push('first') ;
          }
        } ;
      }) ;
      compile.directive('secondDirective', function () {
        return {
          priority: 0,
          compile: () => {
            compilations.push('second') ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div first-directive second-directive></div>') ;
      compile.run(el) ;
      expect(compilations).to.be.deep.equal(['first']) ;
    }) ;

    it('still compiles directives with same priority after terminal',
    function () {

      let compilations = [] ;
      compile.directive('firstDirective', function () {
        return {
          priority: 1,
          terminal: true,
          compile: () => {
            compilations.push('first') ;
          }
        } ;
      }) ;
      compile.directive('secondDirective', function () {
        return {
          priority: 1,
          compile: () => {
            compilations.push('second') ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div first-directive second-directive></div>') ;
      compile.run(el) ;
      expect(compilations).to.be.deep.equal(['first', 'second']) ;
    }) ;

    it('stops child compilation after a terminal directive', function () {

      let compilations = [] ;
      compile.directive('parentDirective', function () {
        return {
          terminal: true,
          compile: () => {
            compilations.push('parent') ;
          }
        } ;
      }) ;
      compile.directive('childDirective', function () {
        return {
          compile: () => {
            compilations.push('child') ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div parent-directive><div child-directive></div></div>') ;
      compile.run(el) ;
      expect(compilations).to.be.deep.equal(['parent']) ;
    }) ;
  }) ;


  describe('Applying Directives Across Multiple Nodes', function () {

    it('allows applying a directive to multiple elements', function () {

      let compileEl = false ;
      compile.directive('myDir', function () {
        return {
          multiElement: true,
          compile: element => {
            compileEl = element ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div my-dir-start></div><span></span><div my-dir-end></div>') ;
      compile.run(el) ;
      expect(compileEl.length).to.be.equal(3) ;
    }) ;
  }) ;


  describe('Directive Attributes', function () {

    function registerAndCompile (dirName, domString, callback) {

      let givenAttrs ;
      compile.directive(dirName, function () {
        return {
          restrict: 'EA',
          compile: (element, attrs) => {
            givenAttrs = attrs ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement(domString) ;
      compile.run(el) ;
      callback(el, givenAttrs, compile.kernel().get('$rootScope')) ;
    }


    /**
     * Passing Attributes to the compile Function
     */

    it('passes the element attributes to the compile function', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive my-attr="1" my-other-attr="two"></my-drective>',
        (element, attrs) => {
          expect(attrs.myAttr).to.be.equal('1') ;
          expect(attrs.myOtherAttr).to.be.equal('two') ;
        }
      ) ;
    }) ;

    it('trims attribute values', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive my-attr=" val "></my-drective>',
        (element, attrs) => {
          expect(attrs.myAttr).to.be.equal('val') ;
        }
      ) ;
    }) ;


    /**
     * Handling Boolean Attributes
     */

    it('sets the value of boolean attributes to true', function () {

      registerAndCompile(
        'myDirective',
        '<input my-directive disabled>',
        (element, attrs) => {
          expect(attrs.disabled).to.be.equal(true) ;
        }
      ) ;
    }) ;

    it('does not set the value of custom boolean attributes to true',
    function () {

      registerAndCompile(
        'myDirective',
        '<input my-directive whatever>',
        (element, attrs) => {
          expect(attrs.whatever).to.be.equal('') ;
        }
      ) ;
    }) ;


    /**
     * Overriding attributes with kjs-attr
     */

    it('overrides attributes with kjs-attr- versions', function () {

      registerAndCompile(
        'myDirective',
        '<input my-directive kjs-attr-whatever="2016" whatever="2015">',
        (element, attrs) => {
          expect(attrs.whatever).to.be.equal('2016') ;
        }
      ) ;
    }) ;


    /**
     * Setting Attributes
     */

    it('allows stting attributes', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive attr="true"></my-directive>',
        (element, attrs) => {
          attrs.$set('attr', 'false') ;
          expect(attrs.attr).to.be.equal('false') ;
        }
      ) ;
    }) ;

    it('sets attributes to DOM', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive attr="true"></my-directive>',
        (element, attrs) => {
          attrs.$set('attr', 'false') ;
          expect(element[0].getAttribute('attr')).to.be.equal('false') ;
        }
      ) ;
    }) ;

    it('does not set attributes to DOM when flag is false', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive attr="true"></my-directive>',
        (element, attrs) => {
          attrs.$set('attr', 'false', false) ;
          expect(element[0].getAttribute('attr')).to.be.equal('true') ;
        }
      ) ;
    }) ;

    it('shares attributes between directives', function () {

      let attrs1, attrs2 ;
      compile.directive({
        myDir: function () {
          return {
            compile: (element, attrs) => {
              attrs1 = attrs ;
            }
          } ;
        },
        myOtherDir: function () {
          return {
            compile: (element, attrs) => {
              attrs2 = attrs ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement('<div my-dir my-other-dir></div>') ;
      compile.run(el) ;
      expect(attrs1).to.be.equal(attrs2) ;
    }) ;

    /**
     * Setting Boolean Properties
     */

    it('sets prop for boolean attributes', function () {

      registerAndCompile(
        'myDirective',
        '<input my-directive>',
        (element, attrs) => {
          attrs.$set('disabled', true) ;
          expect(element[0].disabled).to.be.equal(true) ;
        }
      ) ;
    }) ;

    it('sets prop for boolean attributes', function () {

      registerAndCompile(
        'myDirective',
        '<input my-directive>',
        (element, attrs) => {
          attrs.$set('disabled', true, false) ;
          expect(element[0].disabled).to.be.equal(true) ;
        }
      ) ;
    }) ;


    /**
     * Denormalizing Attribute Names for The DOM
     */

    it('denormalizes attribute name when explicitly given', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive some-attribute="2015"></my-directive>',
        (element, attrs) => {
          attrs.$set('someAttribute', 2016, true, 'some-attribute') ;
          expect(element[0].getAttribute('some-attribute'))
            .to.be.equal('2016') ;
        }
      ) ;
    }) ;

    it('denormalizes attribute by snake-casing', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive some-attribute="2015"></my-directive>',
        (element, attrs) => {
          attrs.$set('someAttribute', 2016) ;
          expect(element[0].getAttribute('some-attribute'))
            .to.be.equal('2016') ;
        }
      ) ;
    }) ;

    it('denormalizes attribute by using original attribute name', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive x-some-attribute="2015"></my-directive>',
        (element, attrs) => {
          attrs.$set('someAttribute', 2016) ;
          expect(element[0].getAttribute('x-some-attribute'))
            .to.be.equal('2016') ;
        }
      ) ;
    }) ;

    it('does not use kjs-attr- prefix in denormalized names', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive kjs-some-attribute="2015"></my-directive>',
        (element, attrs) => {
          attrs.$set('someAttribute', 2016) ;
          expect(element[0].getAttribute('some-attribute'))
            .to.be.equal('2016') ;
        }
      ) ;
    }) ;

    it('uses new attribute name after once given', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive x-some-attribute="2015"></my-directive>',
        (element, attrs) => {
          attrs.$set('someAttribute', 2016, true, 'some-attribute') ;
          attrs.$set('someAttribute', 2017) ;

          expect(element[0].getAttribute('some-attribute'))
            .to.be.equal('2017') ;
          expect(element[0].getAttribute('x-some-attribute'))
            .to.be.equal('2015') ;
        }
      ) ;
    }) ;


    /**
     * Observing Attributes
     */

    it('calls observer immediately when attribute is $set', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive some-attribute="2015"></my-directive>',
        (element, attrs) => {

          let gotValue ;
          attrs.$observe('someAttribute', function (value) {
            gotValue = value ;
          }) ;

          attrs.$set('someAttribute', '2016') ;
          expect(gotValue).to.be.equal('2016') ;
        }
      ) ;
    }) ;

    it('calls observer on next $digest after registration', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive some-attribute="2015"></my-directive>',
        (element, attrs, $rootScope) => {

          let gotValue ;
          attrs.$observe('someAttribute', function (value) {
            gotValue = value ;
          }) ;

          $rootScope.$digest() ;

          expect(gotValue).to.be.equal('2015') ;
        }
      ) ;
    }) ;

    it('lets observers be deregistered', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive some-attribute="2015"></my-directive>',
        (element, attrs) => {

          let gotValue ;
          let remove = attrs.$observe('someAttribute', function (value) {
            gotValue = value ;
          }) ;

          attrs.$set('someAttribute', '2016') ;
          expect(gotValue).to.be.equal('2016') ;

          remove() ;
          attrs.$set('someAttribute', '2017') ;
          expect(gotValue).to.be.equal('2016') ;
        }
      ) ;
    }) ;


    /**
     * Manipulating Classes
     */

    it('allows adding classes', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive></my-directive>',
        (element, attrs) => {
          attrs.$addClass('some-class') ;
          expect(element[0].classList.contains('some-class')).to.be.true ;
        }
      ) ;
    }) ;

    it('allows removing classes', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive class="some-class"></my-directive>',
        (element, attrs) => {
          attrs.$removeClass('some-class') ;
          expect(element[0].classList.contains('some-class')).to.be.false ;
        }
      ) ;
    }) ;

    it('allows updating classes', function () {

      registerAndCompile(
        'myDirective',
        '<my-directive class="one three four"></my-directive>',
        (element, attrs) => {
          attrs.$updateClass('one two three', 'one three four') ;
          expect(element[0].classList.contains('one')).to.be.true ;
          expect(element[0].classList.contains('two')).to.be.true ;
          expect(element[0].classList.contains('three')).to.be.true ;
          expect(element[0].classList.contains('four')).to.be.false ;
        }
      ) ;
    }) ;
  }) ;


  describe('Directive Linking and Scopes', function () {

    let rootScope ;

    beforeEach(function () {

      rootScope = new Scope() ;
    }) ;


    /**
     * The Public Link Function
     */

    it('returns a public link function from compile', function () {

      compile.directive('myDirective', function () {
        return { compile: function () {} } ;
      }) ;
      let el = Utils.str2DomElement('<div my-directive></div>') ;
      let linkFn = compile.run(el) ;

      expect(linkFn).to.be.not.undefined ;
      expect(Utils.isFunction(linkFn)).to.be.true ;
    }) ;

    it('takes a scope and attaches it to elements', function () {

      compile.directive('myDirective', function () {
        return { compile: function () {} } ;
      }) ;
      let el = Utils.str2DomElement('<div my-directive></div>') ;

      compile.run(el)(rootScope) ;
      expect(el.$scope).to.be.equal(rootScope) ;
    }) ;


    /**
     * Directive Link Function
     */

    it('calls directive link function with scope', function () {

      let givenScope, givenElement, givenAttrs ;
      compile.directive('myDirective', function () {
        return {
          compile: function () {
            return function link (scope, element, attrs) {
              givenScope   = scope ;
              givenElement = element ;
              givenAttrs   = attrs ;
            } ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement('<div my-directive></div>') ;

      compile.run(el)(rootScope) ;
      expect(givenScope).to.be.equal(rootScope) ;
      expect(givenElement).to.be.equal(el[0]) ;
      expect(givenAttrs).to.be.not.undefined ;
      expect(givenAttrs.myDirective).to.be.not.undefined ;
    }) ;


    /**
     * Plain Directive Link Functions
     */

    it('supports link function in directive definition object', function () {

      let givenScope, givenElement, givenAttrs ;
      compile.directive('myDirective', function () {
        return {
          link: function (scope, element, attrs) {
            givenScope   = scope ;
            givenElement = element ;
            givenAttrs   = attrs ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement('<div my-directive></div>') ;

      compile.run(el)(rootScope) ;
      expect(givenScope).to.be.equal(rootScope) ;
      expect(givenElement).to.be.equal(el[0]) ;
      expect(givenAttrs).to.be.not.undefined ;
      expect(givenAttrs.myDirective).to.be.not.undefined ;
    }) ;


    /**
     * Linking Child Nodes
     */

    it('links directive on child element first', function () {

      let givenElements = [] ;
      compile.directive('myDirective', function () {
        return {
          link: function (scope, element) {
            givenElements.push(element) ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive><div my-directive></div></div>') ;
      compile.run(el)(rootScope) ;

      expect(givenElements.length).to.be.equal(2) ;
      expect(givenElements[0]).to.be.equal(el[0].firstChild) ;
      expect(givenElements[1]).to.be.equal(el[0]) ;
    }) ;

    it('links children when parents has no directives', function () {

      let givenElements = [] ;
      compile.directive('myDirective', function () {
        return {
          link: function (scope, element) {
            givenElements.push(element) ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement('<div><div my-directive></div></div>') ;
      compile.run(el)(rootScope) ;
      expect(givenElements.length).to.be.equal(1) ;
      expect(givenElements[0]).to.be.equal(el[0].firstChild) ;
    }) ;


    /**
     * Pre- And Post-Linking
     */

    it('supports link function objects', function () {

      let linked ;
      compile.directive('myDirective', function () {
        return {
          link: {
            post: function () {
              linked = true ;
            }
          }
        } ;
      }) ;
      let el = Utils.str2DomElement('<div><div my-directive></div></div>') ;
      compile.run(el)(rootScope) ;
      expect(linked).to.be.true ;
    }) ;

    it('supports prelinking and postlinking', function () {

      let linkings = [] ;
      compile.directive('myDirective', function () {
        return {
          link: {
            pre: function (scope, element) {
              linkings.push(['pre', element]) ;
            },
            post: function (scope, element) {
              linkings.push(['post', element]) ;
            }
          }
        } ;
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive><div my-directive></div></div>') ;
      compile.run(el)(rootScope) ;

      expect(linkings.length).to.be.equal(4) ;
      expect(linkings[0]).to.be.deep.equal(['pre', el[0]]) ;
      expect(linkings[1]).to.be.deep.equal(['pre', el[0].firstChild]) ;
      expect(linkings[2]).to.be.deep.equal(['post', el[0].firstChild]) ;
      expect(linkings[3]).to.be.deep.equal(['post', el[0]]) ;
    }) ;

    it('reverses priority for postlink functions', function () {

      let linkings = [] ;
      compile.directive({
        firstDirective: function () {
          return {
            priority: 2,
            link: {
              pre: function () {
                linkings.push('first-pre') ;
              },
              post: function () {
                linkings.push('first-post') ;
              }
            }
          } ;
        },
        secondDirective: function () {
          return {
            priority: 1,
            link: {
              pre: function () {
                linkings.push('second-pre') ;
              },
              post: function () {
                linkings.push('second-post') ;
              }
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div first-directive second-directive></div>') ;
      compile.run(el)(rootScope) ;

      expect(linkings).to.be.deep.equal([
        'first-pre',
        'second-pre',
        'second-post',
        'first-post'
      ]) ;
    }) ;


    /**
     * Keeping The Node List Stable for Linking
     */

    it('stabilizes node list during linking', function () {

      let givenElements = [] ;
      compile.directive('myDirective', function () {
        return {
          link: function (scope, element) {
            givenElements.push(element) ;
            element.after('<div></div>') ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement(
        '<div><div my-directive></div><div my-directive></div></div>') ;
      let el1 = el[0].childNodes[0],
          el2 = el[0].childNodes[1] ;
      compile.run(el)(rootScope) ;

      expect(givenElements.length).to.be.equal(2) ;
      expect(givenElements[0]).to.be.equal(el1) ;
      expect(givenElements[1]).to.be.equal(el2) ;
    }) ;


    /**
     * Linking Directives Across Multiple Nodes
     */

    it('invokes multi-element directive link functions with whole group',
    function () {

      let givenElements ;
      compile.directive('myDirective', function () {
        return {
          multiElement: true,
          link: function (scope, element) {
            givenElements = element ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive-start></div>'
        + '<p></p>'
        + '<div my-directive-end></div>'
      ) ;
      compile.run(el)(rootScope) ;
      expect(givenElements.length).to.be.equal(3) ;
    }) ;


    /**
     * Linking And Scope Inheritance
     */

    it('makes new scope for element when directive asks for it', function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: true,
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(givenScope.$parent).to.be.equal(rootScope) ;
    }) ;

    it('gives inherited scope to all directives on element', function () {

      let givenScope ;
      compile.directive({
        myDirective: function () {
          return {
            scope: true
          } ;
        },
        myOtherDirective: function () {
          return {
            link: function (scope) {
              givenScope = scope ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive my-other-directive></div>') ;
      compile.run(el)(rootScope) ;

      expect(givenScope.$parent).to.be.equal(rootScope) ;
    }) ;

    it('adds scope class and data for element whith new scope', function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: true,
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el)(rootScope) ;

      expect(el[0].classList.contains('kjs-scope')).to.be.true ;
      expect(el[0].$scope).to.be.equal(givenScope) ;
    }) ;


    /**
     * Isolate Scopes
     */

    it('creates an isolate scope when requested', function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: {},
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(givenScope.$parent).to.be.equal(rootScope) ;
      expect(Object.getPrototypeOf(givenScope)).to.be.not.equal(rootScope) ;
    }) ;

    it('does not share isolate scope with other directives', function () {

      let givenScope ;
      compile.directive({
        myDirective: function () {
          return {
            scope: {}
          } ;
        },
        myOtherDirective: function () {
          return {
            link: function (scope) {
              givenScope = scope ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive my-other-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(givenScope).to.be.equal(rootScope) ;
    }) ;

    it('does not use isolate scope on child elements', function () {

      let givenScope ;
      compile.directive({
        myDirective: function () {
          return {
            scope: {}
          } ;
        },
        myOtherDirective: function () {
          return {
            link: function (scope) {
              givenScope = scope ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive><div my-other-directive></div></div>') ;
      compile.run(el)(rootScope) ;
      expect(givenScope).to.be.equal(rootScope) ;
    }) ;

    it('does not allow two isolate scope directives on an element',
    function () {

      compile.directive({
        myDirective: function () {
          return {
            scope: {}
          } ;
        },
        myOtherDirective: function () {
          return {
            scope: {}
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive my-other-directive></div>') ;
      expect(() => {
        compile.run(el) ;
      }).to.throw() ;
    }) ;

    it('does not allow both isolate and inherited scope directives '
    + 'on an element', function () {

      compile.directive({
        myDirective: function () {
          return {
            scope: {}
          } ;
        },
        myOtherDirective: function () {
          return {
            scope: true
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive my-other-directive></div>') ;
      expect(() => {
        compile.run(el) ;
      }).to.throw() ;
    }) ;

    it('adds class and data for element with isolated scope', function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: {},
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el)(rootScope) ;

      expect(el[0].classList.contains('kjs-isolate-scope')).to.be.true ;
      expect(el[0].classList.contains('kjs-scope')).to.be.false ;
      expect(el[0].$isolateScope).to.be.equal(givenScope) ;
    }) ;


    /**
     * Isolate Attribute Bindings
     */

    it('allows observing attribute to the isolate scope', function () {

      let givenScope, givenAttrs ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            anAttr: '@'
          },
          link: function (scope, element, attrs) {
            givenScope = scope ;
            givenAttrs = attrs ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el)(rootScope) ;

      givenAttrs.$set('anAttr', '2016') ;
      expect(givenScope.anAttr).to.be.equal('2016') ;
    }) ;

    it('sets initial value of observed attr to the isolate scope',
    function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            anAttr: '@'
          },
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement('<div my-directive an-attr="2016"></div>') ;
      compile.run(el)(rootScope) ;
      expect(givenScope.anAttr).to.be.equal('2016') ;
    }) ;

    it('allows aliasing observed attribute', function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            aScopeAttr: '@anAttr'
          },
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement('<div my-directive an-attr="2016"></div>') ;
      compile.run(el)(rootScope) ;
      expect(givenScope.aScopeAttr).to.be.equal('2016') ;
    }) ;


    /**
     * Bi-Directional Data Binding
     */

    it('allows binding expression to isolate scope', function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            anAttr: '='
          },
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement('<div my-directive an-attr="2016"></div>') ;
      compile.run(el)(rootScope) ;
      expect(givenScope.anAttr).to.be.equal(2016) ;
    }) ;

    it('allows aliasing expression attribute on isolate scope', function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            myAttr: '=theAttr'
          },
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive the-attr="2016"></div>') ;
      compile.run(el)(rootScope) ;
      expect(givenScope.myAttr).to.be.equal(2016) ;
    }) ;

    it('evaluates isolate scope expression on parent scope', function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            myAttr: '='
          },
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      rootScope.parentAttr = 2015 ;
      let el = Utils.str2DomElement(
        '<div my-directive my-attr="parentAttr + 1"></div>') ;
      compile.run(el)(rootScope) ;
      expect(givenScope.myAttr).to.be.equal(2016) ;
    }) ;

    it('watches isolated scope expressions', function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            myAttr: '='
          },
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive my-attr="parentAttr + 1"></div>') ;
      compile.run(el)(rootScope) ;

      rootScope.parentAttr = 2015 ;
      rootScope.$digest() ;
      expect(givenScope.myAttr).to.be.equal(2016) ;
    }) ;

    it('allows assigning to isolated scope expressions', function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            myAttr: '='
          },
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive my-attr="parentAttr"></div>') ;
      compile.run(el)(rootScope) ;

      givenScope.myAttr = 2016 ;
      rootScope.$digest() ;
      expect(rootScope.parentAttr).to.be.equal(2016) ;
    }) ;

    it('gives parent change precedence when both parent and child change',
    function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            myAttr: '='
          },
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive my-attr="parentAttr"></div>') ;
      compile.run(el)(rootScope) ;

      rootScope.parentAttr = 2016 ;
      givenScope.myAttr = 2017 ;
      rootScope.$digest() ;
      expect(rootScope.parentAttr).to.be.equal(2016) ;
      expect(givenScope.myAttr).to.be.equal(2016) ;
    }) ;

    it('throws when isolate scope expression returns new arrays', function () {

      compile.directive('myDirective', function () {
        return {
          scope: {
            myAttr: '='
          },
          link: function () {}
        } ;
      }) ;
      rootScope.parentFunction = () => {
        return [1, 2, 3] ;
      } ;
      let el = Utils.str2DomElement(
        '<div my-directive my-attr="parentFunction()"></div>') ;
      compile.run(el)(rootScope) ;
      expect(function () {
        rootScope.$digest() ;
      }).to.throw() ;
    }) ;

    it('can watch isolated scope expressions as collections', function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            myAttr: '=*'
          },
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      rootScope.parentFunction = function () {
        return [1, 2, 3] ;
      } ;
      let el = Utils.str2DomElement(
        '<div my-directive my-attr="parentFunction()"></div>') ;

      compile.run(el)(rootScope) ;
      rootScope.$digest() ;
      expect(givenScope.myAttr).to.be.deep.equal([1, 2, 3]) ;
    }) ;

    it('does not watch optional missiong isolate scope expressions',
    function () {

      compile.directive('myDirective', function () {
        return {
          scope: {
            myAttr: '=?'
          },
          link: function () {}
        } ;
      }) ;
      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(rootScope._watchers.length).to.be.equal(0) ;
    }) ;


    /**
     * Expression Binding
     */

    it('allows binding an invokable expression on the parent scope',
    function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            myExpr: '&'
          },
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      rootScope.parentFunction = function () {
        return 2016 ;
      } ;
      let el = Utils.str2DomElement(
        '<div my-directive my-expr="parentFunction() + 1"></div>') ;
      compile.run(el)(rootScope) ;
      expect(givenScope.myExpr()).to.be.equal(2017) ;
    }) ;

    it('allows passing an arguments to parent scope expression', function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            myExpr: '&'
          },
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      let gotArg ;
      rootScope.parentFunction = function (arg) {
        gotArg = arg ;
      } ;
      let el = Utils.str2DomElement(
        '<div my-directive my-expr="parentFunction(argFromChild)"></div>') ;

      compile.run(el)(rootScope) ;
      givenScope.myExpr({argFromChild: 2016}) ;
      expect(gotArg).to.be.equal(2016) ;
    }) ;

    it('sets missing optional parent scope expression to undefined',
    function () {

      let givenScope ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            myExpr: '&?'
          },
          link: function (scope) {
            givenScope = scope ;
          }
        } ;
      }) ;
      rootScope.parentFunction = function () {} ;
      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(givenScope.myExpr).to.be.undefined ;
    }) ;
  }) ;


  describe('Controllers', function () {

    let rootScope ;
    let controllerProvider ;

    beforeEach(function () {

      rootScope = new Scope() ;
      controllerProvider = compile.kernel().get('$controller') ;
    }) ;


    /**
     * Directive Controllers
     */

    it('can be attached to directives as functions', function () {

      let controllerInvoked ;
      compile.directive('myDirective', function () {
        return {
          controller: function MyController () {
            controllerInvoked = true ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(controllerInvoked).to.be.equal(true) ;
    }) ;

    it('can be attached to directives as string references', function () {

      let controllerInvoked ;
      function MyController () {
        controllerInvoked = true ;
      }
      controllerProvider.register('MyController', MyController) ;
      compile.directive('myDirective', function () {
        return { controller: 'MyController' } ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(controllerInvoked).to.be.true ;
    }) ;

    it('can be applied in the same element independent of each other',
    function () {

      let controllerInvoked ;
      let otherControllerInvoked ;
      function MyController () {
        controllerInvoked = true ;
      }
      function MyOtherController () {
        otherControllerInvoked = true ;
      }
      controllerProvider.register('MyController', MyController) ;
      controllerProvider.register('MyOtherController', MyOtherController) ;
      compile.directive('myDirective', function () {
        return { controller: 'MyController'} ;
      }) ;
      compile.directive('myOtherDirective', function () {
        return { controller: 'MyOtherController' } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div my-directive my-other-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(controllerInvoked).to.be.true ;
      expect(otherControllerInvoked).to.be.true ;
    }) ;

    it('can be applied to different directives, as different instances',
    function () {

      let invocations = 0 ;
      function MyController () {
        invocations++ ;
      }
      controllerProvider.register('MyController', MyController) ;
      compile.directive('myDirective', function () {
        return { controller: 'MyController'} ;
      }) ;
      compile.directive('myOtherDirective', function () {
        return { controller: 'MyController'} ;
      }) ;

      let el = Utils.str2DomElement(
        '<div my-directive my-other-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(invocations).to.be.equal(2) ;
    }) ;

    it('can be aliased with @ when given in directive attribute', function () {

      let controllerInvoked ;
      function MyController () {
        controllerInvoked = true ;
      }
      controllerProvider.register('MyController', MyController) ;
      compile.directive('myDirective', function () {
        return { controller: '@'} ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive="MyController"></div>') ;
      compile.run(el)(rootScope) ;
      expect(controllerInvoked).to.be.equal(true) ;
    }) ;


    /**
     * Locals in Directive Controllers
     */

    it('gets scope, element and attrs through DI', function () {

      let gotScope, gotElement, gotAttrs ;
      function MyController ($element, $scope, $attrs) {
        gotElement = $element ;
        gotScope   = $scope ;
        gotAttrs   = $attrs ;
      }
      controllerProvider.register('MyController', MyController) ;
      compile.directive('myDirective', function () {
        return { controller: 'MyController' } ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive an-attr="abc"></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotElement).to.be.equal(el[0]) ;
      expect(gotScope).to.be.equal(rootScope) ;
      expect(gotAttrs).to.be.not.undefined ;
      expect(gotAttrs.anAttr).to.be.equal('abc') ;
    }) ;

    /**
     * Attaching Directive Controllers on The Scope
     */

    it('can be attached on the scope', function () {

      function MyController () {}
      controllerProvider.register('MyController', MyController) ;
      compile.directive('myDirective', function () {
        return {
          controller:   'MyController',
          controllerAs: 'myCtrl'
        } ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(rootScope.myCtrl).to.be.not.undefined ;
      expect(rootScope.myCtrl).to.be.instanceOf(MyController) ;
    }) ;


    /**
     * Controllers on Isolate Scope Directives
     */

    it('gets isolate scope as injected $scope', function () {

      let gotScope ;
      function MyController ($scope) {
        gotScope = $scope ;
      }
      controllerProvider.register('MyController', MyController) ;
      compile.directive('myDirective', function () {
        return {
          scope: {},
          controller: 'MyController'
        } ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotScope).not.to.be.equal(rootScope) ;
    }) ;

    it('has isolate scope binding available during construction', function () {

      let gotMyAttr ;
      function MyController ($scope) {
        gotMyAttr = $scope.myAttr ;
      }
      controllerProvider.register('MyController', MyController) ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            myAttr: '@myDirective'
          },
          controller:   'MyController'
        } ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive="abc"></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotMyAttr).to.be.equal('abc') ;
    }) ;

    it('can bind isolate scope bindings directly to self', function () {

      let gotMyAttr ;
      function MyController () {
        gotMyAttr = this.myAttr ;
      }
      controllerProvider.register('MyController', MyController) ;
      compile.directive('myDirective', function () {
        return {
          scope: {
            myAttr: '@myDirective'
          },
          controller: 'MyController',
          bindToController: true
        } ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive="abc"></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotMyAttr).to.be.equal('abc') ;
    }) ;

    it('can return a semi-constructed controller', function () {

      function MyController () {
        this.constructed = true ;
        this.myAttrWhenConstructed = this.myAttr ;
      }

      let controller = controllerProvider.get(MyController, null, true) ;

      expect(controller.constructed).to.be.undefined ;
      expect(controller.instance).to.be.not.undefined ;

      controller.instance.myAttr = 2016 ;
      let actualController = controller() ;

      expect(actualController.constructed).to.be.not.undefined ;
      expect(actualController.myAttrWhenConstructed).to.be.equal(2016) ;
    }) ;

    it('can bind iso scope bindings through bindToController', function () {

      let gotMyAttr ;
      function MyController () {
        gotMyAttr = this.myAttr ;
      }
      controllerProvider.register('MyController', MyController) ;
      compile.directive('myDirective', function () {
        return {
          scope: {},
          controller:   'MyController',
          bindToController: {
            myAttr: '@myDirective'
          }
        } ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive="abc"></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotMyAttr).to.be.equal('abc') ;
    }) ;

    it('can bind through bindToController without iso scope', function () {

      let gotMyAttr ;
      function MyController () {
        gotMyAttr = this.myAttr ;
      }
      controllerProvider.register('MyController', MyController) ;
      compile.directive('myDirective', function () {
        return {
          scope: true,
          controller:   'MyController',
          bindToController: {
            myAttr: '@myDirective'
          }
        } ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive="abc"></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotMyAttr).to.be.equal('abc') ;
    }) ;


    /**
     * Requiring Controllers
     */

    it('can be required from a sibling directive', function () {

      function MyController () {}
      let gotMyController ;
      compile.directive('myDirective', function () {
        return {
          scope: {},
          controller: MyController
        } ;
      }) ;
      compile.directive('myOtherDirective', function () {
        return {
          require: 'myDirective',
          link: function (scope, element, attrs, myController) {
            gotMyController = myController ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div my-directive my-other-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotMyController).to.be.not.undefined ;
      expect(gotMyController).to.be.instanceOf(MyController) ;
    }) ;


    /**
     * Requiring Multiple Controllers
     */

    it('can be required from multiple sibling directives', function () {

      function MyController () {}
      function MyOtherController () {}
      let gotMyControllers ;
      compile.directive('myDirective', function () {
        return {
          scope: true,
          controller: MyController
        } ;
      }) ;
      compile.directive('myOtherDirective', function () {
        return {
          scope: true,
          controller: MyOtherController
        } ;
      }) ;
      compile.directive('myThirdDirective', function () {
        return {
          require: ['myDirective', 'myOtherDirective'],
          link: function (scope, element, attrs, controllers) {
            gotMyControllers = controllers ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div my-directive my-other-directive my-third-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotMyControllers).to.be.not.undefined ;
      expect(gotMyControllers.length).to.be.equal(2) ;
      expect(gotMyControllers[0]).to.be.instanceOf(MyController) ;
      expect(gotMyControllers[1]).to.be.instanceOf(MyOtherController) ;
    }) ;


    /**
     * Self-Requiring Directives
     */

    it('is passed to link functions if there is no require', function () {

      function MyController () {}
      let gotMyController ;
      compile.directive('myDirective', function () {
        return {
          scope: {},
          controller: MyController,
          link: function (scope, element, attrs, myController) {
            gotMyController = myController ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotMyController).to.be.not.undefined ;
      expect(gotMyController).to.be.instanceof(MyController) ;
    }) ;


    /**
     * Requiring Controllers in Multi-Element Directives
     */

    it('is passed through grouped link wrapper', function () {

      function MyController () {}
      let gotMyController ;
      compile.directive('myDirective', function () {
        return {
          multiElement: true,
          scope: {},
          controller: MyController,
          link: function (scope, element, attrs, myController) {
            gotMyController = myController ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div my-directive-start></div><div my-directive-end></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotMyController).to.be.not.undefined ;
      expect(gotMyController).to.be.instanceOf(MyController) ;
    }) ;


    /**
     * Requiring Controllers from Parent Element
     */

    it('can be required from a parent directive', function () {

      function MyController () {}
      let gotMyController ;
      compile.directive('myDirective', function () {
        return {
          scope: {},
          controller: MyController
        } ;
      }) ;
      compile.directive('myOtherDirective', function () {
        return {
          require: '^myDirective',
          link: function (scope, element, attrs, myController) {
            gotMyController = myController ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div my-directive><div my-other-directive></div></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotMyController).to.be.not.undefined ;
      expect(gotMyController).to.be.instanceOf(MyController) ;
    }) ;

    it('finds from sibling directive when requiring with parent prefix',
    function () {

      function MyController () {}
      let gotMyController ;
      compile.directive('myDirective', function () {
        return {
          scope: {},
          controller: MyController
        } ;
      }) ;
      compile.directive('myOtherDirective', function () {
        return {
          require: '^myDirective',
          link: function (scope, element, attrs, myController) {
            gotMyController = myController ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div my-directive my-other-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotMyController).to.be.not.undefined ;
      expect(gotMyController).to.be.instanceOf(MyController) ;
    }) ;

    it('can be required from a parent directive with ^^', function () {

      function MyController () {}
      let gotMyController ;
      compile.directive('myDirective', function () {
        return {
          scope: {},
          controller: MyController
        } ;
      }) ;
      compile.directive('myOtherDirective', function () {
        return {
          require: '^^myDirective',
          link: function (scope, element, attrs, myController) {
            gotMyController = myController ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div my-directive><div my-other-directive></div></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotMyController).to.be.not.undefined ;
      expect(gotMyController).to.be.instanceOf(MyController) ;
    }) ;

    it('does not find from sibling directive when requiring with ^^',
    function () {

      function MyController () {}
      compile.directive('myDirective', function () {
        return {
          scope: {},
          controller: MyController
        } ;
      }) ;
      compile.directive('myOtherDirective', function () {
        return {
          require: '^^myDirective',
          link: function () {}
        } ;
      }) ;

      let el =
        Utils.str2DomElement('<div my-directive my-other-directive></div>') ;
      expect(function () {
        compile.run(el)(rootScope) ;
      }).to.throw() ;
    }) ;


    /**
     * Optionally Requiring Controllers
     */

    it('does not throw on required missing controller when optional',
    function () {

      let gotCtrl ;
      compile.directive('myDirective', function () {
        return {
          require: '?noSuchDirective',
          link: function (scope, element, attrs, ctrl) {
            gotCtrl = ctrl ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotCtrl).to.be.equal(null) ;
    }) ;

    it('allows optional marker after parent marker', function () {

      let gotCtrl ;
      compile.directive('myDirective', function () {
        return {
          require: '^?noSuchDirective',
          link: function (scope, element, attrs, ctrl) {
            gotCtrl = ctrl ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotCtrl).to.be.equal(null) ;
    }) ;

    it('allows optional marker before parent marker', function () {

      function MyController () {}
      let gotMyController ;
      compile.directive('myDirective', function () {
        return {
          scope: {},
          controller: MyController
        } ;
      }) ;
      compile.directive('myOtherDirective', function () {
        return {
          require: '?^myDirective',
          link: function (scope, element, attrs, ctrl) {
            gotMyController = ctrl ;
          }
        } ;
      }) ;

      let el = Utils.str2DomElement(
        '<div my-directive my-other-directive></div>') ;
      compile.run(el)(rootScope) ;
      expect(gotMyController).to.be.not.undefined ;
      expect(gotMyController).to.instanceOf(MyController) ;
    }) ;

  }) ;


  describe('Directive Templates', function () {

    let rootScope ;

    beforeEach(function () {

      rootScope = new Scope() ;
    }) ;

    it('populates an element during compilation', function () {

      compile.directive('myDirective', function () {
        return {
          template: '<div class="from-template"></div>'
        } ;
      }) ;
      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el) ;
      expect(el[0].querySelectorAll('.from-template').length).to.be.equal(1) ;
    }) ;

    it('replaces any existing children', function () {

      compile.directive('myDirective', function () {
        return {
          template: '<div class="from-template"></div>'
        } ;
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive><div class="existing"></div></div>') ;
      compile.run(el) ;
      expect(el[0].querySelectorAll('.existing').length).to.be.equal(0) ;
    }) ;

    it('compiles template contents also', function () {

      let compileSpy = sinon.spy() ;
      compile.directive({
        myDirective: function () {
          return {
            template: '<div my-other-directive></div>'
          } ;
        },
        myOtherDirective: function () {
          return {
            compile: compileSpy
          } ;
        }
      }) ;
      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el) ;
      expect(compileSpy.called).to.be.true ;
    }) ;


    /**
     * Disallowing More Than One Template Directive Per Element
     */

    it('does not lalow two directives with templates', function () {

      compile.directive({
        myDirective: function () {
          return {
            template: '<div></div>'
          } ;
        },
        myOtherDirective: function () {
          return {
            template: '<div></div>'
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive my-other-directive></div>') ;
      expect(function () {
        compile.run(el) ;
      }).to.throw() ;
    }) ;


    /**
     * Template Functions
     */

    it('supports functions as template values', function () {

      function tmpl () {
        return '<div class="from-template"></div>' ;
      }
      compile.directive({
        myDirective: function () {
          return {
            template: tmpl
          } ;
        }
      }) ;
      let el = Utils.str2DomElement('<div my-directive></div>') ;
      compile.run(el) ;
      expect(el[0].querySelectorAll('.from-template').length).to.be.equal(1) ;
    }) ;


    /**
     * Isolate Scope Directives with Templates
     */

    it('uses isolate scope for template contents', function () {

      let linkSpy = sinon.spy() ;
      compile.directive({
        myDirective: function () {
          return {
            scope: {
              isoValue: '=myDirective'
            },
            template: '<div my-other-directive></div>'
          } ;
        },
        myOtherDirective: function () {
          return {link: linkSpy} ;
        }
      }) ;
      let el = Utils.str2DomElement('<div my-directive="2016"></div>') ;
      compile.run(el)(rootScope) ;
      expect(linkSpy.firstCall.args[0]).not.to.be.equal(rootScope) ;
      expect(linkSpy.firstCall.args[0].isoValue).to.be.equal(2016) ;
    }) ;


    /**
     * Asynchronous Templates: templateUrl
     */

    describe('templateUrl', function () {

      let xhr, requests ;

      beforeEach(function () {

        xhr = sinon.useFakeXMLHttpRequest() ;
        requests = [] ;
        xhr.onCreate = function (req) {
          requests.push(req) ;
        } ;
      }) ;

      afterEach(function () {
        xhr.restore() ;
      }) ;

      it('defers remaining directive compilation', function () {

        let otherCompileSpy = sinon.spy() ;
        compile.directive({
          myDirective: function () {
            return {templateUrl: '/my_directive.html'} ;
          },
          myOtherDirective: function () {
            return {compile: otherCompileSpy} ;
          }
        }) ;
        let el = Utils.str2DomElement(
          '<div my-directive my-other-directive></div>') ;
        compile.run(el) ;
        expect(otherCompileSpy.called).to.be.false ;
      }) ;

      it('defers current directive compilation', function () {

        let compileSpy = sinon.spy() ;
        compile.directive({
          myDirective: function () {
            return {
              templateUrl: '/my_directive.html',
              compile: compileSpy
            } ;
          }
        }) ;
        let el = Utils.str2DomElement('<div my-directive></div>') ;
        compile.run(el) ;
        expect(compileSpy.called).to.be.false ;
      }) ;

      it('immediately empties out the element', function () {

        compile.directive({
          myDirective: function () {
            return {templateUrl: '/my_directive.html'} ;
          }
        }) ;
        let el = Utils.str2DomElement('<div my-directive>Hello</div>') ;
        compile.run(el) ;
        expect(el[0].innerHTML).to.be.equal('') ;
      }) ;

      it('fetches the template', function () {

        compile.directive({
          myDirective: function () {
            return {templateUrl: '/my_directive.html'} ;
          }
        }) ;
        let el = Utils.str2DomElement('<div my-directive></div>') ;

        compile.run(el) ;

        expect(requests.length).to.be.equal(1) ;
        expect(requests[0].method).to.be.equal('GET') ;
        expect(requests[0].url).to.be.equal('/my_directive.html') ;
      }) ;

      it('populates element with template', function (done) {

        compile.directive({
          myDirective: function () {
            return {templateUrl: '/my_directive.html'} ;
          }
        }) ;
        let el = Utils.str2DomElement('<div my-directive></div>') ;

        el[0]._afterTemplate = () => {
          expect(el[0].querySelectorAll('.from-template').length)
            .to.be.equal(1) ;
          done() ;
        } ;

        compile.run(el) ;
        requests[0].respond(200, {}, '<div class="from-template"></div>') ;
      }) ;

      it('compiles current directive when template received',
      function (done) {

        let compileSpy = sinon.spy() ;
        compile.directive({
          myDirective: function () {
            return {
              templateUrl: '/my_directive.html',
              compile: compileSpy
            } ;
          }
        }) ;
        let el = Utils.str2DomElement('<div my-directive></div>') ;

        el[0]._afterTemplate = () => {
          expect(compileSpy.called).to.be.true ;
          done() ;
        } ;

        compile.run(el) ;
        requests[0].respond(200, {}, '<div class="from-template"></div>') ;
      }) ;

      it('resumes compilation when template received', function (done) {

        let otherCompileSpy = sinon.spy() ;
        compile.directive({
          myDirective: function () {
            return {templateUrl: '/my_directive.html'} ;
          },
          myOtherDirective: function () {
            return {compile: otherCompileSpy} ;
          }
        }) ;
        let el = Utils.str2DomElement(
          '<div my-directive my-other-directive></div>') ;

        el[0]._afterTemplate = () => {
          expect(otherCompileSpy.called).to.be.true ;
          done() ;
        } ;

        compile.run(el) ;
        requests[0].respond(200, {}, '<div class="from-template"></div>') ;
      }) ;

      it('resumes child compilation after template received', function (done) {

        let otherCompileSpy = sinon.spy() ;
        compile.directive({
          myDirective: function () {
            return {templateUrl: '/my_directive.html'} ;
          },
          myOtherDirective: function () {
            return {compile: otherCompileSpy} ;
          }
        }) ;
        let el = Utils.str2DomElement('<div my-directive></div>') ;

        el[0]._afterTemplate = () => {
          expect(otherCompileSpy.called).to.be.true ;
          done() ;
        } ;

        compile.run(el) ;
        requests[0].respond(200, {}, '<div my-other-directive></div>') ;
      }) ;


      /**
       * Template URL Functions
       */

      it('supports functions as values', function () {

        function tmplUrl () {
          return '/my_directive.html' ;
        }
        let templateUrlSpy = sinon.spy(tmplUrl) ;
        compile.directive({
          myDirective: function () {
            return {templateUrl: templateUrlSpy} ;
          }
        }) ;
        let el = Utils.str2DomElement('<div my-directive></div>') ;

        compile.run(el) ;

        expect(requests[0].url).to.be.equal('/my_directive.html') ;
        expect(templateUrlSpy.called).to.be.true ;
        expect(templateUrlSpy.firstCall.args[0]).to.equal(el[0]) ;
        expect(templateUrlSpy.firstCall.args[1].myDirective)
          .to.be.not.undefined ;
      }) ;


      /**
       * Disallowing More Than One Template URL Directive Per Element
       */

      it('does not allow templateUrl directive after template directive',
      function () {

        compile.directive({
          myDirective: function () {
            return {template: '<div></div>'} ;
          },
          myOtherDirective: function () {
            return {templateUrl: '/my_other_directive.html'} ;
          }
        }) ;
        let el = Utils.str2DomElement(
          '<div my-directive my-other-directive></div>') ;

        expect(function () {
          compile.run(el) ;
        }).to.throw() ;
      }) ;


      /**
       * Linking Asynchronous Directives
       */

      it('links the directive when public link function is invoked',
      function (done) {

        let linkSpy = sinon.spy() ;
        compile.directive({
          myDirective: function () {
            return {
              templateUrl: '/my_directive.html',
              link: linkSpy
            } ;
          }
        }) ;
        let el = Utils.str2DomElement('<div my-directive></div>') ;
        el[0]._afterTemplate = () => {
          expect(linkSpy.called).to.be.true ;
          expect(linkSpy.firstCall.args[0]).to.be.equal(rootScope) ;
          expect(linkSpy.firstCall.args[1]).to.be.equal(el[0]) ;
          expect(linkSpy.firstCall.args[2].myDirective).to.be.not.undefined ;
          done() ;
        } ;

        let linkFunction = compile.run(el) ;
        requests[0].respond(200, {}, '<div></div>') ;
        linkFunction(rootScope) ;
      }) ;

      it('links child elements when public link function is invoked',
      function (done) {

        let linkSpy = sinon.spy() ;
        compile.directive({
          myDirective: function () {
            return {templateUrl: '/my_directive.html'} ;
          },
          myOtherDirective: function () {
            return {link: linkSpy} ;
          }
        }) ;
        let el = Utils.str2DomElement('<div my-directive></div>') ;
        el[0]._afterTemplate = () => {
          expect(linkSpy.called).to.be.true ;
          expect(linkSpy.firstCall.args[0]).to.be.equal(rootScope) ;
          expect(linkSpy.firstCall.args[1]).to.be.equal(el[0].firstChild) ;
          expect(linkSpy.firstCall.args[2].myOtherDirective)
            .to.be.not.undefined ;
          done() ;
        } ;

        let linkFunction = compile.run(el) ;
        requests[0].respond(200, {}, '<div my-other-directive></div>') ;
        linkFunction(rootScope) ;
      }) ;

      it('links when template arrives if node link fn was called',
      function (done) {

        let linkSpy = sinon.spy() ;
        compile.directive({
          myDirective: function () {
            return {
              templateUrl: '/my_directive.html',
              link: linkSpy
            } ;
          }
        }) ;
        let el = Utils.str2DomElement('<div my-directive></div>') ;
        el[0]._afterTemplate = () => {
          expect(linkSpy.called).to.be.true ;
          expect(linkSpy.firstCall.args[0]).to.be.equal(rootScope) ;
          expect(linkSpy.firstCall.args[1]).to.be.equal(el[0]) ;
          expect(linkSpy.firstCall.args[2].myDirective).to.be.not.undefined ;
          done() ;
        } ;

        compile.run(el)(rootScope) ; // link first
        requests[0].respond(200, {}, '<div></div>') ; // then receive template
      }) ;


      /**
       * Linking Directives that Were Compiled Earlier
       */

      it('links directives that were compiled earlier', function (done) {

        let linkSpy = sinon.spy() ;
        compile.directive({
          myDirective: function () {
            return {link: linkSpy} ;
          },
          myOtherDirective: function () {
            return {templateUrl: '/my_other_directive.html'} ;
          }
        }) ;
        let el = Utils.str2DomElement(
          '<div my-directive my-other-directive></div>') ;
        el[0]._afterTemplate = () => {
          expect(linkSpy.called).to.be.true ;
          expect(linkSpy.firstCall.args[0]).to.be.equal(rootScope) ;
          expect(linkSpy.firstCall.args[1]).to.be.equal(el[0]) ;
          expect(linkSpy.firstCall.args[2].myDirective).to.be.not.undefined ;
          done() ;
        } ;

        let linkFunction = compile.run(el) ;
        linkFunction(rootScope) ;
        requests[0].respond(200, {}, '<div></div>') ;
      }) ;

      /**
       * Preserving The Isolate Scope Directive
       */

      it('retains isolate scope directives from earlier', function (done) {

        let linkSpy = sinon.spy() ;
        compile.directive({
          myDirective: function () {
            return {
              scope: {val: '=myDirective'},
              link: linkSpy
            } ;
          },
          myOtherDirective: function () {
            return {templateUrl: '/my_other_directive.html'} ;
          }
        }) ;
        let el = Utils.str2DomElement(
          '<div my-directive="2016" my-other-directive></div>') ;
        el[0]._afterTemplate = () => {
          expect(linkSpy.called).to.be.true ;
          expect(linkSpy.firstCall.args[0]).to.be.not.undefined ;
          expect(linkSpy.firstCall.args[0]).to.be.not.equal(rootScope) ;
          expect(linkSpy.firstCall.args[0].val).to.be.equal(2016) ;
          done() ;
        } ;

        let linkFunction = compile.run(el) ;
        linkFunction(rootScope) ;
        requests[0].respond(200, {}, '<div></div>') ;

      }) ;

      /**
       * Preserving Controller Directives
       */

      it('sets up controllers for all controller directives', function (done) {

        let myDirectiveControllerInstantiated,
            myOtherDirectiveControllerInstantiated ;
        compile.directive({
          myDirective: function () {
            return {
              controller: function MyDirectiveController () {
                myDirectiveControllerInstantiated = true ;
              }
            } ;
          },
          myOtherDirective: function () {
            return {
              templateUrl: '/my_other_directive.html',
              controller: function MyOtherDirectiveController () {
                myOtherDirectiveControllerInstantiated = true ;
              }
            } ;
          }
        }) ;
        let el = Utils.str2DomElement(
          '<div my-directive my-other-directive></div>') ;
        el[0]._afterTemplate = () => {
          expect(myDirectiveControllerInstantiated).to.be.true ;
          expect(myOtherDirectiveControllerInstantiated).to.be.true ;
          done() ;
        } ;

        compile.run(el)(rootScope) ;
        requests[0].respond(200, {}, '<div></div>') ;
      }) ;


      /**
       * Transclusion with Template URLs
       */
      describe('with transclusion', function () {

        it('works when template arrives first', function (done) {

          compile.directive({
            myTranscluder: function () {
              return {
                transclude: true,
                templateUrl: 'my_template.html',
                link: function (scope, element, attrs, ctrl, transclude) {
                  element.querySelector('[in-template]')
                    .append(transclude()[0]) ;
                }
              } ;
            }
          }) ;
          let el = Utils.str2DomElement(
            '<div my-transcluder><div in-transclude></div></div>') ;

          let linkFunction = compile.run(el) ;
          requests[0].respond(200, {}, '<div in-template></div>') ;
          linkFunction(rootScope) ;

          el[0]._afterTemplate = () => {
            expect(el[0].querySelector('[in-template] > [in-transclude]'))
              .to.be.not.equal(null) ;
            done() ;
          } ;
        }) ;

        it('works when template arrives after', function (done) {

          compile.directive({
            myTranscluder: function () {
              return {
                transclude: true,
                templateUrl: 'my_template.html',
                link: function (scope, element, attrs, ctrl, transclude) {
                  element.querySelector('[in-template]')
                    .append(transclude()[0]) ;
                }
              } ;
            }
          }) ;

          let el = Utils.str2DomElement(
            '<div my-transcluder><div in-transclude></div></div>') ;

          let linkFunction = compile.run(el) ;
          linkFunction(rootScope) ;
          requests[0].respond(200, {}, '<div in-template></div>') ;

          el[0]._afterTemplate = () => {
            expect(el[0].querySelector('[in-template] > [in-transclude]'))
              .to.be.not.equal(null) ;
            done() ;
          } ;
        }) ;
      }) ;
    }) ;
  }) ;


  describe('Directive Transclusion', function () {

    let rootScope ;

    beforeEach(function () {

      rootScope = new Scope() ;
    }) ;

    it('removes the children of the element from the DOM', function () {

      compile.directive({
        myTranscluder: function () {
          return {transclude: true} ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-transcluder><div>Must go</div></div>') ;

      compile.run(el) ;

      expect(el[0].innerHTML).to.be.equal('') ;
    }) ;

    it('compiles child elements', function () {

      let insideCompileSpy = sinon.spy() ;
      compile.directive({
        myTranscluder: function () {
          return {transclude: true} ;
        },
        insideTranscluder: function () {
          return {compile: insideCompileSpy} ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-transcluder><div inside-transcluder></div></div>') ;

      compile.run(el) ;

      expect(insideCompileSpy.called).to.be.true ;
    }) ;

    it('makes contents available to directive link function', function () {

      compile.directive({
        myTranscluder: function () {
          return {
            transclude: true,
            template: '<div in-template></div>',
            link: function (scope, element, attrs, ctrl, transclude) {
              element.querySelector('[in-template]').append(transclude()[0]) ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-transcluder><div in-transcluder></div></div>') ;

      compile.run(el)(rootScope) ;

      expect(el[0].querySelector('[in-template] > [in-transcluder]'))
        .to.be.not.equal(null) ;
    }) ;

    it('is only allowed once per element', function () {

      compile.directive({
        myTranscluder: function () {
          return {transclude: true} ;
        },
        mySecondTranscluder: function () {
          return {transclude: true} ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-transcluder my-second-transcluder></div>') ;

      expect(function () {
        compile.run(el) ;
      }).to.throw() ;
    }) ;


    /**
     * Transclusion And Scope
     */

    it('makes scope available to link functions inside', function () {

      compile.directive({
        myTranscluder: function () {
          return {
            transclude: true,
            link: function (scope, element, attrs, ctrl, transclude) {
              element.append(transclude()[0]) ;
            }
          } ;
        },
        myInnerDirective: function () {
          return {
            link: function (scope, element) {
              element.innerHTML = scope.anAttr ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-transcluder><div my-inner-directive></div></div>') ;

      rootScope.anAttr = 'Hello from root' ;
      compile.run(el)(rootScope) ;
      expect(el[0].querySelector('[my-inner-directive]').innerHTML)
        .to.be.equal('Hello from root') ;
    }) ;

    it('does not use inherited scope of the directive', function () {

      compile.directive({
        myTranscluder: function () {
          return {
            transclude: true,
            scope: true,
            link: function (scope, element, attrs, ctrl, transclude) {
              scope.anAttr = 'Shadowed attribute' ;
              element.append(transclude()[0]) ;
            }
          } ;
        },
        myInnerDirective: function () {
          return {
            link: function (scope, element) {
              element.innerHTML = scope.anAttr ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-transcluder><div my-inner-directive></div></div>') ;

      rootScope.anAttr = 'Hello from root' ;
      compile.run(el)(rootScope) ;
      expect(el[0].querySelector('[my-inner-directive]').innerHTML)
        .to.be.equal('Hello from root') ;
    }) ;

    it('stops watching when transcluding directive is destroyed', function () {

      let watchSpy = sinon.spy() ;
      compile.directive({
        myTranscluder: function () {
          return {
            transclude: true,
            scope: true,
            link: function (scope, element, attrs, ctrl, transclude) {
              element.append(transclude()[0]) ;
              scope.$on('destroyNow', function () {
                scope.$destroy() ;
              }) ;
            }
          } ;
        },
        myInnerDirective: function () {
          return {
            link: function (scope) {
              scope.$watch(watchSpy) ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-transcluder><div my-inner-directive></div></div>') ;
      compile.run(el)(rootScope) ;

      rootScope.$apply() ;
      expect(watchSpy.getCalls().length).to.be.equal(2) ;

      rootScope.$apply() ;
      expect(watchSpy.getCalls().length).to.be.equal(3) ;

      rootScope.$broadcast('destroyNow') ;
      rootScope.$apply() ;
      expect(watchSpy.getCalls().length).to.be.equal(3) ;
    }) ;

    it('allows passing another scope to transclusion function', function () {

      let otherLinkSpy = sinon.spy() ;
      compile.directive({
        myTranscluder: function () {
          return {
            transclude: true,
            scope: {},
            template: '<div></div>',
            link: function (scope, element, attrs, ctrl, transclude) {
              let mySpecialScope = scope.$new(true) ;
              mySpecialScope.specialAttr = 2016 ;
              transclude(mySpecialScope) ;
            }
          } ;
        },
        myOtherDirective: function () {
          return {link: otherLinkSpy} ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-transcluder><div my-other-directive></div></div>') ;
      compile.run(el)(rootScope) ;
      let transcludedScope = otherLinkSpy.firstCall.args[0] ;
      expect(transcludedScope.specialAttr).to.be.equal(2016) ;
    }) ;


    /**
     * Transclusion from Descendant Nodes
     */

    it('makes contents available to child elements', function () {

      compile.directive({
        myTranscluder: function () {
          return {
            transclude: true,
            template: '<div in-template></div>',
          } ;
        },
        inTemplate: function () {
          return {
            link: function (scope, element, attrs, ctrl, transcludeFn) {
              element.append(transcludeFn()[0]) ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-transcluder><div in-transclude></div></div>') ;
      compile.run(el)(rootScope) ;
      expect(el[0].querySelector('[in-template] > [in-transclude]'))
        .to.be.not.equal(null) ;
    }) ;

    it('makes contents available to indirect child elements', function () {

      compile.directive({
        myTranscluder: function () {
          return {
            transclude: true,
            template: '<div><div in-template></div></div>',
          } ;
        },
        inTemplate: function () {
          return {
            link: function (scope, element, attrs, ctrl, transcludeFn) {
              element.append(transcludeFn()[0]) ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-transcluder><div in-transclude></div></div>') ;
      compile.run(el)(rootScope) ;
      expect(el[0].querySelector('div > [in-template] > [in-transclude]'))
        .to.be.not.equal(null) ;
    }) ;

    it('supports passing transclusion function to public link function',
    function () {

      compile.directive({
        myTranscluder: function (compile) {
          return {
            transclude: true,
            link: function (scope, element, attrs, ctrl, transclude) {
              let customTemplate = Utils.str2DomElement(
                '<div in-custom-template></div>') ;
              element.append(customTemplate[0]) ;
              compile.run(element.childNodes)(scope, undefined, {
                parentBoundTranscludeFn: transclude
              }) ;
            }
          } ;
        },
        inCustomTemplate: function () {
          return {
            link: function (scope, element, attrs, ctrl, transclude) {
              element.append(transclude()[0]) ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-transcluder><div in-transclude></div></div>') ;
      compile.run(el)(rootScope) ;
      expect(el[0].querySelector('[in-custom-template] > [in-transclude]'))
        .to.be.not.equal(null) ;
    }) ;

    it('destroys scope passed through public link fn at the right time',
    function () {

      let watchSpy = sinon.spy() ;
      compile.directive({
        myTranscluder: function (compile) {
          return {
            transclude: true,
            link: function (scope, element, attrs, ctrl, transclude) {
              let customTemplate = Utils.str2DomElement(
                '<div in-custom-template></div>') ;
              element.append(customTemplate[0]) ;
              compile.run(element.childNodes)(scope, undefined, {
                parentBoundTranscludeFn: transclude
              }) ;
            }
          } ;
        },
        inCustomTemplate: function () {
          return {
            scope: true,
            link: function (scope, element, attrs, ctrl, transclude) {
              element.append(transclude()) ;
              scope.$on('destroyNow', function () {
                scope.$destroy() ;
              }) ;
            }
          } ;
        },
        inTransclude: function () {
          return {
            link: function (scope) {
              scope.$watch(watchSpy) ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-transcluder><div in-transclude></div></div>') ;

      compile.run(el)(rootScope) ;

      rootScope.$apply() ;
      expect(watchSpy.getCalls().length).to.be.equal(2) ;

      rootScope.$apply() ;
      expect(watchSpy.getCalls().length).to.be.equal(3) ;

      rootScope.$broadcast('destroyNow') ;
      rootScope.$apply() ;
      expect(watchSpy.getCalls().length).to.be.equal(3) ;
    }) ;

    it('makes contents available to controller', function () {

      compile.directive({
        myTranscluder: function () {
          return {
            transclude: true,
            template: '<div in-template></div>',
            controller: function ($element, $transclude) {
              $element.querySelector('[in-template]')
                .append($transclude()[0]) ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-transcluder><div in-transclude></div></div>') ;
      compile.run(el)(rootScope) ;
      expect(el[0].querySelector('[in-template] > [in-transclude]'))
        .to.be.not.equal(null) ;
    }) ;

    /**
     * The Clone Attach Function
     */

    describe('clone attach function', function () {

      it('can be passed to public link fn', function () {

        compile.directive({}) ;
        let el = Utils.str2DomElement('<div>Hello</div>') ;
        let myScope = rootScope.$new() ;
        let gotEl, gotScope ;

        compile.run(el)(myScope, function cloneAttachFn (el, scope) {
          gotEl    = el ;
          gotScope = scope ;
        }) ;

        expect(gotEl[0].isEqualNode(el[0])).to.be.equal(true) ;
        expect(gotScope).to.be.equal(myScope) ;
      }) ;

      it('causes compiled elements to be cloned', function () {

        compile.directive({}) ;
        let el = Utils.str2DomElement('<div>Hello</div>') ;
        let myScope = rootScope.$new() ;
        let gotClonedEl ;

        compile.run(el)(myScope, function (clonedEl) {
          gotClonedEl = clonedEl ;
        }) ;

        expect(gotClonedEl[0].isEqualNode(el[0])).to.be.true ;
        expect(gotClonedEl[0]).to.be.not.equal(el[0]) ;
      }) ;

      it('causes cloned DOM to be linked', function () {

        let gotCompileEl, gotLinkEl ;
        compile.directive({
          myDirective: function () {
            return {
              compile: function (compileEl) {
                gotCompileEl = compileEl ;
                return function link (scope, linkEl) {
                  gotLinkEl = linkEl ;
                } ;
              }
            } ;
          }
        }) ;
        let el = Utils.str2DomElement('<div my-directive></div>') ;
        let myScope = rootScope.$new() ;

        compile.run(el)(myScope, function () {}) ;

        expect(gotCompileEl).to.be.not.equal(gotLinkEl) ;
      }) ;

      it('allows connecting transcluded content', function () {

        compile.directive({
          myTranscluder: function () {
            return {
              transclude: true,
              template: '<div in-template></div>',
              link: function (scope, element, attrs, ctrl, transcludeFn) {
                let myScope = scope.$new() ;
                transcludeFn(myScope, function (transclNode) {
                  element.querySelector('[in-template]')
                    .append(transclNode[0]) ;
                }) ;
              }
            } ;
          }
        }) ;
        let el = Utils.str2DomElement(
          '<div my-transcluder><div in-transclude></div></div>') ;
        compile.run(el)(rootScope) ;
        expect(el[0].querySelector('[in-template] > [in-transclude]'))
          .to.be.not.equal(null) ;
      }) ;

      it('can be used as the only transclusion function argument',
      function () {

        compile.directive({
          myTranscluder: function () {
            return {
              transclude: true,
              template: '<div in-template></div>',
              link: function (scope, element, attrs, ctrl, transcludeFn) {
                transcludeFn(function (transclNode) {
                  element.querySelector('[in-template]')
                    .append(transclNode[0]) ;
                }) ;
              }
            } ;
          }
        }) ;
        let el =
          Utils.str2DomElement(
            '<div my-transcluder><div in-transclusion></div></div>') ;
        compile.run(el)(rootScope) ;
        expect(el[0].querySelector('[in-template] > [in-transclusion]'))
          .to.be.not.equal(null) ;
      }) ;

      // Ne passe pas avec Firefox. Le problème est l'appendhild qui désassocie
      // le nœud des directives : myOtherDirective n'y a plus accès. D'où
      // l'idée d'associer manuellement transclNode[0], mais ce n'est pas
      // compatible avec Firefox.
      // Il faudra sans doute un jour revoir tout le système de compilation :-/
      it.skip('allows passing data to transclusion', function () {

        compile.directive({
          myTranscluder: function () {
            return {
              transclude: true,
              template: '<div in-template></div>',
              link: function (scope, element, attrs, ctrl, transcludeFn) {
                transcludeFn((transclNode, transclScope) => {
                  transclScope.dataFromTranscluder = 'Hello from transcluder' ;
                  transclNode[0] = element.querySelector('[in-template]')
                    .appendChild(transclNode[0]) ;
                }) ;
              }
            } ;
          },
          myOtherDirective: function () {
            return {
              link: function (scope, element) {
                element.innerHTML = scope.dataFromTranscluder ;
              }
            } ;
          }
        }) ;
        let el = Utils.str2DomElement(
          '<div my-transcluder><div my-other-directive></div></div>') ;
        compile.run(el)(rootScope) ;
        expect(
          el[0].querySelector('[in-template] > [my-other-directive]').innerHTML)
          .to.be.equal('Hello from transcluder') ;
      }) ;
    }) ;


    /**
     * Transclusion with Multi-Element Directives
     */

    it.skip('can be used with multi-element directives', function () {

      compile.directive({
        myTranscluder: function () {
          return {
            transclude: true,
            multiElement: true,
            template: '<div in-template></div>',
            link: function (scope, element, attrs, ctrl, transclude) {
              element.querySelector('[in-template]').append(transclude()) ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div><div my-transcluder-start><div in-transclude></div></div>'
        + '<div my-transcluder-end></div></div>'
      ) ;
      compile.run(el)(rootScope) ;
      expect(
        el[0].querySelector(
          '[my-transcluder-start] [in-template] [in-transclude]'))
        .to.be.equal(1) ;
    }) ;

    /**
     * Full Element Transclusion
     */

    describe('element transclusion', function () {

      it('replaces the element with a comment', function () {

        compile.directive({
          myTranscluder: function () {
            return {
              transclude: 'element'
            } ;
          }
        }) ;
        let el = Utils.str2DomElement('<div><div my-transcluder></div></div>') ;
        compile.run(el) ;
        expect(el[0].innerHTML).to.be.equal('<!-- myTranscluder:  -->') ;
      }) ;

      it('includes directive attribute value in comment', function () {

        compile.directive({
          myTranscluder: function () {
            return {
              transclude: 'element'
            } ;
          }
        }) ;
        let el = Utils.str2DomElement(
          '<div><div my-transcluder=2016></div></div>') ;
        compile.run(el) ;
        expect(el[0].innerHTML).to.be.equal('<!-- myTranscluder: 2016 -->') ;
      }) ;

      it('calls directive compile and link with comment', function () {

        let gotCompiledEl, gotLinkedEl ;
        compile.directive({
          myTranscluder: function () {
            return {
              transclude: 'element',
              compile: function (compiledEl) {
                gotCompiledEl = compiledEl ;
                return function (scope, linkedEl) {
                  gotLinkedEl = linkedEl ;
                } ;
              }
            } ;
          }
        }) ;
        let el = Utils.str2DomElement('<div><div my-transcluder></div></div>') ;
        compile.run(el)(rootScope) ;
        expect(gotCompiledEl.nodeType).to.be.equal(Node.COMMENT_NODE) ;
        expect(gotLinkedEl.nodeType).to.be.equal(Node.COMMENT_NODE) ;
      }) ;

      it.skip('calls lower priority compile with original', function () {

        let gotCompiledEl ;
        compile.directive({
          myTranscluder: function () {
            return {
              priority: 2,
              transclude: 'element'
            } ;
          },
          myOtherDirective: function () {
            return {
              priority: 1,
              compile: function (compiledEl) {
                gotCompiledEl = compiledEl ;
              }
            } ;
          }
        }) ;
        let el =
            Utils.str2DomElement(
              '<div><div my-transcluder my-other-directive></div></div>') ;
        compile.run(el) ;
        expect(gotCompiledEl.nodeType).to.be.equal(Node.ELEMENT_NODE) ;
      }) ;

      it.skip('calls compile on child element directives', function () {

        let compileSpy = sinon.spy() ;
        compile.directive({
          myTranscluder: function () {
            return {
              transclude: 'element'
            } ;
          },
          myOtherDirective: function () {
            return {
              compile: compileSpy
            } ;
          }
        }) ;
        let el = Utils.str2DomElement(
          '<div><div my-transcluder><div my-other-directive>'
          + '</div></div></div>') ;
        compile.run(el) ;
        expect(compileSpy.called).to.be.true ;
      }) ;

      it.skip('compiles original element contents once', function () {

        let compileSpy = sinon.spy() ;
        compile.directive({
          myTranscluder: function () {
            return {
              transclude: 'element'
            } ;
          },
          myOtherDirective: function () {
            return {
              compile: compileSpy
            } ;
          }
        }) ;
        let el = Utils.str2DomElement(
          '<div><div my-transcluder><div my-other-directive>'
          + '</div></div></div>') ;
        compile.run(el) ;
        expect(compileSpy.getCalls().length).to.be.equal(1) ;
      }) ;

      it('makes original element available for transclusion', function () {

        compile.directive({
          myDouble: function () {
            return {
              transclude: 'element',
              link: function (scope, el, attrs, ctrl, transclude) {
                transclude(function (clone) {
                  el.after(clone) ;
                }) ;
                transclude(function (clone) {
                  el.after(clone) ;
                }) ;
              }
            } ;
          }
        }) ;
        let el = Utils.str2DomElement('<div><div my-double>Hello</div>') ;
        compile.run(el)(rootScope) ;
        expect(el[0].querySelectorAll('[my-double]').length).to.be.equal(2) ;
      }) ;

      it('sets directive attributes element to comment', function () {

        compile.directive({
          myTranscluder: function () {
            return {
              transclude: 'element',
              link: function (scope, el, attrs, ctrl, transclude) {
                attrs.$set('testing', '2016') ;
                el.after(transclude()) ;
              }
            } ;
          }
        }) ;
        let el = Utils.str2DomElement('<div><div my-transcluder></div></div>') ;
        compile.run(el)(rootScope) ;
        expect(el[0].querySelector('[my-transcluder]').getAttribute('testing'))
          .to.be.equal(null) ;
      }) ;

      it.skip('supports requiring controllers', function () {

        let MyController = function () {} ;
        let gotCtrl ;
        compile.directive({
          myCtrlDirective: function () {
            return {
              controller: MyController
            } ;
          },
          myTranscluder: function () {
            return {
              transclude: 'element',
              link: function (scope, el, attrs, ctrl, transclude) {
                el.after(transclude()) ;
              }
            } ;
          },
          myOtherDirective: function () {
            return {
              require: '^myCtrlDirective',
              link: function (scope, el, attrs, ctrl) {
                gotCtrl = ctrl ;
              }
            } ;
          }
        }) ;
        let el = Utils.str2DomElement(
          '<div><div my-ctrl-directive my-transcluder>'
          + '<div my-other-directive></div></div></div>') ;
        compile.run(el)(rootScope) ;

        expect(gotCtrl).to.be.not.undefined ;
        expect(gotCtrl).to.be.instanceOf(MyController) ;
      }) ;
    }) ;
  }) ;


  describe('Interpolation', function () {

    let rootScope ;

    beforeEach(function () {

      rootScope = new Scope() ;
    }) ;

    /**
     * Text Node Interpolation
     */

    it('is done for text nodes', function () {

      compile.directive({}) ;
      let el = Utils.str2DomElement('<div>My expression: {{myExpr}}</div>') ;
      compile.run(el)(rootScope) ;

      rootScope.$apply() ;
      expect(el[0].innerHTML).to.be.equal('My expression: ') ;

      rootScope.myExpr = 'Hello' ;
      rootScope.$apply() ;
      expect(el[0].innerHTML).to.be.equal('My expression: Hello') ;
    }) ;

    it('adds binding class to text node parents', function () {

      compile.directive({}) ;
      let el = Utils.str2DomElement('<div>My expression: {{myExpr}}</div>') ;
      compile.run(el)(rootScope) ;
      expect(el[0].classList.contains('kjs-binding')).to.be.true ;
    }) ;

    it('adds binding data to text node parents', function () {

      compile.directive({}) ;
      let el = Utils.str2DomElement(
        '<div>{{myExpr}} and {{myOtherExpr}}</div>') ;
      compile.run(el)(rootScope) ;
      expect(el[0].$binding).to.be.deep.equal(['myExpr', 'myOtherExpr']) ;
    }) ;

    it('adds binding data to parent from multiple text nodes', function () {

      compile.directive({}) ;
      let el = Utils.str2DomElement(
        '<div>{{myExpr}} <span>and</span> {{myOtherExpr}}</div>') ;
      compile.run(el)(rootScope) ;
      expect(el[0].$binding).to.be.deep.equal(['myExpr', 'myOtherExpr']) ;
    }) ;

    /**
     * Attribute Interpolation
     */

    it('is done for attributes', function () {

      compile.directive({}) ;
      let el = Utils.str2DomElement('<img alt="{{myAltText}}" />') ;
      compile.run(el)(rootScope) ;

      rootScope.$apply() ;
      expect(el[0].getAttribute('alt')).to.be.equal('') ;

      rootScope.myAltText = 'My favourite photo' ;
      rootScope.$apply() ;
      expect(el[0].getAttribute('alt')).to.be.equal('My favourite photo') ;
    }) ;

    it('fires observers on attribute expression changes', function () {

      let observerSpy = sinon.spy() ;
      compile.directive({
        myDirective: function () {
          return {
            link: function (scope, element, attrs) {
              attrs.$observe('alt', observerSpy) ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<img alt="{{myAltText}}" my-directive />') ;
      compile.run(el)(rootScope) ;

      rootScope.myAltText = 'My favourite photo' ;
      rootScope.$apply() ;
      expect(observerSpy.lastCall.args[0])
        .to.be.equal('My favourite photo') ;
    }) ;

    it('fires observers just once upon registration', function () {

      let observerSpy = sinon.spy() ;
      compile.directive({
        myDirective: function () {
          return {
            link: function (scope, element, attrs) {
              attrs.$observe('alt', observerSpy) ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<img alt="{{myAltText}}" my-directive />') ;
      compile.run(el)(rootScope) ;
      rootScope.$apply() ;

      expect(observerSpy.getCalls().length).to.be.equal(1) ;
    }) ;

    it('is done for attributes by the time other directive is linked',
    function () {

      let gotMyAttr ;
      compile.directive({
        myDirective: function () {
          return {
            link: function (scope, element, attrs) {
              gotMyAttr = attrs.myAttr ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive my-attr="{{myExpr}}"></div>') ;
      rootScope.myExpr = 'Hello' ;
      compile.run(el)(rootScope) ;

      expect(gotMyAttr).to.be.equal('Hello') ;
    }) ;

    it('is done for attributes by the time bound to iso scope', function () {

      let gotMyAttr ;
      compile.directive({
        myDirective: function () {
          return {
            scope: {myAttr: '@'},
            link: function (scope) {
              gotMyAttr = scope.myAttr ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive my-attr="{{myExpr}}"></div>') ;
      rootScope.myExpr = 'Hello' ;
      compile.run(el)(rootScope) ;

      expect(gotMyAttr).to.be.equal('Hello') ;
    }) ;

    it('is done for attributes so that compile-time changes apply',
    function () {

      compile.directive({
        myDirective: function () {
          return {
            compile: function (element, attrs) {
              attrs.$set('myAttr', '{{myDifferentExpr}}') ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive my-attr="{{myExpr}}"></div>') ;
      rootScope.myExpr = 'Hello' ;
      rootScope.myDifferentExpr = 'Other Hello' ;
      compile.run(el)(rootScope) ;
      rootScope.$apply() ;

      expect(el[0].getAttribute('my-attr')).to.be.equal('Other Hello') ;
    }) ;

    it('is done for attributes so that compile-time removals apply',
    function () {

      compile.directive({
        myDirective: function () {
          return {
            compile: function (element, attrs) {
              attrs.$set('myAttr', null) ;
            }
          } ;
        }
      }) ;
      let el = Utils.str2DomElement(
        '<div my-directive my-attr="{{myExpr}}"></div>') ;
      rootScope.myExpr = 'Hello' ;
      compile.run(el)(rootScope) ;
      rootScope.$apply() ;

      expect(el[0].getAttribute('my-attr')).to.be.equal('null') ;
    }) ;

    it('cannot be done for event handler attributes', function () {

      compile.directive({}) ;
      let el = Utils.str2DomElement(
        '<button onClick="{{myFunction()}}"></button>') ;
      expect(function () {
        compile.run(el)(rootScope) ;
      }).to.throw() ;
    }) ;
  }) ;


  describe('Components', function () {

  }) ;

}) ;
// buddy ignore:end

