'use strict' ;

/**
 * src/browser/Services/Compiles.js
 */

const KernelAccess = require('../../core/KernelAccess'),
      Utils        = require('./../Utils') ;

/**
 * Compile des nœuds DOM pour appliquer les directives.
 */
class Compile extends KernelAccess {

  constructor () {

    super() ;

    this._directives = {} ;
    this._directivesFactories = {} ;
  }


  /**
   * Enregistre la directive `name` que retourne `directiveFactory`.
   * Plusieurs directives peuvent avoir le même nom, mais elles ne peuvent pas
   * s'appeler hasOwnProperty.
   * Si un objet est transmis à la place de `name`, la méthode créera une série
   * de directives ayant pour nom les propriétés de l'objet et pour factory
   * les valeurs correspondantes.
   * La directiveFactory est une fonction, prenant comme paramètre le service
   * compile (que définit notre classe), retournant un  objet avec les
   * propriétés suivantes :
   *   - `compile`: `function (element)` fonction appelée sur l'élément lors de
   *                la compilation. Celle-ci retourne une fonction qui sera
   *                appellée pour le linkage `link(scope, element, attrs, ctrl,
   *                transclude)`
   *   - `link`: Fonction de linkage appelée si `compile` n'est pas défini.
   *   - `restrict`: restriction de déclaration (peuvent être combinés) La
   *              valeur par défaut est `EA`
   *        `E` element name
   *        `A` attribute name
   *   - `priority`: vaut 0 par défaut. Indique la priorité, plus le nombre est
   *              élevé, plus celle-ci est forte.
   *   - `name`: nom de la directive. Si cet attribu n'est pas donné, name
   *             vaudra le nom `name` donné à la fonction.
   *   - `index`: attribué automatiquement, correspond à l'index
   *              d'enregistrement
   *   - `terminal`: si vaut true, les éléments enfants ne seront pas compilé
   *            après la rencontre de la directive. Si un élément possède
   *            d'autres directives, elles ne seront compilées que si leur
   *            priorité est supérieure ou égale.
   *   - `multiElement`: directive de type multi-elements. Le restrict vaut `A`
   *            d'office.
   *        `<div my-dir-start></div><span /><div my-dir-end></div>`
   *   - `scope`: si cet attribut vaut true, alors un nouveau scope (héritant
   *        du $rootScope) sera lié à l'élément et toutes les directives
   *        attachées à cet élément recevront ce nouveau scope. L'élément
   *        se verra ajouté une nouvelle classe (`kjs-scope`) et le scope sera
   *        lié à l'élément (`el.$scope`).
   *        Si cet attribut est un objet, un scope isolé sera créé (cf Scope),
   *        celui-ci ne sera pas partagé avec les autres directives, ni avec
   *        les éléments enfants sauf si ces derniers ont été créés par la
   *        directive durant son exécution. Si une propriété de l'objet à
   *        pour valeur '@', alors celle-ci sera bindée avec l'attribut de
   *        même nom.
   *  - `require`: ??
   * @param {String} name Nom de la directive
   * @param {Function} directiveFactory Fonction permettant de créer la
   *                                    directive
   */
  directive (name, directiveFactory) {

    // Un nom est fournit
    if (Utils.isString(name)) {

      // Nom interdit
      if (name === 'hasOwnProperty')
        throw 'hasOwnProperty is not a valid directive name' ;

      // S'il n'y a pas encore de directive name
      if (!this.has(name)) {

        this._directives[name] = [] ;

        this._directivesFactories[name] = () => {

          return this._directives[name].map((factory, i) => {

            let directive = factory(this) ;

            directive.restrict = directive.restrict || 'EA' ;
            directive.priority = directive.priority || 0 ;

            if (directive.link && !directive.compile) {
              directive.compile = function () {
                return directive.link ;
              } ;
            }

            directive._bindings = this._parseDirectiveBindings(directive) ;
            directive.name = directive.name || name ;
            directive.index = i ;
            directive.require =
              directive.require || (directive.controller && name) ;

            return directive ;
          }) ;
        } ;
      }
      // On enregistre la directive
      this._directives[name].push(directiveFactory) ;
    }
    // Sinon, c'est un objet
    else {
      Utils.forEach(name, (directiveFactory, name) => {
        this.directive(name, directiveFactory) ;
      }) ;
    }
  }


  /**
   * Indique si une directive est enregistrée
   * @param {String} name Nom de la directive
   * @returns {Boolean}
   */
  has (name) {

    return this._directives.hasOwnProperty(name) ;
  }


  /**
   * Retourne des directives
   * @param {String} name Nom de la directive
   * @returns {Array}
   */
  get (name) {

    if (this.has(name))
      return this._directivesFactories[name]() ;
    else
      throw 'Directive `' + name + '` doesn\'t exist!' ;
  }


  /**
   * Lance la compilation pour `compileNodes`.
   * Elle retourne une fonction qui permettra d'établir le lien entre les
   * directives et le scope.
   *    `publicLinkFn (scope)`
   *      Lance la function link retournée par chaque méthode compile() des
   *      directives. On link en premier les éléments enfants.
   *    Par défaut il s'agit d'une post-linking function, mais on peut changer
   *    cela en retournant, au lieu d'une function, un objet avec comme
   *    propriété(s) : `post` ou `pre` indiquant à quel moment à lieu le
   *    linkage et comme valeur la fonction.
   *    Post-linkage : la fonction est exécuté après celles des éléments
   *      enfants.
   *    Pre-linkage : la fonction est exécuté avant celles des éléments
   *      enfants.
   * @param {DOM} compileNodes Nœuds DOM
   * @param {Integer} maxPriority ?
   * @returns {Function}
   */
  run (compileNodes, maxPriority) {

    let compositeLinkFn = this._compileNodes(compileNodes, maxPriority) ;

    return function publicLinkFn (scope, cloneAttachFn, options) {

      options = options || {} ;
      let parentBoundTranscludeFn = options.parentBoundTranscludeFn ;
      let transcludeControllers   = options.transcludeControllers ;

      if (parentBoundTranscludeFn && parentBoundTranscludeFn._boundTransclude)
        parentBoundTranscludeFn = parentBoundTranscludeFn._boundTransclude ;

      let linkNodes ;
      if (cloneAttachFn) {
        if (compileNodes instanceof NodeList) {
          // On clone compileNodes
          linkNodes = document.createElement('div') ;
          compileNodes.forEach(node => {
            linkNodes.append(node.cloneNode(true)) ;
          }) ;
          linkNodes = linkNodes.childNodes ;
        }
        else
          linkNodes = compileNodes.cloneNode(true) ;
        cloneAttachFn(linkNodes, scope) ;
      } else
        linkNodes = compileNodes ;

      Utils.forEach(transcludeControllers, (controller, name) => {
        linkNodes['$' + name + 'Controller'] = controller.instance ;
      }) ;

      linkNodes.$scope = scope ;

      compositeLinkFn(scope, linkNodes, parentBoundTranscludeFn) ;

      return linkNodes ;
    } ;
  }


  /**
   * Compile les nœuds DOM compileNodes
   * @param {DOM} compileNodes Nœuds à compiler
   * @param {Integer} maxPriority ?
   * @returns {Function}
   * @private
   */
  _compileNodes (compileNodes, maxPriority) {

    let linkFns = [] ;

    // On parcours tous les nœuds
    Utils.times(compileNodes.length, i => {

      let attrs = new Attributes(
        compileNodes[i],
        this.kernel().get('$rootScope')
      ) ;
      let directives =
        this._collectDirectives(compileNodes[i], attrs, maxPriority) ;
      let nodeLinkFn ;

      if(directives.length) {
        nodeLinkFn =
          this._applyDirectivesToNode(directives, compileNodes[i], attrs) ;
      }

      // On compile aussi les enfants si besoin
      let childLinkFn ;
      if ((!nodeLinkFn || !nodeLinkFn.terminal) &&
          compileNodes[i].childNodes && compileNodes[i].childNodes.length)
        childLinkFn = this._compileNodes(compileNodes[i].childNodes) ;

      // S'il y a un nouveau scope, on ajoute la classe
      if (nodeLinkFn && nodeLinkFn.scope)
        attrs._element.classList.add('kjs-scope') ;

      if (nodeLinkFn || childLinkFn) {
        linkFns.push({
          nodeLinkFn:  nodeLinkFn,
          childLinkFn: childLinkFn,
          idx: i
        }) ;
      }
    }) ;

    // Invoque toutes les node link functions collectées
    function compositeLinkFn (scope, linkNodes, parentBoundTranscludeFn) {

      // On réalise une copie des Nodes, ainsi si le linkage ajoute des
      // nouveau nœuds, on aura pas d'erreurs
      let stableNodeList = [] ;
      linkFns.forEach(function (linkFn) {
        let nodeIdx = linkFn.idx ;
        stableNodeList[nodeIdx] = linkNodes[nodeIdx] ;
      }) ;

      linkFns.forEach(function (linkFn) {

        let node = stableNodeList[linkFn.idx] ;

        if (linkFn.nodeLinkFn) {

          let childScope ;

          if (linkFn.nodeLinkFn.scope) {
            childScope = scope.$new() ;
            node.$scope = childScope ;
          } else
            childScope = scope ;

          let boundTranscludeFn ;
          if (linkFn.nodeLinkFn.transcludeOnThisElement) {
            boundTranscludeFn = function (
              transcludedScope, cloneAttachFn, transcludeControllers,
              containingScope) {
              if (!transcludedScope)
                transcludedScope = scope.$new(false, containingScope) ;
              return linkFn.nodeLinkFn
                .transclude(transcludedScope, cloneAttachFn, {
                  transcludeControllers: transcludeControllers
                }) ;
            } ;
          }
          else if (parentBoundTranscludeFn)
            boundTranscludeFn = parentBoundTranscludeFn ;

          linkFn.nodeLinkFn(
            linkFn.childLinkFn,
            childScope,
            node,
            boundTranscludeFn
          ) ;
        }
        else {
          linkFn.childLinkFn(
            scope,
            node.childNodes,
            parentBoundTranscludeFn
          ) ;
        }
      }) ;
    }

    return compositeLinkFn ;
  }


  /**
   * Applique les directives à node et récupère les attributs.
   * @param {Node} node Nœud DOM
   * @param {Object} attrs Attributs
   * @param {*} maxPriority ?
   * @returns {Array}
   */
  _collectDirectives (node, attrs, maxPriority) {

    let directives = [] ;

    // Node est bien un ELEMENT_NODE
    if (node.nodeType === Node.ELEMENT_NODE) {

      // Directives <my-directive />
      let normalizedNodeName =
        directiveNormalize(nodeName(node).toLowerCase()) ;
      this._addDirective(directives, normalizedNodeName, 'E', maxPriority) ;

      // Directives <div my-directive />
      Utils.forEach(node.attributes, attr => {

        let attrStartName, attrEndName ;
        let name = attr.name ;
        let normalizedAttrName = directiveNormalize(name.toLowerCase()) ;

        // Pour le préfixe kjs-attr
        let isKjsAttr = /^kjsAttr[A-Z]/.test(normalizedAttrName) ;
        if (isKjsAttr) {
          name = Utils.kebabCase(
            normalizedAttrName[7].toLowerCase() // buddy ignore:line
            + normalizedAttrName.substring(8)   // buddy ignore:line
          ) ;
          normalizedAttrName = directiveNormalize(name.toLowerCase()) ;
        }

        // On sauve le nom de l'attribut
        attrs.$attr[normalizedAttrName] = name ;

        let directiveNName = normalizedAttrName.replace(/(Start|End)$/, '') ;
        if (this._directiveIsMultiElement(directiveNName)) {
          if (/Start$/.test(normalizedAttrName)) {
            attrStartName = name ;
            attrEndName   = name.substring(0, name.length - 5) + 'end' ;
            name = name.substring(0, name.length - 6) ;
          }
        }
        normalizedAttrName = directiveNormalize(name.toLowerCase()) ;

        // On ajoute si besoin une directive pour l'attribute interpolation
        this._addAttrInterpolateDirective(
            directives, attr.value, normalizedAttrName) ;

        // On ajoute la directive
        this._addDirective(directives, normalizedAttrName, 'A', maxPriority,
                     attrStartName, attrEndName) ;

        // On récupère les attributs. Un attribut portant le prefixe
        // kjs-attr- écrase un attributs ayant le même nom (sans préfixe)
        if (isKjsAttr || !attrs.hasOwnProperty(normalizedAttrName)) {
          attrs[normalizedAttrName] = attr.value.trim() ;
          if (isBooleanAttribute(node, normalizedAttrName))
            attrs[normalizedAttrName] = true ;
        }
      }) ;
    }
    // Text Node interpolation
    else if (node.nodeType === Node.TEXT_NODE)
      this._addTextInterpolateDirective(directives, node.nodeValue) ;

    // On trie les directives
    directives.sort(byPriority) ;
    return directives ;
  }

  /**
   * Reçoit une liste de directives (array directives) et le nom d'une
   * directive pour vérifier si le tableau local this._directives possède des
   * directives avec le nom name. S'il en trouve, les directives
   * correspondantes seront obtenues à et ajoutée au tableau directives.
   * Mais la méthode regarde aussi la propriété restrict : la directive ne
   * sera ajouté que si celle-ci correspind au mode (E, A)
   * Retourne une valeur indiquant si une directive a été ajoutée ou non
   *
   * @param {Object} directives Listes des directives
   * @param {String} name Nom de la directive
   * @param {*} mode ?
   * @param {Integer} maxPriority ?
   * @param {String} attrStartName Nom de l'attribut du premier élément
   * @param {String} attrEndName Nom de l'attribut du dernier élément
   * @returns {*}
   */
  _addDirective (directives, name, mode, maxPriority,
    attrStartName, attrEndName) {

    let match ;

    if (this._directives.hasOwnProperty(name)) {

      let foundDirectives = this.get(name) ;
      let applicableDirectives =
        foundDirectives.filter(dir => {
          return (maxPriority === undefined || maxPriority > dir.priority) &&
                 dir.restrict.indexOf(mode) !== -1 ;
        }) ;
      applicableDirectives.forEach(function (directive) {
        if (attrStartName) {
          let dir = function () {} ;
          dir.prototype = directive ;
          directive = new dir ;
          directive._start = attrStartName ;
          directive._end = attrEndName ;
        }
        directives.push(directive) ;
        match = directive ;
      }) ;
    }

    return match ;
  }


  /**
   * Parse une directive pour déterminer son binding
   * @param {Object} directive Directive à parser
   * @returns {Object}
   * @private
   */
  _parseDirectiveBindings (directive) {

    let bindings = {} ;

    if (Utils.isObject(directive.scope)) {

      if (directive.bindToController) {
        bindings.bindToController =
          this._parseIsolateBindings(directive.scope) ;
      }
      else
        bindings.isolateScope = this._parseIsolateBindings(directive.scope) ;
    }

    if (Utils.isObject(directive.bindToController)) {
      bindings.bindToController =
        this._parseIsolateBindings(directive.bindToController) ;
    }

    return bindings ;
  }


  /**
   * Parse un objet scope transmis à une directive pour configurer le binding
   * entre un scope isolé et les attributs.
   * modes :
   *  - @[attrName] : la propriété est lié à l'attribut [attrName] de
   *      l'élément. Si ce nom n'est pas fournit, c'est le nom de la
   *      propriété qui sera utilisé pour déterminer l'attribut à observer.
   *  - =[attrName] : de même, mais le binding est bidirectionnel et la valeur
   *      peut être une expression à évaluer.
   * @param {Scope} scope Scope transmis à la directive
   * @returns {Object}
   * @private
   */
  _parseIsolateBindings (scope) {

    let bindings = {} ;

    Utils.forEach(scope, (definition, scopeName) => {
      let match = definition.match(/\s*([@&]|=(\*?))(\??)\s*(\w*)\s*/) ;
      bindings[scopeName] = {
        mode: match[1][0],
        collection: match[2] === '*',
        optional: match[3],             // buddy ignore:line
        attrName: match[4] || scopeName // buddy ignore:line
      } ;
    }) ;

    return bindings ;
  }


  /**
   * Applique les directives au compileNode et retourne un boolean indiquant
   * si la directive est une directive terminale ou non. attrs contient les
   * attributs du compileNode.
   * @param {Array} directives Directives
   * @param {Node} compileNode Node que l'on compile
   * @param {*} attrs Attributs
   * @param {*} previousCompileContext ?
   * @returns {Boolean}
   * @private
   */
  _applyDirectivesToNode (directives, compileNode, attrs,
    previousCompileContext) {

    previousCompileContext = previousCompileContext || {} ;
    let terminalPriority = -Number.MAX_VALUE ;
    let terminal = false ;
    let preLinkFns  = previousCompileContext.preLinkFns  || [] ;
    let postLinkFns = previousCompileContext.postLinkFns || [] ;
    let controllers = {} ;
    let newScopeDirective ;
    let newIsolateScopeDirective =
      previousCompileContext.newIsolateScopeDirective ;
    let templateDirective    = previousCompileContext.templateDirective ;
    let controllerDirectives = previousCompileContext.controllerDirectives ;
    let childTranscludeFn ;
    let hasTranscludeDirective =
      previousCompileContext.hasTranscludeDirective ;
    let hasElementTranscludeDirective ;

    const $parse       = this.kernel().get('$parser'),
          $interpolate = this.kernel().get('$interpolate') ;


    function getControllers (require, element) {

      if (Array.isArray(require))
        return require.map(getControllers) ;
      else {
        let value ;
        let match = require.match(/^(\^\^?)?(\?)?(\^\^?)?/) ;
        let optional = match[2] ;
        require = require.substring(match[0].length) ;
        if (match[1] || match[3]) {   // buddy ignore:line
          if (match[3] && !match[1])  // buddy ignore:line
            match[1] = match[3] ;     // buddy ignore:line
          if (match[1] === '^^')
            element = element.parentNode ;
          while (element) {
            value = element['$' + require + 'Controller'] ;
            if (value)
              break ;
            else
              element = element.parentNode ;
          }
        } else {
          if (controllers[require])
            value = controllers[require].instance ;
        }

        if (!value && !optional) {
          throw 'Controller ' + require
          + ' required by directive, cannot be found!' ;
        }

        return value || null ;
      }
    }

    let self = this ;

    function groupElementsLinkFnWrapper (linkFn, attrStart, attrEnd) {

      return function (scope, element, attrs, ctrl, transclude) {
        let group = self._groupScan(element, attrStart, attrEnd) ;
        return linkFn(scope, group, attrs, ctrl, transclude) ;
      } ;
    }


    function addLinkFns (preLinkFn, postLinkFn, attrStart, attrEnd,
      isolateScope, require) {

      if (preLinkFn) {
        if (attrStart) {
          preLinkFn =
            groupElementsLinkFnWrapper(preLinkFn, attrStart, attrEnd) ;
        }
        preLinkFn.isolateScope = isolateScope ;
        preLinkFn.require = require ;
        preLinkFns.push(preLinkFn) ;
      }

      if (postLinkFn) {
        if (attrStart) {
          postLinkFn =
            groupElementsLinkFnWrapper(postLinkFn, attrStart, attrEnd) ;
        }
        postLinkFn.isolateScope = isolateScope ;
        postLinkFn.require = require ;
        postLinkFns.push(postLinkFn) ;
      }
    }

    Utils.forEach(directives, (directive, i) => {

      if (directive._start) {
        compileNode = this._groupScan(
          compileNode, directive._start, directive._end
        ) ;
      }

      // Si la priorité de la directive est inférieure à terminalPriority
      // c'est qu'une directive ayant son attribut à true à déjà été
      // rencontrée et terminalPriority contient la valeur de sa priorité
      // On interrompt l'application des directives
      if (directive.priority < terminalPriority)
        return false ;

      // On demande la création d'un nouveau Scope
      if (directive.scope) {
        if (Utils.isObject(directive.scope)) {
          if (newIsolateScopeDirective || newScopeDirective)
            throw 'Multiple directives asking for new/inherited scope' ;
          newIsolateScopeDirective = directive ;
        } else {
          if (newIsolateScopeDirective)
            throw 'Multiple directives asking for new/inherited scope' ;
          newScopeDirective = newScopeDirective || directive ;
        }
      }

      // la directive possède un controller
      if (directive.controller) {
        controllerDirectives = controllerDirectives || {} ;
        controllerDirectives[directive.name] = directive ;
      }

      // Transclude
      if (directive.transclude) {

        if (hasTranscludeDirective)
          throw 'Multiple directives asking for transclude' ;

        hasTranscludeDirective = true ;

        if (directive.transclude === 'element') {

          hasElementTranscludeDirective = true ;
          let originalCompileNode = compileNode ;
          compileNode = attrs._element = document.createComment(
            ' ' + directive.name + ': ' + attrs[directive.name] + ' '
          ) ;
          originalCompileNode.replaceWith(compileNode) ;
          terminalPriority = directive.priority ;
          childTranscludeFn =
            this.run(originalCompileNode, terminalPriority) ;
        } else {
          let transcludedNodes ;
          if (Array.isArray(compileNode)) {
            transcludedNodes = [] ;
            compileNode.forEach(node => {
              transcludedNodes.push(node.cloneNode(true)) ;
            }) ;
          }
          else
            transcludedNodes = compileNode.cloneNode(true).childNodes ;
          childTranscludeFn = this.run(transcludedNodes) ;
          compileNode.innerHTML = '' ;
        }
      }

      // La directive possède un template
      if (directive.template) {

        if (templateDirective)
          throw 'Multiple directives asking for template' ;

        templateDirective = directive ;
        let template = Utils.isFunction(directive.template) ?
          directive.template(compileNode, attrs)
          : directive.template ;
        compileNode.innerHTML = template ;
      }

      if (directive.templateUrl) {

        if (templateDirective)
          throw 'Multiple directives asking for template' ;

        templateDirective = directive ;
        nodeLinkFn = this._compileTemplateUrl( // eslint-disable-line no-func-assign
          directives.slice(i),
          compileNode,
          attrs,
          {
            templateDirective:        templateDirective,
            newIsolateScopeDirective: newIsolateScopeDirective,
            controllerDirectives:     controllerDirectives,
            hasTranscludeDirective:   hasTranscludeDirective,
            preLinkFns:               preLinkFns,
            postLinkFns:              postLinkFns
          }
        ) ;
        return false ;
      }
      // On compile le DOM avec la directive
      else if (directive.compile) {

        let linkFn = directive.compile(compileNode, attrs) ;
        let isolateScope = (directive === newIsolateScopeDirective) ;
        let attrStart = directive._start ;
        let attrEnd   = directive._end ;
        let require   = directive.require ;

        if (Utils.isFunction(linkFn))
          addLinkFns(null, linkFn, attrStart, attrEnd, isolateScope, require) ;
        else if (linkFn) {
          addLinkFns(linkFn.pre, linkFn.post,
                     attrStart, attrEnd, isolateScope, require) ;
        }
      }

      // La directive est une terminale
      if (directive.terminal) {
        terminal = true ;
        terminalPriority = directive.priority ;
      }
    }) ;


    /**
     * Initialise le binding des directives
     * @param {Scope} scope Le scope
     * @param {Object} attrs Attributs
     * @param {*} destination ?
     * @param {*} bindings ?
     * @param {*} newScope ?
     */
    function initializeDirectiveBindings (scope, attrs, destination,
      bindings, newScope) {

      // On bind avec les attributs
      Utils.forEach(bindings, function (definition, scopeName) {

        let attrName = definition.attrName ;

        switch (definition.mode) {

          case '@': {
            attrs.$observe(attrName, function (newAttrValue) {
              destination[scopeName] = newAttrValue ;
            }) ;
            // Si l'attribut est déjà enregistré, on enregistre sa valeur,
            // mais on le passe avant par $interpolate
            if (attrs[attrName])
              destination[scopeName] = $interpolate(attrs[attrName])(scope) ;
            break ;
          }

          case '=': {
            if (definition.optional && !attrs[attrName])
              break ;
            let parentGet = $parse(attrs[attrName]) ;
            let lastValue = destination[scopeName] = parentGet(scope) ;

            let parentValueWatch = function () {
              let parentValue = parentGet(scope) ;
              if(destination[scopeName] !== parentValue) {
                if (parentValue !== lastValue)
                  destination[scopeName] = parentValue ;
                else {
                  parentValue = destination[scopeName] ;
                  parentGet.assign(scope, parentValue) ;
                }
              }
              lastValue = parentValue ;
              return parentValue ;
            } ;

            let unwatch ;
            if (definition.collection) {
              unwatch = scope.$watchCollection(
                attrs[attrName], parentValueWatch) ;
            }
            else
              unwatch = scope.$watch(parentValueWatch) ;
            newScope.$on('$destroy', unwatch) ;
            break ;
          }

          case '&': {
            let parentExpr = $parse(attrs[attrName]) ;
            if (parentExpr === Utils.noop && definition.optional)
              break ;
            destination[scopeName] = function (locals) {
              return parentExpr(scope, locals) ;
            } ;
            break ;
          }
        }
      }) ;
    }

    // On construit la node link function qui fait le lien avec une
    // seule directive. Exécute d'abord les preLinks, puis les links des
    // enfants et enfin les postLinks.
    function nodeLinkFn (childLinkFn, scope, linkNode, boundTranscludeFn) {

      // On prépare un éventuel scope isolé
      let isolateScope ;
      if (newIsolateScopeDirective) {
        isolateScope = scope.$new(true) ;
        linkNode.classList.add('kjs-isolate-scope') ;
        linkNode.$isolateScope = isolateScope ;
      }

      // On instancie les controllers
      if (controllerDirectives) {
        Utils.forEach(controllerDirectives, directive => {
          let locals = {
            $scope:   directive === newIsolateScopeDirective ?
              isolateScope : scope,
            $element: linkNode,
            $transclude: scopeBoundTranscludeFn,
            $attrs:   attrs
          } ;
          let controllerName = directive.controller ;
          if (controllerName === '@')
            controllerName = attrs[directive.name] ;

          let controller = self.kernel().get('$controller').get(
            controllerName, locals, true, directive.controllerAs
          ) ;
          controllers[directive.name] = controller ;
          linkNode['$' + directive.name + 'Controller'] = controller.instance ;
        }) ;
      }

      // On créé un scope isolé si besoin
      if (newIsolateScopeDirective) {
        initializeDirectiveBindings(
          scope,
          attrs,
          isolateScope,
          newIsolateScopeDirective._bindings.isolateScope,
          isolateScope
        ) ;
      }

      let scopeDirective = newIsolateScopeDirective || newScopeDirective ;
      if (scopeDirective && controllers[scopeDirective.name]) {
        initializeDirectiveBindings(
          scope,
          attrs,
          controllers[scopeDirective.name].instance,
          scopeDirective._bindings.bindToController,
          isolateScope
        ) ;
      }

      // Les controllers
      Utils.forEach(controllers, controller => {
        controller() ;
      }) ;

      // Si l'on souhaite pas transmettre de scope, on peut appeler la
      // fonction : scopeBoundTransclude(cloneAttachFn) directment.
      function scopeBoundTranscludeFn (transcludedScope, cloneAttachFn) {

        let transcludeControllers ;

        if (!transcludedScope || !transcludedScope.$watch ||
            !transcludedScope.$evalAsync) {
          cloneAttachFn    = transcludedScope ;
          transcludedScope = undefined ;
        }

        if (hasElementTranscludeDirective)
          transcludeControllers = controllers ;

        return boundTranscludeFn(
          transcludedScope, cloneAttachFn, transcludeControllers, scope
        ) ;
      }
      scopeBoundTranscludeFn._boundTransclude = boundTranscludeFn ;

      // Pre-Linking
      preLinkFns.forEach(function (linkFn) {
        linkFn(
          linkFn.isolateScope ? isolateScope : scope,
          linkNode,
          attrs,
          linkFn.require && getControllers(linkFn.require, linkNode),
          scopeBoundTranscludeFn
        ) ;
      }) ;

      // Les enfants
      if (childLinkFn) {

        let scopeToChild = scope ;

        if (newIsolateScopeDirective && newIsolateScopeDirective.template)
          scopeToChild = isolateScope ;

        childLinkFn(scopeToChild, linkNode.childNodes, boundTranscludeFn) ;
      }

      // Post-Linking
      Utils.forEachRight(postLinkFns, function (linkFn) {
        linkFn(
          linkFn.isolateScope ? isolateScope : scope,
          linkNode,
          attrs,
          linkFn.require && getControllers(linkFn.require, linkNode),
          scopeBoundTranscludeFn
        ) ;
      }) ;
    }
    nodeLinkFn.terminal = terminal ;
    nodeLinkFn.scope    = newScopeDirective && newScopeDirective.scope ;
    nodeLinkFn.transcludeOnThisElement = hasTranscludeDirective ;
    nodeLinkFn.transclude = childTranscludeFn ;

    return nodeLinkFn ;
  }

  /**
   * Indique si la directive name est une directive multi-element
   * @param {String} name Nom de la directive
   * @returns {Boolean}
   * @private
   */
  _directiveIsMultiElement (name) {

    let multi = false ;
    if (this._directives.hasOwnProperty(name)) {
      let directives = this.get(name) ;
      Utils.forEach(directives, directive => {
        multi = multi || directive.multiElement === true ;
      }) ;
    }
    return multi ;
  }


  /**
   * Analyse la valeur value de l'attribut name et si $interpolate renvoit
   * trouve une expression, ajoute une directive qui permettra d'observer
   * et de binder la valeur de l'attribut
   * @param {Object} directives Les directives
   * @param {*} value Valeur de l'attribut
   * @param {String} name Nom de l'attribut
   * @private
   */
  _addAttrInterpolateDirective (directives, value, name) {

    const $interpolate = this.kernel().get('$interpolate') ;
    let interpolateFn = $interpolate(value, true) ;

    // Il y a au moins une expression
    if (interpolateFn) {
      directives.push({
        priority: 100,
        compile: function () {
          return {
            // Pour que cela soit exécuté au plus tôt !
            pre: function link (scope, element, attrs) {

              // Pas d'interpolate sur les eventHandlers (onClick, ...)
              if (/^on[a-z]+|formaction$/.test(name)) {
                throw 'Interpolations for HTML DOM event attributes'
                      + ' not allowed' ;
              }

              // On regarde si l'attribut n'a pas changé depuis la compilation
              let newValue = attrs[name] ;
              // Si oui, on met à jour interpolateFn
              if (newValue !== value)
                interpolateFn = newValue && $interpolate(newValue, true) ;
              // S'il n'y a plus de fonction, on n'enregistre pas de watcher
              if (!interpolateFn)
                return ;

              // On s'assure que l'observeur possède l'attribut _inter
              attrs._observers = attrs._observers || {} ;
              attrs._observers[name] = attrs._observers[name] || [] ;
              attrs._observers[name]._inter = true ;
              // On met a jour la valeur de l'attribut immédiatement
              attrs[name] = interpolateFn(scope) ;

              scope.$watch(interpolateFn, function (newValue) {
                attrs.$set(name, newValue) ; // $set pour pouvoir observer name
              }) ;
            }
          } ;
        }
      }) ;
    }
  }


  /**
   * Scan un groupe (pour une directive multinode)
   * @param {Object} node Node à scanner
   * @param {String} startAttr Nom de l'attribut du premier élément de la
   *    directive (my-directive-start)
   * @param {String} endAttr Nom de l'attribut du dernier élément de la
   *    directive (my-directive-end)
   * @returns {Object} Node
   * @private
   */
  _groupScan (node, startAttr, endAttr) {

    let nodes = [] ;

    // S'il y a un attribut _start
    if (startAttr && node && node.hasAttribute(startAttr)) {

      let depth = 0 ;
      do {
        if (node.nodeType === Node.ELEMENT_NODE) {
          if (node.hasAttribute(startAttr))
            depth++ ;
          else if (node.hasAttribute(endAttr))
            depth-- ;
        }
        nodes.push(node) ;
        node = node.nextSibling ;
      } while (depth > 0) ;
    }
    else
      nodes.push(node) ;

    return nodes ;
  }

  /**
   * Compile une directive de template
   * @param {Object} directives Directives
   * @param {Object} compileNode Nœud à compiler
   * @param {Object} attrs Attributs
   * @param {Object} previousCompileContext ??
   * @returns {Function}
   * @private
   */
  _compileTemplateUrl (directives, compileNode, attrs, previousCompileContext) {

    let origAsyncDirective = directives.shift() ;
    let derivedSyncDirective = Utils.extend(
      {},
      origAsyncDirective,
      {
        templateUrl: null,
        transclude:  null
      }
    ) ;

    let templateUrl = Utils.isFunction(origAsyncDirective.templateUrl) ?
      origAsyncDirective.templateUrl(compileNode, attrs)
      : origAsyncDirective.templateUrl ;

    let afterTemplateNodeLinkFn, afterTemplateChildLinkFn ;
    let linkQueue = [] ;

    compileNode.innerHTML = '' ;

    let self = this ;
    this.kernel().get('$http').get(templateUrl).success(function (template) {

      directives.unshift(derivedSyncDirective) ;
      compileNode.innerHTML = template ;
      afterTemplateNodeLinkFn = self._applyDirectivesToNode(
        directives, compileNode, attrs, previousCompileContext) ;
      afterTemplateChildLinkFn = self._compileNodes(compileNode.childNodes) ;

      linkQueue.forEach(function (linkCall) {
        afterTemplateNodeLinkFn(
          afterTemplateChildLinkFn,
          linkCall.scope,
          linkCall.linkNode,
          linkCall.boundTranscludeFn
        ) ;
      }) ;
      linkQueue = null ;

      // Lance une fonction après le chargement du template (utile pour le
      // debugage)
      if (Utils.isFunction(compileNode._afterTemplate))
        compileNode._afterTemplate() ;
    }) ;

    return function delayedNodeLinkFn (
      _ignoreChildLinkFn, scope, linkNode, boundTranscludeFn) {

      if (linkQueue) {
        linkQueue.push({
          scope: scope,
          linkNode: linkNode,
          boundTranscludeFn: boundTranscludeFn
        }) ;
      }
      else {
        afterTemplateNodeLinkFn(
          afterTemplateChildLinkFn, scope, linkNode, boundTranscludeFn) ;
      }
    } ;
  }

  /**
   * Ajoute une nouvelle directive si l'on trouve une expression dans text.
   * Ainsi cette expression sera observée par $watcher. Le nœud parent se
   * verra ajouter une classe kjs-binding et el.data('$binding') contiendra
   * un array de la liste des expressions.
   * @param {Array} directives Listes des directives
   * @param {String} text Texte dans lequel on cherche une expression
   * @private
   */
  _addTextInterpolateDirective (directives, text) {

    let $interpolate = this.kernel().get('$interpolate') ;
    let interpolateFn = $interpolate(text, true) ;

    if (interpolateFn) {
      directives.push({
        priority: 0,
        compile: function () {

          return function link (scope, element) {

            // On récupère la valeur actuelle de $binding
            // nécessaire si le parent possède plusieurs text nodes
            let bindings = element.parentNode.$binding || [] ;
            bindings = bindings.concat(interpolateFn.expressions) ;

            // On ajoute tous au parent
            element.parentNode.classList.add('kjs-binding') ;
            element.parentNode.$binding = bindings ;

            scope.$watch(interpolateFn, function (newValue) {
              element.nodeValue = newValue ;
            }) ;
          } ;
        }
      }) ;
    }
  }
}

module.exports = Compile ;


/**
 * Représente les attributs d'un élément
 */
class Attributes {

  /**
   * Initilise l'objet
   * @param {Node} element Élément
   * @param {Scope} rootScope Scope principal
   */
  constructor (element, rootScope) {

    this._element = element ;
    this.$attr = {} ;
    this.$rootScope = rootScope ;
  }


  /**
   * Définie un attribue
   * @param {String} key Nom
   * @param {*} value valeur
   * @param {Boolean} writeAttr Écrit la valeur de l'attribut dans l'élément
   * @param {String} attrName Nom de l'attribut dans l'élément
   */
  $set (key, value, writeAttr, attrName) {

    this[key] = value ;

    if (isBooleanAttribute(this._element, key))
      this._element[key] = value ;

    if (!attrName) {
      if (this.$attr[key])
        attrName = this.$attr[key] ;
      else
        attrName = this.$attr[key] = Utils.kebabCase(key) ;
    } else
      this.$attr[key] = attrName ;

    if (writeAttr !== false && this._element.nodeType != Node.COMMENT_NODE)
      this._element.setAttribute(attrName, value) ;

    // On exécute les observeurs
    if (this._observers) {
      Utils.forEach(this._observers[key], observer => {
        try {
          observer(value) ;
        } catch (e) {
          console.log(e) ;
        }
      }) ;
    }
  }


  /**
   * Enregistre l'observeur `fn` sur l'attribut `key`. À chaque fois que `$set`
   * sera appelé sur cet attribut, `fn(value)` avec la nouvelle valeur sera
   * exécutée.
   * Par rapport à une valeur observée par `$watch` (cf Scope), on exécute pas
   * de test à chaque digest, on économise donc du CPU. Cependant, si la
   * valeur d'un attribut est modifié en dehors de `$set`, l'observateur ne
   * sera pas informé : c'est la limite par rapport à `$watch`.
   * Un observer est appelé au prochain `$digest` après sa déclaration.
   * La fonction retourne une fonction de désenregistrement qui permet de
   * détruire l'observeur lorsqu'elle est appelée.
   * @param {String} key Attribut à observer
   * @param {Function} fn Observer
   * @returns {Function}
   */
  $observe (key, fn) {

    // On utilise Object.create(null) pour être sûr de n'avoir aucune
    // propriété (constructor, toString, ...) qui serait en conflit avec le
    // nom d'un attribut.
    this._observers = this._observers || Object.create(null) ;
    this._observers[key] = this._observers[key] || [] ;
    this._observers[key].push(fn) ;

    // On exécute l'observeur au prochain $digest sauf s'il possède
    // l'attribut _inter
    this.$rootScope.$evalAsync(() => {
      if (!this._observers[key]._inter)
        fn(this[key]) ;
    }) ;

    // En renvoit la fonction de destruction
    return () => {
      let index = this._observers[key].indexOf(fn) ;
      if (index >= 0)
        this._observers[key].splice(index, 1) ;
    } ;
  }


  /**
   * Ajoute la classe classVal à l'élément.
   * @param {String} classVal Class à ajouter
   */
  $addClass (classVal) {

    this._element.classList.add(classVal) ;
  }


  /**
   * Retire la classe classVal de l'élément.
   * @param {String} classVal Class à retirer
   */
  $removeClass (classVal) {

    this._element.classList.remove(classVal) ;
  }


  /**
   * Met à jour les classes de l'élément :
   *   - ajoute toutes les classes présentes dans newClassVal mais pas dans
   *     oldClassVal
   *   - retire toutes les classes présentes dans oldClassVal mais pas dans
   *     newClassVal
   * @param {Array} newClassVal classes à ajouter
   * @param {Array} oldClassVal classes à supprimer
   */
  $updateClass (newClassVal, oldClassVal) {

    let newClasses = newClassVal.split(/\s+/) ;
    let oldClasses = oldClassVal.split(/\s+/) ;

    let addedClasses   = [],
        removedClasses = [] ;

    newClasses.forEach(cls => {
      if (oldClasses.indexOf(cls) == -1)
        addedClasses.push(cls) ;
    }) ;
    oldClasses.forEach(cls => {
      if (newClasses.indexOf(cls) == -1)
        removedClasses.push(cls) ;
    }) ;

    if (addedClasses.length)
      this.$addClass(addedClasses.join(' ')) ;
    if (removedClasses.length)
      this.$removeClass(removedClasses.join(' ')) ;
  }
}


let BOOLEAN_ATTRS = {
  multiple: true,
  selected: true,
  checked:  true,
  disabled: true,
  readOnly: true,
  required: true,
  open:     true
} ;
let BOOLEAN_ELEMENTS = {
  INPUT:    true,
  SELECT:   true,
  OPTION:   true,
  TEXTAREA: true,
  BUTTON:   true,
  FORM:     true,
  DETAILS:  true
} ;


/**
 * Indique si l'attribut attrName de l'élément node est un attribut de type
 * boollean.
 * @param {Node} node Node
 * @param {String} attrName Nom de l'attribut
 * @returns {Boolean}
 */
function isBooleanAttribute (node, attrName) {

  return BOOLEAN_ATTRS[attrName] && BOOLEAN_ELEMENTS[node.nodeName] ;
}


/**
 * Retourne le nom du nœud DOM element
 * @param {Object} element Élement DOM
 * @returns {String}
 */
function nodeName (element) {

  return element.nodeName ? element.nodeName : element[0].nodeName ;
}


/**
 * Normalise le nom de la directive name
 * @param {String} name Nom de la directive à normaliser
 * @returns {String}
 */
let PREFIX_REGEXP = /(x[:\-_]|data[:\-_])/i ;
function directiveNormalize (name) {

  return Utils.camelCase(name.replace(PREFIX_REGEXP, '')) ;
}


/**
 * Fonction de trie selon l'attribut priority des directives a et b ou selon
 * l'attribut name si les priorités sont identiques. Enfin, on prendra l'index
 * si le reste est identique.
 * @param {Object} a Directive a
 * @param {Object} b Directive b
 * @returns {Integer}
 */
function byPriority (a, b) {

  let diff = b.priority - a.priority ;
  if (diff !== 0)
    return diff ;
  else {
    if (a.name !== b.name)
      return (a.name < b.name ? -1 : 1) ;
    else
      return a.index - b.index ;
  }
}

