'use strict' ;

/**
 * src/browser/Application.test.js
 */

const expect = require('chai').expect ;

const Assets      = require('./Services/Assets'),
      Compile     = require('./Services/Compile'),
      Controller  = require('./Services/Controller'),
      Http        = require('./Services/Http'),
      Logs        = require('../core/Services/Logs') ;

describe('browser Application', function () {

  it('initialise des services', function () {

    let A        = window.K.app(),
        services = A.kernel().container() ;

    expect(services.has('$http')).to.be.true ;
    expect(services.get('$http')).to.be.instanceOf(Http) ;
    expect(services.has('$controller')).to.be.true ;
    expect(services.get('$controller')).to.be.instanceOf(Controller) ;
    expect(services.has('$compile')).to.be.true ;
    expect(services.get('$compile')).to.be.instanceOf(Compile) ;
    expect(services.has('$assets')).to.be.true ;
    expect(services.get('$assets')).to.be.instanceOf(Assets) ;
    expect(services.has('$logs')).to.be.true ;
    expect(services.get('$logs')).to.be.instanceOf(Logs) ;
  }) ;

  it('initialise des directives', function () {

    let A       = window.K.app(),
        compile = A.kernel().get('$compile') ;

    expect(compile.has('kjsController')).to.be.true ;
    expect(compile.has('kjsClick')).to.be.true ;
    expect(compile.has('kjsTransclude')).to.be.true ;
  }) ;

  it('controller permet de définir un controller', function () {

    let A = window.K.app() ;

    function MyController () {}

    A.controller('test', MyController) ;

    expect(A.kernel().get('$controller').get('test'))
      .to.be.instanceOf(MyController) ;
  }) ;
}) ;

