'use strict' ;

/**
 * src/browser/directives/kjs_show.test.js
 */

const expect = require('chai').expect ;

const Application = require('../Application'),
      Utils       = require('../Utils') ;

describe('kjsShow', function () {

  let app, compile, rootScope ;

  beforeEach(function () {

    app = new Application() ;
    compile   = app.kernel().get('$compile') ;
    rootScope = app.kernel().get('$rootScope') ;
  }) ;

  it('if attribute is true, show element', function () {

    let element = Utils.str2DomElement(
      '<div><p kjs-show="{{true}}"></p></div>') ;
    compile.run(element)(rootScope) ;
    rootScope.$apply() ;

    expect(element[0].querySelectorAll('p')[0]
      .classList.contains('visually-hidden')).to.be.false ;
  }) ;

  it('if attribute is false, hide element', function () {

    let element = Utils.str2DomElement(
      '<div><p kjs-show="{{false}}"></p></div>') ;
    compile.run(element)(rootScope) ;
    rootScope.$apply() ;

    expect(element[0].querySelectorAll('p')[0]
      .classList.contains('visually-hidden')).to.be.true ;
  }) ;

  it('works with property of scope', function () {

    rootScope.foo = true ;
    rootScope.bar = false ;
    let element1 = Utils.str2DomElement(
      '<div><p kjs-show="foo"></p></div>') ;
    let element2 = Utils.str2DomElement(
      '<div><p kjs-show="bar"></p></div>') ;
    compile.run(element1)(rootScope) ;
    compile.run(element2)(rootScope) ;
    rootScope.$apply() ;

    expect(element1[0].querySelectorAll('p')[0]
      .classList.contains('visually-hidden')).to.be.false ;
    expect(element2[0].querySelectorAll('p')[0]
      .classList.contains('visually-hidden')).to.be.true ;
  }) ;

  it('if the value in scope change, the element is updated', function () {

    rootScope.foo = true ;
    let element = Utils.str2DomElement(
      '<div><p kjs-show="foo"></p></div>') ;
    compile.run(element)(rootScope) ;
    rootScope.$apply() ;

    expect(element[0].querySelectorAll('p')[0]
      .classList.contains('visually-hidden')).to.be.false ;

    rootScope.foo = false ;
    rootScope.$apply() ;

    expect(element[0].querySelectorAll('p')[0]
      .classList.contains('visually-hidden')).to.be.true ;
  }) ;
}) ;

