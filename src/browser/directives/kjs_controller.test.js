'use strict' ;

/**
 * src/browser/directives/kjs_controller.test.js
 */

const expect = require('chai').expect ;

const Application = require('../Application'),
      Utils       = require('../Utils') ;

const app = new Application() ;

describe('kjsController', function () {

  let compile, rootScope, controllerProvider ;

  beforeEach(function () {

    compile   = app.kernel().get('$compile') ;
    rootScope = app.kernel().get('$rootScope') ;
    controllerProvider = app.kernel().get('$controller') ;
  }) ;

  it('is instantiated during compilation & linking', function () {

    let instantiated ;
    function MyController () {
      instantiated = true ;
    }
    controllerProvider.register('MyController', MyController) ;
    let el = Utils.str2DomElement('<div kjs-controller="MyController"></div>') ;
    compile.run(el)(rootScope) ;
    expect(instantiated).to.be.equal(true) ;
  }) ;

  it('may inject scope, element and attrs', function () {

    let gotScope, gotElement, gotAttrs ;
    function MyController ($scope, $element, $attrs) {
      gotScope   = $scope ;
      gotElement = $element ;
      gotAttrs   = $attrs ;
    }
    controllerProvider.register('MyController', MyController) ;
    let el = Utils.str2DomElement('<div kjs-controller="MyController"></div>') ;
    compile.run(el)(rootScope) ;
    expect(gotScope).to.be.not.undefined ;
    expect(gotElement).to.be.not.undefined ;
    expect(gotAttrs).to.be.not.undefined ;
  }) ;

  it('has an inherited scope', function () {

    let gotScope ;
    function MyController ($scope) {
      gotScope = $scope ;
    }
    controllerProvider.register('MyController', MyController) ;
    let el = Utils.str2DomElement('<div kjs-controller="MyController"></div>') ;
    compile.run(el)(rootScope) ;
    expect(gotScope).to.be.not.equal(rootScope) ;
    expect(gotScope.$parent).to.be.equal(rootScope) ;
    expect(Object.getPrototypeOf(gotScope)).to.be.equal(rootScope) ;
  }) ;

  it('allows aliasing controller in expression', function () {

    let gotScope ;
    function MyController ($scope) {
      gotScope = $scope ;
    }
    controllerProvider.register('MyController', MyController) ;
    let el = Utils.str2DomElement(
      '<div kjs-controller="MyController as myCtrl"></div>') ;
    compile.run(el)(rootScope) ;
    expect(gotScope.myCtrl).to.be.not.undefined ;
    expect(gotScope.myCtrl).to.be.instanceOf(MyController) ;
    expect(Object.getPrototypeOf(gotScope)).to.be.equal(rootScope) ;
  }) ;

  it('allows looking up controller from surrounding scope', function () {

    let gotScope ;
    function MyController ($scope) {
      gotScope = $scope ;
    }
    let el = Utils.str2DomElement(
      '<div kjs-controller="MyCtrlOnScope as myCtrl"></div>') ;
    rootScope.MyCtrlOnScope = MyController ;
    compile.run(el)(rootScope) ;
    expect(gotScope.myCtrl).to.be.not.undefined ;
    expect(gotScope.myCtrl).to.be.instanceOf(MyController) ;
  }) ;
}) ;

