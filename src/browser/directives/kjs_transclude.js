'use strict' ;

/**
 * src/directives/kjs_transclude.js
 */

function kjsTranscludeDirective () {

  return {
    restrict: 'EA',
    link: function (scope, element, attrs, ctrl, transclude) {
      transclude(function (clone) {
        element.innerHTML = '' ;
        element.appendChild(clone[0]) ;
      }) ;
    }
  } ;
}

module.exports = kjsTranscludeDirective ;

