'use strict' ;

/**
 * src/browser/directives/kjs_controller.js
 */

let kjsControllerDirective = function () {

  return {
    restrict:   'A',
    scope:      true,
    controller: '@'
  } ;
} ;

module.exports = kjsControllerDirective ;

