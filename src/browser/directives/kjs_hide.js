'use strict' ;

/**
 * src/browser/directives/kjs_hide.js
 */

function kjsHideDirective () {

  return {
    restrict: 'A',
    link: function (scope, element, attrs) {

      const hidden = value => {

        if (value)
          element.classList.add('visually-hidden') ;
        else
          element.classList.remove('visually-hidden') ;
      } ;

      hidden(attrs.kjsHide) ;
      scope.$watch(attrs.kjsHide, hidden) ;
    }
  } ;
}

module.exports = kjsHideDirective ;

