'use strict' ;

/**
 * src/server/Slot.test.js
 */

const expect = require('chai').expect,
      sinon  = require('sinon'),
      http   = require('http') ;

const Slot             = require('./Slot'),
      SlotError        = require('./SlotError'),
      View             = require('./View'),
      Kernel           = require('../core/Kernel'),
      Color            = require('../cli/Color'),
      IndexController  = require(
        '../../doc/demo-server/controller/Index/IndexController'),
      StaticController = require('./StaticController'),
      User             = require('./User/User'),
      Resp             = require('./Resp') ;

// On force une nouvelle instance
const kernel = new Kernel(true) ;
let basepath = process.cwd() + '/doc/demo-server/' ;
kernel.get('$config').set('basepath', basepath) ;
kernel.get('$config').set('controllerDir', 'controller/') ;
kernel.container().set('$color', new Color()) ;


describe('server/Slot', function () {

  it('charge un controller et l\'action correspondante', function () {

    let vars     = {a: 1, b: 2},
        request  = {},
        response = {} ;
    let slot = new Slot('index', 'index', vars, request, response) ;

    expect(slot._controllerName).to.be.equal('Index') ;
    expect(slot._controller).to.be.instanceof(IndexController) ;
    expect(slot._action).to.be.equal('index') ;
    expect(slot._vars).to.be.equal(vars) ;
    expect(slot._request).to.be.equal(request) ;
    expect(slot._response).to.be.equal(response) ;
  }) ;

  it('exécute un controller interne si prefix @', function () {

    let slot = new Slot('@static') ;

    expect(slot._controllerName).to.be.equal('@static') ;
    expect(slot._controller).to.be.instanceof(StaticController) ;
  }) ;

  it('on peut transmettre un objet comme controller', function () {

    class Ctrl { indexAction () {} }
    Ctrl.prototype.setViews = { index: null } ;

    let slot = new Slot(Ctrl, 'index') ;
    expect(slot._controllerName).to.be.equal('[object]') ;
    expect(slot._controller).to.be.instanceof(Ctrl) ;
  }) ;

  it('lance une exception si le controller n\'est pas trouvé', function () {

    let exec = function () {
      new Slot('salut') ;
    } ;

    expect(exec.bind({})).to.throw(SlotError, 'Controller `Salut` not found') ;
  }) ;

  it('lance une exception si l\'action n\'est pas trouvée', function () {

    let exec = function () {
      new Slot('index', 'foobar') ;
    } ;

    expect(exec.bind({}))
      .to.throw(SlotError, 'Action `foobar` of controller `Index` not found') ;
  }) ;

  it('transmet l\'accès au Kernel au controller', function () {

    let slot = new Slot('index', 'index') ;

    expect(slot._controller.kernel()).to.be.equal(kernel) ;
  }) ;

  it('les controller ont get() donnant accès aux services', function () {

    let slot = new Slot('index', 'index') ;

    expect(slot._controller.get('$config')).to.be.equal(kernel.get('$config')) ;
  }) ;

  it('accès à la requête et à la réponse', function () {

    let vars     = {a: 1, b: 2},
        request  = {},
        response = {} ;
    let slot = new Slot('index', 'index', vars, request, response) ;

    expect(slot._controller.$request).to.be.equal(request) ;
    expect(slot._controller.$response).to.be.equal(response) ;
  }) ;

  it('transmet un nouvel objet User', function () {

    let vars     = {a: 1, b: 2},
        request  = {},
        response = {} ;
    let slot = new Slot('index', 'index', vars, request, response) ;

    expect(slot._controller.$user).to.be.instanceOf(User) ;
    expect(slot._userInit).to.be.instanceOf(Promise) ;
  }) ;

  it('si slot(... user), le transmet (pas de nouvel obj créé)', function () {

    let vars     = {a: 1, b: 2},
        request  = {},
        response = {},
        user     = {} ;
    let slot = new Slot('index', 'index', vars, request, response, user) ;

    expect(slot._controller.$user).to.be.equal(user) ;
    expect(slot._userInit).to.be.instanceOf(Promise) ;
  }) ;

  it('l\'objet réponse reçoit cet objet User', function () {

    let vars     = {a: 1, b: 2},
        request  = {},
        response = {},
        user     = {} ;
    let slot = new Slot('index', 'index', vars, request, response, user) ;

    expect(slot._response.$user).to.be.equal(user) ;
  }) ;

  it('récupère la configuration de user dans la config générale', function () {

    let vars     = {a: 1, b: 2},
        request  = {},
        response = {} ;
    kernel.get('$config').set('user', {sessionCookieName: 'hello'}) ;
    let slot = new Slot('index', 'index', vars, request, response) ;
    kernel.get('$config').delete('user') ;

    expect(slot._controller.$user._config.sessionCookieName)
      .to.be.equal('hello') ;
  }) ;

  it('user peut être récupéré à partir d\'un objet particulier', function () {

    let vars     = {a: 1, b: 2},
        request  = {},
        response = {} ;
    class MyObject extends User {}
    kernel.get('$config').set('$userObj', MyObject) ;
    let slot = new Slot('index', 'index', vars, request, response) ;
    kernel.get('$config').delete('$userObj') ;

    expect(slot._controller.$user).to.be.instanceOf(MyObject) ;
  }) ;

  it('l\'init d\'User a été appelé', function () {

    let vars     = {a: 1, b: 2},
        request  = {},
        response = {} ;
    class MyObject extends User {
      init () {
        this.foo = 'bar' ;
      }
    }
    kernel.get('$config').set('$userObj', MyObject) ;
    let slot = new Slot('index', 'index', vars, request, response) ;
    kernel.get('$config').delete('$userObj') ;

    expect(slot._controller.$user.foo).to.be.equal('bar') ;
  }) ;

  it('associe une vue au Slot', function () {

    let slot = new Slot('index', 'index') ;

    expect(slot._view).to.be.instanceof(View) ;
    expect(slot._view._contentSource)
      .to.be.equal(
        '<h1>Hello World!</h1>\n\n<p>{{message}}</p>\n\n<div>\n  <p>\n  '
        + 'Ici, nous avons un bloc inclu dans une autre vue !\n</p>\n<p>\n  '
        + 'Il peut également utiliser les données transmises à la vue : '
        + '{{foobar}}\n</p>\n\n\n</div>\n\n'
      ) ;
    expect(slot._controller.$view).to.be.equal(slot._view) ;
  }) ;

  it('transmet à la vue : requête, réponse, user', function () {

    let request  = {},
        response = {} ;

    let slot = new Slot('index', 'index', {}, request, response) ;

    expect(slot._controller.$view.$request)
      .to.be.equal(slot._controller.$request) ;
    expect(slot._controller.$view.$response)
      .to.be.equal(slot._controller.$response) ;
    expect(slot._controller.$view.$user)
      .to.be.equal(slot._controller.$user) ;
  }) ;

  it('peut associer une vue vide si demandée', function () {

    let slot = new Slot('index', 'blank') ;

    expect(slot._view).to.be.instanceof(View) ;
    expect(slot._view._contentSource).to.be.equal('') ;
  }) ;

  it('associe une vue spécifique au Slot', function () {

    let slot = new Slot('index', 'alias') ;

    expect(slot._view).to.be.instanceof(View) ;
    expect(slot._view._contentSource)
      .to.be.equal(
        '<h1>Hello World!</h1>\n\n<p>{{message}}</p>\n\n<div>\n  <p>\n  '
        + 'Ici, nous avons un bloc inclu dans une autre vue !\n</p>\n<p>\n  '
        + 'Il peut également utiliser les données transmises à la vue : '
        + '{{foobar}}\n</p>\n\n\n</div>\n\n'
      ) ;
    expect(slot._controller.$view).to.be.equal(slot._view) ;
  }) ;

  it('charge un layout si défini dans la config', function () {

    kernel.get('$config').set('layout.html', 'controller/layout.html') ;
    let slot = new Slot('index', 'alias') ;

    expect(slot._controller.$view._layoutSource)
      .to.be.equal('<!DOCTYPE html>\n\n'
        + '<html>\n  <head>\n    <meta charset="utf-8" />\n'
        + '    <title>{{title}} - Démo</title>\n'
        + '    <link rel="stylesheet" href="./styles.css" />\n'
        + '  </head>\n\n  <body>\n    '
        + '{{content}}'
        + '\n  </body>\n</html>\n') ;
  }) ;

  it('exécute le slot', function () {

    let vars     = {a: 1, b: 2},
        request  = {},
        response = {} ;
    let slot = new Slot('index', 'index', vars, request, response) ;
    let spy = sinon.spy(slot._controller, 'indexAction') ;

    let execute = slot.execute() ;
    expect(execute).to.be.instanceOf(Promise) ;

    return execute.then(() => {
      expect(spy.calledOnce).to.be.true ;
    }) ;
  }) ;

  it('exécute la ressource si le controller en est une', function () {

    let slot = new Slot(
      '@static', '',
      {file: 'test.css'},
      {},
      new Resp(new http.ServerResponse('get'))) ;
    let spy = sinon.spy(slot._controller, 'slotAction') ;

    return slot.execute().catch(() => {
      expect(spy.calledOnce).to.be.true ;
    }) ;
  }) ;

  it('si User.init() Promise, exec que lors du resolve', function () {

    let vars     = {a: 1, b: 2},
        request  = {},
        response = {} ;
    class MyObject extends User {
      init () {
        return new Promise(resolve => {
          setTimeout(() => {
            this.foo = 'bar' ;
            resolve() ;
          }, 100) ;
        }) ;
      }
    }
    kernel.get('$config').set('$userObj', MyObject) ;
    let slot = new Slot('index', 'index', vars, request, response) ;
    kernel.get('$config').delete('$userObj') ;

    return slot.execute().then(() => {
      expect(slot._controller.foo).to.be.equal('bar') ;
    }) ;
  }) ;

  it('si un init() existe, il est exécuté', function () {

    let vars     = {a: 1, b: 2},
        request  = {},
        response = {} ;
    let slot = new Slot('index', 'index', vars, request, response) ;
    slot._controller.init = function () {} ;
    let spy = sinon.spy(slot._controller, 'init') ;

    return slot.execute().then(() => {
      expect(spy.calledOnce).to.be.true ;
    }) ;
  }) ;

  it('si un init() peut être asynchrone', function () {

    let vars     = {a: 1, b: 2},
        request  = {},
        response = {},
        foo = '' ;
    let slot = new Slot('index', 'index', vars, request, response) ;
    slot._controller.init = function () {
      return new Promise(resolve => {
        foo = 'bar' ;
        resolve() ;
      }) ;
    } ;
    let spy = sinon.spy(slot._controller, 'init') ;

    return slot.execute().then(() => {
      expect(spy.calledOnce).to.be.true ;
      expect(foo).to.be.equal('bar') ;
    }) ;
  }) ;

  it('l\'init reçoit les paramètres', function () {

    let vars     = {a: 1, b: 2} ;
    let slot = new Slot('index', 'index', vars) ;
    slot._controller.init = function () {} ;
    let spy = sinon.spy(slot._controller, 'init') ;

    return slot.execute().then(() => {
      expect(spy.calledWith(vars)).to.be.true ;
    }) ;
  }) ;

  it('l\'action peut être asynchrone', function () {

    let slot = new Slot('index', 'async', {}) ;

    return slot.execute().then(() => {
      expect(slot._controller.okAsync).to.be.equal('ok') ;
    }) ;
  }) ;

  it('l\'action reçoit les paramètres', function () {

    let vars     = {a: 1, b: 2} ;
    let slot = new Slot('index', 'index', vars) ;
    let spy = sinon.spy(slot._controller, 'indexAction') ;

    return slot.execute().then(() => {
      expect(spy.calledWith(vars)).to.be.true ;
    }) ;
  }) ;

  it('l\'action reçoit les data POST', function () {

    let vars     = {a: 1, b: 2} ;
    let slot = new Slot('index', 'index', vars) ;
    let spy = sinon.spy(slot._controller, 'indexAction') ;

    return slot.execute({c: 'test'}).then(() => {
      expect(spy.calledWith({a: 1, b: 2, c: 'test'})).to.be.true ;
    }) ;
  }) ;

  it('if async action returns reject(code), send error', function () {

    class Ctrl {
      indexAction () {}
      error404Action () { return Promise.reject(404) ; }
      errorAction () { return Promise.reject('Un message') ; }
    }
    Ctrl.prototype.setViews = {
      index: null,
      error404: null,
      error: null
    } ;

    let anError = null ;
    let resp = {
      sendError404: () => { anError = 404 ; return Promise.resolve(anError) ; },
      sendError500: () => { anError = 500 ; return Promise.resolve(anError) ; }
    } ;

    let slot1 = new Slot(Ctrl, 'error404', null, null, resp, {}) ;
    let slot2 = new Slot(Ctrl, 'error', null, null, resp, {}) ;

    return slot1.execute().catch(code => {

      expect(code).to.be.equal(404) ;
      expect(anError).to.be.equal(404) ;

      return slot2.execute().catch(code => {

        expect(code).to.be.equal(500) ;
        expect(anError).to.be.equal(500) ;
      }) ;
    }) ;
  }) ;

  it('notifie le canal `Slot` lorsqu\'un slot est initialisé', function () {

    kernel.events().register('Slot') ;
    let spy = sinon.spy() ;
    kernel.events().attach('Slot', 'spy', spy) ;

    let slot = new Slot('index', 'index') ;

    expect(spy.getCall(0).args[0]).to.be.equal(
      'Slot ' + slot._controllerName + ':' + slot._action + ' initialisé'
    ) ;
  }) ;

  it('getView retourne la vue', function () {

    let slot = new Slot('index', 'index') ;

    expect(slot.getView()).to.be.equal(slot._view) ;
  }) ;
}) ;

