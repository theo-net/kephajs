'use strict' ;

/**
 * src/server/Resp.js
 */

const Slot = require('./Slot') ;

/**
 * Objet réponse
 */
class Resp {

  /**
   * Initialise l'objet avec la `response` du serveur
   * @param {http.ServerResponse} response Réponse du serveur
   * @param {http.IncomingMessage} request Requête reçue
   */
  constructor (response, request) {

    this._response = response ;
    this._request  = request ;
    this._cookies = [] ;
    this._view = null ;
    this.$user = null ;
  }


  /**
   * Alias vers response.end()
   * @param {string|Buffer} data Données à renvoyer
   * @param {string} encoding Encodage
   * @param {Function} callback Callback à exécuter après la réponse
   */
  end (data, encoding, callback) {

    this.setHeader('Set-Cookie', this._cookies) ;

    this._response.end(data, encoding, callback) ;
  }


  /**
   * Indique si la réponse a été envoyée ou non
   * @returns {Boolean}
   */
  finished () {

    return this._response.finished ;
  }


  /**
   * Envoit le rendu d'une vue
   * @param {View} view Vue à envoyer
   * @param {Function} [viewRenderCallback] Juste pour passer un test
   */
  send (view, viewRenderCallback) {

    this._view = view ;
    this.done(undefined, viewRenderCallback) ;
  }


  /**
   * Rendu fini, on envoit la réponse. Attention, la réponse sera forcément
   * envoyée ! Donc, ne pas utiliser dans le controller `Error` sauf si vous
   * précisez manuellement le code de retour (500, 404, ...)
   * @param {string|Buffer} [data] Données à renvoyer
   * @param {Function} [viewRenderCallback] Juste pour passer un test
   */
  done (data, viewRenderCallback = () => {}) {

    if (!this.finished()) {

      if (data === null)
        this.end() ;
      else if (this._view) {
        this.setContentType(this._view.$getMimeType()) ;
        if (data)
          this.end(data) ;
        else {
          this._view.$render().then(render => {
            this.end(render) ;
            viewRenderCallback() ;
          }) ;
        }
      }
      else if (data)
        this.end(data) ;
    }
  }


  /**
   * Pipe un flux vers la réponse. Attention, quand le flux est entièrement lu,
   * la réponse est envoyée complètement. On ne peut plus envoyer d'autres
   * contenus, comme le rendu d'une vue par exemple.
   * C'est le moyen idéal pour envoyer de gros contenus.
   * @param {Stream} flux Flux à envoyer
   */
  pipeFrom (flux) {

    flux.pipe(this._response) ;
  }


  /**
   * Alias vers response.setHeader()
   * @param {string} name Nom de l'header
   * @param {string} value Valeur de l'header
   */
  setHeader (name, value) {

    this._response.setHeader(name, value) ;
  }


  /**
   * Alias vers response.getHeader()
   * @param {string} name Nom de l'header que l'on veut lire
   * @returns {string}
   */
  getHeader (name) {

    return this._response.getHeader(name) ;
  }


  /**
   * Alias vers response.getHeaders()
   * @returns {Object}
   */
  getHeaders () {

    return this._response.getHeaders() ;
  }


  /**
   * alias vers response.headersSent()
   * @returns {Boolean}
   */
  headersSent () {

    return this._response.headersSent ;
  }


  /**
   * Définie le Content-Type de la réponse
   * @param {string} type Content-Type
   */
  setContentType (type) {

    this._response.setHeader('Content-Type', type) ;
  }


  /**
   * Lance une redirection HTTP
   * @param {String} url URL de redirection
   */
  redirect (url) {

    this._response.statusCode = 302 ;
    this._response.setHeader('Location', url) ;
    this.end() ;
  }


  /**
   * Définit le code retour de la requête HTTP
   * @param {Integer} code Code à renvoyer
   */
  setStatusCode (code) {

    this._response.statusCode = code ;
  }


  /**
   * Envoit une erreur 403
   * @returns {Promise}
   */
  sendError403 () {

    return this._sendError(403) ;
  }


  /**
   * Envoit une erreur 404
   * @returns {Promise}
   */
  sendError404 () {

    return this._sendError(404) ;
  }


  /**
   * Envoit une erreur 500
   * @returns {Promise}
   */
  sendError500 () {

    return this._sendError(500) ;
  }


  /**
   * Envoit un slot
   * @param {String} controller Nom du controller
   * @param {String} action Nom de l'action
   * @param {Object} vars Variables à transmettre au slot
   * @returns {Promise}
   */
  sendSlot (controller, action, vars = {}) {

    let slot = new Slot(
      controller, action, vars,
      this._request, this, this.$user
    ) ;

    return slot.execute().then(() => {
      this.send(slot.getView()) ;
      return Promise.resolve(this._response.statusCode) ;
    }) ;
  }


  /**
   * Envoit une erreur
   * @param {Integer} code Code erreur HTTP
   * @returns {Promise}
   * @private
   */
  _sendError (code) {

    // Évite une boucle infinie (si le slot envoit déjà la même erreur)
    if (this._response.statusCode < code) {

      this._response.statusCode = code ;
      return this.sendSlot('error', 'e' + code, null) ;
    }
    return Promise.resolve(code) ;
  }
}

module.exports = Resp ;

