'use strict' ;

/*
 * src/server/Services/SlotError.js
 */

const BaseError = require('../core/Error/BaseError') ;

class SlotError extends BaseError {
}

module.exports = SlotError ;

