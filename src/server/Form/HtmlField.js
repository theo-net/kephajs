'use strict' ;

/**
 * src/server/Form/HtmlField.js
 */

const Field = require('./Field') ;

class HtmlField extends Field {

  /**
   * Ce type de field sert uniquement à insérer du code HTML entre d'autres fields.
   * Un attribut `html` fournit le code HTML à insérer
   */
  init () {

    this._html = this._attr.html ? this._attr.html : '' ;
  }


  build () {

    return this._html ;
  }
}

module.exports = HtmlField ;

