'use strict' ;

/**
 * src/server/Form/TimeField.js
 */

const InputField = require('./InputField') ;

class TimeField extends InputField {

  init () {

    super.init() ;
    this._type = 'time' ;

    this._validator.add(/^[0-1]\d:[0-5]\d$/) ;
  }
}

module.exports = TimeField ;

