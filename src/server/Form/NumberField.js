'use strict' ;

/**
 * src/server/Form/NumberField.js
 */

const InputField = require('./InputField') ;

class NumberField extends InputField {

  init () {

    super.init() ;
    this._type = 'number' ;

    let validator = {} ;

    if (this._attr.min) {
      validator.min = this._attr.min ;
      this._min = this._attr.min ;
    }
    if (this._attr.max) {
      validator.max = this._attr.max ;
      this._max = this._attr.max ;
    }

    this._validator.add({float: validator}) ;

    if (this._attr.step) {
      this._step = this._attr.step ;
      this._validator.add(value => {
        return value % this._step === 0 ;
      }) ;
    }
  }


  /**
   * Essayera de transformer en nombre la valeur
   * @param {*} value La valeur
   */
  setValue (value) {

    this._value = Number(value) ;
    this._valueAttr = Number(value) ;
  }


  _buildAttributes () {

    return super._buildAttributes()
      + (this._min ? ' min="' + this._min + '"' : '')
      + (this._max ? ' max="' + this._max + '"' : '')
      + (this._step ? ' step="' + this._step + '"' : '') ;
  }
}

module.exports = NumberField ;

