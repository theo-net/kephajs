
'use strict' ;

/**
 * src/server/Form/SelectField.js
 */

const Field = require('./Field') ;

class SelectField extends Field {

  /**
   * Nouveaux attributs disponibles :
   *  - `[size]` : s'il s'agit d'un selecteur multiple : nombre de ligne affichées
   *  - `[multiple]` : `boolean` indiquant s'il s'agit d'un selecteur multiple
   *  - `[list]` : liste des valeurs du champ sous forme de tableau d'objets.
   *    Un objet est soit de la forme `{value, label}`, pour un élément, soit
   *    `{group, label}` pour un sous-groupe avec `group` étant un tableau d'objets
   *    de la même manière. De plus, les éléments peuvent avoir les propriétés
   *    `disabled` est `selected` (définies avec un boolean). Un groupe ne peut
   *    avoir que `disabled`.
   */
  init () {

    this._size = this._attr.size ? this._attr.size : null ;
    this._multiple = this._attr.multiple ? true : false ;

    if (Array.isArray(this._attr.list))
      this._list = this._attr.list ;
    else
      this._list = null ;
  }


  build () {

    let widget = '  <div'
               + (this.getGroupClass() != '' ?
                  ' class="' + this.getGroupClass() + '"' : '')
               + '>\n' ;

    widget += this._buildLabel()
           + '<select' + this._buildAttributes() + '>\n    '
           + this._buildList(this._list)
           + '</select><i></i>\n    ' + this._buildMsg() + '</div>' ;

    return widget ;
  }


  /**
   * Définie la valeur du field
   * @param {*} value Valeur ou tableau de valeurs
   * @param {Array} [list] Liste sur laquelle on applique la valeur (utile
   *        uniquement en interne)
   */
  setValue (value, list) {

    list = list ? list : this._list ;

    if (Array.isArray(list)) {
      list.forEach(elmt => {
        if (elmt.value == value)
          elmt.selected = true ;
        else if (Array.isArray(value) && value.indexOf(elmt.value) > -1)
          elmt.selected = true ;
        else if (Array.isArray(elmt.group))
          this.setValue(value, elmt.group) ;
        else
          elmt.selected = false ;
      }) ;
    }
  }


  /**
   * Retourne la valeur du field
   * @param {Array} [list] Liste sur laquelle on applique la valeur (utile
   *        uniquement en interne)
   * @returns {*}
   */
  getValue (list) {

    let value = this._multiple ? [] : undefined ;
    list = list ? list : this._list ;

    if (Array.isArray(list)) {
      list.forEach(elmt => {
        if (elmt.selected) {
          if (this._multiple)
            value.push(elmt.value) ;
          else
            value = elmt.value ;
        }
        else if (Array.isArray(elmt.group)) {
          let valueTmp = this.getValue(elmt.group) ;
          if (!this._multiple)
            value = value ? value : valueTmp ;
          else if (valueTmp.length)
            value = value.concat(valueTmp) ;
        }
      }) ;
    }

    return value ;
  }


  /**
   * Construit les attributs du champ select
   * @returns {String}
   * @private
   */
  _buildAttributes () {

    return (this.getClass() != '' ? ' class="' + this.getClass() + '"' : '')
      + ' id="' + this._id + '" name="' + this._name + '"'
      + (this._required ? ' required' : '')
      + (this._disabled ? ' disabled' : '')
      + (this._multiple ? ' multiple' : '')
      + (this._size ? ' size="' + this._size + '"' : '') ;
  }


  /**
   * Contruit la liste des éléments
   * @param {Array} list Liste des éléments
   * @returns {String}
   * @private
   */
  _buildList (list) {

    let builded = '' ;

    if (Array.isArray(list)) {

      list.forEach(elmt => {

        if (Array.isArray(elmt.group)) {
          builded += '<optgroup label="' + elmt.label + '"'
                  + (elmt.disabled ? ' disabled' : '') + '>\n    '
                  + this._buildList(elmt.group)
                  + '</optgroup>\n    ' ;
        }
        else {
          builded += '<option value="' + elmt.value + '"'
                  + (elmt.disabled ? ' disabled' : '')
                  + (elmt.selected ? ' selected' : '')
                  + '>' + elmt.label + '</option>\n    ' ;
        }
      }) ;
    }

    return builded ;
  }
}

module.exports = SelectField ;

