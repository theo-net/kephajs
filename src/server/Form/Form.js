'use strict' ;

/**
 * src/server/Form/Form.js
 */

const Block = require('./Block') ;

/**
 * Représente un formulaire
 * Techniquement, il s'agit d'un Block avec quelques propriétés et
 * méthodes spéciales.
 */
class Form extends Block {

  /**
   * Initialise un nouveau formulaire.
   * En lui donnant une entité, on permet l'autoremplissage des champs et
   * l'autovalidation
   * @param {Entity} [entity=null] Entité à transmettre
   * @param {Forms} forms Service $forms
   */
  constructor (entity = null, forms) {

    super(entity, forms) ;

    this._action = '#' ;
    this._method = 'get' ;
  }


  /**
   * Définit l'action et la méthode
   * @param {String} action="" Action du formulaire
   * @param {String} [method="get"] Méthode du formulaire
   * @returns {this}
   */
  setAction (action, method = 'get') {

    this._action = action ;
    this._method = method ;

    return this ;
  }


  /**
   * Retourne toujours lui-même
   * @returns {this}
   */
  getParent () {

    return this ;
  }


  /**
   * Construit le code HTML du formulaire et le retourne.
   * @returns {String}
   */
  build () {

    let form = '<form action="' + this._action + '" method="'
             + this._method + '"' + (this._id ? ' id="' + this._id + '"' : '')
             + '>\n' ;

    this._fields.forEach(field => {

      form += field.build() + '\n' ;
    }) ;

    return form + '</form>' ;
  }


  /**
   * Exécute le formulaire
   * @param {Object} vars Variables soumises `{id: value, ...}`
   * @returns {Null|Object} Null si erreur, object sinon
   */
  process (vars) {

    return this._process(vars) ;
  }
}

module.exports = Form ;

