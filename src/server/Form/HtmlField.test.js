'use strict' ;

/**
 * src/server/Form/HtmlField.test.js
 */

const HtmlField = require('./HtmlField') ;

const expect = require('chai').expect ;

describe('server/Form/HtmlField', function () {

  it('Lors de la construction retourne le code HTML', function () {

    let input = new HtmlField({name: 'foo', html: 'bar'}) ;
    expect(input.build()).to.be.equal('bar') ;
  }) ;

  it('Par défaut, renvoit rien', function () {

    let input = new HtmlField({name: 'foo'}) ;
    expect(input.build()).to.be.equal('') ;
  }) ;
}) ;

