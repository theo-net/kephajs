'use strict' ;

/**
 * src/server/Form/ButtonField.js
 */

const InputField = require('./InputField') ;

class ButtonField extends InputField {

  init () {

    super.init() ;
    this._type = 'button' ;
  }


  /**
   * Par rapport aux éléments parents, ne prend pas en compte les classes
   * `groupSuccess` et `groupError`
   * @param {Object} cssObj Les classes CSS
   */
  addClass (cssObj) {

    super.addClass(cssObj) ;

    this._class.groupSuccess = [] ;
    this._class.groupError = [] ;
  }
}

module.exports = ButtonField ;

