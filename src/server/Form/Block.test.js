'use strict' ;

/**
 * src/server/Form/Block.test.js
 */

const expect = require('chai').expect ;

const Fieldset    = require('./Fieldset'),
      Block       = require('./Block'),
      Forms       = require('../Services/Forms'),
      StringField = require('./StringField'),
      Validate    = require('../../core/Validate/Validate'),
      ValidateS   = require('../../core/Services/Validate') ;

describe('server/Form/Block', function () {

  it('Peut assigner une entité lors de la construction', function () {

    let entity = {} ;
    let block = new Block(entity) ;
    expect(block.getEntity()).to.be.equal(entity) ;
  }) ;

  it('On peut associer le service $forms', function () {

    let forms = {} ;
    let block = new Block(null, forms) ;
    expect(block.$forms).to.be.equal(forms) ;
  }) ;

  it('setEntity() assigne une entité au block', function () {

    let entity = {} ;
    let block = new Block() ;
    expect(block.setEntity(entity)).to.be.equal(block) ;
    expect(block.getEntity()).to.be.equal(entity) ;
  }) ;

  it('setId() associe un id au block', function () {

    let block = new Block() ;
    expect(block.setId('foobar')).to.be.equal(block) ;
    expect(block.getId()).to.be.equal('foobar') ;
  }) ;

  it('un prefixe peut avoir été déf lors de l\'init', function () {

    let block = new Block({}, {}, 'foo') ;
    expect(block.setId('bar').getId()).to.be.equal('fooBar') ;
  }) ;

  it('on peut associer un parent au Block', function () {

    let block  = new Block(),
        blockP = new Block() ;
    expect(block.setParent(blockP)).to.be.equal(block) ;
    expect(block.getParent()).to.be.equal(blockP) ;
  }) ;

  it('on peut remettre à zéro les classes Css', function () {

    let block    = new Block() ;
    block.addClass({field: 'a'}) ;

    expect(block.resetClass()).to.be.equal(block) ;
    expect(block._class.field.length).to.be.equal(0) ;
  }) ;


  describe('add', function () {

    let block, blockB,
        entity = {
          foo: 'a',
          bar: 'b'
        } ;

    beforeEach(function () {

      block = new Block(entity, new Forms(new ValidateS())) ;
      blockB = new Block(entity, new Forms(new ValidateS()), 'foo') ;
    }) ;

    it('returns the block object', function () {

      expect(block.add('string', {name: 'test'})).to.be.equal(block) ;
    }) ;

    it('throw Error si pas d\'attribut name', function () {

      expect(block.add.bind(block, 'string')).to.throws(Error) ;
      expect(block.add.bind(block, 'string', {foo: 'bar'}))
        .to.throws(Error) ;
    }) ;

    it('enregistre le field', function () {

      block.add('string', {name: 'foo'}) ;
      expect(block._fields[0]).to.be.instanceOf(StringField) ;
      expect(block._fields[0]._name).to.be.equal('foo') ;
    }) ;

    it('transmet le service $validate pour créer le validator', function () {

      block.add('string', {name: 'foo'}, 'mail') ;
      expect(block._fields[0]._validator).to.be.instanceOf(Validate) ;
    }) ;

    it('les champs sont remplis avec la valeur de l\'entité', function () {

      block.add('string', {name: 'foo'})
           .add('string', {name: 'bar'}) ;

      expect(block._fields[0].getValue()).to.be.equal(entity.foo) ;
      expect(block._fields[1].getValue()).to.be.equal(entity.bar) ;
    }) ;

    it('champs bindés remplis avec l\'entité', function () {

      block.add('string', {name: 'fooB', bind: 'foo'})
           .add('string', {name: 'barB', bind: 'bar'}) ;

      expect(block._fields[0].getValue()).to.be.equal(entity.foo) ;
      expect(block._fields[1].getValue()).to.be.equal(entity.bar) ;
    }) ;

    it('on peut forcer l\'ajout de classes CSS', function () {

      expect(block.addClass({
        field: 'i',
        label: 'j',
        group: 'k',
        groupSuccess: 'l',
        groupError: 'm',
        msg: 'n',
        msgHelp: 'o',
        msgError: 'p',
        fieldset: 'q'
      })).to.be.equal(block) ;

      block.add('string', {name: 'foo', required: true, class: {
        field: 'a',
        label: 'b',
        group: 'c',
        groupSuccess: 'd',
        groupError: 'e',
        msg: 'f',
        msgHelp: 'g',
        msgError: 'h'
      }}) ;

      expect(block._fields[0].getClass()).to.be.equal('a i') ;
      expect(block._fields[0].getLabelClass()).to.be.equal('b j') ;
      expect(block._fields[0].getGroupClass()).to.be.equal('c k') ;
      expect(block._fields[0].getMsgClass()).to.be.equal('f n g o') ;
      expect(block._fields[0].getMsgClass('help')).to.be.equal('f n g o') ;
      expect(block._fields[0].getMsgClass('error')).to.be.equal('f n h p') ;

      block._fields[0].isValid() ;
      expect(block._fields[0].getGroupClass()).to.be.equal('c k d l') ;
      block._fields[0].setValue('') ;
      block._fields[0].isValid() ;
      expect(block._fields[0].getGroupClass()).to.be.equal('c k e m') ;
    }) ;

    it('si le block a un id, prefixe le nom des champs', function () {

      expect(block.setId('salut')).to.be.equal(block) ;
      block.add('string', {name: 'foo'})
           .add('string', {name: 'bar'})
           .add('string', {name: 'bind', bind: 'bar'}) ;
      let field1 = block._fields[0],
          field2 = block._fields[1],
          field3 = block._fields[2] ;

      expect(field1._name).to.be.equal('salutFoo') ;
      expect(field1._id).to.be.equal('salutFoo') ;
      expect(field2._name).to.be.equal('salutBar') ;
      expect(field2._id).to.be.equal('salutBar') ;
      expect(field1.getValue()).to.be.equal(entity.foo) ;
      expect(field2.getValue()).to.be.equal(entity.bar) ;
      expect(field3.getValue()).to.be.equal(entity.bar) ;
    }) ;

    it('si l\'id d\'un parent transmis, utilisé pour préfixer', function () {

      blockB.add('string', {name: 'foo'})
            .add('string', {name: 'bar'})
            .add('string', {name: 'bind', bind: 'bar'}) ;
      let field1 = blockB._fields[0],
          field2 = blockB._fields[1],
          field3 = blockB._fields[2] ;

      expect(field1._name).to.be.equal('fooFoo') ;
      expect(field1._id).to.be.equal('fooFoo') ;
      expect(field2._name).to.be.equal('fooBar') ;
      expect(field2._id).to.be.equal('fooBar') ;
      expect(field1.getValue()).to.be.equal(entity.foo) ;
      expect(field2.getValue()).to.be.equal(entity.bar) ;
      expect(field3.getValue()).to.be.equal(entity.bar) ;
    }) ;

    it('combinaison des deux derniers tests', function () {

      blockB.setId('bar') ;
      blockB.add('string', {name: 'foo'})
            .add('string', {name: 'bar'})
            .add('string', {name: 'bind', bind: 'bar'}) ;
      let field1 = blockB._fields[0],
          field2 = blockB._fields[1],
          field3 = blockB._fields[2] ;

      expect(field1._name).to.be.equal('fooBarFoo') ;
      expect(field1._id).to.be.equal('fooBarFoo') ;
      expect(field2._name).to.be.equal('fooBarBar') ;
      expect(field2._id).to.be.equal('fooBarBar') ;
      expect(field1.getValue()).to.be.equal(entity.foo) ;
      expect(field2.getValue()).to.be.equal(entity.bar) ;
      expect(field3.getValue()).to.be.equal(entity.bar) ;
    }) ;
  }) ;


  describe('addBlock', function () {

    let block, blockB, entity, forms ;

    beforeEach(function () {

      entity = {} ;
      forms = new Forms(new ValidateS()) ;
      block = new Block(entity, forms) ;
      blockB = new Block() ;
    }) ;

    it('return new Block', function () {

      let block1 = block.addBlock(),
          block2 = block.addBlock() ;
      expect(block1).to.be.instanceOf(Block) ;
      expect(block1).to.be.not.equal(block2) ;
    }) ;

    it('transmet les classes CSS', function () {

      let classCss = {
        field: 'a',
      } ;
      block.addClass(classCss) ;

      expect(block.addBlock()._class.field[0]).to.be.equal('a') ;
    }) ;

    it('on peut définir directement les classes CSS', function () {

      expect(block.addBlock().getClass()).to.be.equal('') ;
      expect(block.addBlock('foobar').getClass())
        .to.be.equal('foobar') ;
    }) ;

    it('on peut récupérer le parent', function () {

      expect(block.addBlock().getParent()).to.be.equal(block) ;
    }) ;

    it('transmet l\'entité', function () {

      expect(block.addBlock().getEntity()).to.be.equal(entity) ;
      expect(blockB.addBlock().getEntity()).to.be.equal(null) ;
    }) ;

    it('transmet le service $form', function () {

      expect(block.addBlock().$forms).to.be.equal(forms) ;
      expect(blockB.addBlock().$forms).to.be.equal(null) ;
    }) ;
  }) ;


  describe('addFieldset', function () {

    let block, blockB, entity, forms ;

    beforeEach(function () {

      entity = {} ;
      forms = new Forms(new ValidateS()) ;
      block = new Block(entity, forms) ;
      blockB = new Block() ;
    }) ;

    it('return new Fieldset', function () {

      let fieldset1 = block.addFieldset(),
          fieldset2 = block.addFieldset() ;
      expect(fieldset1).to.be.instanceOf(Fieldset) ;
      expect(fieldset1).to.be.not.equal(fieldset2) ;
    }) ;

    it('on peut définir directement la légende', function () {

      expect(block.addFieldset().getLegend()).to.be.equal(null) ;
      expect(block.addFieldset('foobar').getLegend())
        .to.be.equal('foobar') ;
    }) ;

    it('on peut récupérer le parent', function () {

      expect(block.addFieldset().getParent()).to.be.equal(block) ;
    }) ;

    it('transmet l\'entité', function () {

      expect(block.addFieldset().getEntity()).to.be.equal(entity) ;
      expect(blockB.addFieldset().getEntity()).to.be.equal(null) ;
    }) ;

    it('transmet le service $form', function () {

      expect(block.addFieldset().$forms).to.be.equal(forms) ;
      expect(blockB.addFieldset().$forms).to.be.equal(null) ;
    }) ;

    it('prefixe l\'id du fieldset avec celui du block', function () {

      let fieldset1 = block.addFieldset() ;
      expect(fieldset1.getId()).to.be.equal(null) ;
      fieldset1.setId('bar') ;
      expect(fieldset1.getId()).to.be.equal('bar') ;

      block.setId('foo') ;
      let fieldset2 = block.addFieldset() ;
      expect(fieldset2.getId()).to.be.equal(null) ;
      fieldset2.setId('bar') ;
      expect(fieldset2.getId()).to.be.equal('fooBar') ;
    }) ;

    it('on peut forcer l\'ajout de classes CSS', function () {

      expect(block.addClass({
        field: 'i',
        label: 'j',
        group: 'k',
        groupSuccess: 'l',
        groupError: 'm',
        msg: 'n',
        msgHelp: 'o',
        msgError: 'p'
      })).to.be.equal(block) ;

      block.addFieldset('legend', {
        field: 'a',
        label: 'b',
        group: 'c',
        groupSuccess: 'd',
        groupError: 'e',
        msg: 'f',
        msgHelp: 'g',
        msgError: 'h'
      }).add('string', {name: 'foobar', required: true}) ;

      expect(block._fields[0]._fields[0].getClass()).to.be.equal('a i') ;
      expect(block._fields[0]._fields[0].getLabelClass()).to.be.equal('b j') ;
      expect(block._fields[0]._fields[0].getGroupClass()).to.be.equal('c k') ;
      expect(block._fields[0]._fields[0].getMsgClass()).to.be.equal('f n g o') ;
      expect(block._fields[0]._fields[0].getMsgClass('help'))
        .to.be.equal('f n g o') ;
      expect(block._fields[0]._fields[0].getMsgClass('error'))
        .to.be.equal('f n h p') ;

      block._fields[0]._fields[0].isValid() ;
      expect(block._fields[0]._fields[0].getGroupClass())
        .to.be.equal('c k e m') ;
      block._fields[0]._fields[0].setValue('ok') ;
      block._fields[0]._fields[0].isValid() ;
      expect(block._fields[0]._fields[0].getGroupClass())
        .to.be.equal('c k d l') ;
    }) ;
  }) ;


  describe('getField', function () {

    let block, forms ;

    beforeEach(function () {

      forms = new Forms(new ValidateS()) ;
      block = new Block({}, forms) ;
    }) ;

    it('Peut retourner un field enfant', function () {

      block.add('string', {name: 'foobar'})
           .add('string', {name: 'foo'})
           .add('string', {name: 'bar'}) ;
      expect(block.getField('foo')).to.be.equal(block._fields[1]) ;
    }) ;

    it('S\'arrête au premier trouvé', function () {

      block.add('string', {name: 'foo'})
           .add('string', {name: 'foo'})
           .add('string', {name: 'bar'}) ;
      expect(block.getField('foo')).to.be.equal(block._fields[0]) ;
    }) ;

    it('cherche dans les sous-blocks', function () {

      block.add('string', {name: 'foobar'})
           .addBlock()
             .add('string', {name: 'foo'})
             .add('string', {name: 'bar'})
             .getParent()
           .addBlock().addFieldset()
             .add('string', {name: 'salut'}) ;

      expect(block.getField('salut'))
        .to.be.equal(block._fields[2]._fields[0]._fields[0]) ;
    }) ;
  }) ;


  describe('build', function () {

    let block ;

    beforeEach(function () {

      block = new Block(null, new Forms(new ValidateS())) ;
    }) ;

    it('retourne le code HTML du block', function () {

      expect(block.build())
        .to.be.equal('<div>\n</div>') ;

      block.setId('salut') ;
      expect(block.build())
        .to.be.equal('<div id="salut">\n</div>') ;

      block.addClass('a') ;
      expect(block.build())
        .to.be.equal('<div id="salut" class="a">\n</div>') ;
      block.addClass(['b', 'c']) ;
      expect(block.build())
        .to.be.equal('<div id="salut" class="a b c">\n</div>') ;
    }) ;

    it('construit tous les sous-éléments', function () {

      block
        .add('string', {name: 'foo', label: 'Un champ'})
        .add('string', {name: 'bar', label: 'Un autre champ'}) ;
      expect(block.build()).to.be.equal(
        '<div>\n'
        + '  <div>\n'
        + '    <label for="foo">Un champ</label>\n'
        + '    <input id="foo" name="foo" type="text"><i></i>\n'
        + '      </div>\n'
        + '  <div>\n'
        + '    <label for="bar">Un autre champ</label>\n'
        + '    <input id="bar" name="bar" type="text"><i></i>\n'
        + '      </div>\n'
        + '</div>'
      ) ;
    }) ;
  }) ;


  describe('_process', function () {

    let block, blockB, blockC, entity ;

    beforeEach(function () {

      entity = {
        nom: 'a',
        mail: 'b',
        description: ''
      } ;

      block = new Block(null, new Forms(new ValidateS())) ;
      block.setParent(block) ; // hack pour les tests
      block.addBlock()
           .add('string', {name: 'nom', required: true}, {string: {min: 3}})
           .add('string', {name: 'mail', required: true}, 'mail')
           .getParent()
           .add('string', {name: 'description'}) ;

      blockB = new Block(entity, new Forms(new ValidateS())) ;
      blockB.setParent(blockB) ; // hack pour les tests
      blockB.addBlock()
            .add('string', {name: 'nom', required: true}, {string: {min: 3}})
            .add('string', {name: 'mail', required: true}, 'mail')
            .getParent()
            .add('string', {name: 'description'}) ;

      blockC = new Block(entity, new Forms(new ValidateS())) ;
      blockC.setParent(blockC) ; // hack pour les tests
      blockC.addBlock()
            .add('string', {name: 'nom', required: true}, {string: {min: 3}})
            .add('string', {name: 'mail', required: true}, 'mail')
            .getParent()
            .add('string', {name: 'desc', bind: 'description'}) ;
    }) ;

    it('rempli tous les champs', function () {

      let req = {
        nom: 'Jésus',
        mail: 'jesus@cieux.god',
        description: 'Fils de Dieu'
      } ;
      block._process(req) ;

      expect(block._fields[0]._fields[0].getValue()).to.be.equal('Jésus') ;
      expect(block._fields[0]._fields[1].getValue())
        .to.be.equal('jesus@cieux.god') ;
      expect(block._fields[1].getValue()).to.be.equal('Fils de Dieu') ;
    }) ;

    it('retourne la requête si tout est valide (pas d\'entity)', function () {

      let req = {
        nom: 'Jésus',
        mail: 'jesus@cieux.god',
        description: 'Fils de Dieu'
      } ;
      expect(block._process(req)).to.be.equal(req) ;
    }) ;

    it('Si une entité est présente, la retourne mise-à-jour', function () {

      let req = {
        nom: 'Jésus',
        mail: 'jesus@cieux.god',
        description: 'Fils de Dieu'
      } ;
      let ret = blockB._process(req) ;

      expect(ret).to.be.equal(entity) ;
      expect(ret).to.be.deep.equal(req) ;
    }) ;

    it('Ne rajoute pas de propriétés', function () {

      blockB.add('string', {name: 'test'}) ;
      let req = {
        nom: 'Jésus',
        mail: 'jesus@cieux.god',
        description: 'Fils de Dieu',
        test: 'ok'
      } ;
      let ret = blockB._process(req) ;

      expect(ret).to.be.equal(entity) ;
      expect(entity).to.be.deep.equal({
        nom: 'Jésus',
        mail: 'jesus@cieux.god',
        description: 'Fils de Dieu'
      }) ;
    }) ;

    it('Si une entité est présente, mais req invalide, non maj', function () {

      let req = {
        nom: 'Jésus',
        mail: 'jesuscieux.god',
        description: 'Fils de Dieu'
      } ;
      let ret = blockB._process(req) ;

      expect(ret).to.be.equal(null) ;
      expect(blockB.getEntity()).to.be.deep.equal({
        nom: 'a', mail: 'b', description: ''
      }) ;
    }) ;

    it('entité remplis avec champs bindés', function () {

      let req = {
        nom: 'Jésus',
        mail: 'jesus@cieux.god',
        desc: 'Fils de Dieu'
      } ;
      let ret = blockC._process(req) ;

      expect(ret).to.be.equal(entity) ;
      expect(ret).to.be.deep.equal({
        nom: 'Jésus',
        mail: 'jesus@cieux.god',
        description: 'Fils de Dieu'
      }) ;
    }) ;

    it('retourne null sinon', function () {

      let req1 = {
        nom: 'Jésus',
        mail: 'jesus@cieux.god',
      } ;
      let req2 = {
        nom: 'Jésus',
        mail: 'jesuscieux.god',
      } ;
      let req3 = {
        nom: 'Jésus',
      } ;
      expect(block._process(req1)).to.be.equal(req1) ;
      expect(block._process(req2)).to.be.equal(null) ;
      expect(block._process(req3)).to.be.equal(null) ;
    }) ;

    it('tous les champs sont bien parcourus', function () {

      block._process({
        nom: 'Jé',
        mail: 'jesus@cieux.god',
      }) ;

      expect(block._fields[0]._fields[0].isSubmit()).to.be.true ;
      expect(block._fields[0]._fields[1].isSubmit()).to.be.true ;
      expect(block._fields[1].isSubmit()).to.be.true ;
    }) ;
  }) ;
}) ;

