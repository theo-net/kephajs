'use strict' ;

/**
 * src/server/Form/RadioField.js
 */

const InputField = require('./InputField') ;

/**
 * Définie un group `radio` : on ne peut choisir qu'un des éléments.
 */
class RadioField extends InputField {

  /**
   * Vous devez définir un attribut `group` qui doit être un `Array` d'objets
   * décrivant les différentes valeurs possibles : `{label, value}`.
   */
  init () {

    this._type = 'radio' ;

    let validator = [] ;
    this._group = [] ;

    if (Array.isArray(this._attr.group)) {

      this._attr.group.forEach(elmt => {

        validator.push(elmt.value) ;
        this._group.push({
          label: elmt.label,
          value: elmt.value,
          checked: this._value == elmt.value ? true : false
        }) ;
      }) ;
    }

    this._validator.add(validator) ;
  }

  setValue (value) {

    this._value = value ;

    // Le constructor ne peut définir la valeur, car field non initialisé
    if (Array.isArray(this._group)) {
      this._group.forEach(elmt => {
        elmt.checked = value == elmt.value ? true : false ;
      }) ;
    }
  }

  build () {

    let widget = '  <div'
               + (this.getGroupClass() != '' ?
                  ' class="' + this.getGroupClass() + '"' : '')
               + '>\n' + this._buildLabel() ;

    this._group.forEach((elmt, index) => {
      widget += '<div>\n      <input'
             + (this.getClass() != '' ? ' class="' + this.getClass() + '"' : '')
             + ' id="' + this._id + index + '" name="' + this._name
             + '" type="' + this._type + '" value="' + elmt.value + '"'
             + (this._disabled ? ' disabled' : '')
             + (elmt.checked ? ' checked' : '')
             + '>\n      <label'
             + (this.getLabelClass() != '' ?
                ' class="' + this.getLabelClass() + '"' : '')
             + ' for="' + this._id + index + '">'
             + elmt.label + '</label>\n    </div>\n    ' ;
    }) ;

    widget += this._buildMsg() + '</div>' ;

    return widget ;
  }
}

module.exports = RadioField ;

