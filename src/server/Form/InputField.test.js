'use strict' ;

/**
 * src/server/Form/InputField.test.js
 */

const InputField      = require('./InputField'),
      StringField     = require('./StringField'),
      EmailField      = require('./EmailField'),
      PasswordField   = require('./PasswordField'),
      SearchField     = require('./SearchField'),
      UrlField        = require('./UrlField'),
      TelField        = require('./TelField'),
      NumberField     = require('./NumberField'),
      RangeField      = require('./RangeField'),
      ColorField      = require('./ColorField'),
      DateField       = require('./DateField'),
      TimeField       = require('./TimeField'),
      HiddenField     = require('./HiddenField'),
      SubmitField     = require('./SubmitField'),
      ButtonField     = require('./ButtonField'),
      ResetField      = require('./ResetField'),
      RadioField      = require('./RadioField'),
      CheckboxField   = require('./CheckboxField'),
      ValidateS       = require('../../core/Services/Validate'),
      StringValidator = require('../../core/Validate/String'),
      RegExpValidator = require('../../core/Validate/RegExp'),
      MailValidator   = require('../../core/Validate/Mail'),
      FloatValidator  = require('../../core/Validate/Float'),
      ListValidator   = require('../../core/Validate/List') ;

const expect = require('chai').expect ;

describe('server/Form/InputField', function () {

  it('par défaut type text', function () {

    let input = new InputField({name: 'foo'}) ;
    expect(input._type).to.be.equal('text') ;
  }) ;

  it('peut avoir un placeholder', function () {

    let input = new InputField({name: 'foo', placeholder: 'bar'}) ;
    expect(input._placeholder).to.be.equal('bar') ;
  }) ;

  it('peut avoir spellcheck', function () {

    let input1 = new InputField({name: 'foo', spellcheck: true}),
        input2 = new InputField({name: 'foo', spellcheck: false}),
        input3 = new InputField({name: 'foo', spellcheck: 'default'}) ;

    expect(input1._buildAttributes())
      .to.be.equal(' id="foo" name="foo" type="text" spellcheck="true"') ;
    expect(input2._buildAttributes())
      .to.be.equal(' id="foo" name="foo" type="text" spellcheck="false"') ;
    expect(input3._buildAttributes())
      .to.be.equal(' id="foo" name="foo" type="text" spellcheck="default"') ;
  }) ;

  it('peut avoir list', function () {

    let input = new InputField(
      {name: 'foo', list: ['a', 'b']},
      null, new ValidateS()
    ) ;
    let inputB = new InputField(
      {name: 'foo', list: ['a', 'b']},
      'string', new ValidateS()
    ) ;
    expect(input._list).to.be.deep.equal(['a', 'b']) ;

    expect(input._buildAttributes())
      .to.be.equal(' id="foo" name="foo" type="text" list="fooList"') ;
    expect(input._buildList()).to.be.equal(
      '    <datalist id="fooList">\n    '
      + '  <option value="a">\n    '
      + '  <option value="b">\n    '
      + '</datalist>'
    ) ;
    input.setValue('c') ;
    expect(input.isValid()).to.be.false ;
    inputB.setValue('c') ;
    expect(inputB.isValid()).to.be.false ;
  }) ;

  it('construction par défaut', function () {

    let input = new InputField({
      name: 'foo', id: 'bar',
      label: 'Un truc',
      placeholder: 'test',
      value: 'cool',
      help: 'Une aide',
      required: true,
      disabled: true,
      size: 20
    }) ;
    input.addClass({field: 'a', label: 'b', group: 'c', msg: 'd'}) ;

    expect(input.build()).to.be.equal(
      '  <div class="c">\n    '
      + '<label class="b" for="bar">Un truc</label>\n    '
      + '<input class="a" id="bar" name="foo" type="text" value="cool"'
      + ' placeholder="test" required disabled size="20"><i></i>\n    '
      + '<p class="d">Une aide</p>\n'
      + '  </div>'
    ) ;
  }) ;


  describe('String', function () {

    it('input de type text', function () {

      let input = new StringField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('text') ;
    }) ;

    it('validator String', function () {

      let input = new StringField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._validator._validators[1])
        .to.be.instanceOf(StringValidator) ;
    }) ;

    it('attribut min -> minlength', function () {

      let input = new StringField(
        {name: 'foo', min: 1}, null, new ValidateS()
      ) ;

      expect(input._buildAttributes())
        .to.be.equal(' id="foo" name="foo" type="text" minlength="1"') ;
      expect(input._validator._validators[1]._validatorArguments.min)
        .to.be.equal(1) ;
    }) ;

    it('attribut max -> maxlength', function () {

      let input = new StringField(
        {name: 'foo', max: 2}, null, new ValidateS()
      ) ;

      expect(input._buildAttributes())
        .to.be.equal(' id="foo" name="foo" type="text" maxlength="2"') ;
      expect(input._validator._validators[1]._validatorArguments.max)
        .to.be.equal(2) ;
    }) ;

    it('attribut pattern', function () {

      let input1 = new StringField(
        {name: 'foo', pattern: /a.*/}, null, new ValidateS()
      ) ;
      let input2 = new StringField(
        {name: 'foo', pattern: 'a.*'}, null, new ValidateS()
      ) ;

      expect(input1._buildAttributes())
        .to.be.equal(' id="foo" name="foo" type="text" pattern="a.*"') ;
      expect(input2._buildAttributes())
        .to.be.equal(' id="foo" name="foo" type="text" pattern="a.*"') ;

      expect(input1._validator._validators[2])
        .to.be.instanceOf(RegExpValidator) ;
      expect(input2._validator._validators[2])
        .to.be.instanceOf(RegExpValidator) ;

      expect(input1._validator._validators[2]._validatorArguments.regExp)
        .to.be.deep.equal(/a.*/) ;
      expect(input2._validator._validators[2]._validatorArguments.regExp)
        .to.be.deep.equal(/a.*/) ;
    }) ;

    it('trim sur la valeur', function () {

      let input = new StringField({name: 'foo'}, null, new ValidateS()) ;
      input.setValue(' foo') ;
      expect(input._value).to.be.equal('foo') ;
      input.setValue('bar ') ;
      expect(input._value).to.be.equal('bar') ;
      input.setValue(' foobar ') ;
      expect(input._value).to.be.equal('foobar') ;
    }) ;

    it('le trim fonctionne si undefined', function () {

      let input = new StringField({name: 'foo'}, null, new ValidateS()) ;
      input.setValue(undefined) ;
      expect(input._value).to.be.equal(undefined) ;
    }) ;
  }) ;


  describe('Email', function () {

    it('est un dérivé de String', function () {

      let input = new EmailField({name: 'foo'}, null, new ValidateS()) ;
      expect(input).to.be.instanceOf(StringField) ;
    }) ;

    it('input de type email', function () {

      let input = new EmailField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('email') ;
    }) ;

    it('placeholder définit par défaut', function () {

      let input1 = new EmailField({name: 'foo'}, null, new ValidateS()),
          input2 = new EmailField({name: 'foo', placeholder: 'bar'},
            null, new ValidateS()) ;

      expect(input1._placeholder).to.be.equal('exemple@domain.com') ;
      expect(input2._placeholder).to.be.equal('bar') ;
    }) ;

    it('validator Email', function () {

      let input = new EmailField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._validator._validators[2]).to.be.instanceOf(MailValidator) ;
    }) ;
  }) ;


  describe('Password', function () {

    it('est un dérivé de String', function () {

      let input = new PasswordField({name: 'foo'}, null, new ValidateS()) ;
      expect(input).to.be.instanceOf(StringField) ;
    }) ;

    it('input de type password', function () {

      let input = new PasswordField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('password') ;
    }) ;
  }) ;


  describe('Search', function () {

    it('est un dérivé de String', function () {

      let input = new SearchField({name: 'foo'}, null, new ValidateS()) ;
      expect(input).to.be.instanceOf(StringField) ;
    }) ;

    it('input de type search', function () {

      let input = new SearchField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('search') ;
    }) ;
  }) ;


  describe('Url', function () {

    it('est un dérivé de String', function () {

      let input = new UrlField({name: 'foo'}, null, new ValidateS()) ;
      expect(input).to.be.instanceOf(StringField) ;
    }) ;

    it('input de type url', function () {

      let input = new UrlField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('url') ;
    }) ;
  }) ;


  describe('Tel', function () {

    it('est un dérivé de String', function () {

      let input = new TelField({name: 'foo'}, null, new ValidateS()) ;
      expect(input).to.be.instanceOf(StringField) ;
    }) ;

    it('input de type tel', function () {

      let input = new TelField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('tel') ;
    }) ;
  }) ;


  describe('Number', function () {

    it('est un dérivé d\'Input', function () {

      let input = new NumberField({name: 'foo'}, null, new ValidateS()) ;
      expect(input).to.be.instanceOf(InputField) ;
    }) ;

    it('input de type number', function () {

      let input = new NumberField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('number') ;
    }) ;

    it('transforme la valeur en un nombre', function () {

      let input = new NumberField({name: 'foo'}, null, new ValidateS()) ;

      input.setValue(2) ;
      expect(input._value).to.be.equal(2) ;
      input.setValue('2') ;
      expect(input._value).to.be.equal(2) ;
      input.setValue(1.2) ;
      expect(input._value).to.be.equal(1.2) ;
      input.setValue('1.2') ;
      expect(input._value).to.be.equal(1.2) ;
    }) ;

    it('Vérifie qu\'il s\'agit bien d`un nombre', function () {

      let input = new NumberField({name: 'foo'}, null, new ValidateS()) ;

      input.setValue('2') ;
      expect(input.isValid()).to.be.true ;
      input.setValue('a') ;
      expect(input.isValid()).to.be.false ;
      expect(input._validator._validators[1])
        .to.be.instanceOf(FloatValidator) ;
    }) ;

    it('attribut min et validator', function () {

      let input = new NumberField(
        {name: 'foo', min: 1}, null, new ValidateS()
      ) ;

      expect(input._buildAttributes())
        .to.be.equal(' id="foo" name="foo" type="number" min="1"') ;
      expect(input._validator._validators[1]._validatorArguments.min)
        .to.be.equal(1) ;
    }) ;

    it('attribut max et validator', function () {

      let input = new NumberField(
        {name: 'foo', max: 2}, null, new ValidateS()
      ) ;

      expect(input._buildAttributes())
        .to.be.equal(' id="foo" name="foo" type="number" max="2"') ;
      expect(input._validator._validators[1]._validatorArguments.max)
        .to.be.equal(2) ;
    }) ;

    it('attribut step et validator', function () {

      let input = new NumberField(
        {name: 'foo', step: 2}, null, new ValidateS()
      ) ;

      expect(input._buildAttributes())
        .to.be.equal(' id="foo" name="foo" type="number" step="2"') ;

      input.setValue(-2) ;
      expect(input.isValid()).to.be.true ;
      input.setValue(0) ;
      expect(input.isValid()).to.be.true ;
      input.setValue(4) ;
      expect(input.isValid()).to.be.true ;
      input.setValue(5) ;
      expect(input.isValid()).to.be.false ;
      input.setValue(2.5) ;
      expect(input.isValid()).to.be.false ;
    }) ;
  }) ;


  describe('Range', function () {

    it('est un dérivé de Number', function () {

      let input = new RangeField({name: 'foo'}, null, new ValidateS()) ;
      expect(input).to.be.instanceOf(NumberField) ;
    }) ;

    it('input de type range', function () {

      let input = new RangeField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('range') ;
    }) ;
  }) ;


  describe('Color', function () {

    it('input de type color', function () {

      let input = new ColorField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('color') ;
    }) ;

    it('possède un validateur : color format #xxxxxx', function () {

      let input = new ColorField({name: 'foo'}, null, new ValidateS()) ;

      input.setValue('foobar') ;
      expect(input.isValid()).to.be.false ;

      input.setValue('#abc123') ;
      expect(input.isValid()).to.be.true ;

      expect(input._validator._validators[1])
        .to.be.instanceOf(RegExpValidator) ;
    }) ;
  }) ;


  describe('Date', function () {

    it('input de type date', function () {

      let input = new DateField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('date') ;
    }) ;

    it('possède un validateur : format aaaa-mm-jj', function () {

      let input = new DateField({name: 'foo'}, null, new ValidateS()) ;

      input.setValue('foobar') ;
      expect(input.isValid()).to.be.false ;

      input.setValue('2018-04-08') ;
      expect(input.isValid()).to.be.true ;

      expect(input._validator._validators[1])
        .to.be.instanceOf(RegExpValidator) ;
    }) ;
  }) ;


  describe('Time', function () {

    it('input de type time', function () {

      let input = new TimeField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('time') ;
    }) ;

    it('possède un validateur : format hh:mm', function () {

      let input = new TimeField({name: 'foo'}, null, new ValidateS()) ;

      input.setValue('foobar') ;
      expect(input.isValid()).to.be.false ;

      input.setValue('08:30') ;
      expect(input.isValid()).to.be.true ;

      expect(input._validator._validators[1])
        .to.be.instanceOf(RegExpValidator) ;
    }) ;
  }) ;


  describe('Hidden', function () {

    it('input de type hidden', function () {

      let input = new HiddenField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('hidden') ;
    }) ;

    it('le champ est toujours requis', function () {

      let input1 = new HiddenField({name: 'foo'}, null, new ValidateS()),
          input2 = new HiddenField({name: 'foo', required: true},
                                   null, new ValidateS()),
          input3 = new HiddenField({name: 'foo', required: false},
                                   null, new ValidateS()) ;

      expect(input1._required).to.be.true ;
      expect(input2._required).to.be.true ;
      expect(input3._required).to.be.true ;
    }) ;

    it('le rendu est simple, même si CSS défini', function () {

      let input = new HiddenField(
        {name: 'foo', class: {field: 'css'}, value: 'a'},
        null, new ValidateS()
      ) ;

      expect(input.build())
        .to.be.equal('<input id="foo" name="foo" type="hidden" value="a">\n') ;
    }) ;
  }) ;


  describe('Submit', function () {

    it('input de type submit', function () {

      let input = new SubmitField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('submit') ;
    }) ;

    it('attribut formmethod', function () {

      let input = new SubmitField(
        {name: 'foo', formmethod: 'get'}, null, new ValidateS()
      ) ;

      expect(input._formmethod).to.be.equal('get') ;
      expect(input._buildAttributes()).to.be.equal(
        ' id="foo" name="foo" type="submit" formmethod="get"'
      ) ;
    }) ;

    it('attribut formaction', function () {

      let input = new SubmitField(
        {name: 'foo', formaction: 'truc.html'}, null, new ValidateS()
      ) ;

      expect(input._formaction).to.be.equal('truc.html') ;
      expect(input._buildAttributes()).to.be.equal(
        ' id="foo" name="foo" type="submit" formaction="truc.html"'
      ) ;
    }) ;

    it('pas de classes de feedback', function () {

      let input = new SubmitField(
        {name: 'foo', class: {groupSuccess: 'a', groupError: 'b'}},
        null, new ValidateS()
      ) ;

      expect(input._class.groupSuccess).to.be.deep.equal([]) ;
      expect(input._class.groupError).to.be.deep.equal([]) ;
    }) ;
  }) ;


  describe('Button', function () {

    it('input de type button', function () {

      let input = new ButtonField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('button') ;
    }) ;

    it('pas de classes de feedback', function () {

      let input = new ButtonField(
        {name: 'foo', class: {groupSuccess: 'a', groupError: 'b'}},
        null, new ValidateS()
      ) ;

      expect(input._class.groupSuccess).to.be.deep.equal([]) ;
      expect(input._class.groupError).to.be.deep.equal([]) ;
    }) ;
  }) ;


  describe('Reset', function () {

    it('input de type reset', function () {

      let input = new ResetField({name: 'foo'}, null, new ValidateS()) ;
      expect(input._type).to.be.equal('reset') ;
    }) ;

    it('pas de classes de feedback', function () {

      let input = new ResetField(
        {name: 'foo', class: {groupSuccess: 'a', groupError: 'b'}},
        null, new ValidateS()
      ) ;

      expect(input._class.groupSuccess).to.be.deep.equal([]) ;
      expect(input._class.groupError).to.be.deep.equal([]) ;
    }) ;
  }) ;


  describe('Radio', function () {

    it('Listes des valeurs et validator', function () {

      let input = new RadioField({
        name: 'foo',
        group: [
          {label: 'truc', value: 'a'},
          {label: 'muche', value: 'b'}
        ]
      }, null, new ValidateS()) ;

      expect(input._validator._validators[1])
        .to.be.instanceOf(ListValidator) ;
      expect(input._validator._validators[1]._validatorArguments.list)
        .to.be.deep.equal(['a', 'b']) ;

      input.setValue('c') ;
      expect(input.isValid()).to.be.false ;
      input.setValue('a') ;
      expect(input.isValid()).to.be.true ;
    }) ;

    it('valeur, indique celui qui est `checked`', function () {

      let input = new RadioField({
        name: 'foo',
        group: [
          {label: 'truc', value: 'a'},
          {label: 'muche', value: 'b'}
        ],
        value: 'a'
      }, null, new ValidateS()) ;

      expect(input._group[0].checked).to.be.true ;
      expect(input._group[1].checked).to.be.false ;

      input.setValue('b') ;
      expect(input._group[0].checked).to.be.false ;
      expect(input._group[1].checked).to.be.true ;
    }) ;

    it('build spécifique', function () {

      let input = new RadioField({
        name: 'foo',
        group: [
          {label: 'truc', value: 'a'},
          {label: 'muche', value: 'b'}
        ],
        value: 'a',
        label: 'Choisir quelque chose',
        help: 'c'
      }, null, new ValidateS()) ;

      expect(input.build()).to.be.equal('  <div>\n  '
        + '  <label for="foo">Choisir quelque chose</label>\n  '
        + '  <div>\n  '
        + '    <input id="foo0" name="foo" type="radio" value="a" checked>\n  '
        + '    <label for="foo0">truc</label>\n  '
        + '  </div>\n  '
        + '  <div>\n  '
        + '    <input id="foo1" name="foo" type="radio" value="b">\n  '
        + '    <label for="foo1">muche</label>\n  '
        + '  </div>\n  '
        + '  <p>c</p>\n  '
        + '</div>'
      ) ;
    }) ;
  }) ;


  describe('Checkbox', function () {

    it('Étend Radio, mais de type `checkbox`', function () {

      let input = new CheckboxField({
        name: 'foo',
        group: [
          {label: 'truc', value: 'a'},
          {label: 'muche', value: 'b'}
        ],
        label: 'Choisir quelque chose'
      }, null, new ValidateS()) ;

      expect(input).to.be.instanceOf(RadioField) ;
      expect(input._type).to.be.equal('checkbox') ;
    }) ;

    it('setValue, toujours array, même avec une seule valeur', function () {

      let input = new CheckboxField({
        name: 'foo',
        group: [
          {label: 'truc', value: 'a'},
          {label: 'muche', value: 'b'}
        ],
        value: 'a',
        label: 'Choisir quelque chose'
      }, null, new ValidateS()) ;

      expect(input._group[0].checked).to.be.true ;
      expect(input._group[1].checked).to.be.false ;
      expect(input.getValue()).to.be.deep.equal(['a']) ;

      input.setValue('b') ;
      expect(input._group[0].checked).to.be.false ;
      expect(input._group[1].checked).to.be.true ;
      expect(input.getValue()).to.be.deep.equal(['b']) ;

      input.setValue(['a', 'b']) ;
      expect(input._group[0].checked).to.be.true ;
      expect(input._group[1].checked).to.be.true ;
      expect(input.getValue()).to.be.deep.equal(['a', 'b']) ;
    }) ;

    it('tests plusieurs valeurs', function () {

      let input = new CheckboxField({
        name: 'foo',
        group: [
          {label: 'truc', value: 'a'},
          {label: 'muche', value: 'b'}
        ],
        required: true,
        label: 'Choisir quelque chose'
      }, null, new ValidateS()) ;

      expect(input.isValid()).to.be.false ;

      input.setValue('b') ;
      expect(input.isValid()).to.be.true ;

      input.setValue(['a', 'b']) ;
      expect(input.isValid()).to.be.true ;

      input.setValue('c') ;
      expect(input.isValid()).to.be.false ;

      input.setValue(['a', 'c']) ;
      expect(input.isValid()).to.be.false ;
    }) ;
  }) ;
}) ;

