'use strict' ;

/**
 * src/server/Form/Field.test.js
 */

const expect = require('chai').expect ;

const Field             = require('./Field'),
      Validate          = require('../../core/Services/Validate'),
      ValidateValidator = require('../../core/Validate/Validate') ;

describe('server/Form/Field', function () {

  it('sauve les attributs passés en paramètres', function () {

    let attr = {name: 'a', truc: 'b'} ;
    let field = new Field(attr) ;

    expect(field._attr).to.be.equal(attr) ;
  }) ;

  it('enregistre l\'identifiant du field', function () {

    let field = new Field({name: 'bar'}) ;
    expect(field._name).to.be.equal('bar') ;
  }) ;

  it('l\'identifiant sera le `name` et l\'`id`', function () {

    let field = new Field({name: 'bar'}) ;
    expect(field._id).to.be.equal('bar') ;
  }) ;

  it('mais on peut forcer la valeur de l\'id', function () {

    let field = new Field({name: 'foo', id: 'bar'}) ;
    expect(field._id).to.be.equal('bar') ;
  }) ;

  it('on peut indiquer un bind', function () {

    let field1 = new Field({name: 'foo1', bind: 'bar'}),
        field2 = new Field({name: 'foo2'}) ;
    expect(field1._bind).to.be.equal('bar') ;
    expect(field2._bind).to.be.equal(false) ;
  }) ;

  it('on peut lui donner un label', function () {

    let field = new Field({name: 'bar', label: 'BAR'}) ;
    expect(field._label).to.be.equal('BAR') ;
  }) ;

  it('on peut lui associer une valeur', function () {

    let field = new Field({name: 'bar', value: 'foo'}) ;
    expect(field.getValue()).to.be.equal('foo') ;

    field.setValue('foobar') ;
    expect(field.getValue()).to.be.equal('foobar') ;
  }) ;

  it('par défaut, la valeur affecte l\'attribut value', function () {

    let field = new Field({name: 'bar'}) ;
    expect(field.getValue()).to.be.equal(undefined) ;
    field.setValue('foobar') ;
    expect(field._valueAttr).to.be.equal('foobar') ;
  }) ;

  it('mais on peut changer ce comportement', function () {

    let field = new Field({name: 'bar', value: 'foo'}) ;
    field.setValue = value => {
      if (value) field._checked = true ;
      else field._checked = false ;
      field._value = value ;
    } ;

    field.setValue(true) ;
    expect(field._valueAttr).to.be.equal('foo') ;
    expect(field._checked).to.be.equal(true) ;
    expect(field._value).to.be.equal(true) ;
  }) ;

  it('on peut lui associer un ou plusieurs validateur', function () {

    let field = new Field({name: 'bar'}, 'string', new Validate()) ;
    expect(field._validator).to.be.instanceOf(ValidateValidator) ;

    field = new Field(
      {name: 'bar'},
      [{string: {max: 10}}, {mail: {}}],
      new Validate()
    ) ;
    expect(field._validator).to.be.instanceOf(ValidateValidator) ;
  }) ;

  it('on peut appliquer des class CSS', function () {

    let field = new Field({
      name: 'foobar',
      class: {
        field: 'a',
        label: 'b',
        group: 'c',
        groupSuccess: 'd',
        groupError: 'e',
        msg: 'f',
        msgHelp: 'g',
        msgError: 'h'
      },
      required: true
    }) ;

    expect(field.getClass()).to.be.equal('a') ;
    expect(field.getLabelClass()).to.be.equal('b') ;
    expect(field.getGroupClass()).to.be.equal('c') ;
    expect(field.getMsgClass()).to.be.equal('f g') ;
    expect(field.getMsgClass('help')).to.be.equal('f g') ;
    expect(field.getMsgClass('error')).to.be.equal('f h') ;

    field.isValid() ;
    expect(field.getGroupClass()).to.be.equal('c e') ;
    field.setValue('ok') ;
    field.isValid() ;
    expect(field.getGroupClass()).to.be.equal('c d') ;
  }) ;

  it('on peut ajouter des class CSS', function () {

    let field = new Field({
      name: 'foobar',
      class: {
        field: 'a',
        label: 'b',
        group: 'c',
        groupSuccess: 'd',
        groupError: 'e',
        msg: 'f',
        msgHelp: 'g',
        msgError: 'h'
      },
      required: true
    }) ;
    field.addClass({
      field: 'i',
      label: 'j',
      group: 'k',
      groupSuccess: 'l',
      groupError: 'm',
      msg: 'n',
      msgHelp: 'o',
      msgError: 'p'
    }) ;


    expect(field.getClass()).to.be.equal('a i') ;
    expect(field.getLabelClass()).to.be.equal('b j') ;
    expect(field.getGroupClass()).to.be.equal('c k') ;
    expect(field.getMsgClass()).to.be.equal('f n g o') ;
    expect(field.getMsgClass('help')).to.be.equal('f n g o') ;
    expect(field.getMsgClass('error')).to.be.equal('f n h p') ;

    field.isValid() ;
    expect(field.getGroupClass()).to.be.equal('c k e m') ;
    field.setValue('ok') ;
    field.isValid() ;
    expect(field.getGroupClass()).to.be.equal('c k d l') ;
  }) ;

  it('Fonctionne aussi avec des tableaux', function () {

    let field = new Field({
      name: 'foobar',
      required: true
    }) ;
    field.addClass({
      field: ['a', 'i'],
      label: ['b', 'j'],
      group: ['c', 'k'],
      groupSuccess: ['d', 'l'],
      groupError: ['e', 'm'],
      msg: ['f', 'n'],
      msgHelp: ['g', 'o'],
      msgError: ['h', 'p']
    }) ;


    expect(field.getClass()).to.be.equal('a i') ;
    expect(field.getLabelClass()).to.be.equal('b j') ;
    expect(field.getGroupClass()).to.be.equal('c k') ;
    expect(field.getMsgClass()).to.be.equal('f n g o') ;
    expect(field.getMsgClass('help')).to.be.equal('f n g o') ;
    expect(field.getMsgClass('error')).to.be.equal('f n h p') ;

    field.isValid() ;
    expect(field.getGroupClass()).to.be.equal('c k e m') ;
    field.setValue('ok') ;
    field.isValid() ;
    expect(field.getGroupClass()).to.be.equal('c k d l') ;
  }) ;

  it('si une fonction init() existe, sera appellée', function () {

    class Test extends Field {
      init () { this.a = 'ok' ; }
    }
    let field = new Test({name: 'bar'}) ;
    expect(field.a).to.be.equal('ok') ;
  }) ;

  it('par défaut est non requis et actif', function () {

    let field1 = new Field({name: 'bar'}),
        field2 = new Field({name: 'bar', required: true, disabled: true}) ;

    expect(field1._required).to.be.false ;
    expect(field1._disabled).to.be.false ;
    expect(field2._required).to.be.true ;
    expect(field2._disabled).to.be.true ;
  }) ;


  describe('help', function () {

    it('par défaut aucun msg', function () {

      let field = new Field({name: 'bar'}) ;
      expect(field._help).to.be.equal(null) ;
    }) ;

    it('on peut ajouter un message d\'aide', function () {

      let field = new Field({name: 'bar', help: 'message'}) ;
      expect(field._help).to.be.equal('message') ;
    }) ;

    it('msg par défaut si required sauf si help===null', function () {

      let field1 = new Field({name: 'bar', help: 'message', required: true}),
          field2 = new Field({name: 'bar', required: true}),
          field3 = new Field({name: 'bar', help: null, required: true}) ;
      expect(field1._help).to.be.equal('message') ;
      expect(field2._help).to.be.equal('Ce champ est obligatoire.') ;
      expect(field3._help).to.be.equal(null) ;
    }) ;
  }) ;


  describe('validation', function () {

    it('test si un champ requis à bien une valeur', function () {

      let field1 = new Field({name: 'foo'}),
          field2 = new Field({name: 'bar', required: true}) ;
      expect(field1.isValid()).to.be.true ;
      expect(field2.isValid()).to.be.false ;
      field2.setValue('foobar') ;
      expect(field2.isValid()).to.be.true ;
    }) ;

    it('utilise le validator pour le test', function () {

      let field = new Field(
        {name: 'foo', required: true},
        'mail', new Validate()
      ) ;
      expect(field.isValid()).to.be.false ;
      field.setValue('foobar') ;
      expect(field.isValid()).to.be.false ;
      field.setValue('foo@bar.com') ;
      expect(field.isValid()).to.be.true ;
    }) ;

    it('ssi quand validé, isSubmit() = true', function () {

      let field1 = new Field({name: 'foo'}),
          field2 = new Field({name: 'bar', required: true}) ;

      expect(field1.isSubmit()).to.be.false ;
      expect(field2.isSubmit()).to.be.false ;

      field1.isValid() ;
      field2.isValid() ;

      expect(field1.isSubmit()).to.be.true ;
      expect(field2.isSubmit()).to.be.true ;
    }) ;

    it('erreurs issues du validateur', function () {

      let field = new Field(
        {name: 'foo'},
        [{mail: {}}, {string: {max: 10}}], new Validate()
      ) ;

      field.setValue('foobar') ;
      expect(field.getErrors()).to.be.deep.equal([]) ;
      expect(field.hasErrors()).to.be.equal(null) ;
      field.isValid() ;
      expect(field.getErrors()).to.be.deep.equal([
        'Doit être une adresse mail.'
      ]) ;
      expect(field.hasErrors()).to.be.equal(true) ;

      field.setValue('foobar@foobar.com') ;
      field.isValid() ;
      expect(field.getErrors()).to.be.deep.equal([
        'La valeur est trop longue, le maximum est 10 et vous avez entré'
        + ' 17 caractère(s).'
      ]) ;

      field.setValue('foobarFoobar') ;
      field.isValid() ;
      expect(field.getErrors()).to.be.deep.equal([
        'Doit être une adresse mail.',
        'La valeur est trop longue, le maximum est 10 et vous avez entré'
        + ' 12 caractère(s).'
      ]) ;

      field.setValue('foo@ba.ar') ;
      field.isValid() ;
      expect(field.getErrors()).to.be.deep.equal([]) ;
      expect(field.hasErrors()).to.be.equal(false) ;
    }) ;

    it('required et validator errors', function () {

      let field = new Field(
        {name: 'foo', required: 'true'},
        [{mail: {}}, {string: {max: 10}}], new Validate()
      ) ;

      field.isValid() ;
      expect(field.getErrors()).to.be.deep.equal([
        'Ce champ est obligatoire.'
      ]) ;

      field.setValue('foobar') ;
      field.isValid() ;
      expect(field.getErrors()).to.be.deep.equal([
        'Doit être une adresse mail.'
      ]) ;
    }) ;

    it('pas de validator', function () {

      let field = new Field({name: 'foo', required: 'true'}) ;

      field.isValid() ;
      expect(field.getErrors()).to.be.deep.equal([
        'Ce champ est obligatoire.'
      ]) ;

      field.setValue('foobar') ;
      field.isValid() ;
      expect(field.getErrors()).to.be.deep.equal([]) ;
    }) ;

    it('validation avec validateur que si champ rempli', function () {

      let field = new Field({name: 'foo'}, 'mail', new Validate()) ;

      expect(field.isValid()).to.be.true ;
      field.setValue('test') ;
      expect(field.isValid()).to.be.false ;
    }) ;

    it('message d\'erreur personnalisé', function () {

      let field = new Field({
        name: 'foo', required: true,
        error: 'Champ obligatoire. Adresse mail non valide ou trop longue.'
      }, [{mail: {}}, {string: {max: 10}}], new Validate()) ;

      field.setValue('foobar') ;
      field.isValid() ;
      expect(field.getErrors()).to.be.deep.equal([
        'Champ obligatoire. Adresse mail non valide ou trop longue.'
      ]) ;

      field.setValue('foobar@foobar.com') ;
      field.isValid() ;
      expect(field.getErrors()).to.be.deep.equal([
        'Champ obligatoire. Adresse mail non valide ou trop longue.'
      ]) ;

      field.setValue('foobarFoobar') ;
      field.isValid() ;
      expect(field.getErrors()).to.be.deep.equal([
        'Champ obligatoire. Adresse mail non valide ou trop longue.'
      ]) ;

      field.setValue('foo@ba.ar') ;
      field.isValid() ;
      expect(field.getErrors()).to.be.deep.equal([]) ;
    }) ;

    it('on peut forcer, après coup, le statut à invalide', function () {

      let field = new Field({name: 'foo'}) ;

      field.setValue('foobar') ;
      field.isValid() ;
      expect(field.hasErrors()).to.be.equal(false) ;
      expect(field.getErrors()).to.be.deep.equal([]) ;

      field.setInvalid('Un message') ;
      expect(field.hasErrors()).to.be.true ;
      expect(field.getErrors()).to.be.deep.equal(['Un message']) ;
    }) ;
  }) ;


  describe('_buildMsg', function () {

    it('Retourne juste des espaces si aucun msg', function () {

      let field = new Field({name: 'bar'}) ;
      expect(field._buildMsg()).to.be.equal('  ') ;
    }) ;

    it('Retourne le msg sinon', function () {

      let field = new Field({name: 'bar', help: 'msg'}) ;
      expect(field._buildMsg()).to.be.equal('<p>msg</p>\n  ') ;
    }) ;
  }) ;
}) ;

