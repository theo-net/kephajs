'use strict' ;

/**
 * src/server/Form/ColorField.js
 */

const InputField = require('./InputField') ;

class ColorField extends InputField {

  init () {

    super.init() ;
    this._type = 'color' ;

    this._validator.add(/^#[a-zA-z0-9]{6}$/) ;
  }
}

module.exports = ColorField ;

