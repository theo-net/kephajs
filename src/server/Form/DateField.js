'use strict' ;

/**
 * src/server/Form/DateField.js
 */

const InputField = require('./InputField') ;

class DateField extends InputField {

  init () {

    super.init() ;
    this._type = 'date' ;

    this._validator.add(/^\d\d\d\d-[0-1]\d-[0-3]\d$/) ;
  }
}

module.exports = DateField ;

