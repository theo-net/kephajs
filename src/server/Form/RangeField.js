'use strict' ;

/**
 * src/server/Form/RangeField.js
 */

const NumberField = require('./NumberField') ;

class RangeField extends NumberField {

  init () {

    super.init() ;
    this._type = 'range' ;
  }
}

module.exports = RangeField ;

