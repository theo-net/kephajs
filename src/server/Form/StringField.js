'use strict' ;

/**
 * src/server/Form/StringField.js
 */

const InputField = require('./InputField'),
      Utils      = require('../../core/Utils') ;

/**
 * Représente un champ `input` de type `text`
 */
class StringField extends InputField {

  /**
   * Nouveaux attributs :
   *   - `[min]` : taille minimum de la valeur
   *   - `[max]` : taille maximum de la valeur
   */
  init () {

    super.init() ;

    let validator = {} ;

    if (this._attr.min) {
      validator.min = this._attr.min ;
      this._minlength = this._attr.min ;
    }
    if (this._attr.max) {
      validator.max = this._attr.max ;
      this._maxlength = this._attr.max ;
    }

    this._validator.add({string: validator}) ;

    if (this._attr.pattern) {

      if (!Utils.isRegExp(this._attr.pattern))
        this._attr.pattern = new RegExp(this._attr.pattern) ;

      this._patternAttr = this._attr.pattern.toString().slice(1, -1) ;

      this._validator.add(this._attr.pattern) ;
    }
    else
      this._pattern = null ;
  }


  _buildAttributes () {

    return super._buildAttributes()
      + (this._minlength ? ' minlength="' + this._minlength + '"' : '')
      + (this._maxlength ? ' maxlength="' + this._maxlength + '"' : '') ;
  }
}

module.exports = StringField ;

