'use strict' ;

/**
 * src/server/Form/Form.test.js
 */

const expect = require('chai').expect ;

const Form      = require('./Form'),
      Block     = require('./Block'),
      Forms     = require('../Services/Forms'),
      ValidateS = require('../../core/Services/Validate') ;

describe('server/Form/Form', function () {

  it('étend Block', function () {

    let form = new Form() ;
    expect(form).to.be.instanceOf(Block) ;
  }) ;

  it('setParent(), getParent() Retourneront tjs le formulaire', function () {

    let form  = new Form(),
        formB = new Form() ;
    expect(form.setParent(formB)).to.be.equal(form) ;
    expect(form.getParent()).to.be.equal(form) ;
  }) ;


  describe('build', function () {

    let form ;

    beforeEach(function () {

      form = new Form(null, new Forms(new ValidateS())) ;
    }) ;

    it('retourne le code HTML du formulaire', function () {

      expect(form.build())
        .to.be.equal('<form action="#" method="get">\n</form>') ;
      form.setId('salut') ;
      expect(form.build())
        .to.be.equal('<form action="#" method="get" id="salut">\n</form>') ;
    }) ;

    it('on peut préciser l\'action et la méthode', function () {

      expect(form.setAction('action.html', 'post')).to.be.equal(form) ;
      expect(form.build())
        .to.be.equal('<form action="action.html" method="post">\n</form>') ;
      form.setAction('act.html') ;
      expect(form.build())
        .to.be.equal('<form action="act.html" method="get">\n</form>') ;
    }) ;

    it('construit tous les sous-éléments', function () {

      form
        .add('string', {name: 'foo', label: 'Un champ'})
        .add('string', {name: 'bar', label: 'Un autre champ'})
        .setAction('action.html', 'post') ;
      expect(form.build()).to.be.equal(
        '<form action="action.html" method="post">\n'
        + '  <div>\n'
        + '    <label for="foo">Un champ</label>\n'
        + '    <input id="foo" name="foo" type="text"><i></i>\n'
        + '      </div>\n'
        + '  <div>\n'
        + '    <label for="bar">Un autre champ</label>\n'
        + '    <input id="bar" name="bar" type="text"><i></i>\n'
        + '      </div>\n'
        + '</form>'
      ) ;
    }) ;
  }) ;


  describe('process', function () {

    let form, formB, entity ;

    beforeEach(function () {

      entity = {
        nom: 'a',
        mail: 'b',
        description: ''
      } ;

      form = new Form(null, new Forms(new ValidateS())) ;
      form.addFieldset('Auteur')
          .add('string', {name: 'nom', required: true}, {string: {min: 3}})
          .add('string', {name: 'mail', required: true}, 'mail')
          .getParent()
          .add('string', {name: 'description', required: true}) ;

      formB = new Form(entity, new Forms(new ValidateS())) ;
      formB.addFieldset('Auteur')
           .add('string', {name: 'nom', required: true}, {string: {min: 3}})
           .add('string', {name: 'mail', required: true}, 'mail')
           .getParent()
           .add('string', {name: 'description', required: true}) ;
    }) ;

    it('rempli tous les champs', function () {

      let req = {
        nom: 'Jésus',
        mail: 'jesus@cieux.god',
        description: 'Fils de Dieu'
      } ;
      form.process(req) ;

      expect(form._fields[0]._fields[0].getValue()).to.be.equal('Jésus') ;
      expect(form._fields[0]._fields[1].getValue())
        .to.be.equal('jesus@cieux.god') ;
      expect(form._fields[1].getValue()).to.be.equal('Fils de Dieu') ;
    }) ;

    it('retourne la requête si tout est valide (pas d\'entity)', function () {

      let req = {
        nom: 'Jésus',
        mail: 'jesus@cieux.god',
        description: 'Fils de Dieu'
      } ;
      expect(form.process(req)).to.be.equal(req) ;
    }) ;

    it('Si une entité est présente, la retourne mise-à-jour', function () {

      let req = {
        nom: 'Jésus',
        mail: 'jesus@cieux.god',
        description: 'Fils de Dieu'
      } ;
      let ret = formB.process(req) ;

      expect(ret).to.be.equal(entity) ;
      expect(ret).to.be.deep.equal(req) ;
    }) ;

    it('Si une entité est présente, mais req invalide, non maj', function () {

      let req = {
        nom: 'Jésus',
        mail: 'jesuscieux.god',
        description: 'Fils de Dieu'
      } ;
      let ret = formB.process(req) ;

      expect(ret).to.be.equal(null) ;
      expect(formB.getEntity()).to.be.deep.equal({
        nom: 'a', mail: 'b', description: ''
      }) ;
    }) ;

    it('retourne null sinon', function () {

      let req1 = {
        nom: 'Jésus',
        mail: 'jesus@cieux.god',
        description: 'foobar'
      } ;
      let req2 = {
        nom: 'Jésus',
        mail: 'jesuscieux.god',
        description: 'foobar'
      } ;
      let req3 = {
        nom: 'Jésus',
        mail: 'jesus@cieux.god'
      } ;
      expect(form.process(req1)).to.be.equal(req1) ;
      expect(form.process(req2)).to.be.equal(null) ;
      expect(formB.process(req3)).to.be.equal(null) ;
    }) ;

    it('tous les champs sont bien parcourus', function () {

      form.process({
        nom: 'Jé',
        mail: 'jesus@cieux.god',
      }) ;

      expect(form._fields[0]._fields[0].isSubmit()).to.be.true ;
      expect(form._fields[0]._fields[1].isSubmit()).to.be.true ;
      expect(form._fields[1].isSubmit()).to.be.true ;
    }) ;
  }) ;
}) ;

