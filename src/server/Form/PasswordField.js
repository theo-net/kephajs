'use strict' ;

/**
 * src/server/Form/PasswordField.js
 */

const StringField = require('./StringField') ;

class PasswordField extends StringField {

  init () {

    super.init() ;
    this._type = 'password' ;
  }
}

module.exports = PasswordField ;

