'use strict' ;

/**
 * src/server/Form/Fieldset.js
 */

const Block = require('./Block') ;

/**
 * Représente un block DIV
 * Techniquement, il s'agit d'un
 */
class Fieldset extends Block {


  /**
   * Initialise un nouveau fieldset.
   * En lui donnant une entité, on permet l'autoremplissage des champs et
   * l'autovalidation
   * @param {Entity} [entity=null] Entité à transmettre
   * @param {Forms} [forms] Service $forms
   * @param {String} [idPrefix] Préfixe de l'id quand celui-ci est défini
   */
  constructor (entity = null, forms = null, idPrefix = null) {

    super(entity, forms, idPrefix) ;
    this._legend = null ;
  }


  /**
   * Définie la légende du fieldset
   * @param {String} legend La légende
   * @returns {this}
   */
  setLegend (legend) {

    this._legend = legend ;
    return this ;
  }


  /**
   * Retourne la légende associée au fieldset. `null` si aucune
   * @returns {String|null}
   */
  getLegend () {

    return this._legend ;
  }

  /**
   * Construit le code HTML du fieldset et le retourne.
   * @returns {String}
   */
  build () {

    let classCss = this.getClass() ;
    let fieldset = '<fieldset' + (this._id ? ' id="' + this._id + '"' : '')
                 + (classCss != '' ? ' class="' + classCss + '"' : '')
                 + '>\n' ;

    if (this.getLegend() !== null)
      fieldset += '  <legend>' + this.getLegend() + '</legend>\n' ;

    this._fields.forEach(field => {

      fieldset += field.build() + '\n' ;
    }) ;

    return fieldset + '</fieldset>' ;
  }


  /**
   * Retourne les classes CSS appliquées au fieldset. Assemblera celles définies
   * spécifiquement pour lui et celles ajoutées par `addClass{fieldset: ...}`.
   * @returns {String}
   */
  getClass () {

    return this._class.fieldset.concat(this._classBlock).join(' ') ;
  }
}

module.exports = Fieldset ;

