'use strict' ;

/**
 * src/server/Form/CheckboxField.js
 */

const RadioField = require('./RadioField') ;

class CheckboxField extends RadioField {

  init () {

    super.init() ;
    this._type = 'checkbox' ;
  }

  setValue (value) {

    if (!Array.isArray(value))
      value = Array(value) ;
    this._value = value ;

    // Le constructor ne peut définir la valeur, car field non initialisé
    if (Array.isArray(this._group)) {
      this._group.forEach(elmt => {
        elmt.checked = value.indexOf(elmt.value) > -1 ? true : false ;
      }) ;
    }
  }

  isValid () {

    this._validated = true ;
    this._valid = true ;
    this._requiredValid = true ;

    if (this._required) {

      this._requiredValid = this._value !== undefined && this._value !== null &&
        this._value !== '' ;
    }

    if (this._validator && Array.isArray(this._value)) {
      this._value.forEach(value => {
        this._valid = this._valid && this._validator.isValid(value) ;
      }) ;
    }

    return this._requiredValid && this._valid ;
  }
}

module.exports = CheckboxField ;

