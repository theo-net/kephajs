'use strict' ;

/**
 * src/server/Form/SubmitField.js
 */

const InputField = require('./InputField') ;

/**
 * Représente un champ `input` de type `submit`
 */
class SubmitField extends InputField {

  /**
   * Nouveaux attributs :
   *   - `[formmethod]` : change la méthode du formulaire
   *   - `[formaction]` : change l'action du formulaire
   */
  init () {

    super.init() ;
    this._type = 'submit' ;

    this._formmethod = this._attr.formmethod ? this._attr.formmethod : null ;
    this._formaction = this._attr.formaction ? this._attr.formaction : null ;
  }


  /**
   * Par rapport aux éléments parents, ne prend pas en compte les classes
   * `groupSuccess` et `groupError`
   * @param {Object} cssObj Les classes CSS
   */
  addClass (cssObj) {

    super.addClass(cssObj) ;

    this._class.groupSuccess = [] ;
    this._class.groupError = [] ;
  }

  _buildAttributes () {

    return super._buildAttributes()
      + (this._formmethod ? ' formmethod="' + this._formmethod + '"' : '')
      + (this._formaction ? ' formaction="' + this._formaction + '"' : '') ;
  }
}

module.exports = SubmitField ;

