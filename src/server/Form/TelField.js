'use strict' ;

/**
 * src/server/Form/TelField.js
 */

const StringField = require('./StringField') ;

class TelField extends StringField {

  init () {

    super.init() ;
    this._type = 'tel' ;
  }
}

module.exports = TelField ;

