'use strict' ;

/**
 * src/server/Form/UrlField.js
 */

const StringField = require('./StringField') ;

class UrlField extends StringField {

  init () {

    super.init() ;
    this._type = 'url' ;
  }
}

module.exports = UrlField ;

