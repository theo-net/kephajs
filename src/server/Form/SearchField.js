'use strict' ;

/**
 * src/server/Form/SearchField.js
 */

const StringField = require('./StringField') ;

class SearchField extends StringField {

  init () {

    super.init() ;
    this._type = 'search' ;
  }
}

module.exports = SearchField ;

