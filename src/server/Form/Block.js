'use strict' ;

/**
 * src/server/Form/Block.js
 */

const Field = require('./Field'),
      Utils = require('../../core/Utils') ;

/**
 * Représente un block
 */
class Block {

  /**
   * Initialise un nouveau block.
   * En lui donnant une entité, on permet l'autoremplissage des champs et
   * l'autovalidation
   * @param {Entity} [entity=null] Entité à transmettre
   * @param {Forms} [forms] Service $forms
   * @param {String} [idPrefix] Préfixe de l'id quand celui-ci est défini
   */
  constructor (entity = null, forms = null, idPrefix = null) {

    this.$forms = forms ;
    this.setEntity(entity) ;

    this._fields = [] ;

    this.resetClass() ;
    this._classBlock = [] ;
    this._id = null ;
    this._idPrefix = idPrefix ;
    this._legend = null ;
    this._parent = null ;
  }


  /**
   * Associe une entité au block. On peut spécifier de ne pas en avoir avec
   * la valeur `null` (par défaut)
   * @param {Entity} [entity=null] Entité à associer
   * @returns {this}
   */
  setEntity (entity = null) {

    this._entity = entity ;
    return this ;
  }


  /**
   * Retourne l'entité associée au block
   * @returns {Entity}
   */
  getEntity () {

    return this._entity ;
  }


  /**
   * Définie un block parent associé au block
   * @param {Block} block Block
   * @returns {this}
   */
  setParent (block) {

    this._block = block ;
    return this ;
  }


  /**
   * Retourne le block parent
   * @returns {Block}
   */
  getParent () {

    return this._block ;
  }


  /**
   * Définit l'id du block. Celui-ci sera utilisé pour préfixer tous les noms
   * et id des champs. Il sera préfixé, en camelCase, par le préfixe défini
   * lors de la construction de l'objet.
   * @param {String} id Id du block
   * @returns {this}
   */
  setId (id) {

    if (this._idPrefix)
      this._id = this._idPrefix + Utils.ucfirst(id) ;
    else
      this._id = id ;

    return this ;
  }


  /**
   * Retourne l'id du block.
   * @returns {String|null}
   */
  getId () {

    return this._id ;
  }


  /**
   * Ajoute un champ au block
   * @param {String} type Type du champ
   * @param {Object} attributes Attributs du champs, doit avoir au moins `name`
   * @param {*} validator Validator (cf service)
   * @throws {Error} Si attributs ne possède pas de champ `name`
   * @returns {this}
   */
  add (type, attributes, validator) {

    if (!attributes || !attributes.name)
      throw new Error('Pas d\'attribut `name` transmis au champ !') ;

    if (this._id || this._idPrefix) {

      let prefix = this._id ? this._id : this._idPrefix ;

      if (!attributes.bind)
        attributes.bind = attributes.name ;

      attributes.name = prefix + Utils.ucfirst(attributes.name) ;

      if (attributes.id)
        attributes.id = prefix + Utils.ucfirst(attributes.id) ;
    }

    let field = new (this.$forms.getField(type))(
      attributes, validator, this.$forms.$validate
    ) ;
    field.addClass(this._class) ;

    if (this._entity != null) {
      if (Object.keys(this._entity).indexOf(field._name) > -1)
        field.setValue(this._entity[attributes.name]) ;
      else if (Object.keys(this._entity).indexOf(attributes.bind) > -1)
        field.setValue(this._entity[attributes.bind]) ;
    }

    this._fields.push(field) ;

    return this ;
  }


  /**
   * Ajoute un bloc
   * @param {String|Array} [css] Classes css à appliquer au bloc
   * @returns {Block}
   */
  addBlock (css) {

    let block = new Block(this.getEntity(), this.$forms) ;

    if (css) block.addClass(css) ;
    block.setParent(this) ;

    block.addClass(this._class) ;

    this._fields.push(block) ;

    return block ;
  }


  /**
   * Ajoute un fieldset (groupe de champs).
   * @param {String} [legend = null] Légende du Fieldset
   * @param {Object} classCss Classes CSS à transmettre aux fields enfants du
   *    fieldset
   * @returns {Fieldset}
   */
  addFieldset (legend = null, classCss) {

    let fieldset =
      new (require('./Fieldset'))(this.getEntity(), this.$forms, this.getId()) ;

    if (classCss)
      fieldset.addClass(classCss) ;

    fieldset.setParent(this).setLegend(legend).addClass(this._class) ;

    this._fields.push(fieldset) ;

    return fieldset ;
  }


  /**
   * Retourne un field qui a été ajouté au bloc.
   * @param {String} name Identifiant du field
   * @returns {Field}
   */
  getField (name) {

    let  found ;

    Utils.forEach(this._fields, field => {

      if (field instanceof Field && field._name == name) {
        found = field ;
        return false ;
      }
      else if (field instanceof Block) {
        found = field.getField(name) ;
        if (found) return false ;
      }
    }) ;

    return found ;
  }


  /**
   * Ajoute automatiquement des classes à tous les champs ajoutés au formulaire.
   * Cf. Field.addClass(). S'il s'agit d'un `Array` ou d'un `String`, les classes
   * seront ajoutées pour le block courant.
   * @param {String|Array|Object} cssObj Les classes à ajouter
   * @returns {this}
   */
  addClass (cssObj) {

    if (Array.isArray(cssObj))
      this._classBlock = this._classBlock.concat(cssObj) ;
    else if (Utils.isString(cssObj))
      this._classBlock.push(cssObj) ;
    if (Utils.isObject(cssObj)) {

      Utils.forEach(cssObj, (css, type) => {

        if (Array.isArray(this._class[type])) {

          if (Utils.isString(css))
            this._class[type].push(css) ;
          else if (Array.isArray(css))
            this._class[type] = this._class[type].concat(css) ;
        }
      }) ;
    }

    return this ;
  }


  /**
   * Remet à zéro toutes les classes CSS
   * @returns {this}
   */
  resetClass () {

    this._class = {
      field: [],
      label: [],
      group: [],
      groupSuccess: [],
      groupError: [],
      msg: [],
      msgHelp: [],
      msgError: [],
      fieldset: []
    } ;

    return this ;
  }


  /**
   * Retourne les classes CSS appliquées au block.
   * @returns {String}
   */
  getClass () {

    return this._classBlock.join(' ') ;
  }


  /**
   * Rendu du bloc
   * @returns {String}
   */
  build () {

    let classCss = this.getClass() ;
    let block = '<div' + (this._id ? ' id="' + this._id + '"' : '')
              + (classCss != '' ? ' class="' + classCss + '"' : '')
              + '>\n' ;

    this._fields.forEach(field => {

      block += field.build() + '\n' ;
    }) ;

    return block + '</div>' ;
  }


  /**
   * Exécute le sur tous les fields : test valeurs + maj
   * @param {Object} vars Variables soumises `{id: value, ...}`
   * @returns {Null|Object} Null si erreur, object sinon
   * @private
   */
  _process (vars) {

    if (!this._fieldsValid(vars))
      return null ;
    else if (this.getEntity() === null)
      return vars ;
    else {

      this._majFields() ;
      return this.getEntity() ;
    }
  }


  /**
   * Test la valeur de tous les fields
   * @param {Object} vars Variables soumises `{id: value, ...}`
   * @returns {Boolean}
   * @private
   */
  _fieldsValid (vars) {

    let test = true ;

    this._fields.forEach(field => {

      if (field instanceof Field) {

        if (vars[field._id])
          field.setValue(vars[field._id]) ;

        test = field.isValid() && test ;
      }
      else if (field instanceof Block)
        test = field._fieldsValid(vars) && test ;
    }) ;

    return test ;
  }


  /**
   * Met à jour les valeurs des fields avec l'entité
   * @private
   */
  _majFields () {

    this._fields.forEach(field => {
      if (field instanceof Block)
        field._majFields() ;
      else if (Object.keys(this._entity).indexOf(field._name) > -1)
        this.getEntity()[field._name] = field.getValue() ;
      else if (Object.keys(this._entity).indexOf(field._bind) > -1)
        this.getEntity()[field._bind] = field.getValue() ;
    }) ;
  }
}

module.exports = Block ;

