'use strict' ;

/**
 * src/server/Form/TextField.js
 */

const Field = require('./Field') ;

/**
 * Représente un champ `textarea`
 */
class TextField extends Field {

  /**
   * Nouveaux attributs disponibles :
   *  - `[cols]` : Nombre de colonnes
   *  - `[rows]` : Nombre de lignes
   *  - `[placeholder]` : Placeholder du champ
   *  - `[wrap]` : Retour à la ligne (`hard`, `soft`, `off`)
   *  - `[spellcheck]` : correction orthographique (`true`, `false`, `default`)
   *  - `[min]` : taille minimum de la valeur
   *  - `[max]` : taille maximum de la valeur
   */
  init () {

    this._rows = this._attr.rows ;
    this._cols = this._attr.cols ;
    this._placeholder = this._attr.placeholder ? this._attr.placeholder : null ;
    this._wrap = this._attr.wrap ? this._attr.wrap : null ;
    this._spellcheck = this._attr.spellcheck != undefined ?
                         this._attr.spellcheck : null ;

    let validator = {} ;

    if (this._attr.min) {
      validator.min = this._attr.min ;
      this._minlength = this._attr.min ;
    }
    if (this._attr.max) {
      validator.max = this._attr.max ;
      this._maxlength = this._attr.max ;
    }

    this._validator.add({string: validator}) ;
  }

  build () {

    let widget = '  <div'
               + (this.getGroupClass() != '' ?
                  ' class="' + this.getGroupClass() + '"' : '')
               + '>\n' ;

    widget += this._buildLabel() ;
    widget += '    <textarea' + this._buildAttributes() + '>'
            + (this._valueAttr ? this._valueAttr : '')
            + '</textarea><i></i>\n    ' + this._buildMsg() + '</div>' ;

    return widget ;
  }


  /**
   * Construit les attributs du champ
   * @returns {String}
   * @private
   */
  _buildAttributes () {

    return (this.getClass() != '' ? ' class="' + this.getClass() + '"' : '')
      + ' id="' + this._id + '" name="' + this._name + '"'
      + (this._cols ? ' cols="' + this._cols + '"' : '')
      + (this._rows ? ' rows="' + this._rows + '"' : '')
      + (this._placeholder ? ' placeholder="' + this._placeholder + '"' : '')
      + (this._wrap ? ' wrap="' + this._wrap + '"' : '')
      + (this._required ? ' required' : '')
      + (this._disabled ? ' disabled' : '')
      + (this._spellcheck !== null ? (' spellcheck="'
      +   (this._spellcheck == 'default' ? 'default' : (
            (this._spellcheck ? 'true' : 'false')
          )) + '"'
        ) : '')
      + (this._minlength ? ' minlength="' + this._minlength + '"' : '')
      + (this._maxlength ? ' maxlength="' + this._maxlength + '"' : '') ;
  }
}

module.exports = TextField ;

