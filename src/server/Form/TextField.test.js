'use strict' ;

/**
 * src/server/Form/TextField.test.js
 */

const TextField       = require('./TextField'),
      StringValidator = require('../../core/Validate/String'),
      ValidateS       = require('../../core/Services/Validate') ;

const expect = require('chai').expect ;

describe('server/Form/TextField', function () {

  it('on peut préciser cols et rows', function () {

    let text = new TextField({name: 'foo', cols: 10, rows: 5},
                             null, new ValidateS()) ;

    expect(text._cols).to.be.equal(10) ;
    expect(text._rows).to.be.equal(5) ;
    expect(text._buildAttributes())
      .to.be.equal(' id="foo" name="foo" cols="10" rows="5"') ;
  }) ;

  it('peut avoir un placeholder', function () {

    let text = new TextField({name: 'foo', placeholder: 'bar'},
                             null, new ValidateS()) ;

    expect(text._placeholder).to.be.equal('bar') ;
    expect(text._buildAttributes())
      .to.be.equal(' id="foo" name="foo" placeholder="bar"') ;
  }) ;

  it('peut avoir spellcheck', function () {

    let text1 = new TextField({name: 'foo', spellcheck: true},
                              null, new ValidateS()),
        text2 = new TextField({name: 'foo', spellcheck: false},
                              null, new ValidateS()),
        text3 = new TextField({name: 'foo', spellcheck: 'default'},
                              null, new ValidateS()) ;

    expect(text1._buildAttributes())
      .to.be.equal(' id="foo" name="foo" spellcheck="true"') ;
    expect(text2._buildAttributes())
      .to.be.equal(' id="foo" name="foo" spellcheck="false"') ;
    expect(text3._buildAttributes())
      .to.be.equal(' id="foo" name="foo" spellcheck="default"') ;
  }) ;

  it('peut avoir wrap', function () {

    let text1 = new TextField({name: 'foo', wrap: 'hard'},
                              null, new ValidateS()),
        text2 = new TextField({name: 'foo', wrap: 'soft'},
                              null, new ValidateS()),
        text3 = new TextField({name: 'foo', wrap: 'off'},
                              null, new ValidateS()) ;

    expect(text1._buildAttributes())
      .to.be.equal(' id="foo" name="foo" wrap="hard"') ;
    expect(text2._buildAttributes())
      .to.be.equal(' id="foo" name="foo" wrap="soft"') ;
    expect(text3._buildAttributes())
      .to.be.equal(' id="foo" name="foo" wrap="off"') ;
  }) ;

  it('validator String', function () {

    let text = new TextField({name: 'foo'}, null, new ValidateS()) ;
    expect(text._validator._validators[1])
      .to.be.instanceOf(StringValidator) ;
  }) ;

  it('attribut min -> minlength', function () {

    let text = new TextField({name: 'foo', min: 1}, null, new ValidateS()) ;

    expect(text._buildAttributes())
      .to.be.equal(' id="foo" name="foo" minlength="1"') ;
    expect(text._validator._validators[1]._validatorArguments.min)
      .to.be.equal(1) ;
  }) ;

  it('attribut max -> maxlength', function () {

    let text = new TextField({name: 'foo', max: 2}, null, new ValidateS()) ;

    expect(text._buildAttributes())
      .to.be.equal(' id="foo" name="foo" maxlength="2"') ;
    expect(text._validator._validators[1]._validatorArguments.max)
      .to.be.equal(2) ;
  }) ;

  it('build', function () {

    let text = new TextField({name: 'foo', help: 'a'}, null, new ValidateS()) ;

    expect(text.build()).to.be.equal(
      '  <div>\n    '
      + '<textarea id="foo" name="foo"></textarea><i></i>\n    '
      + '<p>a</p>\n  '
      + '</div>'
    ) ;
  }) ;
}) ;

