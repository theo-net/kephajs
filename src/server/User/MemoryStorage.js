'use strict' ;

/**
 * src/server/User/MemoryStorage.js
 */

const Utils = require('../../cli/Utils') ;

const MILLI2SEC = 1000 ;

/**
 * Stockage des sessions en mémoire
 */
class MemoryStorage {

  constructor () {

    this._sessions = {} ;
  }


  /**
   * Initialise une nouvelle session. Si l'identifiant est déjà pris, la
   * nouvelle session écrasera l'ancienne
   * @param {String} id Identifiant de la session
   */
  init (id) {

    this._sessions[id] = {
      content: {},
      lastAccess: Date.now()
    } ;
  }


  /**
   * Retourne le nombre de session stockées en mémoire
   * @returns {Number}
   */
  count () {

    return Object.keys(this._sessions).length ;
  }


  /**
   * Indique si une session est enregistrée
   * @param {String} id Identifiant de la session
   * @returns {Boolean}
   */
  has (id) {

    return Object.keys(this._sessions).indexOf(id) > -1 ;
  }


  /**
   * Détruit une session
   * @param {String} id Identifiant de la session
   */
  remove (id) {

    delete this._sessions[id] ;
  }


  /**
   * Enregistre/met à jour un élément dans une session
   * @param {String} id Identifiant de la session
   * @param {String} name Nom de la variable
   * @param {*} value Valeur à stocker
   */
  set (id, name, value) {

    this._sessions[id].content[name] = value ;
  }


  /**
   * Indique si un élément est contenu dans la session
   * @param {String} id Identifiant de la session
   * @param {String} name Nom de la variable
   * @returns {Boolean}
   */
  hasElement (id, name) {

    return Object.keys(this._sessions[id].content).indexOf(name) > -1 ;
  }

  /**
   * Récupère un élément d'une session
   * @param {String} id Identifiant de la session
   * @param {String} name Nom de la variable
   * @returns {*}
   */
  get (id, name) {

    return this._sessions[id].content[name] ;
  }


  /**
   * Supprime un élément d'une sessions
   * @param {String} id Identifiant de la session
   * @param {String} name Nom de l variable
   */
  delete (id, name) {

    delete this._sessions[id].content[name] ;
  }


  /**
   * Met à jour le timestamp de la session en indiquant qu'un accès est
   * demandé
   * @param {String} id Identifiant de la session
   */
  access (id) {

    this._sessions[id].lastAccess = Date.now() ;
  }


  /**
   * Indique si une session est trop vieille
   * @param {String} id Identifiant de la session
   * @param {Integer} maxAge Âge maximum des sessions
   * @returns {Boolean}
   */
  isTooOld (id, maxAge) {

    return Date.now() - this._sessions[id].lastAccess > maxAge * MILLI2SEC ;
  }

  /**
   * Supprime toutes les sessions trop âgées
   * @param {Integer} maxAge Âge maximum des sessions
   */
  garbage (maxAge) {

    Utils.forEach(this._sessions, (session, id) => {
      if (this.isTooOld(id, maxAge))
        this.remove(id) ;
    }) ;
  }
}

module.exports = MemoryStorage ;

