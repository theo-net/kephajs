'use strict' ;

/**
 * src/server/User/Sessions.js
 */

const Utils = require('../../core/Utils') ;

const NBPURGE    = 200,   // seuil à partir duquel on lance le garbage collector
      EXPSESSION = 1440 ; // s, soit 24 min (durée de vie d'une session)

/**
 * Service donnant acès à toutes les sessions enregistrées sur le serveur
 */
class Sessions {

  /**
   * Initialise le service avec un objet gérant le stockage des sessions
   * @param {Object} storage Objet gérant le stockage des sessions
   */
  constructor (storage) {

    this._storage = storage ;
  }


  /**
   * Initialise une nouvelle session, vérifiera que l'identifiant est unique
   * @return {String} Identifiant de la nouvelle session
   */
  init () {

    let id = Utils.uuid() ;
    while (this._storage.has(id))
      id = Utils.uuid() ;

    this._storage.init(id) ;

    return id ;
  }


  /**
   * Supprime une session
   * @param {String} id Identifiant de la session à supprimer
   */
  remove (id) {

    this._storage.remove(id) ;
  }


  /**
   * Indique si une session existe ou pas. Retournera également `false` si elle
   * a expiré.
   * @param {String} id Identifiant de la session
   * @returns {Boolean}
   */
  has (id) {

    if (!this._storage.has(id))
      return false ;

    return !this._storage.isTooOld(id, EXPSESSION) ;
  }


  /**
   * Définit un élément dans une session
   * @param {String} id Identifiant de la session
   * @param {String} name Nom de la variable
   * @param {*} value Valeur
   */
  set (id, name, value) {

    this._storage.set(id, name, value) ;
  }


  /**
   * Indique si la session possède ou non un élément
   * @param {String} id Identifiant de la session
   * @param {String} name Nom de la valeur
   * @returns {Boolean}
   */
  hasElement (id, name) {

    return this._storage.hasElement(id, name) ;
  }


  /**
   * Récupère un élément de la session
   * @param {String} id Identifiant de la session
   * @param {String} name Nom de la valeur
   * @returns {*}
   */
  get (id, name) {

    return this._storage.get(id, name) ;
  }


  /**
   * Supprime un élément de la sessions
   * @param {String} id Identifiant de la session
   * @param {String} name Nom de la valeur
   */
  delete (id, name) {

    this._storage.delete(id, name) ;
  }


  /**
   * Indique un accès demandé à la session. Lance le garbage collector s'il y a
   * plus de 200 sessions en mémoire. Les sessions ont une durée de vie de
   * 1440 s soit 24 min.
   * @param {String} id Identifiant de la session
   */
  hello (id) {

    this._storage.access(id) ;

    if (this._storage.count() > NBPURGE)
      this._storage.garbage(EXPSESSION) ;
  }
}

module.exports = Sessions ;

