'use strict' ;

/**
 * src/server/View.js
 */

const fs   = require('fs'),
      path = require('path') ;

const ViewError    = require('./ViewError'),
      KernelAccess = require('../core/KernelAccess'),
      Utils        = require('../cli/Utils') ;

const MIME = {
  'html':  'text/html',
  'js':    'text/javascript',
  'json':  'application/json',
  'map':   'application/octet-stream',
  'css':   'text/css',
  'ico':   'image/x-icon',
  'png':   'image/png',
  'jpg':   'image/jpeg',
  'gif':   'image/gif',
  'wav':   'audio/wav',
  'mp3':   'audio/mpeg',
  'svg':   'image/svg+xml',
  'pdf':   'application/pdf',
  'doc':   'application/msword',
  'eot':   'appliaction/vnd.ms-fontobject',
  'ttf':   'application/font-sfnt',
  'txt':   'text/plain',
  'woff':  'application/font-woff',
  'woff2': 'application/font-woff'
} ;

/**
 * Génère une page ressource qui sera envoyée au client.
 *
 * Peut être du html, mais aussi du css, json, ...
 */
class View extends KernelAccess {

  /**
   * Créé une nouvelle vue. Si `contentFile` est `null`, une vue vide sera
   * chargée
   * @param {String} [contentFile] Fichier contenant la vue
   * @param {String} [mime=html] Mime type
   */
  constructor (contentFile, mime) {

    super() ;

    this._async = false ;
    this._layout = false ;
    this._contentFile = contentFile ;
    this._contentSource = '' ;

    // Vue vide
    if (contentFile == null) {

      this._rootDir = '' ;
      this._content = function () { return '' ; } ;
    }
    // On charge la vue
    else {

      this._rootDir = path.dirname(contentFile) ;

      try {
        let data = fs.readFileSync(contentFile) ;
        this.$setContent(data.toString(), contentFile) ;
      } catch (e) {
        throw new ViewError('Impossible de charger la vue ' + contentFile
          + ': accès au fichier impossible (' + e + ')') ;
      }
    }

    // On définie le type MIME
    this.$setMimeType(mime ? mime : 'html') ;
  }


  /**
   * Génère le rendu de la vue et renvoit le contenu
   * Tout le contenu de `$view.helpers` défini dans le service `$config` sera
   * disponible dans la vue. Cela permet de créer un système d'helpers.
   * @returns {Promise}
   */
  $render () {

    let data = Utils.extend({},
      this, {
        '$getSlot': this.$getSlot,
        '$getRoute': this.$getRoute
      },
      this.kernel().get('$config').get('$view', {helpers: {}}).helpers
    ) ;

    let self = this ;
    async function render () {

      try {

        // Rendu du slot
        data.content = await self._content(data) ;
        // Ajout du layout
        if (self._layout)
          data.content = await self._layout(data) ;

        return data.content ;
      }
      catch (e) {

        console.error(self.kernel().get('$color').red(
          'Error view render (' + self._contentFile + ')' + e
        )) ;
        self.$response.sendError500() ;
      }
    }

    return render() ;
  }


  /**
   * Définie le type Mime
   * @param {String} mime Type Mime
   */
  $setMimeType (mime) {

    this._mimeType = MIME[mime] ;
    if (this._mimeType == undefined) {
      throw new ViewError('Impossible de charger la vue ' + this._contentFile
        + ': MIME type `' + mime + '` non supporté') ;
    }
  }


  /**
   * Retourne le type MIME
   * @returns {String}
   */
  $getMimeType () {

    return this._mimeType ;
  }


  /**
   * Défini un layout en chargeant le fichier `layout`. Si ce paramètre vaut
   * `false`, alors aucun layout ne sera chargé.
   * @param {String|Boolean} layout Chemin vers le layout
   * @throws {ViewError} Si la vue n'arrive pas à charger le fichier
   */
  $setLayout (layout) {

    if (layout) {
      try {

        this._layoutSource = this._includeSubFiles(
          fs.readFileSync(layout).toString(), layout
        ) ;
        this._layout = this.kernel().get('$interpolate')(this._layoutSource) ;
      } catch (e) {

        throw new ViewError(
          'Impossible de charger le layout ' + layout + ' ' + e
        ) ;
      }
    }
    else
      this._layout = false ;
  }


  /**
   * Définie le contenu de la vue
   * @param {String} content Contenu
   * @param {String} [contentFile] Fichier contenant la vue
   */
  $setContent (content, contentFile) {

    // On charge les fichiers inclus
    this._contentSource = this._includeSubFiles(content, contentFile) ;

    // On analyse le template
    try {
      this._content = this.kernel().get('$interpolate')(this._contentSource) ;
    } catch (e) {

      throw new ViewError('Impossible de charger la vue ' + contentFile
        + ': template invalide... ' + e) ;
    }
  }


  /**
   * Helper : retourne le contenu d'un autre Slot
   * @param {String} controller Controlleur
   * @param {String} action Action
   * @param {Object} [vars] Variables à transmettre
   * @returns {Promise}
   */
  $getSlot (controller, action, vars = {}) {

    let Slot = require('./Slot') ;

    let slot = new Slot(
      controller, action, vars,
      this.$request, this.$response,
      this.$user
    ) ;

    return new Promise(resolve => {

      slot.execute().then(() => {
        return slot.getView().$render().then(render => { resolve(render) ; }) ;
      }) ;
    }) ;
  }


  /**
   * Helper : retourne une URL construite à partir du routeur. Https si le
   * paramètre `forceHttps` du `$config` vaut true.
   * @param {String} id Identifiant de la router
   * @param {Object} vars Paramètres
   * @returns {String}
   */
  $getRoute (id, vars) {

    let Kernel = require('../core/Kernel') ;
    let kernel = new Kernel() ;

    return kernel.get('$router').unroute(
      id, vars, this.$request.headers.host,
      kernel.get('$config').get('forceHttps', false)
    ) ;
  }


  /**
   * Inclus un fichier dans une view (pour $setContent et $setLayout). Cette
   * inclusion sera relative au fichier courant. Si aucun n'est préciser, ce sera
   * relatif au basepath.
   * @param {String} content Contenu à traité
   * @param {String} [contentFile] Chemin vers le fichier contenant l'inclusion
   * @returns {String} Contenu traité
   * @private
   */
  _includeSubFiles (content, contentFile) {

    let rootDir = this._rootDir ;
    if (contentFile) rootDir = path.dirname(contentFile) ;

    // On charge les fichiers inclus
    content = content.replace(
      /\{\{\s*\$include\(\s*'(.*)'\s*\)\s*\}\}/g,
      (match, file) => {

        file = rootDir + path.sep + file ;
        try {
          let data = fs.readFileSync(file) ;
          return data.toString() ;
        } catch (e) {
          throw new ViewError('#include dans ' + contentFile
            + ' Impossible de charger le bloc ' + file
            + ': accès au fichier impossible') ;
        }
      }
    ) ;

    return content ;
  }
}

module.exports = View ;

