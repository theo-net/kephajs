'use strict' ;

/**
 * src/server/StaticController.test.js
 */

const expect = require('chai').expect,
      sinon  = require('sinon'),
      http   = require('http') ;

const StaticController = require('./StaticController'),
      View             = require('./View'),
      Resp             = require('./Resp'),
      User             = require('./User/User'),
      Kernel           = require('../core/Kernel'),
      Config           = require('../core/Services/Config') ;


describe('server/StaticController', function () {

  let staticCtrl, kernel, basepath, spy, staticCache, request ;

  beforeEach(function () {

    kernel = new Kernel() ;
    basepath = process.cwd() + '/doc/demo-server/' ;
    kernel.get('$config').set('basepath', basepath) ;
    kernel.get('$config').set('staticDir', 'web/') ;
    kernel.container().set('$staticCache', new Config()) ;
    staticCache = kernel.get('$staticCache') ;
    request = new http.IncomingMessage() ;

    staticCtrl = new StaticController() ;
    staticCtrl.kernel = () => { return kernel ; } ;
    staticCtrl.$view = new View(null) ;
    staticCtrl.$request = request ;
    staticCtrl.$response = new Resp(new http.ServerResponse('get')) ;
    staticCtrl.init() ;

    spy = sinon.spy(staticCtrl, '_staticFile') ;
  }) ;

  it('récupère le staticDir depuis la config', function () {

    expect(staticCtrl._staticDir).to.be.equal('web/') ;
  }) ;

  it('on peut le préciser dans les variables', function () {

    return staticCtrl.slotAction({staticDir: 'foobar', file: ''}).catch(() => {
      expect(staticCtrl._staticDir).to.be.equal('foobar') ;
    }) ;
  }) ;

  it('pas de layout !', function () {

    return staticCtrl.slotAction({file: ''}).catch(() => {
      expect(staticCtrl.$view._layout).to.be.equal(false) ;
    }) ;
  }) ;

  it('envoi fichier s\'il existe (semi-test)', function () {

    return staticCtrl.slotAction({file: 'styles.css'}).then(() => {
      expect(spy.calledWith(basepath + 'web/styles.css')).to.be.true ;
    }) ;
  }) ;

  it('envoi dir/index.html si répertoire demandé', function () {

    return staticCtrl.slotAction({file: 'dir'}).then(() => {
      expect(spy.calledWith(basepath + 'web/dir/index.html')).to.be.true ;
    }) ;
  }) ;

  it('reject 404 si fichier inexistant', function () {

    return staticCtrl.slotAction({file: 'foobar'}).catch((code) => {
      expect(code).to.be.equal(404) ;
    }) ;
  }) ;

  it('reject 404 si dir/index.html inexistant', function () {

    return staticCtrl.slotAction({file: ''}).catch((code) => {
      expect(code).to.be.equal(404) ;
    }) ;
  }) ;

  it('enregistre dans `_staticCache` le path du fichier demandé', function () {

    return staticCtrl.slotAction({file: 'styles.css'}).then(() => {

      expect(staticCache.has(basepath + 'web/styles.css')).to.be.true ;
    }) ;
  }) ;

  it('ETag et date du premier accès sauvés', function () {

    return staticCtrl.slotAction({file: 'styles.css'}).then(() => {

      let cache = staticCache.get(basepath + 'web/styles.css'),
          now   = new Date() ;
      expect(cache.lastModified.getUTCSeconds())
        .to.be.equal(now.getUTCSeconds()) ;
      expect(cache.etag).to.be.not.equal(undefined) ;
    }) ;
  }) ;

  it('transmet un header Last-Modified et un header ETag', function () {

    return staticCtrl.slotAction({file: 'styles.css'}).then(() => {

      let cache = staticCache.get(basepath + 'web/styles.css') ;
      expect(staticCtrl.$response.getHeaders()).to.be.deep.equal({
        'content-type': 'text/css',
        'last-modified': cache.lastModified.toString(),
        etag: cache.etag,
        'set-cookie': []
      }) ;
    }) ;
  }) ;

  it('si changement date et/ou etag renvoit fichier', function () {

    request.headers['if-modified-since'] = 'Wed, 08 Nov 2017 08:45:33 +0000' ;
    request.headers['if-none-match'] = 'foobar' ;

    return staticCtrl.slotAction({file: 'styles.css'}).then(() => {

      expect(staticCtrl.$response._response.statusCode).to.be.equal(200) ;
      expect(spy.calledWith(basepath + 'web/styles.css')).to.be.true ;
    }) ;
  }) ;

  it('si pas changement date et etag envoit 304', function () {

    return staticCtrl.slotAction({file: 'styles.css'}).then(() => {

      return Promise.resolve(staticCache.get(basepath + 'web/styles.css')) ;
    }).then(cache => {

      request.headers['if-modified-since'] = cache.lastModified.toString() ;
      request.headers['if-none-match'] = cache.etag ;

      staticCtrl.$response = new Resp(new http.ServerResponse('get')) ;

      return staticCtrl.slotAction({file: 'styles.css'}).then(() => {

        expect(staticCtrl.$response.getHeaders()).to.be.deep.equal({
          'last-modified': cache.lastModified.toString(),
          etag: cache.etag,
          'set-cookie': []
        }) ;
        expect(staticCtrl.$response._response.statusCode).to.be.equal(304) ;
        expect(spy.calledOnce).to.be.true ;
      }) ;
    }) ;
  }) ;

  it('les cookies sont bien envoyés', function () {

    let user   = new User(request, staticCtrl.$response),
        cookie = ['kjs_session_id=' + user._sessionId + ';HttpOnly'] ;
    staticCtrl.$user = user ;

    return staticCtrl.slotAction({file: 'styles.css'}).then(() => {

      let cache = staticCache.get(basepath + 'web/styles.css') ;
      expect(staticCtrl.$response.getHeaders()).to.be.deep.equal({
        'content-type': 'text/css',
        'last-modified': cache.lastModified.toString(),
        etag: cache.etag,
        'set-cookie': cookie
      }) ;
    }) ;
  }) ;

  it('cookie également envoyé pour un fichier en cache', function () {

    return staticCtrl.slotAction({file: 'styles.css'}).then(() => {

      return Promise.resolve(staticCache.get(basepath + 'web/styles.css')) ;
    }).then(cache => {

      request.headers['if-modified-since'] = cache.lastModified.toString() ;
      request.headers['if-none-match'] = cache.etag ;

      staticCtrl.$response = new Resp(new http.ServerResponse('get')) ;

      let user   = new User(request, staticCtrl.$response),
          cookie = ['kjs_session_id=' + user._sessionId + ';HttpOnly'] ;
      staticCtrl.$user = user ;

      return staticCtrl.slotAction({file: 'styles.css'}).then(() => {

        expect(staticCtrl.$response.getHeaders()).to.be.deep.equal({
          'last-modified': cache.lastModified.toString(),
          etag: cache.etag,
          'set-cookie': cookie
        }) ;
      }) ;
    }) ;
  }) ;
}) ;

