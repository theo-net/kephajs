'use strict' ;

/**
 * src/server/Services/Renderer.test.js
 */

const expect = require('chai').expect ;

const Renderer = require('./Renderer') ;

let renderer = new Renderer() ;

describe('renderer', function () {

  it('code', function () {

    expect(renderer.code('test é < >', 'a'))
      .to.be.equal('<!-- a --><pre><code>\ntest é < >\n</code></pre>\n') ;
  }) ;

  it('blockquote', function () {

    expect(renderer.blockquote('test'))
      .to.be.equal('<blockquote>\ntest</blockquote>\n') ;
  }) ;

  it('html', function () {

    expect(renderer.html('test'))
      .to.be.equal('test') ;
  }) ;

  it('heading', function () {

    expect(renderer.heading('test', 1, 'Ok1!'))
      .to.be.equal('<h1 id="ok1-">test</h1>\n') ;
    expect(renderer.heading('test', 3, 'Ok1!')) // buddy ignore:line
      .to.be.equal('<h3 id="ok1-">test</h3>\n') ;
  }) ;

  it('list', function () {

    expect(renderer.list('test', false))
      .to.be.equal('<ul>\ntest</ul>\n') ;
    expect(renderer.list('test', true))
      .to.be.equal('<ol>\ntest</ol>\n') ;
  }) ;

  it('listItem', function () {

    expect(renderer.listItem('test'))
      .to.be.equal('  <li>test</li>\n') ;
  }) ;

  it('taskList', function () {

    expect(renderer.taskList('test', false))
      .to.be.equal('<ul>\ntest</ul>\n') ;
    expect(renderer.taskList('test', true))
      .to.be.equal('<ol>\ntest</ol>\n') ;
  }) ;

  it('taskListItem', function () {

    expect(renderer.taskListItem('test', false))
      .to.be.equal('  <li><input type="checkbox" disabled>test</li>\n') ;
    expect(renderer.taskListItem('test', true)).to.be.equal(
      '  <li><input type="checkbox" disabled checked>test</li>\n') ;
  }) ;

  it('paragraph', function () {

    expect(renderer.paragraph('test'))
      .to.be.equal('<p>test</p>\n') ;
  }) ;

  it('table', function () {

    expect(renderer.table('test', 'foobar')).to.be.equal(
      '<table>\n<thead>\ntest</thead>\n<tbody>\nfoobar</tbody>\n</table>\n') ;
  }) ;

  it('tablerow', function () {

    expect(renderer.tablerow('test'))
      .to.be.equal('  <tr>\ntest  </tr>\n') ;
  }) ;

  it('tablecell', function () {

    expect(renderer.tablecell('test', {header: true, align: 'right'}))
      .to.be.equal('    <th class="txtright">test</th>\n') ;
    expect(renderer.tablecell('test', {align: 'center'}))
      .to.be.equal('    <td class="txtcenter">test</td>\n') ;
    expect(renderer.tablecell('test', {}))
      .to.be.equal('    <td>test</td>\n') ;
  }) ;

  it('strong', function () {

    expect(renderer.strong('test'))
      .to.be.equal('<strong>test</strong>') ;
  }) ;

  it('em', function () {

    expect(renderer.em('test'))
      .to.be.equal('<em>test</em>') ;
  }) ;

  it('codespan', function () {

    expect(renderer.codespan('test'))
      .to.be.equal('<code>test</code>') ;
  }) ;

  it('br', function () {

    expect(renderer.br())
      .to.be.equal('<br>\n') ;
  }) ;

  it('del', function () {

    expect(renderer.del('test'))
      .to.be.equal('<del>test</del>') ;
  }) ;

  it('link', function () {

    expect(renderer.link('test', 'foobar', 'ok'))
      .to.be.equal('<a href="test" title="foobar">ok</a>') ;
    expect(renderer.link('test', '', 'ok'))
      .to.be.equal('<a href="test">ok</a>') ;
  }) ;

  it('text', function () {

    expect(renderer.text('test'))
      .to.be.equal('test') ;
  }) ;

  it('footNote', function () {

    expect(renderer.footNote(2))
      .to.be.equal('<a href="#note-2"><sup>2</sup></a>') ;
  }) ;
}) ;
