'use strict' ;

/**
 * src/server/Services/Forms.test.js
 */

const expect = require('chai').expect ;

const Forms         = require('./Forms'),
      Form          = require('../Form/Form'),
      FormBuilder   = require('../Form/FormBuilder'),
      StringField   = require('../Form/StringField'),
      EmailField    = require('../Form/EmailField'),
      PasswordField = require('../Form/PasswordField'),
      SearchField   = require('../Form/SearchField'),
      TelField      = require('../Form/TelField'),
      UrlField      = require('../Form/UrlField'),
      NumberField   = require('../Form/NumberField'),
      RangeField    = require('../Form/RangeField'),
      ColorField    = require('../Form/ColorField'),
      DateField     = require('../Form/DateField'),
      TimeField     = require('../Form/TimeField'),
      HiddenField   = require('../Form/HiddenField'),
      SubmitField   = require('../Form/SubmitField'),
      ButtonField   = require('../Form/ButtonField'),
      ResetField    = require('../Form/ResetField'),
      RadioField    = require('../Form/RadioField'),
      CheckboxField = require('../Form/CheckboxField'),
      TextField     = require('../Form/TextField'),
      SelectField   = require('../Form/SelectField'),
      HtmlField     = require('../Form/HtmlField'),
      ValidateS     = require('../../core/Services/Validate') ;

describe('server/Services/Forms', function () {

  let forms = null ;

  class TestFormBuilder extends FormBuilder {
    build (config) {
      this.getForm().config = config ;
      this.getForm().a = 'ok' ;
      this.getForm().add('string', {name: 'test', required: true})
                    .addFieldset() ;
    }
  }

  beforeEach(function () {

    forms = new Forms(new ValidateS()) ;
  }) ;

  it('on peut lui transmettre le service $validate', function () {

    let validate = {} ;
    forms = new Forms(validate) ;
    expect(forms.$validate).to.be.equal(validate) ;
  }) ;


  describe('register', function () {

    it('enregistre un form builder', function () {

      forms.register('test', TestFormBuilder) ;
      expect(forms._formBuilders['test']).to.be.equal(TestFormBuilder) ;
    }) ;

    it('retourne le service (pratique pour chaînage)', function () {

      expect(forms.register('test', TestFormBuilder)).to.be.equal(forms) ;
    }) ;
  }) ;


  describe('build', function () {

    it('retourne un form initialisé par le formBuilder', function () {

      forms.register('test', TestFormBuilder) ;
      let form = forms.build('test') ;

      expect(form).to.be.instanceOf(Form) ;
      expect(form.a).to.be.equal('ok') ;
    }) ;

    it('peut transmettre une entité', function () {

      forms.register('test', TestFormBuilder) ;
      let entity = {} ;
      let form = forms.build('test', entity) ;

      expect(form.getEntity()).to.be.equal(entity) ;
    }) ;

    it('le formulaire possède une référence au service', function () {

      forms.register('test', TestFormBuilder) ;
      let form = forms.build('test') ;

      expect(form.$forms).to.be.equal(forms) ;
    }) ;

    it('on peut spécifier classes à ajouter pour tous les formulaires créés',
    function () {

      forms.register('test', TestFormBuilder) ;
      expect(forms.addClass({
        field: 'a',
        label: 'b',
        group: 'c',
        groupSuccess: 'd',
        groupError: 'e',
        msg: 'f',
        msgHelp: 'g',
        msgError: 'h',
        fieldset: 'i'
      })).to.be.equal(forms) ;
      let form = forms.build('test') ;

      expect(form._fields[0].getClass()).to.be.equal('a') ;
      expect(form._fields[0].getLabelClass()).to.be.equal('b') ;
      expect(form._fields[0].getGroupClass()).to.be.equal('c') ;
      expect(form._fields[0].getMsgClass()).to.be.equal('f g') ;
      expect(form._fields[0].getMsgClass('help')).to.be.equal('f g') ;
      expect(form._fields[0].getMsgClass('error')).to.be.equal('f h') ;
      expect(form._fields[1].getClass()).to.be.equal('i') ;

      form._fields[0].isValid() ;
      expect(form._fields[0].getGroupClass()).to.be.equal('c e') ;
      form._fields[0].setValue('ok') ;
      form._fields[0].isValid() ;
      expect(form._fields[0].getGroupClass()).to.be.equal('c d') ;
    }) ;

    it('on peut transmettre des paramètres', function () {

      let config = {} ;
      forms.register('test', TestFormBuilder) ;
      let form = forms.build('test', null, config) ;

      expect(form.config).to.be.equal(config) ;
    }) ;
  }) ;


  describe('has', function () {

    it('returns true if the formBuilder is registered', function () {

      forms.register('test', TestFormBuilder) ;
      expect(forms.has('test')).to.be.equal(true) ;
    }) ;

    it('returns false if the formBuilder isn\'t registred', function () {

      forms.register('test', TestFormBuilder) ;
      expect(forms.has('test1')).to.be.equal(false) ;
    }) ;
  }) ;


  describe('delete', function () {

    it('delete a formBuilder', function () {

      forms.register('test', TestFormBuilder) ;
      expect(forms.has('test')).to.be.equal(true) ;
      forms.delete('test') ;
      expect(forms.has('test')).to.be.equal(false) ;
    }) ;
  }) ;


  describe('fields', function () {

    let field = {} ;

    it('enregistre un field disponible', function () {

      forms.registerField('test', field) ;
      expect(forms.hasField('test')).to.be.true ;
      expect(forms.getField('test')).to.be.equal(field) ;
    }) ;

    it('indique si un field est disponible', function () {

      forms.registerField('test', field) ;
      expect(forms.hasField('test')).to.be.true ;
      expect(forms.hasField('test2')).to.be.false ;
    }) ;

    it('supprime un field', function () {

      forms.registerField('test', field) ;
      expect(forms.hasField('test')).to.be.true ;
      forms.deleteField('test') ;
      expect(forms.hasField('test')).to.be.false ;
    }) ;

    it('envoit une erreur si un field demandé n\'existe pas', function () {

      expect(forms.getField.bind(forms, 'test')).to.throw(Error) ;
    }) ;

    it('fields disponibles par défaut', function () {

      expect(forms.hasField('string')).to.be.true ;
      expect(forms.getField('string')).to.be.equal(StringField) ;
      expect(forms.hasField('email')).to.be.true ;
      expect(forms.getField('email')).to.be.equal(EmailField) ;
      expect(forms.hasField('password')).to.be.true ;
      expect(forms.getField('password')).to.be.equal(PasswordField) ;
      expect(forms.hasField('search')).to.be.true ;
      expect(forms.getField('search')).to.be.equal(SearchField) ;
      expect(forms.hasField('tel')).to.be.true ;
      expect(forms.getField('tel')).to.be.equal(TelField) ;
      expect(forms.hasField('url')).to.be.true ;
      expect(forms.getField('url')).to.be.equal(UrlField) ;
      expect(forms.hasField('number')).to.be.true ;
      expect(forms.getField('number')).to.be.equal(NumberField) ;
      expect(forms.hasField('range')).to.be.true ;
      expect(forms.getField('range')).to.be.equal(RangeField) ;
      expect(forms.hasField('color')).to.be.true ;
      expect(forms.getField('color')).to.be.equal(ColorField) ;
      expect(forms.hasField('date')).to.be.true ;
      expect(forms.getField('date')).to.be.equal(DateField) ;
      expect(forms.hasField('time')).to.be.true ;
      expect(forms.getField('time')).to.be.equal(TimeField) ;
      expect(forms.hasField('hidden')).to.be.true ;
      expect(forms.getField('hidden')).to.be.equal(HiddenField) ;
      expect(forms.hasField('submit')).to.be.true ;
      expect(forms.getField('submit')).to.be.equal(SubmitField) ;
      expect(forms.hasField('button')).to.be.true ;
      expect(forms.getField('button')).to.be.equal(ButtonField) ;
      expect(forms.hasField('reset')).to.be.true ;
      expect(forms.getField('reset')).to.be.equal(ResetField) ;
      expect(forms.hasField('radio')).to.be.true ;
      expect(forms.getField('radio')).to.be.equal(RadioField) ;
      expect(forms.hasField('checkbox')).to.be.true ;
      expect(forms.getField('checkbox')).to.be.equal(CheckboxField) ;
      expect(forms.hasField('text')).to.be.true ;
      expect(forms.getField('text')).to.be.equal(TextField) ;
      expect(forms.hasField('select')).to.be.true ;
      expect(forms.getField('select')).to.be.equal(SelectField) ;
      expect(forms.hasField('html')).to.be.true ;
      expect(forms.getField('html')).to.be.equal(HtmlField) ;
    }) ;
  }) ;
}) ;

