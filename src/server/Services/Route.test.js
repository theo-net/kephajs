'use strict' ;

/**
 * src/server/Services/Route.test.js
 */

const expect  = require('chai').expect ;
const Route = require('./Route') ;

describe('server/Services/Route', function () {

  it('créé une route à partir des paramètres', function () {

    let route = new Route(
      'get', '/', 2,
      {a: '\\d', b: '\\w'}, {a: 1, b: 'a'},
      'www.theo-net.org'
    ) ;

    expect(route._method).to.be.equal('get') ;
    expect(route._pattern).to.be.equal('/') ;
    expect(route.getCallable()).to.be.equal(2) ;
    expect(route._defaults.a).to.be.equal(1) ;
    expect(route._defaults.b).to.be.equal('a') ;
    expect(route._requirements.a).to.be.equal('\\d') ;
    expect(route._requirements.b).to.be.equal('\\w') ;
    expect(route._domain).to.be.equal('www.theo-net.org') ;
  }) ;

  it('si `domain` est null, créé une valeur par défaut', function () {

    let route = new Route('get', '/', 2, null, null, null) ;
    expect(route._domain).to.be.equal('.*') ;

    route = new Route('get', '/', 2) ;
    expect(route._domain).to.be.equal('.*') ;
  }) ;

  it('si `defaults` est null, créé une valeur par défaut', function () {

    let route = new Route('get', '/', 2, null, null, null) ;
    expect(route._defaults).to.be.deep.equal({}) ;

    route = new Route('get', '/', 2) ;
    expect(route._defaults).to.be.deep.equal({}) ;
  }) ;

  it('si `requirements` est null, créé une valeur par défaut', function () {

    let route = new Route('get', '/', 2, null, null, null) ;
    expect(route._requirements).to.be.deep.equal({}) ;

    route = new Route('get', '/', 2) ;
    expect(route._requirements).to.be.deep.equal({}) ;
  }) ;

  it('dans `requirements` remplace `[uuid]`', function () {

    let route = new Route(
      'get', '/{a}{b}{c}', 2,
      {a: '\\d', b: '[uuid]', c: '\\d+_[uuid]_ok'}
    ) ;

    let uuid =
      '[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}' ;
    expect(route._patternRegExp).to.be.deep.equal(RegExp(
      '^/(\\d)(' + uuid + ')(\\d+_' + uuid + '_ok)$'
    )) ;
  }) ;


  describe('match', function () {

    it('parse uri, domain et method : si la route match, retourne un objet '
    + 'contenant la route, false sinon', function () {

      let route1 = new Route('get', '/index.html', 1) ;
      let route2 = new Route('get', '/foobar', 1, {}, {}, 'forum.domain..*') ;
      let route3 = new Route(['get', 'post'], '/index.html', 1) ;

      expect(route1.match('get', '/index.html', 'lol.fr').route)
        .to.be.equal(route1) ;
      expect(route1.match('gEt', '/index.html', 'lol.fr').route)
        .to.be.equal(route1) ;
      expect(route1.match('post', '/index.html', 'lol.fr'))
        .to.be.equal(false) ;
      expect(route1.match('get', '/inde.html', 'lol.fr'))
        .to.be.equal(false) ;
      expect(route2.match('get', '/foobar', 'forum.domain.fr').route)
        .to.be.equal(route2) ;
      expect(route2.match('get', '/foobar', 'lol.fr'))
        .to.be.equal(false) ;
      expect(route3.match('get', '/index.html', 'lol.fr').route)
        .to.be.equal(route3) ;
      expect(route3.match('gEt', '/index.html', 'lol.fr').route)
        .to.be.equal(route3) ;
      expect(route3.match('post', '/index.html', 'lol.fr').route)
        .to.be.equal(route3) ;
    }) ;

    it('retourne un objet avec la route et les vars trouvées', function () {

      let route = new Route('get',
        '/index-{var1}{?var2}.html', 2, {
          var1: '\\w+',
          var2: '-(\\w+)'
        }, {
          var1: 'foo',
          var2: 'bar',
          var3: 'lol'
        }
      ) ;

      let match1 = route.match('get', '/index.html', 'lol.fr') ;
      let match2 = route.match('get', '/index-test-shalom.html', 'lol.fr') ;
      let match3 = route.match('get', '/index-test.html', 'lol.fr') ;

      expect(match1).to.be.false ;
      expect(match2.route).to.be.equal(route) ;
      expect(match3.route).to.be.equal(route) ;
      expect(match2.vars).to.be.deep.equal({
        var1: 'test',
        var2: 'shalom',
        var3: 'lol'
      }) ;
      expect(match3.vars).to.be.deep.equal({
        var1: 'test',
        var2: 'bar',
        var3: 'lol'
      }) ;
    }) ;
  }) ;


  describe('unroute', function () {

    it('reconstruit une URI à partir des params', function () {

      let route = new Route('get', '/index-{var1}{?var2}.html', 2, {
        var1: '\\w+',
        var2: '-(\\w+)'
      }, {
        var1: 'foo',
        var2: 'bar',
        var3: 'lol'
      }) ;

      expect(route.unroute({var1: 'test', var2: 'shalom'}))
        .to.be.equal('/index-test-shalom.html') ;
    }) ;

    it('utilise si besoin les valeurs par défaut', function () {

      let route = new Route('get', '/index-{var1}{?var2}.html', 2, {
        var1: '\\w+',
        var2: '-(\\w+)'
      }, {
        var1: 'foo',
        var2: 'bar',
        var3: 'lol'
      }) ;

      expect(route.unroute({var1: 'test'}))
        .to.be.equal('/index-test-bar.html') ;
    }) ;
  }) ;


  describe('getCallable', function () {

    it('retourne le `callable`', function () {

      let callable = ['controller', 'action'] ;
      let route = new Route('get', '/', callable) ;

      expect(route.getCallable()).to.equal(callable) ;
    }) ;
  }) ;
}) ;

