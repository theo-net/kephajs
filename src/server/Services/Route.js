'use strict' ;

/**
 * src/server/Services/Route.js
 */

const Utils = require('../../cli/Utils') ;

/**
 * Une route utilisable par le routeur
 */
class Route {

  /**
   * Construit une route à partir de différents éléments.
   *
   * `methode` défini la ou les méthodes pour lesquelles cette route est valide
   * (get, post, delete, ...). Un `string` suffit pour une méthode, utilisez
   * un `array` pour plusieurs. Elles sont insensibles à la case.
   *
   * Le pattern de la route est une chaînes permettant de matcher la route :
   *     '/index.html'  // ne traitera que celle-ci
   *     '/{style}.css' // tous ce qui se termine par `.css` et définie une var
   * On nottera que dans le `pattern`, les `"."` sont systématiquement échapés.
   *
   * Les variables seront traitées et leur valeur retournées après le parsage.
   * Vous pouvez, avec des objets `nomDeLaVariable: valeur` définir certaintes
   * choses :
   *   - un objet `defaults` pour les valeurs par défaut (facultatif)
   *   - un objet `requirements` pour la RegExp correspondant à la valeur
   *
   * Une variable facultative commence par un `?`, ex : `{?facultatif}`
   *
   * On peut spécifier quelle partie de la variable récupérer (très pratique
   * pour certains patterns. On prend le pattern `/news-{var1}{var2}.html`
   * et on aura en `requirements` : `var1: \w+` et `var2: -(\d+)` ainsi
   * les URI `/news-a.html` et `/news-a-2.html` matcheront, mais `var2`
   * aura comme valeur `2` et non `-2`. Attention, il ne faut qu'une paire de
   * parenthèses.
   *
   * Mettre un élément dans `defaults` qui n'est pas présent dans le pattern et
   * ni dans le `requirements` est un bon moyen de forcer le passage d'une
   * valeur.
   *
   * `domain` est traité comme une RegExp.
   *
   * Échappez toutes les classes de caractères comme par exemple `\w` en
   * mettant `\\w`.
   *
   * Dans les `requirements`, `[uuid]` sera remplacé par la RegExp permettant
   * de tester si une chaîne est un UUID v4.
   *
   * @param {String|Array} method Méthode(s) de la route
   * @param {String} pattern Pattern de la route
   * @param {Array} callable Tableau ['controller', 'action'] ou [object] avec
   *   `object` l'objet dont on appelera la méthode exec()
   * @param {Object} [requirements={}] Patterns des variables
   * @param {Object} [defaults={}] Valeur par défaut des variables (nomVar: val)
   * @param {String} [domain='*'] Pattern du domain
   */
  constructor (method, pattern, callable, requirements, defaults, domain) {

    this._method = method ;
    this._pattern = pattern ;
    this._callable = callable ;
    this._requirements = requirements ? requirements : {} ;
    this._defaults = defaults ? defaults : {} ;
    this._domain = domain ? domain : '.*' ;

    if (Array.isArray(method))
      method = '(' + method.join(')|(') + ')' ;
    this._methodRegExp = new RegExp('^' + method + '$', 'i') ;
    this._domainRegExp = new RegExp('^' + this._domain + '$') ;

    // On échappe le pattern
    pattern = pattern.replace('.', '\\.') ;

    // On traite les variables
    this._varsName = [] ;
    this._patternRegExp = pattern ;
    let vars ;
    let regVars = /\{(\??\w+)\}/g ;
    while ((vars = regVars.exec(pattern)) !== null) {

      if (vars[1].substr(0,1) === '?')
        this._insertVar(vars[1].substr(1), true, vars[0]) ;
      else
        this._insertVar(vars[1], false, vars[0]) ;
    }
    this._patternRegExp = new RegExp('^' + this._patternRegExp + '$') ;
  }


  /**
   * Retourne le callable
   * @returns {Array}
   */
  getCallable () {

    return this._callable ;
  }


  /**
   * Parse une méthode, une URI et un domaine pour déterminer si la route match
   * @param {String} method Méthode
   * @param {String} uri URI
   * @param {String} domain Domaine
   * @returns {Boolean|Object} false si ne match pas, un objet sinon, avec :
   *    route: this, vars: variables
   */
  match (method, uri, domain) {

    let match ;

    if (this._methodRegExp.test(method) &&
      (match = this._patternRegExp.exec(uri)) &&
      this._domainRegExp.test(domain)) {

      match.shift() ;
      let vars = Utils.clone(this._defaults) ;
      match.forEach((val, index) => {
        if (val !== undefined)
          vars[this._varsName[index]] = val ;
      }) ;

      return {route: this, vars: vars} ;
    }

    return false ;
  }


  /**
   * Reconstruit une URL à partir de paramètres
   *
   * @param {Object} params Paramètres
   * @param {String} [domain = ''] Domaine courant (permettra de déterminé s'il y
   *  a besoin d'une URL absolue ou non
   * @param {Boolean} [https=false] L'URL absolue est-elle en HTTPS ?
   * @returns {String}
   */
  unroute (params, domain = '', https = false) {


    let url    = this._pattern ;

    // Corps de l'URL
    params = Utils.extend({}, this._defaults, params) ;
    Utils.forOwn(params, (val, name) => {

      if (/\(/.test(this._requirements[name]))
        val = this._requirements[name].replace(/\([^()]+\)/, val) ;

      url = url.replace('{?' + name + '}', val) ;
      url = url.replace('{' + name + '}', val) ;
    }) ;

    // URL absolue ?
    if (domain && !this._domainRegExp.exec(domain)) {

      let ext = /\.(\w*)(:\d+)?$/.exec(domain) ;

      ext = ext ? '.' + ext[1] + (ext[2] ? ext[2] : '') : '' ;

      domain = 'http' + (https ? 's' : '') + '://'
             + this._domain.replace('.*', ext) ;
    }
    else
      domain = '' ;


    return domain + url ;
  }


  /**
   * Insert la RegExp d'une variable
   * @param {String} name Nom de la variable
   * @param {Boolean} facultatif Facultatif ou non comme variable
   * @param {String} source Source
   * @private
   */
  _insertVar (name, facultatif, source) {

    this._varsName.push(name) ;

    let requirement = this._requirements[name].replace(
      '[uuid]',
      Utils.uuidRegExp().toString().slice(2, -3)
    ) ;

    this._patternRegExp = this._patternRegExp.replace(
      source,
      (/\(/.test(requirement) ? '(?:' :  '(')
      + requirement + (facultatif ? ')?' : ')')) ;
  }
}

module.exports = Route ;

