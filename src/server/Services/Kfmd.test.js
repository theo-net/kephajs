'use strict' ;

/**
 * src/server/Services/Kfmd.test.js
 */

const expect = require('chai').expect ;

const Kfmd = require('./Kfmd') ;

let kfmd ;

describe('Kfmd', function () {

  beforeEach(function () {

    kfmd = new Kfmd() ;
  }) ;

  it('on peut définir le renderer', function () {

    let renderer = {} ;
    kfmd.setRenderer(renderer) ;
    expect(kfmd._renderer).to.be.equal(renderer) ;
  }) ;


  describe('render', function () {

    it('pas de transformation, si aucune syntaxe spéciale', function () {

      let result = kfmd.render('un texte') ;

      expect(result.source).to.be.equal('un texte') ;
      expect(result.contentHtml).to.be.equal('<p>un texte</p>\n') ;
      expect(result.toc).to.be.deep.equal([]) ;
      expect(result.footNotes).to.be.deep.equal([]) ;
    }) ;

    describe('safeHtml', function () {

      it('elmt blocs ne peuvent pas êtres dans un paragraphe', function () {

        let result = [] ;
        [
          'une ligne\n\n<div>un bloc</div>\n\nune autre ligne',
          'une ligne\n<div>un bloc</div>\nune autre ligne',
          '<div>un bloc</div>\n\nune autre ligne',
          'une ligne\n\n<div>un bloc</div>',
          '<div>un bloc</div>',
          'avant <div>un bloc</div> après',
          '<div>un bloc</div> après',
          'avant <div>un bloc</div>'
        ].forEach(line => {
          result.push(kfmd.render(line).contentHtml) ;
        }) ;
        let expected = [
          '<p>une ligne</p>\n<div>un bloc</div>\n\n<p>une autre ligne</p>\n',
          '<p>une ligne\n&lt;div&gt;un bloc&lt;/div&gt;\nune autre ligne</p>\n',
          '<div>un bloc</div>\n\n<p>une autre ligne</p>\n',
          '<p>une ligne</p>\n<div>un bloc</div>',
          '<div>un bloc</div>',
          '<p>avant &lt;div&gt;un bloc&lt;/div&gt; après</p>\n',
          '<p>&lt;div&gt;un bloc&lt;/div&gt; après</p>\n',
          '<p>avant &lt;div&gt;un bloc&lt;/div&gt;</p>\n'
        ] ;

        expect(result).to.be.deep.equal(expected) ;
      }) ;

      it('les éléments inline sont bien repérés', function () {

        let result = [] ;
        [
          'une ligne\n\nUne <em>autre</em> !\n\nEt la dernière',
          'Là ça <em>passera</strong> même si le HTML est faux',
          'Et s\'il se comporte \n\n<em>comme</em>\n\n un bloc ?'
        ].forEach(line => {
          result.push(kfmd.render(line).contentHtml) ;
        }) ;
        let expected = [
          '<p>une ligne</p>\n<p>Une <em>autre</em> !</p>\n'
          + '<p>Et la dernière</p>\n',
          '<p>Là ça <em>passera</strong> même si le HTML est faux</p>\n',
          '<p>Et s’il se comporte </p>\n<p><em>comme</em></p>\n'
          + '<p> un bloc ?</p>\n'
        ] ;

        expect(result).to.be.deep.equal(expected) ;
      }) ;

      it('les balise peuvent contenir des attributs', function () {

        let result = kfmd.render(
          '<img href="image.jpg" alt="qqc" style="ok" />'
        ).contentHtml ;
        let expected =
          '<p><img href="image.jpg" alt="qqc" style="ok" /></p>\n' ;

        expect(result).to.be.equal(expected) ;
      }) ;

      it('un test d\'imbrication', function () {

        let result = kfmd.render(
          'Une ligne <em>au début</em> de notre texte\n\n'
          + '<table>\n  <tr>\n'
          + '    <td>une cellule <script>arnaque</script></td>\n'
          + '    <td>autre < script >arnaque</script></td>\n'
          + '  </tr>\n</table>\n\n'
          + '<figure>\n<img />\n</figure>\n\n'
          + 'C\'est finit !'
        ).contentHtml ;
        let expected = '<p>Une ligne <em>au début</em> de notre texte</p>\n'
          + '<table>\n  <tr>\n'
          + '    <td>une cellule &lt;script&gt;arnaque&lt;/script&gt;</td>\n'
          + '    <td>autre &lt; script &gt;arnaque&lt;/script&gt;</td>\n'
          + '  </tr>\n</table>\n\n'
          + '<figure>\n<img />\n</figure>\n\n'
          + '<p>C’est finit !</p>\n' ;

        expect(result).to.be.equal(expected) ;
      }) ;

      it('seuls certains éléments sont autorisés', function () {

        let result = [] ;
        [
          '<a>test</a>', '<em>test</em>', '<strong>test</strong>',
          '<small>test</small>', '<s>test</s>', '<cite>test</cite>',
          '<q>test</q>', '<dfn>test</dfn>','<abbr>test</abbr>',
          '<data>test</data>', '<time>test</time>', '<code>test</code>',
          '<var>test</var>', '<samp>test</samp>', '<kbd>test</kbd>',
          '<sub>test</sub>', '<sup>test</sup>', '<i>test</i>',
          '<b>test</b>', '<u>test</u>', '<mark>test</mark>',
          '<ruby>a <rt>b</rt> c <rp>d</rp> e</ruby>',
          '<bdi>test</bdi>', '<bdo>test</bdo>', '<span>test</span>',
          '<br>', '<br />', '<wbr>', '<wbr />',
          '<ins>test</ins>', '<del>test</del>',
          '<img>', '<img />',
          '<h1>test</h1>', '<h2>test</h2>', '<h3>test</h3>',
          '<h4>test</h4>', '<h5>test</h5>', '<h6>test</h6>',
          '<p>test</p>',
          '<ul><li>test</li></ul>',
          '<ol><li>test</li></ol>',
          '<table><thead><tr><th>test</th></tr></thead>'
          + '<tbody><tr><td>test</td></tr></tbody></table>',
          '<figure>test</figure>', '<caption>test</caption>',
          '<figcaption>test</figcaption>', '<div>',
          '<blockquote>test</blockquote>', '<footer>test</footer>',
          '<iframe>test</iframe>', '<dl>test</dl>', '<dd>test</dd>',
          '<dt>test</dt>'
        ].forEach(line => {
          result.push(kfmd.render(line).contentHtml) ;
        }) ;
        let expected = [
          '<p><a>test</a></p>\n',
          '<p><em>test</em></p>\n',
          '<p><strong>test</strong></p>\n',
          '<p><small>test</small></p>\n', '<p><s>test</s></p>\n',
          '<p><cite>test</cite></p>\n', '<p><q>test</q></p>\n',
          '<p><dfn>test</dfn></p>\n', '<p><abbr>test</abbr></p>\n',
          '<p><data>test</data></p>\n', '<p><time>test</time></p>\n',
          '<p><code>test</code></p>\n', '<p><var>test</var></p>\n',
          '<p><samp>test</samp></p>\n', '<p><kbd>test</kbd></p>\n',
          '<p><sub>test</sub></p>\n', '<p><sup>test</sup></p>\n',
          '<p><i>test</i></p>\n', '<p><b>test</b></p>\n',
          '<p><u>test</u></p>\n', '<p><mark>test</mark></p>\n',
          '<p><ruby>a <rt>b</rt> c <rp>d</rp> e</ruby></p>\n',
          '<p><bdi>test</bdi></p>\n', '<p><bdo>test</bdo></p>\n',
          '<p><span>test</span></p>\n', '<p><br></p>\n', '<p><br /></p>\n',
          '<p><wbr></p>\n', '<p><wbr /></p>\n',
          '<p><ins>test</ins></p>\n', '<p><del>test</del></p>\n',
          '<p><img></p>\n', '<p><img /></p>\n',
          '<h1>test</h1>', '<h2>test</h2>', '<h3>test</h3>',
          '<h4>test</h4>', '<h5>test</h5>', '<h6>test</h6>',
          '<p>test</p>', '<ul><li>test</li></ul>', '<ol><li>test</li></ol>',
          '<table><thead><tr><th>test</th></tr></thead>'
          + '<tbody><tr><td>test</td></tr></tbody></table>',
          '<figure>test</figure>', '<caption>test</caption>',
          '<figcaption>test</figcaption>', '<div>',
          '<blockquote>test</blockquote>', '<footer>test</footer>',
          '<iframe>test</iframe>', '<dl>test</dl>', '<dd>test</dd>',
          '<dt>test</dt>'
        ] ;

        expect(result).to.be.deep.equal(expected) ;
      }) ;

      it('les autres éléments seront échapés', function () {

        let result = [] ;
        [
          '<script>test</script>',
          '<style>test</style>',
          '<article>test</article>'
        ].forEach(line => {
          result.push(kfmd.render(line).contentHtml) ;
        }) ;
        let expected = [
          '<p>&lt;script&gt;test&lt;/script&gt;</p>\n',
          '<p>&lt;style&gt;test&lt;/style&gt;</p>\n',
          '<p>&lt;article&gt;test&lt;/article&gt;</p>\n'
        ] ;

        expect(result).to.be.deep.equal(expected) ;
      }) ;

      it('leur contenu ne sera pas interprété', function () {

        let result = [] ;
        [
          '<div>Salut __à__ toi !</div>',
          '<div>\n    & Su~~p~~er\n</div>'
        ].forEach(line => {
          result.push(kfmd.render(line).contentHtml) ;
        }) ;
        let expected = [
          '<div>Salut __à__ toi !</div>',
          '<div>\n    &amp; Su~~p~~er\n</div>'
        ] ;

        expect(result).to.be.deep.equal(expected) ;
      }) ;

      it('& est échapé mais on peut écrire &YYY;', function () {

        let result = [] ;
        [
          'Jésus&Co',
          'un & deux',
          'voilà&nbsp;:'
        ].forEach(line => {
          result.push(kfmd.render(line).contentHtml) ;
        }) ;
        let expected = [
          '<p>Jésus&amp;Co</p>\n',
          '<p>un &amp; deux</p>\n',
          '<p>voilà&nbsp;:</p>\n'
        ] ;

        expect(result).to.be.deep.equal(expected) ;
      }) ;

      it('> et < sont échapés', function () {

        let result = [] ;
        [
          'une ligne avec < ou >',
          '<< >>'
        ].forEach(line => {
          result.push(kfmd.render(line).contentHtml) ;
        }) ;
        let expected = [
          '<p>une ligne avec &lt; ou &gt;</p>\n',
          '<p>&lt;&lt; &gt;&gt;</p>\n'
        ] ;

        expect(result).to.be.deep.equal(expected) ;
      }) ;

      it('ajout d\'éléments inline', function () {

        expect(kfmd.render('a <foo>b</foo> c').contentHtml)
          .to.be.equal('<p>a &lt;foo&gt;b&lt;/foo&gt; c</p>\n') ;

        kfmd.addSafeInline('foo') ;

        expect(kfmd.render('a <foo>b</foo> c').contentHtml)
          .to.be.equal('<p>a <foo>b</foo> c</p>\n') ;
      }) ;

      it('suppression d\'éléments inline', function () {

        expect(kfmd.render('a <em>b</em> c').contentHtml)
          .to.be.equal('<p>a <em>b</em> c</p>\n') ;

        kfmd.removeSafeInline('em') ;

        expect(kfmd.render('a <em>b</em> c').contentHtml)
          .to.be.equal('<p>a &lt;em&gt;b&lt;/em&gt; c</p>\n') ;
      }) ;

      it('ajout d\'éléments block', function () {

        expect(kfmd.render('<foo>b</foo>').contentHtml)
          .to.be.equal('<p>&lt;foo&gt;b&lt;/foo&gt;</p>\n') ;

        kfmd.addSafeBlock('foo') ;

        expect(kfmd.render('<foo>b</foo>').contentHtml)
          .to.be.equal('<foo>b</foo>') ;
      }) ;

      it('suppression d\'éléments block', function () {

        expect(kfmd.render('<div>b</div>').contentHtml)
          .to.be.equal('<div>b</div>') ;

        kfmd.removeSafeBlock('div') ;

        expect(kfmd.render('<div>b</div>').contentHtml)
          .to.be.equal('<p>&lt;div&gt;b&lt;/div&gt;</p>\n') ;
      }) ;
    }) ;

    describe('éléments de bloc', function () {

      describe('paragraphes et sauts de ligne', function () {

        it('chaque paragraphe est séparé par une ligne vide', function () {

          let result = [] ;
          [
            'a\n\nb\n\nc',
            'a\nb',
            'a\n\n\nb\nc'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>a</p>\n<p>b</p>\n<p>c</p>\n',
            '<p>a\nb</p>\n',
            '<p>a</p>\n<p>b\nc</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('une ligne vide peut contenir des espaces ou tabulations',
        function () {

          let result = [] ;
          [
            'a\n  \nb',
            'a\n    \nb',
            'a\n\t\t\nb',
            'a\n\t \nb'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>a</p>\n<p>b</p>\n',
            '<p>a</p>\n<p>b</p>\n',
            '<p>a</p>\n<p>b</p>\n',
            '<p>a</p>\n<p>b</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('on peut forcer un retour à la ligne', function () {

          let result = [] ;
          [
            'a  \nb\n\nc',
            'a    \nb'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>a<br>\nb</p>\n<p>c</p>\n',
            '<p>a<br>\nb</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;
      }) ;

      describe('titres', function () {

        it('il y a six niveaux de titres', function () {

          let result = [] ;
          [
            '# titre éa1',
            '## titre',
            '### titre',
            '#### titre',
            '##### titre',
            '###### titre',
            'a\n\n# titre\n\nb'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<h1 id="titre-a1">titre éa1</h1>\n',
            '<h2 id="titre">titre</h2>\n',
            '<h3 id="titre">titre</h3>\n',
            '<h4 id="titre">titre</h4>\n',
            '<h5 id="titre">titre</h5>\n',
            '<h6 id="titre">titre</h6>\n',
            '<p>a</p>\n<h1 id="titre">titre</h1>\n<p>b</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('il doit y avoir au moins un espace entre le titre et #',
        function () {

          let result = [] ;
          [
            ' ##titre',
            '   ###titre'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p> ##titre</p>\n',
            '<p>   ###titre</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('on peut terminer le titre par un ou plusieurs #', function () {

          let result = [] ;
          [
            '## titre #',
            '### titre ###',
            '# titre #######'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<h2 id="titre">titre</h2>\n',
            '<h3 id="titre">titre</h3>\n',
            '<h1 id="titre">titre</h1>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('alternative pour h1', function () {

          let result = [] ;
          [
            'titre\n=====',
            'titre\n==',
            'titre\n=',
            'titre\n==========',
            'titre\n ====='
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<h1 id="titre">titre</h1>\n',
            '<h1 id="titre">titre</h1>\n',
            '<p>titre\n=</p>\n',
            '<h1 id="titre">titre</h1>\n',
            '<h1 id="titre">titre</h1>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('alternative pour h2', function () {

          let result = [] ;
          [
            'titre\n-----',
            'titre\n--',
            'titre\n-',
            'titre\n----------',
            'titre\n -----'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<h2 id="titre">titre</h2>\n',
            '<h2 id="titre">titre</h2>\n',
            '<p>titre\n-</p>\n',
            '<h2 id="titre">titre</h2>\n',
            '<h2 id="titre">titre</h2>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('une toc est générée', function () {

          let result = [] ;
          [
            '# titre1\n\na\n\n# titre2',
            '# titre 1\n\n## titre 1.1\n\n## titre 1.2'
          ].forEach(line => {
            result.push(kfmd.render(line).toc) ;
          }) ;
          let expected = [
            [
              {type: 1, content: 'titre1'},
              {type: 1, content: 'titre2'}
            ],
            [
              {type: 1, content: 'titre 1'},
              {type: 2, content: 'titre 1.1'},
              {type: 2, content: 'titre 1.2'}
            ]
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('toc fonctionnelle même si liste/tasklist et blockquote',
        function () {

          let result = [] ;
          [
            '# titre1\n\n- a\n- b\n\n# titre2',
            '# titre1\n\n- [x] a\n- [] b\n\n# titre2',
            '# titre1\n\n> quote\n\n# titre2'
          ].forEach(line => {
            result.push(kfmd.render(line).toc) ;
          }) ;
          let expected = [
            [
              {type: 1, content: 'titre1'},
              {type: 1, content: 'titre2'}
            ],
            [
              {type: 1, content: 'titre1'},
              {type: 1, content: 'titre2'}
            ],
            [
              {type: 1, content: 'titre1'},
              {type: 1, content: 'titre2'}
            ]
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;
      }) ;

      describe('citations', function () {

        it('les blocs de citations débutent par >', function () {

          let result = [] ;
          [
            '> citation ligne 1\n> citation ligne 2',
            '>ligne 1\n>ligne 2',
            '> citation',
            ' > citation',
            'a\n\n>b'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<blockquote>\n<p>citation ligne 1\ncitation ligne 2</p>\n'
            + '</blockquote>\n',
            '<blockquote>\n<p>ligne 1\nligne 2</p>\n</blockquote>\n',
            '<blockquote>\n<p>citation</p>\n</blockquote>\n',
            '<blockquote>\n<p>citation</p>\n</blockquote>\n',
            '<p>a</p>\n<blockquote>\n<p>b</p>\n</blockquote>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('On peut imbriquer les blocs', function () {

          let result = [] ;
          [
            '> citation\n> > sous citation'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<blockquote>\n'
            + '<p>citation</p>\n'
            + '<blockquote>\n'
            + '<p>sous citation</p>\n'
            + '</blockquote>\n'
            + '</blockquote>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('Un seul > est nécessaire par paragraphe', function () {

          let result = [] ;
          [
            '>ligne 1\nligne 2'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<blockquote>\n<p>ligne 1\nligne 2</p>\n</blockquote>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('Le contenu est formaté', function () {

          let result = [] ;
          [
            '> une __citation__',
            '> une *autre*'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<blockquote>\n<p>une <strong>citation</strong></p>\n'
            + '</blockquote>\n',
            '<blockquote>\n<p>une <em>autre</em></p>\n</blockquote>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;
      }) ;

      describe('listes', function () {

        it('liste non ordonnée avec *, + ou -', function () {

          let result = [] ;
          [
            'a\n\n* b\n* c\n* d\n\ne',
            'a\n\n+ b\n+ c\n+ d\n\ne',
            'a\n\n- b\n- c\n- d\n\ne',
            'a\n\n* b\n+ c\n- d\n\ne'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>a</p>\n<ul>\n  <li>b</li>\n  <li>c</li>\n  '
            + '<li>d</li>\n</ul>\n<p>e</p>\n',
            '<p>a</p>\n<ul>\n  <li>b</li>\n  <li>c</li>\n  '
            + '<li>d</li>\n</ul>\n<p>e</p>\n',
            '<p>a</p>\n<ul>\n  <li>b</li>\n  <li>c</li>\n  '
            + '<li>d</li>\n</ul>\n<p>e</p>\n',
            '<p>a</p>\n<ul>\n  <li>b</li>\n</ul>\n<ul>\n'
            + '  <li>c</li>\n</ul>\n<ul>\n'
            + '  <li>d</li>\n</ul>\n<p>e</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('liste ordonnée', function () {

          let result = [] ;
          [
            'a\n\n1. b\n2. c\n3. d\n\ne',
            'a\n\n2. b\n3. c\n4. d\n\ne',
            'a\n\n3. b\n2. c\n1. d\n\ne',
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>a</p>\n<ol>\n  <li>b</li>\n  <li>c</li>\n  '
            + '<li>d</li>\n</ol>\n<p>e</p>\n',
            '<p>a</p>\n<ol>\n  <li>b</li>\n  <li>c</li>\n  '
            + '<li>d</li>\n</ol>\n<p>e</p>\n',
            '<p>a</p>\n<ol>\n  <li>b</li>\n  <li>c</li>\n  '
            + '<li>d</li>\n</ol>\n<p>e</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('listes imbriquées', function () {

          let result = [] ;
          [
            '- a\n  - b\n  - c\n- d',
            '1. a\n  1. b\n  2. c\n2. d',
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<ul>\n  <li>a<ul>\n  <li>b</li>\n  <li>c</li>\n</ul>\n'
            + '</li>\n  <li>d</li>\n</ul>\n',
            '<ol>\n  <li>a<ol>\n  <li>b</li>\n  <li>c</li>\n</ol>\n'
            + '</li>\n  <li>d</li>\n</ol>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('indentation possible des marqueurs de liste', function () {

          let result = [] ;
          [
            ' * a\n * b',
            '  * a\n  * b',
            '   * a\n   * b',
            '    * a\n    * b',
            ' - a\n - b',
            ' + a\n + b',
            ' 1. a\n 1. b'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<ul>\n  <li>a</li>\n  <li>b</li>\n</ul>\n',
            '<ul>\n  <li>a</li>\n  <li>b</li>\n</ul>\n',
            '<ul>\n  <li>a</li>\n  <li>b</li>\n</ul>\n',
            '<pre><code>\n* a\n* b\n</code></pre>\n',
            '<ul>\n  <li>a</li>\n  <li>b</li>\n</ul>\n',
            '<ul>\n  <li>a</li>\n  <li>b</li>\n</ul>\n',
            '<ol>\n  <li>a</li>\n  <li>b</li>\n</ol>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('marqueur suivi par des espaces ou tab', function () {

          let result = [] ;
          [
            '*  a\n*  b',
            '+  a\n+  b',
            '-  a\n-  b',
            '*\ta\n*\tb',
            '1.  a\n2.  b',
            '1.\ta\n2.\tb',
            '*a\n*b',
            '1.a\n2.b'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<ul>\n  <li>a</li>\n  <li>b</li>\n</ul>\n',
            '<ul>\n  <li>a</li>\n  <li>b</li>\n</ul>\n',
            '<ul>\n  <li>a</li>\n  <li>b</li>\n</ul>\n',
            '<ul>\n  <li>a</li>\n  <li>b</li>\n</ul>\n',
            '<ol>\n  <li>a</li>\n  <li>b</li>\n</ol>\n',
            '<ol>\n  <li>a</li>\n  <li>b</li>\n</ol>\n',
            '<p><em>a\n</em>b</p>\n',
            '<p>1.a\n2.b</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('liste de tâches', function () {

          let result = [] ;
          [
            '- [] a\n- [x] b',
            ' - [] a\n - [x] b',
            '* [] a\n* [x] b',
            '+ [] a\n+ [x] b'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<ul>\n  '
            + '<li><input type="checkbox" disabled>a</li>\n  '
            + '<li><input type="checkbox" disabled checked>b</li>\n'
            + '</ul>\n',
            '<ul>\n  '
            + '<li><input type="checkbox" disabled>a</li>\n  '
            + '<li><input type="checkbox" disabled checked>b</li>\n'
            + '</ul>\n',
            '<ul>\n  '
            + '<li><input type="checkbox" disabled>a</li>\n  '
            + '<li><input type="checkbox" disabled checked>b</li>\n'
            + '</ul>\n',
            '<ul>\n  '
            + '<li><input type="checkbox" disabled>a</li>\n  '
            + '<li><input type="checkbox" disabled checked>b</li>\n'
            + '</ul>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('élément sur plusieurs lignes', function () {

          let result = [] ;
          [
            '- ligne 1\nligne 2\nligne 3\n- autre item',
            ' - ligne 1\n   ligne 2\n   ligne 3\n - autre item'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<ul>\n  <li>ligne 1\nligne 2\nligne 3</li>\n'
            + '  <li>autre item</li>\n</ul>\n',
            '<ul>\n  <li>ligne 1\nligne 2\nligne 3</li>\n'
            + '  <li>autre item</li>\n</ul>\n',
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;
      }) ;

      describe('code', function () {

        it('code inline', function () {

          let result = [] ;
          [
            'tapez `du code` c\'est bien',
            '`du code`'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>tapez <code>du code</code> c’est bien</p>\n',
            '<p><code>du code</code></p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('bloc de code avec ```', function () {

          let result = [] ;
          [
            '```\ndu\n  code\n```',
            '  ```\n  du\n    code\n```',
            '``` \ncode\n``` '
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<pre><code>\ndu\n  code\n</code></pre>\n',
            '<pre><code>\n  du\n    code\n</code></pre>\n',
            '<pre><code>\ncode\n</code></pre>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('bloc de code par indentation', function () {

          let result = [] ;
          [
            '    du\n      code',
            '      du\n      code',
            '    code'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<pre><code>\ndu\n  code\n</code></pre>\n',
            '<pre><code>\n  du\n  code\n</code></pre>\n',
            '<pre><code>\ncode\n</code></pre>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('tous les & < > transformés', function () {

          let result = [] ;
          [
            '`& moi &amp; <em>cool</em>`',
            '```\n& moi &amp; <em>cool</em>\n```',
            '    & moi &amp; <em>cool</em>'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p><code>&amp; moi &amp;amp; &lt;em&gt;cool&lt;/em&gt;'
            + '</code></p>\n',
            '<pre><code>\n&amp; moi &amp;amp; '
            + '&lt;em&gt;cool&lt;/em&gt;\n</code></pre>\n',
            '<pre><code>\n&amp; moi &amp;amp; '
            + '&lt;em&gt;cool&lt;/em&gt;\n</code></pre>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;
      }) ;

      describe('tableaux', function () {

        it('syntaxe tableau', function () {

          let result = [] ;
          [
            '|A|B|C|\n|---|---|---|\n|D|E|F|',
            '| A | B | C |\n| --- | --- | --- |\n| D | E | F |',
            '| A | B |\n|---|---|\n| C | D |'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<table>\n'
            + '<thead>\n'
            + '  <tr>\n    <th>A</th>\n    <th>B</th>\n'
            + '    <th>C</th>\n  </tr>\n'
            + '</thead>\n<tbody>\n'
            + '  <tr>\n    <td>D</td>\n    <td>E</td>\n'
            + '    <td>F</td>\n  </tr>\n'
            + '</tbody>\n'
            + '</table>\n',
            '<table>\n'
            + '<thead>\n'
            + '  <tr>\n    <th>A</th>\n    <th>B</th>\n'
            + '    <th>C</th>\n  </tr>\n'
            + '</thead>\n<tbody>\n'
            + '  <tr>\n    <td>D</td>\n    <td>E</td>\n'
            + '    <td>F</td>\n  </tr>\n'
            + '</tbody>\n'
            + '</table>\n',
            '<table>\n'
            + '<thead>\n'
            + '  <tr>\n    <th>A</th>\n    <th>B</th>\n  </tr>\n'
            + '</thead>\n<tbody>\n'
            + '  <tr>\n    <td>C</td>\n    <td>D</td>\n  </tr>\n'
            + '</tbody>\n'
            + '</table>\n',
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('Le séparateur de header min 2? -', function () {

          let result = [] ;
          [
            '|A|B|\n|-|-|\n|C|D|',
            '| abcd | efgh |\n|-----|----|\n|1|2|'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<table>\n<thead>\n'
            + '  <tr>\n    <th>A</th>\n    <th>B</th>\n  </tr>\n'
            + '</thead>\n<tbody>\n'
            + '  <tr>\n    <td>C</td>\n    <td>D</td>\n  </tr>\n'
            + '</tbody>\n</table>\n',
            '<table>\n<thead>\n'
            + '  <tr>\n    <th>abcd</th>\n    <th>efgh</th>\n  </tr>\n'
            + '</thead>\n<tbody>\n'
            + '  <tr>\n    <td>1</td>\n    <td>2</td>\n  </tr>\n'
            + '</tbody>\n</table>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('syntaxe à l\'intérieur', function () {

          let result = [] ;
          [
            '|A|B|\n|---|---|\n|*A*|__B__|',
            '|A|B|\n|-|-|\n|a`b`c|d|'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<table>\n<thead>\n'
            + '  <tr>\n    <th>A</th>\n    <th>B</th>\n  </tr>\n'
            + '</thead>\n<tbody>\n'
            + '  <tr>\n    <td><em>A</em></td>\n'
            + '    <td><strong>B</strong></td>\n  </tr>\n'
            + '</tbody>\n</table>\n',
            '<table>\n<thead>\n'
            + '  <tr>\n    <th>A</th>\n    <th>B</th>\n  </tr>\n'
            + '</thead>\n<tbody>\n'
            + '  <tr>\n    <td>a<code>b</code>c</td>\n    <td>d</td>\n  </tr>\n'
            + '</tbody>\n</table>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('alignement', function () {

          let result = [] ;
          [
            '|A|B|C|\n|:---|:---:|---:|\n|E|F|G',
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<table>\n<thead>\n'
            + '  <tr>\n'
            + '    <th class="txtleft">A</th>\n'
            + '    <th class="txtcenter">B</th>\n'
            + '    <th class="txtright">C</th>\n'
            + '  </tr>\n'
            + '</thead>\n<tbody>\n'
            + '  <tr>\n'
            + '    <td class="txtleft">E</td>\n'
            + '    <td class="txtcenter">F</td>\n'
            + '    <td class="txtright">G</td>\n'
            + '  </tr>\n'
            + '</tbody>\n</table>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('tableau mal construit', function () {

          let result = [] ;
          [
            '|A|B|\n|C|D|',
            '|A|B|\n|C|D|E|',
            '|A|B|\n|---|...|\n|C|D|'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>|A|B|\n|C|D|</p>\n',
            '<p>|A|B|\n|C|D|E|</p>\n',
            '<p>|A|B|\n|—|…|\n|C|D|</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;
      }) ;
    }) ;

    describe('éléments de texte', function () {

      describe('liens', function () {

        it('remplacement automatique', function () {

          let result = [] ;
          [
            'Il y a des liens http://www.theo-net.org ici',
            'https://www.theo-net.org',
            'www.theo-net.org',
            'https://www.theo-net.org/index.html',
            'https://www.theo-net.org/index.html?page=1&amp;foo=bar',
            'Mon email truc@bidule.net !'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>Il y a des liens '
            + '<a href="http://www.theo-net.org">http://www.theo-net.org</a>'
            + ' ici</p>\n',
            '<p><a href="https://www.theo-net.org">'
            + 'https://www.theo-net.org</a></p>\n',
            '<p><a href="http://www.theo-net.org">'
            + 'www.theo-net.org</a></p>\n',
            '<p><a href="https://www.theo-net.org/index.html">'
            + 'https://www.theo-net.org/index.html</a></p>\n',
            '<p>'
            + '<a href="https://www.theo-net.org/index.html?page=1&amp;foo=bar">'
            + 'https://www.theo-net.org/index.html?page=1&amp;foo=bar</a></p>\n',
            '<p>Mon email <a href="mailto:truc@bidule.net">'
            + 'truc@bidule.net</a> !</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('syntaxe spécifique', function () {

          let result = [] ;
          [
            'Allez [ici](https://theo-net.org "venez") !',
            '[là](index.html "ok")',
            '[mi*li*eu](truc)'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>Allez <a href="https://theo-net.org" title="venez">ici</a> !</p>\n',
            '<p><a href="index.html" title="ok">là</a></p>\n',
            '<p><a href="truc">mi<em>li</em>eu</a></p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('titre facultatif', function () {

          let result = [] ;
          [
            'Un [lien](truc.html)',
            '[un autre](https://theo-net.org)'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>Un <a href="truc.html">lien</a></p>\n',
            '<p><a href="https://theo-net.org">un autre</a></p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;
      }) ;

      describe('emphase', function () {

        it('emphase faible', function () {

          let result = [] ;
          [
            'Du _texte_ en emphase',
            'Du *texte* en emphase',
            'Au mi*li*eu'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>Du <em>texte</em> en emphase</p>\n',
            '<p>Du <em>texte</em> en emphase</p>\n',
            '<p>Au mi<em>li</em>eu</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('emphase forte', function () {

          let result = [] ;
          [
            'Du __texte__ en emphase',
            'Du **texte** en emphase',
            'Au mi**li**eu'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>Du <strong>texte</strong> en emphase</p>\n',
            '<p>Du <strong>texte</strong> en emphase</p>\n',
            '<p>Au mi<strong>li</strong>eu</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('texte barré', function () {

          let result = [] ;
          [
            'Du ~~texte~~ barré',
            'Au mi~~li~~eu'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>Du <del>texte</del> barré</p>\n',
            '<p>Au mi<del>li</del>eu</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('imbrication possible', function () {

          let result = [] ;
          [
            'Du ~~**_texte_**~~ !',
            'Un *texte __super__ cool*',
            'Il y _a un [lien](https://lien)_',
            'Et *là **aussi** !*'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>Du <del><strong><em>texte</em></strong></del> !</p>\n',
            '<p>Un <em>texte <strong>super</strong> cool</em></p>\n',
            '<p>Il y <em>a un <a href="https://lien">lien</a></em></p>\n',
            '<p>Et <em>là <strong>aussi</strong> !</em></p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('marqueurs colés au texte', function () {

          let result = [] ;
          [
            'Ne _ fonctionnera _ pas',
            'Ne ~~ fonctionnera ~~ pas'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>Ne _ fonctionnera _ pas</p>\n',
            '<p>Ne ~~ fonctionnera ~~ pas</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('peut être sur plusieurs lignes', function () {

          let result = [] ;
          [
            'Sur *plusieurs\nlignes* !',
            'Sur **plusieurs\nlignes** !',
            'Sur _plusieurs\nlignes_ !',
            'Sur __plusieurs\nlignes__ !',
            'Sur ~~plusieurs\nlignes~~ !'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>Sur <em>plusieurs\nlignes</em> !</p>\n',
            '<p>Sur <strong>plusieurs\nlignes</strong> !</p>\n',
            '<p>Sur <em>plusieurs\nlignes</em> !</p>\n',
            '<p>Sur <strong>plusieurs\nlignes</strong> !</p>\n',
            '<p>Sur <del>plusieurs\nlignes</del> !</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('doit être dans le même paragraphe', function () {

          let result = [] ;
          [
            'Ne peut *pas être\n\nsur deux* paragraphes',
            'Ne peut **pas être\n\nsur deux** paragraphes',
            'Ne peut _pas être\n\nsur deux_ paragraphes',
            'Ne peut __pas être\n\nsur deux__ paragraphes',
            'Ne peut ~~pas être\n\nsur deux~~ paragraphes'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>Ne peut *pas être</p>\n<p>sur deux* paragraphes</p>\n',
            '<p>Ne peut **pas être</p>\n<p>sur deux** paragraphes</p>\n',
            '<p>Ne peut _pas être</p>\n<p>sur deux_ paragraphes</p>\n',
            '<p>Ne peut __pas être</p>\n<p>sur deux__ paragraphes</p>\n',
            '<p>Ne peut ~~pas être</p>\n<p>sur deux~~ paragraphes</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;
      }) ;

      describe('notes', function () {

        it('notes de bas de page', function () {

          let result = [] ;
          [
            'Une note((avec son texte)), ok',
            'note((première)) note((deuxième))',
            'note((première))\n\nnote((deuxième))',
            'note((texte sur\nplusieurs lignes))',
            'pas((sur\n\nplusieurs)) paragraphes'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>Une note<a href="#note-1"><sup>1</sup></a>, ok</p>\n',
            '<p>note<a href="#note-1"><sup>1</sup></a> '
            + 'note<a href="#note-2"><sup>2</sup></a></p>\n',
            '<p>note<a href="#note-1"><sup>1</sup></a></p>\n'
            + '<p>note<a href="#note-2"><sup>2</sup></a></p>\n',
            '<p>note<a href="#note-1"><sup>1</sup></a></p>\n',
            '<p>pas((sur</p>\n<p>plusieurs)) paragraphes</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('on recupère le contenu des notes', function () {

          let result = kfmd.render(
            'salut((à toi)) et à bientôt((j\'espère)) !'
          ).footNotes ;
          let expected = [
            'à toi',
            'j’espère'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('ok, même si list/taskList/blockquote', function () {

          let result = [] ;
          [
            'a((b))\n\n- a\n- b\n\nc((d))',
            'a((b))n\n- [x] a\n- [] b\n\nc((d))',
            'a((b))\n\n> quote\n\nc((d))'
          ].forEach(line => {
            result.push(kfmd.render(line).footNotes) ;
          }) ;
          let expected = [
            ['b', 'd'], ['b', 'd'], ['b', 'd']
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;

        it('La syntaxe fonctionne dans les notes', function () {

          let result = kfmd.render(
            'ab((c**d**e)) f(([g](truc.html).)) !'
          ).footNotes ;
          let expected = [
            'c<strong>d</strong>e',
            '<a href="truc.html">g</a>.'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;
      }) ;
    }) ;

    describe('divers', function () {

      describe('échappement par barre oblique inverse', function () {

        it('On peut échapper des caractères spéciaux', function () {

          let result = [] ;
          [
            'salut à \\\\*toi*',
            'salut \\`oui\\`',
            'hello \\*world\\*',
            'hello \\_world\\_',
            'hello \\**world*\\*',
            '\\- oui',
            '1\\. ok',
            '\\# A',
            '\\+ A',
            '\\[a\\]\\(b\\)'
          ].forEach(line => {
            result.push(kfmd.render(line).contentHtml) ;
          }) ;
          let expected = [
            '<p>salut à \\<em>toi</em></p>\n',
            '<p>salut `oui`</p>\n',
            '<p>hello *world*</p>\n',
            '<p>hello _world_</p>\n',
            '<p>hello *<em>world</em>*</p>\n',
            '<p>- oui</p>\n',
            '<p>1. ok</p>\n',
            '<p># A</p>\n',
            '<p>+ A</p>\n',
            '<p>[a](b)</p>\n'
          ] ;

          expect(result).to.be.deep.equal(expected) ;
        }) ;
      }) ;
    }) ;
  }) ;
}) ;

