'use strict' ;

/**
 * src/server/Services/Router.js
 */

const Route        = require('./Route'),
      RouteError   = require('./RouteError'),
      Utils        = require('../../cli/Utils'),
      KernelAccess = require('../../core/KernelAccess') ;

/**
 * Routeur du serveur : il est capable de parcourir une série de routes afin de
 * determiner quelles ressources servir à partir de la requête. Il est
 * également capable de reconstruire une URL.
 */
class Router extends KernelAccess {

  /**
   * Initialise le service.
   *
   * @param {String} basepath Chemin de base pour toutes les ressources chargées
   */
  constructor (basepath = './') {

    super() ;

    this._routes = {} ;
    this._files = [] ;
    this._basepath = basepath ;
  }


  /**
   * Parse un objet de configuration ou un fichier JSON.
   * L'objet doit avoir la structure suivante :
   *    {
   *      "leNom": {
   *        "ressource": "path",
   *        "prefix": "le préfixe",
   *        "domain": "un domain"
   *      }
   *      "unAutre": {
   *        "method": "get",
   *        "pattern": "un pattern",
   *        "domain": "le domaine à parser",
   *        "controller": "un controller",
   *        "action": "une action",
   *        "defaults": {
   *          "nom de la variable": "valeur"
   *        },
   *        "requirements": {
   *          'nom de la variable": "un pattern"
   *        }
   *      }
   *    }
   *
   * `method` peut aussi contenir une série de méthode :
   *    "method": ["get", "post"]
   *
   * Chaque route doit avoir un identifiant unique.
   *
   * Les routes de ressources permettent d'inclure les routes d'un autre
   * fichier. Dans ce cas le nom des routes sera préfixé par le nom de la
   * ressource dont la première lettre sera forcée en majuscule (ici `leNom`,
   * dont si une route de la ressource s'appelle `foobar`, son nom deviendra
   * `leNomFoobar`) et leur pattern par la valeur de `prefix` sauf si
   * celui-ci est précisé lors du chargement de la ressource, auquel cas il
   * servira de préfixe. Enfin, si `domain` est précisé, toutes les routes
   * inclues auront ce pattern de domaine par défaut. Le chemin de la
   * ressource doit toujours être donné relativement au `basepath` défini
   * lors de l'initialisation du service. On peut également spécifier des
   * variables par défauts qui seront associées à chaque route incluse.
   *
   * Pour plus de détails, regardez l'objet Route.
   *
   * Attention, Les classes de caractères, `\d \w \s ...` doivent être
   * échappée : `\\d \\w \\s ...`
   *
   * Attention, les routes seront examinées dans l'ordre du fichier et dans
   * l'ordre d'inclusion !
   *
   * @param {Object|String} routes Objet contenant les routes ou chemin du
   *                               fichier (donné par rapport u absepath)
   * @param {String} [prefix] Préfixe qui sera ajouté au pattern de toutes les
   *                          routes
   * @param {String} [basename] Préfixe qui sera ajouté à l'identifiant de
   *                            toutes les routes
   * @param {String} [domain] Pattern de domaine par défaut
   * @param {String} [defaults] Valeur par défaut des variables (chaque route
   *                            étend cette liste
   * @return {undefinned}
   */
  load (routes, prefix = '', basename = '', domain = '', defaults) {

    defaults = defaults ? defaults : {} ;

    // C'est une chaîne de caractères : on charge le fichier et on retransmet
    // le contenu à la méthode.
    if (Utils.isString(routes)) {

      this._files.push({
        file: this._basepath + routes,
        prefix: prefix,
        basename: basename
      }) ;

      return this.load(
        require(this._basepath + routes),
        prefix, basename, domain, defaults
      ) ;
    }

    // Sinon on traite l'objet
    Utils.forEach(routes, (route, name) => {

      name = basename ?
        basename + name.toUpperCase().charAt(0) + name.substring(1)
        : name ;

      if (Object.keys(this._routes).indexOf(name) < 0) {

        // Ressource
        if (route.ressource) {

          let pref = prefix ;

          if (route.pattern || route.pattern == '')
            pref += route.pattern ;
          else
            pref += route.prefix ? route.prefix : '' ;

          this.load(route.ressource, pref, name,
            route.domain ? route.domain : domain, route.defaults) ;
        }
        // Router
        else {

          route.defaults = route.defaults ? route.defaults : {} ;

          this._routes[name] = new Route(
            route.method,
            (prefix ? prefix : '') + route.pattern,
            route.action ? [route.controller, route.action]
              : [route.controller],
            route.requirements,
            Utils.extend({}, defaults, route.defaults),
            route.domain ? route.domain : domain
          ) ;
        }
      }
    }) ;
  }


  /**
   * Cherche la bonne route
   *
   * @param {String} method Méthode de la requête
   * @param {String} uri URI de la requête
   * @param {String} domain Domaine de la requête
   * @returns {Object} Contenant deux éléments : `routes` et `vars` avec la
   *    route trouvée et les variables extraites de la requête.
   * @throws {RouteError} Si aucune route n'est trouvée
   */
  getRoute (method, uri, domain) {

    let goodRoute = false ;

    Utils.forEach(this._routes, route => {

      if ((route = route.match(method, uri, domain))) {

        this.kernel().events().notify('Router', 'Route pour `'
          + method.toUpperCase() + ':' + domain + uri + '` trouvée'
        ) ;

        goodRoute = route ;
        return false ; // On sort de la boucle
      }
    }) ;

    if (!goodRoute)
      throw new RouteError('Aucune route trouvée') ;

    return goodRoute ;
  }


  /**
   * Reconstruit une route à partir de paramètres
   * @param {String} id Identifiant de la route
   * @param {Object} [params={}] Valeur des variables
   * @param {String} [domain=''] Domaine courant (permettra de déterminer s'il y
   *  a besoin d'une URL absolue ou non
   * @param {Boolean} [https=false] L'URL absolue est-elle en HTTPS ?
   * @returns {String}
   */
  unroute (id, params = {}, domain = '', https = false) {

    if (Object.keys(this._routes).indexOf(id) < 0)
      throw new RouteError('La route ' + id + ' n\'existe pas') ;

    return this._routes[id].unroute(params, domain, https) ;
  }


  /**
   * Retourne la liste des fichiers de routes chargés
   * @returns {Array}
   */
  getFiles () {

    return this._files ;
  }
}

module.exports = Router ;

