'use strict' ;

/**
 * src/server/Services/Renderer.js
 */

/**
 * Il ne s'agit pas d'un service, mais de la classe permettant d'instancier un
 * objet pour le rendu utilisé par $kfmd.
 */
class Renderer {

  /**
   * Portion de code
   * @param {String} code Portion de code
   * @param {String} lang Langage du code
   * @returns {String}
   */
  code (code, lang) {

    lang = lang ? '<!-- ' + lang + ' -->' : '' ;
    return lang + '<pre><code>\n' + code + '\n</code></pre>\n' ;
  }


  /**
   * Citation
   * @param {String} quote Contenu de la citation
   * @returns {String}
   */
  blockquote (quote) {

    return '<blockquote>\n' + quote + '</blockquote>\n' ;
  }


  /**
   * Code HTML
   * @param {String} html Code qui sera retourné tel quel
   * @returns {String}
   */
  html (html) { return html ; }


  /**
   * Titre
   * @param {String} text Texte du titre
   * @param {Integer} level Niveau (entre 1 et 6)
   * @param {String} raw Texte du titre sans transformation
   * @returns {String}
   */
  heading (text, level, raw) {

    return '<h' + level + ' id="'
      + raw.toLowerCase().replace(/[^\w]+/g, '-')
      + '">' + text + '</h' + level + '>\n' ;
  }


  /**
   * Liste
   * @param {String} body Contenu de la liste
   * @param {Boolean} ordered Liste ordonnée ou non
   * @returns {String}
   */
  list (body, ordered) {

    let type = ordered ? 'ol' : 'ul' ;
    return '<' + type + '>\n' + body + '</' + type + '>\n' ;
  }


  /**
   * Item d'une liste
   * @param {String} text Texte de l'item
   * @returns {String}
   */
  listItem (text) { return '  <li>' + text + '</li>\n' ; }


  /**
   * Liste de tâche
   * @param {String} body Contenu de la liste
   * @param {Boolean} ordered Liste ordonnée ou non
   * @returns {String}
   */
  taskList (body, ordered) {

    let type = ordered ? 'ol' : 'ul' ;
    return '<' + type + '>\n' + body + '</' + type + '>\n' ;
  }


  /**
   * Item d'une liste de tâches
   * @param {String} text Texte de l'item
   * @param {Boolean} checked Tâche effectuée ou non
   * @returns {String}
   */
  taskListItem (text, checked) {

    return '  <li><input type="checkbox" disabled'
      + (checked ? ' checked' : '')
      + '>' + text + '</li>\n' ;
  }


  /**
   * Un paragraphe
   * @param {String} text Texte
   * @returns {String}
   */
  paragraph (text) { return '<p>' + text + '</p>\n' ; }


  /**
   * Tableau
   * @param {String} header Contenu du header (`<thead>`)
   * @param {String} body Contenu du corps (`<tbody>`)
   * @returns {String}
   */
  table (header, body) {

    return '<table>\n<thead>\n' + header + '</thead>\n<tbody>\n'
      + body + '</tbody>\n</table>\n' ;
  }


  /**
   * Ligne d'un tableau
   * @param {String} content Contenu de la ligne
   * @returns {String}
   */
  tablerow (content) { return '  <tr>\n' + content + '  </tr>\n' ; }


  /**
   * Cellule d'un tableau
   * @param {String} content Contenu de la cellule
   * @param {Object} flags Si la proprité `header` est définie, il s'agit d'une
   *    cellule d'un header (`th`), la proprité `align` (facultative) définie
   *    l'alignement
   * @returns {String}
   */
  tablecell (content, flags) {

    let type = flags.header ? 'th' : 'td' ;
    let tag = flags.align ?
      '    <' + type + ' class="txt' + flags.align + '">'
      : '    <' + type + '>' ;
    return tag + content + '</' + type + '>\n' ;
  }


  /**
   * Emphase forte
   * @param {String} text Texte à mettre en emphase
   * @returns {String}
   */
  strong (text) { return '<strong>' + text + '</strong>' ; }


  /**
   * Emphase faible
   * @param {String} text Texte à mettre en emphase
   * @returns {String}
   */
  em (text) { return '<em>' + text + '</em>' ; }


  /**
   * Code inline
   * @param {String} text Code
   * @returns {String}
   */
  codespan (text) { return '<code>' + text + '</code>' ; }


  /**
   * Retour à la ligne dans un paragraphe
   * @returns {String}
   */
  br () { return '<br>\n' ; }


  /**
   * Barré (supprimé)
   * @param {String} text Texte
   * @returns {String}
   */
  del (text) { return '<del>' + text + '</del>' ; }


  /**
   * Lien
   * @param {String} href URL du lien
   * @param {String} title Attribut title du lien (peut être vide)
   * @param {String} text Texte du lien
   * @returns {String}
   */
  link (href, title, text) {

    let out = '<a href="' + href + '"' ;
    if (title)
      out += ' title="' + title + '"' ;
    out += '>' + text + '</a>' ;
    return out ;
  }


  /**
   * Simple texte
   * @param {String} text Le texte
   * @returns {String}
   */
  text (text) { return text ; }


  /**
   * footNote
   * @param {Integer} nb Numéro de la note
   * @returns {String}
   */
  footNote (nb) {

    return '<a href="#note-' + nb + '"><sup>' + nb + '</sup></a>' ;
  }
}

module.exports = Renderer ;

