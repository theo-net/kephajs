'use strict' ;

/**
 * src/core/Services/Models.test.js
 */

let expect = require('chai').expect ;

const Models = require('./Models') ;

describe('Models', function () {

  let models = null ;
  let model = {} ;

  beforeEach(function () {

    models = new Models() ;
  }) ;

  describe('register', function () {

    it('enregistre un modèle', function () {

      models.register('test', model) ;
      expect(models._models['test']).to.be.equal(model) ;
    }) ;
  }) ;


  describe('get', function () {

    it('retourne la valeur d\'un paramètre', function () {

      models.register('model', model) ;
      expect(models.get('model')).to.be.equal(model) ;
    }) ;

    it('retourne une valeur par défaut si le modèle est inexistant',
    function () {

      models.register('model', model) ;
      expect(models.get('model2')).to.be.equal(null) ;
    }) ;
  }) ;


  describe('has', function () {

    it('returns true if models have the key', function () {

      models.register('model', model) ;
      expect(models.has('model')).to.be.equal(true) ;
    }) ;

    it('returns false if models haven\'t the key', function () {

      models.register('model', model) ;
      expect(models.has('model1')).to.be.equal(false) ;
    }) ;
  }) ;


  describe('delete', function () {

    it('delete a element', function () {

      models.register('model', model) ;
      expect(models.has('model')).to.be.equal(true) ;
      models.delete('model') ;
      expect(models.has('model')).to.be.equal(false) ;
    }) ;
  }) ;
}) ;

