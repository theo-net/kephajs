'use strict' ;

/**
 * src/index.js
 */

/**
 * Point d'entré du framework
 *
 * Fournit le framework, ainsi que des méthode pour créé une app CLI ou un
 * serveur HTTP.
 *
 * `require('kephajs')` fournit un accès au framework.
 * L'objet possède également deux méthodes pour créer une application CLI ou un
 * serveur HTTP.
 */

const Cli         = require('./cli/Cli'),
      Server      = require('./server/Server'),
      FormBuilder = require('./server/Form/FormBuilder'),
      Field       = require('./server/Form/Field') ;


let K = require('./cli/Utils') ;


/**
 * Créé une nouvelle application CLI
 * @returns {Cli}
 */
K.cli = function () {

  return new Cli() ;
} ;


/**
 * Créé un nouveau serveur
 * @param {String} staticPath Répertoire de fichiers statiques
 *
 * @returns {Server}
 */
K.server = function (staticPath) {

  return new Server(staticPath) ;
} ;

K.FormBuilder = FormBuilder ;
K.Field = Field ;

module.exports = K ;

