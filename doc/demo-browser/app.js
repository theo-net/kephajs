'use strict' ;

/**
 * doc/demo-browser/app.js
 */

const app = K.app() ;

// On teste la disponibilité des services
app.kernel().get('$config').set('foo', 'bar') ;
console.log('foo = ' + app.kernel().get('$config').get('foo')) ;

// On initialise le premier contener
app.controller('ExampleController', function () {

    this.counter = 1 ;
    this.foobar = false ;

    this.increment = () => {
      this.counter++ ;
    }
    this.decrement = () => {
      this.counter-- ;
    }

    this.toogle = () => {
      this.foobar = !this.foobar ;
    }
  }
) ;

