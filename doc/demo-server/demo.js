'use strict' ;

/**
 *  This file is part of the KephaJS project.
 *
 *  (c) Grégoire OLIVEIRA SILVA
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

const server = require('../../src/index')
  .server(require('./config/config.json')) ;

// Init du server
server.init = ($config, kernel) => {

  // Forms
  server.setFormBuilders({
    message: require('./form/MessageFormBuilder')
  }) ;
} ;

// On lance le server
server.run() ;

