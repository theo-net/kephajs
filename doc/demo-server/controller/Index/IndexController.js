'use strict' ;

class IndexController {

  indexAction () {

    this.foo = this.$user.foo ;
    this.$view.title = 'Hello world' ;
    this.$view.message = 'Salut' ;
    this.$view.foobar = 'test' ;
  }

  blankAction () {}

  aliasAction () {}

  testAction () {}

  slotAction (vars) {

    this.$view.$setLayout(false) ;

    if (vars.a)
      this.$view.a = vars.a ;

    this.get('$config').set('testRequest', this.$request, true) ;
    this.get('$config').set('testResponse', this.$response, true) ;
    this.get('$config').set('testUser', this.$user, true) ;
  }

  asyncAction () {

    return new Promise(resolve => {
      this.okAsync = 'ok' ;
      resolve() ;
    }) ;
  }
}

IndexController.prototype.setViews = {
  blank: null,
  alias: 'index.html'
}

module.exports = IndexController ;

