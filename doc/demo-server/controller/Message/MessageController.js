'use strict' ;

class MessageController {

  indexAction (vars) {

    let form = this.get('$forms').build('message') ;
    form.setAction('./message.html', 'post') ;

    this.$view.title = 'Messages' ;
    this.$view.form = form ;

    if(this.$request.method == "POST") {

      let ret = form.process(vars) ;

      if (ret) {
        this.$view.message = ret.auteur + ' a envoyé : <br><b>'
        + ret.titre + '</b><br>' + ret.message ;
      }
    }
  }
}

module.exports = MessageController ;

