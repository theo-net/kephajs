#!/usr/bin/env node

'use strict' ;

const cli = require('../../src/index').cli() ;

cli
  .version('1.0.0')

  // Commande `order`
  .command('burger', 'Commander un burger')
  .alias('jaifaim')

  .argument('<type>', 'Type du burger', ['cheese', 'double-cheese', 'fish'])
  .argument('<from>', 'À quelle enseigne commander', 'string')
  .argument('<account>', 'Avec quel compte commander', 'integer')

  .option('-n, --number <num>', 'Nombre de burger', {integer: {min: 0}}, 1)
  .option('-d, --discount <amount>', 'Remise offerte', {float: {min: 0}})
  .option('-p, --pay-by <mean>', 'Payer avec', ['esp', 'cb', 'chq'])
  .option('-l <test>', 'Version sans sauce du burger')

  .action(function (args, options) {
    console.log('Commande \'burger\' appellée avec :') ;
    console.log('arguments: %j', args) ;
    console.log('options: %j', options) ;
  })

  // La commande "return"
  .command('return', 'Retourne une commande')

  .argument('<order-id>', 'Order id', 'integer')
  .argument('<to-store>', 'Store id', 'integer')

  .option('--ask-change <other-kind-burger>',
          'Demande pour un autre type de burger',
          ['cheese', 'double-cheese', 'fish'])
  .option('--say-something <something>',
          'Dire quelque-chose au manager', 'string')

  .action(function (args, options) {
    return Promise.resolve('wooooo').then(function (ret) {
      console.log('Commande \'return\' appellée avec :') ;
      console.log('arguments: %j', args) ;
      console.log('options: %j', options) ;
      console.log('promise succeed with: %s', ret) ;
    }) ;
  }) ;

cli.run() ;

