# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#container">`container`</a>
* <a href="#events">`events`</a>
* <a href="#get">`get`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor([force=false])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L28 "View in source") [&#x24C9;][1]

Initialisation du Kernel

#### Arguments
1. `[force=false]` *(Boolean)*: Force la création d'une nouvelle instance

#### Returns
*(Kernel)*: L'instance

---

<!-- /div -->

<!-- div -->

<h3 id="container"><a href="#container">#</a>&nbsp;<code>container()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L57 "View in source") [&#x24C9;][1]

Retourne le ServiceContainer

#### Returns
*(ServiceContainer)*:

---

<!-- /div -->

<!-- div -->

<h3 id="events"><a href="#events">#</a>&nbsp;<code>events()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L67 "View in source") [&#x24C9;][1]

Retourne le service $event

#### Returns
*(Events)*:

---

<!-- /div -->

<!-- div -->

<h3 id="get"><a href="#get">#</a>&nbsp;<code>get(name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L77 "View in source") [&#x24C9;][1]

Alias pour `this.container().get()`

#### Arguments
1. `name` *(String)*: Nom du service à récupérer

#### Returns
*(Object)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
