# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#_setProperty">`_setProperty`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#hasModifications">`hasModifications`</a>
* <a href="#isNew">`isNew`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="_setProperty"><a href="#_setProperty">#</a>&nbsp;<code>_setProperty(name, [validator], [aliasOf], [defaultValue])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L83 "View in source") [&#x24C9;][1]

Définie une propriété

#### Arguments
1. `name` *(String)*: Nom de la propriété
2. `[validator]` *(Array|Function|Number|RegExp|String)*: Validateur
3. `[aliasOf]` *(String)*: Alias vers lequel pointe la propriété
4. `[defaultValue]` *(String)*: Valeur par défaut. Sera utilisée si propriété définie à `undefined`

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor([values], [isNew])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L20 "View in source") [&#x24C9;][1]

Construit un objet

#### Arguments
1. `[values]` *(Object)*: Valeurs au départ de l'initialisation
2. `[isNew]` *(Boolean)*: C'est une nouvelle entité

---

<!-- /div -->

<!-- div -->

<h3 id="hasModifications"><a href="#hasModifications">#</a>&nbsp;<code>hasModifications()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L58 "View in source") [&#x24C9;][1]

Indique si les propriétés de l'entité ont été modifiées

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="isNew"><a href="#isNew">#</a>&nbsp;<code>isNew()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L48 "View in source") [&#x24C9;][1]

Indique s'il s'agit d'une nouvelle entité

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
