# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#add">`add`</a>
* <a href="#addValidator">`addValidator`</a>
* <a href="#isValid">`isValid`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="add"><a href="#add">#</a>&nbsp;<code>add(validator, [args={}])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L73 "View in source") [&#x24C9;][1]

Ajoute un nouveau validateur en le construisant grâce au service
`validate`.

#### Arguments
1. `validator` *(Array|Function|Object|RegExp|String): Validateur à ajouter. Si `validator` est un `String`, récupère le validateur correspondant depuis la service, s'il s'agit d'une fonction, construit un nouveau validateur, de même s'il s'agit d'une `RegExp`. S'il s'agit d'un objet, on récupère le service correspondant:<br> { name: args } ex: {integer: {min: `0`, max: `5`}} S'il s'agit d'un tableau : soit on construit une liste &#42;(ListValidator)*&#42;, soit il s'agit d'une série de validateurs décrits par des objets comme ci-dessus.
2. `[args={}]` *(Object|String): Arguments qui seront passés au validateur. S'il s'agit d'un `String`, ce sera le message d'erreur &#42;(utilisé si on construit le validateur à partir d'une fonction ou d'une `RegExp`)*&#42;.

#### Returns
*(ValidateValidator)*: this

---

<!-- /div -->

<!-- div -->

<h3 id="addValidator"><a href="#addValidator">#</a>&nbsp;<code>addValidator(validator)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L37 "View in source") [&#x24C9;][1]

Ajoute un validateur à la collection

#### Arguments
1. `validator` *(Validator)*: Validateur à ajouter

#### Returns
*(ValidateValidator)*: this

---

<!-- /div -->

<!-- div -->

<h3 id="isValid"><a href="#isValid">#</a>&nbsp;<code>isValid(value)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L145 "View in source") [&#x24C9;][1]

Exécute tous les validateurs. Renvoit `false` si un ne passe pas.

#### Arguments
1. `value` *(&#42;)*: Valeur à tester

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
