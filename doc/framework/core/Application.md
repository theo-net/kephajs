# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#name">`name`</a>
* <a href="#version">`version`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="name"><a href="#name">#</a>&nbsp;<code>name([name])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L36 "View in source") [&#x24C9;][1]

Définit ou donne le nom de notre application

#### Arguments
1. `[name]` *(String)*: Nom de l'application

#### Returns
*(&#42;)*: Le nom, si aucun paramètre, `this` sinon

---

<!-- /div -->

<!-- div -->

<h3 id="version"><a href="#version">#</a>&nbsp;<code>version([version])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L53 "View in source") [&#x24C9;][1]

Définit ou donne la version de notre application

#### Arguments
1. `[version]` *(String)*: Version de l'application

#### Returns
*(&#42;)*: La version, si aucun paramètre, `this` sinon

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
