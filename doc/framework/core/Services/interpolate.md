# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#return function interpolate ">`return function interpolate `</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="return function interpolate "><a href="#return function interpolate">#</a>&nbsp;<code>return function interpolate *(text(text, mustHaveExpressions)*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L70 "View in source") [&#x24C9;][1]

Prend en entrée une chaîne de caractères text et retourne une fonction
qui retournera le résultat parsé. L'idée est de prendre en entrée une
chaîne de caractères et de parser toutes les expressions (qui sont par
défaut isolées du reste de la chaîne par {{ et }}) grace au parseur.
Une fonction est alors retournée :<br> return interpolationFn *(context)*
celle-ci prend en paramètre un objet qui sera transmis au parseur
afin de retourner une chaîne de carctère parsée.
<br>
<br>
Tout ce qui est entre {{!-- --}} est considéré comme commentaire et est
donc supprimé.
<br>
<br>
Le contenu entre {{ et }} est parsé à l'aide du service $parse, les blocs
{{#qqc }} du contenu {{/#qqc}} sont traités à l'aide d'helpers.
<br>
<br>
{{#if test}}Retourné si test vrai{{/if}}
<br>
<br>
{{#each items}}Expression bouclées avec chaques éléments d'items{{/each}}
<br>
<br>
Si mustHaveExpressions vaut true, alors la méthode ne renvera une
fonction que si elle trouve une expression dans text, sinon rien ne
sera retourné.
<br>
<br>
Si une expression est une Promise, la fonction renverra une Promise.

#### Arguments
1. `text` *(String)*: Expression à parser
2. `mustHaveExpressions` *(Boolean)*: Cf. présentation

#### Returns
*(Function)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
