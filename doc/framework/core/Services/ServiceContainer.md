# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#get">`get`</a>
* <a href="#getAll">`getAll`</a>
* <a href="#has">`has`</a>
* <a href="#set">`set`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(kernel)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L30 "View in source") [&#x24C9;][1]

Initialise le service container.

#### Arguments
1. `kernel` *(Object)*: Kernel de l'application

---

<!-- /div -->

<!-- div -->

<h3 id="get"><a href="#get">#</a>&nbsp;<code>get(name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L56 "View in source") [&#x24C9;][1]

Retourne un service. Si le service est une fonction, l'exécute avec le
Kernel comme argument et retourne le résultat.

#### Arguments
1. `name` *(String)*: Nom du service

---

<!-- /div -->

<!-- div -->

<h3 id="getAll"><a href="#getAll">#</a>&nbsp;<code>getAll()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L82 "View in source") [&#x24C9;][1]

Retourne tous les services

---

<!-- /div -->

<!-- div -->

<h3 id="has"><a href="#has">#</a>&nbsp;<code>has(name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L71 "View in source") [&#x24C9;][1]

Indique si un service est défini

#### Arguments
1. `name` *(String)*: Nom du service

---

<!-- /div -->

<!-- div -->

<h3 id="set"><a href="#set">#</a>&nbsp;<code>set(name, service)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L43 "View in source") [&#x24C9;][1]

Défini un service

#### Arguments
1. `name` *(String)*: Nom du service
2. `service` *(Object)*: Service à initialiser

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
