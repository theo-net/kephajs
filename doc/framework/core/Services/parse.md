# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#assignableAST">`assignableAST`</a>
* <a href="#ast">`ast`</a>
* <a href="#compile">`compile`</a>
* <a href="#constantWatchDelegate">`constantWatchDelegate`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#createParser">`createParser`</a>
* <a href="#getInputs">`getInputs`</a>
* <a href="#inputsWatchDelegate">`inputsWatchDelegate`</a>
* <a href="#isAssignable">`isAssignable`</a>
* <a href="#isLiteral">`isLiteral`</a>
* <a href="#lex">`lex`</a>
* <a href="#markConstantAndWatchExpressions">`markConstantAndWatchExpressions`</a>
* <a href="#oneTimeLiteralWatchDelegate">`oneTimeLiteralWatchDelegate`</a>
* <a href="#oneTimeWatchDelegate">`oneTimeWatchDelegate`</a>
* <a href="#parse">`parse`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="assignableAST"><a href="#assignableAST">#</a>&nbsp;<code>assignableAST(ast)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1216 "View in source") [&#x24C9;][1]

Retourne un AST assignable

#### Arguments
1. `ast` *(Object)*: AST à transformer en assignable

#### Returns
*(Object)*: AST assignable

---

<!-- /div -->

<!-- div -->

<h3 id="ast"><a href="#ast">#</a>&nbsp;<code>ast(text)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L702 "View in source") [&#x24C9;][1]

Construit un AST à partir d'une expression text

#### Arguments
1. `text` *(String)*: Expression dont on veut l'AST

#### Returns
*(Object)*: AST

---

<!-- /div -->

<!-- div -->

<h3 id="compile"><a href="#compile">#</a>&nbsp;<code>compile(text)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1297 "View in source") [&#x24C9;][1]

Compile l'expression text

#### Arguments
1. `text` *(String)*: Expression à compiler

#### Returns
*(Function)*:

---

<!-- /div -->

<!-- div -->

<h3 id="constantWatchDelegate"><a href="#constantWatchDelegate">#</a>&nbsp;<code>constantWatchDelegate(scope, listenerFn, valueEq, watchFn)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L131 "View in source") [&#x24C9;][1]

C'est un watcher classique, sauf qu'il se supprime dès qu'il est exécuté

#### Arguments
1. `scope` *(Object)*: le scope
2. `listenerFn` *(Function)*: le listener
3. `valueEq` *(&#42;)*: ?
4. `watchFn` *(Function)*: le watcher

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(filter)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L2056 "View in source") [&#x24C9;][1]

Initialise le parser

#### Arguments
1. `filter` *(Object)*: Service filter

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(lexer)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L692 "View in source") [&#x24C9;][1]

Enregistrer le lexer à transformer

#### Arguments
1. `lexer` *(Array)*: Le lexer

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(astBuilder, filter)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1283 "View in source") [&#x24C9;][1]

Initialise le compiler

#### Arguments
1. `astBuilder` *(Object)*: AST
2. `filter` *(Object)*: Service filter

---

<!-- /div -->

<!-- div -->

<h3 id="createParser"><a href="#createParser">#</a>&nbsp;<code>createParser(kernel)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L80 "View in source") [&#x24C9;][1]

Fonction publique *(celle qui est retournée lorsque l'on appelle le module)*
qui s'occupe de parser et de retourner l'expression.
<br>
<br>
L'expression produit une fonction acceptant deux paramètres : scope et
locals. Ainsi toutes les identificateurs se réfèreront à une propriété
de locals *(ou de scope si la propriété n'existe pas dans locals)*
<br>
<br>
Précéance des opérateurs :<br> 1. Primary expressions: lookups, appels de fonctions, ou de méthodes 2. Unary expressions: +a, -a, !a 3. Multiplicative arithmetic expressions: a * b, a / b, et a % b 4. Additive arithmetic expressions: a + b et a - b 5. Relational expressions: a < b, a > b, a <= b, et a >= b 6. Equality testing expressions: a == b, a != b, a === b, et a !== b 7. Logical AND expressions: a && b 8. Logical OR expressions: a || b 9. Ternary expressions: a ? b : c `1`0. Assignments: a = b `1`1. Filters: a | aFilter
<br>
<br>
Si expr est une fonction, parse retournera celle-ci, sinon, si ce n'est pas
une chaîne de caractères, elle retournera une fonction quelconque.
Si expr commence par '::', alors le watcher associé ne sera appelé qu'une
seule fois, de la même manière que si l'expr retourne une valeur constante.
<br>
<br>
Voici les différents éléments reconnus :
<br>
<br>
<br> * Literals
<br> * Integer             `42`
<br> * Floating point      `4.2`
<br> * Scientific notation `42E5` `42e5` `42e-5`
<br> * Single quoted string `'wat'`
<br> * Double-quoted string `"wat"`
<br> * Character escapes    `"\n\f\r\t\v\'\"\\"`
<br> * Unicode escapes      `"\u2665"`
<br> * Boleans   `true` `false`
<br> * null      `null`
<br> * undefined `undefined`
<br> * Arrays  `[1, 2, 3]` `[1, [2, 'three']]`
<br> * Objects `{a: 1, b: 2}` `{'a': 1, "b": "two"}`
<br> * Statements
<br> * Semicolon-separated  `expr; expr; expr`
<br> * Last one is returned `a = 1; a +b` -Parentheses
<br> * Alter precedence order `2 * (a + b)` `(a || b) && c`
<br> * Member Access
<br> * Field lookup    `aKey`
<br> * neated objects  `aKey.otherKey.key3`
<br> * Property lookup `aKey['otherKey']` `aKey[keyVar]` `aKey['other'].key3`
<br> * Array llokup    `anArray[42]`
<br> * Function Calls
<br> * Function calls `aFunction()` `aFunction(42, 'abc')`
<br> * Method calls   `anObject.aFunction()` `anObject[fnVar]()`
<br> * Operators *(in order of precedence)*
<br> * Unary          `-a` `+a` `!done`
<br> * Multiplicative `a * b` `a / b` `a % b`
<br> * Additive       `a + b` `a - b`
<br> * Comparison     `a < b` `a > b` `a <= b` `a >= b`
<br> * Equality       `a == b` `a != b` `a === b` `a !== b`
<br> * Logical And    `a && b`
<br> * Logical Or     `a || b`
<br> * Ternary        `a ? b : c`
<br> * Assignment     `aKey = val` `anObject.aKey = val` `anArray[42] = val`
<br> * Filters        `a | filter` `a | filter1 | filter2` `a | filter:arg1:arg2`

#### Arguments
1. `kernel` *(Object)*: Kernel de l'application

#### Returns
*(Function)*: `(expr)` avec `expr` l'expression à parser

---

<!-- /div -->

<!-- div -->

<h3 id="getInputs"><a href="#getInputs">#</a>&nbsp;<code>getInputs(ast)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L2033 "View in source") [&#x24C9;][1]

getInputs

#### Arguments
1. `ast` *(Object)*: AST

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="inputsWatchDelegate"><a href="#inputsWatchDelegate">#</a>&nbsp;<code>inputsWatchDelegate(scope, listenerFn, valueEq, watchFn)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L240 "View in source") [&#x24C9;][1]



#### Arguments
1. `scope` *(Object)*: le scope
2. `listenerFn` *(Function)*: le listener
3. `valueEq` *(&#42;)*: ?
4. `watchFn` *(Function)*: le watcher

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="isAssignable"><a href="#isAssignable">#</a>&nbsp;<code>isAssignable(ast)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1205 "View in source") [&#x24C9;][1]

isAssignable *(ast)*

#### Arguments
1. `ast` *(Object)*: AST a tester

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="isLiteral"><a href="#isLiteral">#</a>&nbsp;<code>isLiteral(ast)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1889 "View in source") [&#x24C9;][1]

Un programme vide est 'literal', un programme non-vide est 'literal' s'il a
juste une expression de type literal, array ou un objet.

#### Arguments
1. `ast` *(Object)*: AST à tester

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="lex"><a href="#lex">#</a>&nbsp;<code>lex(text)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L371 "View in source") [&#x24C9;][1]

Tokenise l'expression text.

#### Arguments
1. `text` *(String)*: Expression à tokeniser

#### Returns
*(Array)*: tokens

---

<!-- /div -->

<!-- div -->

<h3 id="markConstantAndWatchExpressions"><a href="#markConstantAndWatchExpressions">#</a>&nbsp;<code>markConstantAndWatchExpressions(ast, filter)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1906 "View in source") [&#x24C9;][1]

markConstantAndWatchExpressions *(ast, filter)*
<br>
<br>
Parcours l'AST et détermine si le resultat sera une constante ou pas.

#### Arguments
1. `ast` *(String)*: AST
2. `filter` *(Object)*: Filtres

---

<!-- /div -->

<!-- div -->

<h3 id="oneTimeLiteralWatchDelegate"><a href="#oneTimeLiteralWatchDelegate">#</a>&nbsp;<code>oneTimeLiteralWatchDelegate(scope, listenerFn, valueEq, watchFn)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L195 "View in source") [&#x24C9;][1]

Pas pour tous les Literal, justes pour les objets et les arrays.
Les nombres, strings et boolean sont constants et utilisent donc
constantWatchDelegate

#### Arguments
1. `scope` *(Object)*: le scope
2. `listenerFn` *(Function)*: le listener
3. `valueEq` *(&#42;)*: ?
4. `watchFn` *(Function)*: le watcher

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="oneTimeWatchDelegate"><a href="#oneTimeWatchDelegate">#</a>&nbsp;<code>oneTimeWatchDelegate(scope, listenerFn, valueEq, watchFn)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L156 "View in source") [&#x24C9;][1]

C'est un watcher classique, sauf qu'il se supprime dès qu'il est exécuté

#### Arguments
1. `scope` *(Object)*: le scope
2. `listenerFn` *(Function)*: le listener
3. `valueEq` *(&#42;)*: ?
4. `watchFn` *(Function)*: le watcher

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="parse"><a href="#parse">#</a>&nbsp;<code>parse(text)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L2069 "View in source") [&#x24C9;][1]

Parse l'expression text et renvoit une fonction

#### Arguments
1. `text` *(String)*: Texte à parser

#### Returns
*(Function)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
