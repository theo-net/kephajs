# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#add">`add`</a>
* <a href="#delete">`delete`</a>
* <a href="#get">`get`</a>
* <a href="#has">`has`</a>
* <a href="#set">`set`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="add"><a href="#add">#</a>&nbsp;<code>add(parameters, [ecrase=false])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L93 "View in source") [&#x24C9;][1]

Initialise une série de paramètres

#### Arguments
1. `parameters` *(Object): Tableau de paramètres &#42;(les clés sont l'id)*&#42;
2. `[ecrase=false]` *(Boolean)*: Écrase ou non un élément précédent

---

<!-- /div -->

<!-- div -->

<h3 id="delete"><a href="#delete">#</a>&nbsp;<code>delete(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L82 "View in source") [&#x24C9;][1]

Supprime un élément

#### Arguments
1. `id` *(String)*: Identifiant de l'élément à supprimer

---

<!-- /div -->

<!-- div -->

<h3 id="get"><a href="#get">#</a>&nbsp;<code>get(id, [dflt=null], [format=false])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L55 "View in source") [&#x24C9;][1]

Retourne un paramètre ou la valeur par défaut si celle-ci est spécifiée
et si le paramètre n'est pas initialisé.
En formatant le retour, on remplace une constante par sa valeur.

#### Arguments
1. `id` *(String)*: Identifiant
2. `[dflt=null]` *(&#42;)*: Valeur par défaut
3. `[format=false]` *(Boolean)*: Doit-on formater la sortie

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="has"><a href="#has">#</a>&nbsp;<code>has(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L72 "View in source") [&#x24C9;][1]

Indique si un élément est présent au non

#### Arguments
1. `id` *(String)*: Identifiant de l'élément à tester

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="set"><a href="#set">#</a>&nbsp;<code>set(id, value, [ecrase])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L36 "View in source") [&#x24C9;][1]

Définit un paramètre

#### Arguments
1. `id` *(String)*: Identifiant du paramètre
2. `value` *(&#42;)*: Valeur du paramètre
3. `[ecrase]` *(Boolean): Écrase ou non un paramètre portant le même identifiant &#42;(vaut `false` par défaut)*&#42;.

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
