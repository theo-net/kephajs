# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#$apply">`$apply`</a>
* <a href="#$applyAsync">`$applyAsync`</a>
* <a href="#$beginPhase">`$beginPhase`</a>
* <a href="#$broadcast">`$broadcast`</a>
* <a href="#$emit">`$emit`</a>
* <a href="#$eval">`$eval`</a>
* <a href="#$evalAsync">`$evalAsync`</a>
* <a href="#$new">`$new`</a>
* <a href="#$on">`$on`</a>
* <a href="#$watch">`$watch`</a>
* <a href="#$watchCollection">`$watchCollection`</a>
* <a href="#$watchGroup">`$watchGroup`</a>
* <a href="#digestTtl">`digestTtl`</a>
* <a href="#isArrayLike">`isArrayLike`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="$apply"><a href="#$apply">#</a>&nbsp;<code>$apply(expr)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L148 "View in source") [&#x24C9;][1]

Exécute expr en utilisant $eval et lance le cycle de digest en appellant
$digest. En utilisant cette fonction, on est sûr que si expr modifie le
scope, $digest sera appelé et donc les changements répercutés.
Le digest lancé s'applique à toute la hiérachie du scope *(parents inclus)*.

#### Arguments
1. `expr` *(String)*: Expression a exécuter

#### Returns
*(Function)*:

---

<!-- /div -->

<!-- div -->

<h3 id="$applyAsync"><a href="#$applyAsync">#</a>&nbsp;<code>$applyAsync(expr)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L546 "View in source") [&#x24C9;][1]

comme $apply, mais permet de regrouper plusieurs appels et ainsi lancer un
$digest qu'une seule fois, plutôt qu'après chaque $apply. Mais elle n'est
jamais exécutée dans le cycle courant.

#### Arguments
1. `expr` *(String)*: Expression à évaluer

---

<!-- /div -->

<!-- div -->

<h3 id="$beginPhase"><a href="#$beginPhase">#</a>&nbsp;<code>$beginPhase(phase)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L228 "View in source") [&#x24C9;][1]

Définit la phase courante, envoit une exception si une phase est en cours

#### Arguments
1. `phase` *(String)*: Nom de la phase

---

<!-- /div -->

<!-- div -->

<h3 id="$broadcast"><a href="#$broadcast">#</a>&nbsp;<code>$broadcast(eventName)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L494 "View in source") [&#x24C9;][1]



#### Arguments
1. `eventName` *(String)*: Nom de l'évenement

#### Returns
*(Object)*: L'évenement

---

<!-- /div -->

<!-- div -->

<h3 id="$emit"><a href="#$emit">#</a>&nbsp;<code>$emit(eventName)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L459 "View in source") [&#x24C9;][1]

L'objet event transmis aux listeners contient en plus une méthode
stopPropagation() qui arrête la propagation vers les parents. Les autres
listeners du scope courant seront biens appelés.

#### Arguments
1. `eventName` *(String)*: Nom de l'évenement

#### Returns
*(Object)*: L'évenement

---

<!-- /div -->

<!-- div -->

<h3 id="$eval"><a href="#$eval">#</a>&nbsp;<code>$eval(expr, locals)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L244 "View in source") [&#x24C9;][1]

Exécute la fonction expr en fournissant le scope comme argument, si locals
est définie, il sera passé comme argument.

#### Arguments
1. `expr` *(String)*: Expression a évaluer
2. `locals` *(Object)*: Valeurs locales

#### Returns
*(Function)*:

---

<!-- /div -->

<!-- div -->

<h3 id="$evalAsync"><a href="#$evalAsync">#</a>&nbsp;<code>$evalAsync(expr)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L525 "View in source") [&#x24C9;][1]

Exécute expr, à l'aide d'$eval, plus tard dans le même cycle de digest.
Si aucune boucle de digest n'est en cours, on en lance une.
Le digest porte sur toute la hiérachie *(parents inclus)*.

#### Arguments
1. `expr` *(String)*: Expression à évaluer

---

<!-- /div -->

<!-- div -->

<h3 id="$new"><a href="#$new">#</a>&nbsp;<code>$new(isolated, [parent])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L295 "View in source") [&#x24C9;][1]

Gère l'héritage : retourne un nouveau scope enfant
<br>
<br>
<br> * si on cherche à accéder à une propriété de l'enfant qui n'existe pas, alors ce sera celle de son parent qui sera retournée.
<br> * un parent ne reçoit pas les propriétés de ses enfants
<br> * un enfant héritera des propriétés ajoutés, après sa création, à son parent
<br> * si l'on modifie une propriété partagée, tous ceux qui la partage voient la modification
<br> * lorsqu'on attribut une valeur à une propriété d'un enfant, celle-ci masque celle du parent.
<br>
<br> a /   \ aa     ab /    \   | aaa   aab  abb
<br>
<br>
a.e = [1, `2`, `3`]   // aa.e === [1, `2`, `3`]
aa.f = `3`          // a.f === undefined && ab.f === undefined
aaa.e.push(4)     // a.e === [1, `2`, `3`, `4`]
a.g = 'z'
aa.g = 'y'        // a.g === 'z' && aa.g = 'y'
a.h = {i: `1`}
aa.h.i = `2`        // a.h.i === `2` && aa.h.i === `2`
<br>
<br>
On remarque qu'il est interressant de travailler avec des objets, ainsi on
est sûr de continuer de partager les données avec le parent.
<br>
<br>
Lorsque l'on lance un digest, seul les watchs du scope courant et de ses
enfants sont écoutés, jamais ceux des parents.
<br>
<br>
Si isolated vaut true, alors le nouveau scope n'accèdera pas aux propriétes
de son parent. Cependant, un parent lance un digest, il sera aussi appliqué
aux enfants isolés. $aplly, $evalAsync et $applyAsync remontent jusqu'à la
racine.
<br>
<br>
Si parent est précisé : le scope à partir duquel on appel $new() fournira
l'héritage des propriété, mais la racine sera 'parent'.

#### Arguments
1. `isolated` *(Boolean)*: Isolé ou non
2. `[parent]` *(Scope)*: Racine pour le nouveau scope

#### Returns
*(Scope)*:

---

<!-- /div -->

<!-- div -->

<h3 id="$on"><a href="#$on">#</a>&nbsp;<code>$on(eventName, listener)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L406 "View in source") [&#x24C9;][1]

Enregistre listener pour qu'il écoute eventName.

#### Arguments
1. `eventName` *(String)*: Nom de l'evènement
2. `listener` *(Function)*: Listener

#### Returns
*(Function)*:

---

<!-- /div -->

<!-- div -->

<h3 id="$watch"><a href="#$watch">#</a>&nbsp;<code>$watch(watchFn, listenerFn, [valueEq=false])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L106 "View in source") [&#x24C9;][1]

Permet d'attacher les watchers aux scopes. Les watchers sont des fonctions
qui sont notifiées lorque quelque chose change dans le scope.
L'ajout d'un watcher au cours d'une boucle de digest force à reparcourir
tous les watchers une nouvelle fois *(réinitialisation de _lastDirtyWatch)*
La fonction retourne une fonction, qui lorsqu'elle est appellée, détruit le
watcher.

#### Arguments
1. `watchFn` *(Function): Fonction surveillant le scope watchFn &#42;(scope)*&#42; : {Object} scope scope courant
2. `listenerFn` *(Function): Fonction appelée lorsque le scope est modifié si on ne passe pas ce paramètre, alors on aura juste watchFn qui sera appelé à chaque boucle de digest quelque soit la valeur retournée. Cela permet d'avoir une fonction qui est notifiée à chaque fois que le scope est parcouru. Pour une meilleur optimi- -sation, il est mieux de ne rien retourner &#42;(la valeur du watch sera ainsi toujours undefined) listenerFn (newValue, oldValue, scope)*&#42; :<br>
<br> &#42; {&#42;} newValue nouvelle valeur
<br> &#42; {&#42;} oldValue ancienne valeur, égale à newValue lors de la première boucle
<br> &#42; {Object} scope scope courant
3. `[valueEq=false]` *(Boolean): compare les valeurs &#42;(value-based dirty-checking)*&#42; pour les tableaux ou les objets par exemple.

#### Returns
*(Function)*: Fonction détruisant le watcher lors de son appelle.

---

<!-- /div -->

<!-- div -->

<h3 id="$watchCollection"><a href="#$watchCollection">#</a>&nbsp;<code>$watchCollection(watchFn, listenerFn)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L575 "View in source") [&#x24C9;][1]

Alors que $watch observe les valeurs (référence ou alors les valeurs en
détail de chaque attributs et sous attributs de l'objet observé,
$watchCollection est optimisé pour observer une collection et indiquer ainsi
si un nouvel élément est ajouté, un autre supprimé, ou modifié.
Si on n'observe pas un tableau ou un objet, la fonction fonctionne de la
même manière que $watch.

#### Arguments
1. `watchFn` *(Function)*: watcher
2. `listenerFn` *(Function)*: listener

#### Returns
*(Function)*: fonction de destruction.

---

<!-- /div -->

<!-- div -->

<h3 id="$watchGroup"><a href="#$watchGroup">#</a>&nbsp;<code>$watchGroup(watchFns, listenerFn)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L343 "View in source") [&#x24C9;][1]

Enregistre les watchers watchFns ayant tous le même listener : listenerFn
Cela permet d'enregistrer une même action pour un groupe de variables
différentes à sauvegarder.
Le listener ne sera exécuté qu'une seule fois par digest.
La première fois, oldValues est le même tableau que newValues.
Si watchFns est vide, le listener sera quand même appelé une fois.
Enfin, la fonction retourne une fonction qui permet de désenregistrer le
groupe de watcher, de la même manière que $watch.

#### Arguments
1. `watchFns` *(Array)*: Tableau de watchers
2. `listenerFn` *(Function)*: Listener

#### Returns
*(Function)*:

---

<!-- /div -->

<!-- div -->

<h3 id="digestTtl"><a href="#digestTtl">#</a>&nbsp;<code>digestTtl(value)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L72 "View in source") [&#x24C9;][1]

Fixe à value le TTL. Ainsi, si value n'est pas un nombre ou s'il n'est pas
passé en paramètre, on aura la valeur courante de TTL. Par défaut, TTL
vaut `10`.

#### Arguments
1. `value` *(Integer)*: Valeur du TTL

#### Returns
*(Integer)*: Valeur courante du TTL

---

<!-- /div -->

<!-- div -->

<h3 id="isArrayLike"><a href="#isArrayLike">#</a>&nbsp;<code>isArrayLike(obj)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L879 "View in source") [&#x24C9;][1]

retourne un boolean indique si obj est comme un Array

#### Arguments
1. `obj` *(&#42;)*: élément à tester

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
