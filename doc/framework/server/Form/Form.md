# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#build">`build`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#getParent">`getParent`</a>
* <a href="#process">`process`</a>
* <a href="#setAction">`setAction`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="build"><a href="#build">#</a>&nbsp;<code>build()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L61 "View in source") [&#x24C9;][1]

Construit le code HTML du formulaire et le retourne.

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor([entity=null], forms)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L23 "View in source") [&#x24C9;][1]

Initialise un nouveau formulaire.
En lui donnant une entité, on permet l'autoremplissage des champs et
l'autovalidation

#### Arguments
1. `[entity=null]` *(Entity)*: Entité à transmettre
2. `forms` *(Forms)*: Service $forms

---

<!-- /div -->

<!-- div -->

<h3 id="getParent"><a href="#getParent">#</a>&nbsp;<code>getParent()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L51 "View in source") [&#x24C9;][1]

Retourne toujours lui-même

#### Returns
*(this)*:

---

<!-- /div -->

<!-- div -->

<h3 id="process"><a href="#process">#</a>&nbsp;<code>process(vars)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L81 "View in source") [&#x24C9;][1]

Exécute le formulaire

#### Arguments
1. `vars` *(Object)*: Variables soumises `{id: value, ...}`

#### Returns
*(&#42;)*: Null si erreur, object sinon

---

<!-- /div -->

<!-- div -->

<h3 id="setAction"><a href="#setAction">#</a>&nbsp;<code>setAction(action, [method="get"])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L38 "View in source") [&#x24C9;][1]

Définit l'action et la méthode

#### Arguments
1. `action` *(String)*: ="" Action du formulaire
2. `[method="get"]` *(String)*: Méthode du formulaire

#### Returns
*(this)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
