# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#getForm">`getForm`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor([entity=null], forms)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L21 "View in source") [&#x24C9;][1]

Initialise un nouveau formulaire. Si une entité est transmise, elle sera
donnée au formulaire qui pourra ainsi automatiser certaines choses.

#### Arguments
1. `[entity=null]` *(Entity)*: Entité à transmettre
2. `forms` *(Forms)*: Service $forms, dispo dans l'objet avec `this.$forms`

---

<!-- /div -->

<!-- div -->

<h3 id="getForm"><a href="#getForm">#</a>&nbsp;<code>getForm()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L40 "View in source") [&#x24C9;][1]

Retourne le formulaire associé

#### Returns
*(Form)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
