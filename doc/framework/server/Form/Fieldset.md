# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#build">`build`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#getClass">`getClass`</a>
* <a href="#getLegend">`getLegend`</a>
* <a href="#setLegend">`setLegend`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="build"><a href="#build">#</a>&nbsp;<code>build()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L56 "View in source") [&#x24C9;][1]

Construit le code HTML du fieldset et le retourne.

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor([entity=null], [forms], [idPrefix])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L24 "View in source") [&#x24C9;][1]

Initialise un nouveau fieldset.
En lui donnant une entité, on permet l'autoremplissage des champs et
l'autovalidation

#### Arguments
1. `[entity=null]` *(Entity)*: Entité à transmettre
2. `[forms]` *(Forms)*: Service $forms
3. `[idPrefix]` *(String)*: Préfixe de l'id quand celui-ci est défini

---

<!-- /div -->

<!-- div -->

<h3 id="getClass"><a href="#getClass">#</a>&nbsp;<code>getClass()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L80 "View in source") [&#x24C9;][1]

Retourne les classes CSS appliquées au fieldset. Assemblera celles définies
spécifiquement pour lui et celles ajoutées par `addClass{fieldset: ...}`.

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getLegend"><a href="#getLegend">#</a>&nbsp;<code>getLegend()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L47 "View in source") [&#x24C9;][1]

Retourne la légende associée au fieldset. `null` si aucune

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="setLegend"><a href="#setLegend">#</a>&nbsp;<code>setLegend(legend)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L36 "View in source") [&#x24C9;][1]

Définie la légende du fieldset

#### Arguments
1. `legend` *(String)*: La légende

#### Returns
*(this)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
