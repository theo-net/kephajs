# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#execute">`execute`</a>
* <a href="#getView">`getView`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(controller, action, vars, request, response, [user])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L45 "View in source") [&#x24C9;][1]

Initialise le Slot. Lui associera sa vue et définira le layout

#### Arguments
1. `controller` *(Function|String)*: Controller à exécuter. Peut être un objet à initialiser
2. `action` *(String)*: Action à exécuter
3. `vars` *(Object)*: Variables transmises
4. `request` *(http.IncomingMessage)*: Requête reçue
5. `response` *(http.ServerResponse)*: Réponse du serveur
6. `[user]` *(Object): Objet user à transmettre &#42;(si non définie, en créra un nouveau)*&#42;

---

<!-- /div -->

<!-- div -->

<h3 id="execute"><a href="#execute">#</a>&nbsp;<code>execute([data])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L168 "View in source") [&#x24C9;][1]

Exécute le slot. Gère les actions asynchrones, et même une initialisation
du controleur asynchrone. Pour cela, l'`init` ou l'action doivent
retourner une `Promise`. Le reject de cette Promise peut contenir un code
erreur *(403, `404`, `500`, ...)* qui renverra l'erreur page correspondante. Si
un message est retourné, ce sera une erreur `500`, si une exception est
attrapée, son message sera affiché.
Une action dont la promise est résolue avec la valeur `null`, ne vera pas
son exécution résolue : utile si la requête est terminée par elle-même
*(ex : envoit d'un fichier lourd via un buffer)*.

#### Arguments
1. `[data]` *(Object)*: Données pouvant être issues d'une requête

#### Returns
*(Promise)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getView"><a href="#getView">#</a>&nbsp;<code>getView()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L208 "View in source") [&#x24C9;][1]

Retourne la vue associée au slot

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
