# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#done">`done`</a>
* <a href="#end">`end`</a>
* <a href="#finished">`finished`</a>
* <a href="#getHeader">`getHeader`</a>
* <a href="#getHeaders">`getHeaders`</a>
* <a href="#headersSent">`headersSent`</a>
* <a href="#pipeFrom">`pipeFrom`</a>
* <a href="#redirect">`redirect`</a>
* <a href="#send">`send`</a>
* <a href="#sendError403">`sendError403`</a>
* <a href="#sendError404">`sendError404`</a>
* <a href="#sendError500">`sendError500`</a>
* <a href="#setContentType">`setContentType`</a>
* <a href="#setHeader">`setHeader`</a>
* <a href="#setStatusCode">`setStatusCode`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(response, request)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L19 "View in source") [&#x24C9;][1]

Initialise l'objet avec la `response` du serveur

#### Arguments
1. `response` *(http.ServerResponse)*: Réponse du serveur
2. `request` *(http.IncomingMessage)*: Requête reçue

---

<!-- /div -->

<!-- div -->

<h3 id="done"><a href="#done">#</a>&nbsp;<code>done([data], [viewRenderCallback])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L71 "View in source") [&#x24C9;][1]

Rendu fini, on envoit la réponse. Attention, la réponse sera forcément
envoyée ! Donc, ne pas utiliser dans le controller `Error` sauf si vous
précisez manuellement le code de retour *(500, `404`, ...)*

#### Arguments
1. `[data]` *(Buffer|string)*: Données à renvoyer
2. `[viewRenderCallback]` *(Function)*: Juste pour passer un test

---

<!-- /div -->

<!-- div -->

<h3 id="end"><a href="#end">#</a>&nbsp;<code>end(data, encoding, callback)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L34 "View in source") [&#x24C9;][1]

Alias vers response.end()

#### Arguments
1. `data` *(Buffer|string)*: Données à renvoyer
2. `encoding` *(string)*: Encodage
3. `callback` *(Function)*: Callback à exécuter après la réponse

---

<!-- /div -->

<!-- div -->

<h3 id="finished"><a href="#finished">#</a>&nbsp;<code>finished()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L46 "View in source") [&#x24C9;][1]

Indique si la réponse a été envoyée ou non

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getHeader"><a href="#getHeader">#</a>&nbsp;<code>getHeader(name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L123 "View in source") [&#x24C9;][1]

Alias vers response.getHeader()

#### Arguments
1. `name` *(string)*: Nom de l'header que l'on veut lire

#### Returns
*(string)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getHeaders"><a href="#getHeaders">#</a>&nbsp;<code>getHeaders()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L133 "View in source") [&#x24C9;][1]

Alias vers response.getHeaders()

#### Returns
*(Object)*:

---

<!-- /div -->

<!-- div -->

<h3 id="headersSent"><a href="#headersSent">#</a>&nbsp;<code>headersSent()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L143 "View in source") [&#x24C9;][1]

alias vers response.headersSent()

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="pipeFrom"><a href="#pipeFrom">#</a>&nbsp;<code>pipeFrom(flux)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L101 "View in source") [&#x24C9;][1]

Pipe un flux vers la réponse. Attention, quand le flux est entièrement lu,
la réponse est envoyée complètement. On ne peut plus envoyer d'autres
contenus, comme le rendu d'une vue par exemple.
C'est le moyen idéal pour envoyer de gros contenus.

#### Arguments
1. `flux` *(Stream)*: Flux à envoyer

---

<!-- /div -->

<!-- div -->

<h3 id="redirect"><a href="#redirect">#</a>&nbsp;<code>redirect(url)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L163 "View in source") [&#x24C9;][1]

Lance une redirection HTTP

#### Arguments
1. `url` *(String)*: URL de redirection

---

<!-- /div -->

<!-- div -->

<h3 id="send"><a href="#send">#</a>&nbsp;<code>send(view, [viewRenderCallback])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L57 "View in source") [&#x24C9;][1]

Envoit le rendu d'une vue

#### Arguments
1. `view` *(View)*: Vue à envoyer
2. `[viewRenderCallback]` *(Function)*: Juste pour passer un test

---

<!-- /div -->

<!-- div -->

<h3 id="sendError403"><a href="#sendError403">#</a>&nbsp;<code>sendError403()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L185 "View in source") [&#x24C9;][1]

Envoit une erreur `403`

#### Returns
*(Promise)*:

---

<!-- /div -->

<!-- div -->

<h3 id="sendError404"><a href="#sendError404">#</a>&nbsp;<code>sendError404()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L195 "View in source") [&#x24C9;][1]

Envoit une erreur `404`

#### Returns
*(Promise)*:

---

<!-- /div -->

<!-- div -->

<h3 id="sendError500"><a href="#sendError500">#</a>&nbsp;<code>sendError500()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L205 "View in source") [&#x24C9;][1]

Envoit une erreur `500`

#### Returns
*(Promise)*:

---

<!-- /div -->

<!-- div -->

<h3 id="setContentType"><a href="#setContentType">#</a>&nbsp;<code>setContentType(type)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L153 "View in source") [&#x24C9;][1]

Définie le Content-Type de la réponse

#### Arguments
1. `type` *(string)*: Content-Type

---

<!-- /div -->

<!-- div -->

<h3 id="setHeader"><a href="#setHeader">#</a>&nbsp;<code>setHeader(name, value)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L112 "View in source") [&#x24C9;][1]

Alias vers response.setHeader()

#### Arguments
1. `name` *(string)*: Nom de l'header
2. `value` *(string)*: Valeur de l'header

---

<!-- /div -->

<!-- div -->

<h3 id="setStatusCode"><a href="#setStatusCode">#</a>&nbsp;<code>setStatusCode(code)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L175 "View in source") [&#x24C9;][1]

Définit le code retour de la requête HTTP

#### Arguments
1. `code` *(Integer)*: Code à renvoyer

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
