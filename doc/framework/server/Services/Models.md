# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#delete">`delete`</a>
* <a href="#get">`get`</a>
* <a href="#has">`has`</a>
* <a href="#register">`register`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="delete"><a href="#delete">#</a>&nbsp;<code>delete(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L65 "View in source") [&#x24C9;][1]

Supprime un élément

#### Arguments
1. `id` *(String)*: Identifiant de l'élément à supprimer

---

<!-- /div -->

<!-- div -->

<h3 id="get"><a href="#get">#</a>&nbsp;<code>get(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L44 "View in source") [&#x24C9;][1]

Retourne un modèle ou null si le modèle n'existe pas

#### Arguments
1. `id` *(String)*: Identifiant

#### Returns
*(Object)*:

---

<!-- /div -->

<!-- div -->

<h3 id="has"><a href="#has">#</a>&nbsp;<code>has(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L55 "View in source") [&#x24C9;][1]

Indique si un modèle est enregistré ou non

#### Arguments
1. `id` *(String)*: Identifiant de l'élément à tester

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="register"><a href="#register">#</a>&nbsp;<code>register(id, model)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L32 "View in source") [&#x24C9;][1]

Enregistre un modèle

#### Arguments
1. `id` *(String)*: Identifiant du modèle
2. `model` *(Object): Modèle à enregistrer identifiant &#42;(vaut `false` par défaut)*&#42;.

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
