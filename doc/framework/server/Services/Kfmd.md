# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#addSafeBlock">`addSafeBlock`</a>
* <a href="#addSafeInline">`addSafeInline`</a>
* <a href="#removeSafeBlock">`removeSafeBlock`</a>
* <a href="#removeSafeInline">`removeSafeInline`</a>
* <a href="#render">`render`</a>
* <a href="#setRenderer">`setRenderer`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="addSafeBlock"><a href="#addSafeBlock">#</a>&nbsp;<code>addSafeBlock(element)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L93 "View in source") [&#x24C9;][1]

Ajout d'un élément block au safeHtml

#### Arguments
1. `element` *(String)*: Élément à ajouter

---

<!-- /div -->

<!-- div -->

<h3 id="addSafeInline"><a href="#addSafeInline">#</a>&nbsp;<code>addSafeInline(element)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L67 "View in source") [&#x24C9;][1]

Ajout d'un élément inline au safeHtml

#### Arguments
1. `element` *(String)*: Élément à ajouter

---

<!-- /div -->

<!-- div -->

<h3 id="removeSafeBlock"><a href="#removeSafeBlock">#</a>&nbsp;<code>removeSafeBlock(element)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L107 "View in source") [&#x24C9;][1]

Retrait d'un élément block au safeHtml

#### Arguments
1. `element` *(String)*: Élément à retirer

---

<!-- /div -->

<!-- div -->

<h3 id="removeSafeInline"><a href="#removeSafeInline">#</a>&nbsp;<code>removeSafeInline(element)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L81 "View in source") [&#x24C9;][1]

Retrait d'un élément inline au safeHtml

#### Arguments
1. `element` *(String)*: Élément à retirer

---

<!-- /div -->

<!-- div -->

<h3 id="render"><a href="#render">#</a>&nbsp;<code>render(text)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L128 "View in source") [&#x24C9;][1]

Parse un texte et renvoit un objet contenant les éléments issus de la
transformation :<br> results = { source: 'texte injecté au départ', contentHtml: texte parsé, toc: ['éléments de la table des matières'], footNotes: ['notes de bas de page extraites du document'] } ;

#### Arguments
1. `text` *(String)*: Texte à parser

#### Returns
*(Object)*:

---

<!-- /div -->

<!-- div -->

<h3 id="setRenderer"><a href="#setRenderer">#</a>&nbsp;<code>setRenderer(renderer)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L57 "View in source") [&#x24C9;][1]

Définit le moteur de rendu

#### Arguments
1. `renderer` *(Object)*: Le moteur (doit au moins implémenter les méthodes de Renderer

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
