# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#access">`access`</a>
* <a href="#count">`count`</a>
* <a href="#delete">`delete`</a>
* <a href="#garbage">`garbage`</a>
* <a href="#get">`get`</a>
* <a href="#has">`has`</a>
* <a href="#hasElement">`hasElement`</a>
* <a href="#init">`init`</a>
* <a href="#isTooOld">`isTooOld`</a>
* <a href="#remove">`remove`</a>
* <a href="#set">`set`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="access"><a href="#access">#</a>&nbsp;<code>access(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L118 "View in source") [&#x24C9;][1]

Met à jour le timestamp de la session en indiquant qu'un accès est
demandé

#### Arguments
1. `id` *(String)*: Identifiant de la session

---

<!-- /div -->

<!-- div -->

<h3 id="count"><a href="#count">#</a>&nbsp;<code>count()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L40 "View in source") [&#x24C9;][1]

Retourne le nombre de session stockées en mémoire

#### Returns
*(Number)*:

---

<!-- /div -->

<!-- div -->

<h3 id="delete"><a href="#delete">#</a>&nbsp;<code>delete(id, name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L107 "View in source") [&#x24C9;][1]

Supprime un élément d'une sessions

#### Arguments
1. `id` *(String)*: Identifiant de la session
2. `name` *(String)*: Nom de l variable

---

<!-- /div -->

<!-- div -->

<h3 id="garbage"><a href="#garbage">#</a>&nbsp;<code>garbage(maxAge)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L139 "View in source") [&#x24C9;][1]

Supprime toutes les sessions trop âgées

#### Arguments
1. `maxAge` *(Integer)*: Âge maximum des sessions

---

<!-- /div -->

<!-- div -->

<h3 id="get"><a href="#get">#</a>&nbsp;<code>get(id, name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L96 "View in source") [&#x24C9;][1]

Récupère un élément d'une session

#### Arguments
1. `id` *(String)*: Identifiant de la session
2. `name` *(String)*: Nom de la variable

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="has"><a href="#has">#</a>&nbsp;<code>has(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L51 "View in source") [&#x24C9;][1]

Indique si une session est enregistrée

#### Arguments
1. `id` *(String)*: Identifiant de la session

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="hasElement"><a href="#hasElement">#</a>&nbsp;<code>hasElement(id, name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L85 "View in source") [&#x24C9;][1]

Indique si un élément est contenu dans la session

#### Arguments
1. `id` *(String)*: Identifiant de la session
2. `name` *(String)*: Nom de la variable

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="init"><a href="#init">#</a>&nbsp;<code>init(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L27 "View in source") [&#x24C9;][1]

Initialise une nouvelle session. Si l'identifiant est déjà pris, la
nouvelle session écrasera l'ancienne

#### Arguments
1. `id` *(String)*: Identifiant de la session

---

<!-- /div -->

<!-- div -->

<h3 id="isTooOld"><a href="#isTooOld">#</a>&nbsp;<code>isTooOld(id, maxAge)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L130 "View in source") [&#x24C9;][1]

Indique si une session est trop vieille

#### Arguments
1. `id` *(String)*: Identifiant de la session
2. `maxAge` *(Integer)*: Âge maximum des sessions

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="remove"><a href="#remove">#</a>&nbsp;<code>remove(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L61 "View in source") [&#x24C9;][1]

Détruit une session

#### Arguments
1. `id` *(String)*: Identifiant de la session

---

<!-- /div -->

<!-- div -->

<h3 id="set"><a href="#set">#</a>&nbsp;<code>set(id, name, value)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L73 "View in source") [&#x24C9;][1]

Enregistre/met à jour un élément dans une session

#### Arguments
1. `id` *(String)*: Identifiant de la session
2. `name` *(String)*: Nom de la variable
3. `value` *(&#42;)*: Valeur à stocker

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
