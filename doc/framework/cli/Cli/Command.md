# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#action">`action`</a>
* <a href="#alias">`alias`</a>
* <a href="#argument">`argument`</a>
* <a href="#command">`command`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#getOptions">`getOptions`</a>
* <a href="#name">`name`</a>
* <a href="#option">`option`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="action"><a href="#action">#</a>&nbsp;<code>action(action)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L116 "View in source") [&#x24C9;][1]

Définie l'action correspondante à la commande

#### Arguments
1. `action` *(Function)*: Action à exécuter

#### Returns
*(Command)*:

---

<!-- /div -->

<!-- div -->

<h3 id="alias"><a href="#alias">#</a>&nbsp;<code>alias(alias)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L46 "View in source") [&#x24C9;][1]

Défini un alias pour cette commande. Un seul alias est possible par
commande.

#### Arguments
1. `alias` *(String)*: Alias

---

<!-- /div -->

<!-- div -->

<h3 id="argument"><a href="#argument">#</a>&nbsp;<code>argument(synopsis, description, [validator], [defaultValue])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L66 "View in source") [&#x24C9;][1]

Ajoute un argument

#### Arguments
1. `synopsis` *(String)*: Synopsis de l'argument comme `<mon-argument>` ou `[mon-argument]`. `<>` indiquent un argument requis et `[]` un argument optionnel.
2. `description` *(String)*: Description de l'option ou de l'argument
3. `[validator]` *(Function|Number|RegExp|String)*: Option validator, used for checking or casting.
4. `[defaultValue]` *(&#42;)*: Valeur par défaut

#### Returns
*(Command)*:

---

<!-- /div -->

<!-- div -->

<h3 id="command"><a href="#command">#</a>&nbsp;<code>command()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L127 "View in source") [&#x24C9;][1]

Permet d'enchaîner une méthode `command()` avec une autre `.command()`

#### Returns
*(Command)*:

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(name, description, application)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L26 "View in source") [&#x24C9;][1]

Créé une nouvelle commande

#### Arguments
1. `name` *(|String)*: Nom de la commande
2. `description` *(String)*: Description de la commande
3. `application` *(Application)*: Instance de l'application.

---

<!-- /div -->

<!-- div -->

<h3 id="getOptions"><a href="#getOptions">#</a>&nbsp;<code>getOptions()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L103 "View in source") [&#x24C9;][1]

Retourne les options

#### Returns
*(Array)*:

---

<!-- /div -->

<!-- div -->

<h3 id="name"><a href="#name">#</a>&nbsp;<code>name()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L137 "View in source") [&#x24C9;][1]

Retourne le nom de la commande ou une chaîne vide s'il s'agit de la
commande par défaut.

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="option"><a href="#option">#</a>&nbsp;<code>option(synopsis, description, [validator], defaultValue, [required])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L89 "View in source") [&#x24C9;][1]

Ajoute une option

#### Arguments
1. `synopsis` *(String)*: Synopsis de l'option comme `-f, --force` ou `-f, --file <file>` ou `--hello [path]`
2. `description` *(String)*: Description de l'option
3. `[validator]` *(Function|Number|RegExp|String)*: Validateur
4. `defaultValue` *(&#42;)*: Valeur par défaut
5. `[required]` *(Boolean)*: L'option est-elle nécessaire

#### Returns
*(Command)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
