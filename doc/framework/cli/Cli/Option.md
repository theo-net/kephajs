# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#isRequired">`isRequired`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(synopsis, description, [validator], [defaultValue], [required], [application])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L21 "View in source") [&#x24C9;][1]



#### Arguments
1. `synopsis` *(String)*: Synopsis de l'option
2. `description` *(String)*: Description de l'option
3. `[validator]` *(Function|Number|RegExp|String)*: Validateur
4. `[defaultValue]` *(&#42;): Valeur par défaut &#42;(undefined si aucune)*&#42;
5. `[required]` *(Boolean)*: L'option est-elle nécessaire ou facultative
6. `[application]` *(Program)*: Instance de l'application

---

<!-- /div -->

<!-- div -->

<h3 id="isRequired"><a href="#isRequired">#</a>&nbsp;<code>isRequired()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L88 "View in source") [&#x24C9;][1]

Indique si l'argument est requis

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
