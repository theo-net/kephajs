# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#hasDefault">`hasDefault`</a>
* <a href="#isValid">`isValid`</a>
* <a href="#name">`name`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(synopsis, description, [validator], [defaultValue], application)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L16 "View in source") [&#x24C9;][1]



#### Arguments
1. `synopsis` *(String)*: Synopsis de l'argument/option
2. `description` *(String)*: Description de l'option/argument
3. `[validator]` *(Function|Number|RegExp|String)*: Validateur
4. `[defaultValue]` *(&#42;)*: Valeur par défaut
5. `application` *(Application)*: Instance du programme

---

<!-- /div -->

<!-- div -->

<h3 id="hasDefault"><a href="#hasDefault">#</a>&nbsp;<code>hasDefault()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L33 "View in source") [&#x24C9;][1]

Indique si l'option/argument possède une valeur par défaut

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="isValid"><a href="#isValid">#</a>&nbsp;<code>isValid(value)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L55 "View in source") [&#x24C9;][1]

Valide la valeur de l'option/argument

#### Arguments
1. `value` *(&#42;)*: Valeur à tester

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="name"><a href="#name">#</a>&nbsp;<code>name()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L43 "View in source") [&#x24C9;][1]

Retourne le nom de l'option/argument

#### Returns
*(String)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
