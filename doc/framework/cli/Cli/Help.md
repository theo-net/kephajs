# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#display">`display`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(cli)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L17 "View in source") [&#x24C9;][1]

Associe l'aide avec l'application

#### Arguments
1. `cli` *(Cli)*: Instance de l'application

---

<!-- /div -->

<!-- div -->

<h3 id="display"><a href="#display">#</a>&nbsp;<code>display(command)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L28 "View in source") [&#x24C9;][1]

Affiche l'aide de `command`

#### Arguments
1. `command` *(|Command)*: Commande dont on veut l'aide

#### Returns
*(String)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
