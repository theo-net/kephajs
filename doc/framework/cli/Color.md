# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#_setProto">`_setProto`</a>
* <a href="#addStyle">`addStyle`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#enableColor">`enableColor`</a>
* <a href="#supportsColor">`supportsColor`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="_setProto"><a href="#_setProto">#</a>&nbsp;<code>_setProto(styles)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L297 "View in source") [&#x24C9;][1]

Défini le prototype que sera utilisé par les styles

#### Arguments
1. `styles` *(Object)*: Liste des styles à ajouter

---

<!-- /div -->

<!-- div -->

<h3 id="addStyle"><a href="#addStyle">#</a>&nbsp;<code>addStyle(name, style)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L165 "View in source") [&#x24C9;][1]

Ajoute un style

#### Arguments
1. `name` *(String)*: Nom du style
2. `style` *(Function)*: Le style à ajouter

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor([enable])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L46 "View in source") [&#x24C9;][1]

Créé un nouvel objet
Si `enable` n'est pas donné, les colors seront appliqués si la console
les supporte.

#### Arguments
1. `[enable]` *(boolean)*: On désactive le support des couleurs

---

<!-- /div -->

<!-- div -->

<h3 id="enableColor"><a href="#enableColor">#</a>&nbsp;<code>enableColor(enable)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L150 "View in source") [&#x24C9;][1]

Active ou désactive le support des couleurs

#### Arguments
1. `enable` *(Boolean)*: .

---

<!-- /div -->

<!-- div -->

<h3 id="supportsColor"><a href="#supportsColor">#</a>&nbsp;<code>supportsColor(enable)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L113 "View in source") [&#x24C9;][1]

Indique si le terminal supporte les couleurs
Le paramètre `enable` permet de forcer le résultat. On peut également
forcer le support en définissant la variable d'environnement `FORCE_COLOR`
ou avec `COLORTERM`

#### Arguments
1. `enable` *(boolean)*: Force le résultat

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
