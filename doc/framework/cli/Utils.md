# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#static cpr ">`static cpr `</a>
* <a href="#static rmrf ">`static rmrf `</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="static cpr "><a href="#static cpr">#</a>&nbsp;<code>static cpr *(source(source, dest, callback)*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L43 "View in source") [&#x24C9;][1]

Copie un fichier. S'il s'agit d'un répertoire, la copie sera récursive

#### Arguments
1. `source` *(String)*: Fichier à copier
2. `dest` *(String)*: Emplacement de destination
3. `callback` *(Function)*: Fonction appelée après la copie `(err){}`

---

<!-- /div -->

<!-- div -->

<h3 id="static rmrf "><a href="#static rmrf">#</a>&nbsp;<code>static rmrf *(path(path, [callback])*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L98 "View in source") [&#x24C9;][1]

Supprime un fichier ou un dossier et tout son contenu

#### Arguments
1. `path` *(String)*: Fichier ou dossier à supprimer
2. `[callback]` *(Function)*: Function à exécuter après la suppression `(err){}`

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
