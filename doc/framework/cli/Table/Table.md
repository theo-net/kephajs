# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#static getOptionsNoBorder ">`static getOptionsNoBorder `</a>
* <a href="#static truncate ">`static truncate `</a>
* <a href="#static wordWrap ">`static wordWrap `</a>
* <a href="#toString">`toString`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor([options])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L50 "View in source") [&#x24C9;][1]

Initialise notre tableau

#### Arguments
1. `[options]` *(Array)*: Options du tableau

---

<!-- /div -->

<!-- div -->

<h3 id="static getOptionsNoBorder "><a href="#static getOptionsNoBorder">#</a>&nbsp;<code>static getOptionsNoBorder *(opts([opts={}])*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L409 "View in source") [&#x24C9;][1]

Retourne un tableau sans bordures

#### Arguments
1. `[opts={}]` *(Object)*: Options à appliquer

#### Returns
*(Table)*:

---

<!-- /div -->

<!-- div -->

<h3 id="static truncate "><a href="#static truncate">#</a>&nbsp;<code>static truncate *(str(str, desiredLength, [truncateChar])*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L229 "View in source") [&#x24C9;][1]

Tronque une chaîne de caractères.

#### Arguments
1. `str` *(String)*: Chaîne à tronquer
2. `desiredLength` *(Integer)*: Longueur désirée
3. `[truncateChar]` *(String)*: Caractère indiquant la troncature

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="static wordWrap "><a href="#static wordWrap">#</a>&nbsp;<code>static wordWrap *(maxLength(maxLength, input)*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L295 "View in source") [&#x24C9;][1]

Découpe une chaîne de caractère sur plusieurs lignes. Celle-ci le sera
sur les caractères d'espacements.

#### Arguments
1. `maxLength` *(Integer)*: Taille maximum d'une ligne
2. `input` *(String)*: Chaîne à traiter

---

<!-- /div -->

<!-- div -->

<h3 id="toString"><a href="#toString">#</a>&nbsp;<code>toString()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L63 "View in source") [&#x24C9;][1]

Rendu du tableau

#### Returns
*(String)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
