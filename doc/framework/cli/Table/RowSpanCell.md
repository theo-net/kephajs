# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#draw">`draw`</a>
* <a href="#init">`init`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(originalCell)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L20 "View in source") [&#x24C9;][1]

Renseigne la cellule originale

#### Arguments
1. `originalCell` *(Cell)*: Cellule originale

---

<!-- /div -->

<!-- div -->

<h3 id="draw"><a href="#draw">#</a>&nbsp;<code>draw(lineNum)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L44 "View in source") [&#x24C9;][1]

Dessine le spanning

#### Arguments
1. `lineNum` *(Number)*: Numéro de la ligne

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="init"><a href="#init">#</a>&nbsp;<code>init(tableOptions)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L29 "View in source") [&#x24C9;][1]

Initialise l'espacement à partir des informations du tableau

#### Arguments
1. `tableOptions` *(Object)*: Options du tableau

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
