# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#draw">`draw`</a>
* <a href="#drawBottom">`drawBottom`</a>
* <a href="#drawEmpty">`drawEmpty`</a>
* <a href="#drawLine">`drawLine`</a>
* <a href="#drawTop">`drawTop`</a>
* <a href="#init">`init`</a>
* <a href="#mergeTableOptions">`mergeTableOptions`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(options)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L38 "View in source") [&#x24C9;][1]

Construit une nouvelle cellule, le paramètre option permet de la
configurer :<br>
<br> * `rowSpan` Espacement ligne
<br> * `colSpan` Espacement colonne
<br> * `content` Contenu *(converti tout en `string`)*
<br>
<br>
Si autre chose qu'un objet est transmis, alors il s'agira du contenu

#### Arguments
1. `options` *(&#42;)*: Options à transmettre

---

<!-- /div -->

<!-- div -->

<h3 id="draw"><a href="#draw">#</a>&nbsp;<code>draw(lineNum, [spanningCell])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L167 "View in source") [&#x24C9;][1]

Dessine la ligne demandée de la cellule

#### Arguments
1. `lineNum` *(&#42;)*: Peut être `top`n `bottom` ou un numéro de ligne
2. `[spanningCell]` *(Number)*: Doit être un nombre si doit être appelé depuis `RowSpanCell` et doit représenter le nombre de ligne d'espace avant le contenu. Sinon est `undefined`.

#### Returns
*(String)*: LA représentation de la ligne

---

<!-- /div -->

<!-- div -->

<h3 id="drawBottom"><a href="#drawBottom">#</a>&nbsp;<code>drawBottom(drawRight)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L245 "View in source") [&#x24C9;][1]

Dessine la bordure inférieur de la cellule

#### Arguments
1. `drawRight` *(Booelan)*: `true` si on doit dessiner la bordure droite de la cellule

---

<!-- /div -->

<!-- div -->

<h3 id="drawEmpty"><a href="#drawEmpty">#</a>&nbsp;<code>drawEmpty(drawRight, spanningCell)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L263 "View in source") [&#x24C9;][1]

Dessine une ligne vide pour la cellule. Utilisé pour le padding-top/bottom.

#### Arguments
1. `drawRight` *(Boolean)*: `true` si on doit dessiner la bordure droite de la cellule
2. `spanningCell` *(Number)*: Espacemment

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="drawLine"><a href="#drawLine">#</a>&nbsp;<code>drawLine(lineNum, drawRight, forceTruncationSymbol, spanningCell)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L296 "View in source") [&#x24C9;][1]

Rendu d'une ligne de texte

#### Arguments
1. `lineNum` *(Number)*: Numéro de la ligne du texte à rendre. Ne correspond pas forcément au contenu, peut être le padding
2. `drawRight` *(Boolean)*: `true` pour le rendu de la bordure droite
3. `forceTruncationSymbol` *(Boolean)*: `true` si on doit insérer le symbole de troncature si le texte a été tronqué verticalement. Sinon, ne le sera que lors d'une toncature horizontale
4. `spanningCell` *(Number)*: Padding

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="drawTop"><a href="#drawTop">#</a>&nbsp;<code>drawTop(drawRight)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L211 "View in source") [&#x24C9;][1]

Rendu de la bordure supérieure de la cellule

#### Arguments
1. `drawRight` *(Boolean)*: `true` si la méthode doit rendre la bordure droite de la cellule

---

<!-- /div -->

<!-- div -->

<h3 id="init"><a href="#init">#</a>&nbsp;<code>init(tableOptions)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L137 "View in source") [&#x24C9;][1]

Initialise la structure des données de notre cellule.

#### Arguments
1. `tableOptions` *(Object): Un objet complètement renseigné En plus des valeurs standards par défaut, il doit avoir les éléments `colWidths` et `rowWidths` de renseignés. Ces éléments donnent le nombre de colonnes et de lignes du tableau &#42;(doivent contenir un `Number`)*&#42;.

---

<!-- /div -->

<!-- div -->

<h3 id="mergeTableOptions"><a href="#mergeTableOptions">#</a>&nbsp;<code>mergeTableOptions(tableOptions, cells)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L78 "View in source") [&#x24C9;][1]

Transmet les options à notre cellule à partir du tableau

#### Arguments
1. `tableOptions` *(Object)*: Options du tableau
2. `cells` *(Array)*: Cellules du tableau

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
