# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#static debounce ">`static debounce `</a>
* <a href="#static throttle ">`static throttle `</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="static debounce "><a href="#static debounce">#</a>&nbsp;<code>static debounce *(callback(callback, delay)*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L144 "View in source") [&#x24C9;][1]

Permet de déclencher l'appel à une fonction après un certains délai et
réinitialise le timer si la fonction est appellée avant que le délai soit
dépassé.
<br>
<br>
exemple d'appel:<br>
search.addEventListener('keyup', debounce(function(e) {
// Le code ici sera exécuté au bout de `350` ms
// mais si l'utilisateur tape une nouvelle fois durant cet intervalle
// de temps, le timer sera réinitialisé.
}, `350`))

#### Arguments
1. `callback` *(Function)*: La fonction
2. `delay` *(Number)*: Le délai en ms

#### Returns
*(Function)*:

---

<!-- /div -->

<!-- div -->

<h3 id="static throttle "><a href="#static throttle">#</a>&nbsp;<code>static throttle *(callback(callback, delay)*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L171 "View in source") [&#x24C9;][1]

Permet d'éviter des appels consécutifs en introduisant un délai.
exemple :<br>
window.addEventListener('scroll', throttle(function *(e)* {
// Le code ici ne pourra être exécuté que toutes les `50` ms (20 appels max
// par secondes)
}, `50`))

#### Arguments
1. `callback` *(Function)*: La fonction
2. `delay` *(Number)*: Le délai en ms

#### Returns
*(Function)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
