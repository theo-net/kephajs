# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#controller">`controller`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="controller"><a href="#controller">#</a>&nbsp;<code>controller(name, controller)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L43 "View in source") [&#x24C9;][1]

Alias pour enregistrer un controller

#### Arguments
1. `name` *(Object|String)*: Nom du controller ou objet `{nom1: ctrl1, nom2: ctrl2, ...}`
2. `controller` *(Object)*: Controller à ajouter

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
