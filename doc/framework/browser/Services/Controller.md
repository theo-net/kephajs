# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#addToScope">`addToScope`</a>
* <a href="#get">`get`</a>
* <a href="#register">`register`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="addToScope"><a href="#addToScope">#</a>&nbsp;<code>addToScope(locals, identifier, instance)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L130 "View in source") [&#x24C9;][1]

Ajoute instance à locals.$scope[identifier]

#### Arguments
1. `locals` *(Object)*: Scope local ?
2. `identifier` *(String)*: Identifiant du controller
3. `instance` *(Object)*: Instance du controller

---

<!-- /div -->

<!-- div -->

<h3 id="get"><a href="#get">#</a>&nbsp;<code>get(ctrl, locals, later, identifier)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L30 "View in source") [&#x24C9;][1]

Retourne un controller

#### Arguments
1. `ctrl` *(String): Retourne un controller &#42;(ira le cherché si déjà def)*&#42;
2. `locals` *(Object)*: Scope local
3. `later` *(Boolean)*: Quand instancie t-on le controller ?
4. `identifier` *(String)*: Identifiant du controller

#### Returns
*(Object)*:

---

<!-- /div -->

<!-- div -->

<h3 id="register"><a href="#register">#</a>&nbsp;<code>register(name, controller)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L85 "View in source") [&#x24C9;][1]

Enregistre un controller

#### Arguments
1. `name` *(Object|String)*: Nom du controller ou objet `{nom1: ctrl1, nom2: ctrl2, ...}`
2. `controller` *(Object)*: Controller à ajouter

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
