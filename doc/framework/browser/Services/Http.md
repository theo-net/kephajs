# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#delete">`delete`</a>
* <a href="#get">`get`</a>
* <a href="#head">`head`</a>
* <a href="#paramSerializerJQLike">`paramSerializerJQLike`</a>
* <a href="#patch">`patch`</a>
* <a href="#post">`post`</a>
* <a href="#put">`put`</a>
* <a href="#request">`request`</a>
* <a href="#serializeParams">`serializeParams`</a>
* <a href="#static defaultHttpResponseTransform ">`static defaultHttpResponseTransform `</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="delete"><a href="#delete">#</a>&nbsp;<code>delete(url, config)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L195 "View in source") [&#x24C9;][1]

Requête DELETE

#### Arguments
1. `url` *(String)*: URL
2. `config` *(Object)*: configuration de la requête

#### Returns
*(Promise)*:

---

<!-- /div -->

<!-- div -->

<h3 id="get"><a href="#get">#</a>&nbsp;<code>get(url, config)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L165 "View in source") [&#x24C9;][1]

Requête GET

#### Arguments
1. `url` *(String)*: URL
2. `config` *(Object)*: configuration de la requête

#### Returns
*(Promise)*:

---

<!-- /div -->

<!-- div -->

<h3 id="head"><a href="#head">#</a>&nbsp;<code>head(url, config)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L180 "View in source") [&#x24C9;][1]

Requête HEAD

#### Arguments
1. `url` *(String)*: URL
2. `config` *(Object)*: configuration de la requête

#### Returns
*(Promise)*:

---

<!-- /div -->

<!-- div -->

<h3 id="paramSerializerJQLike"><a href="#paramSerializerJQLike">#</a>&nbsp;<code>paramSerializerJQLike(params)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L286 "View in source") [&#x24C9;][1]

Sérialise une série de paramètres

#### Arguments
1. `params` *(Object)*: Paramètres à sérialiser

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="patch"><a href="#patch">#</a>&nbsp;<code>patch(url, data, config)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L245 "View in source") [&#x24C9;][1]

Requête PATCH

#### Arguments
1. `url` *(String)*: URL
2. `data` *(&#42;)*: Données de la requête
3. `config` *(Object)*: configuration de la requête

#### Returns
*(Promise)*:

---

<!-- /div -->

<!-- div -->

<h3 id="post"><a href="#post">#</a>&nbsp;<code>post(url, data, config)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L211 "View in source") [&#x24C9;][1]

Requête POST

#### Arguments
1. `url` *(String)*: URL
2. `data` *(&#42;)*: Données de la requête
3. `config` *(Object)*: configuration de la requête

#### Returns
*(Promise)*:

---

<!-- /div -->

<!-- div -->

<h3 id="put"><a href="#put">#</a>&nbsp;<code>put(url, data, config)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L228 "View in source") [&#x24C9;][1]

Requête PUT

#### Arguments
1. `url` *(String)*: URL
2. `data` *(&#42;)*: Données de la requête
3. `config` *(Object)*: configuration de la requête

#### Returns
*(Promise)*:

---

<!-- /div -->

<!-- div -->

<h3 id="request"><a href="#request">#</a>&nbsp;<code>request(requestConfig)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L88 "View in source") [&#x24C9;][1]

Envoie une requête
<br>
<br>
Propriétés possibles :<br>
<br> * `method`
<br> * `url`
<br> * `params` Objet clé => valeur permettant d'ajouter des params à l'URL http://...../..?clé=valeur
<br> * `data`
<br> * `headers`
<br> * `Content-type`
<br> * ...
<br> * `withCredential` active xhr withCredential
<br> * `transformRequest` Fonction transformant les `data`, peut-être également un tableau de fonctions. function *(data, headers, status)*
<br> * `data` données à transformer
<br> * `headers` getter disponible dans la réponse
<br> * `transformResponse` Transforme les réponses des requêtes function *(data, headers, status)*
<br> * `timeout` s'il s'agit d'une Promise, interrompt la requête si celle-ci est résolue entre temps. Sinon, c'est après la durée indiquée

#### Arguments
1. `requestConfig` *(Object)*: configuration de la requête

#### Returns
*(Promise)*:

---

<!-- /div -->

<!-- div -->

<h3 id="serializeParams"><a href="#serializeParams">#</a>&nbsp;<code>serializeParams(params)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L260 "View in source") [&#x24C9;][1]

Sérialise une série de paramètres

#### Arguments
1. `params` *(Object)*: Paramètres à sérialiser

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="static defaultHttpResponseTransform "><a href="#static defaultHttpResponseTransform">#</a>&nbsp;<code>static defaultHttpResponseTransform *(data(data, headers)*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L343 "View in source") [&#x24C9;][1]

Méthode par défaut pour transformer les réponses : dessérialise le contenu
s'il s'agit de JSON

#### Arguments
1. `data` *(String)*: Données de la réponse
2. `headers` *(Function)*: Accès au headers

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
