# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#loadCss">`loadCss`</a>
* <a href="#loadScript">`loadScript`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="loadCss"><a href="#loadCss">#</a>&nbsp;<code>loadCss(url, [media])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L23 "View in source") [&#x24C9;][1]

Charge une feuille de style CSS *(un couple url:media)* ne sera chargé qu'une
seule fois.

#### Arguments
1. `url` *(String)*: Url de la feuille de style
2. `[media]` *(String)*: Attribut `media`

---

<!-- /div -->

<!-- div -->

<h3 id="loadScript"><a href="#loadScript">#</a>&nbsp;<code>loadScript(url, [callback])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L46 "View in source") [&#x24C9;][1]

Charge un script s'il n'a pas déjà été chargé

#### Arguments
1. `url` *(String)*: Url du script à chargé
2. `[callback]` *(Function)*: Fonction appelée après le chargement du script

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
