/*
|--------------------------------------------------------------------------
| KephaJs Browser
|--------------------------------------------------------------------------
|
| The contents in this file is meant to bootstrap the KephaJs application
| and start it in the browser.
|
*/

import { Application } from '@kephajs/browser/Application';

(() => {
    new Application(window.location.href).run();
})();
