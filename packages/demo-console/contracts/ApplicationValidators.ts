import { StringType } from '@kephajs/core/Validate/Type/String';
import { CamelCase } from '../src/Validator/CamelCase';

declare module '@kephajs/core/Validate/ApplicationValidators' {
    interface ApplicationValidators {
        camelCase: CamelCase;
        stringShort: StringType;
    }
}
