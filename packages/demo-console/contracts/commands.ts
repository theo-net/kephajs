import { Command } from '@kephajs/console/Command/Command';
import { BurgerCommand } from '../src/Command/BurgerCommand';
import { HelloCommand } from '../src/Command/HelloCommand';
import { MainCommand } from '../src/Command/MainCommand';
import { ReturnCommand } from '../src/Command/ReturnCommand';

export const commands = [
    MainCommand,
    HelloCommand,
    BurgerCommand,
    ReturnCommand,
] as unknown as typeof Command[];

export const commandAliases = {
    food: BurgerCommand,
} as unknown as Record<string, typeof Command>;
