/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { MyService } from '../src/Service/MyService';

declare module '@kephajs/core/ApplicationRegisteredServices' {
    interface ApplicationRegisteredServices {
        myService: MyService;
    }
}
