import {
    INVALID,
    ParsedType,
    ParseInput,
    ParseReturnType,
    ParseStatus,
} from '@kephajs/core/Validate/helpers/parseUtils';
import {
    FirstPartyTypeKind,
    RawCreateParams,
    Type,
    TypeDef,
} from '@kephajs/core/Validate/Type/Type';
import { IssueCode } from '@kephajs/core/Validate/Validator';

export type CamelCaseDef = {
    typeName: FirstPartyTypeKind.StringType;
} & TypeDef;

export class CamelCase extends Type<string, CamelCaseDef> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const parsedType = this._getType(input);
        if (parsedType !== ParsedType.string) {
            const ctx = this._getOrReturnCtx(input);
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: ParsedType.string,
                received: ctx.parsedType,
            });
            return INVALID;
        }

        const status = new ParseStatus();

        if (input.data !== this._toCamelCase(input.data)) {
            const ctx = this._getOrReturnCtx(input);
            this._addIssueToContext(ctx, {
                code: IssueCode.custom,
                message: 'camelCase validation failed',
            });
            status.dirty();
        }

        return { status: status.value, value: input.data };
    }

    static create = (params?: RawCreateParams): CamelCase => {
        return new CamelCase({
            typeName: FirstPartyTypeKind.StringType,
            ...CamelCase.processCreateParams(params),
        });
    };

    protected _toCamelCase(input: string) {
        return input
            .replace(/\s(.)/g, function (a) {
                return a.toUpperCase();
            })
            .replace(/\s/g, '')
            .replace(/^(.)/, function (b) {
                return b.toLowerCase();
            });
    }
}
