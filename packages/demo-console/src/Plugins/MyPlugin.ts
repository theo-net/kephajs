import { Application } from '@kephajs/core/Application';
import { PluginInterface } from '@kephajs/core/Plugin';

export class MyPlugin implements PluginInterface {
    protected _app: Application | undefined;

    boot(app: Application) {
        app.profiler.register('myPlugin booted');
        this._app = app;
        return Promise.resolve();
    }

    ready() {
        this._app?.profiler.register('myPlugin ready');
        return Promise.resolve();
    }
}
