/*
|--------------------------------------------------------------------------
| KephaJs Console
|--------------------------------------------------------------------------
|
| The contents in this file is meant to bootstrap the KephaJs application
| and start.
|
*/

import { Application } from '@kephajs/console/Application';
import { StringType } from '@kephajs/core/Validate/Type/String';

import { CamelCase } from './Validator/CamelCase';
import { MyService } from './Service/MyService';
import { MyPlugin } from './Plugins/MyPlugin';

import '../contracts/ApplicationRegisteredServices';
import '../contracts/ApplicationValidators';
import { commands, commandAliases } from '../contracts/commands';

const app = new Application(__dirname);
app.registerPlugin(new MyPlugin());
app.init();

// demo custom service
app.container.provideClass('myService', MyService);

// demo custom validator
const $validator = app.container.get('$validator');
$validator.register('camelCase', CamelCase.create());
$validator.register('stringShort', StringType.create().max(5));

// register command
app.container.get('$commandRouter').addCommands(commands);
app.container.get('$commandRouter').setDefaultCommand('main');
app.container.get('$commandRouter').addAliases(commandAliases);

// run the application
app.run();
