import { Config } from '@kephajs/core/Config/Config';

export class MyService {
    constructor(private readonly _config: Config) {}

    public static $inject = ['$config'];

    showConfig() {
        console.log(this._config.getAll());
    }
}
