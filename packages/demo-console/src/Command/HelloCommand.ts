import { Argument } from '@kephajs/console/Command/Argument';
import { Command, CommandExecuteArgs } from '@kephajs/console/Command/Command';
import { Option } from '@kephajs/console/Command/Option';

export class HelloCommand extends Command {
    static readonly commandName = 'hello';

    static readonly description = 'Show an Hello world!';

    public static $inject = [];

    constructor() {
        super();
        this.addArgument(new Argument('<name>', 'the name')).addOption(
            new Option('-e, --excla', 'with !', undefined, false)
        );
    }

    public _execute({ output, args, options }: CommandExecuteArgs) {
        output?.writeln('Hello ' + args.name + (options.excla ? '!' : '.'));

        return Command.SUCCESS;
    }
}
