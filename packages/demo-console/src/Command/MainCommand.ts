import { Command, CommandExecuteArgs } from '@kephajs/console/Command/Command';
import { Validator } from '@kephajs/core/Validate/Validator';
import { MyService } from '../Service/MyService';

export class MainCommand extends Command {
    static readonly commandName = 'main';

    static $inject = ['$validator', 'myService', '$output'];

    constructor(private _validator: Validator, private _myService: MyService) {
        super();
    }

    _execute({ output, outputErr }: CommandExecuteArgs) {
        // demo custom validator
        console.log(this._validator.rules.camelCase.safeParse('testLol'));
        console.log(this._validator.rules.stringShort.safeParse('test'));

        // demo custom service
        this._myService.showConfig();

        // demo output
        output?.write('Hello World!\n');
        outputErr?.write('<error>Error?</>\n');

        const section1 = output?.section();
        const section2 = output?.section();
        const section3 = output?.section();

        section1?.writeln('Hello');
        section2?.writeln('World!');
        section3?.writeln('or me...');

        section2?.overwrite('Hello');
        section2?.writeln('my world!');
        section1?.clear();
        // Hello
        // my world!
        // or me...

        return Command.SUCCESS;
    }
}
