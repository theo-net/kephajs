import { Argument } from '@kephajs/console/Command/Argument';
import { Command, CommandExecuteArgs } from '@kephajs/console/Command/Command';
import { Option } from '@kephajs/console/Command/Option';
import { Validator } from '@kephajs/core/Validate/Validator';

export class ReturnCommand extends Command {
    static readonly commandName = 'return';

    static readonly description = 'return a command';

    public static $inject = ['$validator'];

    constructor(private _validator: Validator) {
        super();
        this.addArgument(
            new Argument(
                '<order-id>',
                'order id',
                this._validator.number().int().min(1)
            )
        )
            .addArgument(
                new Argument(
                    '<to-store>',
                    'store id',
                    this._validator.number().int().min(1)
                )
            )
            .addOption(
                new Option(
                    '--ask-change <other-kind-burger>',
                    'ask for an other type of burger',
                    this._validator.enum(['cheese', 'double-cheese', 'fish'])
                )
            )
            .addOption(
                new Option(
                    '--say-something <something>',
                    'say something to the manager',
                    this._validator.string()
                )
            );
    }

    public _execute({ output, args, options }: CommandExecuteArgs) {
        return Promise.resolve('wooooo')
            .then(ret => {
                output?.writeln('Call return command...');
                output?.writeln('Arguments:');
                Object.keys(args).forEach(argumentName => {
                    output?.writeln(
                        '    ' + argumentName + ': ' + args[argumentName]
                    );
                });
                output?.writeln('Options:');
                Object.keys(options).forEach(optionName => {
                    output?.writeln(
                        '    ' + optionName + ': ' + options[optionName]
                    );
                });
                output?.writeln('promise succeed with: ' + ret);
            })
            .then(() => Command.SUCCESS);
    }
}
