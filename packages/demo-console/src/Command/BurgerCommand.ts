import { Argument } from '@kephajs/console/Command/Argument';
import { Command, CommandExecuteArgs } from '@kephajs/console/Command/Command';
import { Option } from '@kephajs/console/Command/Option';
import { Validator } from '@kephajs/core/Validate/Validator';

export class BurgerCommand extends Command {
    static readonly commandName = 'burger';

    static readonly description = 'I want a burger';

    public static $inject = ['$validator'];

    constructor(private _validator: Validator) {
        super();
        this.addArgument(
            new Argument(
                '<type>',
                'burger type',
                this._validator.enum(['cheese', 'double-cheese', 'fish'])
            )
        )
            .addArgument(
                new Argument(
                    '<from>',
                    'where I order',
                    this._validator.string()
                )
            )
            .addArgument(
                new Argument(
                    '<account>',
                    'with witch account',
                    this._validator.number().int()
                )
            )
            .addOption(
                new Option(
                    '-n, --number <num>',
                    'how many burger',
                    this._validator.number().int().min(0),
                    1
                )
            )
            .addOption(
                new Option(
                    '-d, --discount <amount>',
                    'discount offered',
                    this._validator.number().min(0)
                )
            )
            .addOption(
                new Option(
                    '-p, --pay-by <mean>',
                    'pay with',
                    this._validator.enum(['esp', 'card', 'chq'])
                )
            );
    }

    public _execute({ output, args, options }: CommandExecuteArgs) {
        output?.writeln('Call burger command...');
        output?.writeln('Arguments:');
        Object.keys(args).forEach(argumentName => {
            output?.writeln('    ' + argumentName + ': ' + args[argumentName]);
        });
        output?.writeln('Options:');
        Object.keys(options).forEach(optionName => {
            output?.writeln('    ' + optionName + ': ' + options[optionName]);
        });

        return Command.SUCCESS;
    }
}
