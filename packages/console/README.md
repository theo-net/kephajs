# KephaJs `console`

A console application is defined by the class `Application` that extends `@kephajs/core/Application`.

When we create the application object, we need to use `__dirname` or the root directory of our application as `appRoot` parameter :

    new Application(__dirname)

The root folder need to content a `package.json` file with `name` as app name and `version` as app version.

Registered services are:

-   `$env` the Env service

## Where the configuration is take?

First, read from the `env` service (take the value from `process.env`) and register values in the `config` service in the scope `ENV.*`.
