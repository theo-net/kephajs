/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import * as path from 'path';
import Sinon, { SinonStub } from 'sinon';

import { Application } from './Application';
import { Application as CoreApplication } from '@kephajs/core/Application';
import { Argument } from './Command/Argument';
import { Command, CommandRunArgs } from './Command/Command';
import { CommandRouter } from './Command/CommandRouter';
import { ConfigType } from '@kephajs/core/Config/ConfigType';
import { Env } from './Env/Env';
import { Logger } from './Logger/Logger';
import { ObjectType } from '@kephajs/core/Validate/Type/Object';
import { Option } from './Command/Option';
import { ColorDepth, Output } from './Output/Output';
import { OutputFormatter } from './Output/OutputFormatter';
import { SemVer } from '@kephajs/core/SemVer/SemVer';
import { TableManager } from './Table/TableManager';
import { UnknownCommand } from './Command/Error/UnknownCommand';
import { UnknownOptionError } from './Command/Error/UnknownOptionError';
import { Validator } from '@kephajs/core/Validate/Validator';

import './tests/ConfigType';
import './Env/tests/EnvType';

describe('@kephajs/console/Application', () => {
    it('Should extend @kephajs/core/Application', () => {
        const app = new Application('test');
        expect(app).to.be.instanceOf(CoreApplication);
    });

    describe('appRoot', () => {
        it("Should not change the appRoot parameter if it don't start with file:", () => {
            const app = new Application('/home/user/dev/kephajs-app/');
            app.init();
            expect(app.appRoot).to.be.equal('/home/user/dev/kephajs-app/');
        });

        it('Should change the appRoot parameter if it start with file:', () => {
            const app = new Application('file:///home/user/dev/kephajs-app/');
            app.init();
            expect(app.appRoot).to.be.equal('/home/user/dev');
        });
    });

    describe('environment', () => {
        let keepNodeEnv: string | undefined;

        beforeEach(() => {
            keepNodeEnv = process.env.NODE_ENV;
        });

        afterEach(() => {
            if (keepNodeEnv !== undefined) {
                process.env.NODE_ENV = keepNodeEnv;
            } else {
                delete process.env.NODE_ENV;
            }
        });

        it('Should be set to production in default configuration', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.environment).to.be.equal('production');
        });

        it('Should take the environment from NODE_ENV env variable', async () => {
            process.env.NODE_ENV = 'testing';

            const app = new Application('test');
            await app.init();
            expect(app.environment).to.be.equal('testing');
        });

        it('Should use the environment gived to the constructor if defined', async () => {
            process.env.NODE_ENV = 'testing';

            let app = new Application('test', 'staging');
            await app.init();
            expect(app.environment).to.be.equal('testing');
            app = new Application('test', { environment: 'staging' });
            await app.init();
            expect(app.environment).to.be.equal('testing');
        });

        it('Should normalize dev to development', async () => {
            process.env.NODE_ENV = 'dev';

            const app = new Application('test');
            await app.init();
            expect(app.environment).to.be.equal('development');
        });

        it('Should normalize develop to development', async () => {
            process.env.NODE_ENV = 'develop';

            const app = new Application('test');
            await app.init();
            expect(app.environment).to.be.equal('development');
        });

        it('Should normalize stage to staging', async () => {
            process.env.NODE_ENV = 'stage';

            const app = new Application('test');
            await app.init();
            expect(app.environment).to.be.equal('staging');
        });

        it('Should normalize prod to production', async () => {
            process.env.NODE_ENV = 'prod';

            const app = new Application('test');
            await app.init();
            expect(app.environment).to.be.equal('production');
        });

        it('Should normalize test to testing', async () => {
            process.env.NODE_ENV = 'test';

            const app = new Application('test');
            await app.init();
            expect(app.environment).to.be.equal('testing');
        });

        it('Should normalize everything else to production', async () => {
            process.env.NODE_ENV = 'foobar';

            const app = new Application('test');
            await app.init();
            expect(app.environment).to.be.equal('production');
        });
    });

    describe('description, name and version', () => {
        const appRoot =
            __dirname + path.sep + '..' + path.sep + 'test' + path.sep;

        it("Should return the default if package.json isn't in the appRoot folder", () => {
            expect(new Application('test').description).to.be.equal(undefined);
        });
        it("Should throw an error if package.json isn't a valid JSON file", () => {
            try {
                new Application(appRoot + 'badJson');
            } catch (error) {
                expect(error).to.be.instanceOf(SyntaxError);
                return;
            }
            expect.fail('Should have thrown');
        });
        it("Should return default value if package.json don't contain the key description", () => {
            expect(new Application(appRoot + 'empty').description).to.be.equal(
                undefined
            );
        });
        it('Should take the value of description from package.json', () => {
            expect(new Application(appRoot + 'nice').description).to.be.equal(
                'Hello world!'
            );
        });

        it("Should return the default if package.json isn't in the appRoot folder", () => {
            expect(new Application('test').name).to.be.equal(undefined);
        });
        it("Should return default value if package.json don't contain the key name", () => {
            expect(new Application(appRoot + 'empty').name).to.be.equal(
                undefined
            );
        });
        it('Should take the value of name from package.json', () => {
            expect(new Application(appRoot + 'nice').name).to.be.equal(
                'foobar'
            );
        });

        it("Should return the default if package.json isn't in the appRoot folder", () => {
            expect(new Application('test').version.toString()).to.be.equal(
                '0.0.0'
            );
        });
        it("Should return default value if package.json don't contain the key version", () => {
            expect(
                new Application(appRoot + 'empty').version.toString()
            ).to.be.equal('0.0.0');
        });
        it('Should take the value of version from package.json and parse it', () => {
            expect(
                new Application(appRoot + 'nice').version.toString()
            ).to.be.equal('7.8.9');
        });
    });

    describe('bin', () => {
        it('Should have the value of the scriptname', async () => {
            const app = new Application('test');
            await app.init();

            expect(app.bin).to.be.equal('mocha.js');
        });

        it('Should be the basename value', async () => {
            const app = new Application('test');
            await app.init();

            expect(app.bin).to.be.equal(path.basename(process.argv[1]));
        });
    });

    describe('container', () => {
        it('Should register Env service', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.container.get('$env')).to.be.instanceOf(Env);
        });

        it('Should give the env values to the config in ENV.*', async () => {
            const keep = process.env.FOOBAR;
            process.env.FOOBAR = 'foobar';

            function envValidator(
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                defaultValidator: ObjectType<any>,
                $validator: Validator
            ) {
                return defaultValidator.extend({
                    FOOBAR: $validator.string(),
                });
            }

            const app = new Application('test', { envValidator: envValidator });
            await app.init();
            app.config.set('foobar', 'My %ENV.FOOBAR%');

            expect(app.config.get('ENV.FOOBAR')).to.be.equal('foobar');
            expect(app.config.get('foobar')).to.be.equal('My foobar');

            if (keep !== undefined) process.env.FOOBAR = keep;
        });

        it('Should have $argv as registered value', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.container.get('$argv')).to.be.deep.equal({ _: [] });
        });

        it('Should register argv option if process.argv empty', async () => {
            const app = new Application('test', { argv: ['--foo', 'bar'] });
            await app.init();
            expect(app.container.get('$argv')).to.be.deep.equal({
                foo: 'bar',
                _: [],
            });
        });

        it('Should register process.argv', async () => {
            const backup = [...process.argv];
            process.argv.push('--foo');
            process.argv.push('BAR');
            const app = new Application('test', { argv: ['--foo', 'bar'] });
            await app.init();
            expect(app.container.get('$argv')).to.be.deep.equal({
                foo: 'BAR',
                _: [],
            });
            process.argv = backup;
        });

        it('Should register $output service', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.container.get('$output')).to.be.instanceof(Output);
            expect(app.container.get('$output').getStream()).to.be.equal(
                process.stdout
            );
        });

        it('Should register $outputErr service', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.container.get('$outputErr')).to.be.instanceof(Output);
            expect(app.container.get('$outputErr').getStream()).to.be.equal(
                process.stderr
            );
        });

        it('Should register $outputFormatter service', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.container.get('$outputFormatter')).to.be.instanceof(
                OutputFormatter
            );
        });

        it('Should set trueColor parameter to $outputFormatter', async () => {
            const app = new Application('test');
            await app.init();
            expect(
                app.container.get('$outputFormatter').isTrueColor()
            ).to.be.equal(
                app.container.get('$output').getColorDepth() === ColorDepth.C16M
            );
        });

        it('Should give formatter to output', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.container.get('$output').getFormatter()).to.be.equal(
                app.container.get('$outputFormatter')
            );
            expect(app.container.get('$outputErr').getFormatter()).to.be.equal(
                app.container.get('$outputFormatter')
            );
        });

        it('Should set correct verbosity from command line', async () => {
            const backup = [...process.argv];

            process.argv = [...backup, '-q'];
            let app = new Application('test');
            await app.init();
            expect(app.container.get('$output').getVerbosity()).to.be.equal(
                Output.VERBOSITY.QUIET
            );
            process.argv = [...backup, '--quiet'];
            app = new Application('test');
            await app.init();
            expect(app.container.get('$output').getVerbosity()).to.be.equal(
                Output.VERBOSITY.QUIET
            );

            process.argv = [...backup];
            app = new Application('test');
            await app.init();
            expect(app.container.get('$output').getVerbosity()).to.be.equal(
                Output.VERBOSITY.NORMAL
            );

            process.argv = [...backup, '-v'];
            app = new Application('test');
            await app.init();
            expect(app.container.get('$output').getVerbosity()).to.be.equal(
                Output.VERBOSITY.VERBOSE
            );

            process.argv = [...backup, '-vv'];
            app = new Application('test');
            await app.init();
            expect(app.container.get('$output').getVerbosity()).to.be.equal(
                Output.VERBOSITY.VERY_VERBOSE
            );

            process.argv = [...backup, '-vvv'];
            app = new Application('test');
            await app.init();
            expect(app.container.get('$output').getVerbosity()).to.be.equal(
                Output.VERBOSITY.DEBUG
            );

            process.argv = [...backup];
        });

        it('Should set correct verbosity from env var', async () => {
            const backup = process.env;

            process.env = { ...backup, SHELL_VERBOSITY: '-1' };
            let app = new Application('test');
            await app.init();
            expect(app.container.get('$output').getVerbosity()).to.be.equal(
                Output.VERBOSITY.QUIET
            );

            process.env = { ...backup, SHELL_VERBOSITY: '0' };
            app = new Application('test');
            await app.init();
            expect(app.container.get('$output').getVerbosity()).to.be.equal(
                Output.VERBOSITY.NORMAL
            );
            process.env = { ...backup };
            app = new Application('test');
            await app.init();
            expect(app.container.get('$output').getVerbosity()).to.be.equal(
                Output.VERBOSITY.NORMAL
            );

            process.env = { ...backup, SHELL_VERBOSITY: '1' };
            app = new Application('test');
            await app.init();
            expect(app.container.get('$output').getVerbosity()).to.be.equal(
                Output.VERBOSITY.VERBOSE
            );

            process.env = { ...backup, SHELL_VERBOSITY: '2' };
            app = new Application('test');
            await app.init();
            expect(app.container.get('$output').getVerbosity()).to.be.equal(
                Output.VERBOSITY.VERY_VERBOSE
            );

            process.env = { ...backup, SHELL_VERBOSITY: '3' };
            app = new Application('test');
            await app.init();
            expect(app.container.get('$output').getVerbosity()).to.be.equal(
                Output.VERBOSITY.DEBUG
            );

            process.env = { ...backup };
        });

        it('Should command line option has more precedence over env var', async () => {
            const backupArgv = [...process.argv];
            const backupEnv = process.env;

            process.argv = [...backupArgv, '-vv'];
            process.env = { ...backupEnv, SHELL_VERBOSITY: '3' };
            const app = new Application('test');
            await app.init();
            expect(app.container.get('$output').getVerbosity()).to.be.equal(
                Output.VERBOSITY.VERY_VERBOSE
            );

            process.argv = [...backupArgv];
            process.env = { ...backupEnv };
        });

        it('Should register $logger service with the logger of the console', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.container.get('$logger')).to.be.instanceof(Logger);
        });

        it('Should connect $logger to $output and $outputErr', async () => {
            const app = new Application('test');
            await app.init();
            const logger = app.container.get('$logger');
            expect(
                Object.getOwnPropertyDescriptor(logger, '_output')?.value
            ).to.be.equal(app.container.get('$output'));
            expect(
                Object.getOwnPropertyDescriptor(logger, '_outputErr')?.value
            ).to.be.equal(app.container.get('$outputErr'));
        });

        it('Should set $logger to not save', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.container.get('$logger').isSaveToLater()).to.be.equal(
                false
            );
        });

        it('Should print previous log messages', async () => {
            const backup = process.env;
            process.env = { ...backup, SHELL_VERBOSITY: '3' };

            const stub = Sinon.stub(process.stdout, 'write');
            const app = new Application('test', 'development');
            await app.init();
            const calls = stub.getCalls();
            stub.restore();
            process.env = { ...backup };

            expect(calls[0].args[0]).to.be.deep.equal(
                '\u001b[37;42m INFO \u001b[39;49m Environment is development\n'
            );
        });

        it('Should register $commandRouter service', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.container.get('$commandRouter')).to.be.instanceof(
                CommandRouter
            );
        });

        it('Should connect $commandRouter to $container and $outputErr', async () => {
            const app = new Application('test');
            await app.init();
            const commandRouter = app.container.get('$commandRouter');
            expect(
                Object.getOwnPropertyDescriptor(commandRouter, '_container')
                    ?.value
            ).to.be.equal(app.container.get('$container'));
            expect(
                Object.getOwnPropertyDescriptor(commandRouter, '_outputErr')
                    ?.value
            ).to.be.equal(app.container.get('$outputErr'));
        });

        it('Should register $tableManager service', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.container.get('$tableManager')).to.be.instanceof(
                TableManager
            );
        });

        it('Should connect $tableManager to $output', async () => {
            const app = new Application('test');
            await app.init();
            const tableManager = app.container.get('$tableManager');
            expect(
                Object.getOwnPropertyDescriptor(tableManager, '_output')?.value
            ).to.be.equal(app.container.get('$output'));
        });
    });

    describe('load .env* files', () => {
        let env: Env;

        function envValidator(
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            defaultValidator: ObjectType<any>,
            $validator: Validator
        ) {
            return defaultValidator.extend({
                BASIC: $validator.string().optional(),
            });
        }

        it('Should return loaded files list', async () => {
            const envDir =
                __dirname +
                path.sep +
                'Env' +
                path.sep +
                'tests' +
                path.sep +
                'environment';
            const app = new Application(envDir, {
                envValidator: envValidator,
            });
            await app.init();
            env = app.container.get('$env');
            expect(env.getLoadedFiles()).to.be.deep.equal({
                [envDir + path.sep + '.env']: true,
                [envDir + path.sep + '.env.local']: false,
                [envDir + path.sep + '.env.production']: true,
            });
        });

        it('Should load the .env file', async () => {
            const app = new Application(
                __dirname + path.sep + 'Env' + path.sep + 'tests',
                {
                    envValidator: envValidator,
                }
            );
            await app.init();
            env = app.container.get('$env');
            expect(env.get('BASIC')).to.be.equal('basic');
        });

        it('Should load the .env.local file', async () => {
            const app = new Application(
                __dirname +
                    path.sep +
                    'Env' +
                    path.sep +
                    'tests' +
                    path.sep +
                    'local',
                {
                    envValidator: envValidator,
                }
            );
            await app.init();
            env = app.container.get('$env');
            expect(env.get('BASIC')).to.be.equal('local');
        });

        it('Should load the .env.{environment} file', async () => {
            const envDir =
                __dirname +
                path.sep +
                'Env' +
                path.sep +
                'tests' +
                path.sep +
                'environment';

            let app = new Application(envDir, {
                environment: 'development',
                envValidator: envValidator,
            });
            await app.init();
            env = app.container.get('$env');
            expect(env.get('BASIC')).to.be.equal('development');

            app = new Application(envDir, {
                environment: 'production',
                envValidator: envValidator,
            });
            await app.init();
            env = app.container.get('$env');
            expect(env.get('BASIC')).to.be.equal('production');

            app = new Application(envDir, {
                environment: 'staging',
                envValidator: envValidator,
            });
            await app.init();
            env = app.container.get('$env');
            expect(env.get('BASIC')).to.be.equal('staging');

            app = new Application(envDir, {
                environment: 'testing',
                envValidator: envValidator,
            });
            await app.init();
            env = app.container.get('$env');
            expect(env.get('BASIC')).to.be.equal('testing');
        });

        it('Should give priority 90 to process.env', async () => {
            process.env.BASIC = 'process';
            const app = new Application(
                __dirname +
                    path.sep +
                    'Env' +
                    path.sep +
                    'tests' +
                    path.sep +
                    'environment',
                {
                    envValidator: envValidator,
                }
            );
            await app.init();
            env = app.container.get('$env');
            expect(env.get('BASIC')).to.be.equal('process');
            delete process.env.BASIC;
        });

        it('Should give priority 70 to .env.{environment}', async () => {
            const envDir =
                __dirname +
                path.sep +
                'Env' +
                path.sep +
                'tests' +
                path.sep +
                'environment';

            let app = new Application(envDir, {
                environment: 'development',
                envValidator: envValidator,
            });
            await app.init();
            env = app.container.get('$env');
            expect(env.get('BASIC')).to.be.equal('development');

            app = new Application(envDir, {
                environment: 'production',
                envValidator: envValidator,
            });
            await app.init();
            env = app.container.get('$env');
            expect(env.get('BASIC')).to.be.equal('production');

            app = new Application(envDir, {
                environment: 'staging',
                envValidator: envValidator,
            });
            await app.init();
            env = app.container.get('$env');
            expect(env.get('BASIC')).to.be.equal('staging');

            app = new Application(envDir, {
                environment: 'testing',
                envValidator: envValidator,
            });
            await app.init();
            env = app.container.get('$env');
            expect(env.get('BASIC')).to.be.equal('testing');
        });

        it('Should give priority 50 to .env.local', async () => {
            const app = new Application(
                __dirname +
                    path.sep +
                    'Env' +
                    path.sep +
                    'tests' +
                    path.sep +
                    'local',
                {
                    envValidator: envValidator,
                }
            );
            await app.init();
            env = app.container.get('$env');
            expect(env.get('BASIC')).to.be.equal('local');
        });

        it('Should give priority 30 to .env', async () => {
            const app = new Application(
                __dirname + path.sep + 'Env' + path.sep + 'tests',
                {
                    envValidator: envValidator,
                }
            );
            await app.init();
            env = app.container.get('$env');
            expect(env.get('BASIC')).to.be.equal('basic');
        });
    });

    describe('loadJsonConfig ans getJsonLoaded', () => {
        const baseConfig = {
            'ENV.NODE_ENV': 'production',
            environment: 'production',
            version: new SemVer('0.0.0'),
        };

        it('Should load Json file', async () => {
            const app = new Application(__dirname + path.sep + 'tests');
            await app.init();
            app.loadJsonConfig('param1');
            expect(app.config.getAll()).to.be.deep.equal({
                ...baseConfig,
                aKey: 'foobar',
                'sub.param1': 1,
                'sub.param2': 2,
                'sub.subsub.F': 'foo',
                'sub.subsub.B': 'bar',
            });
        });

        it('Should not replace previous value by default', async () => {
            const app = new Application(__dirname + path.sep + 'tests');
            await app.init();
            app.config.set('other' as keyof ConfigType, 'world');
            app.loadJsonConfig('param1');
            app.loadJsonConfig('param2');
            expect(app.config.getAll()).to.be.deep.equal({
                ...baseConfig,
                other: 'world',
                aKey: 'foobar',
                'sub.param1': 1,
                'sub.param2': 2,
                'sub.subsub.F': 'foo',
                'sub.subsub.B': 'bar',
                'foobar.param': 'Hello',
            });
        });

        it('Should replace previous value', async () => {
            const app = new Application(__dirname + path.sep + 'tests');
            await app.init();
            app.config.set('other' as keyof ConfigType, 'world');
            app.loadJsonConfig('param1');
            app.loadJsonConfig('param2', '', true);
            expect(app.config.getAll()).to.be.deep.equal({
                ...baseConfig,
                other: 'lol',
                aKey: 'FOOBAR',
                'sub.param1': 1,
                'sub.param2': 0,
                'sub.subsub.F': 'foo',
                'sub.subsub.B': 'BAR',
                'foobar.param': 'Hello',
            });
        });

        it('Should prefix keys', async () => {
            const app = new Application(__dirname + path.sep + 'tests');
            await app.init();
            app.config.set('other' as keyof ConfigType, 'world');
            app.loadJsonConfig('param1', 'apple');
            app.loadJsonConfig('param2', 'apple');
            expect(app.config.getAll()).to.be.deep.equal({
                ...baseConfig,
                other: 'world',
                'apple.aKey': 'foobar',
                'apple.sub.param1': 1,
                'apple.sub.param2': 2,
                'apple.sub.subsub.F': 'foo',
                'apple.sub.subsub.B': 'bar',
                'apple.foobar.param': 'Hello',
                'apple.other': 'lol',
            });
        });

        it('Should prefix keys, test2', async () => {
            const app = new Application(__dirname + path.sep + 'tests');
            await app.init();
            app.config.set('other' as keyof ConfigType, 'world');
            app.loadJsonConfig('param1', 'apple1');
            app.loadJsonConfig('param2', 'apple2');
            expect(app.config.getAll()).to.be.deep.equal({
                ...baseConfig,
                other: 'world',
                'apple1.aKey': 'foobar',
                'apple1.sub.param1': 1,
                'apple1.sub.param2': 2,
                'apple1.sub.subsub.F': 'foo',
                'apple1.sub.subsub.B': 'bar',
                'apple2.aKey': 'FOOBAR',
                'apple2.sub.param2': 0,
                'apple2.sub.subsub.B': 'BAR',
                'apple2.foobar.param': 'Hello',
                'apple2.other': 'lol',
            });
        });

        it('Should return the list of the loaded files', async () => {
            const app = new Application(__dirname + path.sep + 'tests');
            await app.init();
            app.config.set('other' as keyof ConfigType, 'world');
            expect(app.getJsonLoaded()).to.be.deep.equal([]);
            app.loadJsonConfig('param1');
            app.loadJsonConfig('param2');
            expect(app.getJsonLoaded()).to.be.deep.equal([
                __dirname + path.sep + 'tests' + path.sep + 'param1.json',
                __dirname + path.sep + 'tests' + path.sep + 'param2.json',
            ]);
        });
    });

    describe('run', () => {
        let stubExit: SinonStub;

        beforeEach(() => {
            stubExit = Sinon.stub(process, 'exit');
        });

        afterEach(() => {
            stubExit.restore();
        });

        it('Should run init if needed', async () => {
            let called = false;
            class MyApp extends Application {
                async init() {
                    called = true;
                    return super.init();
                }
            }
            const app = new MyApp('test');
            await app.init();
            expect(called).to.be.equal(true);
        });

        it('Should run the default command if no arg', async () => {
            const app = new Application('test');
            let runed = false;
            class DefaultCommand extends Command {
                public static readonly commandName = 'default';

                protected _execute() {
                    runed = true;

                    return Command.SUCCESS;
                }
            }
            await app.init();
            app.container.get('$commandRouter').addCommands([DefaultCommand]);
            app.container.get('$commandRouter').setDefaultCommand('default');
            await app.run();
            expect(runed).to.be.equal(true);
        });

        it('Should run a command selected by the command line', async () => {
            const backup = [...process.argv];
            process.argv.push('myCommand');

            const app = new Application('test');
            let runed = false;
            class MyCommand extends Command {
                public static readonly commandName = 'myCommand';

                protected _execute() {
                    runed = true;

                    return Command.SUCCESS;
                }
            }
            await app.init();
            app.container.get('$commandRouter').addCommands([MyCommand]);
            await app.run();
            expect(runed).to.be.equal(true);

            process.argv = backup;
        });

        it('Should run a default command if option before arguments', async () => {
            const backup = [...process.argv];
            process.argv.push('-f1', 'oobar', 'myCommand');

            const app = new Application('test');
            let runed = false;
            class DefaultCommand extends Command {
                public static readonly commandName = 'default';

                constructor() {
                    super();
                    this.addArgument(new Argument('<foobar...>', '')).addOption(
                        new Option('-f', '')
                    );
                }

                protected _execute() {
                    runed = true;

                    return Command.SUCCESS;
                }
            }
            class MyCommand extends Command {
                public static readonly commandName = 'myCommand';
            }
            await app.init();
            app.container
                .get('$commandRouter')
                .addCommands([DefaultCommand, MyCommand]);
            app.container.get('$commandRouter').setDefaultCommand('default');
            await app.run();
            expect(runed).to.be.equal(true);

            process.argv = backup;
        });

        it('Should give the $output and $outputErr services', async () => {
            const app = new Application('test');
            let outputGived: Output | undefined = undefined,
                outputErrGived: Output | undefined = undefined;
            class DefaultCommand extends Command {
                public static readonly commandName = 'default';

                protected _execute({ output, outputErr }: CommandRunArgs) {
                    outputGived = output;
                    outputErrGived = outputErr;

                    return Command.SUCCESS;
                }
            }
            await app.init();
            app.container.get('$commandRouter').addCommands([DefaultCommand]);
            app.container.get('$commandRouter').setDefaultCommand('default');
            await app.run();
            expect(outputGived).to.be.equal(app.container.get('$output'));
            expect(outputErrGived).to.be.equal(app.container.get('$outputErr'));
        });

        it('Should exit with the code return of the command', async () => {
            const app = new Application('test');
            class DefaultCommand extends Command {
                public static readonly commandName = 'default';

                protected _execute() {
                    return 42;
                }
            }
            await app.init();
            app.container.get('$commandRouter').addCommands([DefaultCommand]);
            app.container.get('$commandRouter').setDefaultCommand('default');

            const stub = Sinon.stub(app, 'exit');
            await app.run();
            expect(stub.getCalls()[0].args[0]).to.be.equal(42);
            stub.restore();
        });

        it('Should exit with the resolve result of the Promise if number', async () => {
            const app = new Application('test');
            class DefaultCommand extends Command {
                public static readonly commandName = 'default';

                protected _execute() {
                    return Promise.resolve(42);
                }
            }
            await app.init();
            app.container.get('$commandRouter').addCommands([DefaultCommand]);
            app.container.get('$commandRouter').setDefaultCommand('default');

            const stub = Sinon.stub(app, 'exit');
            await app.run();
            expect(stub.getCalls()[0].args[0]).to.be.equal(42);
            stub.restore();
        });

        it('Should catch and print the errors', async () => {
            const app = new Application('test');
            let error: Error | undefined;
            class DefaultCommand extends Command {
                public static readonly commandName = 'default';

                protected _execute() {
                    error = new Error();
                    throw error;

                    return Command.SUCCESS;
                }
            }
            await app.init();
            app.container.get('$commandRouter').addCommands([DefaultCommand]);
            app.container.get('$commandRouter').setDefaultCommand('default');

            const stub = Sinon.stub(
                app.container.get('$commandRouter'),
                'displayError'
            );
            await app.run();
            expect(stub.getCalls()[0].args[0]).to.be.equal(error);
            stub.restore();
        });

        it('Should intercept UnknownCommand', async () => {
            const backup = [...process.argv];
            process.argv.push('myCommand');

            const app = new Application('test');
            await app.init();
            const stub = Sinon.stub(
                app.container.get('$commandRouter'),
                'displayError'
            );
            await app.run();
            expect(stub.getCalls()[0].args[0]).to.be.instanceOf(UnknownCommand);
            expect(stubExit.getCalls()[0].args[0]).to.be.equal(2);
            stub.restore();

            process.argv = backup;
        });

        it('Should intercept UnknownOptionError', async () => {
            const backup = [...process.argv];
            process.argv.push('-f');

            const app = new Application('test');
            await app.init();
            const stub = Sinon.stub(
                app.container.get('$commandRouter'),
                'displayError'
            );
            await app.run();
            expect(stub.getCalls()[0].args[0]).to.be.instanceOf(
                UnknownOptionError
            );
            expect(stubExit.getCalls()[0].args[0]).to.be.equal(2);
            stub.restore();

            process.argv = backup;
        });

        it('Should exit with code 1 if error catched but with no code', async () => {
            const app = new Application('test');
            class DefaultCommand extends Command {
                public static readonly commandName = 'default';

                protected _execute() {
                    throw new Error();

                    return Command.SUCCESS;
                }
            }
            await app.init();
            app.container.get('$commandRouter').addCommands([DefaultCommand]);
            app.container.get('$commandRouter').setDefaultCommand('default');

            const stub = Sinon.stub(app, 'exit');
            await app.run();
            expect(stub.getCalls()[0].args[0]).to.be.equal(1);
            stub.restore();
        });
    });

    describe('exit', () => {
        let stubExit: SinonStub;
        let stubOutput: SinonStub;
        let keepNodeEnv: string | undefined;

        beforeEach(() => {
            keepNodeEnv = process.env.NODE_ENV;
            stubExit = Sinon.stub(process, 'exit');
        });

        afterEach(() => {
            if (keepNodeEnv !== undefined) {
                process.env.NODE_ENV = keepNodeEnv;
            } else {
                delete process.env.NODE_ENV;
            }
            stubExit.restore();
            if (stubOutput) stubOutput.restore();
        });

        it('Should exit the application', async () => {
            const app = new Application('test');
            await app.init();
            await app.exit();
            expect(stubExit.called).to.be.equal(true);
        });

        it('Should exit the application with code', async () => {
            const app = new Application('test');
            await app.init();
            await app.exit(42);
            expect(stubExit.getCalls()[0].args[0]).to.be.equal(42);
        });

        it('Should call shutdown method', async () => {
            let called = false;
            class MyApp extends Application {
                async shutdown() {
                    called = true;
                    return super.shutdown();
                }
            }
            const app = new MyApp('test');
            await app.init();
            await app.exit(0);
            expect(called).to.be.equal(true);
        });

        it('Should not print the profiler if not development env', async () => {
            const app = new Application('test');
            await app.init();
            stubOutput = Sinon.stub(app.container.get('$output'), 'write');
            await app.exit();
            expect(stubOutput.notCalled).to.be.equal(true);
        });

        it('Should print the profiler if development env', async () => {
            process.env.NODE_ENV = 'development';
            const app = new Application('test');
            await app.init();
            stubOutput = Sinon.stub(app.container.get('$output'), 'write');
            await app.exit();
            expect(stubOutput.getCalls()[0].args[0]).to.be.equal(
                '<fg=bright-red>Profiler:</>'
            );
            expect(stubOutput.getCalls()[1].args[0]).to.be.contain(
                'app state setup'
            );
            expect(stubOutput.getCalls()[1].args[0]).to.be.contain('ms</>');
        });

        it('Should print the profiler in verbosity DEBUG', async () => {
            process.env.NODE_ENV = 'development';
            const app = new Application('test');
            await app.init();
            stubOutput = Sinon.stub(app.container.get('$output'), 'write');
            await app.exit();
            expect(stubOutput.getCalls()[0].args[2]).to.be.equal(
                Output.VERBOSITY.DEBUG
            );
            expect(stubOutput.getCalls()[1].args[2]).to.be.equal(
                Output.VERBOSITY.DEBUG
            );
        });
    });
});
