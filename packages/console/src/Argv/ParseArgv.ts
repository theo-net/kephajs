/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export class ParseArgv {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected _argv: Record<string, any> = { _: [] };

    protected _options: Record<string, string | number | boolean> = {};

    /**
     * Set options (erase options already registered)
     */
    setOptions(opts: Record<string, string | number | boolean>) {
        this._options = opts;
    }

    getOptions() {
        return this._options;
    }

    parse(args: (string | number)[]) {
        this._argv = { _: [] };

        // ignore all after '--'
        let notFlags: (string | number)[] = [];
        const notFlagsIndex = args.indexOf('--');
        if (notFlagsIndex > -1) {
            notFlags = args.slice(notFlagsIndex + 1);
            args = args.slice(0, notFlagsIndex);
        }

        // Read the args list
        for (let index = 0; index < args.length; index++) {
            const arg = args[index];
            const nextArg = args[index + 1];
            let broken = true;

            // --*=*
            if (/^--.+=/.test(arg as string)) {
                const match = (arg as string).match(
                    /^--([^=]+)=([\s\S]*)$/
                ) as string[];
                this._setArg(match[1], match[2]);
            }
            // --no-*
            else if (/^--no-.+/.test(arg as string)) {
                this._setArg(
                    ((arg as string).match(/^--no-(.+)/) as string[])[1],
                    false
                );
            }
            // --*
            else if (/^--.+/.test(arg as string)) {
                const key = ((arg as string).match(/^--(.+)/) as string[])[1];
                const next = args[index + 1] as string;

                if (next !== undefined && !/^-/.test(next)) {
                    this._setArg(key, next);
                    index++;
                } else if (/^(true|false)$/.test(next)) {
                    this._setArg(key, next === 'true');
                    index++;
                } else this._setArg(key, true);
            }
            // -*
            else if (/^-[^-]+/.test(arg as string)) {
                broken = false;
                const letters = (arg as string).slice(1, -1).split('');
                letters.every((letter, idx) => {
                    const next = (arg as string).slice(idx + 2);
                    if (next === '-') {
                        this._setArg(letter, next);
                        return true;
                    } else {
                        if (/[A-Za-z]/.test(letter) && /=/.test(next)) {
                            this._setArg(letter, next.split('=')[1]);
                            broken = true;
                            return false;
                        }
                        if (
                            /[A-Za-z]/.test(letter) &&
                            /-?\d+(\.\d*)?(e-?\d+)?$/.test(next)
                        ) {
                            this._setArg(letter, next);
                            broken = true;
                            return false;
                        }
                        if (letters[idx + 1] && letters[idx + 1].match(/\W/)) {
                            this._setArg(letter, next);
                            broken = true;
                            return false;
                        } else {
                            this._setArg(letter, true);
                            return true;
                        }
                    }
                });

                // For not --* args
                const key = (arg as string).slice(-1)[0];
                if (!broken && key !== '-') {
                    // next arg is the value of the current arg
                    if (nextArg && !/^(-|--)[^-]/.test(nextArg as string)) {
                        this._setArg(key, nextArg);
                        index++;
                    } else this._setArg(key, true);
                }
            } else {
                if (
                    typeof arg === 'string' &&
                    (/^0x[0-9a-f]+$/i.test(arg) ||
                        /^[-+]?(?:\d+(?:\.\d*)?|\.\d+)(e[-+]?\d+)?$/.test(arg))
                ) {
                    this._argv._.push(Number(arg));
                } else {
                    this._argv._.push(arg);
                }

                if (this._options.stopEarly) {
                    // merge args in argv
                    Array.prototype.push.apply(
                        this._argv._,
                        args.slice(index + 1)
                    );
                    break;
                }
            }
        }

        // We want to get the content after '--'
        if (this._options['--']) {
            this._argv['--'] = notFlags;
        } else {
            notFlags.forEach(key => {
                this._argv._.push(key);
            });
        }

        return this._argv;
    }

    private _setArg(key: string, value: string | number | boolean) {
        if (
            typeof value === 'string' &&
            (/^0x[0-9a-f]+$/i.test(value) ||
                /^[-+]?(?:\d+(?:\.\d*)?|\.\d+)(e[-+]?\d+)?$/.test(value))
        ) {
            value = Number(value);
        }
        this._setKey(key.split('.'), value);
    }

    private _setKey(keys: string[], value: string | number | boolean) {
        let obj = this._argv;

        // nested dotted objects
        keys.slice(0, -1).forEach(key => {
            if (obj[key] === undefined) {
                obj[key] = {};
            }
            obj = obj[key];
        });

        const key = keys[keys.length - 1];

        // Key undefined or boolean
        if (obj[key] == undefined) {
            obj[key] = value;
        }
        // Key as already an array: add new value
        else if (Array.isArray(obj[key])) {
            (obj[key] as (string | number | boolean)[]).push(value);
        }
        // else transform the value to an array
        else {
            obj[key] = [obj[key], value];
        }
    }
}
