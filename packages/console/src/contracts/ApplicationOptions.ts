/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ObjectType } from '@kephajs/core/Validate/Type/Object';
import { Validator } from '@kephajs/core/Validate/Validator';

declare module '@kephajs/core/ApplicationOptions' {
    interface ApplicationOptions {
        argv?: (string | number)[];
        color?: boolean;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        envValidator?: (
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            defaultValidator: ObjectType<any>,
            $validator: Validator
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        ) => ObjectType<any>;
        envFiles?: string[];
    }
}
