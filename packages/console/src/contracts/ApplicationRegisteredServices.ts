/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { CommandRouter } from '../Command/CommandRouter';
import { Env } from '../Env/Env';
import { Output } from '../Output/Output';
import { OutputFormatter } from '../Output/OutputFormatter';
import { TableManager } from '../Table/TableManager';

declare module '@kephajs/core/ApplicationRegisteredServices' {
    interface ApplicationRegisteredServices {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        $argv: Record<string, any>;
        $commandRouter: CommandRouter;
        $env: Env;
        $output: Output;
        $outputErr: Output;
        $outputFormatter: OutputFormatter;
        $tableManager: TableManager;
    }
}
