/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare module '@kephajs/core/Config/ConfigType' {
    interface ConfigType {
        'ENV.FORCE_COLOR'?: number;
        'ENV.NO_COLOR?'?: boolean;
        'ENV.NODE_DISABLE_COLORS'?: boolean;
        'ENV.NODE_ENV': string;
    }
}

// Because no importation
export {};
