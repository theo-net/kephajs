/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import * as ConsoleIndex from './index';
import { Helpers as CoreHelpers } from '@kephajs/core';
import { Env } from './Env/Env';
import * as Helpers from './Helpers/Helpers';

describe('@kephajs/console', () => {
    it('Should export Env', () => {
        expect(ConsoleIndex.Env).to.contain(Env);
    });

    it('Should export console Helpers', () => {
        expect(ConsoleIndex.Helpers).to.contain(Helpers);
    });

    it('Should export @kephajs/core/Helpers', () => {
        expect(ConsoleIndex.Helpers).to.contain(CoreHelpers);
    });
});
