/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as path from 'path';
import { expect } from 'chai';

import { Config } from '@kephajs/core/Config/Config';
import { Env } from './Env';
import { ObjectType } from '@kephajs/core/Validate/Type/Object';
import {
    ErrorValidator,
    IssueCode,
    Validator,
} from '@kephajs/core/Validate/Validator';

describe('@kephajs/console/Env/Env', () => {
    const validator = new Validator();
    let config: Config;
    let env: Env<MyEnv>;

    function envValidator(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        defaultValidator: ObjectType<any>,
        $validator: Validator
    ) {
        return defaultValidator.extend({
            FOO: $validator.string(),
            OPTIONAL: $validator.string().optional(),
            A_NUMBER: $validator.preprocess(
                value => Number(value),
                $validator.number()
            ),
            BASIC: $validator.string(),
        });
    }

    interface MyEnv {
        FOO: string;
        OPTIONAL?: string;
        A_NUMBER: number;
        BASIC: string;
    }

    beforeEach(() => {
        process.env.FOO = 'bar';
        process.env.OPTIONAL = 'a foo${FOO}!';
        process.env.A_NUMBER = '42';
        process.env.BASIC = 'process';
        config = new Config();
        env = new Env<MyEnv>(validator, config).init([], envValidator);
    });
    afterEach(() => {
        delete process.env.FOO;
        delete process.env.OPTIONAL;
        delete process.env.A_NUMBER;
        delete process.env.BASIC;
    });

    describe('get', () => {
        it('Should preprocess the value', () => {
            expect(typeof env.get('A_NUMBER')).is.equal('number');
            expect(env.get('A_NUMBER')).is.equal(42);
        });
    });

    describe('has', () => {
        it("Should return false if the key don't exist", () => {
            expect(env.has('foo')).to.be.equal(false);
        });

        it('Should return true if the key exists', () => {
            expect(env.has('FOO')).to.be.equal(true);
        });
    });

    describe('load values', () => {
        it('Should validate the values', () => {
            process.env.A_NUMBER = 'foobar';
            try {
                env = new Env<MyEnv>(validator, config).init([], envValidator);
            } catch (error) {
                expect(error).to.be.instanceOf(ErrorValidator);
                expect((error as ErrorValidator).errors[0].code).to.be.equal(
                    IssueCode.invalidType
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should register the values in Config Service', () => {
            expect(config.get('ENV.FOO')).to.be.equal('bar');
        });

        it('Should use environment variables in values', () => {
            expect(env.get('OPTIONAL')).to.be.equal('a foobar!');
            expect(config.get('ENV.OPTIONAL')).to.be.equal('a foobar!');
        });
    });

    describe('parse .env basic', () => {
        const parsed = Env.parseFile(__dirname + path.sep + '.env');

        const expectedPayload = {
            SERVER: 'localhost',
            PASSWORD: 'password',
            DB: 'tests',
        };

        it('Should set basic environment variable', () => {
            expect(parsed.BASIC).to.be.equal('basic');
        });

        it('Should reads after a skipped line', () => {
            expect(parsed.AFTER_LINE).to.be.equal('after_line');
        });

        it('Should default empty values to empty string', () => {
            expect(parsed.EMPTY).to.be.equal('');
        });

        it('Should default empty values to empty string', () => {
            expect(parsed.EMPTY_SINGLE_QUOTES).to.be.equal('');
        });

        it('Should defaults empty values to empty string', () => {
            expect(parsed.EMPTY_DOUBLE_QUOTES).to.be.equal('');
        });

        it('Should default empty values to empty string', () => {
            expect(parsed.EMPTY_BACKTICKS).to.be.equal('');
        });

        it('Should escape single quoted values', () => {
            expect(parsed.SINGLE_QUOTES).to.be.equal('single_quotes');
        });

        it('Should respect surrounding spaces in single quotes', () => {
            expect(parsed.SINGLE_QUOTES_SPACED).to.be.equal(
                '    single quotes    '
            );
        });

        it('Should escape double quoted values', () => {
            expect(parsed.DOUBLE_QUOTES).to.be.equal('double_quotes');
        });

        it('Should respect surrounding spaces in double quotes', () => {
            expect(parsed.DOUBLE_QUOTES_SPACED).to.be.equal(
                '    double quotes    '
            );
        });

        it('Should respect double quotes inside single quote', () => {
            expect(parsed.DOUBLE_QUOTES_INSIDE_SINGLE).to.be.equal(
                'double "quotes" work inside single quotes'
            );
        });

        it('Should respect spacing for badly formed brackets', () => {
            expect(parsed.DOUBLE_QUOTES_WITH_NO_SPACE_BRACKET).to.be.equal(
                '{ port: $MONGOLAB_PORT}'
            );
        });

        it('Should respect single quotes inside double quotes', () => {
            expect(parsed.SINGLE_QUOTES_INSIDE_DOUBLE).to.be.equal(
                "single 'quotes' work inside double quotes"
            );
        });

        it('Should respect backticks inside single quotes', () => {
            expect(parsed.BACKTICKS_INSIDE_SINGLE).to.be.equal(
                '`backticks` work inside single quotes'
            );
        });

        it('Should respect backticks inside double quotes', () => {
            expect(parsed.BACKTICKS_INSIDE_DOUBLE).to.be.equal(
                '`backticks` work inside double quotes'
            );
        });

        it('Should parse backticks string', () => {
            expect(parsed.BACKTICKS).to.be.equal('backticks');
        });

        it('Should respects space insite backticks', () => {
            expect(parsed.BACKTICKS_SPACED).to.be.equal('    backticks    ');
        });

        it('Should respect double quotes inside backticks', () => {
            expect(parsed.DOUBLE_QUOTES_INSIDE_BACKTICKS).to.be.equal(
                'double "quotes" work inside backticks'
            );
        });

        it('Should respect single quotes inside backticks', () => {
            expect(parsed.SINGLE_QUOTES_INSIDE_BACKTICKS).to.be.equal(
                "single 'quotes' work inside backticks"
            );
        });

        it('Should respect single quotes inside backticks', () => {
            expect(
                parsed.DOUBLE_AND_SINGLE_QUOTES_INSIDE_BACKTICKS
            ).to.be.equal(
                'double "quotes" and single \'quotes\' work inside backticks'
            );
        });

        it('Should expand newlines but only if double quoted', () => {
            expect(parsed.EXPAND_NEWLINES).to.be.equal('expand\nnew\nlines');
        });

        it('Should expand newlines but only if double quoted', () => {
            expect(parsed.DONT_EXPAND_UNQUOTED).to.be.equal(
                'dontexpand\\nnewlines'
            );
        });

        it('Should expand newlines but only if double quoted', () => {
            expect(parsed.DONT_EXPAND_SQUOTED).to.be.equal(
                'dontexpand\\nnewlines'
            );
        });

        it('Should ignores commented lines', () => {
            expect(parsed.COMMENTS).to.be.not.equal('');
        });

        it('Should ignore inline comments', () => {
            expect(parsed.INLINE_COMMENTS).to.be.equal('inline comments');
        });

        it('Should ignore inline comments and respects # character inside of single quotes', () => {
            expect(parsed.INLINE_COMMENTS_SINGLE_QUOTES).to.be.equal(
                'inline comments outside of #singlequotes'
            );
        });

        it('Should ignore inline comments and respects # character inside of double quotes', () => {
            expect(parsed.INLINE_COMMENTS_DOUBLE_QUOTES).to.be.equal(
                'inline comments outside of #doublequotes'
            );
        });

        it('Should ignore inline comments and respects # character inside of backticks', () => {
            expect(parsed.INLINE_COMMENTS_BACKTICKS).to.be.equal(
                'inline comments outside of #backticks'
            );
        });

        it('Should treat # character as start of comment', () => {
            expect(parsed.INLINE_COMMENTS_SPACE).to.be.equal(
                'inline comments start with a'
            );
        });

        it('Should respect equals signs in values', () => {
            expect(parsed.EQUAL_SIGNS).to.be.equal('equals==');
        });

        it('Should retain inner quotes', () => {
            expect(parsed.RETAIN_INNER_QUOTES).to.be.equal('{"foo": "bar"}');
        });

        it('Should respect equals signs in values', () => {
            expect(parsed.EQUAL_SIGNS).to.be.equal('equals==');
        });

        it('Should retain inner quotes', () => {
            expect(parsed.RETAIN_INNER_QUOTES).to.be.equal('{"foo": "bar"}');
        });

        it('Should retain inner quotes', () => {
            expect(parsed.RETAIN_INNER_QUOTES_AS_STRING).to.be.equal(
                '{"foo": "bar"}'
            );
        });

        it('Should retain inner quotes', () => {
            expect(parsed.RETAIN_INNER_QUOTES_AS_BACKTICKS).to.be.equal(
                '{"foo": "bar\'s"}'
            );
        });

        it('Should retain spaces in string', () => {
            expect(parsed.TRIM_SPACE_FROM_UNQUOTED).to.be.equal(
                'some spaced out string'
            );
        });

        it('Should parse email addresses completely', () => {
            expect(parsed.USERNAME).to.be.equal(
                'therealnerdybeast@example.tld'
            );
        });

        it('Should parse keys and values surrounded by spaces', () => {
            expect(parsed.SPACED_KEY).to.be.equal('parsed');
        });

        it('Should can parse (\\r) line endings', () => {
            const RPayload = Env.parse(
                'SERVER=localhost\rPASSWORD=password\rDB=tests\r'
            );
            expect(RPayload).to.be.deep.equal(expectedPayload);
        });

        it('Should can parse (\\n) line endings', () => {
            const NPayload = Env.parse(
                'SERVER=localhost\nPASSWORD=password\nDB=tests\n'
            );
            expect(NPayload).to.be.deep.equal(expectedPayload);
        });

        it('Should can parse (\\rn) line endings', () => {
            const RNPayload = Env.parse(
                'SERVER=localhost\r\nPASSWORD=password\r\nDB=tests\r\n'
            );
            expect(RNPayload).to.be.deep.equal(expectedPayload);
        });
    });

    describe('parse .env multiline', () => {
        const parsed = Env.parseFile(__dirname + path.sep + '.env-multiline');

        it('Should parse multi-line strings when using double quotes', () => {
            expect(parsed.MULTI_DOUBLE_QUOTED).to.be.equal(
                'THIS\nIS\nA\nMULTILINE\nSTRING'
            );
        });

        it('Should parse multi-line strings when using single quotes', () => {
            expect(parsed.MULTI_SINGLE_QUOTED).to.be.equal(
                'THIS\nIS\nA\nMULTILINE\nSTRING'
            );
        });

        it('Should parse multi-line strings when using single quotes', () => {
            expect(parsed.MULTI_BACKTICKED).to.be.equal(
                'THIS\nIS\nA\n"MULTILINE\'S"\nSTRING'
            );
        });
    });
});
