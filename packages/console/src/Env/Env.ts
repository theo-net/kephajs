/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Config } from '@kephajs/core/Config/Config';
import { ObjectType } from '@kephajs/core/Validate/Type/Object';
import { Validator } from '@kephajs/core/Validate/Validator';
import { existsSync, readFileSync } from 'fs';

import { EnvType } from './EnvType';

export function defaultEnvValidator(validator: Validator) {
    return validator.object({
        FORCE_COLOR: validator
            .preprocess(forceColor => Number(forceColor), validator.number())
            .optional(),
        NO_COLOR: validator
            .preprocess(() => true, validator.boolean())
            .optional(),
        NODE_DISABLE_COLORS: validator
            .preprocess(() => true, validator.boolean())
            .optional(),
        NODE_ENV: validator.preprocess(nodeEnv => {
            switch (nodeEnv) {
                case 'dev':
                case 'develop':
                case 'development':
                    return 'development';

                case 'stage':
                case 'staging':
                    return 'staging';

                case 'prod':
                case 'production':
                    return 'production';

                case 'test':
                case 'testing':
                    return 'testing';

                default:
                    return 'production';
            }
        }, validator.union([validator.literal('development'), validator.literal('staging'), validator.literal('production'), validator.literal('testing')]).optional()),
        SHELL_VERBOSITY: validator
            .preprocess(
                shellVerbosity => Number(shellVerbosity),
                validator.number().min(-1).max(3)
            )
            .optional(),
    });
}

const LINE =
    /(?:^|^)\s*(?:export\s+)?([\w.-]+)(?:\s*=\s*?|:\s+?)(\s*'(?:\\'|[^'])*'|\s*"(?:\\"|[^"])*"|\s*`(?:\\`|[^`])*`|[^#\r\n]+)?\s*(?:#.*)?(?:$|$)/gm;

/**
 * This module enables to use of environment variables by parsing dotfiles syntax
 * and updates the `process.env` object in Node.js.
 */
export class Env<TypeEnv = EnvType> {
    // A cache of env values
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private _envCache: { [key: string]: any } = {};

    private _loadedFiles: { [key: string]: boolean } = {};

    /**
     * Read and parse the value from process.env
     */
    constructor(
        private readonly _validator: Validator,
        private readonly _config: Config
    ) {}

    static $inject = ['$validator', '$config'];

    /**
     * Return the environment variables value.
     */
    get<T>(key: keyof TypeEnv) {
        if (!this.has(key as string)) {
            throw TypeError(`The key [${key as string}] isn't defined`);
        }
        return <T>(<unknown>this._envCache[key as string]);
    }

    /**
     * Return all values
     */
    getAll() {
        return this._envCache;
    }

    /**
     * Returns if the key is defined
     */
    has(key: string) {
        return key in this._envCache;
    }

    static parse(lines: string) {
        const obj: { [key: string]: string } = {};

        // Convert line breaks to same format
        lines = lines.replace(/\r\n?/gm, '\n');

        let match;
        while ((match = LINE.exec(lines)) != null) {
            const key = match[1];

            // Default undefined or null to empty string
            let value = match[2] || '';

            // Remove whitespace
            value = value.trim();

            // Check if double quoted
            const maybeQuote = value[0];

            // Remove surrounding quotes
            value = value.replace(/^(['"`])([\s\S]*)\1$/gm, '$2');

            // Expand newlines if double quoted
            if (maybeQuote === '"') {
                value = value.replace(/\\n/g, '\n');
                value = value.replace(/\\r/g, '\r');
            }

            // Add to object
            obj[key] = value;
        }

        return obj;
    }

    static parseFile(src: string) {
        return this.parse(readFileSync(src, { encoding: 'utf8' }).toString());
    }

    /**
     * Read and parse the value from process.env
     */
    init(
        files: string[] = [],
        validator?: (
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            defaultValidator: ObjectType<any>,
            $validator: Validator
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        ) => ObjectType<any>
    ) {
        // Get schema
        const schema = validator
            ? validator(defaultEnvValidator(this._validator), this._validator)
            : defaultEnvValidator(this._validator);

        // Merge .env file and process.env
        let envLoaded: NodeJS.ProcessEnv = {};
        files.forEach(file => {
            if (existsSync(file)) {
                envLoaded = { ...envLoaded, ...Env.parseFile(file) };
                this._loadedFiles[file] = true;
            } else {
                this._loadedFiles[file] = false;
            }
        });
        envLoaded = { ...envLoaded, ...process.env };

        // Use environment variables in values
        Object.getOwnPropertyNames(envLoaded).forEach(key => {
            envLoaded[key] = envLoaded[key]?.replace(
                /\$\{([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff.]+)\}/g,
                (_match: string, p1: string): string => {
                    return typeof envLoaded[p1] === 'string'
                        ? (envLoaded[p1] as string)
                        : '';
                }
            );
        });

        // Register the values
        const envParsed = schema.parse(envLoaded);
        Object.getOwnPropertyNames(envParsed).forEach(key => {
            this._envCache[key] = envParsed[key as keyof typeof envParsed];
        });

        // Register env in $config
        Object.getOwnPropertyNames(this.getAll()).forEach(key => {
            this._config.setReadonly(
                'ENV.' + key,
                this.get(key as keyof TypeEnv)
            );
        });

        return this;
    }

    /**
     * Return loaded files list
     */
    getLoadedFiles() {
        return this._loadedFiles;
    }
}
