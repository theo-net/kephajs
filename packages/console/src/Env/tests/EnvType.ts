/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare module '@kephajs/console/Env/EnvType' {
    interface EnvType {
        BASIC: string;
    }
}

// Because no importation
export {};
