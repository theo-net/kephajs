/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Profiler } from '@kephajs/core/Profiler';
import { TabulateList } from './Output/Helpers/TabulateList';

export class ProfilerOutput {
    constructor(private _profiler: Profiler) {}

    render() {
        const tabulateList = new TabulateList({
            prefix: ['    '],
            styles: ['fg=gray'],
        });
        this._profiler.getAll().forEach(element => {
            tabulateList.push([element.duration + 'ms', element.description]);
        });

        return tabulateList.render();
    }
}
