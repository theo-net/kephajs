/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Logger as CoreLogger } from '@kephajs/core/Logger/Logger';
import { Output } from '../Output/Output';

export class Logger extends CoreLogger {
    public static $inject = ['$output', '$outputErr'];

    constructor(private _output: Output, private _outputErr: Output) {
        super();
    }

    protected _printError(message: string) {
        this._outputErr.writeln(
            '<error> ERROR </> ' + message,
            Output.VERBOSITY.QUIET
        );
    }

    protected _printWarning(message: string) {
        this._output.writeln(
            '<bg=yellow;fg=white> WARNING </> ' + message,
            Output.VERBOSITY.NORMAL
        );
    }

    protected _printNotice(message: string) {
        this._output.writeln(
            '<bg=blue;fg=white> NOTICE </> ' + message,
            Output.VERBOSITY.VERBOSE
        );
    }

    protected _printInfo(message: string) {
        this._output.writeln(
            '<bg=green;fg=white> INFO </> ' + message,
            Output.VERBOSITY.VERY_VERBOSE
        );
    }

    protected _printDebug(message: string) {
        this._output.writeln(
            '<bg=white;fg=black> DEBUG </> ' + message,
            Output.VERBOSITY.DEBUG
        );
    }
}
