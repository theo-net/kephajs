/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import Sinon from 'sinon';
import { WriteStream } from 'tty';

import { Logger } from './Logger';
import { Output } from '../Output/Output';
import { OutputFormatter } from '../Output/OutputFormatter';

describe('@kephajs/console/Logger/Logger', () => {
    const formatter = new OutputFormatter();
    let logger: Logger;
    let output: Output;
    let outputErr: Output;
    let stream: WriteStream;
    let stubOutput: Sinon.SinonStub;
    let stubOutputErr: Sinon.SinonStub;

    beforeEach(() => {
        stream = process.stdout; //new WriteStream(5);
        output = new Output();
        output.setStream(stream);
        output.setFormatter(formatter);
        outputErr = new Output();
        outputErr.setStream(stream);
        outputErr.setFormatter(formatter);
        logger = new Logger(output, outputErr);
        stubOutput = Sinon.stub(output, 'writeln');
        stubOutputErr = Sinon.stub(outputErr, 'writeln');
    });

    afterEach(() => {
        stubOutputErr.restore();
        stubOutput.restore();
    });

    describe('error', () => {
        it('Should print the message with $outputErr', () => {
            logger.error('test');
            expect(stubOutputErr.getCalls()[0].args[0]).to.be.contain('test');
        });

        it('Should format the prefix', () => {
            logger.error('test');
            expect(stubOutputErr.getCalls()[0].args[0]).to.be.equal(
                '<error> ERROR </> test'
            );
        });

        it('Should have verbosity QUIET', () => {
            logger.error('test');
            expect(stubOutputErr.getCalls()[0].args[1]).to.be.deep.equal(
                Output.VERBOSITY.QUIET
            );
        });
    });

    describe('warning', () => {
        it('Should print the message with $output', () => {
            logger.warning('test');
            expect(stubOutput.getCalls()[0].args[0]).to.be.contain('test');
        });

        it('Should format the prefix', () => {
            logger.warning('test');
            expect(stubOutput.getCalls()[0].args[0]).to.be.equal(
                '<bg=yellow;fg=white> WARNING </> test'
            );
        });

        it('Should have verbosity NORMAL', () => {
            logger.warning('test');
            expect(stubOutput.getCalls()[0].args[1]).to.be.deep.equal(
                Output.VERBOSITY.NORMAL
            );
        });
    });

    describe('notice', () => {
        it('Should print the message with $output', () => {
            logger.notice('test');
            expect(stubOutput.getCalls()[0].args[0]).to.be.contain('test');
        });

        it('Should format the prefix', () => {
            logger.notice('test');
            expect(stubOutput.getCalls()[0].args[0]).to.be.equal(
                '<bg=blue;fg=white> NOTICE </> test'
            );
        });

        it('Should have verbosity VERBOSE', () => {
            logger.notice('test');
            expect(stubOutput.getCalls()[0].args[1]).to.be.deep.equal(
                Output.VERBOSITY.VERBOSE
            );
        });
    });

    describe('info', () => {
        it('Should print the message with $output', () => {
            logger.info('test');
            expect(stubOutput.getCalls()[0].args[0]).to.be.contain('test');
        });

        it('Should format the prefix', () => {
            logger.info('test');
            expect(stubOutput.getCalls()[0].args[0]).to.be.equal(
                '<bg=green;fg=white> INFO </> test'
            );
        });

        it('Should have verbosity VERY_VERBOSE', () => {
            logger.info('test');
            expect(stubOutput.getCalls()[0].args[1]).to.be.deep.equal(
                Output.VERBOSITY.VERY_VERBOSE
            );
        });
    });

    describe('debug', () => {
        it('Should print the message with $output', () => {
            logger.debug('test');
            expect(stubOutput.getCalls()[0].args[0]).to.be.contain('test');
        });

        it('Should format the prefix', () => {
            logger.debug('test');
            expect(stubOutput.getCalls()[0].args[0]).to.be.equal(
                '<bg=white;fg=black> DEBUG </> test'
            );
        });

        it('Should have verbosity DEBUG', () => {
            logger.debug('test');
            expect(stubOutput.getCalls()[0].args[1]).to.be.deep.equal(
                Output.VERBOSITY.DEBUG
            );
        });
    });
});
