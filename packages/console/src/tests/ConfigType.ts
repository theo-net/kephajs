/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare module '@kephajs/core/Config/ConfigType' {
    interface ConfigType {
        'ENV.NODE_ENV': string;
        foobar: string;
        'ENV.FOOBAR': string;
    }
}

// Because no importation
export {};
