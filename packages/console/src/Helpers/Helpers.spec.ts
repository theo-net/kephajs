/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import * as Helpers from './Helpers';

describe('@kephajs/console/Helpers', () => {
    describe('NodeVersion', () => {
        it('Should export the current Node version', () => {
            expect(Helpers.NodeVersion).to.be.equal(process.version);
        });
    });

    describe('strlenMaxLineStylized', () => {
        it('Should return length of string with style', () => {
            expect(
                Helpers.strlenMaxLineStylized('<error>hel</error>lo')
            ).to.equal(5);
        });

        it('Should return length of string with custom style', () => {
            expect(
                Helpers.strlenMaxLineStylized('<options=bold,blink>hel</>lo')
            ).to.equal(5);
        });

        it('Should return length of string with escaped style', () => {
            expect(
                Helpers.strlenMaxLineStylized('\\<error>hel\\</>lo')
            ).to.equal(15);
        });
    });

    describe('wordWrapStylized', () => {
        it('Should wrap a string with formatting', () => {
            const input =
                '<red>Hello, how are</>' +
                '<blue> you today? I</blue>' +
                '<green> am fine, thank you!</green>';

            const expected =
                '<red>Hello, how\nare</>' +
                '<blue> you\ntoday? I</blue>' +
                '<green>\nam fine,\nthank you!</green>';

            expect(Helpers.wordWrapStylized(10, input).join('\n')).to.equal(
                expected
            );
        });
    });

    describe('truncateStylized', () => {
        it('truncateStylized("hello", 5) === "hello"', () => {
            expect(Helpers.truncateStylized('hello', 5)).to.equal('hello');
        });

        it('truncateStylized("hello sir", 7, "…") == "hello …"', () => {
            expect(Helpers.truncateStylized('hello sir', 7, '…')).to.equal(
                'hello …'
            );
        });

        it('truncateStylized("hello sir", 6, "…") == "hello…"', () => {
            expect(Helpers.truncateStylized('hello sir', 6, '…')).to.equal(
                'hello…'
            );
        });

        it('truncateStylized("goodnight moon", 8, "…") == "goodnig…"', () => {
            expect(Helpers.truncateStylized('goodnight moon', 8, '…')).to.equal(
                'goodnig…'
            );
        });

        it('truncateStylized("<red>goodnight moon</red>"), 15, "…") == "<red>goodnight moon</>"', () => {
            const original = '<red>goodnight moon</red>';
            const expected = '<red>goodnight moon</red>';
            expect(Helpers.truncateStylized(original, 15, '…')).to.equal(
                expected
            );
        });

        it('truncateStylized("<red>goodnight moon</red>", 8, "…") == "<red>goodnig</>…"', () => {
            const original = '<red>goodnight moon</red>';
            const expected = '<red>goodnig</>…';
            expect(Helpers.truncateStylized(original, 8, '…')).to.equal(
                expected
            );
        });

        it('truncateStylized("<red>goodnight moon"</>, 9, "…") == "<red>goodnig</>…"', () => {
            const original = '<red>goodnight moon</>';
            const expected = '<red>goodnigh</>…';
            expect(Helpers.truncateStylized(original, 9, '…')).to.equal(
                expected
            );
        });

        it('truncateStylized("\\<red>goodnight moon"\\</>, 9, "…") == "<red>goodnig</>…"', () => {
            const original = '\\<red>goodnight moon\\</>';
            const expected = '\\<red>go…';
            expect(Helpers.truncateStylized(original, 9, '…')).to.equal(
                expected
            );
        });

        it('"<red>hello</> <green>world</>" truncated to 9 chars', () => {
            const original = '<red>hello</> <green>world</>';
            const expected = '<red>hello</> <green>wo</>…';
            expect(Helpers.truncateStylized(original, 9)).to.equal(expected);
        });

        it('"<red>Hel<italic>lo world</></>" truncated to 9 chars', () => {
            const original = '<red>Hel<italic>lo world</></>';
            const expected = '<red>Hel<italic>lo wo</></>' + '…';
            expect(Helpers.truncateStylized(original, 9)).to.equal(expected);
        });

        it('truncateWidth("漢字テスト", 15) === "漢字テスト"', () => {
            expect(Helpers.truncateStylized('漢字テスト', 15)).to.equal(
                '漢字テスト'
            );
        });

        it('truncateWidth("漢字テスト", 6) === "漢字…"', () => {
            expect(Helpers.truncateStylized('漢字テスト', 6)).to.equal('漢字…');
        });

        it('truncateWidth("漢字テスト", 5) === "漢字…"', () => {
            expect(Helpers.truncateStylized('漢字テスト', 5)).to.equal('漢字…');
        });

        it('truncateWidth("漢字testてすと", 12) === "漢字testて…"', () => {
            expect(Helpers.truncateStylized('漢字testてすと', 12)).to.equal(
                '漢字testて…'
            );
        });
    });
});
