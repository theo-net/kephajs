/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import fsPromise from 'node:fs/promises';
import path from 'path';

export function rmrf(deletePath: string) {
    return fsPromise
        .stat(deletePath)
        .then(stat => {
            if (stat.isFile()) {
                return fsPromise.unlink(deletePath).catch(() => {
                    throw new Error(
                        `console/Helpers/Filesystem.rmrf: can't delete ${deletePath}.`
                    );
                });
            } else if (stat.isDirectory()) {
                return fsPromise.readdir(deletePath).then(files => {
                    const promises: Promise<void>[] = [];
                    files.forEach(file => {
                        promises.push(rmrf(path.join(deletePath, file)));
                    });
                    return Promise.all(promises).then(() =>
                        fsPromise.rmdir(deletePath)
                    );
                });
            } else
                throw new Error(
                    `console/Helpers/Filesystem.rmrf: ${deletePath} isn't file or dir!`
                );
        })
        .catch(() => {
            throw new Error(
                `console/Helpers/Filesystem.rmrf: ${deletePath} doesn't exist.`
            );
        });
}
