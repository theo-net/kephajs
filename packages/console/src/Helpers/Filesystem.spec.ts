/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import fs from 'node:fs';
import os from 'node:os';
import path from 'node:path';

import { expect } from 'chai';

import * as Filesystem from './Filesystem';

describe('@kephajs/console/Helpers/Filesystem', () => {
    const rootPath = os.tmpdir + path.sep + 'console-helpers-filesystem-tests';

    describe('rmrf', () => {
        beforeEach(() => {
            fs.mkdirSync(rootPath);

            fs.mkdirSync(rootPath + path.sep + 'empty');
            fs.mkdirSync(rootPath + path.sep + 'simple');
            fs.mkdirSync(rootPath + path.sep + 'complex');
            fs.mkdirSync(rootPath + path.sep + 'complex' + path.sep + '/empty');
            fs.mkdirSync(rootPath + path.sep + 'complex' + path.sep + '/sub');
            fs.mkdirSync(
                rootPath +
                    path.sep +
                    'complex' +
                    path.sep +
                    '/sub' +
                    path.sep +
                    '/sub'
            );

            fs.writeFileSync(rootPath + path.sep + 'file', 'foobar');
            fs.writeFileSync(
                rootPath + path.sep + 'simple' + path.sep + '/file1',
                'foobar'
            );
            fs.writeFileSync(
                rootPath + path.sep + 'simple' + path.sep + '/file2',
                'foobar'
            );
            fs.writeFileSync(
                rootPath + path.sep + 'complex' + path.sep + '/file1',
                'foobar'
            );
            fs.writeFileSync(
                rootPath + path.sep + 'complex' + path.sep + '/file2',
                'foobar'
            );
            fs.writeFileSync(
                rootPath +
                    path.sep +
                    'complex' +
                    path.sep +
                    '/sub' +
                    path.sep +
                    '/file',
                'foobar'
            );
            fs.writeFileSync(
                rootPath +
                    path.sep +
                    'complex' +
                    path.sep +
                    '/sub' +
                    path.sep +
                    '/sub' +
                    path.sep +
                    '/file1',
                'foobar'
            );
            fs.writeFileSync(
                rootPath +
                    path.sep +
                    'complex' +
                    path.sep +
                    '/sub' +
                    path.sep +
                    '/sub' +
                    path.sep +
                    '/file2',
                'foobar'
            );
        });

        afterEach(async () => {
            if (fs.existsSync(rootPath)) await Filesystem.rmrf(rootPath);
        });

        it('Should remove a file', async () => {
            await Filesystem.rmrf(rootPath + path.sep + 'file');
            expect(fs.existsSync(rootPath + path.sep + 'file')).to.be.equal(
                false
            );
        });

        it("Should throw an error if file / directory don't exist", async () => {
            try {
                await Filesystem.rmrf(
                    rootPath + path.sep + 'myDir' + path.sep + 'file'
                );
            } catch (error) {
                expect(error).to.be.instanceOf(Error);
                expect((error as Error).message).to.contain(
                    'console/Helpers/Filesystem.rmrf:'
                );
                expect((error as Error).message).to.contain("doesn't exist");
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Sould remove an empty dir', async () => {
            await Filesystem.rmrf(rootPath + path.sep + 'empty');
            expect(fs.existsSync(rootPath + path.sep + 'empty')).to.be.equal(
                false
            );
        });

        it('Sould remove a dir with files', async () => {
            await Filesystem.rmrf(rootPath + path.sep + 'simple');
            expect(fs.existsSync(rootPath + path.sep + 'simple')).to.be.equal(
                false
            );
        });

        it('Sould remove a dir with subdir and not empty subdir', async () => {
            await Filesystem.rmrf(rootPath + path.sep + 'complex');
            expect(fs.existsSync(rootPath + path.sep + 'complex')).to.be.equal(
                false
            );
        });
    });
});
