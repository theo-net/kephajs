/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    strlenMaxLine,
    truncate,
    truncateWidth,
    wordWrap,
} from '@kephajs/core/Helpers/Helpers';

export const NodeVersion = process.version;

export function strlenMaxLineStylized(str: string) {
    let offset = 0;
    let output = '';

    const matches = str.matchAll(/<(\/?)([^<>]*)>/gi);
    for (const match of matches) {
        // 0: captured
        // 1: '/' or ''
        // 2: content of tag

        const pos = match.index ? match.index : offset;

        // if tag escaped
        if (pos > 0 && str[pos - 1] === '\\') {
            continue;
        }

        // add the text up to the next tag
        output += str.substring(offset, pos);

        offset = pos + match[0].length;
    }

    output += str.substring(offset);

    return strlenMaxLine(output.replace(/\\</g, '<').replace(/\\>/g, '>'));
}

export function wordWrapStylized(maxLength: number, inputStr: string) {
    return wordWrap(maxLength, inputStr, strlenMaxLineStylized);
}

export function truncateWidthStylized(
    inputStr: string,
    desiredLength: number,
    strlen = strlenMaxLineStylized
) {
    const currentStyle: string[] = [];
    let currentLength = 0;
    let output = '';

    let offset = 0;
    const matches = inputStr.matchAll(/<(\/?)([^<>]*)>/gi);
    for (const match of matches) {
        // 0: captured
        // 1: '/' or ''
        // 2: content of tag

        const pos = match.index ? match.index : offset;

        // if tag escaped
        if (pos > 0 && inputStr[pos - 1] === '\\') continue;

        currentLength = strlen(output);

        // add the text up to the next tag
        const subStr = inputStr.substring(offset, pos);
        if (currentLength + subStr.length <= desiredLength) output += subStr;
        else output += truncateWidth(subStr, desiredLength - currentLength);

        offset = pos + match[0].length;

        // opening or closing tag
        if (match[1] === '') {
            currentStyle.push(match[2]);
            output += '<' + match[2] + '>';
        } else if (match[1] === '/') {
            output += '</>';
            currentStyle.pop();
        }
    }

    return (
        output +
        truncateWidth(inputStr.substring(offset), desiredLength - currentLength)
    );
}

export function truncateStylized(
    inputStr: string,
    desiredLength: number,
    truncateChar = '…',
    strlen = strlenMaxLineStylized,
    truncateFct = truncateWidthStylized
) {
    return truncate(inputStr, desiredLength, truncateChar, strlen, truncateFct);
}
