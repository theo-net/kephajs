/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as fs from 'fs';
import * as path from 'path';
import { fileURLToPath } from 'url';

import {
    Application as CoreApplication,
    TEnvironment,
} from '@kephajs/core/Application';
import { ApplicationOptions } from '@kephajs/core/ApplicationOptions';
import { Command } from './Command/Command';
import { CommandRouter } from './Command/CommandRouter';
import { Env } from './Env/Env';
import { EnvType } from './Env/EnvType';
import { ExitCodeInterface } from './Command/Error/ExitCodeInterface';
import { Logger } from './Logger/Logger';
import { ColorDepth, Output } from './Output/Output';
import { OutputFormatter } from './Output/OutputFormatter';
import { ParseArgv } from './Argv/ParseArgv';
import { TableManager } from './Table/TableManager';
import { ProfilerOutput } from './ProfilerOutput';

import './contracts/ApplicationOptions';
import './contracts/ApplicationRegisteredServices';
import './contracts/ConfigType';

/**
 * The base for the console application
 */
export class Application extends CoreApplication {
    private _bin = '';

    private _jsonLoaded: string[] = [];

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private _packageJson: any = undefined;

    /**
     * Initialize the application
     * @param appRoot The root directory of the console application
     */
    constructor(
        appRoot: string,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        options: TEnvironment | ApplicationOptions = 'production'
    ) {
        appRoot = appRoot.startsWith('file:')
            ? path.dirname(fileURLToPath(appRoot))
            : appRoot;

        // parse options
        if (typeof options === 'string') {
            options = { environment: options };
        }
        options = { _savetoLaterLogs: true, ...options };

        super(appRoot, options);
    }

    get bin() {
        return this._bin;
    }

    /**
     * Launch the application
     */
    async run() {
        await super.run();

        const commandRouter = this.container.get('$commandRouter');

        try {
            const options = Object.assign({}, this.container.get('$argv'));
            let args = this.container.get('$argv')._;
            delete options._;
            let command: Command;

            // node scripts.js -b 1 help -> help is an argument, but it will be in the first place for args._
            if (args.length > 0 && process.argv[2] === args[0]) {
                command = commandRouter.getCommand(args[0]);
                args = args.slice(1);
            } else {
                command = commandRouter.getDefaultCommand();
            }

            const exit = await command.run({
                output: this.container.get('$output'),
                outputErr: this.container.get('$outputErr'),
                args,
                options,
            });
            await this.exit(exit);
        } catch (error) {
            commandRouter.displayError(error as Error);

            if ((error as ExitCodeInterface).exitCode !== undefined) {
                await this.exit((error as ExitCodeInterface).exitCode);
            } else {
                await this.exit(1);
            }
        }
    }

    /**
     * Load a Json file to the config
     */
    loadJsonConfig(file: string, prefix = '', replace = false) {
        const filename = this.appRoot + path.sep + file + '.json';
        this.config.addJson(
            JSON.parse(fs.readFileSync(filename, 'utf-8')),
            prefix,
            replace
        );
        this._jsonLoaded.push(filename);
    }

    /**
     * Return the list of the Json config files
     */
    getJsonLoaded() {
        return this._jsonLoaded;
    }

    /**
     * Exit the application
     */
    async exit(code = 0) {
        await this.shutdown();

        if (this.environment === 'development') {
            const profilerOutput = new ProfilerOutput(this.profiler);
            this.container
                .get('$output')
                .writeln('<fg=bright-red>Profiler:</>', Output.VERBOSITY.DEBUG);
            this.container
                .get('$output')
                .writeln(profilerOutput.render(), Output.VERBOSITY.DEBUG);
        }

        process.exit(code);
    }

    protected async _stateInitiated() {
        // Load environnment configuration
        const envFiles = [
            this.appRoot + path.sep + '.env',
            this.appRoot + path.sep + '.env.local',
            this.appRoot + path.sep + '.env.' + this.environment,
        ];
        const optionsEnvFiles = this._optionsRaw.envFiles
            ? this._optionsRaw.envFiles
            : [];
        const env = this.container
            .getInjector()
            .injectClass(Env) as Env<EnvType>;
        if (this._optionsRaw.envValidator) {
            env.init(
                [...envFiles, ...optionsEnvFiles],
                this._optionsRaw.envValidator
            );
        } else {
            env.init([...envFiles, ...optionsEnvFiles]);
        }

        // Check if the environment is defined by NODE_ENV
        if (env.has('NODE_ENV')) {
            this._setEnvironment(env.get('NODE_ENV'));
        }

        // Register Env as service
        this.container.register('$env', env);

        // Register argv and bin
        this._bin = path.basename(process.argv[1]);
        let argv = process.argv.slice(2);
        if (argv.length == 0 && this._optionsRaw.argv) {
            argv = this._optionsRaw.argv as string[];
        }
        const argvParsed = new ParseArgv().parse(argv);
        this.container.register('$argv', argvParsed);

        // Output services
        const output = this.container
            .getInjector()
            .injectClass(Output) as Output;
        output.setStream(process.stdout);
        const outputErr = this.container
            .getInjector()
            .injectClass(Output) as Output;
        outputErr.setStream(process.stderr);
        const formatter = new OutputFormatter(
            true,
            {},
            output.getColorDepth() === ColorDepth.C16M
        );
        this.container.register('$outputFormatter', formatter);
        output.setFormatter(formatter);
        outputErr.setFormatter(formatter);
        const verbosity = this._getVerbosity(env, argvParsed);
        output.setVerbosity(verbosity);
        outputErr.setVerbosity(verbosity);
        this.container.register('$output', output);
        this.container.register('$outputErr', outputErr);

        // Logger
        const logger = this.container.getInjector().injectClass(Logger);
        logger.addSavedLogs(this.container.get('$logger').getSavedLogs());
        logger.printSavedLogs();
        this.container.register('$logger', logger);

        // Command Router
        this.container.register(
            '$commandRouter',
            this.container.getInjector().injectClass(CommandRouter)
        );

        // Table Manager
        this.container.register(
            '$tableManager',
            this.container.getInjector().injectClass(TableManager)
        );

        return Promise.resolve();
    }

    protected _loadPackageJson() {
        if (this._packageJson === undefined) {
            const packageJsonPath = this.appRoot + path.sep + 'package.json';

            try {
                const packageRaw = fs.readFileSync(packageJsonPath, 'utf-8');
                this._packageJson = JSON.parse(packageRaw);
                this.container.get('$logger').info(`${packageJsonPath} loaded`);
            } catch (error) {
                this._packageJson = false;

                if (error instanceof SyntaxError) {
                    throw new SyntaxError(
                        `Unable to read ${packageJsonPath}, because syntax error: ${error.message}`
                    );
                }

                this.container
                    .get('$logger')
                    .info(`Unable to load ${packageJsonPath}`);
            }
        }

        return this._packageJson;
    }

    /**
     * Define the app description from `package.json`
     */
    protected _getDescription(): string | undefined {
        let description = undefined;

        const packageParsed = this._loadPackageJson();
        if (
            packageParsed &&
            packageParsed.description &&
            typeof packageParsed.description === 'string'
        ) {
            description = packageParsed.description;
        }

        return description;
    }

    /**
     * Define the app name from `package.json`
     */
    protected _getName(): string | undefined {
        let name = undefined;

        const packageParsed = this._loadPackageJson();
        if (
            packageParsed &&
            packageParsed.name &&
            typeof packageParsed.name === 'string'
        ) {
            name = packageParsed.name;
        }

        return name;
    }

    /**
     * Define the app version from `package.json`
     */
    protected _getVersion(): string {
        let version = '0.0.0';

        const packageParsed = this._loadPackageJson();
        if (
            packageParsed &&
            packageParsed.version &&
            typeof packageParsed.version === 'string'
        ) {
            version = packageParsed.version;
        }

        return version;
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected _getVerbosity(env: Env, argv: Record<string, any>) {
        let verbosity = Output.VERBOSITY.NORMAL;

        if (env.has('SHELL_VERBOSITY')) {
            switch (env.get('SHELL_VERBOSITY')) {
                case -1:
                    verbosity = Output.VERBOSITY.QUIET;
                    break;
                case 1:
                    verbosity = Output.VERBOSITY.VERBOSE;
                    break;
                case 2:
                    verbosity = Output.VERBOSITY.VERY_VERBOSE;
                    break;
                case 3:
                    verbosity = Output.VERBOSITY.DEBUG;
            }
        }

        if (argv.q || argv.quiet) {
            verbosity = Output.VERBOSITY.QUIET;
        } else if (argv.v) {
            if (argv.v === true) {
                verbosity = Output.VERBOSITY.VERBOSE;
            } else if (Array.isArray(argv.v)) {
                let verbose = 0;
                argv.v.forEach(value => {
                    if (value === true) {
                        verbose++;
                    }
                });
                switch (verbose) {
                    case 2:
                        verbosity = Output.VERBOSITY.VERY_VERBOSE;
                        break;
                    case 3:
                        verbosity = Output.VERBOSITY.DEBUG;
                }
            }
        }

        return verbosity;
    }
}
