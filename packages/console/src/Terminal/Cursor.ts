/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Output } from '../Output/Output';

export class Cursor {
    constructor(private _output: Output) {}

    moveUp(lines = 1) {
        this._output.write(`\u001b[${lines}A`);
        return this;
    }

    moveDown(lines = 1) {
        this._output.write(`\u001b[${lines}B`);
        return this;
    }

    moveRight(columns = 1) {
        this._output.write(`\u001b[${columns}C`);
        return this;
    }

    moveLeft(columns = 1) {
        this._output.write(`\u001b[${columns}D`);
        return this;
    }

    moveToColumn(column: number) {
        this._output.write(`\u001b[${column}G`);
        return this;
    }

    moveToPosition(column: number, row: number) {
        this._output.write(`\u001b[${row + 1};${column}H`);
        return this;
    }

    savePosition() {
        this._output.write(`\u001b7`);
        return this;
    }

    restorePosition() {
        this._output.write(`\u001b8`);
        return this;
    }

    hide() {
        this._output.write(`\u001b[?251`);
        return this;
    }

    show() {
        this._output.write(`\u001b[?25h\u001b[?0c`);
        return this;
    }

    clearLine() {
        this._output.write(`\u001b[2K`);
        return this;
    }

    clearLineAfter() {
        this._output.write(`\u001b[K`);
        return this;
    }

    clearOutput() {
        this._output.write(`\u001b[0J`);
        return this;
    }

    clearScreen() {
        this._output.write(`\u001b[2J`);
        return this;
    }
}
