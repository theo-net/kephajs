/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import Sinon from 'sinon';
import { WriteStream } from 'tty';

import { Cursor } from '../Terminal/Cursor';
import { Output } from '../Output/Output';

describe('@kephajs/console/Terminal/Cursor', () => {
    let output: Output;
    let cursor: Cursor;
    let stubWrite: Sinon.SinonStub;
    let stream: WriteStream;

    beforeEach(() => {
        output = new Output();
        stream = new WriteStream(5);
        output.setStream(stream);
        cursor = new Cursor(output);
        stubWrite = Sinon.stub(stream, 'write');
    });

    afterEach(() => {
        stubWrite.restore();
    });

    describe('moveUp', () => {
        it('Should move one line up', () => {
            cursor.moveUp();
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[1A'
            );
        });

        it('Should return this', () => {
            expect(cursor.moveUp()).to.be.equal(cursor);
        });

        it('Should move multiple line up', () => {
            cursor.moveUp(3);
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[3A'
            );
        });
    });

    describe('moveDown', () => {
        it('Should move one line down', () => {
            cursor.moveDown();
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[1B'
            );
        });

        it('Should return this', () => {
            expect(cursor.moveDown()).to.be.equal(cursor);
        });

        it('Should move multiple line down', () => {
            cursor.moveDown(3);
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[3B'
            );
        });
    });

    describe('moveRight', () => {
        it('Should move one column right', () => {
            cursor.moveRight();
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[1C'
            );
        });

        it('Should return this', () => {
            expect(cursor.moveRight()).to.be.equal(cursor);
        });

        it('Should move multiple column right', () => {
            cursor.moveRight(3);
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[3C'
            );
        });
    });

    describe('moveLeft', () => {
        it('Should move one column left', () => {
            cursor.moveLeft();
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[1D'
            );
        });

        it('Should return this', () => {
            expect(cursor.moveLeft()).to.be.equal(cursor);
        });

        it('Should move multiple column left', () => {
            cursor.moveLeft(3);
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[3D'
            );
        });
    });

    describe('moveToColumn', () => {
        it('Should move to column', () => {
            cursor.moveToColumn(5);
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[5G'
            );
        });

        it('Should return this', () => {
            expect(cursor.moveToColumn(5)).to.be.equal(cursor);
        });
    });

    describe('moveToPosition', () => {
        it('Should move to position', () => {
            cursor.moveToPosition(3, 5);
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[6;3H'
            );
        });

        it('Should return this', () => {
            expect(cursor.moveToPosition(3, 5)).to.be.equal(cursor);
        });
    });

    describe('savePosition', () => {
        it('Should save position', () => {
            cursor.savePosition();
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal('\u001b7');
        });

        it('Should return this', () => {
            expect(cursor.savePosition()).to.be.equal(cursor);
        });
    });

    describe('restorePosition', () => {
        it('Should restore position', () => {
            cursor.restorePosition();
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal('\u001b8');
        });

        it('Should return this', () => {
            expect(cursor.restorePosition()).to.be.equal(cursor);
        });
    });

    describe('hide', () => {
        it('Should hide cursor', () => {
            cursor.hide();
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[?251'
            );
        });

        it('Should return this', () => {
            expect(cursor.hide()).to.be.equal(cursor);
        });
    });

    describe('show', () => {
        it('Should show cursor', () => {
            cursor.show();
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[?25h\u001b[?0c'
            );
        });

        it('Should return this', () => {
            expect(cursor.show()).to.be.equal(cursor);
        });
    });

    describe('clearLine', () => {
        it('Should clear line', () => {
            cursor.clearLine();
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[2K'
            );
        });

        it('Should return this', () => {
            expect(cursor.clearLine()).to.be.equal(cursor);
        });
    });

    describe('clearLineAfter', () => {
        it('Should clear line after cursor position', () => {
            cursor.clearLineAfter();
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[K'
            );
        });

        it('Should return this', () => {
            expect(cursor.clearLineAfter()).to.be.equal(cursor);
        });
    });

    describe('clearOutput', () => {
        it('Should clear all output from the cursors current position', () => {
            cursor.clearOutput();
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[0J'
            );
        });

        it('Should return this', () => {
            expect(cursor.clearOutput()).to.be.equal(cursor);
        });
    });

    describe('clearScreen', () => {
        it('Should clear screen', () => {
            cursor.clearScreen();
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                '\u001b[2J'
            );
        });

        it('Should return this', () => {
            expect(cursor.clearScreen()).to.be.equal(cursor);
        });
    });
});
