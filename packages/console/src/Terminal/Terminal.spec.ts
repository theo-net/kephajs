/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Terminal } from './Terminal';

describe('@kephajs/console/Terminal/Terminal', () => {
    let terminal: Terminal;

    beforeEach(() => {
        terminal = new Terminal();
    });

    describe('getWidth', () => {
        it('Should return current width', () => {
            if (process.stdout.columns !== undefined) {
                expect(terminal.getWidth()).to.be.equal(process.stdout.columns);
            }
        });

        it('Should return default value if the number is not accessible', () => {
            const backup = process.stdout.columns;
            process.stdout.columns = -1;
            expect(terminal.getWidth()).to.be.equal(80);
            process.stdout.columns = backup;
        });
    });

    describe('getHeight', () => {
        it('Should return current height', () => {
            if (process.stdout.rows !== undefined) {
                expect(terminal.getHeight()).to.be.equal(process.stdout.rows);
            }
        });

        it('Should return default value if the number is not accessible', () => {
            const backup = process.stdout.rows;
            process.stdout.rows = -1;
            expect(terminal.getHeight()).to.be.equal(50);
            process.stdout.rows = backup;
        });
    });
});
