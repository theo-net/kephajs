/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export class Terminal {
    getWidth() {
        if (process.stdout.columns && process.stdout.columns > 0) {
            return process.stdout.columns;
        }
        return 80;
    }

    getHeight() {
        if (process.stdout.rows && process.stdout.rows > 0) {
            return process.stdout.rows;
        }
        return 50;
    }
}
