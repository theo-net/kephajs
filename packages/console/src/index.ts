/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as ConsoleHelpers from './Helpers/Helpers';
import { Helpers as CoreHelpers } from '@kephajs/core';

export { Env } from './Env/Env';

export const Helpers = {
    ...ConsoleHelpers,
    ...CoreHelpers,
};
