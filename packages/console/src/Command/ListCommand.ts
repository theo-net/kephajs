/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TabulateList } from '../Output/Helpers/TabulateList';
import { Argument } from './Argument';
import { Command, CommandExecuteArgs } from './Command';
import { CommandRouter } from './CommandRouter';

export class ListCommand extends Command {
    public static $inject = ['$commandRouter'];

    public static readonly commandName = 'list';

    public static readonly description = 'List command';

    constructor(private _commandRouter: CommandRouter) {
        super();

        this.addArgument(new Argument('[namespace]', 'The namespace name'));
    }

    getHelp(): string {
        return `The <info>%command.name%</info> command lists all commands:

  <info>%app.bin% list</>

You can also display the commands for a specific namespace:

  <info>%app.bin% list test</>`;
    }

    protected _execute({ output, args }: CommandExecuteArgs) {
        // get command list
        const commands = this._commandRouter.getCommands();
        const commandWithoutNamespace: {
            name: string;
            command: typeof Command;
        }[] = [];
        const commandByNamespace: Record<
            string,
            { name: string; command: typeof Command }[]
        > = {};
        Object.keys(commands)
            .sort()
            .forEach(commandName => {
                const splited = commandName.split(':');
                if (splited.length === 1)
                    commandWithoutNamespace.push({
                        name: commandName,
                        command: commands[commandName].command,
                    });
                else {
                    if (!commandByNamespace[splited[0]])
                        commandByNamespace[splited[0]] = [];
                    commandByNamespace[splited[0]].push({
                        name: commandName,
                        command: commands[commandName].command,
                    });
                }
            });

        // write output
        const tabulateList = this._createTabulateList();
        if (args.namespace !== undefined) {
            output?.writeln(
                '<fg=bright-red>Available commands for the "' +
                    args.namespace +
                    '" namespace:</>'
            );
            this._writeCommandList(
                args.namespace,
                tabulateList,
                commandByNamespace
            );
        } else {
            output?.writeln('<fg=bright-red>Available commands:</>');
            commandWithoutNamespace.sort().forEach(command => {
                tabulateList.push([command.name, command.command.description]);
            });
            Object.keys(commandByNamespace)
                .sort()
                .forEach(namespace => {
                    this._addNamespace(namespace, tabulateList);
                    this._writeCommandList(
                        namespace,
                        tabulateList,
                        commandByNamespace
                    );
                });
        }

        tabulateList.render(output);

        return Command.SUCCESS;
    }

    private _createTabulateList() {
        return this.getHelper().tabulateList({
            prefix: ['  '],
            styles: ['fg=green'],
        });
    }

    private _addNamespace(namespace: string, tabulateList: TabulateList) {
        tabulateList.push([
            {
                content: namespace,
                prefix: ' ',
                style: 'fg=bright-red',
            },
        ]);
    }

    private _writeCommandList(
        namespace: string,
        tabulateList: TabulateList,
        commandByNamespace: Record<
            string,
            { name: string; command: typeof Command }[]
        >
    ) {
        commandByNamespace[namespace].sort().forEach(command => {
            tabulateList.push([command.name, command.command.description]);
        });
    }
}
