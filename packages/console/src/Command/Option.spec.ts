/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Option } from './Option';
import { OptionSyntaxError } from './Error/OptionSyntaxError';
import { ReservedOptionNameError } from './Error/ReservedOptionNameError';
import { Validator } from '@kephajs/core/Validate/Validator';

describe('@kephajs/console/Command/Option', () => {
    describe('constructor', () => {
        let opt: Option;

        beforeEach(() => {
            opt = new Option(
                '-c',
                'a description',
                new Validator().any(),
                'oofbar',
                true
            );
        });

        it('Should set if variadic argument', () => {
            opt = new Option('--test [lol...]', 'a description');
            expect(opt.isVariadic()).to.be.equal(true);
            opt = new Option('--test [lol]', 'a description');
            expect(opt.isVariadic()).to.be.equal(false);
        });

        it('Should set if the option need to have a value or not', () => {
            expect(opt.getValueType()).to.be.equal(undefined);
            opt = new Option('--test [lol]', 'a description');
            expect(opt.getValueType()).to.be.equal(Option.OPTIONAL_VALUE);
            opt = new Option('--test <lol>', 'a description');
            expect(opt.getValueType()).to.be.equal(Option.REQUIRED_VALUE);
        });

        it('Should set option name', () => {
            expect(opt.name).to.be.equal('c');
            opt = new Option('--test...', 'a description');
            expect(opt.name).to.be.equal('test');
            opt = new Option('--salut-toi>', 'a description');
            expect(opt.name).to.be.equal('salutToi');
            opt = new Option('--HelloWorld3 <path>', 'a description');
            expect(opt.name).to.be.equal('helloWorld3');
            opt = new Option('--my-HTTP [file]', 'a description');
            expect(opt.name).to.be.equal('myHttp');
        });

        it('Should have short and long name', () => {
            opt = new Option('-t, --test', 'a description');
            expect(opt.name).to.be.equal('test');
            expect(opt.getLongCleanName()).to.be.equal('test');
            expect(opt.getShortCleanName()).to.be.equal('t');

            opt = new Option('-h, --help <command_name>', 'a description');
            expect(opt.name).to.be.equal('help');
            expect(opt.getLongCleanName()).to.be.equal('help');
            expect(opt.getShortCleanName()).to.be.equal('h');
        });

        it('Should throw ReservedOptionNameError if the name is reserved', () => {
            try {
                new Option('-v', '');
            } catch (error) {
                expect(error).to.be.instanceOf(ReservedOptionNameError);
                expect((error as ReservedOptionNameError).message).to.be.equal(
                    'An option can have the name "v": it\'s a reserved name'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should set if requiered or not', () => {
            expect(opt.isRequired()).to.be.equal(true);
            opt = new Option(
                '-c',
                'a description',
                new Validator().any(),
                0,
                false
            );
            expect(opt.isRequired()).to.be.equal(false);
        });
    });

    describe('_analyseSynopsis', () => {
        it('Should parse long option', () => {
            const opt = new Option('--force');
            const expected = {
                long: '--force',
                longCleanName: 'force',
                variadic: false,
            };

            expect(
                Object.getPrototypeOf(opt)._analyseSynopsis.call(opt)
            ).to.be.deep.equal(expected);
        });

        it('Should parse short option', () => {
            const opt = new Option('-f');
            const expected = {
                short: '-f',
                shortCleanName: 'f',
                variadic: false,
            };

            expect(
                Object.getPrototypeOf(opt)._analyseSynopsis.call(opt)
            ).to.be.deep.equal(expected);
        });

        it('Should parse optional option value', () => {
            const opt = new Option('-f [value]');
            const expected = {
                short: '-f',
                shortCleanName: 'f',
                valueType: Option.OPTIONAL_VALUE,
                variadic: false,
            };

            expect(
                Object.getPrototypeOf(opt)._analyseSynopsis.call(opt)
            ).to.be.deep.equal(expected);
        });

        it('Should parse required option value', () => {
            const opt = new Option('-f <value>');
            const expected = {
                short: '-f',
                shortCleanName: 'f',
                valueType: Option.REQUIRED_VALUE,
                variadic: false,
            };

            expect(
                Object.getPrototypeOf(opt)._analyseSynopsis.call(opt)
            ).to.be.deep.equal(expected);
        });

        it('Should parse variadic option', () => {
            const opt = new Option('-f <value...>');
            const expected = {
                short: '-f',
                shortCleanName: 'f',
                valueType: Option.REQUIRED_VALUE,
                variadic: true,
            };

            expect(
                Object.getPrototypeOf(opt)._analyseSynopsis.call(opt)
            ).to.be.deep.equal(expected);
        });

        it('Should throw OptionSyntaxError if error in the synopsis', () => {
            try {
                new Option('f <value...>', '', new Validator().any(), 0, true);
            } catch (error) {
                expect(error).to.be.instanceOf(OptionSyntaxError);
                expect((error as OptionSyntaxError).message).to.be.equal(
                    'Syntax error in option synopsis: f <value...>'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    describe('isRequired', () => {
        it('Should return true if option is required', () => {
            const opt = new Option(
                '-f <value>',
                'desc',
                new Validator().any(),
                'truc',
                true
            );
            expect(opt.isRequired()).to.be.equal(true);
        });

        it('Should return false if option is not required', () => {
            const opt = new Option(
                '-f <value>',
                'desc',
                new Validator().any(),
                'truc',
                false
            );
            expect(opt.isRequired()).to.be.equal(false);
        });
    });
});
