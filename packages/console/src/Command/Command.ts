/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Argument } from './Argument';
import { InvalidArgumentError } from './Error/InvalidArgumentError';
import { InvalidArgumentValueError } from './Error/InvalidArgumentValueError';
import { InvalidOptionValueError } from './Error/InvalidOptionValueError';
import { Issue } from '@kephajs/core/Validate/Validator';
import { MissingArgumentError } from './Error/MissingArgumentError';
import { MissingOptionError } from './Error/MissingOptionError';
import { Option } from './Option';
import { Output } from '../Output/Output';
import { UnknownOptionError } from './Error/UnknownOptionError';
import { WrongNumberOfArgumentError } from './Error/WrongNumberOfArgumentError';
import { NullOutput } from '../Output/NullOutput';
import { FormatterHelper } from '../Output/Helpers/FormatterHelper';

export type CommandRunArgs = {
    output?: Output;
    outputErr?: Output;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    args: (string | number)[] | Record<string, any>;
    options: Record<string, string | number | boolean>;
};

export type CommandExecuteArgs = {
    output?: Output;
    outputErr?: Output;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    args: Record<string, any>;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    options: Record<string, any>;
};

export class Command {
    static readonly commandName: string;

    static readonly description: string = '';

    static readonly SUCCESS = 0;

    static readonly FAILURE = 1;

    static readonly INVALID = 2;

    private _arguments: Argument[] = [];

    protected _commandName = '';

    protected _description = '';

    protected _formatterHelper: FormatterHelper | null = null;

    private _hasOptionalArguments = false;

    private _hasVariadicArguments = false;

    private _options: Option[] = [];

    private _optionsName: string[] = [];

    setName(name = '') {
        this._commandName = name;
    }

    getName() {
        return this._commandName;
    }

    setDescription(description = '') {
        this._description = description;
    }

    getDescription() {
        return this._description;
    }

    run(
        runParams: CommandRunArgs = {
            args: [],
            options: {},
            output: new NullOutput(),
        }
    ): Promise<number> {
        const { args, options } = this._validateCall(
            runParams.args,
            runParams.options
        );

        return Promise.resolve(this._execute({ ...runParams, args, options }));
    }

    get arguments() {
        return this._arguments;
    }

    get options() {
        return this._options;
    }

    addArgument(argument: Argument) {
        if (this._hasOptionalArguments && argument.isRequired()) {
            throw new InvalidArgumentError(
                'You cannot add a required argument after an optional.'
            );
        } else if (argument.isOptional()) {
            this._hasOptionalArguments = true;
        }
        if (this._hasVariadicArguments && !argument.isVariadic()) {
            throw new InvalidArgumentError(
                'You cannot add a non variadic argument after a variadic.'
            );
        } else if (argument.isVariadic()) {
            this._hasVariadicArguments = true;
        }

        this._arguments.push(argument);

        return this;
    }

    addOption(option: Option) {
        this._options.push(option);

        if (option.getShortCleanName() !== undefined) {
            this._optionsName.push(option.getShortCleanName() as string);
        }
        if (option.getLongCleanName() !== undefined) {
            this._optionsName.push(option.getLongCleanName() as string);
        }

        return this;
    }

    getSynopsis() {
        return (
            this._commandName +
            ' [options]' +
            (this._arguments.length > 0 ? ' ' : '') +
            this._arguments.map(a => a.synopsis).join(' ')
        );
    }

    getHelp() {
        return '';
    }

    getProcessedHelp(commandName = '', appBin = '') {
        return this.getHelp()
            .replace(/%command.name%/g, commandName)
            .replace(/%app.bin%/g, appBin);
    }

    getHelper() {
        if (this._formatterHelper === null)
            this._formatterHelper = new FormatterHelper();
        return this._formatterHelper;
    }

    protected _execute(args: CommandExecuteArgs): number | Promise<number> {
        args.output?.writeln('RUN', Output.VERBOSITY.DEBUG);

        return Command.SUCCESS;
    }

    private _validateCall(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        args: (string | number)[] | Record<string, any>,
        options: Record<string, string | number | boolean>
    ) {
        // validate args
        if (Array.isArray(args)) {
            this._checkArgsRange(args);
            args = this._arrayArgumentsToRecord(args);
        }
        args = this._validateArgs(args);

        // validate options
        options = this._checkRequiredOptions(options);
        options = this._validateOptions(options);

        // clean options
        options = this._addLongNotationToOptions(options);
        options = this._camelCaseOptions(options);

        return { args, options };
    }

    private _checkArgsRange(args: (string | number)[]) {
        // set the min and max number of arguments
        const min = this._arguments.filter(a => a.isRequired()).length;
        const max =
            this._arguments.find(a => a.isVariadic()) !== undefined
                ? Infinity
                : min + this._arguments.filter(a => a.isOptional()).length;

        // Number of arguments
        const count = args.length;

        if (count < min || count > max) {
            throw new WrongNumberOfArgumentError(
                'Wrong number of argument(s)' +
                    (this.getName() ? ' for command ' + this.getName() : '') +
                    '. Got ' +
                    count +
                    ', expected ' +
                    (min === max
                        ? 'exactly ' + min
                        : 'between ' + min + ' and ' + max) +
                    '.'
            );
        }
    }

    private _arrayArgumentsToRecord(args: (string | number)[]) {
        const final = {
            index: 0,
            name: '',
        };
        const argsFinal = this._arguments.reduce(
            (
                acc: Record<string, string | number | (string | number)[]>,
                arg,
                index
            ) => {
                if (typeof args[index] !== 'undefined')
                    acc[arg.name] = args[index];
                final.index = index;
                final.name = arg.name;
                return acc;
            },
            {}
        );
        if (
            final.index < args.length - 1 &&
            this._arguments[final.index].isVariadic()
        ) {
            argsFinal[final.name] = [...args.slice(final.index)];
        }

        return argsFinal;
    }

    private _validateArgs(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        args: Record<string, any>
    ) {
        const errors: Record<string, Issue[]> = {};

        // take default, check if requiered and validate the value
        this._arguments.forEach(arg => {
            if (args[arg.name] === undefined) {
                if (arg.hasDefault())
                    args[arg.name] = arg.defaultValue as
                        | string
                        | number
                        | (string | number)[];
                else if (arg.isRequired())
                    throw new MissingArgumentError(arg.name);
            }

            const parsed = arg.parse(args[arg.name]);
            if (parsed.success === true) {
                args[arg.name] = parsed.data;
            } else {
                if (errors[arg.name]) {
                    errors[arg.name] = [
                        ...errors[arg.name],
                        ...parsed.error.errors,
                    ];
                } else {
                    errors[arg.name] = parsed.error.errors;
                }
            }
        });

        if (Object.keys(errors).length > 0)
            throw new InvalidArgumentValueError(errors);

        return args;
    }

    private _checkRequiredOptions(
        options: Record<string, string | number | boolean>
    ): Record<string, string | number | boolean> {
        return this._options.reduce(
            (acc: Record<string, string | number | boolean>, opt) => {
                if (
                    typeof acc[opt.getLongCleanName() as string] ===
                        'undefined' &&
                    typeof acc[opt.getShortCleanName() as string] ===
                        'undefined'
                ) {
                    if (opt.hasDefault())
                        acc[opt.getLongCleanName() as string] =
                            opt.defaultValue as string | number | boolean;
                    else if (opt.isRequired()) {
                        throw new MissingOptionError(
                            opt.getLongOrShortCleanName()
                        );
                    }
                }

                return acc;
            },
            options
        );
    }

    private _validateOptions(
        options: Record<string, string | number | boolean>
    ) {
        const errors: Record<string, Issue[]> = {};

        Object.keys(options).forEach(optionName => {
            if (Option.RESERVED_NAME.indexOf(optionName) > -1) return;

            const opt = this._findOption(optionName);

            if (!opt)
                throw new UnknownOptionError(optionName, this._optionsName);

            const parsed = opt.parse(options[optionName]);
            if (parsed.success === true) {
                options[optionName] = parsed.data;
            } else {
                if (errors[optionName]) {
                    errors[optionName] = [
                        ...errors[optionName],
                        ...parsed.error.errors,
                    ];
                } else {
                    errors[optionName] = parsed.error.errors;
                }
            }
        }, options);

        if (Object.keys(errors).length > 0)
            throw new InvalidOptionValueError(errors);

        return options;
    }

    private _addLongNotationToOptions(
        options: Record<string, string | number | boolean>
    ): Record<string, string | number | boolean> {
        return Object.keys(options).reduce((acc, key) => {
            // is short name
            if (key.length === 1) {
                const value = acc[key];
                const opt = this._findOption(key);
                if (opt && opt.getLongCleanName())
                    acc[opt.getLongCleanName() as string] = value;
            }
            return acc;
        }, options);
    }

    private _findOption(name: string): Option | undefined {
        return this._options.find(
            o => o.getShortCleanName() === name || o.getLongCleanName() === name
        );
    }

    private _camelCaseOptions(
        options: Record<string, string | number | boolean>
    ): Record<string, string | number | boolean> {
        return this._options.reduce(
            (acc: Record<string, string | number | boolean>, opt) => {
                if (
                    opt.getLongCleanName() !== undefined &&
                    options[opt.getLongCleanName() as string] !== undefined
                )
                    acc[opt.name] = options[opt.getLongCleanName() as string];
                else if (
                    opt.getShortCleanName() !== undefined &&
                    options[opt.getShortCleanName() as string] !== undefined
                )
                    acc[opt.name] = options[opt.getShortCleanName() as string];

                return acc;
            },
            {}
        );
    }
}
