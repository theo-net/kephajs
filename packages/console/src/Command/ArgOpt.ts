/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SafeParseReturnType, Type } from '@kephajs/core/Validate/Type/Type';

export class ArgOpt {
    protected _name = '';

    protected _variadic = false;

    constructor(
        public readonly synopsis: string,
        public readonly description = '',
        public readonly validator?: Type,
        public readonly defaultValue?:
            | number
            | string
            | boolean
            | (number | string)[]
    ) {}

    hasDefault(): boolean {
        return this.defaultValue !== undefined;
    }

    get name() {
        return this._name;
    }

    isVariadic() {
        return this._variadic;
    }

    setVariadic(variadic: boolean) {
        this._variadic = variadic;
    }

    parse(
        value: string | number | boolean | (string | number | boolean)[]
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ): SafeParseReturnType<any, any> {
        if (!this.validator) return { success: true, data: value };

        if (this.isVariadic() && !Array.isArray(value)) value = Array(value);

        if (this.isVariadic()) return this.validator.array().safeParse(value);

        return this.validator.safeParse(value);
    }
}
