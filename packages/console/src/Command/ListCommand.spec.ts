/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import Sinon, { SinonStub } from 'sinon';

import { ServiceContainer } from '@kephajs/core/ServiceContainer/ServiceContainer';
import { NullOutput } from '../Output/NullOutput';
import { CommandRouter } from './CommandRouter';
import { ListCommand } from './ListCommand';
import { Command } from './Command';

describe('@kephajs/console/Command/ListCommand', () => {
    let listCommand: ListCommand;
    let output: NullOutput;
    let stubWrite: SinonStub;
    let commandRouter: CommandRouter;

    function getOutput(stub: SinonStub) {
        const writed: string[] = [];
        stub.getCalls().forEach(call => {
            writed.push(...call.args[0].split('\n'));
        });
        return writed;
    }

    class Command1 extends Command {
        public static readonly commandName = 'azerty';

        public static readonly description = 'A command';
    }

    class Command2 extends Command {
        public static readonly commandName = 'foobar:foo';

        public static readonly description = 'A foobar FOO command';
    }

    class Command3 extends Command {
        public static readonly commandName = 'foobar:bar';

        public static readonly description = 'A foobar BAR command';
    }

    class Command4 extends Command {
        public static readonly commandName = 'foobar:azerty:nothing';

        public static readonly description = 'A nothing command';
    }

    class Command5 extends Command {
        public static readonly commandName = 'cache:log';

        public static readonly description = 'A cache command';
    }

    beforeEach(() => {
        output = new NullOutput();
        stubWrite = Sinon.stub(output, 'write');
        commandRouter = new CommandRouter(new ServiceContainer(), output);
        listCommand = new ListCommand(commandRouter);
    });

    it('Should print the command list with the description', () => {
        listCommand.run({ output, args: [], options: {} });

        expect(getOutput(stubWrite)).to.be.deep.equal([
            '<fg=bright-red>Available commands:</>',
            '<fg=green>  help</>     Display help for a command',
            '<fg=green>  list</>     List command',
            '<fg=green>  version</>  Version of the application',
        ]);
    });

    it('Should short the command list', () => {
        commandRouter.addCommands([Command1]);
        listCommand.run({ output, args: [], options: {} });

        expect(getOutput(stubWrite)).to.be.deep.equal([
            '<fg=bright-red>Available commands:</>',
            '<fg=green>  azerty</>   A command',
            '<fg=green>  help</>     Display help for a command',
            '<fg=green>  list</>     List command',
            '<fg=green>  version</>  Version of the application',
        ]);
    });

    it('Should short the command list with namespace', () => {
        commandRouter.addCommands([
            Command1,
            Command2,
            Command3,
            Command4,
            Command5,
        ]);

        listCommand.run({ output, args: [], options: {} });

        expect(getOutput(stubWrite)).to.be.deep.equal([
            '<fg=bright-red>Available commands:</>',
            '<fg=green>  azerty</>                 A command',
            '<fg=green>  help</>                   Display help for a command',
            '<fg=green>  list</>                   List command',
            '<fg=green>  version</>                Version of the application',
            '<fg=bright-red> cache</>',
            '<fg=green>  cache:log</>              A cache command',
            '<fg=bright-red> foobar</>',
            '<fg=green>  foobar:azerty:nothing</>  A nothing command',
            '<fg=green>  foobar:bar</>             A foobar BAR command',
            '<fg=green>  foobar:foo</>             A foobar FOO command',
        ]);
    });

    it('Should display the command list of a namespace', () => {
        commandRouter.addCommands([
            Command1,
            Command2,
            Command3,
            Command5,
            Command4,
        ]);

        listCommand.run({ output, args: { namespace: 'foobar' }, options: {} });

        expect(getOutput(stubWrite)).to.be.deep.equal([
            '<fg=bright-red>Available commands for the "foobar" namespace:</>',
            '<fg=green>  foobar:azerty:nothing</>  A nothing command',
            '<fg=green>  foobar:bar</>             A foobar BAR command',
            '<fg=green>  foobar:foo</>             A foobar FOO command',
        ]);
    });
});
