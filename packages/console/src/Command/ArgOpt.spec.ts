/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { ArgOpt } from './ArgOpt';
import { ErrorValidator, Validator } from '@kephajs/core/Validate/Validator';

describe('@kephajs/console/Command/ArgOpt', () => {
    describe('constructor', () => {
        let arg: ArgOpt;

        beforeEach(() => {
            arg = new ArgOpt(
                '<test>',
                'une description',
                new Validator().string().regexp(/ab+/),
                'foobar'
            );
        });

        it('Should register synopsis', () => {
            expect(arg.synopsis).to.be.equal('<test>');
        });

        it('Should register the validator', () => {
            const validator = new Validator().string();
            arg = new ArgOpt('<test>', 'une description', validator, 'foobar');
            expect(arg.validator).to.be.equal(validator);
        });

        it('Should set validator to undefined by default', () => {
            arg = new ArgOpt('<test>', 'une description');
            expect(arg.validator).to.be.equal(undefined);
        });

        it('Should register default value', () => {
            expect(arg.defaultValue).to.be.equal('foobar');
        });
    });

    describe('hasDefault', () => {
        it('Should return true if argument have default', () => {
            const arg = new ArgOpt(
                '<test>',
                'une description',
                new Validator().any(),
                'foobar'
            );
            expect(arg.hasDefault()).to.be.equal(true);
        });

        it('Should return false if argument have not default', () => {
            const arg = new ArgOpt('<test>', 'une description');
            expect(arg.hasDefault()).to.be.equal(false);
        });
    });

    describe('parse', () => {
        it('Should return true if the value is valid', () => {
            const arg = new ArgOpt(
                '<test>',
                '',
                new Validator().string().regexp(/a/),
                'a'
            );
            expect(arg.parse('a')).to.be.deep.equal({
                success: true,
                data: 'a',
            });
        });

        it('Should return false if the value is invalid', () => {
            const arg = new ArgOpt(
                '<test>',
                '',
                new Validator().string().regexp(/a/),
                'a'
            );
            const result = arg.parse('b');
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error).to.be.instanceOf(ErrorValidator);
                expect(result.error.errors[0].message).to.be.equal('Invalid');
            }
        });

        it('Should return true if all values are valid for variadic', () => {
            const arg = new ArgOpt(
                '<test...>',
                '',
                new Validator().string().regexp(/a/)
            );
            arg.setVariadic(true);
            expect(arg.parse(['a', 'aa', 'abaa'])).to.be.deep.equal({
                success: true,
                data: ['a', 'aa', 'abaa'],
            });
        });

        it('Should return false if one value is invalid for variadic', () => {
            const arg = new ArgOpt(
                '<test...>',
                '',
                new Validator().string().regexp(/a/)
            );
            arg.setVariadic(true);
            const result = arg.parse(['a', 'bbb', 'abaa']);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error).to.be.instanceOf(ErrorValidator);
                expect(result.error.errors.length).to.be.equal(1);
                expect(result.error.errors[0].message).to.be.equal('Invalid');
            }
        });
    });
});
