/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Command } from './Command';
import { HelpCommand } from './HelpCommand';
import { ListCommand } from './ListCommand';
import { Output } from '../Output/Output';
import { ServiceContainer } from '@kephajs/core/ServiceContainer/ServiceContainer';
import { VersionCommand } from './VersionCommand';
import { UnknownCommand } from './Error/UnknownCommand';
import { UnknownOptionError } from './Error/UnknownOptionError';
import { FormatterHelper } from '../Output/Helpers/FormatterHelper';

export class CommandRouter {
    public static $inject = ['$container', '$outputErr', '$app'];

    private _aliases: Record<string, string> = {};

    private _commands: Record<
        string,
        { command: typeof Command; aliases: string[]; cache?: Command }
    > = {};

    private _defaultCommand = 'help';

    constructor(
        private _container: ServiceContainer,
        private _outputErr: Output
    ) {
        this.addCommands([
            HelpCommand,
            ListCommand,
            VersionCommand,
        ] as unknown as typeof Command[]);
    }

    addCommands(commands: typeof Command[]) {
        commands.forEach(command => {
            this._commands[command.commandName] = {
                command,
                aliases: [],
            };
        });
    }

    addAliases(aliases: Record<string, typeof Command>) {
        Object.getOwnPropertyNames(aliases).forEach(alias => {
            if (this._aliases[alias] !== undefined) {
                this._commands[this._aliases[alias]].aliases = this._commands[
                    this._aliases[alias]
                ].aliases.filter(value => value !== alias);
            }
            this._aliases[alias] = aliases[alias].commandName;
            this._commands[aliases[alias].commandName].aliases.push(alias);
        });
    }

    getCommands() {
        return this._commands;
    }

    getCommandsAliasesList() {
        return [
            ...Object.getOwnPropertyNames(this._commands),
            ...Object.getOwnPropertyNames(this._aliases),
        ].sort();
    }

    setDefaultCommand(command: string | typeof Command) {
        if (typeof command !== 'string') {
            command = command.commandName;
        }
        if (this._commands[command] === undefined) {
            throw new TypeError(`Command "${command}" is not defined.`);
        }
        this._defaultCommand = command;
    }

    getDefaultCommand() {
        return this.getCommand(this._defaultCommand);
    }

    getCommand(name: string): Command {
        if (
            this._commands[name] === undefined &&
            this._aliases[name] !== undefined
        ) {
            return this.getCommand(this._aliases[name]);
        }

        if (this._commands[name] === undefined) {
            throw new UnknownCommand(
                `Command "${name}" is not defined.`,
                name,
                this.getCommandsAliasesList()
            );
        }

        if (this._commands[name].cache === undefined) {
            this._commands[name].cache = this._container
                .getInjector()
                .injectClass(this._commands[name].command);
            this._commands[name].cache?.setName(name);
            this._commands[name].cache?.setDescription(
                this._commands[name].command.description
            );
        }

        return this._commands[name].cache as Command;
    }

    displayError(error: Error) {
        const errorPrinted = [error.message];

        if (
            (error instanceof UnknownCommand ||
                error instanceof UnknownOptionError) &&
            error.getSuggestions().length > 0
        ) {
            errorPrinted.push('', 'Did you mean one of these?');
            error.getSuggestions().forEach(suggestion => {
                errorPrinted.push(`    ${suggestion}`);
            });
        }
        this._outputErr.writeln(
            '\n' +
                new FormatterHelper().formatBlock(errorPrinted, 'error', true) +
                '\n'
        );

        this._outputErr.writeln(error.stack ?? '', Output.VERBOSITY.DEBUG);
    }
}
