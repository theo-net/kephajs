/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Argument } from './Argument';
import { Validator } from '@kephajs/core/Validate/Validator';

describe('@kephajs/console/Command/Argument', () => {
    describe('constructor', () => {
        let arg: Argument;

        beforeEach(() => {
            arg = new Argument(
                '<test>',
                'une description',
                new Validator().any(),
                'foobar'
            );
        });

        it('Should set if is required or optional argument', () => {
            expect(Argument.REQUIRED).to.be.not.equal(Argument.OPTIONAL);
            expect(arg.getType()).to.be.equal(Argument.REQUIRED);
            arg = new Argument('[test]', 'une description');
            expect(arg.getType()).to.be.equal(Argument.OPTIONAL);
        });

        it('Should set if is variadic argument', () => {
            expect(arg.isVariadic()).to.be.equal(false);
            arg = new Argument('[test...]', 'une description');
            expect(arg.isVariadic()).to.be.equal(true);
        });

        it('Should set argument name', () => {
            expect(arg.name).to.be.equal('test');
            arg = new Argument('[test...]', 'une description');
            expect(arg.name).to.be.equal('test');
            arg = new Argument('<salut-toi>', 'une description');
            expect(arg.name).to.be.equal('salutToi');

            arg = new Argument('[HelloWorld3]', 'une description');
            expect(arg.name).to.be.equal('helloWorld3');
            arg = new Argument('[my-HTTP]', 'une description');
            expect(arg.name).to.be.equal('myHttp');
        });

        it('Should keep the original argument name', () => {
            expect(arg.synopsisName).to.be.equal('test');
            arg = new Argument('[test...]', 'une description');
            expect(arg.synopsisName).to.be.equal('test');
            arg = new Argument('<salut-toi>', 'une description');
            expect(arg.synopsisName).to.be.equal('salut-toi');
            arg = new Argument('[HelloWorld3]', 'une description');
            expect(arg.synopsisName).to.be.equal('HelloWorld3');
            arg = new Argument('[my-HTTP]', 'une description');
            expect(arg.synopsisName).to.be.equal('my-HTTP');
        });
    });

    describe('isRequired', () => {
        it('Should return true if argument is required', () => {
            const arg = new Argument('<test>', 'une description');
            expect(arg.isRequired()).to.be.equal(true);
        });

        it('Should return false if argument is not required', () => {
            const arg = new Argument('[test]', 'une description');
            expect(arg.isRequired()).to.be.equal(false);
        });
    });

    describe('isVariadic', () => {
        it('Should return true if arg can have variadic number value', () => {
            const arg = new Argument('[test...]', '');
            expect(arg.isVariadic()).to.be.equal(true);
        });

        it("Should return false if arg can't have variadic number value", () => {
            const arg = new Argument('[test]', '');
            expect(arg.isVariadic()).to.be.equal(false);
        });
    });

    describe('isOptional', () => {
        it('Should return true if arg is optional', () => {
            const arg = new Argument('[test]', '');
            expect(arg.isOptional()).to.be.equal(true);
        });

        it("Should return false if arg isn't optional", () => {
            const arg = new Argument('<test>', '');
            expect(arg.isOptional()).to.be.equal(false);
        });
    });
});
