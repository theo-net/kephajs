/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import Sinon from 'sinon';

import { Command } from './Command';
import { CommandRouter } from './CommandRouter';
import { HelpCommand } from './HelpCommand';
import { ListCommand } from './ListCommand';
import { NullOutput } from '../Output/NullOutput';
import { Output } from '../Output/Output';
import { ServiceContainer } from '@kephajs/core/ServiceContainer/ServiceContainer';
import { UnknownCommand } from './Error/UnknownCommand';
import { UnknownOptionError } from './Error/UnknownOptionError';
import { VersionCommand } from './VersionCommand';
import { Application } from '../Application';
import { OutputFormatter } from '../Output/OutputFormatter';

describe('@kephajs/console/Command/CommandRouter', () => {
    let container: ServiceContainer;
    let commandRouter: CommandRouter;
    let output: Output;
    let outputErr: Output;

    beforeEach(() => {
        container = new ServiceContainer();
        output = new NullOutput();
        outputErr = new NullOutput();
        container.register('$output', output).register('$outputErr', outputErr);
        commandRouter = new CommandRouter(container, outputErr);
        container.register('$commandRouter', commandRouter);
        container.register('$app', new Application('test'));
        container.register('$outputFormatter', new OutputFormatter());
    });

    describe('default commands', () => {
        it('Should register HelpCommand', () => {
            expect(commandRouter.getCommands()?.help.command).to.be.equal(
                HelpCommand
            );
        });

        it('Should register ListCommand', () => {
            expect(commandRouter.getCommands()?.list.command).to.be.equal(
                ListCommand
            );
        });

        it('Should register VersionCommand', () => {
            expect(commandRouter.getCommands()?.version.command).to.be.equal(
                VersionCommand
            );
        });
    });

    describe('addCommands', () => {
        it('Should register the commands', () => {
            class Command1 extends Command {
                public static readonly commandName = 'command1';
            }
            class Command2 extends Command {
                public static readonly commandName = 'command2';
            }
            commandRouter.addCommands([Command1, Command2]);

            expect(commandRouter.getCommands()?.command1.command).to.be.equal(
                Command1
            );
            expect(commandRouter.getCommands()?.command2.command).to.be.equal(
                Command2
            );
        });

        it('Should ecrase a command with the same name', () => {
            class Command1 extends Command {
                public static readonly commandName = 'command1';
            }
            class Command2 extends Command {
                public static readonly commandName = 'command1';
            }
            commandRouter.addCommands([Command1, Command2]);

            expect(commandRouter.getCommands()?.command1.command).to.be.equal(
                Command2
            );
        });
    });

    describe('addAliases', () => {
        it('Should register aliases', () => {
            class Command1 extends Command {
                public static readonly commandName = 'command1';
            }
            class Command2 extends Command {
                public static readonly commandName = 'command2';
            }
            commandRouter.addCommands([Command1, Command2]);
            commandRouter.addAliases({
                test: Command1,
                testBis: Command1,
                other: Command2,
            });

            expect(
                commandRouter.getCommands()?.command1.aliases
            ).to.be.deep.equal(['test', 'testBis']);
            expect(
                commandRouter.getCommands()?.command2.aliases
            ).to.be.deep.equal(['other']);
        });

        it('Should ecrase an alias', () => {
            class Command1 extends Command {
                public static readonly commandName = 'command1';
            }
            class Command2 extends Command {
                public static readonly commandName = 'command2';
            }
            commandRouter.addCommands([Command1, Command2]);
            commandRouter.addAliases({
                test: Command1,
                testBis: Command1,
                other: Command2,
            });
            commandRouter.addAliases({
                test: Command2,
            });

            expect(
                commandRouter.getCommands()?.command1.aliases
            ).to.be.deep.equal(['testBis']);
            expect(
                commandRouter.getCommands()?.command2.aliases
            ).to.be.deep.equal(['other', 'test']);
        });
    });

    describe('setDefaultCommand', () => {
        it('Should be HelpCommand by default', () => {
            expect(commandRouter.getDefaultCommand()).to.be.instanceOf(
                HelpCommand
            );
        });

        it('Should define the default command by the name', () => {
            commandRouter.setDefaultCommand('list');
            expect(commandRouter.getDefaultCommand()).to.be.instanceOf(
                ListCommand
            );
        });

        it('Should define the default command by the class', () => {
            commandRouter.setDefaultCommand(
                ListCommand as unknown as typeof Command
            );
            expect(commandRouter.getDefaultCommand()).to.be.instanceOf(
                ListCommand
            );
        });

        it('Should trhow an error if the command is not registered (string)', () => {
            try {
                commandRouter.setDefaultCommand('test');
            } catch (error) {
                expect(error).to.be.instanceOf(TypeError);
                expect((error as TypeError).message).to.be.equal(
                    'Command "test" is not defined.'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should trhow an error if the command is not registered (class)', () => {
            class TestCommand extends Command {
                public static readonly commandName = 'test';
            }
            try {
                commandRouter.setDefaultCommand(TestCommand);
            } catch (error) {
                expect(error).to.be.instanceOf(TypeError);
                expect((error as TypeError).message).to.be.equal(
                    'Command "test" is not defined.'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    describe('getCommandsAliasesList', () => {
        class TestCommand extends Command {
            public static readonly commandName = 'foo:test';
        }

        it('Should return a list of the commands and aliases', () => {
            commandRouter.addCommands([TestCommand]);
            commandRouter.addAliases({
                other: HelpCommand as unknown as typeof Command,
                'list:full': ListCommand as unknown as typeof Command,
                'list:foobar': ListCommand as unknown as typeof Command,
                'foo:list': ListCommand as unknown as typeof Command,
                'foo:bar:list': ListCommand as unknown as typeof Command,
            });
            expect(commandRouter.getCommandsAliasesList()).to.be.deep.equal([
                'foo:bar:list',
                'foo:list',
                'foo:test',
                'help',
                'list',
                'list:foobar',
                'list:full',
                'other',
                'version',
            ]);
        });
    });

    describe('getCommand', () => {
        it('Should return an instance of the command', () => {
            expect(commandRouter.getCommand('help')).to.be.instanceOf(
                HelpCommand
            );
        });

        it('Should return the same instance', () => {
            expect(commandRouter.getCommand('help')).to.be.equal(
                commandRouter.getCommand('help')
            );
        });

        it('Should return an injected instance', () => {
            class TestCommand extends Command {
                public static readonly commandName = 'test';

                public static $inject = ['$output'];

                constructor(private _output?: Output) {
                    super();
                }

                getOutput() {
                    return this._output;
                }
            }
            commandRouter.addCommands([TestCommand]);
            const command = commandRouter.getCommand('test') as TestCommand;
            expect(command.getOutput()).to.be.equal(output);
        });

        it('Should set the name', () => {
            expect(commandRouter.getCommand('help').getName()).to.be.equal(
                'help'
            );
        });

        it('Should set the description', () => {
            expect(
                commandRouter.getCommand('help').getDescription()
            ).to.be.equal('Display help for a command');
        });

        it("Should return the aliased command if command name don't exist", () => {
            commandRouter.addAliases({
                list: HelpCommand as unknown as typeof Command,
                other: ListCommand as unknown as typeof Command,
            });
            expect(commandRouter.getCommand('list')).to.be.instanceOf(
                ListCommand
            );
            expect(commandRouter.getCommand('other')).to.be.instanceOf(
                ListCommand
            );
        });

        it('Should set the main name for alias', () => {
            commandRouter.addAliases({
                other: HelpCommand as unknown as typeof Command,
            });
            expect(commandRouter.getCommand('help').getName()).to.be.equal(
                'help'
            );
        });

        it("Should throw UnknownCommand error if command or alias don't exist", () => {
            try {
                commandRouter.getCommand('other');
            } catch (error) {
                expect(error).to.be.instanceOf(UnknownCommand);
                expect((error as TypeError).message).to.be.equal(
                    'Command "other" is not defined.'
                );
                expect(
                    Object.getOwnPropertyDescriptor(error, '_unknownCommand')
                        ?.value
                ).to.be.equal('other');
                expect(
                    Object.getOwnPropertyDescriptor(error, '_commandsList')
                        ?.value
                ).to.be.deep.equal(commandRouter.getCommandsAliasesList());
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    describe('displayError', () => {
        let stub: Sinon.SinonStub;

        beforeEach(() => {
            stub = Sinon.stub(outputErr, 'write');
        });

        afterEach(() => {
            stub.restore();
        });

        it('Should print error on outputErr with formattedBlock', () => {
            commandRouter.displayError(new Error('test'));
            expect(stub.getCalls()[0].args[0]).to.be.equal(
                [
                    '\n<error>        </>',
                    '<error>  test  </>',
                    '<error>        </>\n',
                ].join('\n')
            );
        });

        it('Should print stack information if VERBOSITY.DEBUG', () => {
            commandRouter.displayError(new Error('test'));
            expect(stub.getCalls()[1].args[0]).to.be.contain(
                'at Context.<anonymous>'
            );
            expect(stub.getCalls()[1].args[2]).to.be.equal(
                Output.VERBOSITY.DEBUG
            );
        });

        it('Should show suggestions for UnknownCommand', () => {
            commandRouter.displayError(
                new UnknownCommand('test', 'footar', ['foobar', 'fooBar'])
            );
            expect(stub.getCalls()[0].args[0]).to.be.equal(
                [
                    '\n<error>                              </>',
                    '<error>  test                        </>',
                    '<error>                              </>',
                    '<error>  Did you mean one of these?  </>',
                    '<error>      foobar                  </>',
                    '<error>      fooBar                  </>',
                    '<error>                              </>\n',
                ].join('\n')
            );
        });

        it('Should not show suggestions for UnknownCommand if nothing', () => {
            commandRouter.displayError(
                new UnknownCommand('test', 'jesus', ['foobar', 'fooBar'])
            );
            expect(stub.getCalls()[0].args[0]).to.be.equal(
                [
                    '\n<error>        </>',
                    '<error>  test  </>',
                    '<error>        </>\n',
                ].join('\n')
            );
        });

        it('Should show suggestions for UnknownOptionError', () => {
            commandRouter.displayError(
                new UnknownOptionError('footar', ['foobar', 'fooBar'])
            );
            expect(stub.getCalls()[0].args[0]).to.be.equal(
                [
                    '\n<error>                              </>',
                    '<error>  Unknown --footar option.    </>',
                    '<error>                              </>',
                    '<error>  Did you mean one of these?  </>',
                    '<error>      foobar                  </>',
                    '<error>      fooBar                  </>',
                    '<error>                              </>\n',
                ].join('\n')
            );
        });

        it('Should not show suggestions for UnknownOptionError if nothing', () => {
            commandRouter.displayError(
                new UnknownOptionError('jesus', ['foobar', 'fooBar'])
            );
            expect(stub.getCalls()[0].args[0]).to.be.equal(
                '\n<error>                           </>\n' +
                    '<error>  Unknown --jesus option.  </>\n' +
                    '<error>                           </>\n'
            );
        });
    });
});
