/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Application } from '../Application';
import { Output } from '../Output/Output';
import { Command, CommandExecuteArgs } from './Command';

export class VersionCommand extends Command {
    public static readonly commandName = 'version';

    public static readonly description = 'Version of the application';

    public static $inject = ['$app'];

    constructor(private _app: Application) {
        super();
    }

    getHelp() {
        return `The <info>%command.name%</info> command return the application version.

If the verbosity is <info>DEBUG</>, the KephaJs version will also returned.`;
    }

    protected _execute({ output }: CommandExecuteArgs) {
        output?.writeln(
            (this._app.name ? this._app.name + ' ' : '') +
                this._app.version.toString()
        );
        output?.writeln(
            '(KephaJs ' + this._app.kephajsVersion.toString() + ')',
            Output.VERBOSITY.DEBUG
        );

        return Command.SUCCESS;
    }
}
