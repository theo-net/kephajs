/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import Sinon, { SinonStub } from 'sinon';

import { Application } from '../Application';
import { NullOutput } from '../Output/NullOutput';
import { VersionCommand } from './VersionCommand';

describe('@kephajs/console/Command/VersionCommand', () => {
    let output: NullOutput;
    let stubWrite: SinonStub;

    beforeEach(() => {
        output = new NullOutput();
        stubWrite = Sinon.stub(output, 'write');
    });

    it('Should print the app version', () => {
        const app = new Application('test', { version: '1.2.3' });
        const versionCommand = new VersionCommand(app);
        versionCommand.run({ output, args: [], options: {} });

        expect(stubWrite.getCalls()[0].args[0]).to.be.equal('1.2.3');
    });

    it('Should print the app name if defined', () => {
        const app = new Application('test', {
            name: 'foobar',
            version: '1.2.3',
        });
        const versionCommand = new VersionCommand(app);
        versionCommand.run({ output, args: [], options: {} });

        expect(stubWrite.getCalls()[0].args[0]).to.be.equal('foobar 1.2.3');
    });

    it('Should print the KephaJs version if VERBOSIY.DEBUG', () => {
        const app = new Application('test', { version: '1.2.3' });
        const versionCommand = new VersionCommand(app);
        versionCommand.run({ output, args: [], options: {} });

        expect(stubWrite.getCalls()[1].args[0]).to.be.equal(
            `(KephaJs ${app.kephajsVersion})`
        );
        expect(stubWrite.getCalls()[1].args[2]).to.be.equal(
            NullOutput.VERBOSITY.DEBUG
        );
    });
});
