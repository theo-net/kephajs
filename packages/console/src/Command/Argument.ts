/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ArgOpt } from './ArgOpt';
import { camelCase } from '@kephajs/core/Helpers/Helpers';
import { Type } from '@kephajs/core/Validate/Type/Type';

export class Argument extends ArgOpt {
    static readonly REQUIRED = 1;

    static readonly OPTIONAL = 2;

    protected _synopsisName: string;

    protected _type: number;

    constructor(
        synopsis: string,
        description = '',
        validator?: Type,
        defaultValue?: number | string | (number | string)[]
    ) {
        super(synopsis, description, validator, defaultValue);

        this._type =
            synopsis.substring(0, 1) === '['
                ? Argument.OPTIONAL
                : Argument.REQUIRED;
        this._variadic =
            synopsis.substring(synopsis.length - 4, synopsis.length - 1) ===
            '...';

        this._synopsisName = synopsis
            .replace(/([[\]<>]+)/g, '')
            .replace('...', '');
        this._name = camelCase(this._synopsisName);
    }

    get synopsisName() {
        return this._synopsisName;
    }

    isRequired(): boolean {
        return this._type === Argument.REQUIRED;
    }

    isOptional(): boolean {
        return this._type === Argument.OPTIONAL;
    }

    getType(): number {
        return this._type;
    }
}
