/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ArgOpt } from './ArgOpt';
import { camelCase } from '@kephajs/core/Helpers/Helpers';
import { OptionSyntaxError } from './Error/OptionSyntaxError';
import { ReservedOptionNameError } from './Error/ReservedOptionNameError';
import { Type } from '@kephajs/core/Validate/Type/Type';

type SynopsisType = {
    long?: string;
    longCleanName?: string;
    short?: string;
    shortCleanName?: string;
    valueType?: number;
    variadic: boolean;
};

export class Option extends ArgOpt {
    static readonly OPTIONAL_VALUE = 1;

    static readonly REQUIRED_VALUE = 2;

    static readonly RESERVED_NAME = ['q', 'quiet', 'v'];

    protected _long: string | undefined;

    protected _longCleanName: string | undefined;

    protected _short: string | undefined;

    protected _shortCleanName: string | undefined;

    protected _valueType: number | undefined;

    constructor(
        synopsis: string,
        description = '',
        validator?: Type,
        defaultValue?: number | string | boolean | (number | string)[],
        protected _required = false
    ) {
        super(synopsis, description, validator, defaultValue);

        const analysis = this._analyseSynopsis();
        this._valueType = analysis.valueType;
        this._variadic = analysis.variadic;
        this._longCleanName = analysis.longCleanName;
        this._shortCleanName = analysis.shortCleanName;
        this._short = analysis.short;
        this._long = analysis.long;

        this._name = this._longCleanName || this._shortCleanName || '';
        this._name = camelCase(
            this._name.replace(/([[\]<>]+)/g, '').replace('...', '')
        );

        if (Option.RESERVED_NAME.indexOf(this._name) > -1)
            throw new ReservedOptionNameError(this._name);
    }

    getValueType(): number | undefined {
        return this._valueType;
    }

    isRequired(): boolean {
        return this._required;
    }

    getLongCleanName() {
        return this._longCleanName;
    }

    getShortCleanName() {
        return this._shortCleanName;
    }

    getLongOrShortCleanName() {
        return this.getLongCleanName() || this.getShortCleanName() || '';
    }

    protected _analyseSynopsis(): SynopsisType {
        const info = this.synopsis.split(/[\s\t,]+/).reduce(
            (acc: SynopsisType, value) => {
                if (value.substring(0, 2) === '--') {
                    acc.long = value;
                    acc.longCleanName = value.substring(2);
                } else if (value.substring(0, 1) === '-') {
                    acc.short = value;
                    acc.shortCleanName = value.substring(1);
                } else if (value.substring(0, 1) === '[') {
                    acc.valueType = Option.OPTIONAL_VALUE;
                    acc.variadic =
                        value.substring(value.length - 4, value.length - 1) ===
                        '...';
                } else if (value.substring(0, 1) === '<') {
                    acc.valueType = Option.REQUIRED_VALUE;
                    acc.variadic =
                        value.substring(value.length - 4, value.length - 1) ===
                        '...';
                } else throw new OptionSyntaxError(this.synopsis);

                return acc;
            },
            { variadic: false }
        );

        return info;
    }
}
