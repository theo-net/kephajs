/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Application } from '../Application';
import { Output } from '../Output/Output';
import { OutputFormatter } from '../Output/OutputFormatter';
import { Argument } from './Argument';
import { Command, CommandExecuteArgs } from './Command';
import { CommandRouter } from './CommandRouter';

export class HelpCommand extends Command {
    public static $inject = ['$commandRouter', '$app'];

    public static readonly commandName = 'help';

    public static readonly description = 'Display help for a command';

    constructor(
        private _commandRouter: CommandRouter,
        private _app: Application
    ) {
        super();

        this.addArgument(new Argument('[command_name]', 'The command name'));
    }

    getHelp() {
        return `The <info>%command.name%</info> command displays help for a given command:

  <info>%app.bin% help list</>

To display the list of available commands, please use the <info>list</> command.`;
    }

    protected _execute({ output, args }: CommandExecuteArgs) {
        if (!args.commandName) this._displayBasicUsage(output);
        else this._helpCommand(args.commandName, output);

        return Command.SUCCESS;
    }

    protected _displayBasicUsage(output?: Output) {
        this._writeTitle('Usage:', output);
        output?.writeln('  command [options] [arguments]\n');

        this._writeTitle('Options:', output);
        const optionsList = this.getHelper().tabulateList({
            prefix: ['  '],
            styles: ['fg=green'],
        });
        optionsList.push(...this._getDefaultOptions());
        optionsList.render(output);
        output?.writeln('');
        this._commandRouter
            .getCommand('list')
            .run({ output, args: [], options: {} });
    }

    protected _getDefaultOptions() {
        return [
            ['-q, --quiet', 'Do not output any message'],
            [
                '-v|vv|vvv',
                'Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug',
            ],
        ];
    }

    protected _writeTitle(title: string, output?: Output) {
        output?.writeln('<fg=bright-red>' + title + '</>');
    }

    protected _writeWithMargin(content: string, output?: Output) {
        content = '  ' + content.split('\n').join('\n  ');
        output?.writeln(content);
    }

    protected _helpCommand(commandName: string, output?: Output) {
        // TODO work with alias and show aliases
        const command = this._commandRouter.getCommand(commandName);

        if (command.getDescription() !== '') {
            this._writeTitle('Description:', output);
            this._writeWithMargin(command.getDescription() + '\n', output);
        }

        this._writeTitle('Usage:', output);
        output?.writeln(
            '  ' + OutputFormatter.escape(command.getSynopsis()) + '\n'
        );

        const argsOptionsList = this.getHelper().tabulateList({
            prefix: ['  '],
            styles: ['fg=green'],
        });
        if (command.arguments.length > 0) {
            argsOptionsList.push([
                { content: 'Arguments:', prefix: '', style: 'fg=bright-red' },
            ]);
            command.arguments.forEach(arg => {
                argsOptionsList.push([arg.synopsisName, arg.description]);
            });
            argsOptionsList.push([{ content: '', prefix: '', style: '' }]);
        }
        argsOptionsList.push([
            { content: 'Options:', prefix: '', style: 'fg=bright-red' },
        ]);
        command.options.forEach(option => {
            argsOptionsList.push([
                OutputFormatter.escape(option.synopsis),
                option.description,
            ]);
        });
        argsOptionsList.push(...this._getDefaultOptions());
        argsOptionsList.render(output);

        const help = command.getProcessedHelp(command.getName(), this._app.bin);
        if (help !== '') {
            output?.writeln('');
            this._writeTitle('Help:', output);
            this._writeWithMargin(help, output);
        }
    }
}
