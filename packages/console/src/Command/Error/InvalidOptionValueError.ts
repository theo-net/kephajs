/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Command } from '../Command';
import { ExitCodeInterface } from './ExitCodeInterface';
import { Issue } from '@kephajs/core/Validate/Validator';

export class InvalidOptionValueError
    extends Error
    implements ExitCodeInterface
{
    public readonly exitCode = Command.INVALID;

    constructor(public readonly errors: Record<string, Issue[]>) {
        let msg = '';
        Object.keys(errors).forEach(option => {
            msg += `Error: Invalid value for option ${
                option.length === 1 ? '-' : '--'
            }${option}\n`;
            errors[option].forEach(issue => {
                msg += '    ' + issue.message + '\n';
            });
        });

        super(msg);
    }
}
