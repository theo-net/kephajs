/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { UnknownOptionError } from './UnknownOptionError';

describe('@kephajs/console/Command/Error/UnknownOptionError', () => {
    describe('getSuggestions', () => {
        it('Should return suggestions', () => {
            const error = new UnknownOptionError('ver', ['version', 'aqstj']);
            expect(error.getSuggestions()).to.be.deep.equal(['version']);
        });

        it('Should return nothing if short option', () => {
            const error = new UnknownOptionError('v', ['version', 'aqstj']);
            expect(error.getSuggestions()).to.be.deep.equal([]);
        });

        it('Should return all of matched options', () => {
            const error = new UnknownOptionError('ver', [
                'version',
                'versi',
                'versio',
                'versions',
                'aqstj',
            ]);
            expect(error.getSuggestions()).to.be.deep.equal([
                'versi',
                'versio',
                'version',
                'versions',
            ]);
        });

        it('Should return the best suggestions if not match', () => {
            const error = new UnknownOptionError('verion', [
                'version',
                'versi',
                'donfuvh',
                'aqstjbj',
            ]);
            expect(error.getSuggestions()).to.be.deep.equal([
                'version',
                'versi',
            ]);
        });

        it('Should return nothing if too far', () => {
            const error = new UnknownOptionError('jesus', [
                'version',
                'veder',
                'aqsrd',
            ]);
            expect(error.getSuggestions()).to.be.deep.equal([]);
        });
    });
});
