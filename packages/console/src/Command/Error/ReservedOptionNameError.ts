/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Command } from '../Command';
import { ExitCodeInterface } from './ExitCodeInterface';

export class ReservedOptionNameError
    extends Error
    implements ExitCodeInterface
{
    public readonly exitCode = Command.INVALID;

    constructor(name: string) {
        super(`An option can have the name "${name}": it's a reserved name`);
    }
}
