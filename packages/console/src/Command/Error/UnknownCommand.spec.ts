/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { UnknownCommand } from './UnknownCommand';

describe('@kephajs/console/Command/Error/UnknownCommand', () => {
    describe('getSuggestions', () => {
        it('Should return suggestions', () => {
            const error = new UnknownCommand('message', 'ver', [
                'version',
                'aqstj',
            ]);
            expect(error.getSuggestions()).to.be.deep.equal(['version']);
        });

        it('Should return all of matched command', () => {
            const error = new UnknownCommand('message', 'ver', [
                'version',
                'versi',
                'versio',
                'versions',
                'aqstj',
            ]);
            expect(error.getSuggestions()).to.be.deep.equal([
                'versi',
                'versio',
                'version',
                'versions',
            ]);
        });

        it('Should return the best suggestions if not match', () => {
            const error = new UnknownCommand('message', 'verion', [
                'version',
                'versi',
                'donfuvh',
                'aqstjbj',
            ]);
            expect(error.getSuggestions()).to.be.deep.equal([
                'version',
                'versi',
            ]);
        });

        it('Should parse group command: match subname', () => {
            const error = new UnknownCommand('message', 'ver', [
                'version',
                'group:ver',
                'aqs',
            ]);
            expect(error.getSuggestions()).to.be.deep.equal([
                'group:ver',
                'version',
            ]);
        });
        it('Should parse group command: match group name', () => {
            const error = new UnknownCommand('message', 'group', [
                'version',
                'group:ver',
                'group:test',
                'aqs',
            ]);
            expect(error.getSuggestions()).to.be.deep.equal([
                'group:ver',
                'group:test',
            ]);
        });
        it('Should parse group command: match part group name', () => {
            const error = new UnknownCommand('message', 'grou', [
                'version',
                'group:ver',
            ]);
            expect(error.getSuggestions()).to.be.deep.equal(['group:ver']);
        });

        it('Should return nothing if too far', () => {
            const error = new UnknownCommand('message', 'jesus', [
                'version',
                'group:veder',
                'aqsrd',
            ]);
            expect(error.getSuggestions()).to.be.deep.equal([]);
        });
    });
});
