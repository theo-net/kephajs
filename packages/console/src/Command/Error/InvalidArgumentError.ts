/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ExitCodeInterface } from './ExitCodeInterface';
import { Command } from '../Command';

export class InvalidArgumentError extends Error implements ExitCodeInterface {
    public readonly exitCode = Command.INVALID;
}
