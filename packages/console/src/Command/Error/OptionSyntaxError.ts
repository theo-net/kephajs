/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Command } from '../Command';
import { ExitCodeInterface } from './ExitCodeInterface';

export class OptionSyntaxError extends Error implements ExitCodeInterface {
    public readonly exitCode = Command.INVALID;

    constructor(synopsis: string) {
        const msg = 'Syntax error in option synopsis: ' + synopsis;

        super(msg);
    }
}
