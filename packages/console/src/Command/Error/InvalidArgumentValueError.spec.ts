/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { InvalidArgumentValueError } from './InvalidArgumentValueError';

describe('@kephajs/console/Command/Error/InvalidArgumentValueError', () => {
    describe('message', () => {
        it('Should return all validation error message', () => {
            const error = new InvalidArgumentValueError({
                a: [
                    { code: 'custom', path: [], message: 'error A' },
                    { code: 'custom', path: [], message: 'error B' },
                ],
                b: [{ code: 'custom', path: [], message: 'error C' }],
            });
            expect(error.message).to.be.equal(
                'Error: Invalid value for argument "a"\n' +
                    '    error A\n' +
                    '    error B\n' +
                    'Error: Invalid value for argument "b"\n' +
                    '    error C\n'
            );
        });
    });
});
