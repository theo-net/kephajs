/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Command } from '../Command';
import { ExitCodeInterface } from './ExitCodeInterface';
import * as Helpers from '@kephajs/core/Helpers/Helpers';

export class UnknownCommand extends Error implements ExitCodeInterface {
    public readonly exitCode = Command.INVALID;

    constructor(
        message: string,
        private _unknownCommand: string,
        private _commandsList: string[]
    ) {
        super(message);
    }

    getSuggestions() {
        const result: string[] = [];
        const levenshteinCommandList: Record<string, number> = {};

        // get Levenshtein value
        this._unknownCommand.split(':').forEach(unknownCommandPart => {
            this._commandsList.forEach(commandFromList => {
                commandFromList.split(':').forEach(commandFromListPart => {
                    let levenshteinPart = Helpers.levenshtein(
                        unknownCommandPart,
                        commandFromListPart
                    );
                    if (
                        commandFromListPart.indexOf(unknownCommandPart) === -1
                    ) {
                        levenshteinPart += 50;
                    }

                    if (
                        levenshteinCommandList[commandFromList] === undefined ||
                        levenshteinCommandList[commandFromList] >
                            levenshteinPart
                    ) {
                        levenshteinCommandList[commandFromList] =
                            levenshteinPart;
                    }
                });
            });
        });

        // sort levenshtein
        const levenshteinShorted: Record<number, string[]> = {};
        Object.getOwnPropertyNames(levenshteinCommandList).forEach(
            commandFromList => {
                if (
                    levenshteinShorted[
                        levenshteinCommandList[commandFromList]
                    ] !== undefined
                ) {
                    levenshteinShorted[
                        levenshteinCommandList[commandFromList]
                    ].push(commandFromList);
                } else {
                    levenshteinShorted[
                        levenshteinCommandList[commandFromList]
                    ] = [commandFromList];
                }
            }
        );

        // Get just the better results
        let length = 0;
        let finish = false;
        Object.getOwnPropertyNames(levenshteinShorted)
            .sort()
            .forEach(levenshtein => {
                // one part match
                if (Number(levenshtein) < 50) {
                    result.push(...levenshteinShorted[Number(levenshtein)]);
                    finish = true;
                } else if (!finish && Number(levenshtein) < 54) {
                    if (levenshteinShorted[Number(levenshtein)].length === 1) {
                        length++;
                        result.push(...levenshteinShorted[Number(levenshtein)]);
                        if (length > 3) {
                            finish = true;
                        }
                    } else {
                        result.push(...levenshteinShorted[Number(levenshtein)]);
                        finish = true;
                    }
                }
            });

        return result;
    }
}
