/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Command } from '../Command';
import { ExitCodeInterface } from './ExitCodeInterface';
import * as Helpers from '@kephajs/core/Helpers/Helpers';

export class UnknownOptionError extends Error implements ExitCodeInterface {
    public readonly exitCode = Command.INVALID;

    constructor(
        private _unknownOption: string,
        private _optionsList: string[]
    ) {
        super(
            `Unknown ${
                _unknownOption.length === 1 ? '-' : '--'
            }${_unknownOption} option.`
        );
    }

    getSuggestions() {
        const result: string[] = [];
        const levenshteinOptionList: Record<string, number> = {};

        // get Levenshtein value
        if (this._unknownOption.length > 1) {
            this._optionsList.forEach(option => {
                let levenshteinPart = Helpers.levenshtein(
                    this._unknownOption,
                    option
                );
                if (option.indexOf(this._unknownOption) === -1) {
                    levenshteinPart += 50;
                }

                if (
                    levenshteinOptionList[option] === undefined ||
                    levenshteinOptionList[option] > levenshteinPart
                ) {
                    levenshteinOptionList[option] = levenshteinPart;
                }
            });
        }

        // sort levenshtein
        const levenshteinShorted: Record<number, string[]> = {};
        Object.getOwnPropertyNames(levenshteinOptionList).forEach(
            optionFromList => {
                if (
                    levenshteinShorted[
                        levenshteinOptionList[optionFromList]
                    ] !== undefined
                ) {
                    levenshteinShorted[
                        levenshteinOptionList[optionFromList]
                    ].push(optionFromList);
                } else {
                    levenshteinShorted[levenshteinOptionList[optionFromList]] =
                        [optionFromList];
                }
            }
        );

        // Get just the better results
        let length = 0;
        let finish = false;
        Object.getOwnPropertyNames(levenshteinShorted)
            .sort()
            .forEach(levenshtein => {
                // one part match
                if (Number(levenshtein) < 50) {
                    result.push(...levenshteinShorted[Number(levenshtein)]);
                    finish = true;
                } else if (!finish && Number(levenshtein) < 54) {
                    if (levenshteinShorted[Number(levenshtein)].length === 1) {
                        length++;
                        result.push(...levenshteinShorted[Number(levenshtein)]);
                        if (length > 3) {
                            finish = true;
                        }
                    } else {
                        result.push(...levenshteinShorted[Number(levenshtein)]);
                        finish = true;
                    }
                }
            });

        return result;
    }
}
