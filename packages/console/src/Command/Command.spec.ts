/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Argument } from './Argument';
import { Command, CommandRunArgs } from './Command';
import { InvalidArgumentError } from './Error/InvalidArgumentError';
import { InvalidArgumentValueError } from './Error/InvalidArgumentValueError';
import { InvalidOptionValueError } from './Error/InvalidOptionValueError';
import { MissingArgumentError } from './Error/MissingArgumentError';
import { MissingOptionError } from './Error/MissingOptionError';
import { Option } from './Option';
import { Output } from '../Output/Output';
import { UnknownOptionError } from './Error/UnknownOptionError';
import { Validator } from '@kephajs/core/Validate/Validator';
import { WrongNumberOfArgumentError } from './Error/WrongNumberOfArgumentError';
import { NullOutput } from '../Output/NullOutput';
import { FormatterHelper } from '../Output/Helpers/FormatterHelper';

describe('@kephajs/console/Command/Command', () => {
    describe('setName and getName', () => {
        it('Should set empty by default', () => {
            const command = new Command();
            expect(command.getName()).to.be.equal('');
            command.setName();
            expect(command.getName()).to.be.equal('');
        });

        it('Should set the name', () => {
            const command = new Command();
            command.setName('foobar');
            expect(command.getName()).to.be.equal('foobar');
        });
    });

    describe('setDescription and getDescription', () => {
        it('Should set empty by default', () => {
            const command = new Command();
            expect(command.getDescription()).to.be.equal('');
            command.setDescription();
            expect(command.getDescription()).to.be.equal('');
        });

        it('Should set the description', () => {
            const command = new Command();
            command.setDescription('An usefull command');
            expect(command.getDescription()).to.be.equal('An usefull command');
        });
    });

    describe('run', () => {
        it('Should run execute method', () => {
            let runed = false;

            class MyCommand extends Command {
                protected _execute() {
                    runed = true;

                    return Command.SUCCESS;
                }
            }
            const myCommand = new MyCommand();
            myCommand.run();
            expect(runed).to.be.equal(true);
        });

        it('Should give the output and outputErr object to the execute method', () => {
            const output = new Output(),
                outputErr = new Output();
            let outputGived: Output | undefined = undefined,
                outputErrGived: Output | undefined = undefined;

            class MyCommand extends Command {
                // eslint-disable-next-line @typescript-eslint/no-shadow
                protected _execute({ output, outputErr }: CommandRunArgs) {
                    outputGived = output;
                    outputErrGived = outputErr;

                    return Command.SUCCESS;
                }
            }
            const myCommand = new MyCommand();
            myCommand.run({ output, outputErr, args: [], options: {} });
            expect(outputGived).to.be.equal(output);
            expect(outputErrGived).to.be.equal(outputErr);
        });

        it('Should give the validated arguments and options to the execute method', () => {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            let argsGived: Record<string, any> = {},
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                optionsGived: Record<string, any> = {};

            const validator = new Validator();

            class MyCommand extends Command {
                constructor() {
                    super();
                    this.addArgument(
                        new Argument(
                            '<foo>',
                            '',
                            validator.string().transform(msg => msg + '!')
                        )
                    ).addOption(
                        new Option(
                            '-b <bar>',
                            '',
                            validator.string().transform(msg => msg + '?')
                        )
                    );
                }

                protected _execute({ args, options }: CommandRunArgs) {
                    argsGived = args;
                    optionsGived = options;

                    return Command.SUCCESS;
                }
            }
            const myCommand = new MyCommand();
            myCommand.run({
                args: ['FOO'],
                options: { b: 'BAR' },
                output: new NullOutput(),
            });
            expect(argsGived).to.be.deep.equal({ foo: 'FOO!' });
            expect(optionsGived).to.be.deep.equal({ b: 'BAR?' });
        });

        it('Should return the exit code', async () => {
            class MyCommand extends Command {
                protected _execute() {
                    return Command.INVALID;
                }
            }
            const exit = await new MyCommand().run();
            expect(exit).to.be.equal(Command.INVALID);
        });
    });

    describe('addArgument', () => {
        let command: Command;

        beforeEach(() => {
            command = new Command();
        });

        it('Should add an argument', () => {
            const arg1 = new Argument('<arg1>', 'description'),
                arg2 = new Argument('<arg2>', 'description');
            command.addArgument(arg1);
            command.addArgument(arg2);

            expect(command.arguments).to.be.deep.equal([arg1, arg2]);
        });

        it('Return the command', () => {
            expect(
                command.addArgument(new Argument('<arg1>', 'description'))
            ).to.be.equal(command);
        });

        it('Should throw InvalidArgumentError if adding a required argument after an optional', () => {
            try {
                const arg1 = new Argument('[arg1]', 'description'),
                    arg2 = new Argument('<arg2>', 'description');
                command.addArgument(arg1).addArgument(arg2);
            } catch (error) {
                expect(error).to.be.instanceOf(InvalidArgumentError);
                expect((error as InvalidArgumentError).message).to.be.equal(
                    'You cannot add a required argument after an optional.'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should throw InvalidArgumentError if adding a non variadic argument after a variadic', () => {
            try {
                const arg1 = new Argument('<arg1...>', 'description'),
                    arg2 = new Argument('<arg2>', 'description');
                command.addArgument(arg1).addArgument(arg2);
            } catch (error) {
                expect(error).to.be.instanceOf(InvalidArgumentError);
                expect((error as InvalidArgumentError).message).to.be.equal(
                    'You cannot add a non variadic argument after a variadic.'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    describe('addOption', () => {
        let command: Command;

        beforeEach(() => {
            command = new Command();
        });

        it('Should add an option', () => {
            const opt1 = new Option('--opt1', 'description'),
                opt2 = new Option('--opt2', 'description');
            command.addOption(opt1);
            command.addOption(opt2);

            expect(command.options).to.be.deep.equal([opt1, opt2]);
        });

        it('Return the command', () => {
            expect(
                command.addOption(new Option('--opt1', 'description'))
            ).to.be.equal(command);
        });
    });

    describe('getSynopsis', () => {
        class MyCommand extends Command {
            public static readonly commandName = 'myCommand';

            protected readonly _commandName = 'myCommand';
        }
        let command: MyCommand;

        beforeEach(() => {
            command = new MyCommand();
        });

        it('Should return the synopsis (without arguments)', () => {
            expect(command.getSynopsis()).to.be.equal('myCommand [options]');
        });

        it('Should return the synopsis (with arguments)', () => {
            command
                .addArgument(new Argument('<foo>', 'Foo'))
                .addArgument(new Argument('[bar]', 'Bar'));
            expect(command.getSynopsis()).to.be.equal(
                'myCommand [options] <foo> [bar]'
            );
        });
    });

    describe('getHelp', () => {
        it('Should return "" by default', () => {
            expect(new Command().getHelp()).to.be.equal('');
        });
    });

    describe('getProcessedHelp', () => {
        it('Should replace %command.name%', () => {
            class MyCommand extends Command {
                static readonly commandName = 'hello';

                getHelp() {
                    return "Hello %command.name%! It's Command %command.name%.";
                }
            }
            expect(
                new MyCommand().getProcessedHelp(MyCommand.commandName)
            ).to.be.equal("Hello hello! It's Command hello.");
        });

        it('Should replace %app.bin%', () => {
            class MyCommand extends Command {
                static readonly commandName = 'hello';

                getHelp() {
                    return "Hello %app.bin%! It's Command %command.name%.";
                }
            }
            expect(
                new MyCommand().getProcessedHelp(
                    MyCommand.commandName,
                    'fullyName'
                )
            ).to.be.equal("Hello fullyName! It's Command hello.");
        });
    });

    describe('getHelper', () => {
        it('Should return the FormatterHelper', () => {
            const command = new Command();
            expect(command.getHelper()).to.be.instanceOf(FormatterHelper);
        });

        it('Should instantiate the FormatterHelper just one time', () => {
            const command = new Command();
            expect(command.getHelper()).to.be.equal(command.getHelper());
        });
    });

    describe('_validateCall', () => {
        class MyCommand extends Command {
            constructor() {
                super();

                this.addArgument(
                    new Argument(
                        '<aa>',
                        '',
                        new Validator().string().regexp(/a/)
                    )
                )
                    .addArgument(
                        new Argument(
                            '<ab>',
                            '',
                            new Validator().string().regexp(/b/)
                        )
                    )
                    .addArgument(
                        new Argument(
                            '[ac]',
                            'c',
                            new Validator().string().regexp(/c/),
                            'c'
                        )
                    )
                    .addArgument(
                        new Argument(
                            '[ad]',
                            'd',
                            new Validator().string().regexp(/d/),
                            'd'
                        )
                    )
                    .addOption(
                        new Option(
                            '-m <num>',
                            '',
                            new Validator().number().int(),
                            undefined,
                            true
                        )
                    )
                    .addOption(
                        new Option(
                            '-n <num>',
                            '',
                            new Validator().number().int()
                        )
                    )
                    .addOption(
                        new Option('-k, --koala-lol', '', undefined, 'a koala')
                    )
                    .addOption(
                        new Option(
                            '-l, --list <what>',
                            '',
                            new Validator().string()
                        )
                    );
            }
        }
        let command: Command;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        let commandProto: any;

        beforeEach(() => {
            command = new MyCommand();
            command.setName('aName');
            commandProto = Object.getPrototypeOf(command);
        });

        describe('_checkArgsRange', () => {
            it('Should not trhow error if correct arg number', () => {
                expect(
                    commandProto._checkArgsRange.bind(command, [
                        'a',
                        'b',
                        'c',
                        'd',
                    ])
                ).to.not.throw(WrongNumberOfArgumentError);
                expect(
                    commandProto._checkArgsRange.bind(command, ['a', 'b', 'c'])
                ).to.not.throw(WrongNumberOfArgumentError);
                expect(
                    commandProto._checkArgsRange.bind(command, ['a', 'b'])
                ).to.not.throw(WrongNumberOfArgumentError);
            });

            it('Should throw WrongNumberOfArgumentError if not enough args', () => {
                expect(
                    commandProto._checkArgsRange.bind(command, ['a'])
                ).to.throw(WrongNumberOfArgumentError);
                expect(
                    commandProto._checkArgsRange.bind(command, ['a'])
                ).to.throw(
                    'Wrong number of argument(s) for command aName. Got 1, ' +
                        'expected between 2 and 4.'
                );
            });

            it('Should throw WrongNumberOfArgumentError if too many args', () => {
                expect(
                    commandProto._checkArgsRange.bind(command, [
                        'a',
                        'b',
                        'c',
                        'd',
                        'e',
                    ])
                ).to.throw(WrongNumberOfArgumentError);
                expect(
                    commandProto._checkArgsRange.bind(command, [
                        'a',
                        'b',
                        'c',
                        'd',
                        'e',
                    ])
                ).to.throw(
                    'Wrong number of argument(s) for command aName. Got 5, ' +
                        'expected between 2 and 4.'
                );
            });
        });

        describe('_arrayArgumentsToRecord', () => {
            it('Should transform an array of arguments to a Record', () => {
                expect(
                    commandProto._arrayArgumentsToRecord.call(command, [
                        'a',
                        'b',
                        'c',
                        'd',
                    ])
                ).to.be.deep.equal({
                    aa: 'a',
                    ab: 'b',
                    ac: 'c',
                    ad: 'd',
                });
            });

            it('Should add the rest to an array for a variadic element', () => {
                class MyCommandVariadic extends Command {
                    constructor() {
                        super();

                        this.addArgument(new Argument('<aa>', '')).addArgument(
                            new Argument('<ab...>', '')
                        );
                    }
                }
                command = new MyCommandVariadic();
                commandProto = Object.getPrototypeOf(command);
                expect(
                    commandProto._arrayArgumentsToRecord.call(command, [
                        'a',
                        'b',
                        'c',
                        'd',
                    ])
                ).to.be.deep.equal({
                    aa: 'a',
                    ab: ['b', 'c', 'd'],
                });
            });
        });

        describe('_validateArgs', () => {
            it('Should add default values if arguments missing', () => {
                expect(
                    commandProto._validateArgs.call(command, {
                        aa: 'a',
                        ab: 'b',
                    })
                ).to.be.deep.equal({ aa: 'a', ab: 'b', ac: 'c', ad: 'd' });
            });

            it('Should throw MissingArgumentError if arguments missing and no default value', () => {
                expect(
                    commandProto._validateArgs.bind(command, {
                        aa: 'a',
                    })
                ).to.throw(MissingArgumentError);
                expect(
                    commandProto._validateArgs.bind(command, { aa: 'a' })
                ).to.throw('Missing "ab" argument.');
            });

            it('Should not throw if all required arguments defined', () => {
                expect(
                    commandProto._validateArgs.bind(command, {
                        aa: 'a',
                        ab: 'b',
                    })
                ).to.not.throw(MissingArgumentError);
                expect(
                    commandProto._validateArgs.bind(command, {
                        aa: 'a',
                        ab: 'b',
                        ac: 'c',
                    })
                ).to.not.throw(MissingArgumentError);
            });

            it("Should throw InvalidArgumentValueError if argument's value invalid", () => {
                expect(
                    commandProto._validateArgs.bind(command, {
                        aa: 'b',
                        ab: 'a',
                    })
                ).to.throw(InvalidArgumentValueError);
                expect(
                    commandProto._validateArgs.bind(command, {
                        aa: 'b',
                        ab: 'a',
                    })
                ).to.throw(
                    'Error: Invalid value for argument "aa"\n' +
                        '    Invalid\n' +
                        'Error: Invalid value for argument "ab"\n' +
                        '    Invalid\n'
                );
            });

            it("Should not throw InvalidArgumentValueError if argument's value valid", () => {
                expect(
                    commandProto._validateArgs.bind(command, {
                        aa: 'a',
                        ab: 'b',
                    })
                ).to.not.throw(InvalidArgumentValueError);
            });

            it('Should work with transform validator', () => {
                command.addArgument(
                    new Argument(
                        '[ae]',
                        'e',
                        new Validator().string().transform(val => val + 'bar')
                    )
                );
                expect(
                    commandProto._validateArgs.call(command, {
                        aa: 'a',
                        ab: 'b',
                        ae: 'foo',
                    })
                ).to.be.deep.equal({
                    aa: 'a',
                    ab: 'b',
                    ac: 'c',
                    ad: 'd',
                    ae: 'foobar',
                });
            });

            it('Should work with preprocess validator', () => {
                const validator = new Validator();
                command.addArgument(
                    new Argument(
                        '[ae]',
                        'e',
                        validator.preprocess(arg => {
                            if (typeof arg == 'string' || arg instanceof Date)
                                return new Date(arg);
                            throw new Error();
                        }, validator.date())
                    )
                );
                expect(
                    commandProto._validateArgs.call(command, {
                        aa: 'a',
                        ab: 'b',
                        ae: '2022-06-26',
                    })
                ).to.be.deep.equal({
                    aa: 'a',
                    ab: 'b',
                    ac: 'c',
                    ad: 'd',
                    ae: new Date('2022-06-26'),
                });
            });
        });

        describe('_checkRequiredOptions', () => {
            it('Should not throw if all required options defined', () => {
                expect(
                    commandProto._checkRequiredOptions.bind(command, { m: 1 })
                ).to.not.throw(MissingOptionError);
                expect(
                    commandProto._checkRequiredOptions.bind(command, {
                        m: 1,
                        n: 1,
                    })
                ).to.not.throw(MissingOptionError);
            });

            it('Should throw MissionOptionError if an option is missing', () => {
                expect(
                    commandProto._checkRequiredOptions.bind(command, { n: 1 })
                ).to.throw(MissingOptionError);
                expect(
                    commandProto._checkRequiredOptions.bind(command, { n: 1 })
                ).to.throw('Missing "m" option.');
            });

            it('Should give the default value if option not defined', () => {
                expect(
                    commandProto._checkRequiredOptions.call(command, { m: 1 })
                ).to.be.deep.equal({ m: 1, 'koala-lol': 'a koala' });
            });
        });

        describe('_validateOptions', () => {
            it('Should not validate reserved option', () => {
                expect(
                    commandProto._validateOptions.bind(command, {
                        v: true,
                    })
                ).to.not.throw(UnknownOptionError);
            });

            it('Should not throw error if reconized option', () => {
                expect(
                    commandProto._validateOptions.bind(command, {
                        m: true,
                        n: true,
                        k: true,
                        l: true,
                        'koala-lol': true,
                        list: true,
                    })
                ).to.not.throw(UnknownOptionError);
            });

            it('Should throw UnknownOptionError if unreconized option', () => {
                expect(
                    commandProto._validateOptions.bind(command, {
                        a: true,
                    })
                ).to.throw(UnknownOptionError);
                expect(
                    commandProto._validateOptions.bind(command, {
                        foobar: true,
                    })
                ).to.throw('Unknown --foobar option.');
            });

            it('Should throw InvalidOptionError if option value invalid', () => {
                expect(
                    commandProto._validateOptions.bind(command, { m: 'a' })
                ).to.throw(InvalidOptionValueError);
                expect(
                    commandProto._validateOptions.bind(command, { m: 'a' })
                ).to.throw(
                    'Error: Invalid value for option -m\n' +
                        '    Expected number, received string\n'
                );
            });

            it('Should work with preprocess validator', () => {
                const validator = new Validator();
                command.addOption(
                    new Option(
                        '--valid',
                        '',
                        validator.preprocess(opt => {
                            if (typeof opt == 'string' || opt instanceof Date)
                                return new Date(opt);
                            throw new Error();
                        }, validator.date())
                    )
                );
                expect(
                    commandProto._validateOptions.call(command, {
                        valid: '2022-06-26',
                    })
                ).to.be.deep.equal({
                    valid: new Date('2022-06-26'),
                });
            });
        });

        describe('_addLongNotationToOptions', () => {
            it('Should add the long option name if needed', () => {
                expect(
                    commandProto._addLongNotationToOptions.call(command, {
                        m: 1,
                        l: 'test',
                    })
                ).to.be.deep.equal({ m: 1, l: 'test', list: 'test' });
                expect(
                    commandProto._addLongNotationToOptions.call(command, {
                        k: true,
                    })
                ).to.be.deep.equal({ k: true, 'koala-lol': true });
                expect(
                    commandProto._addLongNotationToOptions.call(command, {
                        'koala-lol': true,
                    })
                ).to.be.deep.equal({ 'koala-lol': true });
            });
        });

        describe('_camelCaseOptions', () => {
            it('Should transform to camelCase the long option name', () => {
                expect(
                    commandProto._camelCaseOptions.call(command, {
                        m: 1,
                        list: 'test',
                        'koala-lol': true,
                    })
                ).to.be.deep.equal({
                    m: 1,
                    list: 'test',
                    koalaLol: true,
                });
            });

            it("Should delete the short name option if the long name don't exist", () => {
                expect(
                    commandProto._camelCaseOptions.call(command, {
                        m: 1,
                        l: 'test',
                        k: true,
                        'koala-lol': true,
                    })
                ).to.be.deep.equal({
                    m: 1,
                    list: 'test',
                    koalaLol: true,
                });
            });
        });
    });
});
