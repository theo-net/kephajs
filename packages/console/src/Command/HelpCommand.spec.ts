/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import Sinon, { SinonStub } from 'sinon';

import { ServiceContainer } from '@kephajs/core/ServiceContainer/ServiceContainer';
import { NullOutput } from '../Output/NullOutput';
import { Command } from './Command';
import { CommandRouter } from './CommandRouter';
import { HelpCommand } from './HelpCommand';
import { Option } from './Option';
import { UnknownCommand } from './Error/UnknownCommand';
import { Application } from '../Application';

describe('@kephajs/console/Command/HelpCommand', () => {
    let helpCommand: HelpCommand;
    let output: NullOutput;
    let stubWrite: SinonStub;
    let commandRouter: CommandRouter;

    function getOutput(stub: SinonStub) {
        const writed: string[] = [];
        stub.getCalls().forEach(call => {
            writed.push(...call.args[0].split('\n'));
        });
        return writed;
    }

    class Command1 extends Command {
        public static readonly commandName = 'azerty';

        public static readonly description = 'A command';
    }

    class Command2 extends Command {
        public static readonly commandName = 'foobar:foo';

        public static readonly description = 'A foobar FOO command';
    }

    class Command3 extends Command {
        public static readonly commandName = 'foobar:bar';

        public static readonly description = 'A foobar BAR command';
    }

    class Command4 extends Command {
        public static readonly commandName = 'foobar:azerty:nothing';

        public static readonly description = 'A nothing command';
    }

    class Command5 extends Command {
        public static readonly commandName = 'cache:log';

        public static readonly description = 'A cache command';
    }

    beforeEach(async () => {
        output = new NullOutput();
        stubWrite = Sinon.stub(output, 'write');
        const serviceContainer = new ServiceContainer();
        commandRouter = new CommandRouter(serviceContainer, output);
        const app = new Application('test');
        await app.init();
        serviceContainer.register('$commandRouter', commandRouter);
        serviceContainer.register('$app', app);
        helpCommand = new HelpCommand(commandRouter, app);
    });

    it('Should print the basic usage', () => {
        commandRouter.addCommands([
            Command1,
            Command2,
            Command3,
            Command4,
            Command5,
        ]);

        helpCommand.run({ output: output, args: [], options: {} });

        expect(getOutput(stubWrite)).to.be.deep.equal([
            '<fg=bright-red>Usage:</>',
            '  command [options] [arguments]',
            '',
            '<fg=bright-red>Options:</>',
            '<fg=green>  -q, --quiet</>  Do not output any message',
            '<fg=green>  -v|vv|vvv</>    Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug',
            '',
            '<fg=bright-red>Available commands:</>',
            '<fg=green>  azerty</>                 A command',
            '<fg=green>  help</>                   Display help for a command',
            '<fg=green>  list</>                   List command',
            '<fg=green>  version</>                Version of the application',
            '<fg=bright-red> cache</>',
            '<fg=green>  cache:log</>              A cache command',
            '<fg=bright-red> foobar</>',
            '<fg=green>  foobar:azerty:nothing</>  A nothing command',
            '<fg=green>  foobar:bar</>             A foobar BAR command',
            '<fg=green>  foobar:foo</>             A foobar FOO command',
        ]);
    });

    it('Should print the help for help command', () => {
        helpCommand.run({
            output: output,
            args: { commandName: 'help' },
            options: {},
        });

        expect(getOutput(stubWrite)).to.be.deep.equal([
            '<fg=bright-red>Description:</>',
            '  Display help for a command',
            '  ',
            '<fg=bright-red>Usage:</>',
            '  help [options] [command_name]',
            '',
            '<fg=bright-red>Arguments:</>',
            '<fg=green>  command_name</>  The command name',
            '',
            '<fg=bright-red>Options:</>',
            '<fg=green>  -q, --quiet</>   Do not output any message',
            '<fg=green>  -v|vv|vvv</>     Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug',
            '',
            '<fg=bright-red>Help:</>',
            '  The <info>help</info> command displays help for a given command:',
            '  ',
            '    <info>mocha.js help list</>',
            '  ',
            '  To display the list of available commands, please use the <info>list</> command.',
        ]);
    });

    it('Should print the help for a command', () => {
        helpCommand.run({
            output: output,
            args: { commandName: 'list' },
            options: {},
        });

        expect(getOutput(stubWrite)).to.be.deep.equal([
            '<fg=bright-red>Description:</>',
            '  List command',
            '  ',
            '<fg=bright-red>Usage:</>',
            '  list [options] [namespace]',
            '',
            '<fg=bright-red>Arguments:</>',
            '<fg=green>  namespace</>    The namespace name',
            '',
            '<fg=bright-red>Options:</>',
            '<fg=green>  -q, --quiet</>  Do not output any message',
            '<fg=green>  -v|vv|vvv</>    Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug',
            '',
            '<fg=bright-red>Help:</>',
            '  The <info>list</info> command lists all commands:',
            '  ',
            '    <info>mocha.js list</>',
            '  ',
            '  You can also display the commands for a specific namespace:',
            '  ',
            '    <info>mocha.js list test</>',
        ]);
    });

    it("Should don't print the description and help parts if nothing to show", () => {
        class MyCommand extends Command {
            static readonly commandName = 'hello';

            constructor() {
                super();
                this.addOption(
                    new Option('--format <format>', 'The output format')
                );
            }
        }
        commandRouter.addCommands([MyCommand]);
        helpCommand.run({
            output: output,
            args: { commandName: 'hello' },
            options: {},
        });

        expect(getOutput(stubWrite)).to.be.deep.equal([
            '<fg=bright-red>Usage:</>',
            '  hello [options]',
            '',
            '<fg=bright-red>Options:</>',
            '<fg=green>  --format \\<format\\></>  The output format',
            '<fg=green>  -q, --quiet</>        Do not output any message',
            '<fg=green>  -v|vv|vvv</>          Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug',
        ]);
    });

    it("Should throw UnknownCommand if command doesn't exist", () => {
        try {
            helpCommand.run({
                output: output,
                args: { commandName: 'hello' },
                options: {},
            });
        } catch (error) {
            expect(error).to.be.instanceOf(UnknownCommand);
            return;
        }
        expect.fail('Should have thrown');
    });
});
