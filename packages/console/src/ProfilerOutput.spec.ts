/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Profiler } from '@kephajs/core/Profiler';
import { ProfilerOutput } from './ProfilerOutput';
import { Output } from './Output/Output';
import { OutputFormatter } from './Output/OutputFormatter';

describe('@kephajs/console/ProfilerOutput', () => {
    describe('render', () => {
        it('Should return nothing if nothing registered in teh profiler', () => {
            expect(new ProfilerOutput(new Profiler()).render()).to.be.equal('');
        });

        it('Should return a tabulate list of the element', () => {
            const profiler = new Profiler();
            profiler.register('first', 123);
            profiler.register('second', 456);
            profiler.register('third', 789);

            const output = new Output();
            output.setStream(process.stdout);
            output.setFormatter(new OutputFormatter());
            output.writeln(new ProfilerOutput(profiler).render());
            expect(new ProfilerOutput(profiler).render()).to.be.equal(
                [
                    '<fg=gray>    0ms</>    first',
                    '<fg=gray>    ' + (456 - 123) + 'ms</>  second',
                    '<fg=gray>    ' + (789 - 456) + 'ms</>  third',
                ].join('\n')
            );
        });
    });
});
