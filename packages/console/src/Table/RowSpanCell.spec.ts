/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import Sinon from 'sinon';

import { Cell } from './Cell';
import { RowSpanCell } from './RowSpanCell';
import { defaultTableOptions } from './Table';

describe('@kephajs/console/Table/RowSpanCell', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let original: Record<string, any>;
    const tableOptions = { ...defaultTableOptions, rowHeights: [2, 3, 4, 5] };

    beforeEach(function () {
        original = {
            rowSpan: 3,
            y: 0,
            draw: Sinon.spy(),
        };
    });

    it('Should have an mergeTableOptions function', () => {
        expect(new RowSpanCell(original as Cell)).to.respondTo('init');
        new RowSpanCell(original as Cell).mergeTableOptions(); // nothing happens.
    });

    it('Should draw top of the next row', () => {
        const spanner = new RowSpanCell(original as Cell);
        spanner.x = 0;
        spanner.y = 1;
        spanner.init(tableOptions);
        spanner.draw('top');
        expect(original.draw.calledOnce).to.be.equal(true);
        expect(original.draw.calledWith(2)).to.be.equal(true);
    });

    it('drawing line 0 of the next row', () => {
        const spanner = new RowSpanCell(original as Cell);
        spanner.x = 0;
        spanner.y = 1;
        spanner.init(tableOptions);
        spanner.draw(0);
        expect(original.draw.calledOnce).to.be.equal(true);
        expect(original.draw.calledWith(3)).to.be.equal(true);
    });

    it('drawing line 1 of the next row', () => {
        const spanner = new RowSpanCell(original as Cell);
        spanner.x = 0;
        spanner.y = 1;
        spanner.init(tableOptions);
        spanner.draw(1);
        expect(original.draw.calledOnce).to.be.equal(true);
        expect(original.draw.calledWith(4)).to.be.equal(true);
    });

    it('drawing top of two rows below', () => {
        const spanner = new RowSpanCell(original as Cell);
        spanner.x = 0;
        spanner.y = 2;
        spanner.init(tableOptions);
        spanner.draw('top');
        expect(original.draw.calledOnce).to.be.equal(true);
        expect(original.draw.calledWith(6)).to.be.equal(true);
    });

    it('drawing line 0 of two rows below', () => {
        const spanner = new RowSpanCell(original as Cell);
        spanner.x = 0;
        spanner.y = 2;
        spanner.init(tableOptions);
        spanner.draw(0);
        expect(original.draw.calledOnce).to.be.equal(true);
        expect(original.draw.calledWith(7)).to.be.equal(true);
    });

    it('drawing line 1 of two rows below', () => {
        const spanner = new RowSpanCell(original as Cell);
        spanner.x = 0;
        spanner.y = 2;
        spanner.init(tableOptions);
        spanner.draw(1);
        expect(original.draw.calledOnce).to.be.equal(true);
        expect(original.draw.calledWith(8)).to.be.equal(true);
    });

    it('drawing bottom', () => {
        const spanner = new RowSpanCell(original as Cell);
        spanner.x = 0;
        spanner.y = 1;
        spanner.init(tableOptions);
        spanner.draw('bottom');
        expect(original.draw.calledOnce).to.be.equal(true);
        expect(original.draw.calledWith('bottom')).to.be.equal(true);
    });
});
