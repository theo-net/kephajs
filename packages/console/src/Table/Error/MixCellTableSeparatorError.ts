/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export class MixCellTableSeparatorError extends Error {
    constructor(rowIndex: number) {
        super(`The row ${rowIndex} cannot mix Cell and TableSeparator.`);
    }
}
