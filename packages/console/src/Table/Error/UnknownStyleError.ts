/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export class UnknownStyleError extends Error {
    constructor(style: string) {
        super(`Unknown table style "${style}".`);
    }
}
