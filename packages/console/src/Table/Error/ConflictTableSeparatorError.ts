/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export class ConflictTableSeparatorError extends Error {
    constructor(rowIndex: number) {
        super(
            `The TableSeparator in row ${rowIndex} is in conflict with a Cell rowSpan.`
        );
    }
}
