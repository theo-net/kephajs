/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Cell } from './Cell';
import { ColSpanCell } from './ColSpanCell';
import { ConflictTableSeparatorError } from './Error/ConflictTableSeparatorError';
import { LayoutManagerError } from './Error/LayoutManagerError';
import { MixCellTableSeparatorError } from './Error/MixCellTableSeparatorError';
import { computeHeights, computeWidths, LayoutManager } from './LayoutManager';
import { RowSpanCell } from './RowSpanCell';
import { CellDeclaration } from './Table';
import { TableSeparator } from './TableSeparator';

describe('@kephajs/console/Table/LayoutManager', () => {
    describe('computeWidths', () => {
        function mc(
            y: number,
            x: number,
            desiredWidth: number,
            colSpan?: number
        ) {
            const cell = new Cell({ content: '', colSpan });
            cell.x = x;
            cell.y = y;
            cell.desiredWidth = desiredWidth;

            return cell;
        }

        it('Should find the maximum desired width of each column', () => {
            const cells = [
                [mc(0, 0, 7), mc(0, 1, 3), mc(0, 2, 5)],
                [mc(1, 0, 8), mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 6), mc(2, 1, 9), mc(2, 2, 1)],
            ];

            expect(computeWidths([], cells)).to.deep.equal([8, 9, 5]);
        });

        it('Sould not change hard coded values', () => {
            const cells = [
                [mc(0, 0, 7), mc(0, 1, 3), mc(0, 2, 5)],
                [mc(1, 0, 8), mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 6), mc(2, 1, 9), mc(2, 2, 1)],
            ];

            expect(computeWidths([null, 3], cells)).to.deep.equal([8, 3, 5]);
        });

        it('Should assume undefined desiredWidth is 1', () => {
            const cell1 = new Cell('');
            const cell2 = new Cell('');
            const cell3 = new Cell('');
            cell1.x = cell2.x = cell3.x = 0;
            cell1.y = 0;
            cell2.y = 1;
            cell3.y = 2;

            const cells = [[cell1], [cell2], [cell3]];
            expect(computeWidths([], cells)).to.deep.equal([1]);
        });

        it('Should take into account colSpan and wont over expand', () => {
            const cells = [
                [mc(0, 0, 10, 2), mc(0, 2, 5)],
                [mc(1, 0, 5), mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)],
            ];
            expect(computeWidths([], cells)).to.deep.equal([5, 5, 5]);
        });

        it('Should will expand rows involved in colSpan in a balanced way', () => {
            const cells = [
                [mc(0, 0, 13, 2), mc(0, 2, 5)],
                [mc(1, 0, 5), mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)],
            ];
            expect(computeWidths([], cells)).to.deep.equal([6, 6, 5]);
        });

        it('Should expand across 3 cols', () => {
            const cells = [
                [mc(0, 0, 25, 3)],
                [mc(1, 0, 5), mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)],
            ];
            expect(computeWidths([], cells)).to.deep.equal([9, 9, 5]);
        });

        it('Should have multiple spans in same table', () => {
            const cells = [
                [mc(0, 0, 25, 3)],
                [mc(1, 0, 30, 3)],
                [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)],
            ];
            expect(computeWidths([], cells)).to.deep.equal([11, 9, 8]);
        });

        it('Should work with multiple span and normal cell', () => {
            const cells = [
                [mc(0, 0, 7), mc(0, 1, 9, 2)],
                [mc(1, 0, 9, 2), mc(1, 2, 7)],
                [mc(2, 0, 7), mc(2, 1, 7), mc(2, 2, 7)],
            ];
            expect(computeWidths([], cells)).to.deep.equal([7, 7, 7]);
        });

        it('Should span will only edit uneditable tables', () => {
            const cells = [
                [mc(0, 0, 20, 3)],
                [mc(1, 0, 4), mc(1, 1, 20), mc(1, 2, 5)],
            ];
            expect(computeWidths([null, 3], cells)).to.deep.equal([7, 3, 8]);
        });

        it('Should spans will only edit uneditable tables - first column uneditable', () => {
            const cells = [
                [mc(0, 0, 20, 3)],
                [mc(1, 0, 4), mc(1, 1, 3), mc(1, 2, 5)],
            ];
            expect(computeWidths([3], cells)).to.deep.equal([3, 7, 8]);
        });
    });

    describe('computeHeights', () => {
        function mc(
            y: number,
            x: number,
            desiredHeight: number,
            rowSpan?: number
        ) {
            const cell = new Cell({ content: '', rowSpan });
            cell.x = x;
            cell.y = y;
            cell.desiredHeight = desiredHeight;

            return cell;
        }

        it('Should find the maximum desired height of each row', () => {
            const cells = [
                [mc(0, 0, 7), mc(0, 1, 3), mc(0, 2, 5)],
                [mc(1, 0, 8), mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 6), mc(2, 1, 9), mc(2, 2, 1)],
            ];
            expect(computeHeights([], cells)).to.deep.equal([7, 8, 9]);
        });

        it('Should not touch hard coded values', () => {
            const cells = [
                [mc(0, 0, 7), mc(0, 1, 3), mc(0, 2, 5)],
                [mc(1, 0, 8), mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 6), mc(2, 1, 9), mc(2, 2, 1)],
            ];
            expect(computeHeights([null, 3], cells)).to.deep.equal([7, 3, 9]);
        });

        it('Should assume undefined desiredHeight is 1', () => {
            const cell1 = new Cell('');
            const cell2 = new Cell('');
            const cell3 = new Cell('');
            cell1.x = 0;
            cell2.x = 1;
            cell3.x = 2;
            cell1.y = cell2.y = cell3.y = 0;
            const cells = [[cell1, cell2, cell3]];
            expect(computeHeights([], cells)).to.deep.equal([1]);
        });

        it('Should take into account rowSpan and wont over expand', () => {
            const cells = [
                [mc(0, 0, 10, 2), mc(0, 1, 5), mc(0, 2, 2)],
                [mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)],
            ];
            expect(computeHeights([], cells)).to.deep.equal([5, 5, 4]);
        });

        it('Should expand rows involved in rowSpan in a balanced way', () => {
            const cells = [
                [mc(0, 0, 13, 2), mc(0, 1, 5), mc(0, 2, 5)],
                [mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 0, 4), mc(2, 1, 2), mc(2, 2, 1)],
            ];
            expect(computeHeights([], cells)).to.deep.equal([6, 6, 4]);
        });

        it('Should expand across 3 rows', () => {
            const cells = [
                [mc(0, 0, 25, 3), mc(0, 1, 5), mc(0, 2, 4)],
                [mc(1, 1, 5), mc(1, 2, 2)],
                [mc(2, 1, 2), mc(2, 2, 1)],
            ];
            expect(computeHeights([], cells)).to.deep.equal([9, 9, 5]);
        });

        it('Should multiple spans in same table', () => {
            const cells = [
                [mc(0, 0, 25, 3), mc(0, 1, 30, 3), mc(0, 2, 4)],
                [mc(1, 2, 2)],
                [mc(2, 2, 1)],
            ];
            expect(computeHeights([], cells)).to.deep.equal([11, 9, 8]);
        });
    });

    function findCell(
        table: (Cell | ColSpanCell | RowSpanCell | TableSeparator)[][],
        x: number,
        y: number
    ) {
        for (let i = 0; i < table.length; i++) {
            const row = table[i];

            for (let j = 0; j < row.length; j++) {
                const cell = row[j];
                if (cell.x === x && cell.y === y) return cell;
            }
        }
        return null;
    }

    function checkExpectation(
        actualCell: null | Cell | RowSpanCell | ColSpanCell | TableSeparator,
        expectedCell:
            | string
            | Cell
            | CellDeclaration
            | { spannerFor: number[] }
            | { tableSeparator: boolean },
        x: number,
        y: number,
        actualTable: (Cell | ColSpanCell | RowSpanCell | TableSeparator)[][]
    ) {
        if (typeof expectedCell === 'string')
            expectedCell = { content: expectedCell };

        const address = '(' + y + ',' + x + ')';

        if (Object.prototype.hasOwnProperty.call(expectedCell, 'content')) {
            expect(actualCell, address).to.be.instanceOf(Cell);
            expect(
                (actualCell as Cell).content,
                'content of ' + address
            ).to.equal((expectedCell as Cell).content);
        }

        if (Object.prototype.hasOwnProperty.call(expectedCell, 'rowSpan')) {
            expect(actualCell, address).to.be.instanceOf(Cell);
            expect(
                (actualCell as Cell).rowSpan,
                'rowSpan of ' + address
            ).to.equal((expectedCell as Cell).rowSpan);
        }

        if (Object.prototype.hasOwnProperty.call(expectedCell, 'colSpan')) {
            expect(actualCell, address).to.be.instanceOf(Cell);
            expect(
                (actualCell as Cell).colSpan,
                'colSpan of ' + address
            ).to.equal((expectedCell as Cell).colSpan);
        }

        if (Object.prototype.hasOwnProperty.call(expectedCell, 'spannerFor')) {
            expect(actualCell, address).to.be.instanceOf(RowSpanCell);

            if (actualCell instanceof RowSpanCell) {
                expect(
                    actualCell.originalCell,
                    address + 'originalCell should be a cell'
                ).to.be.instanceOf(Cell);
                expect(
                    actualCell.originalCell,
                    address + 'originalCell not right'
                ).to.equal(
                    findCell(
                        actualTable,
                        (expectedCell as { spannerFor: number[] })
                            .spannerFor[1],
                        (expectedCell as { spannerFor: number[] }).spannerFor[0]
                    )
                );
            }
        }

        if (
            Object.prototype.hasOwnProperty.call(expectedCell, 'tableSeparator')
        ) {
            expect(actualCell, address).to.be.instanceOf(TableSeparator);
        }
    }

    function checkLayout(
        actualTable: (Cell | ColSpanCell | RowSpanCell | TableSeparator)[][],
        expectedTable: (
            | null
            | Cell
            | string
            | CellDeclaration
            | { spannerFor: number[] }
            | { tableSeparator: boolean }
        )[][]
    ) {
        expectedTable.forEach((expectedRow, y) => {
            expectedRow.forEach((expectedCell, x) => {
                if (expectedCell !== null) {
                    const actualCell = findCell(actualTable, x, y);
                    checkExpectation(
                        actualCell,
                        expectedCell,
                        x,
                        y,
                        actualTable
                    );
                }
            });
        });
    }

    function checkAdress(
        actual: (Cell | TableSeparator)[][],
        expected: {
            content?: string | null;
            x: number;
            y: number;
            colSpan?: number;
            rowSpan?: number;
        }[][]
    ) {
        actual.forEach((row, rowIndex) => {
            row.forEach((cell, colIndex) => {
                expect(cell.x, 'checkLayout: x value invalid').to.be.equal(
                    expected[rowIndex][colIndex].x
                );
                expect(cell.y, 'checkLayout: y value invalid').to.be.equal(
                    expected[rowIndex][colIndex].y
                );
                if (cell instanceof Cell) {
                    expect(
                        cell.content,
                        'checkLayout: content value invalid'
                    ).to.be.equal(expected[rowIndex][colIndex].content);
                    expect(
                        cell.colSpan,
                        'checkLayout: colSpan value invalid'
                    ).to.be.equal(expected[rowIndex][colIndex].colSpan);
                    expect(
                        cell.rowSpan,
                        'checkLayout: rowSpan value invalid'
                    ).to.be.equal(expected[rowIndex][colIndex].rowSpan);
                }
            });
        });
    }

    describe('_layoutTable', () => {
        let lm: LayoutManager;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        let proto: any;

        beforeEach(() => {
            lm = new LayoutManager([['']], 'foobar');
            proto = Object.getPrototypeOf(lm);
        });

        it('Should set x and y', () => {
            const table = proto._generateCells.call(lm, [
                ['', ''],
                new TableSeparator(),
                ['', ''],
            ]);
            proto._layoutTable.call(lm, table);

            checkAdress(table, [
                [
                    {
                        content: '',
                        x: 0,
                        y: 0,
                        colSpan: 1,
                        rowSpan: 1,
                    },
                    {
                        content: '',
                        x: 1,
                        y: 0,
                        colSpan: 1,
                        rowSpan: 1,
                    },
                ],
                [{ x: 0, y: 1 }],
                [
                    {
                        content: '',
                        x: 0,
                        y: 2,
                        colSpan: 1,
                        rowSpan: 1,
                    },
                    {
                        content: '',
                        x: 1,
                        y: 2,
                        colSpan: 1,
                        rowSpan: 1,
                    },
                ],
            ]);
            expect(proto.getMaxWidth.call(lm, table)).to.equal(2);
        });

        it('Should throw an error if row mix Cell and TableSeparator', () => {
            try {
                const table = proto._generateCells.call(lm, [
                    ['', ''],
                    [new TableSeparator(), ''],
                    ['', ''],
                ]);
                proto._layoutTable.call(lm, table);
            } catch (error) {
                expect(error).to.be.instanceOf(MixCellTableSeparatorError);
                expect(
                    (error as MixCellTableSeparatorError).message
                ).to.be.equal('The row 1 cannot mix Cell and TableSeparator.');
                return;
            }
            try {
                const table = proto._generateCells.call(lm, [
                    ['', ''],
                    ['', ''],
                    ['', new TableSeparator()],
                    ['', ''],
                ]);
                proto._layoutTable.call(lm, table);
            } catch (error) {
                expect(error).to.be.instanceOf(MixCellTableSeparatorError);
                expect(
                    (error as MixCellTableSeparatorError).message
                ).to.be.equal('The row 2 cannot mix Cell and TableSeparator.');
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should colSpan will push x values to the right', () => {
            const table = proto._generateCells.call(lm, [
                [{ content: '', colSpan: 2 }, ''],
                ['', { content: '', colSpan: 2 }],
            ]);
            proto._layoutTable.call(lm, table);

            checkAdress(table, [
                [
                    {
                        content: '',
                        x: 0,
                        y: 0,
                        colSpan: 2,
                        rowSpan: 1,
                    },
                    {
                        content: '',
                        x: 2,
                        y: 0,
                        colSpan: 1,
                        rowSpan: 1,
                    },
                ],
                [
                    {
                        content: '',
                        x: 0,
                        y: 1,
                        colSpan: 1,
                        rowSpan: 1,
                    },
                    {
                        content: '',
                        x: 1,
                        y: 1,
                        colSpan: 2,
                        rowSpan: 1,
                    },
                ],
            ]);

            expect(proto.getMaxWidth.call(lm, table)).to.equal(3);
        });

        it('Should rowSpan will push x values on cells below', () => {
            const table = proto._generateCells.call(lm, [
                [{ content: '', rowSpan: 2 }, ''],
                [''],
            ]);
            proto._layoutTable.call(lm, table);

            checkAdress(table, [
                [
                    { content: '', x: 0, y: 0, colSpan: 1, rowSpan: 2 },
                    { content: '', x: 1, y: 0, colSpan: 1, rowSpan: 1 },
                ],
                [{ content: '', x: 1, y: 1, colSpan: 1, rowSpan: 1 }],
            ]);

            expect(proto.getMaxWidth.call(lm, table)).to.equal(2);
        });

        it('Should work with colSpan and rowSpan together', () => {
            const table = proto._generateCells.call(lm, [
                [{ content: '', rowSpan: 2, colSpan: 2 }, ''],
                [''],
            ]);
            proto._layoutTable.call(lm, table);

            checkAdress(table, [
                [
                    { content: '', x: 0, y: 0, rowSpan: 2, colSpan: 2 },
                    { content: '', x: 2, y: 0, colSpan: 1, rowSpan: 1 },
                ],
                [{ content: '', x: 2, y: 1, colSpan: 1, rowSpan: 1 }],
            ]);

            expect(proto.getMaxWidth.call(lm, table)).to.equal(3);
        });

        it('Should complex layout', () => {
            const table = proto._generateCells.call(lm, [
                [
                    { content: 'a' },
                    { content: 'b' },
                    { content: 'c', rowSpan: 3, colSpan: 2 },
                    { content: 'd' },
                ],
                [{ content: 'e', rowSpan: 2, colSpan: 2 }, { content: 'f' }],
                [{ content: 'g' }],
            ]);
            proto._layoutTable.call(lm, table);

            checkAdress(table, [
                [
                    { content: 'a', y: 0, x: 0, colSpan: 1, rowSpan: 1 },
                    { content: 'b', y: 0, x: 1, colSpan: 1, rowSpan: 1 },
                    { content: 'c', y: 0, x: 2, rowSpan: 3, colSpan: 2 },
                    { content: 'd', y: 0, x: 4, colSpan: 1, rowSpan: 1 },
                ],
                [
                    { content: 'e', rowSpan: 2, colSpan: 2, y: 1, x: 0 },
                    { content: 'f', y: 1, x: 4, colSpan: 1, rowSpan: 1 },
                ],
                [{ content: 'g', y: 2, x: 4, colSpan: 1, rowSpan: 1 }],
            ]);
        });

        it('Should maxWidth of single element', () => {
            expect(new LayoutManager([['']], 'foobar').getMaxWidth()).to.equal(
                1
            );
        });
    });

    describe('_fillInTable', () => {
        function mc(opts: CellDeclaration, y: number, x: number): Cell {
            const cell = new Cell(opts);

            cell.x = x;
            cell.y = y;

            return cell;
        }

        let lm: LayoutManager;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        let proto: any;

        beforeEach(() => {
            lm = new LayoutManager([['']], 'foobar');
            proto = Object.getPrototypeOf(lm);
        });

        it('Should add blank out individual cells', () => {
            const cells = [[mc('a', 0, 1)], [mc('b', 1, 0)]];
            proto._fillInTable.call(lm, cells);

            checkLayout(cells, [
                ['', 'a'],
                ['b', ''],
            ]);
        });

        it('Should add autospan to the right', () => {
            const cells = [[], [mc('a', 1, 1)]];
            proto._fillInTable.call(lm, cells);

            checkLayout(cells, [
                [{ content: '', colSpan: 2 }, null],
                ['', 'a'],
            ]);
        });

        it('Should add TableSeparator to the right', () => {
            const ts = new TableSeparator();
            ts.y = 1;
            const cells = [[], [ts], [mc('a', 2, 1)]];
            proto._fillInTable.call(lm, cells);

            checkLayout(cells, [
                [{ content: '', colSpan: 2 }, null],
                [{ tableSeparator: true }, { tableSeparator: true }],
                ['', 'a'],
            ]);
        });

        it('Throw error if cell conflict with TableSeparator', () => {
            try {
                const ts = new TableSeparator();
                const cell = mc('a', 0, 1);
                cell.rowSpan = 2;
                const cells = [[mc('b', 0, 0), cell], [ts]];
                proto._fillInTable.call(lm, cells);
            } catch (error) {
                expect(error).to.be.instanceOf(ConflictTableSeparatorError);
                expect(
                    (error as ConflictTableSeparatorError).message
                ).to.be.equal(
                    'The TableSeparator in row 1 is in conflict with a Cell rowSpan.'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    describe('_addRowSpanCells', () => {
        function mc(opts: CellDeclaration, y: number, x: number): Cell {
            const cell = new Cell(opts);

            cell.x = x;
            cell.y = y;

            return cell;
        }

        let lm: LayoutManager;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        let proto: any;

        beforeEach(() => {
            lm = new LayoutManager([['']], 'foobar');
            proto = Object.getPrototypeOf(lm);
        });

        it('Should add autospan down', () => {
            const cells = [[mc('a', 0, 1)], []];
            proto._fillInTable.call(lm, cells);
            proto._addRowSpanCells.call(lm, cells);

            checkLayout(cells, [
                [{ content: '', rowSpan: 2 }, 'a'],
                [{ spannerFor: [0, 0] }, ''],
            ]);
        });

        it('Should add autospan right and down', () => {
            const cells = [[mc('a', 0, 2)], [], [mc('b', 2, 1)]];
            proto._fillInTable.call(lm, cells);
            proto._addRowSpanCells.call(lm, cells);

            checkLayout(cells, [
                [{ content: '', colSpan: 2, rowSpan: 2 }, null, 'a'],
                [
                    { spannerFor: [0, 0] },
                    null,
                    { content: '', colSpan: 1, rowSpan: 2 },
                ],
                ['', 'b', { spannerFor: [1, 2] }],
            ]);
        });

        it('Should insert a rowSpan cell - beginning of line', () => {
            const table = [
                [
                    { x: 0, y: 0, rowSpan: 2 },
                    { x: 1, y: 0 },
                ],
                [{ x: 1, y: 1 }],
            ];

            proto._addRowSpanCells.call(lm, table);

            expect(table[0]).to.deep.equal([
                { x: 0, y: 0, rowSpan: 2 },
                { x: 1, y: 0 },
            ]);
            expect(table[1].length).to.equal(2);
            expect(table[1][0]).to.be.instanceOf(RowSpanCell);
            expect(table[1][1]).to.deep.equal({ x: 1, y: 1 });
        });

        it('Should insert a rowSpan cell - end of line', () => {
            const table = [
                [
                    { x: 0, y: 0 },
                    { x: 1, y: 0, rowSpan: 2 },
                ],
                [{ x: 0, y: 1 }],
            ];

            proto._addRowSpanCells.call(lm, table);

            expect(table[0]).to.deep.equal([
                { x: 0, y: 0 },
                { rowSpan: 2, x: 1, y: 0 },
            ]);
            expect(table[1].length).to.equal(2);
            expect(table[1][0]).to.deep.equal({ x: 0, y: 1 });
            expect(table[1][1]).to.be.instanceOf(RowSpanCell);
        });

        it('Should insert a rowSpan cell - middle of line', () => {
            const table = [
                [
                    { x: 0, y: 0 },
                    { x: 1, y: 0, rowSpan: 2 },
                    { x: 2, y: 0 },
                ],
                [
                    { x: 0, y: 1 },
                    { x: 2, y: 1 },
                ],
            ];

            proto._addRowSpanCells.call(lm, table);

            expect(table[0]).to.deep.equal([
                { x: 0, y: 0 },
                { rowSpan: 2, x: 1, y: 0 },
                { x: 2, y: 0 },
            ]);
            expect(table[1].length).to.equal(3);
            expect(table[1][0]).to.deep.equal({ x: 0, y: 1 });
            expect(table[1][1]).to.be.instanceOf(RowSpanCell);
            expect(table[1][2]).to.deep.equal({ x: 2, y: 1 });
        });

        it('Should insert a rowSpan cell - multiple on the same line', () => {
            const table = [
                [
                    { x: 0, y: 0 },
                    { x: 1, y: 0, rowSpan: 2 },
                    { x: 2, y: 0, rowSpan: 2 },
                    { x: 3, y: 0 },
                ],
                [
                    { x: 0, y: 1 },
                    { x: 3, y: 1 },
                ],
            ];

            proto._addRowSpanCells.call(lm, table);

            expect(table[0]).to.deep.equal([
                { x: 0, y: 0 },
                { rowSpan: 2, x: 1, y: 0 },
                { rowSpan: 2, x: 2, y: 0 },
                { x: 3, y: 0 },
            ]);
            expect(table[1].length).to.equal(4);
            expect(table[1][0]).to.deep.equal({ x: 0, y: 1 });
            expect(table[1][1]).to.be.instanceOf(RowSpanCell);
            expect(table[1][2]).to.be.instanceOf(RowSpanCell);
            expect(table[1][3]).to.deep.equal({ x: 3, y: 1 });
        });

        it('Should throw an error if not enought lines', () => {
            const table = [
                [
                    { x: 0, y: 0 },
                    { x: 1, y: 0, rowSpan: 3 },
                ],
                [{ x: 0, y: 1 }],
            ];
            try {
                proto._addRowSpanCells.call(lm, table);
            } catch (error) {
                expect(error).to.be.instanceOf(LayoutManagerError);
                expect((error as LayoutManagerError).message).to.be.equal(
                    'Not enought rows ine the table for the cell {1;0} with a rowspan of 3. You need to add 1 row(s).'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    describe('constructor (make table layout)', () => {
        it('Should work with simple 2x2 layout', () => {
            const actual = new LayoutManager(
                [
                    ['hello', 'goodbye'],
                    ['hola', 'adios'],
                ],
                'foobar'
            ).getCells();

            const expected = [
                ['hello', 'goodbye'],
                ['hola', 'adios'],
            ];

            checkLayout(actual, expected);
        });

        it('Should work with cross table', () => {
            const actual = new LayoutManager(
                [{ '1.0': ['yes', 'no'] }, { '2.0': ['hello', 'goodbye'] }],
                'foobar'
            ).getCells();

            const expected = [
                ['1.0', 'yes', 'no'],
                ['2.0', 'hello', 'goodbye'],
            ];

            checkLayout(actual, expected);
        });

        it('Should work with vertical table', () => {
            const actual = new LayoutManager(
                [{ '1.0': 'yes' }, { '2.0': 'hello' }],
                'foobar'
            ).getCells();

            const expected = [
                ['1.0', 'yes'],
                ['2.0', 'hello'],
            ];

            checkLayout(actual, expected);
        });

        it('Should work with colSpan adds RowSpanCells to the right', () => {
            const actual = new LayoutManager(
                [[{ content: 'hello', colSpan: 2 }], ['hola', 'adios']],
                'foobar'
            ).getCells();

            const expected = [
                [{ content: 'hello', colSpan: 2 }, null],
                ['hola', 'adios'],
            ];

            checkLayout(actual, expected);
        });

        it('Should work with rowSpan adds RowSpanCell below', () => {
            const actual = new LayoutManager(
                [[{ content: 'hello', rowSpan: 2 }, 'goodbye'], ['adios']],
                'foobar'
            ).getCells();

            const expected = [
                ['hello', 'goodbye'],
                [{ spannerFor: [0, 0] }, 'adios'],
            ];

            checkLayout(actual, expected);
        });

        it('Should work with rowSpan and cellSpan together', () => {
            const actual = new LayoutManager(
                [
                    [{ content: 'hello', rowSpan: 2, colSpan: 2 }, 'goodbye'],
                    ['adios'],
                ],
                'foobar'
            ).getCells();

            const expected = [
                ['hello', null, 'goodbye'],
                [{ spannerFor: [0, 0] }, null, 'adios'],
            ];

            checkLayout(actual, expected);
        });

        it('Should work with complex layout', () => {
            const actual = new LayoutManager(
                [
                    [
                        { content: 'hello', rowSpan: 2, colSpan: 2 },
                        { content: 'yo', rowSpan: 2, colSpan: 2 },
                        'goodbye',
                    ],
                    ['adios'],
                ],
                'foobar'
            ).getCells();

            const expected = [
                ['hello', null, 'yo', null, 'goodbye'],
                [
                    { spannerFor: [0, 0] },
                    null,
                    { spannerFor: [0, 2] },
                    null,
                    'adios',
                ],
            ];

            checkLayout(actual, expected);
        });

        it('Should work with complex layout2', () => {
            const actual = new LayoutManager(
                [
                    ['a', 'b', { content: 'c', rowSpan: 3, colSpan: 2 }, 'd'],
                    [{ content: 'e', rowSpan: 2, colSpan: 2 }, 'f'],
                    ['g'],
                ],
                'foobar'
            ).getCells();

            const expected = [
                ['a', 'b', 'c', null, 'd'],
                ['e', null, { spannerFor: [0, 2] }, null, 'f'],
                [
                    { spannerFor: [1, 0] },
                    null,
                    { spannerFor: [0, 2] },
                    null,
                    'g',
                ],
            ];

            checkLayout(actual, expected);
        });

        it('Should work with stairstep spans', () => {
            const actual = new LayoutManager(
                [
                    [{ content: '', rowSpan: 2 }, ''],
                    [{ content: '', rowSpan: 2 }],
                    [''],
                ],
                'foobar'
            ).getCells();

            const expected = [
                [{ content: '', rowSpan: 2 }, ''],
                [{ spannerFor: [0, 0] }, { content: '', rowSpan: 2 }],
                ['', { spannerFor: [1, 1] }],
            ];

            checkLayout(actual, expected);
        });
    });
});
