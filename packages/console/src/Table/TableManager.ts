/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Output } from '../Output/Output';
import { TableStyle } from './TableStyle';
import { Table, TableOptions } from './Table';
import { UnknownStyleError } from './Error/UnknownStyleError';

export class TableManager {
    public static $inject = ['$output'];

    private _styles: Record<string, TableStyle> = {};

    constructor(private _output?: Output) {
        this._initStyles();
    }

    addStyle(name: string, style: TableStyle) {
        this._styles[name] = style;
    }

    getStyle(style?: string | TableStyle) {
        if (style === undefined) return this._styles.default;

        if (style instanceof TableStyle) return style;

        if (this._styles[style] === undefined)
            throw new UnknownStyleError(style);

        return this._styles[style];
    }

    table(style?: string, options: Partial<TableOptions> = {}) {
        if (style !== undefined && options.style === undefined) {
            options.style = this.getStyle(style);
        } else if (options.style === undefined) {
            options.style = this.getStyle('default');
        }
        return new Table(options, this, this._output);
    }

    private _initStyles() {
        this.addStyle('default', new TableStyle());
        this.addStyle(
            'compact',
            new TableStyle(
                {
                    verticalOutside: '',
                    verticalInside: ' ',
                    horizontalOutside: '',
                    horizontalInside: '',
                    topMid: '',
                    topLeft: '',
                    topRight: '',
                    bottomMid: '',
                    bottomLeft: '',
                    bottomRight: '',
                    midMid: '',
                    midLeft: '',
                    midRight: '',
                    mid: '',
                    left: '',
                    right: '',
                    crossIntersection: '',
                    crossBottom: '',
                    crossMid: '',
                    truncate: '…',
                    padding: ' ',
                },
                {
                    head: undefined,
                    cell: undefined,
                    borderOutside: undefined,
                    borderInside: undefined,
                },
                {
                    left: 0,
                    right: 0,
                },
                true
            )
        );
        this.addStyle(
            'borderless',
            new TableStyle(
                {
                    verticalOutside: '',
                    verticalInside: ' ',
                    horizontalOutside: '=',
                    horizontalInside: '-',
                    topMid: ' ',
                    topLeft: '',
                    topRight: '',
                    bottomMid: ' ',
                    bottomLeft: '',
                    bottomRight: '',
                    midMid: ' ',
                    midLeft: '',
                    midRight: '',
                    mid: ' ',
                    left: '',
                    right: '',
                    crossIntersection: ' ',
                    crossBottom: ' ',
                    crossMid: ' ',
                    truncate: '…',
                    padding: ' ',
                },
                {
                    head: undefined,
                    cell: undefined,
                    borderOutside: undefined,
                    borderInside: undefined,
                },
                {
                    left: 0,
                    right: 0,
                }
            )
        );
        this.addStyle(
            'box',
            new TableStyle(
                {
                    verticalOutside: '│',
                    verticalInside: '│',
                    horizontalOutside: '─',
                    horizontalInside: '╌',
                    topMid: '┬',
                    topLeft: '┌',
                    topRight: '┐',
                    bottomMid: '┴',
                    bottomLeft: '└',
                    bottomRight: '┘',
                    midMid: '┼',
                    midLeft: '├',
                    midRight: '┤',
                    mid: '┼',
                    left: '├',
                    right: '┤',
                    crossIntersection: '┼',
                    crossBottom: '┴',
                    crossMid: '┼',
                    truncate: '…',
                    padding: ' ',
                },
                {
                    head: undefined,
                    cell: undefined,
                    borderOutside: undefined,
                    borderInside: undefined,
                },
                {
                    left: 1,
                    right: 1,
                }
            )
        );
        this.addStyle(
            'dashed',
            new TableStyle(
                {
                    verticalOutside: '╎',
                    verticalInside: '╎',
                    horizontalOutside: '╌',
                    horizontalInside: '╌',
                    topMid: '┬',
                    topLeft: '╭',
                    topRight: '╮',
                    bottomMid: '┴',
                    bottomLeft: '╰',
                    bottomRight: '╯',
                    midMid: '┼',
                    midLeft: '├',
                    midRight: '┤',
                    mid: '┼',
                    left: '├',
                    right: '┤',
                    crossIntersection: '┼',
                    crossBottom: '┴',
                    crossMid: '┼',
                    truncate: '…',
                    padding: ' ',
                },
                {
                    head: undefined,
                    cell: undefined,
                    borderOutside: undefined,
                    borderInside: undefined,
                },
                {
                    left: 1,
                    right: 1,
                }
            )
        );
        this.addStyle(
            'box-heavy',
            new TableStyle(
                {
                    verticalOutside: '┃',
                    verticalInside: '│',
                    horizontalOutside: '━',
                    horizontalInside: '─',
                    topMid: '┯',
                    topLeft: '┏',
                    topRight: '┓',
                    bottomMid: '┷',
                    bottomLeft: '┗',
                    bottomRight: '┛',
                    midMid: '┿',
                    midLeft: '┣',
                    midRight: '┫',
                    mid: '┼',
                    left: '┠',
                    right: '┨',
                    crossIntersection: '╋',
                    crossBottom: '┻',
                    crossMid: '╂',
                    truncate: '…',
                    padding: ' ',
                },
                {
                    head: undefined,
                    cell: undefined,
                    borderOutside: undefined,
                    borderInside: undefined,
                },
                {
                    left: 1,
                    right: 1,
                }
            )
        );
        this.addStyle(
            'box-double',
            new TableStyle(
                {
                    verticalOutside: '║',
                    verticalInside: '│',
                    horizontalOutside: '═',
                    horizontalInside: '─',
                    topMid: '╤',
                    topLeft: '╔',
                    topRight: '╗',
                    bottomMid: '╧',
                    bottomLeft: '╚',
                    bottomRight: '╝',
                    midMid: '╪',
                    midLeft: '╠',
                    midRight: '╣',
                    mid: '┼',
                    left: '╟',
                    right: '╢',
                    crossIntersection: '╬',
                    crossBottom: '╩',
                    crossMid: '╫',
                    truncate: '…',
                    padding: ' ',
                },
                {
                    head: undefined,
                    cell: undefined,
                    borderOutside: undefined,
                    borderInside: undefined,
                },
                {
                    left: 1,
                    right: 1,
                }
            )
        );
        this.addStyle(
            'rounded',
            new TableStyle(
                {
                    verticalOutside: '│',
                    verticalInside: '│',
                    horizontalOutside: '─',
                    horizontalInside: '╌',
                    topMid: '┬',
                    topLeft: '╭',
                    topRight: '╮',
                    bottomMid: '┴',
                    bottomLeft: '╰',
                    bottomRight: '╯',
                    midMid: '┼',
                    midLeft: '├',
                    midRight: '┤',
                    mid: '┼',
                    left: '├',
                    right: '┤',
                    crossIntersection: '┼',
                    crossBottom: '┴',
                    crossMid: '┼',
                    truncate: '…',
                    padding: ' ',
                },
                {
                    head: undefined,
                    cell: undefined,
                    borderOutside: undefined,
                    borderInside: undefined,
                },
                {
                    left: 1,
                    right: 1,
                }
            )
        );
    }
}
