/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/*
 * Inspired by `cli-table2` James Talmage <james.talmage@jrtechnical.com>
 */

import { computeHeights, computeWidths, LayoutManager } from './LayoutManager';
import { Output } from '../Output/Output';
import { Cell } from './Cell';
import { TableManager } from './TableManager';
import { TableStyle } from './TableStyle';
import { ColSpanCell } from './ColSpanCell';
import { RowSpanCell } from './RowSpanCell';
import { TableSeparator } from './TableSeparator';

export type TableOptions = {
    style: TableStyle;
    compact: boolean;
    head: string[];
    colWidths: number[];
    rowHeights: number[];
    colAligns: ('left' | 'center' | 'right')[];
    rowAligns: ('top' | 'middle' | 'bottom')[];
    hAlign: 'left' | 'center' | 'right';
    vAlign: 'top' | 'middle' | 'bottom';
    wordWrap: boolean;
};

export const defaultTableOptions: TableOptions = {
    style: new TableStyle(),
    compact: false,
    head: [],
    colWidths: [],
    rowHeights: [],
    colAligns: [],
    rowAligns: [],
    hAlign: 'left',
    vAlign: 'top',
    wordWrap: false,
};

export type CellDeclarationObject = {
    content: string;
    head?: boolean;
    style?: TableStyle | string;
    colSpan?: number;
    rowSpan?: number;
    hAlign?: 'left' | 'center' | 'right';
    vAlign?: 'top' | 'middle' | 'bottom';
};

export type CellDeclaration =
    | string
    | number
    | boolean
    | null
    | undefined
    | CellDeclarationObject;

export type RowDeclaration =
    | TableSeparator
    | TableSeparator[]
    | CellDeclaration[]
    | Record<string, CellDeclaration>
    | Record<string, CellDeclaration[]>;

export class Table extends Array<RowDeclaration> {
    protected _options: TableOptions;

    constructor(
        options: Partial<TableOptions | { style: TableStyle | string }> = {},
        private _tableManager?: TableManager,
        private _output?: Output
    ) {
        super();

        this._options = this._mergeOptions(options);
    }

    getOptions() {
        return this._options;
    }

    toString() {
        return this.render();
    }

    render(output?: Output) {
        const array: RowDeclaration[] = [];

        // add header
        const headersPresent = this._options.head.length > 0;
        if (headersPresent) {
            array.push(this._options.head);
        } else this._options.head = [];
        if (this.length > 0) array.push(...this);

        const cells = new LayoutManager(
            array,
            this._options.style,
            this._tableManager
        ).getCells();
        cells.forEach((row, line) => {
            row.forEach(cell => {
                if (headersPresent && line === 0) cell.head = true;
                cell.mergeTableOptions(this._options, cells as Cell[][]);
            });
        });
        this._options.colWidths = computeWidths(
            this._options.colWidths,
            cells as Cell[][]
        );
        this._options.rowHeights = computeHeights(
            this._options.rowHeights,
            cells as Cell[][]
        );
        cells.forEach(row => {
            row.forEach(cell => {
                cell.init(this._options);
            });
        });

        // render, line after line
        const result: string[] = [];

        for (let rowIndex = 0; rowIndex < cells.length; rowIndex++) {
            const row = cells[rowIndex];
            const heightOfRow = this._options.rowHeights[rowIndex];

            // top
            if (
                rowIndex === 0 ||
                !(this._options.compact || this._options.style.isCompact()) ||
                (rowIndex == 1 && headersPresent)
            )
                this._doDraw(row, 'top', result);

            // other lines
            for (let lineNum = 0; lineNum < heightOfRow; lineNum++)
                this._doDraw(row, lineNum, result);

            // bottom
            if (rowIndex + 1 == cells.length)
                this._doDraw(row, 'bottom', result);
        }

        const resultString = result.join('\n');

        if (output instanceof Output) output.writeln(resultString);
        else if (this._output instanceof Output)
            this._output.writeln(resultString);

        return resultString;
    }

    get width() {
        const str = this.render().split('\n');

        return str[0].length;
    }

    private _mergeOptions(
        options: Partial<TableOptions | { style: TableStyle | string }>
    ) {
        if (this._tableManager instanceof TableManager) {
            options.style = this._tableManager.getStyle(options.style);
        } else if (typeof options.style === 'string') {
            throw new Error();
        }

        return {
            ...defaultTableOptions,
            ...options,
        } as TableOptions;
    }

    _doDraw(
        row: (Cell | ColSpanCell | RowSpanCell | TableSeparator)[],
        lineNum: number | string,
        result: string[]
    ) {
        const line: string[] = [];

        row.forEach(cell => {
            line.push(cell.draw(lineNum));
        });

        const str = line.join('');
        if (str.length) result.push(str);
    }
}
