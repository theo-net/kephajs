/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/*
 * Inspired by `cli-table2` James Talmage <james.talmage@jrtechnical.com>
 */

import {
    strlenMaxLineStylized,
    truncateStylized,
    wordWrapStylized,
} from '../Helpers/Helpers';
import { OutputFormatter } from '../Output/OutputFormatter';
import { ColSpanCell } from './ColSpanCell';
import { RowSpanCell } from './RowSpanCell';
import { CellDeclaration, defaultTableOptions, TableOptions } from './Table';
import { TableManager } from './TableManager';
import { TableSeparator } from './TableSeparator';
import {
    defaultTableStyleChars,
    defaultTableStylePaddings,
    defaultTableStyleStyles,
    TableStyle,
    TableStyleChars,
    TableStyleStyles,
} from './TableStyle';

const defaultStyle = new TableStyle();

export class Cell {
    public cells:
        | (Cell | ColSpanCell | RowSpanCell | TableSeparator)[][]
        | null = null;

    public content: string;

    public head = false;

    public x: number | null = null;

    public y: number | null = null;

    public widths: number[] = [];

    public heights: number[] = [];

    public desiredWidth = 0;

    public desiredHeight = 0;

    public width = 0;

    public height = 0;

    public colSpan = 1;

    public rowSpan = 1;

    public hAlign: 'left' | 'center' | 'right' | null = null;

    public vAlign: 'top' | 'middle' | 'bottom' | null = null;

    public drawRight = false;

    public style = defaultStyle;

    public lines: string[] = [];

    private _options = defaultTableOptions;

    private _chars = defaultTableStyleChars;

    private _paddings = defaultTableStylePaddings;

    private _styles = defaultTableStyleStyles;

    constructor(
        cell: CellDeclaration,
        style?: TableStyle | string,
        private _tableManager?: TableManager
    ) {
        if (cell === false) this.content = 'false';
        else if (cell === true) this.content = 'true';
        else if (cell === null || cell === undefined) this.content = '';
        else if (typeof cell === 'number') this.content = cell.toString();
        else if (typeof cell === 'string') this.content = cell;
        else {
            this.content = cell.content ?? '';
            this.colSpan = cell.colSpan ?? 1;
            this.rowSpan = cell.rowSpan ?? 1;
            this.hAlign = cell.hAlign ?? null;
            this.vAlign = cell.vAlign ?? null;
            this.head = cell.head ?? false;
            if (style === undefined) style = cell.style;
        }

        this._setStyle(style);
    }

    mergeTableOptions(
        tableOptions: TableOptions,
        cells: (Cell | ColSpanCell | RowSpanCell | TableSeparator)[][] = []
    ) {
        this.cells = cells;
        this._options = { ...this._options, ...tableOptions };

        if (this.style === defaultStyle) this._setStyle(this._options.style);

        let fixedWidth = tableOptions.colWidths[this.x as number];
        if (tableOptions.wordWrap && fixedWidth) {
            fixedWidth -= this._paddings.left + this._paddings.right;
            this.lines = OutputFormatter.stylizeLines(
                wordWrapStylized(fixedWidth, this.content)
            );
        } else
            this.lines = OutputFormatter.stylizeLines(this.content.split('\n'));

        this.desiredWidth =
            strlenMaxLineStylized(this.content) +
            this._paddings.left +
            this._paddings.right;
        this.desiredHeight = this.lines.length;
    }

    init(tableOptions: TableOptions) {
        this.widths = tableOptions.colWidths.slice(
            this.x as number,
            (this.x as number) + this.colSpan
        );
        this.heights = tableOptions.rowHeights.slice(
            this.y as number,
            (this.y as number) + this.rowSpan
        );

        this.width = this.widths.reduce(
            (a, b) => {
                return a + b + 1;
            },
            this.widths.length ? -1 : 0
        );
        this.height = this.heights.reduce(
            (a, b) => {
                return a + b + 1;
            },
            this.heights.length ? -1 : 0
        );

        this.hAlign =
            this.hAlign ||
            tableOptions.colAligns[this.x as number] ||
            this._options.hAlign;
        this.vAlign =
            this.vAlign ||
            tableOptions.rowAligns[this.y as number] ||
            this._options.vAlign;

        this.drawRight =
            (this.x as number) + this.colSpan == tableOptions.colWidths.length;
    }

    /**
     * Draw a line of the cell
     * @param lineNum 'top' | 'bottom' or line number
     * @param spanningCell `number` if called from `RowSpanCell`, `number` as vertical padding, else `undefined`
     */
    draw(lineNum: string | number, spanningCell?: number) {
        if (lineNum == 'top') return this.drawTop(this.drawRight);
        if (lineNum == 'bottom') return this.drawBottom(this.drawRight);

        const padLen = Math.max(this.height - this.lines.length, 0);

        let padTop;
        switch (this.vAlign) {
            case 'middle':
                padTop = Math.ceil(padLen / 2);
                break;
            case 'bottom':
                padTop = padLen;
                break;
            default:
                padTop = 0;
        }

        if (lineNum < padTop || lineNum >= padTop + this.lines.length)
            return this.drawEmpty(this.drawRight, spanningCell as number);

        const forceTruncation =
            this.lines.length > this.height &&
            (lineNum as number) + 1 >= this.height;

        return this.drawLine(
            (lineNum as number) - padTop,
            this.drawRight,
            forceTruncation,
            spanningCell as number
        );
    }

    /**
     * Draw top border
     * @param drawRight `true` if method render right border
     */
    drawTop(drawRight: boolean) {
        const content = [];
        let underHead = false;

        if (this.cells !== null) {
            this.widths.forEach((width, index) => {
                if (
                    this._isCornerCrossTable(this) &&
                    this._chars.horizontalOutside !== ''
                ) {
                    content.push(
                        this._chars.padding.repeat(
                            width + this._chars.topLeft.length
                        )
                    );
                } else {
                    underHead = this._isUnderHeader();
                    content.push(this._topLeftChar(index));
                    content.push(
                        this._applyStyle(
                            this.y == 0 ||
                                (underHead &&
                                    this.cells !== null &&
                                    this.y !== null &&
                                    this.cells[this.y - 1][index + 1].head)
                                ? 'borderOutside'
                                : 'borderInside',
                            this._chars[
                                this.y == 0 ||
                                (underHead &&
                                    this.cells !== null &&
                                    this.y !== null &&
                                    this.cells[this.y - 1][index + 1].head)
                                    ? 'horizontalOutside'
                                    : 'horizontalInside'
                            ].repeat(width)
                        )
                    );
                }
            });
        } else {
            content.push(this._topLeftChar(0));
            content.push(
                this._applyStyle(
                    this.y == 0 ? 'borderOutside' : 'borderInside',
                    this._chars[
                        this.y == 0 ? 'horizontalOutside' : 'horizontalInside'
                    ].repeat(this.width)
                )
            );
        }

        if (drawRight)
            content.push(
                this._applyStyle(
                    'borderOutside',
                    this._chars[
                        this.y == 0
                            ? 'topRight'
                            : underHead
                            ? 'midRight'
                            : 'right'
                    ]
                )
            );

        return content.join('');
    }

    /**
     * Draw bottom border
     * @param drawRight `true` if method render right border
     */
    drawBottom(drawRight: boolean) {
        let left = this._chars.bottomMid;
        if (this.x === 0) left = this._chars.bottomLeft;
        if (
            this.x === 1 &&
            this.cells !== null &&
            this.y !== null &&
            this.cells[this.y][0].head
        )
            left = this._chars.crossBottom;

        const content = this._chars.horizontalOutside.repeat(this.width);
        const right = drawRight ? this._chars.bottomRight : '';

        return this._applyStyle('borderOutside', left + content + right);
    }

    /**
     * Draw empty line. Used for the top/bottom padding
     * @param drawRight `true` if method render right border
     */
    drawEmpty(drawRight?: boolean, spanningCell?: number) {
        if (this._isCornerCrossTable(this))
            return this._chars.padding.repeat(
                this.width + this._chars.verticalOutside.length
            );

        let leftChar: keyof TableStyleChars = 'verticalInside';
        if (this.x === 0) leftChar = 'verticalOutside';
        else if (
            this.x === 1 &&
            this.y !== null &&
            this.cells !== null &&
            this.cells[this.y][0].head
        )
            leftChar = this._chars.verticalOutside.length
                ? 'verticalOutside'
                : 'verticalInside';

        if (this.x && spanningCell && this.cells !== null) {
            let cellLeft =
                this.cells[(this.y as number) + spanningCell][
                    (this.x as number) - 1
                ];
            while (cellLeft instanceof ColSpanCell)
                cellLeft =
                    this.cells[cellLeft.y as number][
                        (cellLeft.x as number) - 1
                    ];

            if (!(cellLeft instanceof RowSpanCell)) leftChar = 'midRight';
        }

        const right = drawRight ? this._chars.verticalOutside : '';
        const content = this._chars.padding.repeat(this.width);

        return this._stylizeLine(leftChar, content, right);
    }

    /**
     * Draw text line
     *
     * @param lineNum Number of text line. Can be padding, not content
     * @param drawRight `true` if method render right border
     * @param forceTruncationSymbol `true` if need to show truncat char for vetical cut, else just display for horizontal cut
     */
    drawLine(
        lineNum: number,
        drawRight: boolean,
        forceTruncationSymbol: boolean,
        spanningCell: number
    ) {
        if (this._isCornerCrossTable(this))
            return this._chars.padding.repeat(
                this.width + this._chars.verticalOutside.length
            );

        let leftChar: keyof TableStyleChars = 'verticalInside';
        if (this.x === 0) leftChar = 'verticalOutside';
        else if (
            this.x === 1 &&
            this.y !== null &&
            this.cells !== null &&
            this.cells[this.y][0].head &&
            (this.y > 0 ||
                this._isCornerCrossTable(this.cells[0][0]) ||
                !this.head)
        )
            leftChar = this._chars.verticalOutside.length
                ? 'verticalOutside'
                : 'verticalInside';

        if (this.x && spanningCell && this.cells) {
            let cellLeft =
                this.cells[(this.y as number) + spanningCell][this.x - 1];
            while (cellLeft instanceof ColSpanCell)
                cellLeft =
                    this.cells[cellLeft.y as number][
                        (cellLeft.x as number) - 1
                    ];

            if (!(cellLeft instanceof RowSpanCell)) leftChar = 'midRight';
        }

        const leftPadding = ' '.repeat(this._paddings.left);
        const right = drawRight ? this._chars.verticalOutside : '';
        const rightPadding = ' '.repeat(this._paddings.right);
        let line = this.lines[lineNum];
        const len = this.width - (this._paddings.left + this._paddings.right);
        if (forceTruncationSymbol) line += this._chars.truncate;

        let content = truncateStylized(line ?? '', len, this._chars.truncate);
        const contentLength = strlenMaxLineStylized(content);

        // Add padding
        if (len + 1 >= contentLength) {
            const padlen = len - contentLength;
            switch (this.hAlign) {
                case 'right':
                    content = ' '.repeat(padlen) + content;
                    break;
                case 'center':
                    content =
                        ' '.repeat(padlen - Math.ceil(padlen / 2)) +
                        content +
                        ' '.repeat(Math.ceil(padlen / 2));
                    break;
                default:
                    content = content + ' '.repeat(padlen);
                    break;
            }
        }

        content = leftPadding + content + rightPadding;

        return this._stylizeLine(leftChar, content, right);
    }

    private _setStyle(style?: string | TableStyle) {
        if (
            typeof style === 'string' &&
            this._tableManager instanceof TableManager
        ) {
            this.style = this._tableManager.getStyle(style);
        } else if (style instanceof TableStyle) this.style = style;

        this._chars = this.style.getChars();
        this._paddings = this.style.getPaddings();
        this._styles = this.style.getStyles();
    }

    private _topLeftChar(offset: number) {
        const x = (this.x as number) + offset;
        let leftChar: keyof TableStyleChars;

        // First line: header and cross corner
        if (this.y == 0)
            leftChar =
                x == 0
                    ? 'topLeft'
                    : offset == 0
                    ? x === 1 &&
                      this.cells !== null &&
                      this._isCornerCrossTable(this.cells[0][0])
                        ? this._chars.topLeft !== ''
                            ? 'topLeft'
                            : this._chars.topMid === '' // For compact style
                            ? 'topMid'
                            : 'padding'
                        : 'topMid'
                    : 'horizontalOutside';
        else {
            // First column
            if (x === 0 && this.cells === null) leftChar = 'left';
            // First cell of the horizontal header for cross table
            else if (
                x === 0 &&
                this.y === 1 &&
                this.cells !== null &&
                this._isCornerCrossTable(this.cells[0][0])
            )
                leftChar = 'topLeft';
            else {
                leftChar = offset == 0 ? 'mid' : 'bottomMid';

                if (this.cells) {
                    // Check if span
                    const spanAbove =
                        this.cells[(this.y as number) - 1][x] instanceof
                        ColSpanCell;
                    const afterHeader =
                        this.cells[(this.y as number) - 1][x].head;
                    leftChar =
                        offset == 0
                            ? afterHeader
                                ? 'midMid'
                                : 'mid'
                            : 'bottomMid';

                    if (spanAbove)
                        leftChar =
                            offset == 0
                                ? 'topMid'
                                : afterHeader
                                ? 'horizontalOutside'
                                : 'horizontalInside';

                    if (offset == 0) {
                        let i = 1;
                        while (
                            this.cells[this.y as number][x - i] instanceof
                            ColSpanCell
                        )
                            i++;

                        if (
                            this.cells[this.y as number][x - i] instanceof
                            RowSpanCell
                        )
                            leftChar = 'midLeft';
                    }

                    // first line after header for non crossing table
                    if (x === 0)
                        leftChar =
                            afterHeader &&
                            this.cells[(this.y as number) - 1][x + 1].head
                                ? 'midLeft'
                                : 'left';
                    // Intersection of the vertical and the horizontal headers
                    else if (
                        x === 1 &&
                        this.y === 1 &&
                        this._isCornerCrossTable(this.cells[0][0])
                    )
                        leftChar = 'crossIntersection';
                    // cell just near the horizontal header
                    else if (
                        x === 1 &&
                        this.y !== null &&
                        this.cells !== null &&
                        this.cells[this.y][0].head
                    )
                        leftChar = 'crossMid';
                }
            }
        }

        if (
            [
                'verticalOutside',
                'horizontalOutside',
                'topMid',
                'topLeft',
                'bottomMid',
                'midMid',
                'midLeft',
                'left',
                'crossIntersection',
                'crossMid',
            ].indexOf(leftChar) > -1
        )
            return this._applyStyle('borderOutside', this._chars[leftChar]);
        else return this._applyStyle('borderInside', this._chars[leftChar]);
    }

    private _applyStyle(style: keyof TableStyleStyles, content: string) {
        if (content === '') return '';
        if (this._styles[style] !== undefined) {
            return `<${this._styles[style]}>${content}</>`;
        } else return content;
    }

    private _stylizeLine(
        leftChar: keyof TableStyleChars,
        content: string,
        rightBorder: string
    ) {
        const leftBorder = this._applyStyle(
            leftChar === 'verticalOutside' || leftChar === 'midRight'
                ? 'borderOutside'
                : 'borderInside',
            this._chars[leftChar]
        );
        rightBorder = this._applyStyle('borderOutside', rightBorder);
        content = this._applyStyle(this.head ? 'head' : 'cell', content);

        return leftBorder + content + rightBorder;
    }

    private _isCornerCrossTable(
        cell: Cell | ColSpanCell | RowSpanCell | TableSeparator
    ) {
        return (
            cell instanceof Cell &&
            cell.head &&
            cell.x === 0 &&
            cell.y === 0 &&
            cell.content === '' &&
            cell.cells !== null &&
            cell.cells[0][1] &&
            cell.cells[0][1].head &&
            cell.cells[1][0] &&
            cell.cells[1][0].head
        );
    }

    private _isUnderHeader() {
        return (
            this.cells !== null &&
            this.x !== null &&
            this.y !== null &&
            this.y > 0 &&
            this.cells[this.y - 1][this.x].head
        );
    }
}
