/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Cell } from './Cell';
import { defaultTableOptions, TableOptions } from './Table';
import { TableManager } from './TableManager';
import { TableStyle } from './TableStyle';

describe('@kephajs/console/Table/Cell', () => {
    describe('constructor', () => {
        it('Should set colSpan and rowSpan default to 1', () => {
            const cell = new Cell({ content: '' });
            expect(cell.colSpan).to.equal(1);
            expect(cell.rowSpan).to.equal(1);
        });

        it('Should set colSpan and rowSpan can be set via constructor', () => {
            const cell = new Cell({ content: '', colSpan: 2, rowSpan: 3 });
            expect(cell.colSpan).to.equal(2);
            expect(cell.rowSpan).to.equal(3);
        });

        it('Should set content', () => {
            expect(new Cell('hello\nworld').content).to.equal('hello\nworld');
        });

        it("Should set null or undefined content to ''", () => {
            expect(new Cell(null).content).to.equal('');
            expect(new Cell(undefined).content).to.equal('');
        });

        it('Should set number content as string', () => {
            expect(new Cell(42).content).to.equal('42');
        });

        it('Should set boolean content as string', () => {
            expect(new Cell(false).content).to.equal('false');
            expect(new Cell(true).content).to.equal('true');
        });

        it('Should set hAlign default to null', () => {
            const cell = new Cell({ content: '' });
            expect(cell.hAlign).to.equal(null);
        });

        it('Should set hAlign', () => {
            const cell = new Cell({ content: '', hAlign: 'right' });
            expect(cell.hAlign).to.equal('right');
        });

        it('Should set vAlign default to null', () => {
            const cell = new Cell({ content: '' });
            expect(cell.vAlign).to.equal(null);
        });

        it('Should set vAlign', () => {
            const cell = new Cell({ content: '', vAlign: 'bottom' });
            expect(cell.vAlign).to.equal('bottom');
        });

        it('Should set head default to false', () => {
            const cell = new Cell({ content: '' });
            expect(cell.head).to.equal(false);
        });

        it('Should set head', () => {
            const cell = new Cell({ content: '', head: true });
            expect(cell.head).to.equal(true);
        });

        it('Should set a default style', () => {
            expect(new Cell('').style).instanceOf(TableStyle);
        });

        it('Should set a style with TableStyle instance', () => {
            const style = new TableStyle();
            expect(new Cell('', style).style).to.be.equal(style);
            expect(new Cell({ content: '', style }).style).to.be.equal(style);
        });

        it('Should set a style with string name and TableManager instance', () => {
            const style = new TableStyle();
            const tm = new TableManager();
            tm.addStyle('foobar', style);
            expect(new Cell('', 'foobar', tm).style).to.be.equal(style);
            expect(
                new Cell({ content: '', style: 'foobar' }, undefined, tm).style
            ).to.be.equal(style);
        });
    });

    describe('mergeTableOptions', () => {
        it('Should register cells', () => {
            const table = [[new Cell('')]];
            const cell = new Cell('');
            cell.mergeTableOptions(defaultTableOptions, table);
            expect(cell.cells).to.be.equal(table);
        });

        it('Should merge the style', () => {
            const style = new TableStyle();
            const options = { ...defaultTableOptions, style };
            const cell = new Cell('');
            cell.mergeTableOptions(options);
            expect(cell.style).to.be.equal(style);
        });

        it('Should not merge the style if cell have already one', () => {
            const style = new TableStyle();
            const style2 = new TableStyle();
            const options = { ...defaultTableOptions, style };
            const cell = new Cell('', style2);
            cell.mergeTableOptions(options);
            expect(cell.style).to.be.equal(style2);
        });

        describe('desiredWidth', () => {
            it('content(hello) padding(1,1) == 7', () => {
                const cell = new Cell('hello');
                cell.mergeTableOptions(defaultTableOptions);
                expect(cell.desiredWidth).to.be.equal(7);
            });

            it('content(hi) padding(1,2) == 5', () => {
                const style = new TableStyle(undefined, undefined, {
                    left: 1,
                    right: 2,
                });
                const cell = new Cell({
                    content: 'hi',
                    style,
                });
                cell.mergeTableOptions(defaultTableOptions);
                expect(cell.desiredWidth).to.be.equal(5);
            });

            it('content(hi) padding(3,2) == 7', () => {
                const style = new TableStyle(undefined, undefined, {
                    left: 3,
                    right: 2,
                });
                const cell = new Cell({
                    content: 'hi',
                    style,
                });
                cell.mergeTableOptions(defaultTableOptions);
                expect(cell.desiredWidth).to.be.equal(7);
            });
        });

        describe('desiredHeight', () => {
            it('1 lines of text', () => {
                const cell = new Cell('hi');
                cell.mergeTableOptions(defaultTableOptions);
                expect(cell.desiredHeight).to.be.equal(1);
            });

            it('2 lines of text', () => {
                const cell = new Cell('hi\nbye');
                cell.mergeTableOptions(defaultTableOptions);
                expect(cell.desiredHeight).to.be.equal(2);
            });

            it('2 lines of text', () => {
                const cell = new Cell('hi\nbye\nyo');
                cell.mergeTableOptions(defaultTableOptions);
                expect(cell.desiredHeight).to.be.equal(3);
            });
        });

        describe('multiline stylization', () => {
            it('Should keep single line stylization', () => {
                const cell = new Cell('<error>Hello World!</>');
                cell.mergeTableOptions(defaultTableOptions);
                expect(cell.lines).to.be.deep.equal(['<error>Hello World!</>']);
            });

            it('Should keep multiline lines stylization', () => {
                const cell = new Cell('<error>Hello\nWorld!</>');
                cell.mergeTableOptions(defaultTableOptions);
                expect(cell.lines).to.be.deep.equal([
                    '<error>Hello</>',
                    '<error>World!</>',
                ]);
            });
        });
    });

    describe('init', () => {
        describe('hAlign', () => {
            it('Should take colAlign value from tableOptions if not set in cell', () => {
                const tableOptions = { ...defaultTableOptions };
                tableOptions.colAligns = ['left', 'right', 'center'];

                let cell = new Cell('');
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.hAlign).to.be.equal('left');

                cell = new Cell('');
                cell.x = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.hAlign).to.be.equal('right');

                cell = new Cell('');
                cell.x = 2;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.hAlign).to.be.equal('center');
            });

            it('Should take cell value and override tableOptions', () => {
                const tableOptions = { ...defaultTableOptions };
                tableOptions.colAligns = ['left', 'right', 'center'];

                let cell = new Cell({ content: '', hAlign: 'right' });
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.hAlign).to.be.equal('right');

                cell = new Cell({ content: '', hAlign: 'left' });
                cell.x = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.hAlign).to.be.equal('left');

                cell = new Cell({ content: '', hAlign: 'right' });
                cell.x = 2;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.hAlign).to.be.equal('right');
            });
        });

        describe('vAlign', () => {
            it('Should take rowAlign value from tableOptions if not set in cell', () => {
                const tableOptions = { ...defaultTableOptions };
                tableOptions.rowAligns = ['top', 'bottom', 'middle'];

                let cell = new Cell('');
                cell.y = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.vAlign).to.be.equal('top');

                cell = new Cell('');
                cell.y = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.vAlign).to.be.equal('bottom');

                cell = new Cell('');
                cell.y = 2;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.vAlign).to.be.equal('middle');
            });

            it('Should take cell value and override tableOptions', () => {
                const tableOptions = { ...defaultTableOptions };
                tableOptions.rowAligns = ['top', 'bottom', 'middle'];

                let cell = new Cell({ content: '', vAlign: 'bottom' });
                cell.y = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.vAlign).to.be.equal('bottom');

                cell = new Cell({ content: '', vAlign: 'top' });
                cell.y = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.vAlign).to.be.equal('top');

                cell = new Cell({ content: '', vAlign: 'middle' });
                cell.y = 2;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.vAlign).to.be.equal('middle');
            });
        });

        describe('width', () => {
            it('Should match colWidth of x', () => {
                const tableOptions = { ...defaultTableOptions };
                tableOptions.colWidths = [5, 10, 15];

                let cell = new Cell('');
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.width).to.be.equal(5);

                cell = new Cell('');
                cell.x = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.width).to.be.equal(10);

                cell = new Cell('');
                cell.x = 2;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.width).to.be.equal(15);
            });

            it('Should add colWidths if colSpan > 1', () => {
                const tableOptions = { ...defaultTableOptions };
                tableOptions.colWidths = [5, 10, 15];

                let cell = new Cell({ content: '', colSpan: 2 });
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.width).to.be.equal(16);

                cell = new Cell({ content: '', colSpan: 2 });
                cell.x = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.width).to.be.equal(26);

                cell = new Cell({ content: '', colSpan: 3 });
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.width).to.be.equal(32);
            });
        });

        describe('height', () => {
            it('Should match rowHeight of x', () => {
                const tableOptions = { ...defaultTableOptions };
                tableOptions.rowHeights = [5, 10, 15];

                let cell = new Cell('');
                cell.y = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.height).to.be.equal(5);

                cell = new Cell('');
                cell.y = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.height).to.be.equal(10);

                cell = new Cell('');
                cell.y = 2;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.height).to.be.equal(15);
            });

            it('Should add rowHeights if rowSpan > 1', () => {
                const tableOptions = { ...defaultTableOptions };
                tableOptions.rowHeights = [5, 10, 15];

                let cell = new Cell({ content: '', rowSpan: 2 });
                cell.y = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.height).to.be.equal(16);

                cell = new Cell({ content: '', rowSpan: 2 });
                cell.y = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.height).to.be.equal(26);

                cell = new Cell({ content: '', rowSpan: 3 });
                cell.y = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.height).to.be.equal(32);
            });
        });

        describe('drawRight', () => {
            let tableOptions: TableOptions;

            beforeEach(() => {
                tableOptions = { ...defaultTableOptions };
                tableOptions.colWidths = [20, 20, 20];
            });

            it('col 1 of 3, with default colspan', () => {
                const cell = new Cell('');
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.drawRight).to.be.equal(false);
            });

            it('col 2 of 3, with default colspan', () => {
                const cell = new Cell('');
                cell.x = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.drawRight).to.be.equal(false);
            });

            it('col 3 of 3, with default colspan', () => {
                const cell = new Cell('');
                cell.x = 2;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.drawRight).to.be.equal(true);
            });

            it('col 3 of 4, with default colspan', () => {
                const cell = new Cell('');
                cell.x = 2;
                tableOptions.colWidths = [20, 20, 20, 20];
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.drawRight).to.be.equal(false);
            });

            it('col 2 of 3, with colspan of 2', () => {
                const cell = new Cell({ content: '', colSpan: 2 });
                cell.x = 1;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.drawRight).to.be.equal(true);
            });

            it('col 1 of 3, with colspan of 3', () => {
                const cell = new Cell({ content: '', colSpan: 3 });
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.drawRight).to.be.equal(true);
            });

            it('col 1 of 3, with colspan of 2', () => {
                const cell = new Cell({ content: '', colSpan: 2 });
                cell.x = 0;
                cell.mergeTableOptions(tableOptions);
                cell.init(tableOptions);
                expect(cell.drawRight).to.be.equal(false);
            });
        });
    });

    describe('drawLine', () => {
        const crossCorner = new Cell({ content: '', head: true });
        crossCorner.x = crossCorner.y = 0;
        let cell: Cell;
        let style: TableStyle;

        beforeEach(() => {
            style = new TableStyle({
                verticalOutside: '║',
                verticalInside: '│',
                horizontalOutside: '═',
                horizontalInside: '─',
                topMid: '╤',
                topLeft: '╔',
                topRight: '╗',
                bottomMid: '╧',
                bottomLeft: '╚',
                bottomRight: '╝',
                midMid: '╪',
                midLeft: '╠',
                midRight: '╣',
                mid: '┼',
                left: '╟',
                right: '╢',
                crossIntersection: '╬',
                crossBottom: '╩',
                crossMid: '╫',
                truncate: '…',
                padding: ' ',
            });
            cell = new Cell({
                content: 'hello\nhowdy\ngoodnight',
                style,
                hAlign: 'center',
                vAlign: 'middle',
            });

            //manually init
            cell.width = 7;
            cell.height = 3;
            cell.lines = cell.content.split('\n');
            cell.x = cell.y = 0;
            crossCorner.cells = null;
        });

        describe('top line', () => {
            it('Should draw the top left corner when x=0,y=0', () => {
                cell.x = cell.y = 0;
                expect(cell.draw('top')).to.be.equal('╔═══════');
                cell.drawRight = true;
                expect(cell.draw('top')).to.be.equal('╔═══════╗');
            });

            it('Should not draw the top left corner if cross table', () => {
                cell.x = cell.y = 0;
                cell.head = true;
                cell.content = '';
                cell.cells = [
                    [cell, new Cell({ content: 'Test', head: true })],
                    [new Cell({ content: 'Test', head: true })],
                ];
                cell.widths = [7];
                expect(cell.draw('top')).to.be.equal('        ');
            });

            it('Should draw top left for 1,0 cell if cross table', () => {
                cell.x = 1;
                cell.y = 0;
                cell.head = true;
                cell.cells = [
                    [crossCorner, cell],
                    [
                        new Cell({ content: 'Test', head: true }),
                        new Cell('foobar'),
                    ],
                ];
                crossCorner.cells = cell.cells;
                cell.widths = [7];
                expect(cell.draw('top')).to.be.equal('╔═══════');
            });

            it('Should draw top left for 0,1 cell if cross table', () => {
                cell.x = 0;
                cell.y = 1;
                cell.head = true;
                cell.cells = [
                    [crossCorner, new Cell({ content: 'Test', head: true })],
                    [cell, new Cell('foobar')],
                ];
                crossCorner.cells = cell.cells;
                cell.widths = [7];
                expect(cell.draw('top')).to.be.equal('╔═══════');
            });

            it('Should draw the top mid corner when x=1,y=0', () => {
                cell.x = 1;
                cell.y = 0;
                expect(cell.draw('top')).to.be.equal('╤═══════');
                cell.drawRight = true;
                expect(cell.draw('top')).to.be.equal('╤═══════╗');
            });

            it('Should draw the left mid corner when x=0,y=1', () => {
                cell.x = 0;
                cell.y = 1;
                expect(cell.draw('top')).to.be.equal('╟───────');
                cell.drawRight = true;
                expect(cell.draw('top')).to.be.equal('╟───────╢');
            });

            it('Should draw the mid mid corner when x=1,y=1', () => {
                cell.x = 1;
                cell.y = 1;
                expect(cell.draw('top')).to.be.equal('┼───────');
                cell.drawRight = true;
                expect(cell.draw('top')).to.be.equal('┼───────╢');
            });

            it('Should draw the top cell under an header', () => {
                const head = new Cell({ content: 'Test', head: true });
                cell.cells = [
                    [head, head, head],
                    [cell, cell, cell],
                ];
                cell.x = 0;
                cell.y = 1;
                cell.widths = [7];
                expect(cell.draw('top')).to.be.equal('╠═══════');
                cell.drawRight = true;
                expect(cell.draw('top')).to.be.equal('╠═══════╣');
                cell.x = 1;
                cell.y = 1;
                cell.drawRight = false;
                expect(cell.draw('top')).to.be.equal('╪═══════');
                cell.drawRight = true;
                expect(cell.draw('top')).to.be.equal('╪═══════╣');
            });

            it('Should draw inside border under a row header for a row header cell', () => {
                const head = new Cell({ content: 'Test', head: true });
                cell.cells = [
                    [crossCorner, head, head],
                    [head, new Cell('foobar'), new Cell('foobar')],
                    [cell, new Cell('foobar'), new Cell('foobar')],
                ];
                cell.x = 0;
                cell.y = 2;
                cell.head = true;
                crossCorner.cells = cell.cells;
                cell.widths = [7];
                expect(cell.draw('top')).to.be.equal('╟───────');
            });

            it('Should draw cross intersection for cross table', () => {
                const head = new Cell({ content: 'Test', head: true });
                cell.cells = [
                    [crossCorner, head, head],
                    [head, cell, new Cell('foobar')],
                    [head, new Cell('foobar'), new Cell('foobar')],
                ];
                cell.x = 1;
                cell.y = 1;
                crossCorner.cells = cell.cells;
                cell.widths = [7];
                expect(cell.draw('top')).to.be.equal('╬═══════');
            });

            it('Should draw horizontal header with tbody', () => {
                const head = new Cell({ content: 'Test', head: true });
                cell.cells = [
                    [crossCorner, head, head],
                    [head, new Cell('foobar'), new Cell('foobar')],
                    [head, cell, new Cell('foobar')],
                ];
                cell.x = 1;
                cell.y = 2;
                crossCorner.cells = cell.cells;
                cell.widths = [7];
                expect(cell.draw('top')).to.be.equal('╫───────');
            });

            it('Should draw in the color specified by border style', () => {
                style.defineStyles({
                    head: undefined,
                    cell: undefined,
                    borderOutside: 'grey',
                    borderInside: 'blue',
                });
                Object.getPrototypeOf(cell)._setStyle.call(cell, style);
                expect(cell.draw('top')).to.be.equal(
                    '<grey>╔</><grey>═══════</>'
                );
                cell.x = cell.y = 1;
                cell.drawRight = true;
                expect(cell.draw('top')).to.be.equal(
                    '<blue>┼</><blue>───────</><grey>╢</>'
                );
            });
        });

        describe('bottom line', () => {
            it('Should draw the bottom left corner if x=0', () => {
                cell.x = 0;
                cell.y = 1;
                expect(cell.draw('bottom')).to.be.equal('╚═══════');
                cell.drawRight = true;
                expect(cell.draw('bottom')).to.be.equal('╚═══════╝');
            });

            it('Should draw the bottom left corner if x=1', () => {
                cell.x = 1;
                cell.y = 1;
                expect(cell.draw('bottom')).to.be.equal('╧═══════');
                cell.drawRight = true;
                expect(cell.draw('bottom')).to.be.equal('╧═══════╝');
            });

            it('Should draw the bottom left corner after horizontal header', () => {
                const head = new Cell({ content: 'Test', head: true });
                cell.cells = [
                    [crossCorner, head, head],
                    [head, new Cell('foobar'), new Cell('foobar')],
                    [head, cell, new Cell('foobar')],
                ];
                cell.x = 1;
                cell.y = 1;
                crossCorner.cells = cell.cells;
                cell.widths = [7];
                expect(cell.draw('bottom')).to.be.equal('╩═══════');
            });

            it('Should draw in the color specified by border style', () => {
                style.defineStyles({
                    head: undefined,
                    cell: undefined,
                    borderOutside: 'grey',
                    borderInside: 'blue',
                });
                Object.getPrototypeOf(cell)._setStyle.call(cell, style);
                expect(cell.draw('bottom')).to.be.equal('<grey>╚═══════</>');
            });
        });

        describe('drawEmpty', () => {
            it('Should draw an empty line', () => {
                expect(cell.drawEmpty()).to.be.equal('║       ');
                expect(cell.drawEmpty(true)).to.be.equal('║       ║');
            });

            it('Should draw an empty line', () => {
                style.defineStyles({
                    head: undefined,
                    cell: 'red',
                    borderOutside: 'grey',
                    borderInside: 'blue',
                });
                Object.getPrototypeOf(cell)._setStyle.call(cell, style);
                expect(cell.drawEmpty()).to.be.equal(
                    '<grey>║</><red>       </>'
                );
                expect(cell.drawEmpty(true)).to.be.equal(
                    '<grey>║</><red>       </><grey>║</>'
                );
            });

            it('Should not draw an empty line for the corner if cross table', () => {
                cell.x = cell.y = 0;
                cell.head = true;
                cell.content = '';
                cell.cells = [
                    [cell, new Cell({ content: 'Test', head: true })],
                    [new Cell({ content: 'Test', head: true })],
                ];
                expect(cell.drawEmpty()).to.be.equal('        ');
            });

            it('Should out border if after horizontal header', () => {
                cell.x = 1;
                cell.cells = [
                    [new Cell({ content: 'Test', head: true }), cell],
                    [new Cell('foobar')],
                ];
                expect(cell.drawEmpty()).to.be.equal('║       ');
            });
        });

        describe('first line of text', () => {
            beforeEach(() => {
                cell.width = 9;
            });

            it('Should draw left side if x=0', () => {
                cell.x = 0;
                expect(cell.draw(0)).to.be.equal('║  hello  ');
                cell.drawRight = true;
                expect(cell.draw(0)).to.be.equal('║  hello  ║');
            });

            it('Should draw mid side if x=1', () => {
                cell.x = 1;
                expect(cell.draw(0)).to.be.equal('│  hello  ');
                cell.drawRight = true;
                expect(cell.draw(0)).to.be.equal('│  hello  ║');
            });

            it('Should out border if after horizontal header', () => {
                cell.x = 1;
                cell.cells = [
                    [new Cell({ content: 'Test', head: true }), cell],
                    [new Cell('foobar')],
                ];
                expect(cell.draw(0)).to.be.equal('║  hello  ');
            });

            it('Should not draw an empty line for the corner if cross table', () => {
                cell.x = cell.y = 0;
                cell.head = true;
                cell.content = '';
                cell.cells = [
                    [cell, new Cell({ content: 'Test', head: true })],
                    [new Cell({ content: 'Test', head: true })],
                ];
                expect(cell.draw(0)).to.be.equal('          ');
            });

            it('Should align left', () => {
                cell.x = 1;
                cell.hAlign = 'left';
                expect(cell.draw(0)).to.be.equal('│ hello   ');
                cell.drawRight = true;
                expect(cell.draw(0)).to.be.equal('│ hello   ║');
            });

            it('Should align right', () => {
                cell.x = 1;
                cell.hAlign = 'right';
                expect(cell.draw(0)).to.be.equal('│   hello ');
                cell.drawRight = true;
                expect(cell.draw(0)).to.be.equal('│   hello ║');
            });

            it('Should draw left and right in color of border style', () => {
                style.defineStyles({
                    head: undefined,
                    cell: undefined,
                    borderOutside: 'grey',
                    borderInside: 'blue',
                });
                Object.getPrototypeOf(cell)._setStyle.call(cell, style);
                cell.x = 0;
                expect(cell.draw(0)).to.be.equal('<grey>║</>  hello  ');
                cell.drawRight = true;
                expect(cell.draw(0)).to.be.equal(
                    '<grey>║</>  hello  <grey>║</>'
                );
            });

            it('Should draw text in color of head style if y == 0', () => {
                style.defineStyles({
                    head: undefined,
                    cell: 'red',
                    borderOutside: undefined,
                    borderInside: undefined,
                });
                Object.getPrototypeOf(cell)._setStyle.call(cell, style);
                cell.x = cell.y = 0;
                expect(cell.draw(0)).to.be.equal('║<red>  hello  </>');
                cell.drawRight = true;
                expect(cell.draw(0)).to.be.equal('║<red>  hello  </>║');
            });

            it('Should not draw text in color of head style if y == 1', () => {
                style.defineStyles({
                    head: 'red',
                    cell: undefined,
                    borderOutside: undefined,
                    borderInside: undefined,
                });
                Object.getPrototypeOf(cell)._setStyle.call(cell, style);
                cell.x = cell.y = 1;
                expect(cell.draw(0)).to.be.equal('│  hello  ');
                cell.drawRight = true;
                expect(cell.draw(0)).to.be.equal('│  hello  ║');
            });

            it('Should draw head and border colors together', () => {
                style.defineStyles({
                    head: 'red',
                    cell: undefined,
                    borderOutside: 'grey',
                    borderInside: 'blue',
                });
                cell.head = true;
                Object.getPrototypeOf(cell)._setStyle.call(cell, style);
                cell.x = cell.y = 0;
                expect(cell.draw(0)).to.be.equal('<grey>║</><red>  hello  </>');
                cell.drawRight = true;
                expect(cell.draw(0)).to.be.equal(
                    '<grey>║</><red>  hello  </><grey>║</>'
                );
            });
        });

        describe('second line of text', () => {
            beforeEach(() => {
                cell.width = 9;
            });

            it('Should draw left side if x=0', () => {
                cell.x = 0;
                expect(cell.draw(1)).to.be.equal('║  howdy  ');
                cell.drawRight = true;
                expect(cell.draw(1)).to.be.equal('║  howdy  ║');
            });

            it('Should draw mid side if x=1', () => {
                cell.x = 1;
                expect(cell.draw(1)).to.be.equal('│  howdy  ');
                cell.drawRight = true;
                expect(cell.draw(1)).to.be.equal('│  howdy  ║');
            });

            it('Should align left', () => {
                cell.x = 1;
                cell.hAlign = 'left';
                expect(cell.draw(1)).to.be.equal('│ howdy   ');
                cell.drawRight = true;
                expect(cell.draw(1)).to.be.equal('│ howdy   ║');
            });

            it('Should align right', () => {
                cell.x = 1;
                cell.hAlign = 'right';
                expect(cell.draw(1)).to.be.equal('│   howdy ');
                cell.drawRight = true;
                expect(cell.draw(1)).to.be.equal('│   howdy ║');
            });
        });

        describe('truncated line of text', () => {
            beforeEach(() => {
                cell.width = 9;
            });

            it('Should draw left side if x=0', () => {
                cell.x = 0;
                expect(cell.draw(2)).to.be.equal('║ goodni… ');
                cell.drawRight = true;
                expect(cell.draw(2)).to.be.equal('║ goodni… ║');
            });

            it('Should draw mid side if x=1', () => {
                cell.x = 1;
                expect(cell.draw(2)).to.be.equal('│ goodni… ');
                cell.drawRight = true;
                expect(cell.draw(2)).to.be.equal('│ goodni… ║');
            });

            it('Should not change when aligned left', () => {
                cell.x = 1;
                cell.hAlign = 'left';
                expect(cell.draw(2)).to.be.equal('│ goodni… ');
                cell.drawRight = true;
                expect(cell.draw(2)).to.be.equal('│ goodni… ║');
            });

            it('Should not change when aligned right', () => {
                cell.x = 1;
                cell.hAlign = 'right';
                expect(cell.draw(2)).to.be.equal('│ goodni… ');
                cell.drawRight = true;
                expect(cell.draw(2)).to.be.equal('│ goodni… ║');
            });
        });

        describe('vAlign', () => {
            beforeEach(() => {
                cell.height = 5;
            });

            it('middle', () => {
                cell.vAlign = 'middle';
                expect(cell.draw(0)).to.be.equal('║       ');
                expect(cell.draw(1)).to.be.equal('║ hello ');
                expect(cell.draw(2)).to.be.equal('║ howdy ');
                expect(cell.draw(3)).to.be.equal('║ good… ');
                expect(cell.draw(4)).to.be.equal('║       ');

                cell.drawRight = true;
                expect(cell.draw(0)).to.be.equal('║       ║');
                expect(cell.draw(1)).to.be.equal('║ hello ║');
                expect(cell.draw(2)).to.be.equal('║ howdy ║');
                expect(cell.draw(3)).to.be.equal('║ good… ║');
                expect(cell.draw(4)).to.be.equal('║       ║');

                cell.x = 1;
                cell.drawRight = false;
                expect(cell.draw(0)).to.be.equal('│       ');
                expect(cell.draw(1)).to.be.equal('│ hello ');
                expect(cell.draw(2)).to.be.equal('│ howdy ');
                expect(cell.draw(3)).to.be.equal('│ good… ');
                expect(cell.draw(4)).to.be.equal('│       ');
            });

            it('top', () => {
                cell.vAlign = 'top';
                expect(cell.draw(0)).to.be.equal('║ hello ');
                expect(cell.draw(1)).to.be.equal('║ howdy ');
                expect(cell.draw(2)).to.be.equal('║ good… ');
                expect(cell.draw(3)).to.be.equal('║       ');
                expect(cell.draw(4)).to.be.equal('║       ');

                cell.vAlign = null; //top is the default
                cell.drawRight = true;
                expect(cell.draw(0)).to.be.equal('║ hello ║');
                expect(cell.draw(1)).to.be.equal('║ howdy ║');
                expect(cell.draw(2)).to.be.equal('║ good… ║');
                expect(cell.draw(3)).to.be.equal('║       ║');
                expect(cell.draw(4)).to.be.equal('║       ║');

                cell.x = 1;
                cell.drawRight = false;
                expect(cell.draw(0)).to.be.equal('│ hello ');
                expect(cell.draw(1)).to.be.equal('│ howdy ');
                expect(cell.draw(2)).to.be.equal('│ good… ');
                expect(cell.draw(3)).to.be.equal('│       ');
                expect(cell.draw(4)).to.be.equal('│       ');
            });

            it('bottom', () => {
                cell.vAlign = 'bottom';
                expect(cell.draw(0)).to.be.equal('║       ');
                expect(cell.draw(1)).to.be.equal('║       ');
                expect(cell.draw(2)).to.be.equal('║ hello ');
                expect(cell.draw(3)).to.be.equal('║ howdy ');
                expect(cell.draw(4)).to.be.equal('║ good… ');

                cell.drawRight = true;
                expect(cell.draw(0)).to.be.equal('║       ║');
                expect(cell.draw(1)).to.be.equal('║       ║');
                expect(cell.draw(2)).to.be.equal('║ hello ║');
                expect(cell.draw(3)).to.be.equal('║ howdy ║');
                expect(cell.draw(4)).to.be.equal('║ good… ║');

                cell.x = 1;
                cell.drawRight = false;
                expect(cell.draw(0)).to.be.equal('│       ');
                expect(cell.draw(1)).to.be.equal('│       ');
                expect(cell.draw(2)).to.be.equal('│ hello ');
                expect(cell.draw(3)).to.be.equal('│ howdy ');
                expect(cell.draw(4)).to.be.equal('│ good… ');
            });
        });

        it('Should show truncation for vertically truncated on last visible line', () => {
            cell.height = 2;
            expect(cell.draw(0)).to.be.equal('║ hello ');
            expect(cell.draw(1)).to.be.equal('║ howd… ');
        });

        it('Should not vertically truncate if the lines just fit', () => {
            cell.height = 2;
            cell.content = 'hello\nhowdy';
            cell.lines = cell.content.split('\n');
            expect(cell.draw(0)).to.be.equal('║ hello ');
            expect(cell.draw(1)).to.be.equal('║ howdy ');
        });

        it('Should vertically truncate even if last line is short', () => {
            cell.height = 2;
            cell.content = 'hello\nhi\nhowdy';
            cell.lines = cell.content.split('\n');
            expect(cell.draw(0)).to.be.equal('║ hello ');
            expect(cell.draw(1)).to.be.equal('║  hi…  ');
        });

        it('Should allow custom truncation', () => {
            style.defineChars({
                verticalOutside: '║',
                verticalInside: '│',
                horizontalOutside: '═',
                horizontalInside: '─',
                topMid: '╤',
                topLeft: '╔',
                topRight: '╗',
                bottomMid: '╧',
                bottomLeft: '╚',
                bottomRight: '╝',
                midMid: '╪',
                midLeft: '╠',
                midRight: '╣',
                mid: '┼',
                left: '╟',
                right: '╢',
                crossIntersection: '╬',
                crossBottom: '╩',
                crossMid: '╫',
                truncate: '...',
                padding: ' ',
            });
            Object.getPrototypeOf(cell)._setStyle.call(cell, style);
            cell.height = 2;
            cell.content = 'hello\nhi\nhowdy';
            cell.lines = cell.content.split('\n');
            expect(cell.draw(0)).to.be.equal('║ hello ');
            expect(cell.draw(1)).to.be.equal('║ hi... ');

            cell.content = 'hello\nhowdy\nhi';
            cell.lines = cell.content.split('\n');
            expect(cell.draw(0)).to.be.equal('║ hello ');
            expect(cell.draw(1)).to.be.equal('║ ho... ');
        });
    });
});
