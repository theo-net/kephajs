/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/*
 * Inspired by `cli-table2` James Talmage <james.talmage@jrtechnical.com>
 */

import { Cell } from './Cell';
import { TableOptions } from './Table';

export class RowSpanCell {
    public colSpan = 0;

    public x: number | null = 0;

    public y: number | null = 0;

    public head = false;

    public cellOffset = 0;

    public offset = 0;

    constructor(public originalCell: Cell) {}

    init(tableOptions: TableOptions) {
        const originalY = this.originalCell.y;
        this.cellOffset = (this.y as number) - (originalY as number);

        this.offset = tableOptions.rowHeights[originalY as number];
        for (let i = 1; i < this.cellOffset; i++)
            this.offset +=
                1 + tableOptions.rowHeights[(originalY as number) + i];
    }

    mergeTableOptions() {}

    draw(lineNum: number | string) {
        if (lineNum == 'top')
            return this.originalCell.draw(this.offset, this.cellOffset);

        if (lineNum == 'bottom') return this.originalCell.draw('bottom');

        return this.originalCell.draw(this.offset + 1 + (lineNum as number));
    }
}
