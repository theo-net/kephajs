/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export class ColSpanCell {
    public x: number | null = 0;

    public y: number | null = 0;

    public head = false;

    draw() {
        return '';
    }

    init() {
        return;
    }

    mergeTableOptions() {
        return;
    }
}
