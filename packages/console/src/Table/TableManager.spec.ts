/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { TableManager } from './TableManager';
import { TableStyle } from './TableStyle';
import { UnknownStyleError } from './Error/UnknownStyleError';

describe('@kephajs/console/Table/TableManager', () => {
    describe('constructor', () => {
        it('Should define default style', () => {
            const style = new TableManager().getStyle('default');
            expect(style).to.be.deep.equal(new TableStyle());
        });
    });

    describe('addStyle and getStyle', () => {
        it('Should add a style', () => {
            const tableManager = new TableManager();
            const style = new TableStyle();
            tableManager.addStyle('foobar', style);
            expect(tableManager.getStyle('foobar')).to.be.equal(style);
        });

        it('Should replace a style', () => {
            const tableManager = new TableManager();
            const style = new TableStyle();
            tableManager.addStyle('default', style);
            expect(tableManager.getStyle('default')).to.be.equal(style);
        });

        it('Should thrown UnknownStyleError if style undefined', () => {
            try {
                new TableManager().getStyle('foobar');
            } catch (error) {
                expect(error).to.be.instanceOf(UnknownStyleError);
                expect((error as UnknownStyleError).message).to.be.equal(
                    'Unknown table style "foobar".'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should return the default style if getStyle without parameter', () => {
            const tableManager = new TableManager();
            expect(tableManager.getStyle()).to.be.equal(
                Object.getOwnPropertyDescriptor(tableManager, '_styles')?.value
                    ?.default
            );
        });

        it('Should return the style if getStyle with TableStyle instance parameter', () => {
            const style = new TableStyle();
            expect(new TableManager().getStyle(style)).to.be.equal(style);
        });
    });

    describe('registered styles', () => {
        it('Should have default style', () => {
            const tm = new TableManager();
            expect(tm.getStyle('default')).to.be.instanceOf(TableStyle);
            let table = tm.table('default', {
                head: ['a', 'b'],
                compact: true,
            });
            table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '+---+---+',
                '| a | b |',
                '+---+---+',
                '| c | d |',
                '| e | f |',
                '+---+---+',
            ]);
            table = tm.table('default', {
                head: ['', 'a', 'b'],
                compact: true,
            });
            table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '    +---+---+',
                '    | a | b |',
                '+---+---+---+',
                '| c | d | e |',
                '| f | g | h |',
                '+---+---+---+',
            ]);
            table = tm.table('default', { head: ['a', 'b'] });
            table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '+---+---+',
                '| a | b |',
                '+---+---+',
                '| c | d |',
                '+---+---+',
                '| e | f |',
                '+---+---+',
            ]);
            table = tm.table('default', { head: ['', 'a', 'b'] });
            table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '    +---+---+',
                '    | a | b |',
                '+---+---+---+',
                '| c | d | e |',
                '+---+---+---+',
                '| f | g | h |',
                '+---+---+---+',
            ]);
        });

        it('Should have compact style', () => {
            const tm = new TableManager();
            expect(tm.getStyle('compact')).to.be.instanceOf(TableStyle);
            let table = tm.table('compact', {
                head: ['a', 'b'],
                compact: true,
            });
            /*table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                'a b',
                'c d',
                'e f',
            ]);*/
            table = tm.table('compact', {
                head: ['', 'a', 'b'],
                compact: true,
            });
            table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '  a b',
                'c d e',
                'f g h',
            ]);
            table = tm.table('compact', { head: ['a', 'b'] });
            table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                'a b',
                'c d',
                'e f',
            ]);
            table = tm.table('compact', { head: ['', 'a', 'b'] });
            table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '  a b',
                'c d e',
                'f g h',
            ]);
        });

        it('Should have borderless style', () => {
            const tm = new TableManager();
            expect(tm.getStyle('borderless')).to.be.instanceOf(TableStyle);
            let table = tm.table('borderless', {
                head: ['aa', 'bb'],
                compact: true,
            });
            table.push(['cc', 'dd'], ['ee', 'ff']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '== ==',
                'aa bb',
                '== ==',
                'cc dd',
                'ee ff',
                '== ==',
            ]);
            table = tm.table('borderless', {
                head: ['', 'aa', 'bb'],
                compact: true,
            });
            table.push({ cc: ['dd', 'ee'] }, { ff: ['gg', 'hh'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '   == ==',
                '   aa bb',
                '== == ==',
                'cc dd ee',
                'ff gg hh',
                '== == ==',
            ]);
            table = tm.table('borderless', { head: ['aa', 'bb'] });
            table.push(['cc', 'dd'], ['ee', 'ff']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '== ==',
                'aa bb',
                '== ==',
                'cc dd',
                '-- --',
                'ee ff',
                '== ==',
            ]);
            table = tm.table('borderless', { head: ['', 'aa', 'bb'] });
            table.push({ cc: ['dd', 'ee'] }, { ff: ['gg', 'hh'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '   == ==',
                '   aa bb',
                '== == ==',
                'cc dd ee',
                '-- -- --',
                'ff gg hh',
                '== == ==',
            ]);
        });

        it('Should have box style', () => {
            const tm = new TableManager();
            expect(tm.getStyle('box')).to.be.instanceOf(TableStyle);
            let table = tm.table('box', {
                head: ['a', 'b'],
                compact: true,
            });
            table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '┌───┬───┐',
                '│ a │ b │',
                '├───┼───┤',
                '│ c │ d │',
                '│ e │ f │',
                '└───┴───┘',
            ]);
            table = tm.table('box', {
                head: ['', 'a', 'b'],
                compact: true,
            });
            table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '    ┌───┬───┐',
                '    │ a │ b │',
                '┌───┼───┼───┤',
                '│ c │ d │ e │',
                '│ f │ g │ h │',
                '└───┴───┴───┘',
            ]);
            table = tm.table('box', { head: ['a', 'b'] });
            table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '┌───┬───┐',
                '│ a │ b │',
                '├───┼───┤',
                '│ c │ d │',
                '├╌╌╌┼╌╌╌┤',
                '│ e │ f │',
                '└───┴───┘',
            ]);
            table = tm.table('box', { head: ['', 'a', 'b'] });
            table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '    ┌───┬───┐',
                '    │ a │ b │',
                '┌───┼───┼───┤',
                '│ c │ d │ e │',
                '├╌╌╌┼╌╌╌┼╌╌╌┤',
                '│ f │ g │ h │',
                '└───┴───┴───┘',
            ]);
        });

        it('Should have dashed style', () => {
            const tm = new TableManager();
            expect(tm.getStyle('dashed')).to.be.instanceOf(TableStyle);
            let table = tm.table('dashed', {
                head: ['a', 'b'],
                compact: true,
            });
            table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '╭╌╌╌┬╌╌╌╮',
                '╎ a ╎ b ╎',
                '├╌╌╌┼╌╌╌┤',
                '╎ c ╎ d ╎',
                '╎ e ╎ f ╎',
                '╰╌╌╌┴╌╌╌╯',
            ]);
            table = tm.table('dashed', {
                head: ['', 'a', 'b'],
                compact: true,
            });
            table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '    ╭╌╌╌┬╌╌╌╮',
                '    ╎ a ╎ b ╎',
                '╭╌╌╌┼╌╌╌┼╌╌╌┤',
                '╎ c ╎ d ╎ e ╎',
                '╎ f ╎ g ╎ h ╎',
                '╰╌╌╌┴╌╌╌┴╌╌╌╯',
            ]);
            table = tm.table('dashed', { head: ['a', 'b'] });
            table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '╭╌╌╌┬╌╌╌╮',
                '╎ a ╎ b ╎',
                '├╌╌╌┼╌╌╌┤',
                '╎ c ╎ d ╎',
                '├╌╌╌┼╌╌╌┤',
                '╎ e ╎ f ╎',
                '╰╌╌╌┴╌╌╌╯',
            ]);
            table = tm.table('dashed', { head: ['', 'a', 'b'] });
            table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '    ╭╌╌╌┬╌╌╌╮',
                '    ╎ a ╎ b ╎',
                '╭╌╌╌┼╌╌╌┼╌╌╌┤',
                '╎ c ╎ d ╎ e ╎',
                '├╌╌╌┼╌╌╌┼╌╌╌┤',
                '╎ f ╎ g ╎ h ╎',
                '╰╌╌╌┴╌╌╌┴╌╌╌╯',
            ]);
        });

        it('Should have box-heavy style', () => {
            const tm = new TableManager();
            expect(tm.getStyle('box-heavy')).to.be.instanceOf(TableStyle);
            let table = tm.table('box-heavy', {
                head: ['a', 'b'],
                compact: true,
            });
            table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '┏━━━┯━━━┓',
                '┃ a │ b ┃',
                '┣━━━┿━━━┫',
                '┃ c │ d ┃',
                '┃ e │ f ┃',
                '┗━━━┷━━━┛',
            ]);
            table = tm.table('box-heavy', {
                head: ['', 'a', 'b'],
                compact: true,
            });
            table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '    ┏━━━┯━━━┓',
                '    ┃ a │ b ┃',
                '┏━━━╋━━━┿━━━┫',
                '┃ c ┃ d │ e ┃',
                '┃ f ┃ g │ h ┃',
                '┗━━━┻━━━┷━━━┛',
            ]);
            table = tm.table('box-heavy', { head: ['a', 'b'] });
            table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '┏━━━┯━━━┓',
                '┃ a │ b ┃',
                '┣━━━┿━━━┫',
                '┃ c │ d ┃',
                '┠───┼───┨',
                '┃ e │ f ┃',
                '┗━━━┷━━━┛',
            ]);
            table = tm.table('box-heavy', { head: ['', 'a', 'b'] });
            table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '    ┏━━━┯━━━┓',
                '    ┃ a │ b ┃',
                '┏━━━╋━━━┿━━━┫',
                '┃ c ┃ d │ e ┃',
                '┠───╂───┼───┨',
                '┃ f ┃ g │ h ┃',
                '┗━━━┻━━━┷━━━┛',
            ]);
        });

        it('Should have box-double style', () => {
            const tm = new TableManager();
            expect(tm.getStyle('box-double')).to.be.instanceOf(TableStyle);
            let table = tm.table('box-double', {
                head: ['a', 'b'],
                compact: true,
            });
            table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '╔═══╤═══╗',
                '║ a │ b ║',
                '╠═══╪═══╣',
                '║ c │ d ║',
                '║ e │ f ║',
                '╚═══╧═══╝',
            ]);
            table = tm.table('box-double', {
                head: ['', 'a', 'b'],
                compact: true,
            });
            table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '    ╔═══╤═══╗',
                '    ║ a │ b ║',
                '╔═══╬═══╪═══╣',
                '║ c ║ d │ e ║',
                '║ f ║ g │ h ║',
                '╚═══╩═══╧═══╝',
            ]);
            table = tm.table('box-double', { head: ['a', 'b'] });
            table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '╔═══╤═══╗',
                '║ a │ b ║',
                '╠═══╪═══╣',
                '║ c │ d ║',
                '╟───┼───╢',
                '║ e │ f ║',
                '╚═══╧═══╝',
            ]);
            table = tm.table('box-double', { head: ['', 'a', 'b'] });
            table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '    ╔═══╤═══╗',
                '    ║ a │ b ║',
                '╔═══╬═══╪═══╣',
                '║ c ║ d │ e ║',
                '╟───╫───┼───╢',
                '║ f ║ g │ h ║',
                '╚═══╩═══╧═══╝',
            ]);
        });

        it('Should have rounded style', () => {
            const tm = new TableManager();
            expect(tm.getStyle('rounded')).to.be.instanceOf(TableStyle);
            let table = tm.table('rounded', {
                head: ['a', 'b'],
                compact: true,
            });
            table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '╭───┬───╮',
                '│ a │ b │',
                '├───┼───┤',
                '│ c │ d │',
                '│ e │ f │',
                '╰───┴───╯',
            ]);
            table = tm.table('rounded', {
                head: ['', 'a', 'b'],
                compact: true,
            });
            table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '    ╭───┬───╮',
                '    │ a │ b │',
                '╭───┼───┼───┤',
                '│ c │ d │ e │',
                '│ f │ g │ h │',
                '╰───┴───┴───╯',
            ]);
            table = tm.table('rounded', { head: ['a', 'b'] });
            table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '╭───┬───╮',
                '│ a │ b │',
                '├───┼───┤',
                '│ c │ d │',
                '├╌╌╌┼╌╌╌┤',
                '│ e │ f │',
                '╰───┴───╯',
            ]);
            table = tm.table('rounded', { head: ['', 'a', 'b'] });
            table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
            expect(table.render().split('\n')).to.be.deep.equal([
                '    ╭───┬───╮',
                '    │ a │ b │',
                '╭───┼───┼───┤',
                '│ c │ d │ e │',
                '├╌╌╌┼╌╌╌┼╌╌╌┤',
                '│ f │ g │ h │',
                '╰───┴───┴───╯',
            ]);
        });
    });

    describe('table', () => {
        it('Should return a new table with default style', () => {
            const tableManager = new TableManager();
            const defaultStyle = tableManager.getStyle('default');
            expect(tableManager.table().getOptions().style).to.be.equal(
                defaultStyle
            );
        });

        it('Should take the style defined by options', () => {
            const tableManager = new TableManager();
            const style = new TableStyle();
            expect(
                tableManager.table(undefined, { style }).getOptions().style
            ).to.be.equal(style);
            expect(
                tableManager.table('default', { style }).getOptions().style
            ).to.be.equal(style);
        });

        it('Should take the style defined by style', () => {
            const tableManager = new TableManager();
            const style = new TableStyle();
            tableManager.addStyle('foobar', style);
            expect(tableManager.table('foobar').getOptions().style).to.be.equal(
                style
            );
        });
    });
});
