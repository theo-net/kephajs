/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { ColSpanCell } from './ColSpanCell';

describe('@kephajs/console/Table/ColSpanCell', () => {
    it('Should have an init function', () => {
        expect(new ColSpanCell()).to.respondTo('init');
        new ColSpanCell().init(); // nothing happens.
    });

    it('Should draw an empty string', () => {
        expect(new ColSpanCell().draw()).to.equal('');
    });
});
