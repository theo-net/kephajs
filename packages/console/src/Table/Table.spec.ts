/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import Sinon from 'sinon';
import { NullOutput } from '../Output/NullOutput';

import { UnknownStyleError } from './Error/UnknownStyleError';
import { defaultTableOptions, Table } from './Table';
import { TableManager } from './TableManager';
import { TableSeparator } from './TableSeparator';
import { TableStyle } from './TableStyle';

describe('@kephajs/console/Table/Table', () => {
    describe('constructor', () => {
        it('Should extend an array', () => {
            expect(Object.getPrototypeOf(Table)).to.equal(Array);
        });

        it('Should set default options', () => {
            expect(new Table().getOptions()).to.be.deep.equal(
                defaultTableOptions
            );
        });

        it('Should merge with default options', () => {
            const style = new TableStyle();
            expect(new Table({ style }).getOptions()).to.be.deep.equal({
                ...defaultTableOptions,
                style,
            });
        });

        it('Should replace style name by TableStyle instance', () => {
            const tm = new TableManager();
            expect(
                new Table({ style: 'borderless' }, tm).getOptions().style
            ).to.be.equal(tm.getStyle('borderless'));
        });

        it('Should throw error if style name but not TableManager', () => {
            const tm = new TableManager();
            try {
                new Table({ style: 'foobar', head: ['test'] }, tm);
            } catch (error) {
                expect(error).to.be.instanceOf(UnknownStyleError);
                expect((error as UnknownStyleError).message).to.be.equal(
                    'Unknown table style "foobar".'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    describe('toString', () => {
        it('Should be an alias for render without Output', () => {
            const table = new Table();
            const spy = Sinon.spy(table, 'render');
            table.push(['a']);
            const output = table.toString();
            expect(spy.getCalls()[0].args[0]).to.be.equal(undefined);
            expect(table.render()).to.be.equal(output);
        });
    });

    describe('render', () => {
        it('Should return the rendered string', () => {
            const output = new NullOutput();
            const table = new Table();
            table.push(['a']);
            expect(table.render()).to.be.equal('+---+\n| a |\n+---+');
            expect(table.render(output)).to.be.equal('+---+\n| a |\n+---+');
        });

        it('Should writeln on the Output', () => {
            const output = new NullOutput();
            const spy = Sinon.spy(output, 'writeln');
            const table = new Table();
            table.push(['a']);
            table.render(output);
            expect(spy.getCalls()[0].args[0]).to.be.equal(
                '+---+\n| a |\n+---+'
            );
        });

        it('Should return very basic usage', () => {
            const table = new Table({ head: ['a', 'b'], compact: true });
            table.push(['c', 'd'], ['e', 'f']);
            expect(table.render().split('\n')).to.be.deep.equal([
                '+---+---+',
                '| a | b |',
                '+---+---+',
                '| c | d |',
                '| e | f |',
                '+---+---+',
            ]);
        });

        it('Should wordWrap with colored text', () => {
            const table = new Table({
                wordWrap: true,
                colWidths: [7, 9],
            });
            table.push([
                '<red>Hello how are you?</red>',
                '<blue>I am fine thanks!</>',
            ]);

            const expected = [
                '+-------+---------+',
                '| <red>Hello</> | <blue>I am</>    |',
                '| <red>how</>   | <blue>fine</>    |',
                '| <red>are</>   | <blue>thanks!</> |',
                '| <red>you?</>  |         |',
                '+-------+---------+',
            ];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with CJK values', () => {
            const table = new Table({
                colWidths: [5, 10, 5],
            });

            table.push(
                ['foobar', 'English test', 'baz'],
                ['foobar', '中文测试', 'baz'],
                ['foobar', '日本語テスト', 'baz'],
                ['foobar', '한국어테스트', 'baz']
            );

            const expected = [
                '+-----+----------+-----+',
                '| fo… | English… | baz |',
                '+-----+----------+-----+',
                '| fo… | 中文测试 | baz |',
                '+-----+----------+-----+',
                '| fo… | 日本語…  | baz |',
                '+-----+----------+-----+',
                '| fo… | 한국어…  | baz |',
                '+-----+----------+-----+',
            ];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with complete table', () => {
            const table = new Table({
                head: ['Rel', 'Change', 'By', 'When'],
                colWidths: [6, 21, 25, 17],
            });

            table.push(
                [
                    'v0.1',
                    'Testing something cool',
                    'rauchg@gmail.com',
                    '7 minutes ago',
                ],
                [
                    'v0.1',
                    'Testing something cool',
                    'rauchg@gmail.com',
                    '8 minutes ago',
                ]
            );

            const expected = [
                '+------+---------------------+-------------------------+-----------------+',
                '| Rel  | Change              | By                      | When            |',
                '+------+---------------------+-------------------------+-----------------+',
                '| v0.1 | Testing something … | rauchg@gmail.com        | 7 minutes ago   |',
                '+------+---------------------+-------------------------+-----------------+',
                '| v0.1 | Testing something … | rauchg@gmail.com        | 8 minutes ago   |',
                '+------+---------------------+-------------------------+-----------------+',
            ];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with vertical table', () => {
            const style = new TableStyle();
            style.definePaddings({ left: 0, right: 0 });
            const table = new Table({ style });

            table.push(
                { 'v0.1': 'Testing something cool' },
                { 'v0.1': 'Testing something cool' }
            );

            const expected = [
                '+----+----------------------+',
                '|v0.1|Testing something cool|',
                '+----+----------------------+',
                '|v0.1|Testing something cool|',
                '+----+----------------------+',
            ];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with cross table', () => {
            const style = new TableStyle();
            style.definePaddings({ left: 0, right: 0 });
            const table = new Table({
                style,
                head: ['', 'Header 1', 'Header 2'],
            });

            table.push(
                { 'Header 3': ['v0.1', 'Testing something cool'] },
                { 'Header 4': ['v0.1', 'Testing something cool'] }
            );

            const expected = [
                '         +--------+----------------------+',
                '         |Header 1|Header 2              |',
                '+--------+--------+----------------------+',
                '|Header 3|v0.1    |Testing something cool|',
                '+--------+--------+----------------------+',
                '|Header 4|v0.1    |Testing something cool|',
                '+--------+--------+----------------------+',
            ];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with styles', () => {
            const style = new TableStyle();
            style.defineStyles({
                head: 'red',
                cell: 'green',
                borderInside: 'grey',
                borderOutside: 'blue',
            });

            let table = new Table({
                head: ['Rel', 'What'],
                style,
            });
            table.push(
                ['v0.1', 'A first version!'],
                ['v0.2', 'A second version!']
            );
            let expected = [
                '<blue>+</><blue>------</><blue>+</><blue>-------------------</><blue>+</>',
                '<blue>|</><red> Rel  </><grey>|</><red> What              </><blue>|</>',
                '<blue>+</><blue>------</><blue>+</><blue>-------------------</><blue>+</>',
                '<blue>|</><green> v0.1 </><grey>|</><green> A first version!  </><blue>|</>',
                '<blue>+</><grey>------</><grey>+</><grey>-------------------</><blue>+</>',
                '<blue>|</><green> v0.2 </><grey>|</><green> A second version! </><blue>|</>',
                '<blue>+------</><blue>+-------------------+</>',
            ];
            expect(table.render()).to.equal(expected.join('\n'));

            table = new Table({
                head: ['', 'Rel', 'What'],
                style,
            });
            table.push(
                { a: ['v0.1', 'A first version!'] },
                { b: ['v0.2', 'A second version!'] }
            );
            expected = [
                '    <blue>+</><blue>------</><blue>+</><blue>-------------------</><blue>+</>',
                '    <blue>|</><red> Rel  </><grey>|</><red> What              </><blue>|</>',
                '<blue>+</><blue>---</><blue>+</><blue>------</><blue>+</><blue>-------------------</><blue>+</>',
                '<blue>|</><red> a </><blue>|</><green> v0.1 </><grey>|</><green> A first version!  </><blue>|</>',
                '<blue>+</><grey>---</><blue>+</><grey>------</><grey>+</><grey>-------------------</><blue>+</>',
                '<blue>|</><red> b </><blue>|</><green> v0.2 </><grey>|</><green> A second version! </><blue>|</>',
                '<blue>+---</><blue>+------</><blue>+-------------------+</>',
            ];
            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with custom chars', () => {
            const table = new Table(
                {
                    style: 'box-double',
                },
                new TableManager()
            );

            table.push(['foo', 'bar', 'baz'], ['frob', 'bar', 'quuz']);

            const expected = [
                '╔══════╤═════╤══════╗',
                '║ foo  │ bar │ baz  ║',
                '╟──────┼─────┼──────╢',
                '║ frob │ bar │ quuz ║',
                '╚══════╧═════╧══════╝',
            ];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with compact shortand', () => {
            const table = new Table({ compact: true });

            table.push(['foo', 'bar', 'baz'], ['frob', 'bar', 'quuz']);

            const expected = [
                '+------+-----+------+',
                '| foo  | bar | baz  |',
                '| frob | bar | quuz |',
                '+------+-----+------+',
            ];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with compact value of the style', () => {
            const style = new TableStyle();
            style.setCompact(true);
            const table = new Table({ style });

            table.push(['foo', 'bar', 'baz'], ['frob', 'bar', 'quuz']);

            const expected = [
                '+------+-----+------+',
                '| foo  | bar | baz  |',
                '| frob | bar | quuz |',
                '+------+-----+------+',
            ];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should print compact if chars empty', () => {
            const style = new TableStyle(
                {
                    verticalOutside: '',
                    verticalInside: ' ', // Important
                    horizontalOutside: '',
                    horizontalInside: '',
                    topMid: '',
                    topLeft: '',
                    topRight: '',
                    bottomMid: '',
                    bottomLeft: '',
                    bottomRight: '',
                    midMid: '',
                    midLeft: '',
                    midRight: '',
                    mid: '',
                    left: '',
                    right: '',
                    crossIntersection: '',
                    crossBottom: '',
                    crossMid: '',
                    truncate: '…',
                    padding: ' ',
                },
                undefined,
                {
                    left: 0,
                    right: 0,
                }
            );
            const table = new Table({ style });

            table.push(['foo', 'bar', 'baz'], ['frobnicate', 'bar', 'quuz']);

            const expected = ['foo        bar baz ', 'frobnicate bar quuz'];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with null/undefined as values or column names', () => {
            const table = new Table();
            table.push([null, undefined, 0]);

            const expected = ['+--+--+---+', '|  |  | 0 |', '+--+--+---+'];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with newlines in headers', () => {
            const table = new Table({ head: ['Test', '1\n2\n3'] });

            const expected = [
                '+------+---+',
                '| Test | 1 |',
                '|      | 2 |',
                '|      | 3 |',
                '+------+---+',
            ];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with custom padding', () => {
            const style = new TableStyle();
            style.definePaddings({ left: 4, right: 5 });
            const table = new Table({ style });

            table.push(
                ['v0.1', 'Testing something cool'],
                ['v0.1', 'Testing something cool']
            );

            const expected = [
                '+-------------+-------------------------------+',
                '|    v0.1     |    Testing something cool     |',
                '+-------------+-------------------------------+',
                '|    v0.1     |    Testing something cool     |',
                '+-------------+-------------------------------+',
            ];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with newlines in body cells', () => {
            const table = new Table();

            table.push(['something\nwith\nnewlines']);

            const expected = [
                '+-----------+',
                '| something |',
                '| with      |',
                '| newlines  |',
                '+-----------+',
            ];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with newlines in vertical cell header and body', () => {
            const style = new TableStyle();
            style.definePaddings({ left: 0, right: 0 });
            const table = new Table({ style });

            table.push({ 'v\n0.1': 'Testing\nsomething cool' });

            const expected = [
                '+---+--------------+',
                '|v  |Testing       |',
                '|0.1|something cool|',
                '+---+--------------+',
            ];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with newlines in cross table header and body', () => {
            const style = new TableStyle();
            style.definePaddings({ left: 0, right: 0 });
            const table = new Table({ style, head: ['', 'Header\n1'] });

            table.push({ 'Header\n2': ['Testing\nsomething\ncool'] });

            const expected = [
                '       +---------+',
                '       |Header   |',
                '       |1        |',
                '+------+---------+',
                '|Header|Testing  |',
                '|2     |something|',
                '|      |cool     |',
                '+------+---------+',
            ];

            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should work with colSpan and rowSpan', () => {
            let table = new Table();
            table.push(
                ['hello', { colSpan: 2, content: 'greetings' }],
                [{ colSpan: 2, content: 'greetings' }, 'howdy'],
                ['final', 'hello', 'howdy']
            );
            let expected = [
                '+-------+---------------+',
                '| hello | greetings     |',
                '+-------+-------+-------+',
                '| greetings     | howdy |',
                '+-------+-------+-------+',
                '| final | hello | howdy |',
                '+-------+-------+-------+',
            ];
            expect(table.render()).to.equal(expected.join('\n'));

            table = new Table();
            table.push(
                [
                    'hello',
                    { rowSpan: 2, content: 'greetings', vAlign: 'middle' },
                    { rowSpan: 2, content: 'final', vAlign: 'bottom' },
                ],
                ['howdy']
            );
            expected = [
                '+-------+-----------+-------+',
                '| hello |           |       |',
                '+-------+ greetings |       |',
                '| howdy |           | final |',
                '+-------+-----------+-------+',
            ];
            expect(table.render()).to.equal(expected.join('\n'));

            table = new Table();
            table.push([{ content: 'a', rowSpan: 3, colSpan: 2 }, 'b'], [], []);
            expected = [
                '+---+---+',
                '| a | b |',
                '|   +---+',
                '|   |   |',
                '|   |   |',
                '|   |   |',
                '+---+---+',
            ];
            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should truncate content', () => {
            const table = new Table({ colWidths: [8], rowHeights: [2] });
            table.push(['hello\nhi\nsup'], ['megaLongWord']);
            const expected = [
                '+--------+',
                '| hello  |',
                '| hi…    |',
                '+--------+',
                '| megaL… |',
                '+--------+',
            ];
            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should draw table separator', () => {
            const content = [
                ['2022-06-26', 'Mega party'],
                ['2O22-07-14', 'For the french'],
                new TableSeparator(),
                ['2022-09-01', 'Back to school'],
            ];

            let table = new Table({ head: ['Date', 'Event'], compact: true });
            table.push(...content);
            let expected = [
                '+------------+----------------+',
                '| Date       | Event          |',
                '+------------+----------------+',
                '| 2022-06-26 | Mega party     |',
                '| 2O22-07-14 | For the french |',
                '+------------+----------------+',
                '| 2022-09-01 | Back to school |',
                '+------------+----------------+',
            ];
            expect(table.render()).to.equal(expected.join('\n'));

            table = new Table({ head: ['Date', 'Event'] });
            table.push(...content);
            expected = [
                '+------------+----------------+',
                '| Date       | Event          |',
                '+------------+----------------+',
                '| 2022-06-26 | Mega party     |',
                '+------------+----------------+',
                '| 2O22-07-14 | For the french |',
                '+------------+----------------+',
                '+------------+----------------+',
                '| 2022-09-01 | Back to school |',
                '+------------+----------------+',
            ];
            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should draw table separator with headers', () => {
            const table = new Table({
                head: ['', 'Date', 'Event'],
                compact: true,
            });
            table.push(
                { First: ['2022-06-26', 'Mega party'] },
                { Second: ['2O22-07-14', 'For the french'] },
                new TableSeparator(),
                { Last: ['2022-09-01', 'Back to school'] }
            );
            const expected = [
                '         +------------+----------------+',
                '         | Date       | Event          |',
                '+--------+------------+----------------+',
                '| First  | 2022-06-26 | Mega party     |',
                '| Second | 2O22-07-14 | For the french |',
                '+--------+------------+----------------+',
                '| Last   | 2022-09-01 | Back to school |',
                '+--------+------------+----------------+',
            ];
            expect(table.render()).to.equal(expected.join('\n'));
        });

        it('Should draw table separator with col span', () => {
            const table = new Table({ compact: true });
            table.push(
                ['a', 'b', 'c', 'd'],
                new TableSeparator(),
                [{ content: 'e', colSpan: 3 }, 'f'],
                new TableSeparator(),
                ['g', { content: 'h', colSpan: 3 }]
            );
            const expected = [
                '+---+---+---+---+',
                '| a | b | c | d |',
                '+---+---+---+---+',
                '| e         | f |',
                '+---+-------+---+',
                '| g | h         |',
                '+---+-----------+',
            ];
            expect(table.render()).to.equal(expected.join('\n'));
        });
    });

    describe('width', () => {
        it('Should work with width property', () => {
            const table = new Table({
                head: ['Cool'],
            });

            expect(table.width).to.equal(8);
        });

        it('Should return the good width with cell new lines', () => {
            const table = new Table({
                head: ['Test\nWidth'],
            });
            expect(table.width).to.equal(9);
        });
    });
});
