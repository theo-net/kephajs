/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Cell } from './Cell';
import { ColSpanCell } from './ColSpanCell';
import { RowSpanCell } from './RowSpanCell';
import { defaultTableOptions, TableOptions } from './Table';
import {
    defaultTableStyleChars,
    defaultTableStyleStyles,
    TableStyle,
    TableStyleStyles,
} from './TableStyle';

const defaultStyle = new TableStyle();

export class TableSeparator {
    public cells:
        | (Cell | ColSpanCell | RowSpanCell | TableSeparator)[][]
        | null = null;

    public x: number | null = 0;

    public y: number | null = 0;

    public widths: number[] = [];

    public heights: number[] = [];

    public colSpan = 1;

    public rowSpan = 1;

    public desiredWidth = 0;

    public desiredHeight = 0;

    public width = 0;

    public head = false;

    public drawRight = false;

    public style = defaultStyle;

    public lines: string[] = [];

    private _options = defaultTableOptions;

    private _chars = defaultTableStyleChars;

    private _styles = defaultTableStyleStyles;

    mergeTableOptions(
        tableOptions: TableOptions,
        cells: (Cell | ColSpanCell | RowSpanCell | TableSeparator)[][] = []
    ) {
        this.cells = cells;
        this._options = { ...this._options, ...tableOptions };

        this._setStyle(this._options.style);
    }

    init(tableOptions: TableOptions) {
        this.widths = tableOptions.colWidths.slice(
            this.x as number,
            (this.x as number) + this.colSpan
        );
        this.heights = tableOptions.rowHeights.slice(
            this.y as number,
            (this.y as number) + this.rowSpan
        );

        this.width = this.widths.reduce(
            (a, b) => {
                return a + b + 1;
            },
            this.widths.length ? -1 : 0
        );

        this.drawRight =
            (this.x as number) == tableOptions.colWidths.length - 1;
    }

    draw(lineNum: string | number) {
        if (lineNum !== 0) return '';

        const content = [];

        if (this.cells !== null) {
            if (this.x === 0)
                content.push(
                    this._applyStyle('borderOutside', this._chars.left)
                );
            else if (
                this.x === 1 &&
                this.y &&
                this.y > 0 &&
                this.cells[this.y - 1][0].head
            )
                content.push(
                    this._applyStyle('borderOutside', this._chars.crossMid)
                );
            // check if span
            else if (this.x && this.y && this.cells.length > this.y + 1) {
                let spanAbove =
                    this.cells[this.y - 1][this.x] instanceof ColSpanCell;
                let spanBelow =
                    this.cells[this.y + 1][this.x] instanceof ColSpanCell;

                let i = this.x;
                while (i >= 0 && !spanAbove) {
                    const cell = this.cells[this.y - 1][i];
                    if (cell instanceof Cell) {
                        if (cell.x && cell.x > this.x) i--;
                        else if (cell.x && cell.x === this.x) i = -1;
                        else if (cell.colSpan > 1) spanAbove = true;
                        else i = -1;
                    } else if (cell instanceof RowSpanCell) {
                        if (cell.x !== cell.originalCell.x && cell.colSpan > 1)
                            spanAbove = true;
                        i--;
                    } else i--;
                }
                i = this.x;
                while (i >= 0 && !spanBelow) {
                    const cell = this.cells[this.y + 1][i];
                    if (cell instanceof Cell) {
                        if (cell.x && cell.x > this.x) i--;
                        else if (cell.x && cell.x === this.x) i = -1;
                        else if (cell.colSpan > 1) spanBelow = true;
                        else i = -1;
                    } else i--;
                }

                content.push(
                    this._applyStyle(
                        'borderInside',
                        spanAbove && spanBelow
                            ? this._chars.horizontalInside
                            : this._chars.mid
                    )
                );
            } else
                content.push(this._applyStyle('borderInside', this._chars.mid));
            content.push(
                this._applyStyle(
                    'borderInside',
                    this._chars.horizontalInside.repeat(this.width)
                )
            );
        } else {
            if (this.x === 0)
                content.push(
                    this._applyStyle('borderOutside', this._chars.left)
                );
            else
                content.push(this._applyStyle('borderInside', this._chars.mid));
            content.push(
                this._applyStyle(
                    'borderInside',
                    this._chars.horizontalInside.repeat(this.width)
                )
            );
        }

        if (this.drawRight)
            content.push(this._applyStyle('borderOutside', this._chars.right));

        return content.join('');
    }

    private _setStyle(style: TableStyle) {
        this.style = style;
        this._chars = this.style.getChars();
        this._styles = this.style.getStyles();
    }

    private _applyStyle(style: keyof TableStyleStyles, content: string) {
        if (content === '') return '';
        if (this._styles[style] !== undefined) {
            return `<${this._styles[style]}>${content}</>`;
        } else return content;
    }
}
