/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * 6=====3=====5==============5============7
 * 1           2              2            1
 * 12====3=====11=============11==========13
 * 1           2              2            1
 * 15----4-----14-------------14----------16
 * 1           2              2            1
 * 15----4-----14-------------14----------16
 * 1           2              2            1
 * 9=====3=====8==============8===========10
 *
 *
 *             6=======3======5============7
 *             1              2            1
 * 6=====3=====17=============11==========13
 * 1           1              2            1
 * 15----4-----19-------------14----------16
 * 1           1              2            1
 * 15----4-----19-------------14----------16
 * 1           1              2            1
 * 9=====3=====18=============8===========10
 *
 */
export type TableStyleChars = {
    verticalOutside: string; // 1
    verticalInside: string; // 2
    horizontalOutside: string; // 3
    horizontalInside: string; // 4
    topMid: string; // 5
    topLeft: string; // 6
    topRight: string; // 7
    bottomMid: string; // 8
    bottomLeft: string; // 9
    bottomRight: string; // 10
    midMid: string; // 11
    midLeft: string; // 12
    midRight: string; // 13
    mid: string; // 14
    left: string; // 15
    right: string; // 16
    crossIntersection: string; // 17
    crossBottom: string; // 18
    crossMid: string; // 19
    truncate: string;
    padding: string;
};

export type TableStyleStyles = {
    head: string | undefined;
    cell: string | undefined;
    borderOutside: string | undefined;
    borderInside: string | undefined;
};

export type TableStylePaddings = {
    left: number;
    right: number;
};

export const defaultTableStyleChars: TableStyleChars = {
    verticalOutside: '|',
    verticalInside: '|',
    horizontalOutside: '-',
    horizontalInside: '-',
    topMid: '+',
    topLeft: '+',
    topRight: '+',
    bottomMid: '+',
    bottomLeft: '+',
    bottomRight: '+',
    midMid: '+',
    midLeft: '+',
    midRight: '+',
    mid: '+',
    left: '+',
    right: '+',
    crossIntersection: '+',
    crossBottom: '+',
    crossMid: '+',
    truncate: '…',
    padding: ' ',
};
export const defaultTableStyleStyles: TableStyleStyles = {
    head: undefined,
    cell: undefined,
    borderOutside: undefined,
    borderInside: undefined,
};
export const defaultTableStylePaddings: TableStylePaddings = {
    left: 1,
    right: 1,
};

export class TableStyle {
    private _chars: TableStyleChars;

    private _paddings: TableStylePaddings;

    private _styles: TableStyleStyles;

    constructor(
        chars?: TableStyleChars,
        styles?: TableStyleStyles,
        padding?: TableStylePaddings,
        private _compact = false
    ) {
        this._chars = chars ?? defaultTableStyleChars;
        this._styles = styles ?? defaultTableStyleStyles;
        this._paddings = padding ?? defaultTableStylePaddings;
    }

    defineChars(chars: TableStyleChars) {
        this._chars = chars;
    }

    getChars() {
        return this._chars;
    }

    setCompact(compact: boolean) {
        this._compact = compact;
    }

    isCompact() {
        return this._compact;
    }

    defineStyles(styles: TableStyleStyles) {
        this._styles = styles;
    }

    getStyles() {
        return this._styles;
    }

    definePaddings(paddings: TableStylePaddings) {
        this._paddings = paddings;
    }

    getPaddings() {
        return this._paddings;
    }
}
