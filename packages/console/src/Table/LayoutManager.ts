/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/*
 * Inspired by `cli-table2` James Talmage <james.talmage@jrtechnical.com>
 */

import { Cell } from './Cell';
import { ColSpanCell } from './ColSpanCell';
import { ConflictTableSeparatorError } from './Error/ConflictTableSeparatorError';
import { LayoutManagerError } from './Error/LayoutManagerError';
import { MixCellTableSeparatorError } from './Error/MixCellTableSeparatorError';
import { RowSpanCell } from './RowSpanCell';
import { CellDeclaration, RowDeclaration } from './Table';
import { TableManager } from './TableManager';
import { TableSeparator } from './TableSeparator';
import { TableStyle } from './TableStyle';

export class LayoutManager {
    private _cells: (Cell | ColSpanCell | RowSpanCell | TableSeparator)[][];

    constructor(
        rows: (RowDeclaration | TableSeparator)[],
        private _tableStyle: TableStyle | string,
        private _tableManager?: TableManager
    ) {
        this._cells = this._generateCells(rows);
        this._layoutTable(this._cells as (Cell | TableSeparator)[][]);
        this._fillInTable(this._cells as (Cell | TableSeparator)[][]);
        this._addRowSpanCells(this._cells as (Cell | TableSeparator)[][]);
        this._addColSpanCells(
            this._cells as (Cell | RowSpanCell | TableSeparator)[][]
        );
    }

    getCells() {
        return this._cells;
    }

    getMaxWidth(cells?: (Cell | TableSeparator)[][]) {
        let mw = 0;

        (cells ?? this._cells).forEach(row => {
            row.forEach(
                (cell: Cell | ColSpanCell | RowSpanCell | TableSeparator) => {
                    mw = Math.max(
                        mw,
                        (cell.x as number) + ((cell as Cell).colSpan || 1)
                    );
                }
            );
        });

        return mw;
    }

    private _generateCells(rows: RowDeclaration[]) {
        return rows.map(row => {
            if (row instanceof TableSeparator) {
                row = [row];
            } else if (!Array.isArray(row)) {
                const key = Object.keys(row)[0];
                row = row[key] as CellDeclaration[];
                const header: CellDeclaration = { content: key, head: true };

                if (Array.isArray(row)) {
                    row = row.slice();
                    (row as CellDeclaration[]).unshift(header);
                } else row = [header, row];
            }

            return row.map(cell => {
                if (cell instanceof TableSeparator) return cell;
                return new Cell(cell, this._tableStyle, this._tableManager);
            });
        });
    }

    /**
     * Define values of x and y for each cell
     */
    private _layoutTable(rows: (Cell | TableSeparator)[][]) {
        rows.forEach((row, rowIndex) => {
            if (row[0] instanceof TableSeparator) {
                row.forEach(cell => {
                    if (cell instanceof Cell)
                        throw new MixCellTableSeparatorError(rowIndex);
                });
                row[0].y = rowIndex;
            } else {
                row.forEach((cell, columnIndex) => {
                    if (cell instanceof TableSeparator)
                        throw new MixCellTableSeparatorError(rowIndex);

                    cell.y = rowIndex;
                    cell.x = columnIndex;

                    for (let y = rowIndex; y >= 0; y--) {
                        const row2 = rows[y];
                        const xMax = y === rowIndex ? columnIndex : row2.length;

                        for (let x = 0; x < xMax; x++) {
                            const cell2 = row2[x];
                            while (this._cellsConflict(cell, cell2)) cell.x++;
                        }
                    }
                });
            }
        });
    }

    /**
     * Detect if conflict exists between two cells, because `colSpan` or `rowSpan`
     */
    private _cellsConflict(
        cell1: Cell | TableSeparator,
        cell2: Cell | TableSeparator
    ) {
        const yMin1 = cell1.y as number;
        const yMax1 = (cell1.y as number) - 1 + (cell2.rowSpan || 1);
        const yMin2 = cell2.y as number;
        const yMax2 = (cell2.y as number) - 1 + (cell2.rowSpan || 1);
        const yConflict = !(yMin1 > yMax2 || yMin2 > yMax1);

        const xMin1 = cell1.x as number;
        const xMax1 = (cell1.x as number) - 1 + (cell2.colSpan || 1);
        const xMin2 = cell2.x as number;
        const xMax2 = (cell2.x as number) - 1 + (cell2.colSpan || 1);
        const xConflict = !(xMin1 > xMax2 || xMin2 > xMax1);

        return yConflict && xConflict;
    }

    private _fillInTable(rows: (Cell | TableSeparator)[][]) {
        const hMax = rows.length;
        const wMax = this.getMaxWidth(rows);

        for (let y = 0; y < hMax; y++) {
            if (rows[y][0] instanceof TableSeparator) {
                for (let x = 1; x < wMax; x++) {
                    if (this._conflictExists(rows, x, y))
                        throw new ConflictTableSeparatorError(y);
                    const tableSeparator = new TableSeparator();
                    tableSeparator.x = x;
                    tableSeparator.y = y;
                    this._insertCell(tableSeparator, rows[y]);
                }
            }

            for (let x = 0; x < wMax; x++) {
                if (!this._conflictExists(rows, x, y)) {
                    const opts = { x: x, y: y, colSpan: 1, rowSpan: 1 };
                    x++;
                    while (x < wMax && !this._conflictExists(rows, x, y)) {
                        opts.colSpan++;
                        x++;
                    }
                    let y2 = y + 1;
                    while (
                        y2 < hMax &&
                        this._allBlank(
                            rows as Cell[][],
                            y2,
                            opts.x,
                            opts.x + opts.colSpan
                        )
                    ) {
                        opts.rowSpan++;
                        y2++;
                    }

                    const cell = new Cell({
                        content: '',
                        colSpan: opts.colSpan,
                        rowSpan: opts.rowSpan,
                    });
                    cell.x = opts.x;
                    cell.y = opts.y;
                    this._insertCell(cell, rows[y]);
                }
            }
        }
    }

    private _conflictExists(
        rows: (Cell | TableSeparator)[][],
        x: number,
        y: number
    ) {
        const iMax = Math.min(rows.length - 1, y);
        const cell = { x: x, y: y };

        for (let i = 0; i <= iMax; i++) {
            const row = rows[i];
            for (let j = 0; j < row.length; j++) {
                if (this._cellsConflict(cell as Cell | TableSeparator, row[j]))
                    return true;
            }
        }

        return false;
    }

    private _allBlank(
        rows: Cell[][],
        y: number,
        xMin: number,
        xMax: number
    ): boolean {
        for (let x = xMin; x < xMax; x++) {
            if (this._conflictExists(rows, x, y)) return false;
        }
        return true;
    }

    private _insertCell(
        cell: Cell | RowSpanCell | TableSeparator,
        row: (Cell | TableSeparator)[]
    ) {
        let x = 0;
        while (x < row.length && (row[x].x as number) < (cell.x as number)) x++;

        row.splice(x, 0, cell as Cell);
    }

    private _addRowSpanCells(rows: (Cell | TableSeparator)[][]) {
        rows.forEach((row, rowIndex) => {
            if (row[0] instanceof TableSeparator) return;
            (row as Cell[]).forEach(cell => {
                for (let i = 1; i < cell.rowSpan; i++) {
                    if (rowIndex + i >= rows.length)
                        throw new LayoutManagerError(
                            'Not enought rows ine the table for the cell {' +
                                cell.x +
                                ';' +
                                cell.y +
                                '} with a rowspan of ' +
                                cell.rowSpan +
                                '. You need to add ' +
                                (rowIndex + i - rows.length + 1) +
                                ' row(s).'
                        );
                    const rowSpanCell = new RowSpanCell(cell);
                    rowSpanCell.x = cell.x;
                    rowSpanCell.y = (cell.y as number) + i;
                    rowSpanCell.colSpan = cell.colSpan;
                    this._insertCell(rowSpanCell, rows[rowIndex + i]);
                }
            });
        });
    }

    private _addColSpanCells(
        rows: (Cell | ColSpanCell | RowSpanCell | TableSeparator)[][]
    ) {
        for (let rowIndex = rows.length - 1; rowIndex >= 0; rowIndex--) {
            const cellColumns = rows[rowIndex];
            if (cellColumns[0] instanceof TableSeparator) return;
            for (
                let columnIndex = 0;
                columnIndex < cellColumns.length;
                columnIndex++
            ) {
                const cell = cellColumns[columnIndex] as Cell | RowSpanCell;

                for (let k = 1; k < cell.colSpan; k++) {
                    const colSpanCell = new ColSpanCell();
                    colSpanCell.x = (cell.x as number) + k;
                    colSpanCell.y = cell.y;
                    cellColumns.splice(columnIndex + 1, 0, colSpanCell);
                }
            }
        }
    }
}

function makeComputeWidths(
    colOrRowSpan: 'colSpan' | 'rowSpan',
    desired: 'desiredWidth' | 'desiredHeight',
    xOrY: 'x' | 'y',
    forcedMin: number
) {
    return (values: (number | null)[], rows: (Cell | TableSeparator)[][]) => {
        const result: number[] = [];
        const spanners: (Cell | TableSeparator)[] = [];

        rows.forEach(row => {
            row.forEach(cell => {
                // if colSpan or rowSpan
                if ((cell[colOrRowSpan] || 1) > 1) spanners.push(cell);
                // else take the value of the cell
                else {
                    result[cell[xOrY] as number] = Math.max(
                        result[cell[xOrY] as number] || 0,
                        cell[desired] || 0,
                        forcedMin
                    );
                }
            });
        });

        // if a value is defined, take it
        values.forEach((value, index) => {
            if (typeof value === 'number') result[index] = value;
        });

        // parse the spanners
        for (let k = spanners.length - 1; k >= 0; k--) {
            const cell = spanners[k];
            const span = cell[colOrRowSpan];
            const col = cell[xOrY] as number;
            let existingWidth = result[col];
            let editableCols = typeof values[col] === 'number' ? 0 : 1;

            for (let i = 1; i < span; i++) {
                existingWidth += 1 + result[col + i];
                if (typeof values[col + i] !== 'number') editableCols++;
            }

            if (cell[desired] > existingWidth) {
                let i = 0;
                while (editableCols > 0 && cell[desired] > existingWidth) {
                    if (typeof values[col + i] !== 'number') {
                        const dif = Math.round(
                            (cell[desired] - existingWidth) / editableCols
                        );

                        existingWidth += dif;
                        result[col + i] += dif;
                        editableCols--;
                    }
                    i++;
                }
            }
        }

        for (let j = 0; j < result.length; j++)
            result[j] = Math.max(forcedMin, result[j] || 0);

        return result;
    };
}

export const computeWidths = makeComputeWidths(
    'colSpan',
    'desiredWidth',
    'x',
    1
);
export const computeHeights = makeComputeWidths(
    'rowSpan',
    'desiredHeight',
    'y',
    1
);
