/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import { Cell } from './Cell';

import { TableSeparator } from './TableSeparator';
import { TableStyle } from './TableStyle';

describe('@kephajs/console/Table/TableSeparator', () => {
    describe('draw', () => {
        const style = new TableStyle({
            verticalOutside: '║',
            verticalInside: '│',
            horizontalOutside: '═',
            horizontalInside: '─',
            topMid: '╤',
            topLeft: '╔',
            topRight: '╗',
            bottomMid: '╧',
            bottomLeft: '╚',
            bottomRight: '╝',
            midMid: '╪',
            midLeft: '╠',
            midRight: '╣',
            mid: '┼',
            left: '╟',
            right: '╢',
            crossIntersection: '╬',
            crossBottom: '╩',
            crossMid: '╫',
            truncate: '…',
            padding: ' ',
        });
        let cell: TableSeparator;

        beforeEach(() => {
            cell = new TableSeparator();
            cell.style = style;
            cell.width = 7;
            Object.getPrototypeOf(cell)._setStyle.call(cell, style);
        });

        it('Should not draw for something else than 0', () => {
            expect(cell.draw('top')).to.be.equal('');
            expect(cell.draw(1)).to.be.equal('');
            expect(cell.draw('bottom')).to.be.equal('');
        });

        it('Should draw the left mid corner when x=0', () => {
            cell.x = 0;
            cell.y = 1;
            expect(cell.draw(0)).to.be.equal('╟───────');
            cell.drawRight = true;
            expect(cell.draw(0)).to.be.equal('╟───────╢');
        });

        it('Should draw the mid mid corner when x=1', () => {
            cell.x = 1;
            cell.y = 1;
            expect(cell.draw(0)).to.be.equal('┼───────');
            cell.drawRight = true;
            expect(cell.draw(0)).to.be.equal('┼───────╢');
        });

        it('Should draw the good corner under header', () => {
            const head = new Cell({ content: 'Test', head: true });
            const normalCell = new Cell('Test');
            cell.cells = [
                [head, normalCell],
                [cell, cell],
            ];

            cell.x = 0;
            cell.y = 1;
            cell.widths = [7];
            expect(cell.draw(0)).to.be.equal('╟───────');
            cell.drawRight = true;
            expect(cell.draw(0)).to.be.equal('╟───────╢');

            cell.x = 1;
            cell.y = 1;
            cell.widths = [7];
            cell.drawRight = false;
            expect(cell.draw(0)).to.be.equal('╫───────');
            cell.drawRight = true;
            expect(cell.draw(0)).to.be.equal('╫───────╢');
        });

        it('Should draw in the color specified by border style', () => {
            style.defineStyles({
                head: undefined,
                cell: undefined,
                borderOutside: 'grey',
                borderInside: 'blue',
            });
            Object.getPrototypeOf(cell)._setStyle.call(cell, style);
            cell.x = 0;
            cell.y = 1;
            expect(cell.draw(0)).to.be.equal('<grey>╟</><blue>───────</>');
            cell.x = cell.y = 1;
            cell.drawRight = true;
            expect(cell.draw(0)).to.be.equal(
                '<blue>┼</><blue>───────</><grey>╢</>'
            );
        });

        it('Should draw in the color specified by border style under header', () => {
            style.defineStyles({
                head: undefined,
                cell: undefined,
                borderOutside: 'grey',
                borderInside: 'blue',
            });
            Object.getPrototypeOf(cell)._setStyle.call(cell, style);

            const head = new Cell({ content: 'Test', head: true });
            const normalCell = new Cell('Test');
            cell.cells = [
                [head, normalCell],
                [cell, cell],
            ];

            cell.x = 0;
            cell.y = 1;
            cell.widths = [7];
            expect(cell.draw(0)).to.be.equal('<grey>╟</><blue>───────</>');
            cell.drawRight = true;
            expect(cell.draw(0)).to.be.equal(
                '<grey>╟</><blue>───────</><grey>╢</>'
            );

            cell.x = 1;
            cell.y = 1;
            cell.widths = [7];
            cell.drawRight = false;
            expect(cell.draw(0)).to.be.equal('<grey>╫</><blue>───────</>');
            cell.drawRight = true;
            expect(cell.draw(0)).to.be.equal(
                '<grey>╫</><blue>───────</><grey>╢</>'
            );
        });
    });
});
