/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { AvailableOptions, Color } from './Color';

export class OutputFormatterStyle {
    private _color: Color;

    private _foreground: string;

    private _background: string;

    private _options: AvailableOptions[];

    private _truecolor: boolean;

    private _href: string | null = null;

    constructor(
        foreground: string | null = null,
        background: string | null = null,
        options: AvailableOptions[] = [],
        truecolor = false
    ) {
        this._color = new Color(
            (this._foreground = foreground ? foreground : ''),
            (this._background = background ? background : ''),
            (this._options = options),
            (this._truecolor = truecolor)
        );
    }

    setForeground(color: string) {
        this._color = new Color(
            (this._foreground = color),
            this._background,
            this._options,
            this._truecolor
        );
    }

    getForeground() {
        return this._foreground;
    }

    setBackground(color: string) {
        this._color = new Color(
            this._foreground,
            (this._background = color),
            this._options,
            this._truecolor
        );
    }

    getBackground() {
        return this._background;
    }

    setHref(url: string) {
        this._href = url;
    }

    getHref() {
        return this._href;
    }

    setOption(option: AvailableOptions) {
        this._options.push(option);
        this._color = new Color(
            this._foreground,
            this._background,
            this._options,
            this._truecolor
        );
    }

    unsetOption(option: AvailableOptions) {
        const pos = this._options.indexOf(option);
        if (-1 !== pos) {
            delete this._options[pos];
        }

        this._color = new Color(
            this._foreground,
            this._background,
            this._options,
            this._truecolor
        );
    }

    setOptions(options: AvailableOptions[]) {
        this._color = new Color(
            this._foreground,
            this._background,
            (this._options = options),
            this._truecolor
        );
    }

    getOptions() {
        return this._options;
    }

    apply(text: string) {
        if (null !== this._href) {
            text = `\u001b]8;;${this._href}\u001b\\${text}\u001b]8;;\u001b\\`;
        }

        return this._color.apply(text);
    }
}
