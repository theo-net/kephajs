/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { OutputFormatterStyle } from './OutputFormatterStyle';
import { OutputFormatterStyleStack } from './OutputFormatterStyleStack';

describe('@kephajs/console/Output/OutputFormatterStyleSlack', () => {
    it('Should set an empty style', () => {
        const empty = new OutputFormatterStyleStack().getEmptyStyle();
        expect(empty.getForeground()).to.be.equal('');
        expect(empty.getBackground()).to.be.equal('');
        expect(empty.getOptions()).to.be.deep.equal([]);
    });

    it('Should push and pop', () => {
        const style1 = new OutputFormatterStyle();
        const style2 = new OutputFormatterStyle();
        const stack = new OutputFormatterStyleStack();
        stack.push(style1);
        stack.push(style2);
        expect(stack.pop()).is.equal(style2);
        expect(stack.pop()).is.equal(style1);
    });

    it('Should return empty style if the stack is empty', () => {
        const stack = new OutputFormatterStyleStack();
        expect(stack.pop()).to.be.equal(stack.getEmptyStyle());
    });

    it('Should return current style or empty style if stack is empty', () => {
        const stack = new OutputFormatterStyleStack();
        const style1 = new OutputFormatterStyle();
        const style2 = new OutputFormatterStyle();

        expect(stack.getCurrent()).to.be.equal(stack.getEmptyStyle());
        stack.push(style1);
        stack.push(style2);
        expect(stack.getCurrent()).to.be.equal(style2);
        stack.pop();
        expect(stack.getCurrent()).to.be.equal(style1);
    });

    it('Should reset the stack', () => {
        const stack = new OutputFormatterStyleStack();
        stack.push(new OutputFormatterStyle());
        stack.push(new OutputFormatterStyle());
        stack.reset();
        expect(stack.getCurrent()).to.be.equal(stack.getEmptyStyle());
    });
});
