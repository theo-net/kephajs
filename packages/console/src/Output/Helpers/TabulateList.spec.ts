/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import Sinon from 'sinon';

import { NullOutput } from '../NullOutput';
import { TabulateList } from './TabulateList';

describe('@kephajs/console/Command/Output/Helpers/TabulateList', () => {
    describe('options', () => {
        it('Should have default options', () => {
            expect(new TabulateList().options).to.be.deep.equal({
                prefix: [],
                styles: [],
                margin: 2,
            });
        });

        it('Should print merge the options', () => {
            expect(
                new TabulateList({ prefix: ['a'], styles: ['b'], margin: 3 })
                    .options
            ).to.be.deep.equal({ prefix: ['a'], styles: ['b'], margin: 3 });
            expect(
                new TabulateList({ prefix: ['a'] }).options
            ).to.be.deep.equal({ prefix: ['a'], styles: [], margin: 2 });
            expect(
                new TabulateList({ styles: ['b'], margin: 3 }).options
            ).to.be.deep.equal({ prefix: [], styles: ['b'], margin: 3 });
        });

        it('Should update options', () => {
            const tabulateList = new TabulateList({
                prefix: ['a'],
                styles: ['b'],
                margin: 3,
            });
            tabulateList.setOptions({ margin: 4 });
            expect(tabulateList.options).to.be.deep.equal({
                prefix: ['a'],
                styles: ['b'],
                margin: 4,
            });
        });
    });

    describe('render', () => {
        let tabulateList: TabulateList;

        beforeEach(() => {
            tabulateList = new TabulateList();
        });

        it('Should return the rendered string', () => {
            const output = new NullOutput();
            tabulateList.push(['a', 'b'], ['aa', 'bb']);
            expect(tabulateList.render()).to.be.equal('a   b\naa  bb');
            expect(tabulateList.render(output)).to.be.equal('a   b\naa  bb');
        });

        it('Should writeln on the Output', () => {
            const output = new NullOutput();
            const spy = Sinon.spy(output, 'writeln');
            tabulateList.push(['a', 'b'], ['aa', 'bb']);
            tabulateList.render(output);
            expect(spy.getCalls()[0].args[0]).to.be.equal('a   b\naa  bb');
        });

        it('Should return empty string if list empty', () => {
            expect(tabulateList.render()).to.be.equal('');
        });

        it('Should have different prefix for each column', () => {
            tabulateList.setOptions({ prefix: ['..', ';;'] });
            tabulateList.push(['aa', 'bb', 'cc'], ['aaa', 'bbb', 'ccc']);
            expect(tabulateList.render()).to.be.equal(
                '..aa   ;;bb   cc\n..aaa  ;;bbb  ccc'
            );
        });

        it('Should have different style for each column', () => {
            tabulateList.setOptions({ styles: ['error', 'info'] });
            tabulateList.push(['aa', 'bb', 'cc'], ['aaa', 'bbb', 'ccc']);
            expect(tabulateList.render()).to.be.equal(
                '<error>aa</>   <info>bb</>   cc\n<error>aaa</>  <info>bbb</>  ccc'
            );
        });

        it('Should stylize the prefix', () => {
            tabulateList.setOptions({
                styles: ['error', 'info'],
                prefix: ['..', ';;'],
            });
            tabulateList.push(['aa', 'bb', 'cc'], ['aaa', 'bbb', 'ccc']);
            expect(tabulateList.render()).to.be.equal(
                '<error>..aa</>   <info>;;bb</>   cc\n<error>..aaa</>  <info>;;bbb</>  ccc'
            );
        });

        it('Should have style in the prefix', () => {
            tabulateList.setOptions({
                styles: ['', 'error'],
                prefix: ['', '<info>..</>'],
            });
            tabulateList.push(['aa', 'bb', 'cc'], ['aaa', 'bbb', 'ccc']);
            expect(tabulateList.render()).to.be.equal(
                'aa   <error><info>..</>bb</>   cc\naaa  <error><info>..</>bbb</>  ccc'
            );
        });

        it('Should have custom margin', () => {
            tabulateList.setOptions({ margin: 3 });
            tabulateList.push(['aa', 'bb', 'cc'], ['aaa', 'bbb', 'ccc']);
            expect(tabulateList.render()).to.be.equal(
                'aa    bb    cc\naaa   bbb   ccc'
            );
        });

        it('Should have custom prefix', () => {
            tabulateList.push(
                ['aa', { content: 'bb', style: '', prefix: ' - ' }, 'cc'],
                ['aaa', 'bbb', 'ccc']
            );
            expect(tabulateList.render()).to.be.equal(
                'aa    - bb  cc\naaa  bbb    ccc'
            );
        });

        it('Should have custom style', () => {
            tabulateList.setOptions({ styles: ['', 'error'] });
            tabulateList.push(
                ['aa', { content: 'bb', style: 'info', prefix: ' - ' }, 'cc'],
                ['aaa', 'bbb', 'ccc']
            );
            expect(tabulateList.render()).to.be.equal(
                'aa   <info> - bb</>  cc\naaa  <error>bbb</>    ccc'
            );
        });

        it('Should work with style inside content', () => {
            tabulateList.push(
                ['aa', 'b<info>b</info>', 'cc'],
                ['aaa', 'bbb', 'ccc']
            );
            expect(tabulateList.render()).to.be.equal(
                'aa   b<info>b</info>   cc\naaa  bbb  ccc'
            );
        });
    });
});
