/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { strlenMaxLineStylized } from '../../Helpers/Helpers';
import { Output } from '../Output';

export type TabulateListOptions = {
    prefix: string[];
    styles: string[];
    margin: number;
};

export type TabulateListElement = {
    content: string;
    prefix: string;
    style: string;
};

export class TabulateList extends Array<(string | TabulateListElement)[]> {
    private _list: TabulateListElement[][] = [];

    private _options: TabulateListOptions = {
        prefix: [],
        styles: [],
        margin: 2,
    };

    constructor(options: Partial<TabulateListOptions> = {}) {
        super();

        this.setOptions(options);
    }

    get options() {
        return this._options;
    }

    setOptions(options: Partial<TabulateListOptions> = {}) {
        this._options = {
            ...this.options,
            ...options,
        };
    }

    render(output?: Output) {
        const result: string[] = [];

        if (this._list.length > 0) this._list = [];

        this.forEach(row => {
            const parsedRow: TabulateListElement[] = [];
            row.forEach((element, col) => {
                if (typeof element === 'string')
                    element = {
                        content: element,
                        prefix: this._options.prefix[col] ?? '',
                        style: this._options.styles[col] ?? '',
                    };
                parsedRow.push(element);
            });
            this._list.push(parsedRow);
        });

        const maxWidth = this._getMaxWidth();

        this._list.forEach(row => {
            let finalRow = '';
            row.forEach((element, col) => {
                if (col !== 0) finalRow += ' '.repeat(this._options.margin);
                finalRow += this._applyStyle(
                    element.prefix + element.content,
                    element.style
                );

                if (col < row.length - 1)
                    finalRow += ' '.repeat(
                        maxWidth[col] -
                            strlenMaxLineStylized(element.prefix) -
                            strlenMaxLineStylized(element.content)
                    );
            });
            result.push(finalRow);
        });

        const resultString = result.join('\n');
        if (output) output.writeln(resultString);
        return resultString;
    }

    private _getMaxWidth() {
        const result: number[] = [];

        this._list.forEach(row => {
            row.forEach((element, col) => {
                const length =
                    strlenMaxLineStylized(element.prefix) +
                    strlenMaxLineStylized(element.content);
                if (!result[col]) result[col] = length;
                else result[col] = Math.max(result[col], length);
            });
        });

        return result;
    }

    private _applyStyle(content: string, style: string) {
        if (content === '') return '';
        if (style !== '') {
            return `<${style}>${content}</>`;
        } else return content;
    }
}
