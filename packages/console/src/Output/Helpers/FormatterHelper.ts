/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { OutputFormatter } from '../OutputFormatter';
import { TabulateList, TabulateListOptions } from './TabulateList';

export class FormatterHelper {
    tabulateList(options: Partial<TabulateListOptions> = {}) {
        return new TabulateList(options);
    }

    formatSection(section: string, message: string, style = 'info') {
        return `<${style}>[${section}]</> ${message}`;
    }

    formatBlock(messages: string | string[], style: string, large = false) {
        if (!Array.isArray(messages)) messages = [messages];

        let len = 0;
        const lines: string[] = [];
        messages.forEach(message => {
            message = OutputFormatter.escape(message);
            lines.push(large ? '  ' + message + '  ' : ' ' + message + ' ');
            len = Math.max(message.length + (large ? 4 : 2), len);
        });

        const block = large ? [' '.repeat(len)] : [];
        lines.forEach(line => {
            block.push(line + ' '.repeat(len - line.length));
        });
        if (large) block.push(' '.repeat(len));

        block.forEach((line, index) => {
            block[index] = `<${style}>${line}</>`;
        });

        return block.join('\n');
    }
}
