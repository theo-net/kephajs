/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import { FormatterHelper } from './FormatterHelper';

import { TabulateList } from './TabulateList';

describe('@kephajs/console/Command/Output/Helpers/FormatterHelper', () => {
    describe('tabulateList', () => {
        it('Should return a new TabulateList', () => {
            expect(new FormatterHelper().tabulateList()).to.be.instanceOf(
                TabulateList
            );
        });

        it('Should give options', () => {
            const tabulateList = new FormatterHelper().tabulateList({
                prefix: ['a'],
                styles: ['b'],
                margin: 3,
            });
            expect(tabulateList.options).to.be.deep.equal({
                prefix: ['a'],
                styles: ['b'],
                margin: 3,
            });
        });
    });

    describe('formatSection', () => {
        it('Should create a format section with the default style info', () => {
            expect(
                new FormatterHelper().formatSection('foo', 'bar')
            ).to.be.equal('<info>[foo]</> bar');
        });

        it('Should custom the style', () => {
            expect(
                new FormatterHelper().formatSection('foo', 'bar', 'error')
            ).to.be.equal('<error>[foo]</> bar');
        });
    });

    describe('formatBlock', () => {
        it('Should return a formatted block', () => {
            expect(
                new FormatterHelper().formatBlock(['hello'], 'error')
            ).to.be.equal('<error> hello </>');
        });

        it('Should be large if we want', () => {
            expect(
                new FormatterHelper().formatBlock(['hello'], 'error', true)
            ).to.be.equal(
                '<error>         </>\n<error>  hello  </>\n<error>         </>'
            );
        });

        it('Should have same length for each line', () => {
            expect(
                new FormatterHelper().formatBlock(
                    ['hello', 'World!'],
                    'error',
                    true
                )
            ).to.be.equal(
                [
                    '<error>          </>',
                    '<error>  hello   </>',
                    '<error>  World!  </>',
                    '<error>          </>',
                ].join('\n')
            );
        });
    });
});
