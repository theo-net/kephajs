/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { OutputFormatter } from './OutputFormatter';
import { OutputFormatterStyle } from './OutputFormatterStyle';

describe('@kephajs/console/Output/OutputFormatter', () => {
    describe('constructor', () => {
        it('Should set decorated to true by default', () => {
            const formatter = new OutputFormatter();
            expect(formatter.isDecorated()).to.be.equal(true);
        });

        it('Should set decorated', () => {
            const formatter = new OutputFormatter(false);
            expect(formatter.isDecorated()).to.be.equal(false);
        });

        it('Should set style error', () => {
            const formatter = new OutputFormatter();
            const style = formatter.getStyle('error');
            expect(style.getForeground()).to.be.equal('white');
            expect(style.getBackground()).to.be.equal('red');
            expect(style.getOptions()).to.be.deep.equal([]);
        });

        it('Should set style info', () => {
            const formatter = new OutputFormatter();
            const style = formatter.getStyle('info');
            expect(style.getForeground()).to.be.equal('green');
            expect(style.getBackground()).to.be.equal('');
            expect(style.getOptions()).to.be.deep.equal([]);
        });

        it('Should set style comment', () => {
            const formatter = new OutputFormatter();
            const style = formatter.getStyle('comment');
            expect(style.getForeground()).to.be.equal('yellow');
            expect(style.getBackground()).to.be.equal('');
            expect(style.getOptions()).to.be.deep.equal([]);
        });

        it('Should set style question', () => {
            const formatter = new OutputFormatter();
            const style = formatter.getStyle('question');
            expect(style.getForeground()).to.be.equal('black');
            expect(style.getBackground()).to.be.equal('cyan');
            expect(style.getOptions()).to.be.deep.equal([]);
        });

        it('Should add styles', () => {
            const styles = {
                style1: new OutputFormatterStyle(),
                style2: new OutputFormatterStyle(),
            };
            const formatter = new OutputFormatter(false, styles);
            expect(formatter.getStyle('style1')).to.be.equal(styles.style1);
            expect(formatter.getStyle('style2')).to.be.equal(styles.style2);
        });
    });

    describe('decorated', () => {
        it('Should set decorated', () => {
            const formatter = new OutputFormatter(false);
            formatter.setDecorated(true);
            expect(formatter.isDecorated()).to.be.equal(true);
        });
    });

    describe('trueColor', () => {
        it('Should get trueColor', () => {
            expect(new OutputFormatter().isTrueColor()).to.be.equal(false);
            expect(
                new OutputFormatter(true, {}, true).isTrueColor()
            ).to.be.equal(true);
        });
    });

    describe('styles', () => {
        it('Should add style', () => {
            const formatter = new OutputFormatter();
            const style = new OutputFormatterStyle();
            formatter.setStyle('test', style);
            expect(formatter.getStyle('test')).to.be.equal(style);
        });

        it('Should return has style, case insensitive', () => {
            const formatter = new OutputFormatter();
            expect(formatter.hasStyle('info')).to.be.equal(true);
            expect(formatter.hasStyle('InFo')).to.be.equal(true);
        });

        it('Should get style, case insensitive', () => {
            const formatter = new OutputFormatter();
            const style = new OutputFormatterStyle();
            formatter.setStyle('test', style);
            expect(formatter.getStyle('Test')).to.be.equal(style);
        });
    });

    describe('format', () => {
        it('Should work with non styled', () => {
            const formatter = new OutputFormatter();
            expect(formatter.format('Hello World!')).to.be.equal(
                'Hello World!'
            );
        });

        it('Should replace styles', () => {
            const formatter = new OutputFormatter();
            expect(
                formatter.format('H<error>ell</error>o <info>World!</info>')
            ).to.be.equal(
                'H\u001b[37;41mell\u001b[39;49mo \u001b[32mWorld!\u001b[39m'
            );
        });

        it('Should work with imbricated styles', () => {
            const formatter = new OutputFormatter();
            expect(
                formatter.format('Hello <info>Worl<error>d</error>!</info>')
            ).to.be.equal(
                'Hello \u001b[32mWorl\u001b[39m\u001b[37;41md\u001b[39;49m\u001b[32m!\u001b[39m'
            );
        });

        it('Should work with imbricate errors', () => {
            const formatter = new OutputFormatter();
            expect(
                formatter.format('Hello <info>Worl<error>d</info>!</error>')
            ).to.be.equal(
                'Hello \u001b[32mWorl\u001b[39m\u001b[37;41md\u001b[39;49m\u001b[32m!\u001b[39m'
            );
        });

        it('Should create style if needed', () => {
            const formatter = new OutputFormatter();
            expect(
                formatter.format(
                    'H<bg=red;options=bold>ell</><href=https://www.theo-net.org>o</> <fg=#00F;options=bold,underline>World!</>'
                )
            ).to.be.equal(
                'H\u001b[41;1mell\u001b[49;22m\u001b]8;;https://www.theo-net.org\u001b\\o\u001b]8;;\u001b\\ \u001b[34;1;4mWorld!\u001b[39;22;24m'
            );
        });

        it('Should close style with </>', () => {
            const formatter = new OutputFormatter();
            expect(
                formatter.format('Hello <info>Worl<error>d</>!</info>')
            ).to.be.equal(
                'Hello \u001b[32mWorl\u001b[39m\u001b[37;41md\u001b[39;49m\u001b[32m!\u001b[39m'
            );
        });

        it('Should apply previous style if not closed', () => {
            const formatter = new OutputFormatter();
            expect(
                formatter.format('Hello <info>Worl<error>d</error>!')
            ).to.be.equal(
                'Hello \u001b[32mWorl\u001b[39m\u001b[37;41md\u001b[39;49m\u001b[32m!\u001b[39m'
            );
            expect(formatter.format(' is an info</info>')).to.be.equal(
                '\u001b[32m is an info\u001b[39m'
            );
        });

        it('Should work with multilines', () => {
            const formatter = new OutputFormatter();
            expect(formatter.format('Hello Wor\nld!')).to.be.equal(
                'Hello Wor\nld!'
            );
            expect(
                formatter.format('Hello <info>Wor\nl<error>d</error>!</info>')
            ).to.be.equal(
                'Hello \u001b[32mWor\nl\u001b[39m\u001b[37;41md\u001b[39;49m\u001b[32m!\u001b[39m'
            );
        });

        it('Should convert escaped char \\< and \\>', () => {
            const formatter = new OutputFormatter();
            expect(
                formatter.format(
                    'He<info>l\\<lo\\></info> in \\<info\\>\\</\\>'
                )
            ).to.be.equal('He\u001b[32ml<lo>\u001b[39m in <info></>');
        });

        it('Should work with true color', () => {
            const formatter = new OutputFormatter(true, {}, true);
            expect(
                formatter.format('<fg=#123456;bg=#dba562>triple</>')
            ).to.be.equal(
                '\u001b[38;2;18;52;86;48;2;219;165;98mtriple\u001b[39;49m'
            );
        });

        it('Should not apply style if not decorated', () => {
            const formatter = new OutputFormatter(false);
            expect(
                formatter.format('<error>foo<options=bold>b</>ar \\<error></>')
            ).to.be.equal('foobar <error>');
        });

        it('Should not apply style if set to not decorated', () => {
            const formatter = new OutputFormatter(false);
            expect(
                formatter.format('<error>foo<options=bold>b</>ar \\<error></>')
            ).to.be.equal('foobar <error>');
        });
    });

    describe('wrap', () => {
        it('Should wrap a text', () => {
            const formatter = new OutputFormatter(true, {}, true);
            expect(
                formatter.formatAndWrap('a long text to wrap', 6)
            ).to.be.equal('a long\ntext t\no wrap');
        });

        it('Should wrap a multiline text (length less than width)', () => {
            const formatter = new OutputFormatter(true, {}, true);
            expect(
                formatter.formatAndWrap('a long text\nto wrap', 15)
            ).to.be.equal('a long text\nto wrap');
        });

        it('Should wrap a multiline text (length more than width)', () => {
            const formatter = new OutputFormatter(true, {}, true);
            expect(
                formatter.formatAndWrap('a long text\nto wrap', 6)
            ).to.be.equal('a long\ntext\nto wra\np');
        });

        it('Should wrap a stylised text', () => {
            const formatter = new OutputFormatter(true, {}, true);
            expect(
                formatter.formatAndWrap('a long <error>text to</error> wrap', 6)
            ).to.be.equal('a long\n\u001b[37;41mtext t\no\u001b[39;49m wrap');
        });

        it('Should trim a wraped', () => {
            const formatter = new OutputFormatter(true, {}, true);
            expect(
                formatter.formatAndWrap(' a long text \n to wrap ', 15)
            ).to.be.equal('a long text \nto wrap ');
        });
    });

    describe('prefix', () => {
        it('Should prefix all line', () => {
            const formatter = new OutputFormatter(true, {}, true);
            expect(
                formatter.formatAndWrap('a long text multiline', 6, '&')
            ).to.be.equal('&a lon\n&g tex\n&t mul\n&tilin\n&e');
        });

        it('Should prefix first line', () => {
            const formatter = new OutputFormatter(true, {}, true);
            expect(
                formatter.formatAndWrap('a long text multiline', 6, {
                    first: '&',
                })
            ).to.be.equal('&a lon\ng text\nmultil\nine');
        });

        it('Should prefix other lines', () => {
            const formatter = new OutputFormatter(true, {}, true);
            expect(
                formatter.formatAndWrap('a long text multiline', 6, {
                    other: '"',
                })
            ).to.be.equal('a long\n"text \n"multi\n"line');
        });

        it('Should prefix first and other lines', () => {
            const formatter = new OutputFormatter(true, {}, true);
            expect(
                formatter.formatAndWrap('a long text multiline', 6, {
                    first: '*',
                    other: '&',
                })
            ).to.be.equal('*a lon\n&g tex\n&t mul\n&tilin\n&e');
        });

        it('Should prefix non wraped text', () => {
            const formatter = new OutputFormatter(true, {}, true);
            expect(
                formatter.formatAndWrap('a long text \n multiline', 0, {
                    first: '-',
                    other: '*',
                })
            ).to.be.equal('-a long text \n*multiline');
        });
    });

    describe('escape', () => {
        it('Should escape "<" and ">" special chars in given text', () => {
            expect(
                OutputFormatter.escape('Hello <error>world</>!')
            ).to.be.equal('Hello \\<error\\>world\\</\\>!');
        });
    });

    describe('stylizeLines', () => {
        it('Should not do nothing for single line', () => {
            expect(
                OutputFormatter.stylizeLines(['Hello World!'])
            ).to.be.deep.equal(['Hello World!']);
            expect(
                OutputFormatter.stylizeLines(['<error>Hello</error> World!'])
            ).to.be.deep.equal(['<error>Hello</> World!']);
            expect(
                OutputFormatter.stylizeLines(['Hello <error>World!</error>'])
            ).to.be.deep.equal(['Hello <error>World!</>']);
            expect(
                OutputFormatter.stylizeLines(['<bg=red>Hello World!</>'])
            ).to.be.deep.equal(['<bg=red>Hello World!</>']);
            expect(
                OutputFormatter.stylizeLines([
                    'H<red>e<bold>ll</bold>o Wor</red>ld!',
                ])
            ).to.be.deep.equal(['H<red>e<bold>ll</>o Wor</>ld!']);
        });

        it('Should continue stylization for new lines', () => {
            expect(
                OutputFormatter.stylizeLines(['<error>Hello', 'World!</error>'])
            ).to.be.deep.equal(['<error>Hello</>', '<error>World!</>']);
        });

        it('Should work with multiple stylization', () => {
            expect(
                OutputFormatter.stylizeLines([
                    'H<error>el<bold>lo',
                    'Wo</bold>rld</error>!',
                ])
            ).to.be.deep.equal([
                'H<error>el<bold>lo</></>',
                '<error><bold>Wo</>rld</>!',
            ]);
        });

        it('Should work with escaping', () => {
            expect(
                OutputFormatter.stylizeLines([
                    'H\\<error>ello',
                    'World\\</error>!',
                ])
            ).to.be.deep.equal(['H\\<error>ello', 'World\\</error>!']);
        });

        it('Should work with line without style', () => {
            expect(
                OutputFormatter.stylizeLines([
                    '<red>Hello',
                    'how',
                    'are',
                    'you?</red>',
                ])
            ).to.be.deep.equal([
                '<red>Hello</>',
                '<red>how</>',
                '<red>are</>',
                '<red>you?</>',
            ]);
        });
    });
});
