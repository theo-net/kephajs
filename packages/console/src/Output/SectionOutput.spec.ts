/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import Sinon from 'sinon';
import { WriteStream } from 'tty';

import { Output } from './Output';
import { OutputFormatter } from './OutputFormatter';
import { SectionOutput } from './SectionOutput';

describe('@kephajs/console/Output/SectionOutput', () => {
    let section: SectionOutput;
    let sections: SectionOutput[];
    let stubWrite: Sinon.SinonStub;
    let stream: WriteStream;

    beforeEach(() => {
        stream = new WriteStream(5);
        sections = [];
        section = new SectionOutput(
            stream,
            sections,
            Output.VERBOSITY.NORMAL,
            new OutputFormatter()
        );
        stubWrite = Sinon.stub(stream, 'write');
    });

    afterEach(() => {
        stubWrite.restore();
    });

    it('Should extend Output', () => {
        expect(section).to.be.instanceOf(Output);
    });

    describe('add and get content', () => {
        it('Should add content', () => {
            section.addContent('foo');
            section.addContent('bar');
            section.addContent('foobar');
            expect(section.getContent()).to.be.equal('foo\nbar\nfoobar\n');
            expect(
                Object.getOwnPropertyDescriptor(section, '_lines')?.value
            ).to.be.equal(3);
        });

        it('Should add multiline content', () => {
            section.addContent('foo\nbar');
            section.addContent('FOOBAR');
            expect(section.getContent()).to.be.equal('foo\nbar\nFOOBAR\n');
            expect(
                Object.getOwnPropertyDescriptor(section, '_lines')?.value
            ).to.be.equal(3);
        });

        it('Should add large content (than terminal width)', () => {
            const backup = process.stdout.columns;
            process.stdout.columns = 3;

            section.addContent('foobar');
            expect(section.getContent()).to.be.equal('foobar\n');
            expect(
                Object.getOwnPropertyDescriptor(section, '_lines')?.value
            ).to.be.equal(2);

            process.stdout.columns = backup;
        });
    });

    describe('write', () => {
        it('Should add newline at the end', () => {
            section.writeln('hello');
            expect(stubWrite.getCalls()[0].args[0]).to.be.equal('hello\n');
        });

        it('Should keep the next section', () => {
            const section2 = new SectionOutput(
                stream,
                sections,
                Output.VERBOSITY.NORMAL,
                new OutputFormatter()
            );
            section.writeln('foo\nbar');
            section2.writeln('hello');
            section.writeln('FOOBAR');
            expect(section.getContent()).to.be.deep.equal('foo\nbar\nFOOBAR\n');
            expect(stubWrite.getCalls()[0].args[0]).to.be.equal('foo\nbar\n'); // section.writeln()
            expect(stubWrite.getCalls()[1].args[0]).to.be.equal(''); // no erased content to print with writeln
            expect(stubWrite.getCalls()[2].args[0]).to.be.equal('hello\n'); // section2.writeln()
            expect(stubWrite.getCalls()[3].args[0]).to.be.equal(''); // no erased content to print with writeln
            expect(stubWrite.getCalls()[4].args[0]).to.be.equal('\u001b[1A'); // moveUp(1)
            expect(stubWrite.getCalls()[5].args[0]).to.be.equal('\u001b[0J'); // clearOutput()
            expect(stubWrite.getCalls()[6].args[0]).to.be.equal('FOOBAR\n'); // section.writeln()
            expect(stubWrite.getCalls()[7].args[0]).to.be.equal('hello\n'); // erased content to print with writeln
        });
    });

    describe('clear', () => {
        it('Should clear the section', () => {
            section.writeln('foo\nbar\nFOOBAR');
            section.clear();
            expect(section.getContent()).to.be.deep.equal('');
            expect(stubWrite.getCalls()[0].args[0]).to.be.equal(
                'foo\nbar\nFOOBAR\n'
            ); // the section.writeln
            expect(stubWrite.getCalls()[1].args[0]).to.be.equal(''); // no erased content to print with writeln
            expect(stubWrite.getCalls()[2].args[0]).to.be.equal('\u001b[3A'); // moveUp(3)
            expect(stubWrite.getCalls()[3].args[0]).to.be.equal('\u001b[0J'); // clearOutput()
            expect(stubWrite.getCalls()[4].args[0]).to.be.equal(''); // no erased content to print with clear
        });

        it('Should clear just the current section', () => {
            const section2 = new SectionOutput(
                stream,
                sections,
                Output.VERBOSITY.NORMAL,
                new OutputFormatter()
            );
            section.writeln('foo\nbar\nFOOBAR');
            section2.writeln('hello');
            section.clear();
            expect(section.getContent()).to.be.deep.equal('');
            expect(stubWrite.getCalls()[0].args[0]).to.be.equal(
                'foo\nbar\nFOOBAR\n'
            ); // section.writeln()
            expect(stubWrite.getCalls()[1].args[0]).to.be.equal(''); // no erased content to print with writeln
            expect(stubWrite.getCalls()[2].args[0]).to.be.equal('hello\n'); // section2.writeln()
            expect(stubWrite.getCalls()[3].args[0]).to.be.equal(''); // no erased content to print with writeln
            expect(stubWrite.getCalls()[4].args[0]).to.be.equal('\u001b[4A'); // moveUp(4)
            expect(stubWrite.getCalls()[5].args[0]).to.be.equal('\u001b[0J'); // clearOutput()
            expect(stubWrite.getCalls()[6].args[0]).to.be.equal('hello\n');
        });

        it('Should clear some lines', () => {
            section.writeln('foo\nbar\nFOOBAR');
            section.clear(2);
            expect(section.getContent()).to.be.deep.equal('foo\n');
            expect(stubWrite.getCalls()[0].args[0]).to.be.equal(
                'foo\nbar\nFOOBAR\n'
            ); // the section.writeln
            expect(stubWrite.getCalls()[1].args[0]).to.be.equal(''); // no erased content to print with writeln
            expect(stubWrite.getCalls()[2].args[0]).to.be.equal('\u001b[2A'); // moveUp(2)
            expect(stubWrite.getCalls()[3].args[0]).to.be.equal('\u001b[0J'); // clearOutput()
            expect(stubWrite.getCalls()[4].args[0]).to.be.equal(''); // no erased content to print with clear
        });
    });

    describe('overwrite', () => {
        it('Should clear then write message', () => {
            const stubClear = Sinon.stub(section, 'clear');
            const stubWriteln = Sinon.stub(section, 'writeln');
            section.write('foo\n\bar');
            section.overwrite('FOOBAR');
            expect(stubClear.getCalls()[0].args[0]).to.be.equal(undefined);
            expect(stubWriteln.getCalls()[0].args[0]).to.be.equal('FOOBAR');
            stubClear.restore();
            stubWriteln.restore();
        });
    });
});
