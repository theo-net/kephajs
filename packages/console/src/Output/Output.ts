/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { WriteStream } from 'tty';

import { OutputFormatter } from './OutputFormatter';

export enum ColorDepth {
    TWO = 1, // 2
    C16 = 4, // 16
    C256 = 8, // 256
    C16M = 24, // 16,777,216 colors supported.
}

export class Output {
    public static readonly VERBOSITY = {
        QUIET: 16,
        NORMAL: 32,
        VERBOSE: 64,
        VERY_VERBOSE: 128,
        DEBUG: 256,
    };

    public static readonly OUTPUT = {
        NORMAL: 1,
        RAW: 2,
        PLAIN: 4,
    };

    protected _sectionOutputs: SectionOutput[] = [];

    protected _formatter: OutputFormatter | undefined;

    protected _stream: WriteStream | undefined;

    constructor(protected _verbosity: number = Output.VERBOSITY.NORMAL) {}

    /**
     * Set stream output
     */
    setStream(stream: WriteStream) {
        this._stream = stream;
    }

    /**
     * Get stream output
     */
    getStream() {
        return this._stream;
    }

    /**
     * Get formatter
     */
    getFormatter() {
        return this._formatter;
    }

    /**
     * Set formatter
     */
    setFormatter(formatter: OutputFormatter) {
        this._formatter = formatter;
    }

    /**
     * Get verbosity
     */
    getVerbosity() {
        return this._verbosity;
    }

    setVerbosity(verbosity: number) {
        this._verbosity = verbosity;
    }

    isQuiet() {
        return this._verbosity == Output.VERBOSITY.QUIET;
    }

    isVerbose() {
        return this._verbosity == Output.VERBOSITY.VERBOSE;
    }

    isVeryVerbose() {
        return this._verbosity == Output.VERBOSITY.VERY_VERBOSE;
    }

    isDebug() {
        return this._verbosity == Output.VERBOSITY.DEBUG;
    }

    /**
     * Write something to the output
     */
    write(
        messages: string[] | string,
        newline = false,
        options: number = Output.VERBOSITY.NORMAL + Output.OUTPUT.NORMAL
    ) {
        if (!Array.isArray(messages)) {
            messages = [messages];
        }

        const types =
            Output.OUTPUT.NORMAL | Output.OUTPUT.RAW | Output.OUTPUT.PLAIN;
        const type = types & options ? types & options : Output.OUTPUT.NORMAL;

        const verbosities =
            Output.VERBOSITY.QUIET |
            Output.VERBOSITY.NORMAL |
            Output.VERBOSITY.VERBOSE |
            Output.VERBOSITY.VERY_VERBOSE |
            Output.VERBOSITY.DEBUG;
        const verbosity =
            verbosities & options
                ? verbosities & options
                : Output.VERBOSITY.NORMAL;

        if (verbosity > this.getVerbosity()) {
            return;
        }

        messages.forEach(message => {
            switch (type) {
                case Output.OUTPUT.NORMAL:
                    if (this._formatter) {
                        message = this._formatter.format(message);
                    }
                    break;
                case Output.OUTPUT.RAW:
                    break;
                case Output.OUTPUT.PLAIN:
                    if (this._formatter) {
                        message = this._formatter.format(message, false);
                    }
                    break;
            }

            this._doWrite(message, newline);
        });
    }

    /**
     * Write with a \n at the end
     */
    writeln(
        messages: string[] | string,
        options: number = Output.VERBOSITY.NORMAL + Output.OUTPUT.NORMAL
    ) {
        this.write(messages, true, options);
    }

    /**
     * Return a new section
     */
    section() {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        return new SectionOutput(
            this._stream,
            this._sectionOutputs,
            this._verbosity,
            this._formatter
        );
    }

    /**
     * Use this to determine what colors the terminal supports.
     */
    getColorDepth(): ColorDepth {
        const depth =
            this._stream && this._stream.getColorDepth
                ? this._stream.getColorDepth()
                : 2;

        switch (depth) {
            case 24:
                return ColorDepth.C16M;
            case 8:
                return ColorDepth.C256;
            case 4:
                return ColorDepth.C16;

            default:
                return ColorDepth.TWO;
        }
    }

    /**
     * Returns true if the stream supports colorization.
     */
    hasColorSupport() {
        return this.getColorDepth() !== ColorDepth.TWO;
    }

    protected _doWrite(message: string, newline = false) {
        if (newline) {
            message += '\n';
        }
        this._stream?.write(message);
    }
}

import { SectionOutput } from './SectionOutput';
