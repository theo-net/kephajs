/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Color } from './Color';

describe('@kephajs/console/Output/Color', () => {
    describe('basic foreground and background colors', () => {
        const colors = [
            'black',
            'red',
            'green',
            'yellow',
            'blue',
            'magenta',
            'cyan',
            'white',
            'default',
            'gray',
            'bright-red',
            'bright-green',
            'bright-yellow',
            'bright-blue',
            'bright-magenta',
            'bright-cyan',
            'bright-white',
        ];
        const colorCodeForeground: { [key: string]: number } = {
            black: 30,
            red: 31,
            green: 32,
            yellow: 33,
            blue: 34,
            magenta: 35,
            cyan: 36,
            white: 37,
            default: 39,
            gray: 90,
            'bright-red': 91,
            'bright-green': 92,
            'bright-yellow': 93,
            'bright-blue': 94,
            'bright-magenta': 95,
            'bright-cyan': 96,
            'bright-white': 97,
        };
        const colorCodeBackground: { [key: string]: number } = {
            black: 40,
            red: 41,
            green: 42,
            yellow: 43,
            blue: 44,
            magenta: 45,
            cyan: 46,
            white: 47,
            default: 49,
            gray: 100,
            'bright-red': 101,
            'bright-green': 102,
            'bright-yellow': 103,
            'bright-blue': 104,
            'bright-magenta': 105,
            'bright-cyan': 106,
            'bright-white': 107,
        };
        colors.forEach(foreground => {
            colors.forEach(background => {
                it(`Foreground: ${foreground}; Background: ${background}`, () => {
                    const col = new Color(foreground, background);
                    const styled = col.apply(`${foreground}/${background}`);
                    expect(styled).to.be.equal(
                        `\u001b[${colorCodeForeground[foreground]};${colorCodeBackground[background]}m${foreground}/${background}\u001b[39;49m`
                    );
                });
            });
        });

        it('Should work with just foreground', () => {
            const col = new Color('blue');
            const styled = col.apply('test');
            expect(styled).to.be.equal(
                `\u001b[${colorCodeForeground.blue}mtest\u001b[39m`
            );
        });

        it('Should work with just background', () => {
            const col = new Color('', 'blue');
            const styled = col.apply('test');
            expect(styled).to.be.equal(
                `\u001b[${colorCodeBackground.blue}mtest\u001b[49m`
            );
        });
    });

    describe('options', () => {
        const optionCode: { [key: string]: { set: number; unset: number } } = {
            bold: { set: 1, unset: 22 },
            underline: { set: 4, unset: 24 },
            blink: { set: 5, unset: 25 },
            reverse: { set: 7, unset: 27 },
            conceal: { set: 8, unset: 28 },
        };
        const options = ['bold', 'underline', 'blink', 'reverse', 'conceal'];
        options.forEach(option => {
            it(`Should add option ${option}`, () => {
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                const col = new Color('', '', [option as any]);
                const styled = col.apply(option);
                expect(styled).to.be.equal(
                    `\u001b[${optionCode[option].set}m${option}\u001b[${optionCode[option].unset}m`
                );
            });
        });

        it('Should combine multiple options', () => {
            const col = new Color('', '', ['underline', 'bold']);
            const styled = col.apply('multiple');
            expect(styled).to.be.equal(
                `\u001b[${optionCode.underline.set};${optionCode.bold.set}mmultiple\u001b[${optionCode.underline.unset};${optionCode.bold.unset}m`
            );
        });

        it('Should combine option, foreground and background', () => {
            const col = new Color('blue', 'yellow', ['underline', 'bold']);
            const styled = col.apply('combine');
            expect(styled).to.be.equal(
                `\u001b[34;43;${optionCode.underline.set};${optionCode.bold.set}mcombine\u001b[39;49;${optionCode.underline.unset};${optionCode.bold.unset}m`
            );
        });
    });

    describe('color hexa', () => {
        it('Should parse color #xxx', () => {
            const col = new Color('#123', '#ABC', [], true);
            const styled = col.apply('triple');
            expect(styled).to.be.equal(
                `\u001b[38;2;17;34;51;48;2;170;187;204mtriple\u001b[39;49m`
            );
        });

        it('Should parse color #xxxxxx', () => {
            const col = new Color('#123456', '#dba562', [], true);
            const styled = col.apply('triple');
            expect(styled).to.be.equal(
                `\u001b[38;2;18;52;86;48;2;219;165;98mtriple\u001b[39;49m`
            );
        });

        it('Should degrade colors if term not truecolor', () => {
            const col = new Color('#123456', '#dba562', [], false);
            const styled = col.apply('triple');
            expect(styled).to.be.equal(`\u001b[30;43mtriple\u001b[39;49m`);
        });
    });
});
