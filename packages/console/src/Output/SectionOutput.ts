/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { WriteStream } from 'tty';

import { Output } from './Output';
import { OutputFormatter } from './OutputFormatter';
import { Terminal } from '../Terminal/Terminal';
import { Cursor } from '../Terminal/Cursor';

export class SectionOutput extends Output {
    private _content: string[] = [];

    private _cursor: Cursor;

    private _lines = 0;

    private _terminal: Terminal;

    constructor(
        protected _stream: WriteStream | undefined,
        protected _sections: SectionOutput[],
        protected _verbosity: number,
        protected _formatter: OutputFormatter | undefined
    ) {
        super(_verbosity);

        const output = new Output(_verbosity);
        output.setStream(_stream as WriteStream);
        this._cursor = new Cursor(output);
        this._sections.unshift(this);
        this._terminal = new Terminal();
    }

    getContent() {
        return this._content.join('');
    }

    addContent(input: string) {
        input.split('\n').forEach(lineContent => {
            const addLines = Math.ceil(
                this._getDisplayLength(lineContent) / this._terminal.getWidth()
            );
            this._lines += addLines ? addLines : 1;
            this._content.push(lineContent);
            this._content.push('\n');
        });
    }

    clear(lines = 0) {
        if (!this._content.length) {
            return;
        }

        if (lines) {
            this._content.splice(-(lines * 2)); // Multiply lines by 2 to cater for each new line added between content
        } else {
            lines = this._lines;
            this._content = [];
        }

        this._lines -= lines;

        super._doWrite(this._popStreamContentUntilCurrentSection(lines), false);
    }

    overwrite(message: string | string[]) {
        this.clear();
        this.writeln(message);
    }

    protected _doWrite(message: string) {
        const erasedContent = this._popStreamContentUntilCurrentSection();

        this.addContent(message);

        super._doWrite(message, true);
        super._doWrite(erasedContent, false);
    }

    /**
     * At initial stage, cursor is at the end of stream output. This method makes cursor crawl upwards until it
     * hits current section. Then it erases content it crawled through. Optionally, it erases part of current
     * section too.
     */
    private _popStreamContentUntilCurrentSection(
        numberOfLinesToClearFromCurrentSection = 0
    ) {
        let numberOfLinesToClear = numberOfLinesToClearFromCurrentSection;
        const erasedContent: string[] = [];

        this._sections.every(section => {
            if (section === this) {
                return false;
            }

            numberOfLinesToClear += section._lines;
            erasedContent.push(section.getContent());
            return true;
        });

        if (numberOfLinesToClear > 0) {
            this._cursor.moveUp(numberOfLinesToClear).clearOutput();
        }

        return erasedContent.reverse().join('');
    }

    private _getDisplayLength(text: string) {
        return this._removeDecoration(text.replace('\t', '        ')).length;
    }

    private _removeDecoration(text: string) {
        if (this._formatter) {
            const isDecorated = this._formatter.isDecorated();
            this._formatter.setDecorated(false);
            // remove <...> formatting
            text = this._formatter.format(text ?? '');
            // remove already formatted characters
            text = text.replace('\u001b', '\\033').replace(/\\033\[[^m]*m/, '');
            this._formatter.setDecorated(isDecorated);
        }

        return text;
    }
}
