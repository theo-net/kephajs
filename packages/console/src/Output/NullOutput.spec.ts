/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import { WriteStream } from 'tty';

import { NullOutput } from './NullOutput';
import { Output } from './Output';

describe('@kephajs/console/Output/NullOutput', () => {
    let output: NullOutput;

    beforeEach(() => {
        output = new NullOutput();
    });

    describe('stream', () => {
        it('Should not have stream', () => {
            expect(output.getStream()).to.be.equal(undefined);
        });

        it('Should throw TypeError if try to set stream', () => {
            try {
                (output as Output).setStream(new WriteStream(5));
            } catch (error) {
                expect(error).to.be.instanceOf(TypeError);
                expect((error as TypeError).message).to.be.equal(
                    'You cannot set stream to NullOutput'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });
});
