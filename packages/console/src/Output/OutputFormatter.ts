/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { AvailableOptions } from './Color';
import { OutputFormatterStyle } from './OutputFormatterStyle';
import { OutputFormatterStyleStack } from './OutputFormatterStyleStack';

type Prefix = { first?: string; other?: string; isFirst?: boolean };
type WrapInfo = { width: number; currentLineLength: number };

export class OutputFormatter {
    private _styles: { [key: string]: OutputFormatterStyle } = {};

    private _styleStack: OutputFormatterStyleStack;

    /**
     * Initializes console output formatter.
     */
    constructor(
        private _decorated = true,
        styles: { [key: string]: OutputFormatterStyle } = {},
        private _trueColor = false
    ) {
        this.setStyle(
            'error',
            new OutputFormatterStyle('white', 'red', [], this._trueColor)
        );
        this.setStyle(
            'info',
            new OutputFormatterStyle('green', '', [], this._trueColor)
        );
        this.setStyle(
            'comment',
            new OutputFormatterStyle('yellow', '', [], this._trueColor)
        );
        this.setStyle(
            'question',
            new OutputFormatterStyle('black', 'cyan', [], this._trueColor)
        );

        Object.getOwnPropertyNames(styles).forEach(name => {
            this.setStyle(name, styles[name]);
        });

        this._styleStack = new OutputFormatterStyleStack();
    }

    setDecorated(decorated: boolean) {
        this._decorated = decorated;
    }

    isDecorated(decorated = true) {
        return this._decorated && decorated;
    }

    isTrueColor() {
        return this._trueColor;
    }

    setStyle(name: string, style: OutputFormatterStyle) {
        this._styles[name.toLowerCase()] = style;
    }

    hasStyle(name: string) {
        return this._styles[name.toLowerCase()] !== undefined;
    }

    getStyle(name: string) {
        if (!this.hasStyle(name)) {
            throw new TypeError(`Undefined style: "${name}".`);
        }

        return this._styles[name.toLowerCase()];
    }

    format(message: null | string, decorated = true): string {
        return this.formatAndWrap(message, 0, '', decorated);
    }

    formatAndWrap(
        message: string | null,
        width: number,
        prefix: string | Prefix = '',
        decorated = true
    ) {
        if (null === message) {
            return '';
        }

        // set prefix
        if (typeof prefix === 'string') {
            prefix = {
                first: prefix,
                other: prefix,
            };
        }
        prefix = {
            ...{
                first: '',
                other: '',
                isFirst: true,
            },
            ...prefix,
        };

        let offset = 0;
        let output = '';
        const wrapInfo: WrapInfo = {
            width,
            currentLineLength: 0,
        };

        const matches = message.matchAll(/<(\/?)([^<>]*)>/gi);
        for (const match of matches) {
            // 0: captured
            // 1: '/' or ''
            // 2: content of tag

            const pos = match.index ? match.index : offset;

            // if tag escaped
            if (pos > 0 && message[pos - 1] === '\\') {
                continue;
            }

            // add the text up to the next tag
            output += this._applyCurrentStyle(
                message.substring(offset, pos),
                wrapInfo,
                prefix,
                decorated
            );

            offset = pos + match[0].length;

            // opening or closing tag
            if (match[1] === '') {
                const style = this._createStyleFromString(match[2]);
                if (style !== null) {
                    this._styleStack.push(style);
                }
            } else if (match[1] === '/') {
                this._styleStack.pop();
            }
        }

        output += this._applyCurrentStyle(
            message.substring(offset),
            wrapInfo,
            prefix,
            decorated
        );

        return output.replace(/\\</g, '<').replace(/\\>/g, '>');
    }

    static escape(input: string) {
        return input.replace(/</g, '\\<').replace(/>/g, '\\>');
    }

    static stylizeLines(input: string[]) {
        const currentStyle: string[] = [];

        input.forEach((line, index) => {
            input[index] = '';
            if (currentStyle.length > 0) {
                input[index] = currentStyle.reduce((accumulator, style) => {
                    return accumulator + '<' + style + '>';
                }, '');
            }

            let offset = 0;
            const matches = line.matchAll(/<(\/?)([^<>]*)>/gi);
            for (const match of matches) {
                // 0: captured
                // 1: '/' or ''
                // 2: content of tag

                const pos = match.index ? match.index : offset;

                // if tag escaped
                if (pos > 0 && line[pos - 1] === '\\') continue;

                // add the text up to the next tag
                input[index] += line.substring(offset, pos);

                offset = pos + match[0].length;

                // opening or closing tag
                if (match[1] === '') {
                    currentStyle.push(match[2]);
                    input[index] += '<' + match[2] + '>';
                } else if (match[1] === '/') {
                    input[index] += '</>';
                    currentStyle.pop();
                }
            }

            input[index] += line.substring(offset);

            if (currentStyle.length > 0)
                input[index] += '</>'.repeat(currentStyle.length);
        });

        return input;
    }

    /**
     * Tries to create new style instance from string.
     */
    private _createStyleFromString(
        stringStyle: string
    ): OutputFormatterStyle | null {
        if (this._styles[stringStyle]) {
            return this._styles[stringStyle];
        }

        const style = new OutputFormatterStyle(null, null, [], this._trueColor);
        const matches = stringStyle.matchAll(/([^=]+)=([^;]+)(;|$)/g);
        for (const match of matches) {
            // 0: captured
            // 1: fg, bg, options
            // 2: content
            match[1] = match[1].toLowerCase();
            if ('fg' == match[1]) {
                style.setForeground(match[2].toLowerCase());
            } else if ('bg' == match[1]) {
                style.setBackground(match[2].toLowerCase());
            } else if ('href' === match[1]) {
                style.setHref(match[2]);
            } else if ('options' === match[1]) {
                match[2]
                    .toLowerCase()
                    .split(',')
                    .forEach(option => {
                        style.setOption(option.trim() as AvailableOptions);
                    });
            }
        }

        return style;
    }

    /**
     * Applies current style from stack to text, if must be applied.
     */
    private _applyCurrentStyle(
        text: string,
        wrapInfo: WrapInfo,
        prefix: Prefix,
        decorated = true
    ): string {
        if ('' === text) {
            return '';
        }

        if (!wrapInfo.width) {
            return this.isDecorated(decorated)
                ? this._styleStack
                      .getCurrent()
                      .apply(this._prefix(text, prefix))
                : this._prefix(text, prefix);
        }

        const linesOutput: string[] = [];
        let first = true;
        text.split('\n').forEach(line => {
            if (first) {
                first = false;
            } else {
                wrapInfo.currentLineLength = 0;
            }
            this._wrapLine(wrapInfo, line, linesOutput, prefix);
        });

        return this.isDecorated(decorated)
            ? this._styleStack.getCurrent().apply(linesOutput.join('\n'))
            : linesOutput.join('\n');
    }

    private _wrapLine(
        wrapInfo: WrapInfo,
        line: string,
        linesOutput: string[],
        prefix: Prefix
    ) {
        if (wrapInfo.currentLineLength === 0) {
            line = line.trimStart();
        }

        if (prefix.isFirst) {
            line = prefix.first + line;
            prefix.isFirst = false;
        } else {
            line = prefix.other + line;
        }

        if (line.length <= wrapInfo.width - wrapInfo.currentLineLength) {
            linesOutput.push(line);
            wrapInfo.currentLineLength += line.length;
        } else {
            linesOutput.push(
                line.substring(0, wrapInfo.width - wrapInfo.currentLineLength)
            );
            wrapInfo.currentLineLength = 0;
            this._wrapLine(
                wrapInfo,
                line.substring(wrapInfo.width - wrapInfo.currentLineLength),
                linesOutput,
                prefix
            );
        }
    }

    private _prefix(text: string, prefix: Prefix) {
        if (prefix.first || prefix.other) {
            let output = '';
            text.split('\n').forEach(line => {
                if (prefix.isFirst) {
                    output += prefix.first + line.trimStart();
                    prefix.isFirst = false;
                } else {
                    output += '\n' + prefix.other + line.trimStart();
                }
            });
            return output;
        }

        return text;
    }
}
