/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { OutputFormatterStyle } from './OutputFormatterStyle';

describe('@kephajs/console/Output/OutputFormatterStyle', () => {
    describe('Color', () => {
        it('Should define colors and options', () => {
            const format = new OutputFormatterStyle('blue', 'yellow', [
                'underline',
                'bold',
            ]);
            const styled = format.apply('combine');
            expect(styled).to.be.equal(
                `\u001b[34;43;4;1mcombine\u001b[39;49;24;22m`
            );
        });

        it('Should set foreground', () => {
            const format = new OutputFormatterStyle('blue', 'yellow');
            format.setForeground('black');
            const styled = format.apply('combine');
            expect(styled).to.be.equal(`\u001b[30;43mcombine\u001b[39;49m`);
        });

        it('Should set background', () => {
            const format = new OutputFormatterStyle('blue', 'yellow');
            format.setBackground('black');
            const styled = format.apply('combine');
            expect(styled).to.be.equal(`\u001b[34;40mcombine\u001b[39;49m`);
        });

        it('Should set option', () => {
            const format = new OutputFormatterStyle('blue', 'yellow', [
                'underline',
            ]);
            format.setOption('bold');
            const styled = format.apply('combine');
            expect(styled).to.be.equal(
                `\u001b[34;43;4;1mcombine\u001b[39;49;24;22m`
            );
        });

        it('Should unset option', () => {
            const format = new OutputFormatterStyle('blue', 'yellow', [
                'underline',
                'bold',
            ]);
            format.unsetOption('underline');
            const styled = format.apply('combine');
            expect(styled).to.be.equal(
                `\u001b[34;43;1mcombine\u001b[39;49;22m`
            );
        });

        it('Should set options', () => {
            const format = new OutputFormatterStyle('blue', 'yellow', [
                'blink',
            ]);
            format.setOptions(['underline', 'bold']);
            const styled = format.apply('combine');
            expect(styled).to.be.equal(
                `\u001b[34;43;4;1mcombine\u001b[39;49;24;22m`
            );
        });
    });

    describe('Href', () => {
        it('Should set href', () => {
            const format = new OutputFormatterStyle('blue', 'yellow', [
                'underline',
                'bold',
            ]);
            format.setHref('https://www.theo-net.org');
            const styled = format.apply('link');
            expect(styled).to.be.equal(
                `\u001b[34;43;4;1m\u001b]8;;https://www.theo-net.org\u001b\\link\u001b]8;;\u001b\\\u001b[39;49;24;22m`
            );
        });
    });
});
