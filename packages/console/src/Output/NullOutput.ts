/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Output } from './Output';

export class NullOutput extends Output {
    setStream() {
        throw new TypeError('You cannot set stream to NullOutput');
    }
}
