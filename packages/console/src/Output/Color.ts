/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

type Option = {
    set: number;
    unset: number;
};
type Options = Option[];

export type AvailableOptions =
    | 'bold'
    | 'underline'
    | 'blink'
    | 'reverse'
    | 'conceal';

export class Color {
    private COLORS: { [key: string]: number } = {
        black: 0,
        red: 1,
        green: 2,
        yellow: 3,
        blue: 4,
        magenta: 5,
        cyan: 6,
        white: 7,
        default: 9,
    };

    private BRIGHT_COLORS: { [key: string]: number } = {
        gray: 0,
        'bright-red': 1,
        'bright-green': 2,
        'bright-yellow': 3,
        'bright-blue': 4,
        'bright-magenta': 5,
        'bright-cyan': 6,
        'bright-white': 7,
    };

    private AVAILABLE_OPTIONS = {
        bold: { set: 1, unset: 22 },
        underline: { set: 4, unset: 24 },
        blink: { set: 5, unset: 25 },
        reverse: { set: 7, unset: 27 },
        conceal: { set: 8, unset: 28 },
    };

    private _foreground = '';

    private _background = '';

    private _options: Options = [];

    constructor(
        foreground = '',
        background = '',
        options: AvailableOptions[] = [],
        private _trueColor = false // ie color depth 16M
    ) {
        this._foreground = this._parseColor(foreground);
        this._background = this._parseColor(background, true);

        options.forEach(option => {
            if (this.AVAILABLE_OPTIONS[option] === undefined) {
                throw new TypeError(
                    `Invalid option specified: "${option}". Expected one of (${Object.getOwnPropertyNames(
                        this.AVAILABLE_OPTIONS
                    ).join(', ')}).`
                );
            }

            this._options.push(this.AVAILABLE_OPTIONS[option]);
        });
    }

    public apply(text: string): string {
        return this.set() + text + this.unset();
    }

    public set(): string {
        const setCodes: string[] = [];
        if ('' !== this._foreground) {
            setCodes.push(this._foreground);
        }
        if ('' !== this._background) {
            setCodes.push(this._background);
        }
        this._options.forEach(option => {
            setCodes.push(option.set + '');
        });
        if (0 === setCodes.length) {
            return '';
        }

        return `\u001b[${setCodes.join(';')}m`;
    }

    public unset(): string {
        const unsetCodes: string[] = [];
        if ('' !== this._foreground) {
            unsetCodes.push('39');
        }
        if ('' !== this._background) {
            unsetCodes.push('49');
        }
        this._options.forEach(option => {
            unsetCodes.push(option.unset + '');
        });
        if (0 === unsetCodes.length) {
            return '';
        }

        return `\u001b[${unsetCodes.join(';')}m`;
    }

    private _parseColor(color: string, background = false): string {
        if ('' === color) {
            return '';
        }

        if ('#' === color[0]) {
            color = color.substring(1);

            if (3 === color.length) {
                color =
                    color[0] +
                    color[0] +
                    color[1] +
                    color[1] +
                    color[2] +
                    color[2];
            }

            if (6 !== color.length) {
                throw new TypeError(`Invalid "${color}" color.`);
            }

            return (
                (background ? '4' : '3') +
                this._convertHexColorToAnsi(parseInt(color, 16))
            );
        }

        if (this.COLORS[color] !== undefined) {
            return (background ? '4' : '3') + this.COLORS[color];
        }

        if (this.BRIGHT_COLORS[color] !== undefined) {
            return (background ? '10' : '9') + this.BRIGHT_COLORS[color];
        }

        throw new TypeError(
            `Invalid "${color}" color; expected one of (${[
                ...Object.getOwnPropertyNames(this.COLORS),
                ...Object.getOwnPropertyNames(this.BRIGHT_COLORS),
            ].join(', ')}).`
        );
    }

    private _convertHexColorToAnsi(color: number): string {
        const r = (color >> 16) & 255;
        const g = (color >> 8) & 255;
        const b = color & 255;

        if (!this._trueColor) {
            return this._degradeHexColorToAnsi(r, g, b) + '';
        }

        return `8;2;${r};${g};${b}`;
    }

    private _degradeHexColorToAnsi(r: number, g: number, b: number): number {
        if (0 === Math.round(this._getSaturation(r, g, b) / 50)) {
            return 0;
        }

        return (
            (Math.round(b / 255) << 2) |
            (Math.round(g / 255) << 1) |
            Math.round(r / 255)
        );
    }

    private _getSaturation(r: number, g: number, b: number): number {
        r = r / 255;
        g = g / 255;
        b = b / 255;
        const v = Math.max(r, g, b);

        const diff = v - Math.min(r, g, b);

        if (0 === diff) {
            return 0;
        }

        return (diff * 100) / v;
    }
}
