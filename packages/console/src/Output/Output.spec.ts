/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import Sinon from 'sinon';
import { WriteStream } from 'tty';

import { ColorDepth, Output } from './Output';
import { OutputFormatter } from './OutputFormatter';
import { OutputFormatterStyle } from './OutputFormatterStyle';
import { SectionOutput } from './SectionOutput';

describe('@kephajs/console/Output/Output', () => {
    let output: Output;
    let stubWrite: Sinon.SinonStub;
    let stream: WriteStream;

    beforeEach(() => {
        output = new Output();
        stream = new WriteStream(5);
        output.setStream(stream);
        stubWrite = Sinon.stub(stream, 'write');
    });

    afterEach(() => {
        stubWrite.restore();
    });

    describe('setStream and getStream', () => {
        it('Should set and get a stream', () => {
            expect(output.getStream()).to.be.equal(stream);
        });
    });

    describe('write', () => {
        it('Should write the message to the stream', () => {
            output.write('Hello World!');
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                'Hello World!'
            );
        });

        it('Should accept array', () => {
            output.write(['Hello', ' World!']);
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal('Hello');
            expect(stubWrite.getCalls()[1].args[0]).to.be.deep.equal(' World!');
        });

        it('Should add new line', () => {
            output.write('Hello World!', true);
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                'Hello World!\n'
            );
        });
    });

    describe('writeln', () => {
        it('Should write with \\n at the end', () => {
            output.writeln('Hello World!');
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal(
                'Hello World!\n'
            );
        });

        it('Should write array as multiline', () => {
            output.writeln(['Hello', ' World!']);
            expect(stubWrite.getCalls()[0].args[0]).to.be.deep.equal('Hello\n');
            expect(stubWrite.getCalls()[1].args[0]).to.be.deep.equal(
                ' World!\n'
            );
        });
    });

    describe('getColorDepth', () => {
        const forceColor = process.env.FORCE_COLOR;
        const noColor = process.env.NO_COLOR;
        const nodeDisableColors = process.env.NODE_DISABLE_COLORS;

        it('Should return the current value', () => {
            if (process.stdout.getColorDepth) {
                expect(output.getColorDepth()).to.be.equal(
                    process.stdout.getColorDepth()
                );
            }
        });

        it('Should force to 2 by FORCE_COLOR env variable', () => {
            process.env.FORCE_COLOR = '0';
            expect(output.getColorDepth()).to.be.equal(ColorDepth.TWO);

            if (forceColor) {
                process.env.FORCE_COLOR = forceColor;
            } else {
                delete process.env.FORCE_COLOR;
            }
        });

        it('Should force to 2 by NO_COLOR env variable', () => {
            process.env.NO_COLOR = '1';
            expect(output.getColorDepth()).to.be.equal(ColorDepth.TWO);

            if (noColor) {
                process.env.NO_COLOR = noColor;
            } else {
                delete process.env.NO_COLOR;
            }
        });

        it('Should force to 2 by NODE_DISABLE_COLORS env variable', () => {
            process.env.NODE_DISABLE_COLORS = '1';
            expect(output.getColorDepth()).to.be.equal(ColorDepth.TWO);

            if (nodeDisableColors) {
                process.env.NODE_DISABLE_COLORS = nodeDisableColors;
            } else {
                delete process.env.NODE_DISABLE_COLORS;
            }
        });

        it('Should force to 16 by FORCE_COLOR env variable', () => {
            process.env.FORCE_COLOR = '1';
            expect(output.getColorDepth()).to.be.equal(ColorDepth.C16);

            if (forceColor) {
                process.env.FORCE_COLOR = forceColor;
            } else {
                delete process.env.FORCE_COLOR;
            }
        });

        it('Should force to 256 by FORCE_COLOR env variable', () => {
            process.env.FORCE_COLOR = '2';
            expect(output.getColorDepth()).to.be.equal(ColorDepth.C256);

            if (forceColor) {
                process.env.FORCE_COLOR = forceColor;
            } else {
                delete process.env.FORCE_COLOR;
            }
        });

        it('Should force to 16M by FORCE_COLOR env variable', () => {
            process.env.FORCE_COLOR = '3';
            expect(output.getColorDepth()).to.be.equal(ColorDepth.C16M);

            if (forceColor) {
                process.env.FORCE_COLOR = forceColor;
            } else {
                delete process.env.FORCE_COLOR;
            }
        });
    });

    describe('hasColorSupport', () => {
        const forceColor = process.env.FORCE_COLOR;
        const noColor = process.env.NO_COLOR;
        const nodeDisableColors = process.env.NODE_DISABLE_COLORS;

        it('Should disable color FORCE_COLOR env variable', () => {
            process.env.FORCE_COLOR = '0';
            expect(output.hasColorSupport()).to.be.equal(false);

            if (forceColor) {
                process.env.FORCE_COLOR = forceColor;
            } else {
                delete process.env.FORCE_COLOR;
            }
        });

        it('Should disable color by NO_COLOR env variable', () => {
            process.env.NO_COLOR = '1';
            expect(output.hasColorSupport()).to.be.equal(false);

            if (noColor) {
                process.env.NO_COLOR = noColor;
            } else {
                delete process.env.NO_COLOR;
            }
        });

        it('Should disable color by NODE_DISABLE_COLORS env variable', () => {
            process.env.NODE_DISABLE_COLORS = '1';
            expect(output.hasColorSupport()).to.be.equal(false);

            if (nodeDisableColors) {
                process.env.NODE_DISABLE_COLORS = nodeDisableColors;
            } else {
                delete process.env.NODE_DISABLE_COLORS;
            }
        });

        it('Should enbale colorby FORCE_COLOR env variable', () => {
            process.env.FORCE_COLOR = '1';
            expect(output.hasColorSupport()).to.be.equal(true);

            if (forceColor) {
                process.env.FORCE_COLOR = forceColor;
            } else {
                delete process.env.FORCE_COLOR;
            }
        });

        it('Should enbale color by FORCE_COLOR env variable', () => {
            process.env.FORCE_COLOR = '2';
            expect(output.hasColorSupport()).to.be.equal(true);

            if (forceColor) {
                process.env.FORCE_COLOR = forceColor;
            } else {
                delete process.env.FORCE_COLOR;
            }
        });

        it('Should enbale color by FORCE_COLOR env variable', () => {
            process.env.FORCE_COLOR = '3';
            expect(output.hasColorSupport()).to.be.equal(true);

            if (forceColor) {
                process.env.FORCE_COLOR = forceColor;
            } else {
                delete process.env.FORCE_COLOR;
            }
        });
    });

    describe('formatter', () => {
        it('Should not have formatter by default', () => {
            expect(output.getFormatter()).to.be.equal(undefined);
        });

        it('Should set formatter', () => {
            const formatter = new OutputFormatter();
            output.setFormatter(formatter);
            expect(output.getFormatter()).to.be.equal(formatter);
        });

        it('Should not format if no formatter', () => {
            output.writeln('<error>test</error>');
            expect(stubWrite.getCalls()[0].args[0]).to.be.equal(
                '<error>test</error>\n'
            );
        });

        it('Should format if a formatter is given', () => {
            output.setFormatter(new OutputFormatter());
            output.writeln('<error>test</error>');
            expect(stubWrite.getCalls()[0].args[0]).to.be.equal(
                '\u001b[37;41mtest\u001b[39;49m\n'
            );
        });

        it('Should work with custom style registered', () => {
            const outputStyle = new OutputFormatterStyle('red', '#ff0', [
                'bold',
                'blink',
            ]);
            const formatter = new OutputFormatter();
            output.setFormatter(formatter);
            formatter.setStyle('fire', outputStyle);

            output.writeln('<fire>foo</>');
            expect(stubWrite.getCalls()[0].args[0]).to.be.equal(
                '\u001b[31;43;1;5mfoo\u001b[39;49;22;25m\n'
            );
        });

        it('Should work with custom style', () => {
            const formatter = new OutputFormatter();
            output.setFormatter(formatter);

            output.writeln('<options=bold,blink>foo</>');
            expect(stubWrite.getCalls()[0].args[0]).to.be.equal(
                '\u001b[1;5mfoo\u001b[22;25m\n'
            );
        });
    });

    describe('verbosity', () => {
        it('Should be Normal by default', () => {
            expect(output.getVerbosity()).to.be.equal(Output.VERBOSITY.NORMAL);
        });

        it('Should not print message if more verbose', () => {
            output.write('test', false, Output.VERBOSITY.VERBOSE);
            expect(stubWrite.getCalls()).to.be.deep.equal([]);

            output.setVerbosity(Output.VERBOSITY.VERBOSE);
            output.write('test', false, Output.VERBOSITY.VERBOSE);
            expect(stubWrite.getCalls()[0].args[0]).to.be.equal('test');
        });

        it('Should get if is quiet', () => {
            output.setVerbosity(Output.VERBOSITY.QUIET);
            expect(output.isQuiet()).to.be.equal(true);
            output.setVerbosity(Output.VERBOSITY.NORMAL);
            expect(output.isQuiet()).to.be.equal(false);
            output.setVerbosity(Output.VERBOSITY.VERBOSE);
            expect(output.isQuiet()).to.be.equal(false);
            output.setVerbosity(Output.VERBOSITY.VERY_VERBOSE);
            expect(output.isQuiet()).to.be.equal(false);
            output.setVerbosity(Output.VERBOSITY.DEBUG);
            expect(output.isQuiet()).to.be.equal(false);
        });

        it('Should get if is verbose', () => {
            output.setVerbosity(Output.VERBOSITY.QUIET);
            expect(output.isVerbose()).to.be.equal(false);
            output.setVerbosity(Output.VERBOSITY.NORMAL);
            expect(output.isVerbose()).to.be.equal(false);
            output.setVerbosity(Output.VERBOSITY.VERBOSE);
            expect(output.isVerbose()).to.be.equal(true);
            output.setVerbosity(Output.VERBOSITY.VERY_VERBOSE);
            expect(output.isVerbose()).to.be.equal(false);
            output.setVerbosity(Output.VERBOSITY.DEBUG);
            expect(output.isVerbose()).to.be.equal(false);
        });

        it('Should get if is very verbose', () => {
            output.setVerbosity(Output.VERBOSITY.QUIET);
            expect(output.isVeryVerbose()).to.be.equal(false);
            output.setVerbosity(Output.VERBOSITY.NORMAL);
            expect(output.isVeryVerbose()).to.be.equal(false);
            output.setVerbosity(Output.VERBOSITY.VERBOSE);
            expect(output.isVeryVerbose()).to.be.equal(false);
            output.setVerbosity(Output.VERBOSITY.VERY_VERBOSE);
            expect(output.isVeryVerbose()).to.be.equal(true);
            output.setVerbosity(Output.VERBOSITY.DEBUG);
            expect(output.isVeryVerbose()).to.be.equal(false);
        });

        it('Should get if is debug', () => {
            output.setVerbosity(Output.VERBOSITY.QUIET);
            expect(output.isDebug()).to.be.equal(false);
            output.setVerbosity(Output.VERBOSITY.NORMAL);
            expect(output.isDebug()).to.be.equal(false);
            output.setVerbosity(Output.VERBOSITY.VERBOSE);
            expect(output.isDebug()).to.be.equal(false);
            output.setVerbosity(Output.VERBOSITY.VERY_VERBOSE);
            expect(output.isDebug()).to.be.equal(false);
            output.setVerbosity(Output.VERBOSITY.DEBUG);
            expect(output.isDebug()).to.be.equal(true);
        });
    });

    describe('output type', () => {
        beforeEach(() => {
            const formatter = new OutputFormatter();
            output.setFormatter(formatter);
        });

        it('Should return raw message', () => {
            output.write('<error>foobar</>', false, Output.OUTPUT.RAW);
            expect(stubWrite.getCalls()[0].args[0]).to.be.equal(
                '<error>foobar</>'
            );
        });

        it('Should return plain message', () => {
            output.write(
                '<error>foo<options=bold>b</>ar \\<error></>',
                false,
                Output.OUTPUT.PLAIN
            );
            expect(stubWrite.getCalls()[0].args[0]).to.be.equal(
                'foobar <error>'
            );
        });
    });

    describe('section', () => {
        it('Should return a new section', () => {
            const section1 = output.section();
            const section2 = output.section();
            expect(section1).to.be.instanceOf(SectionOutput);
            expect(section1).to.be.not.equal(section2);
        });

        it('Should give the current stream', () => {
            const section = output.section();
            expect(output.getStream()).to.be.equal(section.getStream());
        });

        // section

        it('Should give the current verbosity', () => {
            output.setVerbosity(Output.VERBOSITY.VERBOSE);
            const section = output.section();
            expect(output.getVerbosity()).to.be.equal(section.getVerbosity());
        });

        // formatter
        it('Should give the current formatter', () => {
            const section = output.section();
            expect(output.getFormatter()).to.be.equal(section.getFormatter());
        });

        it('Should add the new section to the output', () => {
            const section1 = output.section();
            const section2 = output.section();
            expect(
                Object.getOwnPropertyDescriptor(output, '_sectionOutputs')
                    ?.value
            ).to.be.deep.equal([section2, section1]);
        });
    });
});
