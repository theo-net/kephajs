/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { OutputFormatterStyle } from './OutputFormatterStyle';

export class OutputFormatterStyleStack {
    private _styles: OutputFormatterStyle[] = [];

    private _emptyStyle: OutputFormatterStyle;

    constructor() {
        this._emptyStyle = new OutputFormatterStyle();
    }

    reset() {
        this._styles = [];
    }

    /**
     * Pushes a style in the stack.
     */
    push(style: OutputFormatterStyle) {
        this._styles.push(style);
    }

    /**
     * Pops a style from the stack.
     */
    pop(): OutputFormatterStyle {
        if (this._styles.length === 0) {
            return this._emptyStyle;
        }

        return this._styles.pop() as OutputFormatterStyle;
    }

    /**
     * Computes current style with stacks top codes.
     */
    getCurrent(): OutputFormatterStyle {
        if (this._styles.length === 0) {
            return this._emptyStyle;
        }

        return this._styles[this._styles.length - 1];
    }

    getEmptyStyle() {
        return this._emptyStyle;
    }
}
