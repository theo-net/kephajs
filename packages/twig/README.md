# KephaJs `twig`

It's a pure TypeScript re-implementation of the Twig PHP templating language (<https://twig.symfony.com/> v3.4.3)

The goal is to provide a library that is compatible with both browsers and server side JavaScript environments.
