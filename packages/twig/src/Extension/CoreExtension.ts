/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import {
    arrayChunk,
    getAllMethods,
    getAllStaticMethods,
    objectKeys,
    range,
} from '@kephajs/core/Helpers/Helpers';
import { forEach } from '@kephajs/core/Helpers/IterationHelpers';
import { numberFormat } from '@kephajs/core/Helpers/NumberHelpers';
import { escape, str2RegExp } from '@kephajs/core/Helpers/RegExpHelpers';
import { sprintf, stripTags } from '@kephajs/core/Helpers/StringHelpers';
import { Environment } from '../Environment';
import { LoaderError } from '../Error/LoaderError';
import { RuntimeError } from '../Error/RuntimeError';
import { ExpressionParser } from '../ExpressionParser';
import { Markup } from '../Markup';
import { AddBinary } from '../Node/Expression/Binary/AddBinary';
import { AndBinary } from '../Node/Expression/Binary/AndBinary';
import { BitwiseAndBinary } from '../Node/Expression/Binary/BitwiseAndBinary';
import { BitwiseOrBinary } from '../Node/Expression/Binary/BitwiseOrBinary';
import { BitwiseXorBinary } from '../Node/Expression/Binary/BitwiseXorBinary';
import { ConcatBinary } from '../Node/Expression/Binary/ConcatBinary';
import { DivBinary } from '../Node/Expression/Binary/DivBinary';
import { EndsWithBinary } from '../Node/Expression/Binary/EndsWithBinary';
import { EqualBinary } from '../Node/Expression/Binary/EqualBinary';
import { FloorDivBinary } from '../Node/Expression/Binary/FloorDivBinary';
import { GreaterBinary } from '../Node/Expression/Binary/GreaterBinary';
import { GreaterEqualBinary } from '../Node/Expression/Binary/GreaterEqualBinary';
import { InBinary } from '../Node/Expression/Binary/InBinary';
import { LessBinary } from '../Node/Expression/Binary/LessBinary';
import { LessEqualBinary } from '../Node/Expression/Binary/LessEqualBinary';
import { MatchesBinary } from '../Node/Expression/Binary/MatchesBinary';
import { ModBinary } from '../Node/Expression/Binary/ModBinary';
import { MulBinary } from '../Node/Expression/Binary/MulBinary';
import { NotEqualBinary } from '../Node/Expression/Binary/NotEqualBinary';
import { NotInBinary } from '../Node/Expression/Binary/NotInBinary';
import { OrBinary } from '../Node/Expression/Binary/OrBinary';
import { PowerBinary } from '../Node/Expression/Binary/PowerBinary';
import { RangeBinary } from '../Node/Expression/Binary/RangeBinary';
import { SpaceshipBinary } from '../Node/Expression/Binary/SpaceshipBinary';
import { StartsWithBinary } from '../Node/Expression/Binary/StartsWithBinary';
import { SubBinary } from '../Node/Expression/Binary/SubBinary';
import { DefaultFilter } from '../Node/Expression/Filter/DefaultFilter';
import { NullCoalesceExpression } from '../Node/Expression/NullCoalesceExpression';
import { DefinedTest } from '../Node/Expression/Test/DefinedTest';
import { DivisiblebyTest } from '../Node/Expression/Test/DivisiblebyTest';
import { EvenTest } from '../Node/Expression/Test/EvenTest';
import { NullTest } from '../Node/Expression/Test/NullTest';
import { OddTest } from '../Node/Expression/Test/OddTest';
import { SameasTest } from '../Node/Expression/Test/SameasTest';
import { NegUnary } from '../Node/Expression/Unary/NegUnary';
import { NotUnary } from '../Node/Expression/Unary/NotUnary';
import { PosUnary } from '../Node/Expression/Unary/PosUnary';
import { MacroAutoImportNodeVisitor } from '../NodeVisitor/MacroAutoImportNodeVisitor';
import { NodeVisitorInterface } from '../NodeVisitor/NodeVisitorInterface';
import { Source } from '../Source';
import { Template } from '../Template';
import { TemplateWrapper } from '../TemplateWrapper';
import { ApplyTokenParser } from '../TokenParser/ApplyTokenParser';
import { BlockTokenParser } from '../TokenParser/BlockTokenParser';
import { DeprecatedTokenParser } from '../TokenParser/DeprecatedTokenParser';
import { DoTokenParser } from '../TokenParser/DoTokenParser';
import { EmbedTokenParser } from '../TokenParser/EmbedTokenParser';
import { ExtendsTokenParser } from '../TokenParser/ExtendsTokenParser';
import { ForTokenParser } from '../TokenParser/ForTokenParser';
import { FromTokenParser } from '../TokenParser/FromTokenParser';
import { IfTokenParser } from '../TokenParser/IfTokenParser';
import { ImportTokenParser } from '../TokenParser/ImportTokenParser';
import { IncludeTokenParser } from '../TokenParser/IncludeTokenParser';
import { MacroTokenParser } from '../TokenParser/MacroTokenParser';
import { SetTokenParser } from '../TokenParser/SetTokenParser';
import { UseTokenParser } from '../TokenParser/UseTokenParser';
import { WithTokenParser } from '../TokenParser/WithTokenParser';
import { TwigFilter, TwigFilterType } from '../TwigFilter';
import { TwigFunction, TwigFunctionType } from '../TwigFunction';
import { TwigTest } from '../TwigTest';
import { AbstractExtension } from './AbstractExtension';
import { Operator } from './ExtensionInterface';
import { SandboxExtension } from './SandboxExtension';

function twigCycle(values: unknown, position: number) {
    if (!Array.isArray(values)) return values;

    return (values as unknown[])[position % (values as unknown[]).length];
}

function twigToArray(seq: unknown) {
    if (Array.isArray(seq)) return seq;
    else if (
        seq !== null &&
        typeof seq === 'object' &&
        typeof (seq as Record<symbol, unknown>)[Symbol.iterator] === 'function'
    )
        return [...(seq as Iterable<unknown>)];
    else if (seq !== null && typeof seq === 'object') return Object.values(seq);
    else return [seq];
}

function twigTestIterable(value: unknown) {
    return (
        value !== null && (Array.isArray(value) || typeof value === 'object')
    );
}

export function twigRandom(values: unknown = null, max: number | null = null) {
    if (values === null)
        return max === null
            ? Math.floor(Math.random() * Number.MAX_VALUE)
            : Math.floor(Math.random() * Number(max));

    let min = 0;
    if (typeof values === 'number') {
        if (max === null) {
            if (values < 0) {
                max = 0;
                min = values;
            } else {
                max = values;
                min = 0;
            }
        } else min = values;

        return Math.floor(
            Math.random() * (Number(max) - Number(min)) + Number(min)
        );
    }

    if (typeof values === 'string') {
        if (values === '') return '';

        return values[Math.floor(Math.random() * Number(values.length - 1))];
    }

    if (!twigTestIterable(values)) return values;

    values = twigToArray(values);

    if (Array.isArray(values) && values.length === 0)
        throw new RuntimeError(
            'The random function cannot pick from an empty array.'
        );

    return (values as unknown[])[
        Math.floor(Math.random() * Number((values as unknown[]).length - 1))
    ];
}

function twigDateFormatFilter(
    env: Environment,
    date: Date,
    format: string | null = null,
    timezone: string | false | null = null
) {
    if (format === null) {
        const formats = (
            env.getExtension('CoreExtension') as CoreExtension
        ).getDateFormat();
        format = date instanceof Date ? formats[1] : formats[0];
    }

    //if (date instanceof DateInterval) return date.format(format);
    return String(date) + timezone;

    //return twigDateConverter(env, date, timezone).format(format);
}

function twigSprintf(format?: string, ...values: unknown[]) {
    return sprintf(format ?? '', ...values);
}

/**
 * Converts an input to a \DateTime instance.
 *
 *    {% if date(user.created_at) < date('+2days') %}
 *      {# do something #}
 *    {% endif %}
 *
 * @param \DateTimeInterface|string|null  $date     A date or null to use the current time
 * @param \DateTimeZone|string|false|null $timezone The target timezone, null to use the default, false to leave unchanged
 *
 * @return \DateTimeInterface
 */
function twigDateConverter(
    env: Environment,
    date: null | string | Date = null,
    timezone: null | false | string = null
) {
    // determine the timezone
    if (timezone === null)
        timezone = (
            env.getExtension('CoreExtension') as CoreExtension
        ).getTimezone();

    /*if (date instanceof Date) {
        date = structuredClone(date);
        if (timezone !== false) (date as Date).setTimezone(timezone);

        return date;
    }

    if (date === null || date === 'now') {
        return new DateTime(
            'now',
            timezone !== false
                ? timezone
                : (
                      env.getExtension('CoreExtension') as CoreExtension
                  ).getTimezone()
        );
    }

    asString = date;
    if (
        ctype_digit(asString) ||
        (!empty(asString) &&
            asString[0] === '-' &&
            ctype_digit(substr(asString, 1)))
    )
        date = new DateTime('@' + date);
    else
        date = new DateTime(
            date,
            env.getExtension('CoreExtension').getTimezone()
        );

    if (timezone !== false) date.setTimezone(timezone);*/

    return date as Date;
}

/**
 * Returns a new date object modified.
 *
 *   {{ post.published_at|date_modify("-1day")|date("m/d/Y") }}
 *
 * @param \DateTimeInterface|string $date     A date
 * @param string                    $modifier A modifier string
 *
 * @return \DateTimeInterface
 */
function twigDateModifyFilter(
    env: Environment,
    date: string | Date,
    modifier: string
) {
    date = twigDateConverter(env, date, false);

    console.log(modifier);
    return date; //$date->modify($modifier);
}

function twigReplaceFilter(str: string, from: Record<string, string>) {
    if (!twigTestIterable(from))
        throw new RuntimeError(
            `The "replace" filter expects an array or "Traversable" as replace values, got "${
                typeof from === 'object'
                    ? from !== null
                        ? from.constructor.name
                        : 'null'
                    : typeof from
            }".`
        );
    if (typeof str !== 'string') str = String(str);

    objectKeys(from).forEach(placeholder => {
        str = str.replace(
            new RegExp(escape(placeholder), 'g'),
            from[placeholder]
        );
    });
    return str;
}

function twigRound(
    value: string | null | number,
    precision = 0,
    method: 'common' | 'ceil' | 'floor' = 'common'
) {
    value = Number(value);

    let fct: (val: number) => number;
    if (method === 'common') fct = Math.round;
    else if (method === 'ceil' || method === 'floor') fct = Math[method];
    else
        throw new RuntimeError(
            'The round filter only supports the "common", "ceil", and "floor" methods.'
        );

    return fct(value * 10 ** precision) / 10 ** precision;
}

function twigNumberFormatFilter(
    env: Environment,
    number: number | string,
    decimal: number | null = null,
    decimalPoint: string | null = null,
    thousandSep: string | null = null
) {
    const defaults: [number, string, string] = (
        env.getExtension('CoreExtension') as CoreExtension
    ).getNumberFormat();
    if (decimal === null) decimal = defaults[0];

    if (decimalPoint === null) decimalPoint = defaults[1];

    if (thousandSep === null) thousandSep = defaults[2];

    return numberFormat(Number(number), decimal, decimalPoint, thousandSep);
}

function twigUrlencodeFilter(url: string | Record<string, string>) {
    if (typeof url !== 'string') return new URLSearchParams(url).toString;

    return encodeURI(url ?? '');
}

function twigArrayMerge(
    arr1: unknown[] | Record<string | number | symbol, unknown>,
    arr2: unknown[] | Record<string | number | symbol, unknown>
) {
    if (
        (Array.isArray(arr1) ||
            (arr1 !== null &&
                typeof arr1 === 'object' &&
                typeof (arr1 as Record<symbol, unknown>)[Symbol.iterator] ===
                    'function')) &&
        (Array.isArray(arr2) ||
            (arr2 !== null &&
                typeof arr2 === 'object' &&
                typeof (arr2 as Record<symbol, unknown>)[Symbol.iterator] ===
                    'function'))
    )
        return [...(arr1 as Iterable<unknown>), ...(arr2 as Iterable<unknown>)];
    if (typeof arr1 === 'object' && typeof arr2 === 'object')
        return { ...arr1, ...arr2 };

    throw new RuntimeError(
        `The merge filter only works with arrays or hash. The two arguments need have the same type, got "${typeof arr1}" and "${typeof arr2}"`
    );
}

export function twigSlice(
    item: unknown,
    start: number,
    length: null | number = null,
    preserveKeys = false
) {
    if (item === null) return '';

    if (
        !Array.isArray(item) &&
        typeof item === 'object' &&
        typeof (item as Record<symbol, unknown>)[Symbol.iterator] === 'function'
    ) {
        const items: unknown[] = [];
        for (const value of item as Iterable<unknown>) items.push(value);

        if (!preserveKeys) item = items;
        else {
            const record: Record<string | number | symbol, unknown> = {};
            let keys: (string | number | symbol)[] = [];
            if (typeof (item as Record<string, unknown>).keys === 'function')
                keys = (
                    item as { keys: () => (string | number | symbol)[] }
                ).keys();
            else
                for (const key in item as Iterable<string | number | symbol>)
                    keys.push(key);
            keys.slice(
                start,
                start >= 0
                    ? length === 0 || length
                        ? start + length
                        : undefined
                    : keys.length - (length ?? 0)
            ).forEach((key, index) => {
                record[key] = items[index];
            });
            return record;
        }
    } else if (!Array.isArray(item) && typeof item === 'object') {
        const record: Record<string | number | symbol, unknown> = {};
        const keys = objectKeys(item);
        keys.slice(
            start,
            start >= 0
                ? length === 0 || length
                    ? start + length
                    : undefined
                : keys.length - (length ? length - 1 : 0)
        ).forEach(key => {
            record[key] = (item as Record<string | number | symbol, unknown>)[
                key
            ];
        });
        return record;
    }

    if (Array.isArray(item) && preserveKeys) {
        const keys = Object.keys(item).slice(
            start,
            start >= 0
                ? length === 0 || length
                    ? start + length
                    : undefined
                : (item as unknown[]).length - (length ? length - 1 : 0)
        );
        const result: unknown[] = [];
        keys.forEach(key => {
            result[Number(key)] = (item as unknown[])[Number(key)];
        });
        return result;
    } else if (Array.isArray(item))
        return (item as unknown[]).slice(
            start,
            start >= 0
                ? length === 0 || length
                    ? start + length
                    : undefined
                : (item as unknown[]).length - (length ? length - 1 : 0)
        );

    const itemStr = String(item);
    return itemStr.substring(
        start >= 0 ? start : itemStr.length + start,
        start >= 0
            ? length === 0 || length
                ? start + length
                : undefined
            : itemStr.length + start + (length ?? 0)
    );
}

export function twigFirst(item: unknown) {
    let elements = twigSlice(item, 0, 1, false);

    if (elements !== null && typeof elements === 'object')
        elements = Object.values(elements);

    return typeof elements === 'string' ? elements : elements[0];
}

export function twigLast(item: unknown) {
    let elements = twigSlice(item, -1, 1, false);

    if (elements !== null && typeof elements === 'object')
        elements = Object.values(elements);

    return typeof elements === 'string' ? elements : elements[0];
}

function twigJoinFilter(
    value: unknown[],
    glue = '',
    and: string | null = null
) {
    value = twigToArray(value);

    if (value.length === 0) return '';

    if (and === null || and === glue) {
        return value.join(glue);
    }

    if (value.length === 1) return value[0];

    return (
        value.slice(0, value.length - 1).join(glue) +
        and +
        value[value.length - 1]
    );
}

function twigSplitFilter(value: string, delimiter: string, limit?: number) {
    value = value ? String(value) : '';

    if (limit === 0) limit = 1;

    if (delimiter.length > 0) {
        if (limit === undefined) return value.split(delimiter);
        else if (limit < 0) {
            const splited = value.split(delimiter);
            return splited.slice(0, splited.length + limit);
        } else {
            const splited = value.split(delimiter, limit - 1); // (limit - 1) splited elem + last (withe the rest) = limit
            if (splited.length === 0) return [value];
            else
                return [
                    ...splited,
                    value.substring(splited.join(delimiter).length + 1), // +1: remove the delimiter before the rest
                ];
        }
    }

    if (limit === undefined) limit = 1;
    if (limit <= 1) return value.split('');

    const length = value.length;
    if (typeof limit === 'number' && length < limit) return [value];

    const r: string[] = [];
    for (let i = 0; i < length; i += limit ?? 1) {
        r.push(value.substring(i, i + limit));
    }

    return r;
}

function twigTestEmpty(value: unknown) {
    if (Array.isArray(value)) return value.length === 0;

    if (
        value !== null &&
        typeof value === 'object' &&
        (value as Record<string, unknown>).length !== undefined
    )
        return (value as Record<string, unknown>).length === 0;

    if (
        value !== null &&
        typeof value === 'object' &&
        typeof value.toString === 'function'
    )
        return value.toString() === '';

    return (
        value === '' || value === false || value === null || value === undefined
    );
}

function twigDefaultFilter(value: unknown, dflt = '') {
    if (twigTestEmpty(value)) return dflt;

    return value;
}

function twigReverseFilter(item: unknown) {
    if (Array.isArray(item)) return item.reverse();
    else if (
        item !== null &&
        typeof item === 'object' &&
        typeof (item as Record<symbol, unknown>)[Symbol.iterator] === 'function'
    ) {
        return [...(item as Iterable<unknown>)].reverse();
    } else if (item !== null && typeof item === 'object')
        return Object.values(item as Record<string, unknown>).reverse();

    return [...String(item).matchAll(/./gsu)].reverse().join('');
}

function twigCheckArrowInSandbox(
    env: Environment,
    arrow: (...args: unknown[]) => unknown,
    thing: string,
    type: string
) {
    if (
        arrow.name !== '' &&
        env.hasExtension('SandboxExtension') &&
        (env.getExtension('SandboxExtension') as SandboxExtension).isSandboxed()
    )
        throw new RuntimeError(
            `The callable passed to the "${thing}" ${type} must be anonymous in sandbox mode.`
        );
}

function twigSortFilter(
    env: Environment,
    array: unknown[],
    arrow: null | ((a: unknown, b: unknown) => number) = null
) {
    if (!Array.isArray(array))
        throw new RuntimeError(
            `The sort filter only works with arrays", got "${typeof array}".`
        );

    if (arrow !== null) {
        twigCheckArrowInSandbox(env, arrow, 'sort', 'filter');

        array.sort(arrow);
    } else array.sort();

    return array;
}

export function twigInFilter(value: unknown, compare: unknown) {
    if (value instanceof Markup) value = value.toString();
    if (compare instanceof Markup) compare = compare.toString();

    if (typeof compare === 'string') {
        if (
            typeof value === 'string' ||
            typeof value === 'number' ||
            typeof value === 'boolean'
        )
            return value === '' || compare.indexOf(String(value)) > -1;

        return false;
    }

    if (Array.isArray(compare))
        return (
            compare.indexOf(value) > -1 ||
            compare.indexOf(String(value)) > -1 ||
            compare.map(comp => String(comp)).indexOf(value as string) > -1
        );

    if (
        typeof compare === 'object' &&
        typeof (compare as Record<symbol, unknown>)[Symbol.iterator] ===
            'function'
    ) {
        const items = [...(compare as Iterable<unknown>)];
        //for (const val of compare as Iterable<unknown>) items.push(val);
        return (
            items.indexOf(value) > -1 ||
            items.indexOf(String(value)) > -1 ||
            items.map(comp => String(comp)).indexOf(value as string) > -1
        );
    } else if (typeof compare === 'object') {
        const values = Object.values(compare as Record<string, unknown>);
        return (
            values.indexOf(value) > -1 ||
            values.indexOf(String(value)) > -1 ||
            values.map(comp => String(comp)).indexOf(value as string) > -1
        );
    }

    return false;
}

function twigTrimFilter(string: string, side = 'both') {
    string = String(string ?? '');

    switch (side) {
        case 'both':
            return string.trim();
        case 'left':
            return string.trimStart();
        case 'right':
            return string.trimEnd();
        default:
            throw new RuntimeError(
                'Trimming side must be "left", "right" or "both".'
            );
    }
}

function twigNl2br(string: string) {
    return String(string).replace(/\r\n/g, '<br />').replace(/\n/g, '<br />');
}

function twigSpaceless(content: string) {
    return String(content ?? '')
        .replace(/>\s+</g, '><')
        .trim();
}

// eslint-disable-next-line @typescript-eslint/ban-types
function twigLengthFilter(
    thing: unknown[] | string | null | number | Record<string, unknown>
) {
    if (thing === null) return 0;

    if (
        thing !== undefined &&
        typeof thing !== 'number' &&
        thing &&
        thing.length &&
        typeof thing.length === 'number'
    )
        return thing.length;
    else if (
        thing !== null &&
        typeof thing === 'object' &&
        typeof (thing as Record<symbol, unknown>)[Symbol.iterator] ===
            'function'
    )
        return [...(thing as Iterable<unknown>)].length;
    else if (
        thing !== null &&
        typeof thing === 'object' &&
        typeof thing.toString === 'function'
    )
        return thing.toString().length;
    else if (typeof thing === 'number' || typeof thing === 'bigint')
        return String(thing).length;

    return 0;
}

function twigUpperFilter(env: Environment, string: string) {
    return String(string).toLocaleUpperCase(env.getLocale());
}

function twigLowerFilter(env: Environment, string: string) {
    return String(string).toLocaleLowerCase(env.getLocale());
}

function twigStriptags(
    string: string | null,
    allowableTags?: string | string[]
) {
    return stripTags(String(string) ?? '', allowableTags);
}

function twigTitleStringFilter(env: Environment, string: string) {
    const words = (String(string) ?? '')
        .toLocaleLowerCase(env.getLocale())
        .split(/\s+/);
    let str = '';
    let first = true;
    words.forEach(word => {
        if (!first) str += ' ';
        str +=
            word.charAt(0).toLocaleUpperCase(env.getLocale()) + word.slice(1);
        first = false;
    });
    return str;
}

function twigCapitalizeStringFilter(env: Environment, string: string) {
    const locale = env.getLocale();

    return (
        (String(string) ?? '').substring(0, 1).toLocaleUpperCase(locale) +
        (String(string) ?? '').substring(1).toLocaleLowerCase(locale)
    );
}

function twigCallMacro(
    template: Template,
    method: string,
    args: unknown[],
    lineno: number,
    context: Record<string, unknown>,
    source: Source
) {
    if (typeof template[method] !== 'function') {
        let parent: false | Template = template;
        while ((parent = parent.getParent(context) as false | Template)) {
            if (typeof parent[method] === 'function') {
                return (parent[method] as (...macroArgs: unknown[]) => unknown)(
                    ...args
                );
            }
        }

        throw new RuntimeError(
            `Macro "${method.substring(
                'macro_'.length
            )}" is not defined in template "${template.getTemplateName()}".`,
            lineno,
            source
        );
    }

    return (template[method] as (...macroArgs: unknown[]) => unknown)(...args);
}

function convertForMinMax(firstValue: unknown | unknown[], ...args: unknown[]) {
    if (Array.isArray(firstValue)) args = [...firstValue, ...args];
    else args = [firstValue, ...args];

    if (!Array.isArray(firstValue) && typeof firstValue === 'object') {
        args = [];
        objectKeys(firstValue as Record<string, unknown>).forEach(property => {
            args.push((firstValue as Record<string, unknown>)[property]);
        });
    }

    const isNumber = args.every(arg => {
        if (typeof arg === 'number') return true;
        if (typeof arg === 'string') {
            if (!/^[0-9]+$/.test(arg)) return false;
            else return true;
        }
        return false;
    });

    return {
        isNumber,
        args,
    };
}

function twigMax(firstValue: unknown | unknown[], ...args: unknown[]) {
    const result = convertForMinMax(firstValue, ...args);
    if (result.isNumber) return Math.max(...(result.args as number[]));
    else return result.args.sort()[result.args.length - 1];
}

function twigMin(firstValue: unknown | unknown[], ...args: unknown[]) {
    const result = convertForMinMax(firstValue, ...args);
    if (result.isNumber) return Math.min(...(result.args as number[]));
    else return result.args.sort()[0];
}

function twigInclude(
    env: Environment,
    context: Record<string, unknown>,
    template:
        | (string | Template | TemplateWrapper)
        | (string | Template | TemplateWrapper)[],
    variables: Record<string, unknown> = {},
    withContext = true,
    ignoreMissing = false,
    sandboxed = false
) {
    let alreadySandboxed = false;
    let isSandboxed = false;
    let sandbox: SandboxExtension | null = null;
    if (withContext) variables = { ...context, ...variables };

    if ((isSandboxed = sandboxed && env.hasExtension('SandboxExtension'))) {
        sandbox = env.getExtension('SandboxExtension') as SandboxExtension;
        if (!(alreadySandboxed = sandbox.isSandboxed()))
            sandbox.enableSandbox();

        /*(Array.isArray(template) ? template : [template]).forEach(name => {
            // if a Template instance is passed, it might have been instantiated outside of a sandbox, check security
            if (name instanceof TemplateWrapper || name instanceof Template)
                name.unwrap().checkSecurity();
        });*/
    }

    try {
        let loaded: Template | TemplateWrapper | null = null;
        try {
            loaded = env.resolveTemplate(template);
        } catch (error) {
            if (!(error instanceof LoaderError) || !ignoreMissing) throw error;
        }

        return loaded ? loaded.render(variables) : '';
    } finally {
        if (isSandboxed && !alreadySandboxed)
            (sandbox as SandboxExtension).disableSandbox();
    }
}

function twigSource(env: Environment, name: string, ignoreMissing = false) {
    const loader = env.getLoader();
    try {
        return loader.getSourceContext(name).code;
    } catch (error) {
        if (!(error instanceof LoaderError) || !ignoreMissing) throw error;
    }
    return '';
}

export function twigArrayBatch(
    items:
        | unknown[]
        | Record<string | number | symbol, unknown>
        | Iterable<unknown>,
    size: number,
    fill: unknown = null
) {
    size = Math.ceil(size);
    const values = items;
    let isRecord = false;

    if (
        !Array.isArray(items) &&
        items !== null &&
        typeof items === 'object' &&
        typeof (items as Record<string | number | symbol, unknown>)[
            Symbol.iterator
        ] === 'function'
    )
        items = [...(items as Iterable<unknown>)];
    else if (
        !Array.isArray(items) &&
        items !== null &&
        typeof items === 'object'
    ) {
        isRecord = true;
        items = Object.keys(items);
    }

    const result = arrayChunk(items as unknown[], size);

    if (fill !== null && result.length > 0) {
        const last = result.length - 1;
        let fillCount: number;
        if ((fillCount = size - result[last].length)) {
            for (let i = 0; i < fillCount; ++i) {
                if (isRecord) {
                    result[last].push(i);
                    (values as Record<string | number | symbol, unknown>)[i] =
                        fill;
                } else result[last].push(fill);
            }
        }
    }

    if (!isRecord) return result;
    else {
        const recordArray: Record<string | number | symbol, unknown>[] = [];
        (result as (string | number | symbol)[][]).forEach(row => {
            const recordRow: Record<string | number | symbol, unknown> = {};
            row.forEach(cell => {
                recordRow[cell] = (
                    values as Record<string | number | symbol, unknown>
                )[cell];
            });
            recordArray.push(recordRow);
        });

        return recordArray;
    }
}

function toCache(
    cache: Record<string, Record<string, string>>,
    object: object,
    className: string,
    getStatic = false
) {
    // precedence: getXxx() > isXxx() > hasXxx()
    if (cache[className] === undefined) {
        const methods: string[] = (
            getStatic ? getAllStaticMethods : getAllMethods
        )(object);
        const lcMethods = methods.map(method => method.toLowerCase());
        const classCache: Record<string, string> = {};
        let methodName = '';
        methods.forEach((method, i) => {
            let lcName = lcMethods[i];
            classCache[method] = method;
            classCache[lcName] = method;

            if ('g' === lcName[0] && 0 === lcName.indexOf('get')) {
                methodName = method.substring(3);
                lcName = lcName.substring(3);
            } else if ('i' === lcName[0] && 0 === lcName.indexOf('is')) {
                methodName = method.substring(2);
                lcName = lcName.substring(2);
            } else if ('h' === lcName[0] && 0 === lcName.indexOf('has')) {
                methodName = method.substring(3);
                lcName = lcName.substring(3);
                if (lcMethods.indexOf('is' + lcName) > -1) return;
            } else return;

            // skip get() and is() methods (in which case, $name is empty)
            if (methodName !== '') {
                if (classCache[methodName] === undefined)
                    classCache[methodName] = method;

                if (classCache[lcName] === undefined)
                    classCache[lcName] = method;
            }
        });
        cache[className] = classCache;
    }
}
const cache: Record<string, Record<string, string>> = {};
const cacheStatic: Record<string, Record<string, string>> = {};
export function twigGetAttribute(
    env: Environment,
    source: Source,
    object: unknown,
    item: number | boolean | string,
    args: unknown[] = [],
    type = Template.ANY_CALL,
    isDefinedTest = false,
    ignoreStrictCheck = false,
    sandboxed = false,
    lineno = -1
) {
    // array
    if (type !== Template.METHOD_CALL) {
        const arrayItem = typeof item === 'boolean' ? Number(item) : item;

        if (
            Array.isArray(object) &&
            typeof arrayItem === 'number' &&
            object[arrayItem] !== undefined
        ) {
            if (isDefinedTest) return true;

            return object[arrayItem];
        }

        if (
            object !== null &&
            object !== undefined &&
            (Object.getOwnPropertyNames(Object.getPrototypeOf(object)).indexOf(
                String(arrayItem)
            ) > -1 ||
                Object.prototype.hasOwnProperty.call(object, arrayItem))
        ) {
            if (sandboxed && typeof object === 'object') {
                (
                    env.getExtension('SandboxExtension') as SandboxExtension
                ).checkMethodAllowed(
                    object as object,
                    arrayItem + '',
                    lineno,
                    source
                );
            }
            if (
                typeof (object as Record<string | number, unknown>)[
                    arrayItem
                ] === 'function'
            ) {
                const result = (
                    (object as Record<string | number, unknown>)[arrayItem] as (
                        // eslint-disable-next-line @typescript-eslint/no-shadow
                        ...args: unknown[]
                    ) => unknown
                )(...args);
                if (isDefinedTest) return result !== undefined;
                return result;
            }

            if (isDefinedTest) return true;

            return (object as Record<string | number, unknown>)[arrayItem];
        }

        if (
            type === Template.ARRAY_CALL ||
            (typeof object !== 'undefined' &&
                typeof object !== 'object' &&
                (object as Record<string, unknown>).constructor() !== Object)
        ) {
            if (isDefinedTest) return false;

            if (ignoreStrictCheck || !env.isStrictVariables()) return;

            let message = '';
            if (Array.isArray(object) && typeof item !== 'number')
                message = `Impossible to access to an array with the key "${item}" (${typeof item}).`;
            else if (Array.isArray(object)) {
                if (object.length === 0)
                    message = `Key "${arrayItem}" does not exist as the array is empty.`;
                else message = `Key "${arrayItem}" for array does not exist.`;
            } else if (
                object !== null &&
                typeof object === 'object' &&
                (object as Record<string | number, unknown>)[arrayItem] ===
                    undefined
            )
                message = `Key "${arrayItem}" in object does not exist.`;
            else if (type === Template.ARRAY_CALL) {
                if (object === null)
                    message = `Impossible to access a key ("${item}") on a null variable.`;
                else if (object === undefined)
                    message = `Impossible to access a key ("${item}") on a undefined variable.`;
                else
                    message = `Impossible to access a key ("${item}") on a ${typeof object} variable ("${object}").`;
            } else if (object === null)
                message = `Impossible to invoke a method or to access an attribute ("${item}") on a null variable.`;
            else if (object === undefined)
                message = `Impossible to invoke a method or to access an attribute ("${item}") on a undefined variable.`;
            else
                message = `Impossible to access an attribute ("${item}") on a ${typeof object} variable ("${object}").`;

            throw new RuntimeError(message, lineno, source);
        }
    }

    // null
    if (object === null) {
        if (isDefinedTest) return false;

        if (ignoreStrictCheck || !env.isStrictVariables()) return;

        throw new RuntimeError(
            `Impossible to invoke a method or to access an attribute ("${item}") on a null variable.`,
            lineno,
            source
        );
    }
    // undefined
    else if (object === undefined) {
        if (isDefinedTest) return false;

        if (ignoreStrictCheck || !env.isStrictVariables()) return;

        throw new RuntimeError(
            `Impossible to invoke a method or to access an attribute ("${item}") on a undefined variable.`,
            lineno,
            source
        );
    }
    // array
    else if (Array.isArray(object)) {
        if (object[Number(item)] !== undefined) {
            if (isDefinedTest) return true;

            return object[Number(item)];
        }
        if (isDefinedTest) return false;
        if (ignoreStrictCheck || !env.isStrictVariables()) return;

        throw new RuntimeError(
            `Impossible to invoke a method or to access an attribute ("${item}") on an array.`,
            lineno,
            source
        );
    }
    // other, but no an object
    else if (
        typeof object !== 'object' &&
        (object as Record<string, unknown>).constructor() !== Object
    ) {
        if (isDefinedTest) return false;

        if (ignoreStrictCheck || !env.isStrictVariables()) return;
        throw new RuntimeError(
            `Impossible to invoke a method ("${item}") on a ${typeof object} variable ("${object}").`,
            lineno,
            source
        );
    }

    if (object instanceof Template)
        throw new RuntimeError(
            'Accessing Template attributes is forbidden.',
            lineno,
            source
        );

    // object property
    if (type !== Template.METHOD_CALL) {
        if ((object as Record<string, unknown>)[item as string] !== undefined) {
            if (isDefinedTest) return true;

            if (sandboxed)
                (
                    env.getExtension('SandboxExtension') as SandboxExtension
                ).checkPropertyAllowed(
                    object as object,
                    String(item),
                    lineno,
                    source
                );

            return (object as Record<string, unknown>)[item as string];
        }
    }

    const className = (object as object).constructor.name;

    // object method
    toCache(cache, object as object, className, false);
    toCache(cacheStatic, object as object, className, true);

    let method = '';
    let isStatic = false;
    const lcItem = String(item).toLowerCase();
    if (cache[className][item as string] !== undefined)
        method = cache[className][item as string];
    else if (cache[className][lcItem] !== undefined)
        method = cache[className][lcItem];
    else if (cacheStatic[className][item as string] !== undefined) {
        method = cacheStatic[className][item as string];
        isStatic = true;
    } else if (cacheStatic[className][lcItem] !== undefined) {
        method = cacheStatic[className][lcItem];
        isStatic = true;
    } else {
        if (isDefinedTest) return false;

        if (ignoreStrictCheck || !env.isStrictVariables()) return;

        throw new RuntimeError(
            `Neither the property "${item}" nor one of the methods "${item}()", "get${item}()", "is${item}()" or "has${item}()" exist and have public access in class "${className}".`,
            lineno,
            source
        );
    }

    if (isDefinedTest) return true;

    if (sandboxed)
        (
            env.getExtension('SandboxExtension') as SandboxExtension
        ).checkMethodAllowed(object as object, method, lineno, source);

    if (isStatic)
        return (
            (object as object).constructor as unknown as Record<
                string,
                (...methodArgs: unknown[]) => unknown
            >
        )[method](...args);

    return (object as Record<string, (...methodArgs: unknown[]) => unknown>)[
        method
    ](...args);
}

function twigArrayColumn(
    array:
        | Record<string | number | symbol, unknown>[]
        | Iterable<Record<string | number | symbol, unknown>>,
    name: string | number | symbol
) {
    if (!twigTestIterable(array))
        throw new RuntimeError(
            `The column filter only works with iterable, got "${typeof array}" as first argument.`
        );

    const ret: unknown[] = [];
    if (!Array.isArray(array))
        array = [
            ...(array as Iterable<Record<string | number | symbol, unknown>>),
        ];
    (array as Record<string | number | symbol, unknown>[]).forEach(item => {
        if (item !== null && item !== undefined && item[name] !== undefined)
            ret.push(item[name]);
    });
    return ret;
}

function twigArrayFilter(
    env: Environment,
    array: unknown[] | Record<string, unknown> | Iterable<unknown>,
    arrow: (
        value: unknown,
        index?: string | number | symbol,
        arr?: unknown[] | Record<string, unknown> | Iterable<unknown>
    ) => unknown
) {
    if (!twigTestIterable(array))
        throw new RuntimeError(
            `The "filter" filter expects an array, got "${typeof array}".`
        );

    twigCheckArrowInSandbox(
        env,
        arrow as (...args: unknown[]) => unknown,
        'filter',
        'filter'
    );

    if (Array.isArray(array)) return array.filter(arrow);
    else if (
        array !== null &&
        typeof array === 'object' &&
        typeof (array as Record<symbol, unknown>)[Symbol.iterator] ===
            'function'
    )
        return [...(array as Iterable<unknown>)].filter(arrow);
    else if (array !== null && typeof array === 'object') {
        const result: Record<string, unknown> = {};
        Object.keys(array).forEach(key => {
            if (arrow((array as Record<string, unknown>)[key], key, array))
                result[key] = (array as Record<string, unknown>)[key];
        });
        return result;
    } else return array;
}

function twigArrayMap<
    T extends unknown[] | Record<K, T>,
    K extends string | number | symbol
>(
    env: Environment,
    array: T,
    arrow: (value: T, index: K, iterable: T) => unknown
): T {
    twigCheckArrowInSandbox(
        env,
        arrow as (...args: unknown[]) => unknown,
        'map',
        'filter'
    );

    if (!twigTestIterable(array))
        throw new RuntimeError(
            `The "map" filter expects an iterable value, got "${
                array === null ? 'null' : typeof array
            }".`
        );

    const r = (Array.isArray(array) ? [] : {}) as T;
    forEach(array, (v, k) => {
        (r as Record<K, T>)[k as K] = arrow(v, k as K, array) as T;
    });

    return r;
}

function twigArrayReduce(
    env: Environment,
    array: unknown[],
    arrow: (
        previousValue: unknown,
        currentValue: unknown,
        index?: number,
        arr?: unknown[]
    ) => unknown,
    initial: unknown = null
) {
    twigCheckArrowInSandbox(
        env,
        arrow as (...args: unknown[]) => unknown,
        'reduce',
        'filter'
    );

    if (Array.isArray(array)) return array.reduce(arrow, initial);
    else if (
        array !== null &&
        typeof array === 'object' &&
        typeof (array as Record<symbol, unknown>)[Symbol.iterator] ===
            'function'
    )
        return [...array].reduce(arrow, initial);
    else
        throw new RuntimeError(
            `The "reduce" filter expects an array or an Iterable object, got "${typeof array}".`
        );
}

export class CoreExtension extends AbstractExtension {
    private _dateFormats: string[] = ['F j, Y H:i', '%d days'];

    private _numberFormat: [number, string, string] = [0, '.', ','];

    private _timezone: string | null = null;

    setDateFormat(
        format: string | null = null,
        dateIntervalFormat: string | null = null
    ) {
        if (format !== null) this._dateFormats[0] = format;

        if (dateIntervalFormat !== null)
            this._dateFormats[1] = dateIntervalFormat;
    }

    getDateFormat() {
        return this._dateFormats;
    }

    setTimezone(timezone: string) {
        this._timezone = timezone;
    }

    getTimezone() {
        if (this._timezone === null)
            this._timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

        return this._timezone;
    }

    setNumberFormat(
        decimal: number,
        decimalPoint: string,
        thousandSep: string
    ) {
        this._numberFormat = [decimal, decimalPoint, thousandSep];
    }

    getNumberFormat() {
        return this._numberFormat;
    }

    getTokenParsers() {
        return {
            0: new ApplyTokenParser(),
            1: new ForTokenParser(),
            2: new IfTokenParser(),
            3: new ExtendsTokenParser(),
            4: new IncludeTokenParser(),
            5: new BlockTokenParser(),
            6: new UseTokenParser(),
            7: new MacroTokenParser(),
            8: new ImportTokenParser(),
            9: new FromTokenParser(),
            10: new SetTokenParser(),
            12: new DoTokenParser(),
            13: new EmbedTokenParser(),
            14: new WithTokenParser(),
            15: new DeprecatedTokenParser(),
        };
    }

    getFilters() {
        return {
            // formatting filters
            date: new TwigFilter(
                'date',
                twigDateFormatFilter as TwigFilterType,
                {
                    needsEnvironment: true,
                }
            ),
            date_modify: new TwigFilter(
                'date_modify',
                twigDateModifyFilter as TwigFilterType,
                {
                    needsEnvironment: true,
                }
            ),
            format: new TwigFilter('format', twigSprintf as TwigFilterType),
            replace: new TwigFilter(
                'replace',
                twigReplaceFilter as TwigFilterType
            ),
            number_format: new TwigFilter(
                'number_format',
                twigNumberFormatFilter as TwigFilterType,
                {
                    needsEnvironment: true,
                }
            ),
            abs: new TwigFilter('abs', Math.abs as TwigFilterType),
            round: new TwigFilter('round', twigRound as TwigFilterType),

            // encoding
            url_encode: new TwigFilter(
                'url_encode',
                twigUrlencodeFilter as TwigFilterType
            ),
            json_encode: new TwigFilter(
                'json_encode',
                JSON.stringify as TwigFilterType
            ),

            // string filters
            title: new TwigFilter(
                'title',
                twigTitleStringFilter as TwigFilterType,
                {
                    needsEnvironment: true,
                }
            ),
            capitalize: new TwigFilter(
                'capitalize',
                twigCapitalizeStringFilter as TwigFilterType,
                {
                    needsEnvironment: true,
                }
            ),
            upper: new TwigFilter('upper', twigUpperFilter as TwigFilterType, {
                needsEnvironment: true,
            }),
            lower: new TwigFilter('lower', twigLowerFilter as TwigFilterType, {
                needsEnvironment: true,
            }),
            striptags: new TwigFilter(
                'striptags',
                twigStriptags as TwigFilterType
            ),
            trim: new TwigFilter('trim', twigTrimFilter as TwigFilterType),
            nl2br: new TwigFilter('nl2br', twigNl2br as TwigFunctionType, {
                preEscape: 'html',
                isSafe: ['html'],
            }),
            spaceless: new TwigFilter(
                'spaceless',
                twigSpaceless as TwigFilterType,
                { isSafe: ['html'] }
            ),

            // array helpers
            join: new TwigFilter('join', twigJoinFilter as TwigFilterType),
            split: new TwigFilter('split', twigSplitFilter as TwigFilterType),
            sort: new TwigFilter('sort', twigSortFilter as TwigFilterType, {
                needsEnvironment: true,
            }),
            merge: new TwigFilter('merge', twigArrayMerge as TwigFilterType),
            batch: new TwigFilter('batch', twigArrayBatch as TwigFilterType),
            column: new TwigFilter('column', twigArrayColumn as TwigFilterType),
            filter: new TwigFilter(
                'filter',
                twigArrayFilter as TwigFilterType,
                {
                    needsEnvironment: true,
                }
            ),
            map: new TwigFilter('map', twigArrayMap as TwigFilterType, {
                needsEnvironment: true,
            }),
            reduce: new TwigFilter(
                'reduce',
                twigArrayReduce as TwigFilterType,
                {
                    needsEnvironment: true,
                }
            ),

            // string/array filters
            reverse: new TwigFilter(
                'reverse',
                twigReverseFilter as TwigFilterType
            ),
            length: new TwigFilter(
                'length',
                twigLengthFilter as TwigFilterType
            ),
            slice: new TwigFilter('slice', twigSlice as TwigFunctionType),
            first: new TwigFilter('first', twigFirst),
            last: new TwigFilter('last', twigLast),

            // iteration and runtime
            default: new TwigFilter(
                'default',
                twigDefaultFilter as TwigFilterType,
                {
                    nodeClass: DefaultFilter,
                }
            ),
            keys: new TwigFilter('keys', objectKeys as TwigFunctionType),
        };
    }

    getFunctions() {
        return {
            attribute: new TwigFunction(
                'attribute',
                twigGetAttribute as TwigFunctionType
            ),
            max: new TwigFunction('max', twigMax as TwigFunctionType),
            min: new TwigFunction('min', twigMin as TwigFunctionType),
            range: new TwigFunction('range', range as TwigFunctionType),
            cycle: new TwigFunction('cycle', twigCycle as TwigFunctionType),
            random: new TwigFunction('random', twigRandom as TwigFunctionType),
            date: new TwigFunction(
                'date',
                twigDateConverter as TwigFunctionType,
                {
                    needsEnvironment: true,
                }
            ),
            include: new TwigFunction(
                'include',
                twigInclude as TwigFilterType,
                {
                    needsEnvironment: true,
                    needsContext: true,
                    isSafe: ['all'],
                }
            ),
            source: new TwigFunction('source', twigSource as TwigFunctionType, {
                needsEnvironment: true,
                isSafe: ['all'],
            }),
            twigCallMacro: new TwigFunction(
                'twigCallMacro',
                twigCallMacro as TwigFunctionType
            ),
            twigForEach: new TwigFunction(
                'twigForEach',
                forEach as TwigFunctionType
            ),
            twigInFilter: new TwigFunction(
                'twigInFilter',
                twigInFilter as TwigFunctionType
            ),
            twigStrToRegExp: new TwigFunction(
                'twigStrToRegExp',
                str2RegExp as TwigFunctionType
            ),
            twigToArray: new TwigFunction(
                'twigToArray',
                twigToArray as TwigFunctionType
            ),
        };
    }

    getTests() {
        return {
            even: new TwigTest('even', null, { nodeClass: EvenTest }),
            odd: new TwigTest('odd', null, { nodeClass: OddTest }),
            defined: new TwigTest('defined', null, { nodeClass: DefinedTest }),
            sameAs: new TwigTest('same as', null, {
                nodeClass: SameasTest,
                oneMandatoryArgument: true,
            }),
            none: new TwigTest('none', null, { nodeClass: NullTest }),
            null: new TwigTest('null', null, { nodeClass: NullTest }),
            divisibleBy: new TwigTest('divisible by', null, {
                nodeClass: DivisiblebyTest,
                oneMandatoryArgument: true,
            }),
            empty: new TwigTest('empty', twigTestEmpty),
            iterable: new TwigTest('iterable', twigTestIterable),
        };
    }

    getNodeVisitors() {
        return [new MacroAutoImportNodeVisitor()] as NodeVisitorInterface[];
    }

    getOperators() {
        return [
            {
                not: {
                    precedence: 50,
                    class: NotUnary,
                },
                '-': {
                    precedence: 500,
                    class: NegUnary,
                },
                '+': {
                    precedence: 500,
                    class: PosUnary,
                },
            } as unknown as Record<string, Operator>,
            {
                or: {
                    precedence: 10,
                    class: OrBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                and: {
                    precedence: 15,
                    class: AndBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                'b-or': {
                    precedence: 16,
                    class: BitwiseOrBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                'b-xor': {
                    precedence: 17,
                    class: BitwiseXorBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                'b-and': {
                    precedence: 18,
                    class: BitwiseAndBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '==': {
                    precedence: 20,
                    class: EqualBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '!=': {
                    precedence: 20,
                    class: NotEqualBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '<=>': {
                    precedence: 20,
                    class: SpaceshipBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '<': {
                    precedence: 20,
                    class: LessBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '>': {
                    precedence: 20,
                    class: GreaterBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '>=': {
                    precedence: 20,
                    class: GreaterEqualBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '<=': {
                    precedence: 20,
                    class: LessEqualBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                'not in': {
                    precedence: 20,
                    class: NotInBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                in: {
                    precedence: 20,
                    class: InBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                matches: {
                    precedence: 20,
                    class: MatchesBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                'starts with': {
                    precedence: 20,
                    class: StartsWithBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                'ends with': {
                    precedence: 20,
                    class: EndsWithBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '..': {
                    precedence: 25,
                    class: RangeBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '+': {
                    precedence: 30,
                    class: AddBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '-': {
                    precedence: 30,
                    class: SubBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '~': {
                    precedence: 40,
                    class: ConcatBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '*': {
                    precedence: 60,
                    class: MulBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '/': {
                    precedence: 60,
                    class: DivBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '//': {
                    precedence: 60,
                    class: FloorDivBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '%': {
                    precedence: 60,
                    class: ModBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                is: {
                    precedence: 100,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                'is not': {
                    precedence: 100,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '**': {
                    precedence: 200,
                    class: PowerBinary,
                    associativity: ExpressionParser.OPERATOR_RIGHT,
                },
                '??': {
                    precedence: 300,
                    class: NullCoalesceExpression,
                    associativity: ExpressionParser.OPERATOR_RIGHT,
                },
            } as unknown as Record<string, Operator>,
        ];
    }
}
