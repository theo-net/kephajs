/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { TokenParserInterface } from '../TokenParser/TokenParserInterface';
import { TwigFilter } from '../TwigFilter';
import { TwigFunction } from '../TwigFunction';
import { TwigTest } from '../TwigTest';
import { AbstractExtension } from './AbstractExtension';

export class StagingExtension extends AbstractExtension {
    private _functions: Record<string, TwigFunction> = {};

    private _filters: Record<string, TwigFilter> = {};

    private _visitors = [];

    private _tokenParsers: Record<string, TokenParserInterface> = {};

    private _tests: Record<string, TwigTest> = {};

    addFunction(fct: TwigFunction) {
        if (this._functions[fct.getName()] !== undefined)
            throw new Error(
                `Function "${fct.getName()}" is already registered.`
            );

        this._functions[fct.getName()] = fct;
    }

    getFunctions() {
        return this._functions;
    }

    addFilter(filter: TwigFilter) {
        if (this._filters[filter.getName()] !== undefined)
            throw new Error(
                `Filter "${filter.getName()}" is already registered.`
            );

        this._filters[filter.getName()] = filter;
    }

    getFilters() {
        return this._filters;
    }

    /*addNodeVisitor(NodeVisitorInterface $visitor): void
    {
        this._visitors[] = $visitor;
    }*/

    getNodeVisitors() {
        return this._visitors;
    }

    addTokenParser(parser: TokenParserInterface) {
        if (this._tokenParsers[parser.getTag()] !== undefined)
            throw new Error(`Tag "${parser.getTag()}" is already registered.`);

        this._tokenParsers[parser.getTag()] = parser;
    }

    getTokenParsers() {
        return this._tokenParsers;
    }

    addTest(test: TwigTest) {
        if (this._tests[test.getName()] !== undefined)
            throw new Error(`Test "${test.getName()}" is already registered.`);

        this._tests[test.getName()] = test;
    }

    getTests() {
        return this._tests;
    }
}
