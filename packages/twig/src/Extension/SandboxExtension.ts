/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { SecurityNotAllowedMethodError } from '../Sandbox/SecurityNotAllowedMethodError';
import { SecurityNotAllowedPropertyError } from '../Sandbox/SecurityNotAllowedPropertyError';
import { SecurityPolicyInterface } from '../Sandbox/SecurityPolicyInterface';
import { Source } from '../Source';
import { AbstractExtension } from './AbstractExtension';

export class SandboxExtension extends AbstractExtension {
    private _sandboxedGlobally = false;

    private _sandboxed = false;

    constructor(private _policy: SecurityPolicyInterface, sandboxed = false) {
        super();
        this._sandboxedGlobally = sandboxed;
    }
    /*public function getTokenParsers(): array
    {
        return [new SandboxTokenParser()];
    }

    public function getNodeVisitors(): array
    {
        return [new SandboxNodeVisitor()];
    }*/

    enableSandbox() {
        this._sandboxed = true;
    }

    disableSandbox() {
        this._sandboxed = false;
    }

    isSandboxed() {
        return this._sandboxedGlobally || this._sandboxed;
    }

    isSandboxedGlobally() {
        return this._sandboxedGlobally;
    }

    /*public function setSecurityPolicy(SecurityPolicyInterface $policy)
    {
        $this->policy = $policy;
    }

    public function getSecurityPolicy(): SecurityPolicyInterface
    {
        return $this->policy;
    }

    public function checkSecurity($tags, $filters, $functions): void
    {
        if ($this->isSandboxed()) {
            $this->policy->checkSecurity($tags, $filters, $functions);
        }
    }*/

    checkMethodAllowed(
        obj: object,
        method: string,
        lineno = -1,
        source: null | Source = null
    ) {
        if (this.isSandboxed()) {
            try {
                this._policy.checkMethodAllowed(obj, method);
            } catch (error) {
                if (error instanceof SecurityNotAllowedMethodError) {
                    error.setSourceContext(source);
                    error.setTemplateLine(lineno);
                }

                throw error;
            }
        }
    }

    checkPropertyAllowed(
        obj: object,
        property: string,
        lineno = -1,
        source: null | Source = null
    ) {
        if (this.isSandboxed()) {
            try {
                this._policy.checkPropertyAllowed(obj, property);
            } catch (error) {
                if (error instanceof SecurityNotAllowedPropertyError) {
                    error.setSourceContext(source);
                    error.setTemplateLine(lineno);
                }

                throw error;
            }
        }
    }

    /*public function ensureToStringAllowed($obj, int $lineno = -1, Source $source = null)
    {
        if ($this->isSandboxed() && \is_object($obj) && method_exists($obj, '__toString')) {
            try {
                $this->policy->checkMethodAllowed($obj, '__toString');
            } catch (SecurityNotAllowedMethodError $e) {
                $e->setSourceContext($source);
                $e->setTemplateLine($lineno);

                throw $e;
            }
        }

        return $obj;
    }*/
}
