/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { NodeVisitorInterface } from '../NodeVisitor/NodeVisitorInterface';
import { AbstractBinary } from '../Node/Expression/Binary/AbstractBinary';
import { AbstractExpression } from '../Node/Expression/AbstractExpression';
import { Parser } from '../Parser';
import { TwigFilter } from '../TwigFilter';
import { TokenParserInterface } from '../TokenParser/TokenParserInterface';
import { TwigFunction } from '../TwigFunction';
import { AbstractUnary } from '../Node/Expression/Unary/AbstractUnary';
import { TwigTest } from '../TwigTest';

export type Operator = {
    precedence: number;
    associativity?: number;
    class?: typeof AbstractBinary | typeof AbstractUnary;
    callable?: (parser: Parser, expr: AbstractExpression) => AbstractExpression;
};

export interface ExtensionInterface {
    getTokenParsers(): Record<string, TokenParserInterface>;

    getNodeVisitors(): NodeVisitorInterface[];

    getFilters(): Record<string, TwigFilter>;

    getTests(): Record<string, TwigTest>;

    getFunctions(): Record<string, TwigFunction>;

    getOperators(): Record<string, Operator>[];
}
