/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import {
    twigArrayBatch,
    twigFirst,
    twigInFilter,
    twigLast,
    twigRandom,
    twigSlice,
} from './CoreExtension';
import { RuntimeError } from '../Error/RuntimeError';
import { objectKeys, range } from '@kephajs/core/Helpers/Helpers';
import { Markup } from '../Markup';

describe('@kephajs/twig/Extension/CoreExtension', () => {
    describe('random', () => {
        it('Should return random result', () => {
            (
                [
                    [
                        ['apple', 'orange', 'citrus'],
                        ['apple', 'orange', 'citrus'],
                    ],
                    [['Ä', '€', 'é'], 'Ä€é'],
                    [['1', '2', '3'], '123'],
                    [[0, 1, 2, 3, 4, 5], 5],
                    [[0, 1, 2, 3, 4, 5], 5.9],
                    [[0, -1, -2], -2],
                    [range(50, 100), 50, 100],
                    [range(-10, 10), -9.5, 9.5],
                    [range(0, 100), null, 100],
                ] as [
                    unknown[],
                    string | number | unknown[],
                    (number | null)?
                ][]
            ).forEach(test => {
                for (let i = 0; i < 100; ++i) {
                    const random = twigRandom(test[1], test[2]);
                    expect(test[0].indexOf(random) > -1).to.be.equal(true);
                }
            });
        });

        it('Should work without parameter: return an number', () => {
            for (let i = 0; i < 100; ++i) {
                const random = twigRandom();
                expect(
                    typeof random === 'number' &&
                        random >= 0 &&
                        random <= Number.MAX_VALUE
                ).to.be.equal(true);
            }
        });

        it('Should work with hash', () => {
            expect(
                (['a', 'b', 'c'] as unknown[]).indexOf(
                    twigRandom({ foo: 'a', bar: 'b', foobar: 'c' })
                ) > -1
            ).to.be.equal(true);
        });

        it('Should return same value if is not an iterable or a number or is an empty string', () => {
            expect(twigRandom('')).to.be.equal('');
        });

        it('Should throw a RuntimeError for empty array', () => {
            try {
                twigRandom([]);
            } catch (error) {
                expect(error).to.be.instanceOf(RuntimeError);
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    class CoreTestIterator implements Iterable<unknown> {
        constructor(
            private _array: Record<string, unknown>,
            private _arrayKeys: (string | number)[]
        ) {}

        [Symbol.iterator]() {
            const length = this._arrayKeys.length;
            const array = this._array;
            const keys = this._arrayKeys;
            let counter = -1;

            return {
                next(): IteratorResult<unknown> {
                    if (++counter < length) {
                        return {
                            done: false,
                            value: array[keys[counter]],
                        };
                    } else {
                        return {
                            done: true,
                            value: undefined,
                        };
                    }
                },
            };
        }

        keys() {
            return this._arrayKeys;
        }
    }

    describe('slice', () => {
        it('Should return a sliced value', () => {
            const i = { a: 1, b: 2, c: 3, d: 4 };
            const keys = objectKeys(i);

            (
                [
                    [[1], [1, 2, 3, 4], 0, 1],
                    [[2, 3], [1, 2, 3, 4], 1, 2],
                    [[2, 3], new CoreTestIterator(i, keys), 1, 2],
                    [
                        i,
                        new CoreTestIterator(i, keys),
                        0,
                        keys.length + 10,
                        true,
                    ],
                    [[], new CoreTestIterator(i, keys), keys.length + 10],
                    ['de', 'abcdef', 3, 2],
                    [{ a: 1 }, i, 0, 1, true],
                    [{ a: 1 }, i, 0, 1, false],
                    [{ b: 2, c: 3 }, i, 1, 2],
                ] as [unknown, unknown, number, number, boolean?][]
            ).forEach(test => {
                expect(
                    twigSlice(test[1], test[2], test[3], test[4])
                ).to.be.deep.equal(test[0]);
            });
        });

        it('Should work with no length', () => {
            expect(
                twigSlice(
                    new CoreTestIterator(
                        { 0: 1, 1: 2, 2: 3, 3: 4 },
                        [0, 1, 2, 3]
                    ),
                    1
                )
            ).to.be.deep.equal([2, 3, 4]);
            expect(twigSlice([1, 2, 3, 4], 1)).to.be.deep.equal([2, 3, 4]);
            expect(twigSlice('1234', 1)).to.be.deep.equal('234');
        });

        it('Should work with negative start', () => {
            expect(
                twigSlice(
                    new CoreTestIterator(
                        { 0: 1, 1: 2, 2: 3, 3: 4 },
                        [0, 1, 2, 3]
                    ),
                    -3,
                    2
                )
            ).to.be.deep.equal([2, 3]);
            expect(twigSlice([1, 2, 3, 4], -3, 2)).to.be.deep.equal([2, 3]);
            expect(twigSlice('1234', -3, 2)).to.be.deep.equal('23');
            expect(
                twigSlice({ a: 1, b: 2, c: 3, d: 4 }, -3, 2)
            ).to.be.deep.equal({ b: 2, c: 3 });
        });
    });

    describe('first', () => {
        it('Should return the first element', () => {
            const i = { 1: 'a', 2: 'b', 3: 'c' };
            [
                ['a', 'abc'],
                [1, [1, 2, 3]],
                ['', null],
                ['', ''],
                ['a', new CoreTestIterator(i, objectKeys(i))],
            ].forEach(test => {
                expect(twigFirst(test[1])).to.be.deep.equal(test[0]);
            });
        });

        it('Should return the first element of a Record', () => {
            [[1, { a: 1, b: 2, c: 3, d: 4 }]].forEach(test => {
                expect(twigFirst(test[1])).to.be.deep.equal(test[0]);
            });
        });
    });

    describe('last', () => {
        it('Should return the last element', () => {
            const i = { 1: 'a', 2: 'b', 3: 'c' };
            [
                [3, [1, 2, 3]],
                ['', null],
                ['', ''],
                ['c', new CoreTestIterator(i, objectKeys(i))],
                ['c', 'abc'],
            ].forEach(test => {
                expect(twigLast(test[1])).to.be.deep.equal(test[0]);
            });
        });

        it('Should return the last element of a Record', () => {
            [[4, { a: 1, b: 2, c: 3, d: 4 }]].forEach(test => {
                expect(twigLast(test[1])).to.be.deep.equal(test[0]);
            });
        });
    });

    describe('twigInFilter', () => {
        it('Should return true if an element in the items', () => {
            const array = [1, 2, 3];
            const record = { a: 1, b: 2, c: 3 };
            const keys = objectKeys(record);

            [
                [true, '3', 'abc3def'],
                [true, 1, array],
                [true, 1, record],
                [true, 1, new CoreTestIterator(record, keys)],
                [false, 4, array],
                [false, 4, new CoreTestIterator(record, keys)],
                [false, 1, 1],
            ].forEach(test => {
                expect(twigInFilter(test[1], test[2])).to.be.deep.equal(
                    test[0]
                );
            });
        });

        it('Should convert to string if needed', () => {
            const array = [1, '2', false];
            const record = { a: 1, b: '2', c: 'false' };
            const keys = objectKeys(record);

            [
                [true, '1', array],
                [true, 2, array],
                [true, 'false', array],
                [true, '1', record],
                [true, 2, record],
                [true, 'false', record],
                [true, '1', new CoreTestIterator(record, keys)],
                [true, 2, new CoreTestIterator(record, keys)],
                [true, 'false', new CoreTestIterator(record, keys)],
            ].forEach(test => {
                expect(twigInFilter(test[1], test[2])).to.be.deep.equal(
                    test[0]
                );
            });
        });

        it('Should take the string from Markup', () => {
            expect(twigInFilter(new Markup('foo'), ['foo', 'bar'])).to.be.equal(
                true
            );
            expect(twigInFilter('fo', new Markup('foo'))).to.be.equal(true);
        });
    });

    describe('twigArrayBatch', () => {
        it('Should batch an array', () => {
            expect(twigArrayBatch([1, 2, 3, 4, 5, 6], 3)).to.be.deep.equal([
                [1, 2, 3],
                [4, 5, 6],
            ]);
        });

        it('Should batch a Record', () => {
            expect(
                twigArrayBatch({ a: 1, b: 2, c: 3, d: 4, e: 5, f: 6 }, 3)
            ).to.be.deep.equal([
                { a: 1, b: 2, c: 3 },
                { d: 4, e: 5, f: 6 },
            ]);
        });

        it('Should batch an Iterable', () => {
            expect(
                twigArrayBatch(
                    new CoreTestIterator(
                        { a: 1, b: 2, c: 3, d: 4, e: 5, f: 6 },
                        ['a', 'b', 'c', 'd', 'e', 'f']
                    ),
                    3
                )
            ).to.be.deep.equal([
                [1, 2, 3],
                [4, 5, 6],
            ]);
        });

        it('Should not fill the empty cell', () => {
            expect(twigArrayBatch([1, 2, 3, 4, 5], 3)).to.be.deep.equal([
                [1, 2, 3],
                [4, 5],
            ]);
            expect(
                twigArrayBatch({ a: 1, b: 2, c: 3, d: 4, e: 5 }, 3)
            ).to.be.deep.equal([
                { a: 1, b: 2, c: 3 },
                { d: 4, e: 5 },
            ]);
            expect(
                twigArrayBatch(
                    new CoreTestIterator({ a: 1, b: 2, c: 3, d: 4, e: 5 }, [
                        'a',
                        'b',
                        'c',
                        'd',
                        'e',
                    ]),
                    3
                )
            ).to.be.deep.equal([
                [1, 2, 3],
                [4, 5],
            ]);
        });

        it('Should fill the empty cell', () => {
            expect(twigArrayBatch([1, 2, 3, 4, 5], 3, 42)).to.be.deep.equal([
                [1, 2, 3],
                [4, 5, 42],
            ]);
            expect(
                twigArrayBatch({ a: 1, b: 2, c: 3, d: 4, e: 5 }, 3, 42)
            ).to.be.deep.equal([
                { a: 1, b: 2, c: 3 },
                { d: 4, e: 5, 0: 42 },
            ]);
            expect(
                twigArrayBatch(
                    new CoreTestIterator({ a: 1, b: 2, c: 3, d: 4, e: 5 }, [
                        'a',
                        'b',
                        'c',
                        'd',
                        'e',
                    ]),
                    3,
                    42
                )
            ).to.be.deep.equal([
                [1, 2, 3],
                [4, 5, 42],
            ]);
        });
    });
});
