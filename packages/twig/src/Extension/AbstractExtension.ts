/*
 * This file is part of Twig.
/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { NodeVisitorInterface } from '../NodeVisitor/NodeVisitorInterface';
import { TokenParserInterface } from '../TokenParser/TokenParserInterface';
import { TwigFilter } from '../TwigFilter';
import { TwigFunction } from '../TwigFunction';
import { TwigTest } from '../TwigTest';
import { ExtensionInterface, Operator } from './ExtensionInterface';

export abstract class AbstractExtension implements ExtensionInterface {
    getTokenParsers(): Record<string, TokenParserInterface> {
        return {};
    }

    getNodeVisitors(): NodeVisitorInterface[] {
        return [];
    }

    getFilters(): Record<string, TwigFilter> {
        return {};
    }

    getTests(): Record<string, TwigTest> {
        return {};
    }

    getFunctions(): Record<string, TwigFunction> {
        return {};
    }

    getOperators() {
        return [{} as Record<string, Operator>, {} as Record<string, Operator>];
    }
}
