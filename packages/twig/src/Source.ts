/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Holds information about a non-compiled Twig template.
 */
export class Source {
    constructor(
        private _code: string,
        private _name: string,
        private _path = ''
    ) {}

    get code() {
        return this._code;
    }

    get name() {
        return this._name;
    }

    get path() {
        return this._path;
    }
}
