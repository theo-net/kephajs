/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

export class Token {
    public static readonly EOF_TYPE = -1;

    public static readonly TEXT_TYPE = 0;

    public static readonly BLOCK_START_TYPE = 1;

    public static readonly VAR_START_TYPE = 2;

    public static readonly BLOCK_END_TYPE = 3;

    public static readonly VAR_END_TYPE = 4;

    public static readonly NAME_TYPE = 5;

    public static readonly NUMBER_TYPE = 6;

    public static readonly STRING_TYPE = 7;

    public static readonly OPERATOR_TYPE = 8;

    public static readonly PUNCTUATION_TYPE = 9;

    public static readonly INTERPOLATION_START_TYPE = 10;

    public static readonly INTERPOLATION_END_TYPE = 11;

    public static readonly ARROW_TYPE = 12;

    constructor(
        private _type: number,
        private _value: string | number | bigint,
        private _lineno: number
    ) {}

    toString() {
        return `${Token.typeToString(this._type, true)}(${this._value})`;
    }

    test(type: unknown, values: unknown = null) {
        if (values === null && !Number.isInteger(type)) {
            values = type;
            type = Token.NAME_TYPE;
        }

        return (
            this._type === type &&
            (values === null ||
                (Array.isArray(values) && values.indexOf(this._value) > -1) ||
                this._value == values)
        );
    }

    get line() {
        return this._lineno;
    }

    get type() {
        return this._type;
    }

    get value() {
        return this._value;
    }

    static typeToString(type: number, short = false) {
        let typeName = '';
        switch (type) {
            case Token.EOF_TYPE:
                typeName = 'EOF_TYPE';
                break;
            case Token.TEXT_TYPE:
                typeName = 'TEXT_TYPE';
                break;
            case Token.BLOCK_START_TYPE:
                typeName = 'BLOCK_START_TYPE';
                break;
            case Token.VAR_START_TYPE:
                typeName = 'VAR_START_TYPE';
                break;
            case Token.BLOCK_END_TYPE:
                typeName = 'BLOCK_END_TYPE';
                break;
            case Token.VAR_END_TYPE:
                typeName = 'VAR_END_TYPE';
                break;
            case Token.NAME_TYPE:
                typeName = 'NAME_TYPE';
                break;
            case Token.NUMBER_TYPE:
                typeName = 'NUMBER_TYPE';
                break;
            case Token.STRING_TYPE:
                typeName = 'STRING_TYPE';
                break;
            case Token.OPERATOR_TYPE:
                typeName = 'OPERATOR_TYPE';
                break;
            case Token.PUNCTUATION_TYPE:
                typeName = 'PUNCTUATION_TYPE';
                break;
            case Token.INTERPOLATION_START_TYPE:
                typeName = 'INTERPOLATION_START_TYPE';
                break;
            case Token.INTERPOLATION_END_TYPE:
                typeName = 'INTERPOLATION_END_TYPE';
                break;
            case Token.ARROW_TYPE:
                typeName = 'ARROW_TYPE';
                break;
            default:
                throw new TypeError(`Token of type "${type}" does not exist.`);
        }

        return short ? typeName : 'Twig/Token.' + typeName;
    }

    static typeToEnglish(type: number) {
        switch (type) {
            case Token.EOF_TYPE:
                return 'end of template';
            case Token.TEXT_TYPE:
                return 'text';
            case Token.BLOCK_START_TYPE:
                return 'begin of statement block';
            case Token.VAR_START_TYPE:
                return 'begin of print statement';
            case Token.BLOCK_END_TYPE:
                return 'end of statement block';
            case Token.VAR_END_TYPE:
                return 'end of print statement';
            case Token.NAME_TYPE:
                return 'name';
            case Token.NUMBER_TYPE:
                return 'number';
            case Token.STRING_TYPE:
                return 'string';
            case Token.OPERATOR_TYPE:
                return 'operator';
            case Token.PUNCTUATION_TYPE:
                return 'punctuation';
            case Token.INTERPOLATION_START_TYPE:
                return 'begin of string interpolation';
            case Token.INTERPOLATION_END_TYPE:
                return 'end of string interpolation';
            case Token.ARROW_TYPE:
                return 'arrow function';
            default:
                throw new TypeError(`Token of type "${type}" does not exist.`);
        }
    }
}
