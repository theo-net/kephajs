/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Environment } from './Environment';
import { SyntaxError } from './Error/SyntaxError';
import { ExpressionParser } from './ExpressionParser';
import { BlockNode } from './Node/BlockNode';
import { BlockReferenceNode } from './Node/BlockReferenceNode';
import { BodyNode } from './Node/BodyNode';
import { AbstractExpression } from './Node/Expression/AbstractExpression';
import { MacroNode } from './Node/MacroNode';
import { ModuleNode } from './Node/ModuleNode';
import { NodeCaptureInterface } from './Node/NodeCaptureInterface';
import { NodeOutputInterface } from './Node/NodeOutputInterface';
import { PrintNode } from './Node/PrintNode';
import { TextNode } from './Node/TextNode';
import { TwigNode } from './Node/TwigNode';
import { NodeTraverser } from './NodeTraverser';
import { NodeVisitorInterface } from './NodeVisitor/NodeVisitorInterface';
import { Token } from './Token';
import { TokenParserInterface } from './TokenParser/TokenParserInterface';
import { TokenStream } from './TokenStream';

export class Parser {
    private _stack: Record<string, unknown> = {};

    private _stream = new TokenStream();

    private _parent: null | TwigNode = null;

    private _visitors: null | NodeVisitorInterface[] = null;

    private _expressionParser: ExpressionParser | null = null;

    private _blocks: Record<string, BlockNode> = {};

    private _blockStack: string[] = [];

    private _macros: Record<string, MacroNode> = {};

    private _importedSymbols: Record<
        string,
        Record<string, { name: null | string; node: null | AbstractExpression }>
    >[] = [{}];

    private _traits: TwigNode[] = [];

    private _embeddedTemplates: ModuleNode[] = [];

    private _varNameSalt = 0;

    constructor(private _env: Environment) {}

    getVarName() {
        return '__internal_parse_' + this._varNameSalt++;
    }

    parse(
        stream: TokenStream,
        test: null | ((token: Token) => boolean) = null,
        dropNeedle = false
    ) {
        this._saveStack();

        // node visitors
        if (this._visitors === null)
            this._visitors = this._env.getNodeVisitors();

        if (this._expressionParser === null)
            this._expressionParser = new ExpressionParser(this, this._env);

        this._stream = stream;
        this._parent = null;
        this._blocks = {};
        this._macros = {};
        this._blockStack = [];
        this._importedSymbols = [{}];
        this._traits = [];
        this._embeddedTemplates = [];

        let body: null | TwigNode;
        try {
            body = this.subparse(test, dropNeedle);

            if (
                this._parent !== null &&
                (body = this._filterBodyNodes(body)) === null
            )
                body = new TwigNode();
        } catch (error) {
            if (error instanceof SyntaxError) {
                if (!(error as SyntaxError).getSourceContext())
                    error.setSourceContext(this._stream.getSourceContext());

                if (!error.getTemplateLine())
                    error.setTemplateLine(this._stream.getCurrent().line);
            }

            throw error;
        }

        let node: TwigNode = new ModuleNode(
            this._env.getExportType(),
            new BodyNode([body]),
            this._parent,
            new TwigNode(this._blocks),
            new TwigNode(this._macros),
            new TwigNode(this._traits),
            this._embeddedTemplates,
            stream.getSourceContext()
        );

        const traverser = new NodeTraverser(this._env, this._visitors);

        node = traverser.traverse(node);

        // restore previous stack so previous parse() call can resume working
        this._restoreStack();

        return node;
    }

    private _saveStack() {
        this._stack._stream = this._stream;
        this._stack._parent = this._parent;
        this._stack._blocks = this._blocks;
        this._stack._blockStack = this._blockStack;
        this._stack._macros = this._macros;
        this._stack._importedSymbols = this._importedSymbols;
        this._stack._traits = this._traits;
        this._stack._embeddedTemplates = this._embeddedTemplates;
    }

    private _restoreStack() {
        this._stream = this._stack._stream as TokenStream;
        this._parent = this._stack._parent as null | TwigNode;
        this._blocks = this._stack._blocks as Record<string, BlockNode>;
        this._blockStack = this._stack._blockStack as string[];
        this._macros = this._stack._macros as Record<string, MacroNode>;
        this._importedSymbols = this._stack._importedSymbols as Record<
            string,
            Record<
                string,
                { name: null | string; node: null | AbstractExpression }
            >
        >[];
        this._traits = this._stack._traits as TwigNode[];
        this._embeddedTemplates = this._stack
            ._embeddedTemplates as ModuleNode[];
    }

    subparse(test: null | ((token: Token) => boolean), dropNeedle = false) {
        const lineno = this.getCurrentToken().line;
        const rv: TwigNode[] = [];
        while (!this._stream.isEOF()) {
            let token: Token;
            switch (this.getCurrentToken().type) {
                case Token.TEXT_TYPE:
                    token = this._stream.next();
                    rv.push(new TextNode(token.value + '', token.line));
                    break;

                case Token.VAR_START_TYPE:
                    token = this._stream.next();
                    // eslint-disable-next-line no-case-declarations
                    const expr = (
                        this._expressionParser as ExpressionParser
                    ).parseExpression();
                    this._stream.expect(Token.VAR_END_TYPE);
                    rv.push(new PrintNode(expr, token.line));
                    break;

                case Token.BLOCK_START_TYPE:
                    this._stream.next();
                    token = this.getCurrentToken();

                    if (Token.NAME_TYPE !== token.type) {
                        throw new SyntaxError(
                            'A block must start with a tag name.',
                            token.line,
                            this._stream.getSourceContext()
                        );
                    }

                    if (test !== null && test(token)) {
                        if (dropNeedle) this._stream.next();

                        if (rv.length === 1) return rv[0];

                        return new TwigNode(rv, {}, lineno);
                    }

                    // eslint-disable-next-line no-case-declarations
                    const subparser = this._env.getTokenParser(
                        token.value as string
                    );
                    if (!subparser) {
                        let error: SyntaxError;
                        if (test !== null) {
                            error = new SyntaxError(
                                'Unexpected "' + token.value + '" tag',
                                token.line,
                                this._stream.getSourceContext()
                            );

                            if (
                                Array.isArray(test) &&
                                test[0] !== undefined &&
                                test[0] instanceof TokenParserInterface
                            )
                                error.appendMessage(
                                    ' (expecting closing tag for the "' +
                                        test[0].getTag() +
                                        '" tag defined near line ' +
                                        lineno +
                                        ').'
                                );
                        } else {
                            error = new SyntaxError(
                                'Unknown "' + token.value + '" tag.',
                                token.line,
                                this._stream.getSourceContext()
                            );
                            error.addSuggestions(
                                token.value as string,
                                Object.keys(this._env.getTokenParsers())
                            );
                        }

                        throw error;
                    }

                    this._stream.next();

                    subparser.setParser(this);
                    // eslint-disable-next-line no-case-declarations
                    const node = subparser.parse(token);
                    if (node !== null) rv.push(node);
                    break;

                default:
                    throw new SyntaxError(
                        'Lexer or parser ended up in unsupported state.',
                        this.getCurrentToken().line,
                        this._stream.getSourceContext()
                    );
            }
        }

        if (rv.length === 1) return rv[0];

        return new TwigNode(rv, {}, lineno);
    }

    getBlockStack() {
        return this._blockStack;
    }

    peekBlockStack() {
        return this._blockStack[this._blockStack.length - 1] ?? null;
    }

    popBlockStack() {
        this._blockStack.pop();
    }

    pushBlockStack(blockName: string) {
        this._blockStack.push(blockName);
    }

    hasBlock(blockName: string) {
        return this._blocks[blockName] !== undefined;
    }

    getBlock(blockName: string) {
        return this._blocks[blockName];
    }

    setBlock(blockName: string, value: BlockNode) {
        this._blocks[blockName] = new BodyNode(
            [value],
            {},
            value.getTemplateLine()
        );
    }

    hasMacro(macroName: string) {
        return this._macros[macroName] !== undefined;
    }

    setMacro(macroName: string, node: MacroNode) {
        this._macros[macroName] = node;
    }

    addTrait(trait: TwigNode) {
        this._traits.push(trait);
    }

    hasTraits() {
        return this._traits.length > 0;
    }

    embedTemplate(template: ModuleNode) {
        template.setIndex(Math.floor(Math.random() * (9999999999 - 1)) + 1);

        this._embeddedTemplates.push(template);
    }

    addImportedSymbol(
        type: string,
        alias: string,
        symboleName: null | string = null,
        node: null | AbstractExpression = null
    ) {
        if (this._importedSymbols[0][type] === undefined)
            this._importedSymbols[0][type] = {};
        this._importedSymbols[0][type][alias] = { name: symboleName, node };
    }

    getImportedSymbol(type: string, alias: string) {
        // if the symbol does not exist in the current scope (0), try in the main/global scope (last index)
        return this._importedSymbols[0][type] &&
            this._importedSymbols[0][type][alias]
            ? this._importedSymbols[0][type][alias]
            : this._importedSymbols[this._importedSymbols.length - 1] &&
              this._importedSymbols[this._importedSymbols.length - 1][type] &&
              this._importedSymbols[this._importedSymbols.length - 1][type][
                  alias
              ]
            ? this._importedSymbols[this._importedSymbols.length - 1][type][
                  alias
              ]
            : null;
    }

    isMainScope() {
        return this._importedSymbols.length === 1;
    }

    pushLocalScope() {
        this._importedSymbols = [{}, ...this._importedSymbols];
    }

    popLocalScope() {
        this._importedSymbols.shift();
    }

    getExpressionParser(): ExpressionParser {
        return this._expressionParser as ExpressionParser;
    }

    getParent() {
        return this._parent;
    }

    setParent(parent: TwigNode) {
        this._parent = parent;
    }

    getStream() {
        return this._stream;
    }

    getCurrentToken() {
        return this._stream?.getCurrent();
    }

    private _filterBodyNodes(node: TwigNode, nested = false) {
        // check that the body does not contain non-empty output nodes
        if (
            (node instanceof TextNode &&
                !/^\s+$/.exec(node.getAttribute('data') as string)) ||
            (!(node instanceof TextNode) &&
                !(node instanceof BlockReferenceNode) &&
                node instanceof NodeOutputInterface)
        ) {
            throw new SyntaxError(
                'A template that extends another one cannot include content outside Twig blocks. Did you forget to put the content inside a {% block %} tag?',
                node.getTemplateLine(),
                this._stream.getSourceContext()
            );
        }

        // bypass nodes that "capture" the output
        if (node instanceof NodeCaptureInterface) {
            // a "block" tag in such a node will serve as a block definition AND be displayed in place as well
            return node;
        }

        // "block" tags that are not captured (see above) are only used for defining
        // the content of the block. In such a case, nesting it does not work as
        // expected as the definition is not part of the default template code flow.
        if (nested && node instanceof BlockReferenceNode)
            throw new SyntaxError(
                'A block definition cannot be nested under non-capturing nodes.',
                node.getTemplateLine(),
                this._stream.getSourceContext()
            );

        if (node instanceof NodeOutputInterface) return null;

        // here, nested means "being at the root level of a child template"
        // we need to discard the wrapping "TwigNode" for the "body" node
        nested = nested || node.constructor.name !== 'TwigNode';
        const nodes = node.getNodes();
        Object.keys(nodes).forEach(key => {
            if (
                nodes[key] !== undefined &&
                this._filterBodyNodes(nodes[key], nested) === null
            )
                node.removeNode(key);
        });

        return node;
    }
}
