/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Environment } from '../Environment';
import { ArrayExpression } from '../Node/Expression/ArrayExpression';
import { AssignNameExpression } from '../Node/Expression/AssignNameExpression';
import { ConstantExpression } from '../Node/Expression/ConstantExpression';
import { GetAttrExpression } from '../Node/Expression/GetAttrExpression';
import { MethodCallExpression } from '../Node/Expression/MethodCallExpression';
import { NameExpression } from '../Node/Expression/NameExpression';
import { ImportNode } from '../Node/ImportNode';
import { ModuleNode } from '../Node/ModuleNode';
import { TwigNode } from '../Node/TwigNode';
import { NodeVisitorInterface } from './NodeVisitorInterface';

export class MacroAutoImportNodeVisitor implements NodeVisitorInterface {
    private _inAModule = false;

    private _hasMacroCalls = false;

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    enterNode(node: TwigNode, _env: Environment) {
        if (node instanceof ModuleNode) {
            this._inAModule = true;
            this._hasMacroCalls = false;
        }

        return node;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    leaveNode(node: TwigNode, _env: Environment) {
        if (node instanceof ModuleNode) {
            this._inAModule = false;
            if (this._hasMacroCalls)
                node.getNode('constructorEnd').setNode(
                    '_auto_macro_import',
                    new ImportNode(
                        new NameExpression('_self', 0),
                        new AssignNameExpression('_self', 0),
                        0,
                        'import',
                        true
                    )
                );
        } else if (this._inAModule) {
            if (
                node instanceof GetAttrExpression &&
                node.getNode('node') instanceof NameExpression &&
                node.getNode('node').getAttribute('name') === '_self' &&
                node.getNode('attribute') instanceof ConstantExpression
            ) {
                this._hasMacroCalls = true;

                const name = node.getNode('attribute').getAttribute('value');
                node = new MethodCallExpression(
                    node.getNode('node'),
                    'macro_' + name,
                    node.getNode('arguments') as ArrayExpression,
                    node.getTemplateLine()
                );
                node.setAttribute('safe', true);
            }
        }

        return node;
    }

    getPriority() {
        // we must be ran before auto-escaping
        return -10;
    }
}
