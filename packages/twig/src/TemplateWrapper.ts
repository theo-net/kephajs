/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Environment } from './Environment';
import { BlockType, Template } from './Template';

export class TemplateWrapper {
    protected _output: string | undefined = undefined;

    protected _outputStack: (string | undefined)[] = [];

    constructor(protected _env: Environment, protected _template: Template) {}

    print(output: string) {
        if (this._output === undefined) this._output = '';
        if (output !== undefined && output !== null) this._output += output;
    }

    stash() {
        this._outputStack.push(this._output);
        this._output = undefined;
    }

    pop() {
        const actual = this._output;
        this._output = this._outputStack.pop();
        return actual ?? '';
    }

    getOutput() {
        return this._output ?? '';
    }

    render(
        context: Record<string, unknown> = {},
        blocks: Record<string, BlockType> = {}
    ) {
        return this._template.render(context, blocks);
    }

    hasBlock(name: string, context: Record<string, unknown> = {}) {
        return this._template.hasBlock(name, context);
    }

    getBlockNames(context: Record<string, unknown> = {}) {
        return this._template.getBlockNames(context);
    }

    renderBlock(blockName: string, context: Record<string, unknown> = {}) {
        return this._template.renderBlock(
            blockName,
            this._env.mergeGlobals(context)
        );
    }

    getSourceContext() {
        return this._template.getSourceContext();
    }

    getTemplateName() {
        return this._template.getTemplateName();
    }

    unwrap() {
        return this._template;
    }
}
