/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { FunctionExpression } from './Node/Expression/FunctionExpression';

type FunctionOptions = {
    needsEnvironment: boolean;
    needsContext: boolean;
    isVariadic: boolean;
    isSafe: null | string[];
    isSafeCallback: null;
    nodeClass: typeof FunctionExpression;
    deprecated: boolean | string;
    alternative: null | string;
};

export type TwigFunctionType = (...args: unknown[]) => unknown;

export class TwigFunction {
    private _options: FunctionOptions;

    private _arguments: string[] = [];

    constructor(
        private _name: string,
        private _callable: null | TwigFunctionType = null,
        options: Partial<FunctionOptions> = {}
    ) {
        this._options = {
            needsEnvironment: false,
            needsContext: false,
            isVariadic: false,
            isSafe: null,
            isSafeCallback: null,
            nodeClass: FunctionExpression,
            deprecated: false,
            alternative: null,
            ...options,
        };
    }

    getName() {
        return this._name;
    }

    getCallable() {
        return this._callable;
    }

    getNodeClass() {
        return this._options.nodeClass;
    }

    setArguments(args: string[]) {
        this._arguments = args;
    }

    getArguments() {
        return this._arguments;
    }

    needsEnvironment() {
        return this._options.needsEnvironment;
    }

    needsContext() {
        return this._options.needsContext;
    }

    /*getSafe(Node $functionArgs): ?array
    {
        if (null !== this._options['is_safe']) {
            return this._options['is_safe'];
        }

        if (null !== this._options['is_safe_callback']) {
            return this._options['is_safe_callback']($functionArgs);
        }

        return [];
    }*/

    isVariadic() {
        return this._options.isVariadic;
    }

    isDeprecated() {
        return this._options.deprecated !== false;
    }

    getDeprecatedVersion() {
        return typeof this._options.deprecated === 'boolean'
            ? ''
            : this._options.deprecated;
    }

    getAlternative() {
        return this._options.alternative;
    }
}
