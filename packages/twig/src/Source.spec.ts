/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Source } from './Source';

describe('@kephajs/console/twig/Source', () => {
    it('Should have getters', () => {
        const source = new Source('My template', 'foobar', 'a path');
        expect(source.code).to.be.equal('My template');
        expect(source.name).to.be.equal('foobar');
        expect(source.path).to.be.equal('a path');
    });

    it('Should have empty path by default', () => {
        const source = new Source('My template', 'foobar');
        expect(source.path).to.be.equal('');
    });
});
