/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { CacheInterface } from './Cache/CacheInterface';
import { NullCache } from './Cache/NullCache';
import { Compiler } from './Compiler';
import { TwigError } from './Error/TwigError';
import { SyntaxError } from './Error/SyntaxError';
import { CoreExtension } from './Extension/CoreExtension';
import { ExtensionInterface } from './Extension/ExtensionInterface';
import { ExtensionSet } from './ExtensionSet';
import { Lexer } from './Lexer';
import { LoaderInterface } from './Loader/LoaderInterface';
import { TwigNode } from './Node/TwigNode';
import { Parser } from './Parser';
import { Source } from './Source';
import { TokenParserInterface } from './TokenParser/TokenParserInterface';
import { TokenStream } from './TokenStream';
import { version } from './version';
import { hash } from '@kephajs/core/Helpers/StringHelpers';
import { TwigTest } from './TwigTest';
import { TemplateWrapper } from './TemplateWrapper';
import { Template } from './Template';
import { LoaderError } from './Error/LoaderError';
import { RuntimeError } from './Error/RuntimeError';
import { objectKeys } from '@kephajs/core/Helpers/Helpers';
import { Markup } from './Markup';
import { TwigFilter } from './TwigFilter';
import { TwigFunction } from './TwigFunction';

export type EnvironmentOptions = {
    debug: boolean;
    locale: string;
    strictVariables: boolean;
    autoescape: false | string;
    cache: string | boolean | CacheInterface;
    autoReload: boolean | null;
    optimizations: number;
};

export class Environment {
    protected _autoescape: false | string;

    protected _autoReload: boolean;

    protected _cache: CacheInterface = new NullCache();

    protected _locale = 'fr-FR';

    protected _compiler: Compiler | null = null;

    protected _debug: boolean;

    protected _extensionSet = new ExtensionSet();

    protected _globals: Record<string, unknown> = {};

    protected _lexer: Lexer | null = null;

    protected _loadedTemplates: Record<string, Template> = {};

    protected _optimizations: number;

    protected _optionsHash = '';

    protected _originalCache: string | boolean | CacheInterface = false;

    protected _parser: null | Parser = null;

    protected _resolvedGlobals: Record<string, unknown> | null = null;

    protected _strictVariables: boolean;

    protected _templateClassPrefix = '__TwigTemplate_';

    constructor(
        protected _loader: LoaderInterface,
        options: Partial<EnvironmentOptions> = {}
    ) {
        const mergedOptions: EnvironmentOptions = {
            debug: false,
            locale: 'en-EN',
            strictVariables: false,
            autoescape: 'html',
            cache: false,
            autoReload: null,
            optimizations: -1,
            ...options,
        };

        this._debug = mergedOptions.debug;
        this.setLocale(mergedOptions.locale);
        this._autoReload =
            mergedOptions.autoReload === null
                ? this._debug
                : mergedOptions.autoReload;
        this._strictVariables = mergedOptions.strictVariables;
        this._autoescape = mergedOptions.autoescape;
        this.setCache(mergedOptions.cache);
        this._optimizations = mergedOptions.optimizations;

        this.addExtension(new CoreExtension());
        /*this.addExtension(new EscaperExtension(this._autoescape));
        this.addExtension(new OptimizerExtension(this._optimizations));*/
    }

    enableDebug() {
        this._debug = true;
        this._updateOptionsHash();
    }

    disableDebug() {
        this._debug = false;
        this._updateOptionsHash();
    }

    isDebug() {
        return this._debug;
    }

    enableAutoReload() {
        this._autoReload = true;
    }

    disableAutoReload() {
        this._autoReload = false;
    }

    isAutoReload() {
        return this._autoReload;
    }

    enableStrictVariables() {
        this._strictVariables = true;
        this._updateOptionsHash();
    }

    disableStrictVariables() {
        this._strictVariables = false;
        this._updateOptionsHash();
    }

    getAutoescape() {
        return this._autoescape;
    }

    isStrictVariables() {
        return this._strictVariables;
    }

    getCache(): string | boolean | CacheInterface;
    getCache(original: false): CacheInterface;
    getCache(original: true): string | boolean | CacheInterface;
    getCache(original = true) {
        return original ? this._originalCache : this._cache;
    }

    setCache(cache: string | boolean | CacheInterface) {
        this._originalCache = cache;
        if (typeof cache === 'string')
            throw new TypeError(
                'Use EnvironmentConsole to use a string to set the cache'
            );
        else if (cache === false) this._cache = new NullCache();
        else if (cache !== true) this._cache = cache;
        else
            throw new TypeError(
                'Cache can only be a string, false, or a CacheInterface implementation.'
            );
    }

    getExportType() {
        return this._cache.exportType;
    }

    getOptimizations() {
        return this._optimizations;
    }

    /**
     * Gets the template class associated with the given string.
     *
     * The generated template class is based on the following parameters:
     *
     *  * The cache key for the given template;
     *  * The currently enabled extensions;
     *  * Whether the Twig C extension is available or not;
     *  * PHP version;
     *  * Twig version;
     *  * Options with what environment was created.
     */
    getTemplateClass(name: string, index: null | number = null) {
        const key = this.getLoader().getCacheKey(name) + this._optionsHash;

        return (
            this._templateClassPrefix +
            hash(key) +
            (index === null ? '' : '___' + index)
        );
    }

    /**
     * Renders a template
     */
    render(
        templateName: string | TemplateWrapper,
        context: Record<string, unknown> = {}
    ) {
        return this.load(templateName).render(context);
    }

    load(templateName: string | TemplateWrapper | Template) {
        if (
            templateName instanceof TemplateWrapper ||
            templateName instanceof Template
        )
            return templateName;

        return new TemplateWrapper(
            this,
            this.loadTemplate(this.getTemplateClass(templateName), templateName)
        );
    }

    /**
     * Loads a template internal representation.
     *
     * This method is for internal use only and should never be called
     * directly.
     */
    loadTemplate(
        cls: string,
        templateName: string,
        index: null | number = null
    ): Template {
        //const mainCls = cls;
        if (index !== null) cls += '___' + index;

        if (this._loadedTemplates[cls] !== undefined)
            return this._loadedTemplates[cls];

        const key = this._cache.generateKey(templateName, cls);

        let classPrototype: typeof Template | null = null;
        if (
            !this.isAutoReload() ||
            this.isTemplateFresh(templateName, this._cache.getTimestamp(key))
        )
            classPrototype = this._cache.load(key);

        if (classPrototype === null) {
            const source = this.getLoader().getSourceContext(templateName);
            const content = this.compileSource(source);
            this._cache.write(key, content);
            classPrototype = this._cache.load(key);

            if (classPrototype === null) {
                throw new RuntimeError(
                    `Failed to load Twig template "${templateName}", index "${index}": cache might be corrupted.`,
                    -1,
                    source
                );
            }
        }

        this._extensionSet.initRuntime();

        return (this._loadedTemplates[cls] = new classPrototype(this, {
            Source,
            Markup,
            RuntimeError,
        })).init();
    }

    /**
     * Creates a template from source.
     *
     * This method should not be used as a generic way to load templates.
     *
     * @param string $template The template source
     * @param string $name     An optional name of the template to be used in error messages
     *
     * @throws LoaderError When the template cannot be found
     * @throws SyntaxError When an error occurred during compilation
     */
    /* public function createTemplate(string $template, string $name = null): TemplateWrapper
     {
         $hash = hash(\PHP_VERSION_ID < 80100 ? 'sha256' : 'xxh128', $template, false);
         if (null !== $name) {
             $name = sprintf('%s (string template %s)', $name, $hash);
         } else {
             $name = sprintf('__string_template__%s', $hash);
         }

         $loader = new ChainLoader([
             new ArrayLoader([$name => $template]),
             $current = $this->getLoader(),
         ]);

         $this->setLoader($loader);
         try {
             return new TemplateWrapper($this, $this->loadTemplate($this->getTemplateClass($name), $name));
         } finally {
             $this->setLoader($current);
         }
     }*/

    isTemplateFresh(templateName: string, time: number) {
        return this.getLoader().isFresh(templateName, time);
    }

    /**
     * Tries to load a template consecutively from an array.
     *
     * Similar to load() but it also accepts instances of Template and
     * TemplateWrapper, and an array of templates where each is tried to be loaded.
     */
    resolveTemplate(
        names:
            | string
            | TemplateWrapper
            | Template
            | (string | TemplateWrapper | Template)[]
    ): Template | TemplateWrapper {
        if (!Array.isArray(names)) return this.load(names);

        const length = names.length;
        for (const templateName of names) {
            if (templateName instanceof Template) return templateName;
            if (templateName instanceof TemplateWrapper) return templateName;

            if (length !== 1 && !this.getLoader().exists(templateName))
                continue;

            return this.load(templateName);
        }

        throw new LoaderError(
            `Unable to find one of the following templates: "${names.join(
                '", "'
            )}".`
        );
    }

    setLexer(lexer: Lexer) {
        this._lexer = lexer;
    }

    tokenize(source: Source): TokenStream {
        if (null === this._lexer) this._lexer = new Lexer(this);

        return this._lexer.tokenize(source);
    }

    setParser(parser: Parser) {
        this._parser = parser;
    }

    parse(stream: TokenStream) {
        if (this._parser === null) this._parser = new Parser(this);

        return this._parser.parse(stream);
    }

    setCompiler(compiler: Compiler) {
        this._compiler = compiler;
    }

    compile(node: TwigNode) {
        if (this._compiler === null) this._compiler = new Compiler(this);

        return this._compiler.compile(node).getSource();
    }

    compileSource(source: Source) {
        try {
            return this.compile(this.parse(this.tokenize(source)));
        } catch (error) {
            if (error instanceof TwigError) {
                error.setSourceContext(source);
                throw error;
            } else
                throw new SyntaxError(
                    `An exception has been thrown during the compilation of a template ("${
                        (error as Error).message
                    }").`,
                    -1,
                    source
                );
        }
    }

    setLoader(loader: LoaderInterface) {
        this._loader = loader;
    }

    getLoader() {
        return this._loader;
    }

    setLocale(locale: string) {
        this._locale = locale;
    }

    getLocale() {
        return this._locale;
    }

    hasExtension(className: string) {
        return this._extensionSet.hasExtension(className);
    }

    /* public function addRuntimeLoader(RuntimeLoaderInterface $loader)
     {
         $this->runtimeLoaders[] = $loader;
     }*/

    getExtension(className: string) {
        return this._extensionSet.getExtension(className);
    }

    /**
     * Returns the runtime implementation of a Twig element (filter/function/tag/test).
     *
     * @template TRuntime of object
     *
     * @param class-string<TRuntime> $class A runtime class name
     *
     * @return TRuntime The runtime implementation
     *
     * @throws RuntimeError When the template cannot be found
     */
    /* public function getRuntime(string $class)
     {
         if (isset($this->runtimes[$class])) {
             return $this->runtimes[$class];
         }

         foreach ($this->runtimeLoaders as $loader) {
             if (null !== $runtime = $loader->load($class)) {
                 return $this->runtimes[$class] = $runtime;
             }
         }

         throw new RuntimeError(sprintf('Unable to load the "%s" runtime.', $class));
     }*/

    addExtension(extension: ExtensionInterface) {
        this._extensionSet.addExtension(extension);
        this._updateOptionsHash();
    }

    setExtensions(extensions: ExtensionInterface[]) {
        this._extensionSet.setExtensions(extensions);
        this._updateOptionsHash();
    }

    getExtensions() {
        return this._extensionSet.getExtensions();
    }

    addTokenParser(parser: TokenParserInterface) {
        this._extensionSet.addTokenParser(parser);
    }

    getTokenParsers() {
        return this._extensionSet.getTokenParsers();
    }

    getTokenParser(tokenParserName: string) {
        return this._extensionSet.getTokenParser(tokenParserName);
    }

    /* public function registerUndefinedTokenParserCallback(callable $callable): void
     {
         $this->extensionSet->registerUndefinedTokenParserCallback($callable);
     }

     public function addNodeVisitor(NodeVisitorInterface $visitor)
     {
         $this->extensionSet->addNodeVisitor($visitor);
     }*/

    getNodeVisitors() {
        return this._extensionSet.getNodeVisitors();
    }

    addFilter(filter: TwigFilter) {
        this._extensionSet.addFilter(filter);
    }

    getFilter(filterName: string) {
        return this._extensionSet.getFilter(filterName);
    }

    /*public function registerUndefinedFilterCallback(callable $callable): void
     {
         $this->extensionSet->registerUndefinedFilterCallback($callable);
     }*/

    getFilters() {
        return this._extensionSet.getFilters();
    }

    addTest(test: TwigTest) {
        this._extensionSet.addTest(test);
    }

    getTests() {
        return this._extensionSet.getTests();
    }

    getTest(name: string) {
        return this._extensionSet.getTest(name);
    }

    addFunction(fct: TwigFunction) {
        this._extensionSet.addFunction(fct);
    }

    getFunction(fctName: string) {
        return this._extensionSet.getFunction(fctName);
    }

    /*public function registerUndefinedFunctionCallback(callable $callable): void
     {
         $this->extensionSet->registerUndefinedFunctionCallback($callable);
     }*/

    getFunctions() {
        return this._extensionSet.getFunctions();
    }

    /**
     * Registers a Global.
     *
     * New globals can be added before compiling or rendering a template;
     * but after, you can only update existing globals.
     */
    addGlobal(globalName: string, value: unknown) {
        if (
            this._extensionSet.isInitialized() &&
            objectKeys(this.getGlobals()).indexOf(globalName) === -1
        )
            throw new Error(
                `Unable to add global "${globalName}" as the runtime or the extensions have already been initialized.`
            );

        if (this._resolvedGlobals !== null)
            this._resolvedGlobals[globalName] = value;
        else this._globals[globalName] = value;
    }

    getGlobals() {
        if (this._extensionSet.isInitialized()) {
            if (this._resolvedGlobals === null)
                this._resolvedGlobals = {
                    ...this._extensionSet.getGlobals(),
                    ...this._globals,
                };

            return this._resolvedGlobals;
        }

        return { ...this._extensionSet.getGlobals(), ...this._globals };
    }

    mergeGlobals(context: Record<string, unknown>) {
        return { ...this.getGlobals(), ...context };
    }

    getUnaryOperators() {
        return this._extensionSet.getUnaryOperators();
    }

    getBinaryOperators() {
        return this._extensionSet.getBinaryOperators();
    }

    private _updateOptionsHash() {
        this._optionsHash = [
            this._extensionSet.getSignature(),
            process.version,
            version,
            this._debug ? 1 : 0,
            this._strictVariables ? 1 : 0,
        ].join(':');
    }
}
