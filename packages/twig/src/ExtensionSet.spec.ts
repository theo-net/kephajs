/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { AbstractExtension } from './Extension/AbstractExtension';
import { ExtensionSet } from './ExtensionSet';

describe('@kephajs/twig/ExtensionSet', () => {
    class MyExtension1 extends AbstractExtension {}
    class MyExtension2 extends AbstractExtension {}

    describe('initRuntime', () => {
        it('Should set as initialized', () => {
            const extensionSet = new ExtensionSet();
            expect(extensionSet.isInitialized()).to.be.equal(false);
            extensionSet.initRuntime();
            expect(extensionSet.isInitialized()).to.be.equal(true);
        });
    });

    describe('setExtensions', () => {
        it('Should add extensions', () => {
            const extensionSet = new ExtensionSet();
            const myExtension1 = new MyExtension1();
            const myExtension2 = new MyExtension2();
            extensionSet.setExtensions([myExtension1, myExtension2]);
            expect(extensionSet.getExtensions()).to.be.deep.equal({
                MyExtension1: myExtension1,
                MyExtension2: myExtension2,
            });
        });
    });

    describe('addExtension', () => {
        it('Should add an extension', () => {
            const extensionSet = new ExtensionSet();
            const myExtension1 = new MyExtension1();
            extensionSet.addExtension(myExtension1);
            expect(extensionSet.getExtensions()).to.be.deep.equal({
                MyExtension1: myExtension1,
            });
        });

        it('Should throw an Error if ExtensionSet already initialized', () => {
            const extensionSet = new ExtensionSet();
            extensionSet.isInitialized();
            extensionSet.getFunctions();
            try {
                extensionSet.addExtension(new MyExtension1());
            } catch (error) {
                expect(error).to.be.instanceOf(Error);
                expect((error as Error).message).to.contain(
                    'as extensions have already been initialized'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should throw an Error if an extension with the same name already registered', () => {
            const extensionSet = new ExtensionSet();
            extensionSet.addExtension(new MyExtension1());
            try {
                extensionSet.addExtension(new MyExtension1());
            } catch (error) {
                expect(error).to.be.instanceOf(Error);
                expect((error as Error).message).to.contain(
                    'as it is already registered'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });
});
