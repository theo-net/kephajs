/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Environment } from './Environment';
import { RuntimeError } from './Error/RuntimeError';
import { twigGetAttribute } from './Extension/CoreExtension';
import { SandboxExtension } from './Extension/SandboxExtension';
import { RecordLoader } from './Loader/RecordLoader';
import { SecurityError } from './Sandbox/SecurityError';
import { SecurityPolicy } from './Sandbox/SecurityPolicy';
import { Source } from './Source';
import { BlockType, Template } from './Template';

class StdClass {}

class TemplateForTest extends Template {
    constructor(env: Environment, private _templateName = 'index.twig') {
        super(env);
    }

    getZero() {
        return 0;
    }

    getEmpty() {
        return '';
    }

    getString() {
        return 'some_string';
    }

    getTrue() {
        return true;
    }

    getTemplateName() {
        return this._templateName;
    }

    getDebugInfo() {
        return [];
    }

    getSourceContext() {
        return new Source('', this.getTemplateName());
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    doGetParent(_context: Record<string, unknown>) {
        return false;
    }

    doDisplay(
        _context: Record<string, unknown>,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        _blocks: Record<string, BlockType> = {}
    ) {}

    block_name(
        _context: Record<string, unknown>,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        _blocks: Record<string, BlockType> = {}
    ) {}
}

class TemplatePropertyObject {
    public defined = 'defined';

    public zero = 0;

    public null = null;

    public bar = true;

    public foo = true;

    public baz = 'baz';

    public baf = 'baf';

    protected protected = 'protected';
}

class TemplateMethodObject {
    getDefined() {
        return 'defined';
    }

    get1() {
        return 1;
    }

    get09() {
        return '09';
    }

    getZero() {
        return 0;
    }

    getNull() {
        return null;
    }

    isBar() {
        return true;
    }

    hasFoo() {
        return true;
    }

    hasBaz() {
        return 'should never be returned (has)';
    }

    isBaz() {
        return 'should never be returned (is)';
    }

    getBaz() {
        return 'Baz';
    }

    baz() {
        return 'baz';
    }

    hasBaf() {
        return 'should never be returned (has)';
    }

    isBaf() {
        return 'baf';
    }

    protected getProtected() {
        return 'protected';
    }

    static getStatic() {
        return 'static';
    }
}

describe('@kephajs/twig/Template', () => {
    it('Should accept Template only as blocks to renderBlock method', () => {
        const twig = new Environment(new RecordLoader());
        const template = new TemplateForTest(twig);

        try {
            template.renderBlock(
                'foo',
                {},
                { foo: [new StdClass() as Template, 'foo'] }
            );
        } catch (error) {
            expect(error).to.be.instanceOf(Error);
            return;
        }
        expect.fail('Should have thrown');
    });

    it('Should throw RuntimeError if accessing to an invalid attribute', () => {
        [
            [
                '{{ string["a"] }}',
                'Impossible to access a key ("a") on a string variable ("foo") in "%s" at line 1.',
            ],
            [
                '{{ null["a"] }}',
                'Impossible to access a key ("a") on a null variable in "%s" at line 1.',
            ],
            [
                '{{ empty_array[1] }}',
                'Key "1" does not exist as the array is empty in "%s" at line 1.',
            ],
            [
                '{{ array["a"] }}',
                'Impossible to access to an array with the key "a" (string) in "%s" at line 1.',
            ],
            [
                '{{ record["a"] }}',
                'Key "a" in object does not exist in "%s" at line 1.',
            ],
            [
                '{{ string.a }}',
                'Impossible to access an attribute ("a") on a string variable ("foo") in "%s" at line 1.',
            ],
            [
                '{{ string.a() }}',
                'Impossible to invoke a method ("a") on a string variable ("foo") in "%s" at line 1.',
            ],
            [
                '{{ null.a }}',
                'Impossible to invoke a method or to access an attribute ("a") on a null variable in "%s" at line 1.',
            ],
            [
                '{{ null.a() }}',
                'Impossible to invoke a method or to access an attribute ("a") on a null variable in "%s" at line 1.',
            ],
            [
                '{{ record.a() }}',
                'Neither the property "a" nor one of the methods "a()", "geta()", "isa()" or "hasa()" exist and have public access in class "Object" in "%s" at line 1.',
            ],
            [
                '{{ array.a() }}',
                'Impossible to invoke a method or to access an attribute ("a") on an array in "%s" at line 1.',
            ],
            [
                '{{ empty_array.a }}',
                'Impossible to invoke a method or to access an attribute ("a") on an array in "%s" at line 1.',
            ],
            [
                '{{ array.a }}',
                'Impossible to invoke a method or to access an attribute ("a") on an array in "%s" at line 1.',
            ],
            [
                '{{ record.a }}',
                'Neither the property "a" nor one of the methods "a()", "geta()", "isa()" or "hasa()" exist and have public access in class "Object" in "%s" at line 1.',
            ],
            [
                '{{ attribute(record, "bar") }}',
                'Neither the property "bar" nor one of the methods "bar()", "getbar()", "isbar()" or "hasbar()" exist and have public access in class "Object" in "%s" at line 1.',
            ],
            [
                '{{ attribute(array, -10) }}',
                'Impossible to invoke a method or to access an attribute ("-10") on an array in "index" at line 1.',
            ],
            [
                '{% from _self import foo %}{% macro foo(obj) %}{{ obj.missing_method() }}{% endmacro %}{{ foo(record) }}',
                'Neither the property "missing_method" nor one of the methods "missing_method()", "getmissing_method()", "ismissing_method()" or "hasmissing_method()" exist and have public access in class "Object" in "%s" at line 1.',
            ],
        ].forEach(test => {
            const twig = new Environment(new RecordLoader({ index: test[0] }), {
                strictVariables: true,
            });
            const template = twig.load('index');

            const context = {
                string: 'foo',
                null: null,
                empty_array: [],
                record: { foo: 'foo' },
                array: ['foo', 'bar'],
                object: new StdClass() as Template,
            };

            try {
                template.render(context);
            } catch (error) {
                expect(error).to.be.instanceOf(RuntimeError);
                expect((error as RuntimeError).message).to.be.equal(
                    test[1].replace('%s', 'index')
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    it('Should get attribute with sandox', () => {
        (
            [
                [new TemplatePropertyObject(), 'defined', false],
                [new TemplatePropertyObject(), 'defined', true],
                [new TemplateMethodObject(), 'defined', false],
                [new TemplateMethodObject(), 'defined', true],
            ] as [unknown, string, boolean][]
        ).forEach(test => {
            const twig = new Environment(new RecordLoader());
            const policy = new SecurityPolicy([], [], {}, {}, []);
            twig.addExtension(new SandboxExtension(policy, !test[2]));
            const template = new TemplateForTest(twig);

            try {
                twigGetAttribute(
                    twig,
                    template.getSourceContext(),
                    test[0],
                    test[1],
                    [],
                    'any',
                    false,
                    false,
                    true
                );

                if (!test[2]) expect.fail();
            } catch (error) {
                if (error instanceof SecurityError) {
                    if (test[2]) expect.fail();
                    expect((error as SecurityError).message).to.contain(
                        'is not allowed'
                    );
                } else expect.fail();
            }
        });
    });

    it('Sould throw RuntimeError if undefined block', () => {
        const twig = new Environment(new RecordLoader());
        const template = new TemplateForTest(twig, 'index.twig');
        try {
            template.renderBlock('unknown', {});
        } catch (error) {
            expect(error).to.be.instanceOf(RuntimeError);
            expect((error as RuntimeError).message).to.be.equal(
                'Block "unknown" on template "index.twig" does not exist in "index.twig".'
            );
            return;
        }
        expect.fail('Should have thrown');
    });

    it('Should throw RuntimeError if undefined parent block', () => {
        const twig = new Environment(new RecordLoader());
        const template = new TemplateForTest(twig, 'parent.twig');
        try {
            template.renderBlock(
                'foo',
                {},
                { foo: [new TemplateForTest(twig, 'index.twig'), 'block_foo'] },
                false
            );
        } catch (error) {
            expect(error).to.be.instanceOf(RuntimeError);
            expect((error as RuntimeError).message).to.be.equal(
                'Block "foo" should not call parent() as the block does not exist in the parent template "parent.twig" in "index.twig".'
            );
            return;
        }
        expect.fail('Should have thrown');
    });

    class TemplatePropertyObjectDefinedWithUndefinedValue {
        public foo = undefined;
    }

    class TemplateGetIsMethods {
        get() {}

        is() {}
    }

    class TemplateMethodAndPropObject {
        #a = 'a_prop';

        getA() {
            return 'a';
        }

        b = 'b_prop';

        getB() {
            return 'b';
        }

        #c = 'c_prop';

        #getC() {
            return 'c';
        }

        debug() {
            console.log(this.#a + this.#c + this.#getC());
        }
    }

    function getGetAttributeTests() {
        const record = {
            defined: 'defined',
            zero: 0,
            null: null,
            '1': 1,
            bar: true,
            foo: true,
            baz: 'baz',
            baf: 'baf',
            '09': '09',
            '+4': '+4',
        };

        const propertyObject = new TemplatePropertyObject();
        const propertyObject3 =
            new TemplatePropertyObjectDefinedWithUndefinedValue();
        const methodObject = new TemplateMethodObject();

        const anyType = Template.ANY_CALL;
        const methodType = Template.METHOD_CALL;
        const arrayType = Template.ARRAY_CALL;

        const basicTests: [
            boolean,
            null | number | boolean | string | undefined,
            unknown
        ][] = [
            // array(defined, value, property to fetch)
            [true, 'defined', 'defined'],
            [false, undefined, 'undefined'],
            [true, 0, 'zero'],
            [true, 1, 1],
            [true, 1, 1.0],
            [true, null, 'null'],
            [true, true, 'bar'],
            [true, true, 'foo'],
            [true, 'baz', 'baz'],
            [true, 'baf', 'baf'],
            [true, '09', '09'],
            [true, '+4', '+4'],
        ];
        const testObjects: [unknown, string][] = [
            // array(object, type of fetch)
            [record, arrayType],
            [methodObject, methodType],
            [methodObject, anyType],
            [propertyObject, anyType],
        ];

        const tests: [
            boolean,
            null | number | boolean | string | undefined,
            unknown,
            string,
            unknown[],
            string,
            string?
        ][] = [];
        testObjects.forEach(testObject => {
            basicTests.forEach(test => {
                // properties cannot be numbers
                if (
                    testObject[0] instanceof TemplatePropertyObject &&
                    (typeof test[2] === 'number' ||
                        /^[+-]?[0-9]*$/.test(String(test[2])))
                )
                    return;

                if ('+4' === test[2] && methodObject === testObject[0]) return;

                tests.push([
                    test[0],
                    test[1],
                    testObject[0],
                    test[2] as string,
                    [],
                    testObject[1],
                ]);
            });
        });

        // additional properties tests
        tests.push([true, undefined, propertyObject3, 'foo', [], anyType]);

        // additional method tests
        tests.push(
            ...([
                [true, 'defined', methodObject, 'defined', [], methodType],
                [true, 'defined', methodObject, 'DEFINED', [], methodType],
                [true, 'defined', methodObject, 'getDefined', [], methodType],
                [true, 'defined', methodObject, 'GETDEFINED', [], methodType],
                [true, 'static', methodObject, 'static', [], methodType],
                [true, 'static', methodObject, 'getStatic', [], methodType],
            ] as [
                boolean,
                string | undefined,
                unknown,
                string,
                unknown[],
                string
            ][])
        );

        // add the same tests for the any type
        tests.forEach(test => {
            if (anyType !== test[5]) {
                test[5] = anyType;
                tests.push(test);
            }
        });

        const methodAndPropObject = new TemplateMethodAndPropObject();

        // additional method tests
        tests.push(
            ...([
                [true, 'a', methodAndPropObject, 'a', [], anyType],
                [true, 'a', methodAndPropObject, 'a', [], methodType],
                [false, undefined, methodAndPropObject, 'a', [], arrayType],

                [true, 'b_prop', methodAndPropObject, 'b', [], anyType],
                [true, 'b', methodAndPropObject, 'B', [], anyType],
                [true, 'b', methodAndPropObject, 'b', [], methodType],
                [true, 'b', methodAndPropObject, 'B', [], methodType],
                [true, 'b_prop', methodAndPropObject, 'b', [], arrayType],

                [false, undefined, methodAndPropObject, 'c', [], anyType],
                [false, undefined, methodAndPropObject, 'c', [], methodType],
                [false, undefined, methodAndPropObject, 'c', [], arrayType],
            ] as [
                boolean,
                string | undefined,
                unknown,
                string,
                unknown[],
                string
            ][])
        );

        // tests when input is not an array or object
        tests.push(
            ...([
                [
                    false,
                    undefined,
                    42,
                    'a',
                    [],
                    anyType,
                    'Impossible to access an attribute ("a") on a integer variable ("42") in "index.twig".',
                ],
                [
                    false,
                    undefined,
                    'string',
                    'a',
                    [],
                    anyType,
                    'Impossible to access an attribute ("a") on a string variable ("string") in "index.twig".',
                ],
                [
                    false,
                    undefined,
                    {},
                    'a',
                    [],
                    anyType,
                    'Key "a" does not exist as the array is empty in "index.twig".',
                ],
            ] as [
                boolean,
                string | undefined,
                unknown,
                string,
                unknown[],
                string,
                string
            ][])
        );

        return tests;
    }

    it('Should get attribute', () => {
        getGetAttributeTests().forEach(test => {
            const value = test[1],
                object = test[2],
                item = test[3] as string,
                args = test[4] as unknown[],
                type = test[5] as string;
            const twig = new Environment(new RecordLoader());
            const template = new TemplateForTest(twig);

            expect(
                twigGetAttribute(
                    twig,
                    template.getSourceContext(),
                    object,
                    item,
                    args,
                    type
                )
            ).to.be.equal(value);
        });
    });

    it('Should get attribute defined', () => {
        getGetAttributeTests().forEach(test => {
            const defined = test[0],
                object = test[2],
                item = test[3] as string,
                args = test[4] as unknown[],
                type = test[5] as string;
            const twig = new Environment(new RecordLoader());
            const template = new TemplateForTest(twig);

            expect(
                twigGetAttribute(
                    twig,
                    template.getSourceContext(),
                    object,
                    item,
                    args,
                    type,
                    true
                )
            ).to.be.equal(defined);
        });
    });

    it('Should get attribute defined strict', () => {
        getGetAttributeTests().forEach(test => {
            const defined = test[0],
                object = test[2],
                item = test[3] as string,
                args = test[4] as unknown[],
                type = test[5] as string;
            const twig = new Environment(new RecordLoader(), {
                strictVariables: true,
            });
            const template = new TemplateForTest(twig);

            expect(
                twigGetAttribute(
                    twig,
                    template.getSourceContext(),
                    object,
                    item,
                    args,
                    type,
                    true
                )
            ).to.be.equal(defined);
        });
    });

    it('Should call is methods', () => {
        const twig = new Environment(new RecordLoader());
        const getIsObject = new TemplateGetIsMethods();
        const template = new TemplateForTest(twig, 'index.twig');

        // first time should not create a cache for "get"
        expect(
            twigGetAttribute(
                twig,
                template.getSourceContext(),
                getIsObject,
                'get'
            )
        ).to.be.equal(undefined);
        // 0 should be in the method cache now, so this should fail
        expect(
            twigGetAttribute(twig, template.getSourceContext(), getIsObject, 0)
        ).to.be.equal(undefined);
    });

    it('Should work with undefined object', () => {
        const twig = new Environment(new RecordLoader());
        const template = new TemplateForTest(twig, 'index.twig');

        // first time should not create a cache for "get"
        expect(
            twigGetAttribute(
                twig,
                template.getSourceContext(),
                undefined,
                'foobar'
            )
        ).to.be.equal(undefined);
        expect(
            twigGetAttribute(
                twig,
                template.getSourceContext(),
                undefined,
                'foobar',
                [],
                'any',
                true
            )
        ).to.be.equal(false);
    });
});
