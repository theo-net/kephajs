/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { ConstantExpression } from '../Node/Expression/ConstantExpression';
import { TwigNode } from '../Node/TwigNode';
import { SyntaxError } from '../Error/SyntaxError';
import { Token } from '../Token';
import { AbstractTokenParser } from './AbstractTokenParser';
import { AbstractExpression } from '../Node/Expression/AbstractExpression';

export class UseTokenParser extends AbstractTokenParser {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    parse(_token: Token) {
        const template = this._parser.getExpressionParser()?.parseExpression();
        const stream = this._parser.getStream();

        if (!(template instanceof ConstantExpression))
            throw new SyntaxError(
                'The template references in a "use" statement must be a string.',
                stream.getCurrent().line,
                stream.getSourceContext()
            );

        const targets: Record<string, AbstractExpression> = {};
        if (stream.nextIf('with')) {
            do {
                const useName = stream.expect(Token.NAME_TYPE).value;

                let alias = useName;
                if (stream.nextIf('as'))
                    alias = stream.expect(Token.NAME_TYPE).value;

                targets[useName as string] = new ConstantExpression(alias, -1);

                if (!stream.nextIf(Token.PUNCTUATION_TYPE, ',')) break;
                // eslint-disable-next-line no-constant-condition
            } while (true);
        }

        stream.expect(Token.BLOCK_END_TYPE);

        this._parser.addTrait(
            new TwigNode({ template, targets: new TwigNode(targets) })
        );

        return new TwigNode();
    }

    getTag() {
        return 'use';
    }
}
