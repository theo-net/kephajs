/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { ConstantExpression } from '../Node/Expression/ConstantExpression';
import { Token } from '../Token';
import { AbstractTokenParser } from './AbstractTokenParser';
import { SyntaxError } from '../Error/SyntaxError';
import { AutoEscapeNode } from '../Node/AutoEscapeNode';

export class AutoEscapeTokenParser extends AbstractTokenParser {
    parse(token: Token) {
        const lineno = token.line;
        const stream = this._parser.getStream();
        let value = '';

        if (stream.test(Token.BLOCK_END_TYPE)) value = 'html';
        else {
            const expr = this._parser.getExpressionParser().parseExpression();
            if (!(expr instanceof ConstantExpression))
                throw new SyntaxError(
                    'An escaping strategy must be a string or false.',
                    stream.getCurrent().line,
                    stream.getSourceContext()
                );
            value = expr.getAttribute('value') as string;
        }

        stream.expect(Token.BLOCK_END_TYPE);
        const body = this._parser.subparse(
            this.decideBlockEnd.bind(this),
            true
        );
        stream.expect(Token.BLOCK_END_TYPE);

        return new AutoEscapeNode(value, body, lineno, this.getTag());
    }

    decideBlockEnd(token: Token) {
        return token.test('endautoescape');
    }

    getTag() {
        return 'autoescape';
    }
}
