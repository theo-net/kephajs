/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { AbstractExpression } from '../Node/Expression/AbstractExpression';
import { TwigNode } from '../Node/TwigNode';
import { WithNode } from '../Node/WithNode';
import { Token } from '../Token';
import { AbstractTokenParser } from './AbstractTokenParser';

export class WithTokenParser extends AbstractTokenParser {
    parse(token: Token): TwigNode {
        const stream = this._parser.getStream();

        let variables: null | AbstractExpression = null;
        let only: false | Token = false;
        if (!stream.test(Token.BLOCK_END_TYPE)) {
            const exprParser = this._parser.getExpressionParser();
            if (exprParser !== null) variables = exprParser.parseExpression();
            const next = stream.nextIf(Token.NAME_TYPE, 'only');
            if (next !== null) only = next;
        }

        stream.expect(Token.BLOCK_END_TYPE);

        const body = this._parser.subparse(this.decideWithEnd.bind(this), true);

        stream.expect(Token.BLOCK_END_TYPE);

        return new WithNode(body, variables, only, token.line, this.getTag());
    }

    decideWithEnd(token: Token) {
        return token.test('endwith');
    }

    getTag() {
        return 'with';
    }
}
