/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Token } from '../Token';
import { AbstractTokenParser } from './AbstractTokenParser';
import { SyntaxError } from '../Error/SyntaxError';
import { BodyNode } from '../Node/BodyNode';
import { MacroNode } from '../Node/MacroNode';
import { TwigNode } from '../Node/TwigNode';

export class MacroTokenParser extends AbstractTokenParser {
    parse(token: Token) {
        const lineno = token.line;
        const stream = this._parser.getStream();
        const macroName = stream.expect(Token.NAME_TYPE).value as string;

        const args = this._parser
            .getExpressionParser()
            .parseArguments(true, true);

        stream.expect(Token.BLOCK_END_TYPE);
        this._parser.pushLocalScope();
        const body = this._parser.subparse(
            this.decideBlockEnd.bind(this),
            true
        );
        const next = stream.nextIf(Token.NAME_TYPE);
        if (next !== null) {
            token = next;
            const value = token.value;

            if (value != macroName)
                throw new SyntaxError(
                    `Expected endmacro for macro "${macroName}" (but "${value}" given).`,
                    stream.getCurrent().line,
                    stream.getSourceContext()
                );
        }
        this._parser.popLocalScope();
        stream.expect(Token.BLOCK_END_TYPE);

        this._parser.setMacro(
            macroName,
            new MacroNode(
                macroName,
                new BodyNode([body]),
                args,
                lineno,
                this.getTag()
            )
        );

        return new TwigNode();
    }

    decideBlockEnd(token: Token) {
        return token.test('endmacro');
    }

    getTag() {
        return 'macro';
    }
}
