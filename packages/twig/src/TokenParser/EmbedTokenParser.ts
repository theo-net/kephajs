/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { EmbedNode } from '../Node/EmbedNode';
import { ConstantExpression } from '../Node/Expression/ConstantExpression';
import { NameExpression } from '../Node/Expression/NameExpression';
import { ModuleNode } from '../Node/ModuleNode';
import { Token } from '../Token';
import { IncludeTokenParser } from './IncludeTokenParser';

export class EmbedTokenParser extends IncludeTokenParser {
    parse(token: Token) {
        const stream = this._parser.getStream();

        const parent = this._parser.getExpressionParser().parseExpression();

        const { variables, only, ignoreMissing } = this.parseArguments();

        let parentToken = new Token(
            Token.STRING_TYPE,
            '__parent__',
            token.line
        );
        const fakeParentToken = parentToken;
        if (parent instanceof ConstantExpression)
            parentToken = new Token(
                Token.STRING_TYPE,
                parent.getAttribute('value') as string,
                token.line
            );
        else if (parent instanceof NameExpression)
            parentToken = new Token(
                Token.NAME_TYPE,
                parent.getAttribute('name') as string,
                token.line
            );

        // inject a fake parent to make the parent() function work
        stream.injectTokens([
            new Token(Token.BLOCK_START_TYPE, '', token.line),
            new Token(Token.NAME_TYPE, 'extends', token.line),
            parentToken,
            new Token(Token.BLOCK_END_TYPE, '', token.line),
        ]);

        const module = this._parser.parse(
            stream,
            this.decideBlockEnd.bind(this),
            true
        ) as ModuleNode;

        // override the parent with the correct one
        if (fakeParentToken === parentToken) module.setNode('parent', parent);

        this._parser.embedTemplate(module);

        stream.expect(Token.BLOCK_END_TYPE);

        return new EmbedNode(
            module.getTemplateName() as string,
            module.getAttribute('index') as number,
            variables,
            only,
            ignoreMissing,
            token.line,
            this.getTag()
        );
    }

    decideBlockEnd(token: Token) {
        return token.test('endembed');
    }

    getTag() {
        return 'embed';
    }
}
