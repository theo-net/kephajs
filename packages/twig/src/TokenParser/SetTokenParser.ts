/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { SetNode } from '../Node/SetNode';
import { TwigNode } from '../Node/TwigNode';
import { Token } from '../Token';
import { AbstractTokenParser } from './AbstractTokenParser';
import { SyntaxError } from '../Error/SyntaxError';

export class SetTokenParser extends AbstractTokenParser {
    parse(token: Token) {
        const lineno = token.line;
        const stream = this._parser.getStream();
        const names = this._parser
            .getExpressionParser()
            .parseAssignmentExpression();

        let values: TwigNode;
        let capture = false;
        if (stream.nextIf(Token.OPERATOR_TYPE, '=')) {
            values = this._parser
                .getExpressionParser()
                .parseMultitargetExpression();

            stream.expect(Token.BLOCK_END_TYPE);

            if (names.length !== values.length)
                throw new SyntaxError(
                    'When using set, you must have the same number of variables and assignments.',
                    stream.getCurrent().line,
                    stream.getSourceContext()
                );
        } else {
            capture = true;

            if (names.length > 1)
                throw new SyntaxError(
                    'When using set with a block, you cannot have a multi-target.',
                    stream.getCurrent().line,
                    stream.getSourceContext()
                );

            stream.expect(Token.BLOCK_END_TYPE);

            values = this._parser.subparse(
                this.decideBlockEnd.bind(this),
                true
            );
            stream.expect(Token.BLOCK_END_TYPE);
        }

        return new SetNode(capture, names, values, lineno, this.getTag());
    }

    decideBlockEnd(token: Token) {
        return token.test('endset');
    }

    getTag() {
        return 'set';
    }
}
