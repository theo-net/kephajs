/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Token } from '../Token';
import { AbstractTokenParser } from './AbstractTokenParser';
import { SyntaxError } from '../Error/SyntaxError';
import { BlockNode } from '../Node/BlockNode';
import { TwigNode } from '../Node/TwigNode';
import { PrintNode } from '../Node/PrintNode';
import { BlockReferenceNode } from '../Node/BlockReferenceNode';

export class BlockTokenParser extends AbstractTokenParser {
    parse(token: Token) {
        const lineno = token.line;
        const stream = this._parser.getStream();
        const blockName = stream.expect(Token.NAME_TYPE).value as string;
        if (this._parser.hasBlock(blockName)) {
            throw new SyntaxError(
                `The block '${blockName}' has already been defined line ${this._parser
                    .getBlock(blockName)
                    .getTemplateLine()}.`,
                stream.getCurrent().line,
                stream.getSourceContext()
            );
        }
        const block = new BlockNode(blockName, new TwigNode({}), lineno);
        this._parser.setBlock(blockName as string, block);
        this._parser.pushLocalScope();
        this._parser.pushBlockStack(blockName);

        let body: TwigNode;
        if (stream.nextIf(Token.BLOCK_END_TYPE)) {
            body = this._parser.subparse(this.decideBlockEnd.bind(this), true);
            const next = stream.nextIf(Token.NAME_TYPE);
            if (next !== null) {
                token = next;
                if (token.value !== blockName)
                    throw new SyntaxError(
                        `Expected endblock for block "${blockName}" (but "${token.value}" given).`,
                        stream.getCurrent().line,
                        stream.getSourceContext()
                    );
            }
        } else {
            body = new TwigNode([
                new PrintNode(
                    this._parser.getExpressionParser().parseExpression(),
                    lineno
                ),
            ]);
        }
        stream.expect(Token.BLOCK_END_TYPE);

        block.setNode('body', body);
        this._parser.popBlockStack();
        this._parser.popLocalScope();

        return new BlockReferenceNode(blockName, lineno, this.getTag());
    }

    decideBlockEnd(token: Token) {
        return token.test('endblock');
    }

    getTag() {
        return 'block';
    }
}
