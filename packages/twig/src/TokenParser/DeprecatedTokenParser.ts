/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { DeprecatedNode } from '../Node/DeprecatedNode';
import { Token } from '../Token';
import { AbstractTokenParser } from './AbstractTokenParser';

export class DeprecatedTokenParser extends AbstractTokenParser {
    parse(token: Token) {
        const expr = this._parser.getExpressionParser().parseExpression();

        this._parser.getStream().expect(Token.BLOCK_END_TYPE);

        return new DeprecatedNode(expr, token.line, this.getTag());
    }

    getTag() {
        return 'deprecated';
    }
}
