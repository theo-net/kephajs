/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { AssignNameExpression } from '../Node/Expression/AssignNameExpression';
import { ImportNode } from '../Node/ImportNode';
import { Token } from '../Token';
import { AbstractTokenParser } from './AbstractTokenParser';

export class FromTokenParser extends AbstractTokenParser {
    parse(token: Token) {
        const macro = this._parser.getExpressionParser().parseExpression();
        const stream = this._parser.getStream();
        stream.expect(Token.NAME_TYPE, 'import');

        const targets: Record<string, string> = {};
        let fromName = '';
        let alias = '';
        do {
            fromName = stream.expect(Token.NAME_TYPE).value as string;

            alias = fromName;
            if (stream.nextIf('as'))
                alias = stream.expect(Token.NAME_TYPE).value as string;

            targets[fromName] = alias;

            if (!stream.nextIf(Token.PUNCTUATION_TYPE, ',')) {
                break;
            }
            // eslint-disable-next-line no-constant-condition
        } while (true);

        stream.expect(Token.BLOCK_END_TYPE);

        const vars = new AssignNameExpression(
            this._parser.getVarName(),
            token.line
        );
        const node = new ImportNode(
            macro,
            vars,
            token.line,
            this.getTag(),
            this._parser.isMainScope()
        );

        Object.keys(targets).forEach(targetName => {
            this._parser.addImportedSymbol(
                'function',
                targets[targetName],
                'macro_' + targetName,
                vars
            );
        });

        return node;
    }

    getTag() {
        return 'from';
    }
}
