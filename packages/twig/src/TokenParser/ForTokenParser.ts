/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { AssignNameExpression } from '../Node/Expression/AssignNameExpression';
import { ForNode } from '../Node/ForNode';
import { TwigNode } from '../Node/TwigNode';
import { Token } from '../Token';
import { AbstractTokenParser } from './AbstractTokenParser';

export class ForTokenParser extends AbstractTokenParser {
    parse(token: Token) {
        const lineno = token.line;
        const stream = this._parser.getStream();
        const targets = this._parser
            .getExpressionParser()
            .parseAssignmentExpression();
        stream.expect(Token.OPERATOR_TYPE, 'in');
        const seq = this._parser.getExpressionParser().parseExpression();

        stream.expect(Token.BLOCK_END_TYPE);
        const body = this._parser.subparse(this.decideForFork.bind(this));
        let elseNode: null | TwigNode;
        if (stream.next().value === 'else') {
            stream.expect(Token.BLOCK_END_TYPE);
            elseNode = this._parser.subparse(
                this.decideForEnd.bind(this),
                true
            );
        } else elseNode = null;
        stream.expect(Token.BLOCK_END_TYPE);

        let keyTarget, valueTarget: AssignNameExpression;
        if (targets.length > 1) {
            keyTarget = targets.getNode(0);
            keyTarget = new AssignNameExpression(
                keyTarget.getAttribute('name') as string,
                keyTarget.getTemplateLine()
            );
            valueTarget = targets.getNode(1) as AssignNameExpression;
        } else {
            keyTarget = new AssignNameExpression('_key', lineno);
            valueTarget = targets.getNode(0) as AssignNameExpression;
        }
        valueTarget = new AssignNameExpression(
            valueTarget.getAttribute('name') as string,
            valueTarget.getTemplateLine()
        );

        return new ForNode(
            keyTarget,
            valueTarget,
            seq,
            null,
            body,
            elseNode,
            lineno,
            this.getTag()
        );
    }

    decideForFork(token: Token) {
        return token.test(['else', 'endfor']);
    }

    decideForEnd(token: Token) {
        return token.test('endfor');
    }

    getTag() {
        return 'for';
    }
}
