/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { TwigNode } from '../Node/TwigNode';
import { Token } from '../Token';
import { AbstractTokenParser } from './AbstractTokenParser';
import { SyntaxError } from '../Error/SyntaxError';
import { IfNode } from '../Node/IfNode';

export class IfTokenParser extends AbstractTokenParser {
    parse(token: Token) {
        const lineno = token.line;
        let expr = this._parser.getExpressionParser().parseExpression();
        const stream = this._parser.getStream();
        stream.expect(Token.BLOCK_END_TYPE);
        let body = this._parser.subparse(this.decideIfFork.bind(this));
        const tests = [expr, body];
        let elseNode: null | TwigNode = null;

        let end = false;
        while (!end) {
            switch (stream.next().value as string) {
                case 'else':
                    stream.expect(Token.BLOCK_END_TYPE);
                    elseNode = this._parser.subparse(
                        this.decideIfEnd.bind(this)
                    );
                    break;

                case 'elseif':
                    expr = this._parser.getExpressionParser().parseExpression();
                    stream.expect(Token.BLOCK_END_TYPE);
                    body = this._parser.subparse(this.decideIfFork.bind(this));
                    tests.push(expr);
                    tests.push(body);
                    break;

                case 'endif':
                    end = true;
                    break;

                default:
                    throw new SyntaxError(
                        `Unexpected end of template. Twig was looking for the following tags "else", "elseif", or "endif" to close the "if" block started at line ${lineno}).`,
                        stream.getCurrent().line,
                        stream.getSourceContext()
                    );
            }
        }

        stream.expect(Token.BLOCK_END_TYPE);

        return new IfNode(new TwigNode(tests), elseNode, lineno, this.getTag());
    }

    decideIfFork(token: Token) {
        return token.test(['elseif', 'else', 'endif']);
    }

    decideIfEnd(token: Token) {
        return token.test(['endif']);
    }

    getTag() {
        return 'if';
    }
}
