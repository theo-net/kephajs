/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { AbstractExpression } from '../Node/Expression/AbstractExpression';
import { IncludeNode } from '../Node/IncludeNode';
import { Token } from '../Token';
import { AbstractTokenParser } from './AbstractTokenParser';

export class IncludeTokenParser extends AbstractTokenParser {
    parse(token: Token) {
        const expr = this._parser.getExpressionParser().parseExpression();

        const { variables, only, ignoreMissing } = this.parseArguments();

        return new IncludeNode(
            expr,
            variables,
            only,
            ignoreMissing,
            token.line,
            this.getTag()
        );
    }

    parseArguments() {
        const stream = this._parser.getStream();

        let ignoreMissing = false;
        if (stream.nextIf(Token.NAME_TYPE, 'ignore')) {
            stream.expect(Token.NAME_TYPE, 'missing');
            ignoreMissing = true;
        }

        let variables: null | AbstractExpression = null;
        if (stream.nextIf(Token.NAME_TYPE, 'with'))
            variables = this._parser.getExpressionParser().parseExpression();

        let only = false;
        if (stream.nextIf(Token.NAME_TYPE, 'only')) only = true;

        stream.expect(Token.BLOCK_END_TYPE);

        return { variables, only, ignoreMissing };
    }

    getTag() {
        return 'include';
    }
}
