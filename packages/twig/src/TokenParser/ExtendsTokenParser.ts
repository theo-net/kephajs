/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { AbstractTokenParser } from './AbstractTokenParser';
import { SyntaxError } from '../Error/SyntaxError';
import { Token } from '../Token';
import { TwigNode } from '../Node/TwigNode';

export class ExtendsTokenParser extends AbstractTokenParser {
    parse(token: Token) {
        const stream = this._parser.getStream();

        if (this._parser.peekBlockStack())
            throw new SyntaxError(
                'Cannot use "extend" in a block.',
                token.line,
                stream.getSourceContext()
            );
        else if (!this._parser.isMainScope())
            throw new SyntaxError(
                'Cannot use "extend" in a macro.',
                token.line,
                stream.getSourceContext()
            );

        if (null !== this._parser.getParent())
            throw new SyntaxError(
                'Multiple extends tags are forbidden.',
                token.line,
                stream.getSourceContext()
            );
        this._parser.setParent(
            this._parser.getExpressionParser().parseExpression()
        );

        stream.expect(Token.BLOCK_END_TYPE);

        return new TwigNode();
    }

    getTag() {
        return 'extends';
    }
}
