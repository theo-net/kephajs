/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { TwigNode } from '../Node/TwigNode';
import { Parser } from '../Parser';
import { Token } from '../Token';

export abstract class TokenParserInterface {
    abstract setParser(parser: Parser): void;

    abstract parse(token: Token): TwigNode;

    abstract getTag(): string;
}
