/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { TempNameExpression } from '../Node/Expression/TempNameExpression';
import { PrintNode } from '../Node/PrintNode';
import { SetNode } from '../Node/SetNode';
import { TwigNode } from '../Node/TwigNode';
import { Token } from '../Token';
import { AbstractTokenParser } from './AbstractTokenParser';

export class ApplyTokenParser extends AbstractTokenParser {
    parse(token: Token) {
        const lineno = token.line;
        const varName = this._parser.getVarName();

        const ref = new TempNameExpression(varName, lineno);
        ref.setAttribute('alwaysDefined', true);

        const filter = this._parser
            .getExpressionParser()
            .parseFilterExpressionRaw(ref, this.getTag());

        this._parser.getStream().expect(Token.BLOCK_END_TYPE);
        const body = this._parser.subparse(
            this.decideApplyEnd.bind(this),
            true
        );
        this._parser.getStream().expect(Token.BLOCK_END_TYPE);

        return new TwigNode([
            new SetNode(true, ref, body, lineno, this.getTag()),
            new PrintNode(filter, lineno, this.getTag()),
        ]);
    }

    decideApplyEnd(token: Token) {
        return token.test('endapply');
    }

    getTag() {
        return 'apply';
    }
}
