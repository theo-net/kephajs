/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Token } from '../Token';
import { AbstractTokenParser } from './AbstractTokenParser';
import { SyntaxError } from '../Error/SyntaxError';
import { IncludeNode } from '../Node/IncludeNode';
import { TextNode } from '../Node/TextNode';
import { SandboxNode } from '../Node/SandboxNode';

export class SandboxTokenParser extends AbstractTokenParser {
    parse(token: Token) {
        const stream = this._parser.getStream();
        stream.expect(Token.BLOCK_END_TYPE);
        const body = this._parser.subparse(
            this.decideBlockEnd.bind(this),
            true
        );
        stream.expect(Token.BLOCK_END_TYPE);

        // in a sandbox tag, only include tags are allowed
        if (!(body instanceof IncludeNode)) {
            for (const node of body) {
                if (
                    node instanceof TextNode &&
                    /$\s*^/.test(node.getAttribute('data') as string)
                )
                    continue;

                if (!(node instanceof IncludeNode))
                    throw new SyntaxError(
                        'Only "include" tags are allowed within a "sandbox" section.',
                        node.getTemplateLine(),
                        stream.getSourceContext()
                    );
            }
        }

        return new SandboxNode(body, token.line, this.getTag());
    }

    decideBlockEnd(token: Token) {
        return token.test('endsandbox');
    }

    getTag() {
        return 'sandbox';
    }
}
