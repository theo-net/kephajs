/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { AssignNameExpression } from '../Node/Expression/AssignNameExpression';
import { ImportNode } from '../Node/ImportNode';
import { Token } from '../Token';
import { AbstractTokenParser } from './AbstractTokenParser';

export class ImportTokenParser extends AbstractTokenParser {
    parse(token: Token) {
        const macro = this._parser.getExpressionParser().parseExpression();
        this._parser.getStream().expect(Token.NAME_TYPE, 'as');
        const vars = new AssignNameExpression(
            this._parser.getStream().expect(Token.NAME_TYPE).value as string,
            token.line
        );
        this._parser.getStream().expect(Token.BLOCK_END_TYPE);

        this._parser.addImportedSymbol(
            'template',
            vars.getAttribute('name') as string
        );

        return new ImportNode(
            macro,
            vars,
            token.line,
            this.getTag(),
            this._parser.isMainScope()
        );
    }

    getTag() {
        return 'import';
    }
}
