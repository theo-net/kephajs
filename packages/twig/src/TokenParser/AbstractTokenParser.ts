/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';
import { Parser } from '../Parser';
import { TokenParserInterface } from './TokenParserInterface';

export abstract class AbstractTokenParser extends TokenParserInterface {
    protected _parserInstance: Parser | null = null;

    get _parser() {
        if (this._parserInstance === null)
            this._parserInstance = new Parser(
                new Environment(new RecordLoader())
            );
        return this._parserInstance;
    }

    abstract getTag(): string;

    setParser(parser: Parser) {
        this._parserInstance = parser;
    }
}
