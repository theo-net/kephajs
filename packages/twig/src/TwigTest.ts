/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestExpression } from './Node/Expression/TestExpression';

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

type TwigTestOptions = {
    isVariadic: boolean;
    nodeClass: typeof TestExpression;
    deprecated: boolean | string;
    alternative: null;
    oneMandatoryArgument: boolean;
    needContext: boolean;
};

export class TwigTest {
    private _options: TwigTestOptions;

    private _arguments: string[] = [];

    constructor(
        private _name: string,
        private _callable: null | ((...args: unknown[]) => boolean) = null,
        options: Partial<TwigTestOptions> = {}
    ) {
        this._options = {
            isVariadic: false,
            nodeClass: TestExpression,
            deprecated: false,
            alternative: null,
            oneMandatoryArgument: false,
            needContext: false,
            ...options,
        };
    }

    getName() {
        return this._name;
    }

    getCallable() {
        return this._callable;
    }

    getNodeClass() {
        return this._options.nodeClass;
    }

    setArguments(args: string[]) {
        this._arguments = args;
    }

    getArguments() {
        return this._arguments;
    }

    isVariadic() {
        return this._options.isVariadic;
    }

    isDeprecated() {
        return this._options.deprecated;
    }

    getDeprecatedVersion() {
        return typeof this._options.deprecated === 'boolean'
            ? ''
            : this._options.deprecated;
    }

    getAlternative() {
        return this._options.alternative;
    }

    hasOneMandatoryArgument() {
        return this._options.oneMandatoryArgument;
    }
}
