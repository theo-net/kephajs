/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import os from 'node:os';
import path from 'node:path';

import { expect } from 'chai';

import { Environment } from './Environment';
import { EnvironmentConsole } from './EnvironmentConsole';
import { RecordLoader } from './Loader/RecordLoader';
import { FilesystemCache } from './Cache/FilesystemCache';

describe('@kephajs/twig/EnvironmentConsole', () => {
    it('Should extend Environment', () => {
        const environment = new EnvironmentConsole(new RecordLoader());
        expect(environment).to.be.instanceOf(Environment);
    });

    describe('setCache', () => {
        it('Should create a FilesystemCache if string', () => {
            const environment = new EnvironmentConsole(new RecordLoader(), {
                cache: path.join(os.tmpdir(), 'cache'),
            });
            expect(environment.getCache(false)).to.be.instanceOf(
                FilesystemCache
            );
            expect(
                (environment.getCache(false) as FilesystemCache).getDirectory()
            ).to.be.equal(path.join(os.tmpdir(), 'cache') + path.sep);
        });
    });
});
