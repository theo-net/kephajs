/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Parser } from './Parser';
import { SyntaxError } from './Error/SyntaxError';
import { Environment } from './Environment';
import { RecordLoader } from './Loader/RecordLoader';
import { Source } from './Source';
import { ArrayExpression } from './Node/Expression/ArrayExpression';
import { ConstantExpression } from './Node/Expression/ConstantExpression';
import { NameExpression } from './Node/Expression/NameExpression';
import { ConcatBinary } from './Node/Expression/Binary/ConcatBinary';
import { RecordExpression } from './Node/Expression/RecordExpression';

describe('@kephajs/twig/ExpressionParser', () => {
    let env: Environment;
    let parser: Parser;

    beforeEach(() => {
        env = new Environment(new RecordLoader(), {
            cache: false,
            autoescape: false,
        });
        parser = new Parser(env);
    });

    it('Should assign only to names', () => {
        [
            '{% set false = "foo" %}',
            '{% set FALSE = "foo" %}',
            '{% set true = "foo" %}',
            '{% set TRUE = "foo" %}',
            '{% set none = "foo" %}',
            '{% set NONE = "foo" %}',
            '{% set null = "foo" %}',
            '{% set NULL = "foo" %}',
            '{% set 3 = "foo" %}',
            '{% set 1 + 2 = "foo" %}',
            '{% set "bar" = "foo" %}',
            '{% set %}{% endset %}',
        ].forEach(template => {
            try {
                parser.parse(env.tokenize(new Source(template, 'index')));
            } catch (error) {
                expect(error).to.be.instanceOf(SyntaxError);
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    it('Should parse array and record expression', () => {
        [
            // simple array
            [
                '{{ [1, 2] }}',
                new ArrayExpression(
                    [
                        new ConstantExpression(1, 1),
                        new ConstantExpression(2, 1),
                    ],
                    1
                ),
            ],

            // array with trailing ,
            [
                '{{ [1, 2, ] }}',
                new ArrayExpression(
                    [
                        new ConstantExpression(1, 1),
                        new ConstantExpression(2, 1),
                    ],
                    1
                ),
            ],

            // simple hash
            [
                '{{ {"a": "b", "b": "c"} }}',
                new RecordExpression(
                    [
                        new ConstantExpression('a', 1),
                        new ConstantExpression('b', 1),

                        new ConstantExpression('b', 1),
                        new ConstantExpression('c', 1),
                    ],
                    1
                ),
            ],

            // hash with trailing ,
            [
                '{{ {"a": "b", "b": "c", } }}',
                new RecordExpression(
                    [
                        new ConstantExpression('a', 1),
                        new ConstantExpression('b', 1),

                        new ConstantExpression('b', 1),
                        new ConstantExpression('c', 1),
                    ],
                    1
                ),
            ],

            // hash in an array
            [
                '{{ [1, {"a": "b", "b": "c"}] }}',
                new ArrayExpression(
                    [
                        new ConstantExpression(1, 1),
                        new RecordExpression(
                            [
                                new ConstantExpression('a', 1),
                                new ConstantExpression('b', 1),

                                new ConstantExpression('b', 1),
                                new ConstantExpression('c', 1),
                            ],
                            1
                        ),
                    ],
                    1
                ),
            ],

            // array in a hash
            [
                '{{ {"a": [1, 2], "b": "c"} }}',
                new RecordExpression(
                    [
                        new ConstantExpression('a', 1),
                        new ArrayExpression(
                            [
                                new ConstantExpression(1, 1),
                                new ConstantExpression(2, 1),
                            ],
                            1
                        ),
                        new ConstantExpression('b', 1),
                        new ConstantExpression('c', 1),
                    ],
                    1
                ),
            ],
            [
                '{{ {a, b} }}',
                new RecordExpression(
                    [
                        new ConstantExpression('a', 1),
                        new NameExpression('a', 1),
                        new ConstantExpression('b', 1),
                        new NameExpression('b', 1),
                    ],
                    1
                ),
            ],
        ].forEach(test => {
            const source = new Source(test[0] as string, '');
            const stream = env.tokenize(source);
            (test[1] as ArrayExpression).setSourceContext(source);

            expect(
                parser.parse(stream).getNode('body').getNode(0).getNode('expr'),
                test[0] as string
            ).to.be.deep.equal(test[1]);
        });
    });

    it('Should throw SyntaxError if syntax error in array', () => {
        ['{{ [1, "a": "b"] }}', '{{ {"a": "b", 2} }}', '{{ {"a"} }}'].forEach(
            template => {
                try {
                    parser.parse(env.tokenize(new Source(template, 'index')));
                } catch (error) {
                    expect(error).to.be.instanceOf(SyntaxError);
                    return;
                }
                expect.fail('Should have thrown');
            }
        );
    });

    it('Should throw SyntaxError if an expression try to concatenate two consecutive strings', () => {
        try {
            parser.parse(env.tokenize(new Source('{{ "a" "b" }}', 'index')));
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            return;
        }
        expect.fail('Should have thrown');
    });

    it('Should parse string expression', () => {
        [
            ['{{ "foo" }}', new ConstantExpression('foo', 1)],
            [
                '{{ "foo #{bar}" }}',
                new ConcatBinary(
                    new ConstantExpression('foo ', 1),
                    new NameExpression('bar', 1),
                    1
                ),
            ],
            [
                '{{ "foo #{bar} baz" }}',
                new ConcatBinary(
                    new ConcatBinary(
                        new ConstantExpression('foo ', 1),
                        new NameExpression('bar', 1),
                        1
                    ),
                    new ConstantExpression(' baz', 1),
                    1
                ),
            ],

            [
                '{{ "foo #{"foo #{bar} baz"} baz" }}',
                new ConcatBinary(
                    new ConcatBinary(
                        new ConstantExpression('foo ', 1),
                        new ConcatBinary(
                            new ConcatBinary(
                                new ConstantExpression('foo ', 1),
                                new NameExpression('bar', 1),
                                1
                            ),
                            new ConstantExpression(' baz', 1),
                            1
                        ),
                        1
                    ),
                    new ConstantExpression(' baz', 1),
                    1
                ),
            ],
        ].forEach(test => {
            const source = new Source(test[0] as string, '');
            (test[1] as ConcatBinary).setSourceContext(source);
            expect(
                parser
                    .parse(env.tokenize(source))
                    .getNode('body')
                    .getNode(0)
                    .getNode('expr')
            ).to.be.deep.equal(test[1]);
        });
    });

    it('Should throw SyntaxError, because attribute call does not support named argument', () => {
        try {
            parser.parse(
                env.tokenize(new Source('{{ foo.bar(name="Foo") }}', 'index'))
            );
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            return;
        }
        expect.fail('Should have thrown');
    });

    it('Should throw SyntaxError, because macro call does not support named argument', () => {
        try {
            parser.parse(
                env.tokenize(
                    new Source(
                        '{% from _self import foo %}{% macro foo() %}{% endmacro %}{{ foo(name="Foo") }}',
                        'index'
                    )
                )
            );
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            return;
        }
        expect.fail('Should have thrown');
    });

    it('Should throw SyntaxError, because macro definition does not support non name variable name', () => {
        try {
            parser.parse(
                env.tokenize(
                    new Source('{% macro foo("a") %}{% endmacro %}', 'index')
                )
            );
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            expect((error as SyntaxError).message).to.be.equal(
                'An argument must be a name. Unexpected token "string" of value "a" ("name" expected) in "index" at line 1.'
            );
            return;
        }
        expect.fail('Should have thrown');
    });

    it('Should throw SyntaxError, because test macro definition does not support non constant default values', () => {
        [
            '{% macro foo(name = "a #{foo} a") %}{% endmacro %}',
            '{% macro foo(name = [["b", "a #{foo} a"]]) %}{% endmacro %}',
        ].forEach(template => {
            try {
                parser.parse(env.tokenize(new Source(template, 'index')));
            } catch (error) {
                expect(error).to.be.instanceOf(SyntaxError);
                expect((error as SyntaxError).message).to.be.equal(
                    'A default value for an argument must be a constant (a boolean, a string, a number, an array, or a record) in "index" at line 1.'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    it('Should have macro definition supports constant default values', () => {
        [
            '{% macro foo(name = "aa") %}{% endmacro %}',
            '{% macro foo(name = 12) %}{% endmacro %}',
            '{% macro foo(name = true) %}{% endmacro %}',
            '{% macro foo(name = ["a"]) %}{% endmacro %}',
            '{% macro foo(name = [["a"]]) %}{% endmacro %}',
            '{% macro foo(name = {a: "a"}) %}{% endmacro %}',
            '{% macro foo(name = {a: {b: "a"}}) %}{% endmacro %}',
        ].forEach(template => {
            parser.parse(env.tokenize(new Source(template, 'index')));
        });
        expect(1).to.be.equal(1);
    });

    it('Should throw SyntaxError if unknown function', () => {
        try {
            parser.parse(env.tokenize(new Source('{{ cycl() }}', 'index')));
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            expect((error as SyntaxError).message).to.be.equal(
                'Unknown "cycl" function. Did you mean "cycle" in "index" at line 1?'
            );
            return;
        }
        expect.fail('Should have thrown');
    });

    it('Should throw SyntaxError if unknown function (without suggestions)', () => {
        try {
            parser.parse(env.tokenize(new Source('{{ foobar() }}', 'index')));
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            expect((error as SyntaxError).message).to.be.equal(
                'Unknown "foobar" function in "index" at line 1.'
            );
            return;
        }
    });

    it('Should throw SyntaxError if unknown filter', () => {
        try {
            parser.parse(env.tokenize(new Source('{{ 1|lowe }}', 'index')));
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            expect((error as SyntaxError).message).to.be.equal(
                'Unknown "lowe" filter. Did you mean "lower" in "index" at line 1?'
            );
            return;
        }
    });

    it('Should throw SyntaxError if unknown filter (without suggestions)', () => {
        try {
            parser.parse(env.tokenize(new Source('{{ 1|foobar }}', 'index')));
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            expect((error as SyntaxError).message).to.be.equal(
                'Unknown "foobar" filter in "index" at line 1.'
            );
            return;
        }
    });

    it('Should throw SyntaxError if unknown test', () => {
        try {
            parser.parse(env.tokenize(new Source('{{ 1 is nul }}', 'index')));
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            expect((error as SyntaxError).message).to.be.equal(
                'Unknown "nul" test. Did you mean "null" in "index" at line 1?'
            );
            return;
        }
    });

    it('Should throw SyntaxError if unknown test (without suggestions)', () => {
        try {
            parser.parse(
                env.tokenize(new Source('{{ 1 is foobar }}', 'index'))
            );
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            expect((error as SyntaxError).message).to.be.equal(
                'Unknown "foobar" test in "index" at line 1.'
            );
            return;
        }
    });
});
