/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Source } from './Source';
import { SyntaxError } from './Error/SyntaxError';
import { Token } from './Token';

export class TokenStream {
    private _current = 0;

    constructor(
        private _tokens: Token[] = [],
        private _source = new Source('', '')
    ) {}

    toString() {
        return this._tokens.join('\n');
    }

    injectTokens(tokens: Token[]) {
        this._tokens = [
            ...this._tokens.slice(0, this._current),
            ...tokens,
            ...this._tokens.slice(this._current),
        ];
    }

    next() {
        if (this._tokens[++this._current] === undefined)
            throw new SyntaxError(
                'Unexpected end of template.',
                this._tokens[this._current - 1].line,
                this._source
            );

        return this._tokens[this._current - 1];
    }

    nextIf(primary: number | string, secondary: null | string = null) {
        if (this._tokens[this._current].test(primary, secondary))
            return this.next();
        return null;
    }

    public expect(
        type: number,
        value: string | number | bigint | null = null,
        message = ''
    ) {
        const token = this._tokens[this._current];
        if (!token.test(type, value)) {
            const line = token.line;
            throw new SyntaxError(
                (message !== '' ? message + '. ' : '') +
                    'Unexpected token "' +
                    Token.typeToEnglish(token.type) +
                    '"' +
                    (token.value ? ` of value "${token.value}"` : '') +
                    ' ("' +
                    Token.typeToEnglish(type) +
                    '"' +
                    (value ? ` with value "${value}"` : '') +
                    ' expected).',
                line,
                this._source
            );
        }
        this.next();

        return token;
    }

    look(number = 1) {
        if (this._tokens[this._current + number] === undefined)
            throw new SyntaxError(
                'Unexpected end of template.',
                this._tokens[this._current + number - 1].line,
                this._source
            );

        return this._tokens[this._current + number];
    }

    test(primary: unknown, secondary: unknown = null) {
        return this._tokens[this._current].test(primary, secondary);
    }

    isEOF() {
        return this._tokens[this._current].type === Token.EOF_TYPE;
    }

    getCurrent() {
        return this._tokens[this._current];
    }

    getSourceContext() {
        return this._source;
    }
}
