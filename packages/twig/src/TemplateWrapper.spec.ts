/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Environment } from './Environment';
import { RecordLoader } from './Loader/RecordLoader';

describe('@kephajs/console/twig/TemplateWrapper', () => {
    it('Should has get blocks', () => {
        const twig = new Environment(
            new RecordLoader({
                index: '{% block foo %}{% endblock %}',
                index_with_use:
                    '{% use "imported" %}{% block foo %}{% endblock %}',
                index_with_extends:
                    '{% extends "extended" %}{% block foo %}{% endblock %}',
                imported: '{% block imported %}{% endblock %}',
                extended: '{% block extended %}{% endblock %}',
            })
        );

        let wrapper = twig.load('index');
        expect(wrapper.hasBlock('foo')).to.be.equal(true);
        expect(wrapper.hasBlock('bar')).to.be.equal(false);
        expect(wrapper.getBlockNames()).to.be.deep.equal(['foo']);

        wrapper = twig.load('index_with_use');
        expect(wrapper.hasBlock('foo')).to.be.equal(true);
        expect(wrapper.hasBlock('imported')).to.be.equal(true);
        expect(wrapper.getBlockNames()).to.be.deep.equal(['imported', 'foo']);

        wrapper = twig.load('index_with_extends');
        expect(wrapper.hasBlock('foo')).to.be.equal(true);
        expect(wrapper.hasBlock('extended')).to.be.equal(true);
        expect(wrapper.getBlockNames()).to.be.deep.equal(['foo', 'extended']);
    });

    it('Should render block', () => {
        const twig = new Environment(
            new RecordLoader({
                index: '{% block foo %}{{ foo }}{{ bar }}{% endblock %}',
            })
        );
        twig.addGlobal('bar', 'BAR');

        const wrapper = twig.load('index');
        expect(wrapper.renderBlock('foo', { foo: 'FOO' })).to.be.equal(
            'FOOBAR'
        );
    });
});
