/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { levenshtein } from '@kephajs/core/Helpers/Helpers';
import { TwigError } from './TwigError';

export class SyntaxError extends TwigError {
    addSuggestions(badName: string, items: string[]) {
        const alternatives: Record<string, number> = {};
        let empty = true;
        items.forEach(item => {
            const lev = levenshtein(badName, item);
            if (lev <= badName.length / 3 || item.indexOf(badName) > -1) {
                alternatives[item] = lev;
                empty = false;
            }
        });

        if (empty) return;

        this.appendMessage(
            ` Did you mean "${Object.keys(alternatives).sort().join(', ')}"?`
        );
    }
}
