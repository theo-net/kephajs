/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Source } from '../Source';

export class TwigError extends Error {
    protected _file = '';

    protected _line = -1;

    protected _name: string | null;

    protected _sourceCode: null | string = null;

    protected _sourcePath: null | string = null;

    constructor(
        protected _rawMessage: string,
        protected _lineno = -1,
        source: Source | null = null
    ) {
        super('');

        if (source === null) this._name = null;
        else {
            this._name = source.name;
            this._sourceCode = source.code;
            this._sourcePath = source.path;
        }

        this._updateRepr();
    }

    getLine() {
        return this._line;
    }

    getFile() {
        return this._file;
    }

    /*
    public function getRawMessage(): string
    {
        return this.rawMessage;
    }*/

    getTemplateLine() {
        return this._lineno;
    }

    setTemplateLine(lineno: number) {
        this._lineno = lineno;

        this._updateRepr();
    }

    getSourceContext() {
        return this._name
            ? new Source(
                  this._sourceCode as string,
                  this._name,
                  this._sourcePath as string
              )
            : null;
    }

    setSourceContext(source: null | Source = null) {
        if (source === null)
            this._sourceCode = this._name = this._sourcePath = null;
        else {
            this._sourceCode = source.code;
            this._name = source.name;
            this._sourcePath = source.path;
        }

        this._updateRepr();
    }

    guess() {
        this._updateRepr();
    }

    appendMessage(rawMessage: string) {
        this._rawMessage += rawMessage;
        this._updateRepr();
    }

    private _updateRepr() {
        this.message = this._rawMessage;

        if (this._sourcePath !== null && this._lineno > 0) {
            this._file = this._sourcePath;
            this._line = this._lineno;
        }

        let dot = false;
        if (this.message[this.message.length - 1] === '.') {
            this.message = this.message.substring(0, this.message.length - 1);
            dot = true;
        }

        let questionMark = false;
        if (this.message[this.message.length - 1] === '?') {
            this.message = this.message.substring(0, this.message.length - 1);
            questionMark = true;
        }

        if (this._name !== null && this._name !== '')
            this.message += ' in "' + this._name + '"';

        if (this._lineno && this._lineno >= 0) {
            this.message += ' at line ' + this._lineno;
        }

        if (dot) this.message += '.';

        if (questionMark) this.message += '?';
    }
}
