/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Environment } from '../Environment';
import { AbstractExtension } from '../Extension/AbstractExtension';
import { RecordLoader } from '../Loader/RecordLoader';
import { Source } from '../Source';
import { TwigFilter, TwigFilterType } from '../TwigFilter';
import { RuntimeError } from './RuntimeError';
import { TwigError } from './TwigError';

describe('@kephajs/twig/Error/TwigError', () => {
    class ErrorTestFoo {
        bar() {
            throw new Error('Runtime error...');
        }
    }

    it('Should insert filename in the error message', () => {
        const error = new TwigError(
            'test',
            3,
            new Source('tests', 'myfilename.twig')
        );
        expect(error.message).to.be.contain('myfilename.twig');
    });

    it('Should define message for missing var with RecordLoader', () => {
        const loader = new RecordLoader({
            'base.html': '{% block content %}{% endblock %}',
            'index.html':
                "{% extends 'base.html' %}\n{% block content %}\n    {{ foo.bar }}\n{% endblock %}\n{% block foo %}\n    {{ foo.bar }}\n{% endblock %}\n",
        });

        const twig = new Environment(loader, {
            strictVariables: true,
            cache: false,
        });

        const template = twig.load('index.html');
        try {
            template.render({});

            expect.fail();
        } catch (error) {
            expect(error).to.be.instanceOf(RuntimeError);
            expect((error as RuntimeError).message).to.be.equal(
                'Variable "foo" does not exist in "index.html" at line 3.'
            );
            expect((error as RuntimeError).getTemplateLine()).to.be.equal(3);
            expect(
                (error as RuntimeError).getSourceContext()?.name
            ).to.be.equal('index.html');
        }
    });

    it('Should guess error with exceptions and RecordLoader', () => {
        const loader = new RecordLoader({
            'base.html': '{% block content %}{% endblock %}',
            'index.html':
                "{% extends 'base.html' %}\n{% block content %}\n    {{ foo.bar }}\n{% endblock %}\n{% block foo %}\n    {{ foo.bar }}\n{% endblock %}",
        });

        const twig = new Environment(loader, {
            strictVariables: true,
            cache: false,
        });

        const template = twig.load('index.html');
        try {
            template.render({ foo: new ErrorTestFoo() });

            expect.fail();
        } catch (error) {
            expect(error).to.be.instanceOf(RuntimeError);
            expect((error as RuntimeError).message).to.be.equal(
                'An exception has been thrown during the rendering of a template ("Runtime error...") in "index.html".'
            );
            expect(
                (error as RuntimeError).getSourceContext()?.name
            ).to.be.equal('index.html');
        }
    });

    it('Should add file name and line', () => {
        (
            [
                // error occurs in a template
                [
                    {
                        index: "\n\n{{ foo.bar }}\n\n\n{{ 'foo' }}",
                    },
                    'index',
                    3,
                ],

                // error occurs in an included template
                [
                    {
                        index: "{% include 'partial' %}",
                        partial: '{{ foo.bar }}',
                    },
                    'partial',
                    1,
                ],

                // error occurs in a parent block when called via parent()
                [
                    {
                        index: "{% extends 'base' %}\n{% block content %}\n    {{ parent() }}\n{% endblock %}",
                        base: '{% block content %}{{ foo.bar }}{% endblock %}',
                    },
                    'base',
                    1,
                ],

                // error occurs in a block from the child
                [
                    {
                        index: "{% extends 'base' %}\n{% block content %}\n    {{ foo.bar }}\n{% endblock %}\n{% block foo %}\n    {{ foo.bar }}\n{% endblock %}",
                        base: '{% block content %}{% endblock %}',
                    },
                    'index',
                    3,
                ],
            ] as [Record<string, string>, string, number][]
        ).forEach(test => {
            const twig = new Environment(new RecordLoader(test[0]), {
                strictVariables: true,
                cache: false,
            });
            const template = twig.load('index');

            try {
                template.render({});
                expect.fail();
            } catch (error) {
                expect(error).to.be.instanceOf(RuntimeError);
                expect((error as RuntimeError).message).to.be.equal(
                    `Variable "foo" does not exist in "${test[1]}" at line ${test[2]}.`
                );
                expect((error as RuntimeError).getTemplateLine()).to.be.equal(
                    test[2]
                );
                expect(
                    (error as RuntimeError).getSourceContext()?.name
                ).to.be.equal(test[1]);
            }

            try {
                template.render({ foo: new ErrorTestFoo() });
                expect.fail();
            } catch (error) {
                expect(error).to.be.instanceOf(RuntimeError);
                expect((error as RuntimeError).message).to.be.equal(
                    `An exception has been thrown during the rendering of a template ("Runtime error...") in "${test[1]}".`
                );
                expect(
                    (error as RuntimeError).getSourceContext()?.name
                ).to.be.equal(test[1]);
            }
        });
    });

    it('Should catch filter error', () => {
        const loader = new RecordLoader({
            'filter-null.html': `
{% for n in variable|filter(x => x > 3) %}
    This list contains {{n}}.
{% endfor %}`,
        });

        const twig = new Environment(loader, { cache: false });

        const template = twig.load('filter-null.html');
        const out = template.render({ variable: [1, 2, 3, 4] });
        expect(out.trim()).to.be.equal('This list contains 4.');

        try {
            template.render({ variable: null });
            expect.fail();
        } catch (error) {
            expect(
                (error as RuntimeError).getSourceContext()?.name
            ).to.be.equal('filter-null.html');
        }
    });

    it('Should catch map error', () => {
        const loader = new RecordLoader({
            'map-null.html': `
{# We expect a runtime error if 'variable' is not traversable #}
{% for n in variable|map(x => x * 3) %}
    {{- n -}}
{% endfor %}`,
        });

        const twig = new Environment(loader, { cache: false });

        const template = twig.load('map-null.html');
        const out = template.render({ variable: [1, 2, 3, 4] });
        expect(out.trim()).to.be.equal('36912');

        try {
            template.render({ variable: null });
            expect.fail();
        } catch (error) {
            expect(error).to.be.instanceOf(RuntimeError);
            expect(
                (error as RuntimeError).getSourceContext()?.name
            ).to.be.equal('map-null.html');
        }
    });

    it('Should catch reduce error', () => {
        const loader = new RecordLoader({
            'reduce-null.html': `
{# We expect a runtime error if 'variable' is not traversable #}
{{ variable|reduce((carry, x) => carry + x) }}`,
        });

        const twig = new Environment(loader, { cache: false });
        twig.enableStrictVariables();

        const template = twig.load('reduce-null.html');
        const out = template.render({ variable: [1, 2, 3, 4] });
        expect(out.trim()).to.be.equal('10');

        try {
            template.render({ variable: null });
            expect.fail();
        } catch (error) {
            expect(
                (error as RuntimeError).getSourceContext()?.name
            ).to.be.equal('reduce-null.html');
        }
    });

    class BrokenExtension extends AbstractExtension {
        getFilters() {
            return {
                broken: new TwigFilter('broken', this.broken as TwigFilterType),
            };
        }

        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        broken() {
            throw new Error('OOPS');
        }
    }

    it('Should return the error if debug', () => {
        const loader = new RecordLoader({
            'index.html.twig': 'Hello {{ "world"|broken }}',
        });
        const twig = new Environment(loader, { debug: true });
        twig.addExtension(new BrokenExtension());

        expect(twig.render('index.html.twig')).to.be.equal('Hello OOPS');
    });
});
