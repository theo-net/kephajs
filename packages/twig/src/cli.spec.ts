/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import { realpathSync } from 'fs';
import { sep } from 'path';

import { Environment } from './Environment';
import { RuntimeError } from './Error/RuntimeError';
import { FilesystemLoader } from './Loader/FilesystemLoader';

describe('@kephajs/twig', () => {
    describe('@kephajs/twig/Error/TwigError', () => {
        class ErrorTestFoo {
            bar() {
                throw new Error('Runtime error...');
            }
        }

        it('Should guess error with missing var and FilesystemLoader', () => {
            const loader = new FilesystemLoader([
                realpathSync(__dirname + '/../Fixtures/errors'),
            ]);

            const twig = new Environment(loader, {
                strictVariables: true,
                cache: false,
            });

            const template = twig.load('index.html');
            try {
                template.render({});

                expect.fail();
            } catch (error) {
                expect(error).to.be.instanceOf(RuntimeError);
                expect((error as RuntimeError).message).to.be.equal(
                    'Variable "foo" does not exist in "index.html" at line 3.'
                );
                expect((error as RuntimeError).getTemplateLine()).to.be.equal(
                    3
                );
                expect(
                    (error as RuntimeError).getSourceContext()?.name
                ).to.be.equal('index.html');
                expect((error as RuntimeError).getLine()).to.be.equal(3);
                expect((error as RuntimeError).getFile()).to.be.equal(
                    realpathSync(__dirname + '/../Fixtures/errors') +
                        sep +
                        'index.html'
                );
            }
        });

        it('Should guess error with error and FilesystemLoader', () => {
            const loader = new FilesystemLoader([
                realpathSync(__dirname + '/../Fixtures/errors'),
            ]);

            const twig = new Environment(loader, {
                strictVariables: true,
                cache: false,
            });

            const template = twig.load('index.html');
            try {
                template.render({ foo: new ErrorTestFoo() });

                expect.fail();
            } catch (error) {
                expect(error).to.be.instanceOf(RuntimeError);
                expect((error as RuntimeError).message).to.be.equal(
                    'An exception has been thrown during the rendering of a template ("Runtime error...") in "index.html".'
                );
                expect(
                    (error as RuntimeError).getSourceContext()?.name
                ).to.be.equal('index.html');
            }
        });
    });
});

/*public function testLoadTemplateAndRenderBlockWithCache()
{
    const loader = new FilesystemLoader([]);
    loader.addPath(__dirname + '/Fixtures/themes/theme2');
    loader.addPath(__dirname + '/Fixtures/themes/theme1');
    loader.addPath(__dirname + '/Fixtures/themes/theme1', 'default_theme');

    $twig = new Environment($loader);

    $template = $twig->load('blocks.html.twig');
    $this->assertSame('block from theme 1', $template->renderBlock('b1', []));

    $template = $twig->load('blocks.html.twig');
    $this->assertSame('block from theme 2', $template->renderBlock('b2', []));
}


    public function getArrayInheritanceTests()
    {
        return [
            'valid array inheritance' => ['array_inheritance_valid_parent.html.twig'],
            'array inheritance with empty first template' => ['array_inheritance_empty_parent.html.twig'],
            'array inheritance with non-existent first template' => ['array_inheritance_nonexistent_parent.html.twig'],
        ];
    }

    /**
     * @dataProvider getArrayInheritanceTests
     */
/*public function testArrayInheritance(string $templateName)
    {
        const loader = new FilesystemLoader([]);
        loader.addPath(__dirname + '/Fixtures/inheritance');

        $twig = new Environment(loader);

        $template = $twig->load($templateName);
        $this->assertSame('VALID Child', $template->renderBlock('body', []));
    }

*/
