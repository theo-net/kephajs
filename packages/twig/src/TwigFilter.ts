/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { FilterExpression } from './Node/Expression/FilterExpression';

type FilterOptions = {
    needsEnvironment: boolean;
    needsContext: boolean;
    isVariadic: boolean;
    isSafe: string[] | null;
    isSafeCallback: null;
    preEscape: string | null;
    preservesSafety: null | string[];
    nodeClass: typeof FilterExpression;
    deprecated: boolean | string;
    alternative: null | string;
};

export type TwigFilterType = (...args: unknown[]) => unknown;

export class TwigFilter {
    private _name: string;

    private _options: FilterOptions;

    private _arguments: string[] = [];

    constructor(
        filterName: string,
        private _callable: TwigFilterType | null = null,
        options: Partial<FilterOptions> = {}
    ) {
        this._name = filterName;
        this._options = {
            needsEnvironment: false,
            needsContext: false,
            isVariadic: false,
            isSafe: null,
            isSafeCallback: null,
            preEscape: null,
            preservesSafety: null,
            nodeClass: FilterExpression,
            deprecated: false,
            alternative: null,
            ...options,
        };
    }

    getName() {
        return this._name;
    }

    getCallable() {
        return this._callable;
    }

    getNodeClass() {
        return this._options.nodeClass;
    }

    setArguments(args: string[]) {
        this._arguments = args;
    }

    getArguments() {
        return this._arguments;
    }

    needsEnvironment() {
        return this._options.needsEnvironment;
    }

    needsContext() {
        return this._options.needsContext;
    }

    /*public function getSafe(Node $filterArgs): ?array
    {
        if (null !== this.options['is_safe']) {
            return this.options['is_safe'];
        }

        if (null !== this.options['is_safe_callback']) {
            return this.options['is_safe_callback']($filterArgs);
        }

        return null;
    }

    public function getPreservesSafety(): ?array
    {
        return this.options['preserves_safety'];
    }

    public function getPreEscape(): ?string
    {
        return this.options['pre_escape'];
    }*/

    isVariadic() {
        return this._options.isVariadic;
    }

    isDeprecated() {
        return this._options.deprecated !== false;
    }

    getDeprecatedVersion() {
        return typeof this._options.deprecated === 'boolean'
            ? ''
            : this._options.deprecated;
    }

    getAlternative() {
        return this._options.alternative as string;
    }
}
