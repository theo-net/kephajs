/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Environment } from './Environment';
import { TwigNode } from './Node/TwigNode';
import { NodeVisitorInterface } from './NodeVisitor/NodeVisitorInterface';

export class NodeTraverser {
    private _visitors: Record<number, NodeVisitorInterface[]> = {};

    constructor(
        private _env: Environment,
        visitors: NodeVisitorInterface[] = []
    ) {
        visitors.forEach(visitor => {
            this.addVisitor(visitor);
        });
    }

    addVisitor(visitor: NodeVisitorInterface) {
        if (this._visitors[visitor.getPriority()] === undefined)
            this._visitors[visitor.getPriority()] = [];
        this._visitors[visitor.getPriority()].push(visitor);
    }

    traverse(node: TwigNode) {
        Object.keys(this._visitors)
            .sort()
            .forEach(priority => {
                this._visitors[Number(priority)].forEach(visitor => {
                    node = this._traverseForVisitor(visitor, node) as TwigNode;
                });
            });

        return node;
    }

    private _traverseForVisitor(visitor: NodeVisitorInterface, node: TwigNode) {
        node = visitor.enterNode(node, this._env);
        const nodes = node.getNodes();

        Object.keys(nodes).forEach(key => {
            const m = this._traverseForVisitor(visitor, nodes[key]);
            if (m !== null) {
                if (m !== nodes[key]) node.setNode(key, m);
            } else node.removeNode(key);
        });

        return visitor.leaveNode(node, this._env);
    }
}
