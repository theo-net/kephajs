/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { objectKeys } from '@kephajs/core/Helpers/Helpers';
import { escape } from '@kephajs/core/Helpers/RegExpHelpers';
import { RuntimeError } from './Error/RuntimeError';
import { ExtensionInterface, Operator } from './Extension/ExtensionInterface';
import { GlobalsInterface } from './Extension/GlobalsInterface';
import { StagingExtension } from './Extension/StagingExtension';
import { NodeVisitorInterface } from './NodeVisitor/NodeVisitorInterface';
import { TokenParserInterface } from './TokenParser/TokenParserInterface';
import { TwigFilter } from './TwigFilter';
import { TwigFunction } from './TwigFunction';
import { TwigTest } from './TwigTest';

export class ExtensionSet {
    private _extensions: Record<string, ExtensionInterface> = {};

    private _initialized = false;

    private _runtimeInitialized = false;

    private _parsers: Record<string, TokenParserInterface> = {};

    private _filters: Record<string, TwigFilter> = {};

    private _functions: Record<string, TwigFunction> = {};

    private _tests: Record<string, TwigTest> = {};

    private _visitors: NodeVisitorInterface[] = [];

    private _unaryOperators: Record<string, Operator> = {};

    private _binaryOperators: Record<string, Operator> = {};

    private _staging = new StagingExtension();

    private _globals: Record<string, unknown> | null = null;

    private _functionCallbacks: ((fctName: string) => TwigFunction | false)[] =
        [];

    private _filterCallbacks: ((filterName: string) => TwigFilter | false)[] =
        [];

    private _parserCallbacks: ((
        parserName: string
    ) => TokenParserInterface | null)[] = [];

    initRuntime() {
        this._runtimeInitialized = true;
    }

    hasExtension(className: string) {
        return this._extensions[className] !== undefined;
    }

    getExtension(className: string) {
        if (this._extensions[className] === undefined)
            throw new RuntimeError(
                `The "${className}" extension is not enabled.`
            );

        return this._extensions[className];
    }

    setExtensions(extensions: ExtensionInterface[]) {
        extensions.forEach(extension => {
            this.addExtension(extension);
        });
    }

    getExtensions() {
        return this._extensions;
    }

    getSignature() {
        return JSON.stringify(objectKeys(this._extensions));
    }

    isInitialized() {
        return this._initialized || this._runtimeInitialized;
    }

    addExtension(extension: ExtensionInterface) {
        const name = extension.constructor.name;

        if (this._initialized)
            throw new Error(
                `Unable to register extension "${name}" as extensions have already been initialized.`
            );

        if (this._extensions[name] !== undefined)
            throw new Error(
                `Unable to register extension "${name}" as it is already registered.`
            );

        this._extensions[name] = extension;
    }

    addFunction(fct: TwigFunction) {
        if (this._initialized)
            throw new Error(
                `Unable to add function "${fct.getName()}" as extensions have already been initialized.`
            );

        this._staging.addFunction(fct);
    }

    getFunctions() {
        if (!this._initialized) this._initExtensions();

        return this._functions;
    }

    getFunction(fctName: string) {
        if (!this._initialized) this._initExtensions();

        if (this._functions[fctName] !== undefined)
            return this._functions[fctName];

        for (const pattern in this._functions) {
            const escapedPattern = escape(pattern);
            const starPattern = pattern.replace(/\*/g, '(.*?)');

            const matches = fctName.match(new RegExp('^' + starPattern + '$'));
            if (escapedPattern !== starPattern && matches !== null) {
                matches.shift();
                this._functions[pattern].setArguments(matches);

                return this._functions[pattern];
            }
        }

        let fctReturned: null | TwigFunction = null;
        this._functionCallbacks.every(callback => {
            const filter = callback(fctName);
            if (filter !== false) {
                fctReturned = filter;
                return false;
            } else return true;
        });

        return fctReturned;
    }

    /*public function registerUndefinedFunctionCallback(callable $callable): void
    {
        $this->functionCallbacks[] = $callable;
    }*/

    addFilter(filter: TwigFilter) {
        if (this._initialized)
            throw new Error(
                `Unable to add filter "${filter.getName()}" as extensions have already been initialized.`
            );

        this._staging.addFilter(filter);
    }

    getFilters() {
        if (!this._initialized) this._initExtensions();

        return this._filters;
    }

    getFilter(filterName: string) {
        if (!this._initialized) this._initExtensions();

        if (this._filters[filterName]) return this._filters[filterName];

        for (const pattern in this._filters) {
            const escapedPattern = escape(pattern);
            const starPattern = pattern.replace(/\*/g, '(.*?)');

            const matches = filterName.match(
                new RegExp('^' + starPattern + '$')
            );
            if (escapedPattern !== starPattern && matches !== null) {
                matches.shift();
                this._filters[pattern].setArguments(matches);

                return this._filters[pattern];
            }
        }

        let filterReturned: null | TwigFilter = null;
        this._filterCallbacks.every(callback => {
            const filter = callback(filterName);
            if (filter !== false) {
                filterReturned = filter;
                return false;
            } else return true;
        });

        return filterReturned;
    }

    registerUndefinedFilterCallback(
        callable: (filterName: string) => TwigFilter | false
    ) {
        this._filterCallbacks.push(callable);
    }

    /*public function addNodeVisitor(NodeVisitorInterface $visitor): void
    {
        if ($this->initialized) {
            throw new \LogicException('Unable to add a node visitor as extensions have already been initialized.');
        }

        $this->staging->addNodeVisitor($visitor);
    }*/

    getNodeVisitors() {
        if (!this._initialized) this._initExtensions();

        return this._visitors;
    }

    addTokenParser(parser: TokenParserInterface) {
        if (this._initialized)
            throw new Error(
                'Unable to add a token parser as extensions have already been initialized.'
            );

        this._staging.addTokenParser(parser);
    }

    getTokenParsers() {
        if (!this._initialized) this._initExtensions();

        return this._parsers;
    }

    getTokenParser(tokenParserName: string) {
        if (!this._initialized) this._initExtensions();

        if (this._parsers[tokenParserName] !== undefined)
            return this._parsers[tokenParserName];

        let parserReturned: null | TokenParserInterface = null;
        this._parserCallbacks.forEach(callback => {
            const parser = callback(tokenParserName);
            if (parser !== null) parserReturned = parser;
        });

        return parserReturned;
    }

    registerUndefinedTokenParserCallback(
        callable: (parserName: string) => TokenParserInterface | null
    ) {
        this._parserCallbacks.push(callable);
    }

    getGlobals() {
        if (this._globals !== null) return this._globals;

        let globals: Record<string, unknown> = {};
        objectKeys(this._extensions).forEach(key => {
            if (
                (this._extensions[key] as unknown as GlobalsInterface)
                    .getGlobals === undefined
            )
                return;

            const extGlobals = (
                this._extensions[key] as unknown as GlobalsInterface
            ).getGlobals();
            if (typeof extGlobals !== 'object')
                throw new TypeError(
                    `"${this._extensions[key].constructor.name}.getGlobals()" must return an array of globals.`
                );

            globals = { ...globals, ...extGlobals };
        });

        if (this._initialized) this._globals = globals;

        return globals;
    }

    addTest(test: TwigTest) {
        if (this._initialized)
            throw new Error(
                `Unable to add test "${test.getName()}" as extensions have already been initialized.`
            );

        this._staging.addTest(test);
    }

    getTests() {
        if (!this._initialized) this._initExtensions();

        return this._tests;
    }

    getTest(testName: string) {
        if (!this._initialized) this._initExtensions();

        if (this._tests[testName] !== undefined) return this._tests[testName];

        for (const pattern in this._tests) {
            const escapedPattern = escape(pattern);
            const starPattern = pattern.replace(/\*/g, '(.*?)');

            if (escapedPattern !== starPattern) {
                const matches = testName.match(
                    new RegExp('^' + starPattern + '$')
                );
                if (matches !== null) {
                    matches.shift();
                    this._tests[pattern].setArguments(matches);

                    return this._tests[pattern];
                }
            }
        }

        return null;
    }

    getUnaryOperators() {
        if (!this._initialized) this._initExtensions();

        return this._unaryOperators;
    }

    getBinaryOperators() {
        if (!this._initialized) this._initExtensions();

        return this._binaryOperators;
    }

    private _initExtensions() {
        this._parsers = {};
        this._filters = {};
        this._functions = {};
        this._tests = {};
        this._visitors = [];
        this._unaryOperators = {};
        this._binaryOperators = {};

        for (const key in this._extensions)
            this._initExtension(this._extensions[key]);
        this._initExtension(this._staging);
        // Done at the end only, so that an exception during initialization does not mark the environment as initialized when catching the exception
        this._initialized = true;
    }

    private _initExtension(extension: ExtensionInterface) {
        // filters
        const filters = extension.getFilters();
        objectKeys(filters).forEach(filterName => {
            this._filters[filters[filterName].getName()] = filters[filterName];
        });

        // functions
        const functions = extension.getFunctions();
        objectKeys(functions).forEach(fctName => {
            this._functions[functions[fctName].getName()] = functions[fctName];
        });

        // tests
        objectKeys(extension.getTests()).forEach(key => {
            this._tests[extension.getTests()[key].getName()] =
                extension.getTests()[key];
        });

        // token parsers
        const tokenParsers = extension.getTokenParsers();
        objectKeys(tokenParsers).forEach(parserName => {
            if (!(tokenParsers[parserName] instanceof TokenParserInterface))
                throw new Error(
                    'getTokenParsers() must return an record of TokenParser/TokenParserInterface.'
                );

            this._parsers[tokenParsers[parserName].getTag()] =
                tokenParsers[parserName];
        });

        // node visitors
        extension.getNodeVisitors().forEach(visitor => {
            this._visitors.push(visitor);
        });

        // operators
        const operators = extension.getOperators();
        if (operators.length !== 2)
            throw new TypeError(
                `"${extension.constructor.name}.getOperators()" must return an array of 2 elements, got ${operators.length}.`
            );

        this._unaryOperators = { ...this._unaryOperators, ...operators[0] };
        this._binaryOperators = { ...this._binaryOperators, ...operators[1] };
    }
}
