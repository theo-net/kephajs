/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { objectKeys } from '@kephajs/core/Helpers/Helpers';
import { Environment } from './Environment';
import { RuntimeError } from './Error/RuntimeError';
import { TwigError } from './Error/TwigError';
import { ExtensionInterface } from './Extension/ExtensionInterface';
import { LoaderError } from './Error/LoaderError';
import { Source } from './Source';
import { TemplateWrapper } from './TemplateWrapper';

export type BlockType = [Template, string];

export class Template {
    [key: string]: unknown;

    public static readonly ANY_CALL = 'any';

    public static readonly ARRAY_CALL = 'array';

    public static readonly METHOD_CALL = 'method';

    protected _parent: Template | TemplateWrapper | null = null;

    protected _parents: Record<string, Template | TemplateWrapper> = {};

    protected _blocks: Record<string, BlockType> = {};

    protected _traits: Record<string, BlockType> = {};

    protected _extensions: Record<string, ExtensionInterface>;

    protected _output: string | undefined = undefined;

    protected _outputStack: (string | undefined)[] = [];

    constructor(
        protected _env: Environment,
        protected _prototypes: Record<string, unknown> = {}
    ) {
        this._extensions = _env.getExtensions();
    }

    init() {
        return this;
    }

    print(output: string) {
        if (this._output === undefined) this._output = '';
        if (output !== undefined && output !== null) this._output += output;
    }

    stash() {
        this._outputStack.push(this._output);
        this._output = undefined;
    }

    pop() {
        const actual = this._output;
        this._output = this._outputStack.pop();
        return actual ?? '';
    }

    getOutput() {
        return this._output ?? '';
    }

    getTemplateName() {
        return '';
    }

    getDebugInfo() {}

    getSourceContext() {
        return new Source('', '');
    }

    getParent(context: Record<string, unknown>) {
        if (this._parent !== null) return this._parent;

        try {
            const parent = this._doGetParent(context) as unknown as
                | false
                | Template
                | TemplateWrapper
                | string;

            if (parent === false) return false;

            if (parent instanceof Template || parent instanceof TemplateWrapper)
                return (this._parents[parent.getSourceContext().name] = parent);

            if (this._parents[parent] === undefined)
                this._parents[parent] = this._loadTemplate(parent);

            return this._parents[parent];
        } catch (error) {
            if (error instanceof LoaderError) {
                error.setSourceContext(null);
                error.guess();
            }

            throw error;
        }
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    _doGetParent(_context: Record<string, unknown>) {
        return false;
    }

    isTraitable() {
        return true;
    }

    renderParentBlock(
        blockName: string,
        context: Record<string, unknown>,
        blocks: Record<string, BlockType> = {}
    ) {
        let parent: false | Template | TemplateWrapper;
        if (this._traits[blockName] !== undefined)
            return this._traits[blockName][0].renderBlock(
                blockName,
                context,
                blocks,
                false
            );
        else if ((parent = this.getParent(context)) !== false)
            return parent.renderBlock(blockName, context, blocks, false);
        else
            throw new RuntimeError(
                `The template has no parent and no traits defining the "${blockName}" block.`,
                -1,
                this.getSourceContext()
            );
    }

    renderBlock(
        blockName: string,
        context: Record<string, unknown>,
        blocks: Record<string, BlockType> = {},
        useBlocks = true,
        templateContext: null | Template = null
    ): string {
        let template: null | Template;
        let block: null | string;
        if (useBlocks && blocks[blockName] !== undefined) {
            template = blocks[blockName][0];
            block = blocks[blockName][1];
        } else if (this._blocks[blockName] !== undefined) {
            template = this._blocks[blockName][0];
            block = this._blocks[blockName][1];
        } else {
            template = null;
            block = null;
        }

        // avoid RCEs when sandbox is enabled
        if (
            template !== null &&
            block !== null &&
            !(template instanceof Template)
        )
            throw new Error('A block must be a method on a Template instance.');

        let parent: false | Template | TemplateWrapper = false;
        if (template !== null && block !== null) {
            try {
                template.stash();
                (
                    template[block] as (
                        context: Record<string, unknown>,
                        blocks: Record<string, BlockType>
                    ) => string
                ).call(template, context, blocks);
                return template.pop();
            } catch (error) {
                let newError: TwigError;
                if (error instanceof TwigError) {
                    if (!error.getSourceContext())
                        error.setSourceContext(template.getSourceContext());

                    // this is mostly useful for LoaderError exceptions
                    // see LoaderError
                    if (error.getTemplateLine() === -1) error.guess();
                    newError = error;
                } else {
                    newError = new RuntimeError(
                        `An exception has been thrown during the rendering of a template ("${
                            (error as Error).message
                        }").`,
                        -1,
                        template.getSourceContext()
                    );
                    newError.guess();
                }

                throw newError;
            }
        } else if ((parent = this.getParent(context)) !== false) {
            return parent.renderBlock(
                blockName,
                context,
                { ...this._blocks, ...blocks },
                false,
                templateContext ?? this
            );
        } else if (blocks[blockName] !== undefined)
            throw new RuntimeError(
                `Block "${blockName}" should not call parent() as the block does not exist in the parent template "${this.getTemplateName()}".`,
                -1,
                blocks[blockName][0].getSourceContext()
            );
        else
            throw new RuntimeError(
                `Block "${blockName}" on template "${this.getTemplateName()}" does not exist.`,
                -1,
                (templateContext ?? this).getSourceContext()
            );
    }

    hasBlock(
        blockName: string,
        context: Record<string, unknown> = {},
        blocks: Record<string, BlockType> = {}
    ): boolean {
        if (blocks[blockName] !== undefined)
            return blocks[blockName][0] instanceof Template;

        if (this._blocks[blockName] !== undefined) return true;

        const parent: false | Template | TemplateWrapper =
            this.getParent(context);
        // changez to typeof...
        if (parent !== false) return parent.hasBlock(blockName, context);

        return false;
    }

    getBlockNames(
        context: Record<string, unknown> = {},
        blocks: Record<string, BlockType> = {}
    ) {
        let blockNames = [...objectKeys(blocks), ...objectKeys(this._blocks)];

        const parent = this.getParent(context);
        if (parent !== false)
            blockNames = [...blockNames, ...parent.getBlockNames(context)];

        return blockNames.filter((value, index, self) => {
            return self.indexOf(value) === index;
        });
    }

    protected _loadTemplate(
        template: [] | Template | TemplateWrapper | string,
        templateName = null,
        line = null,
        index = null
    ) {
        try {
            if (Array.isArray(template))
                return this._env.resolveTemplate(template);

            if (
                template instanceof Template ||
                template instanceof TemplateWrapper
            )
                return template;

            let className = '';
            if (template === this.getTemplateName()) {
                className = this.constructor.name;
                const pos = className.indexOf('___');
                if (pos > -1) className = className.substring(0, pos);
            } else className = this._env.getTemplateClass(template);

            return this._env.loadTemplate(className, template, index);
        } catch (error) {
            if (error instanceof TwigError) {
                if (!error.getSourceContext())
                    error.setSourceContext(
                        templateName
                            ? new Source('', templateName)
                            : this.getSourceContext()
                    );

                if (error.getTemplateLine() > 0) throw error;

                if (!line) error.guess();
                else error.setTemplateLine(line);
            }

            throw error;
        }
    }

    unwrap() {
        return this;
    }

    getBlocks() {
        // Each time we return a new object: if in the constructor on trait is deleted,
        // the original block will be not deleted
        return { ...this._blocks };
    }

    render(
        context: Record<string, unknown>,
        blocks: Record<string, BlockType> = {}
    ) {
        return this._renderWithErrorHandling(this._env.mergeGlobals(context), {
            ...this._blocks,
            ...blocks,
        });
    }

    protected _renderWithErrorHandling(
        context: Record<string, unknown>,
        blocks: Record<string, BlockType> = {}
    ) {
        try {
            this.stash();
            this._doRender(context, blocks);
            return this.pop();
        } catch (error) {
            let newError: TwigError;
            if (error instanceof TwigError) {
                if (!error.getSourceContext())
                    error.setSourceContext(this.getSourceContext());

                // this is mostly useful for LoaderError exceptions
                // LoaderError
                if (error.getTemplateLine() === -1) error.guess();
                newError = error;
            } else {
                newError = new RuntimeError(
                    `An exception has been thrown during the rendering of a template ("${
                        (error as Error).message
                    }").`,
                    -1,
                    this.getSourceContext()
                );
                newError.guess();
            }

            if (this._env.isDebug()) {
                return this.pop() + (error as Error).message;
            }
            throw newError;
        }
    }

    protected _doRender(
        _context: Record<string, unknown>,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        _blocks: Record<string, BlockType> = {}
    ) {
        return '';
    }
}
