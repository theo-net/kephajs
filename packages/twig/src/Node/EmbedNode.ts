/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { AbstractExpression } from './Expression/AbstractExpression';
import { ConstantExpression } from './Expression/ConstantExpression';
import { IncludeNode } from './IncludeNode';

export class EmbedNode extends IncludeNode {
    // we don't inject the module to avoid node visitors to traverse it twice (as it will be already visited in the main module)
    constructor(
        nodeName: string,
        index: number,
        variables: AbstractExpression | null,
        only: boolean,
        ignoreMissing: boolean,
        lineno: number,
        tag: null | string = null
    ) {
        super(
            new ConstantExpression('not_used', lineno),
            variables,
            only,
            ignoreMissing,
            lineno,
            tag
        );

        this.setAttribute('name', nodeName);
        this.setAttribute('index', index);
    }

    protected _addGetTemplate(compiler: Compiler) {
        compiler
            .write('this._loadTemplate(')
            .string(this.getAttribute('name') as string)
            .raw(', ')
            .repr(this.getTemplateName())
            .raw(', ')
            .repr(this.getTemplateLine())
            .raw(', ')
            .string(String(this.getAttribute('index') as number))
            .raw(')');
    }
}
