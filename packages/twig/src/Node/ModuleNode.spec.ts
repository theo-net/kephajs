/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../Compiler';
import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';
import { Source } from '../Source';
import { AssignNameExpression } from './Expression/AssignNameExpression';
import { ConditionalExpression } from './Expression/ConditionalExpression';
import { ConstantExpression } from './Expression/ConstantExpression';
import { ImportNode } from './ImportNode';
import { ModuleNode } from './ModuleNode';
import { SetNode } from './SetNode';
import { TextNode } from './TextNode';
import { TwigNode } from './TwigNode';

describe('@kephajs/twig/Node/ModuleNode', () => {
    it('Should compile the constructor', () => {
        const body = new TextNode('foo', 1);
        const parent = new ConstantExpression('layout.twig', 1);
        const blocks = new TwigNode();
        const macros = new TwigNode();
        const traits = new TwigNode();
        const source = new Source('{{ foo }}', 'foo.twig');
        const node = new ModuleNode(
            'export',
            body,
            parent,
            blocks,
            macros,
            traits,
            [new TwigNode([])],
            source
        );

        expect(node.getNode('body')).to.be.equal(body);
        expect(node.getNode('blocks')).to.be.equal(blocks);
        expect(node.getNode('macros')).to.be.equal(macros);
        expect(node.getNode('parent')).to.be.equal(parent);
        expect(node.getTemplateName()).to.be.equal(source.name);
    });

    function getTemplateClass(compiler: Compiler, module: ModuleNode) {
        return compiler
            .getEnvironment()
            .getTemplateClass(
                (module.getSourceContext() as Source).name,
                module.getAttribute('index') as number
            );
    }

    it('Should compile the node', () => {
        let env = new Environment(new RecordLoader({ 'foo.twig': '' }));
        let compiler = new Compiler(env);

        let body = new TextNode('foo', 1);
        let extendsNode: null | TwigNode = null;
        const blocks = new TwigNode();
        const macros = new TwigNode();
        const traits = new TwigNode();
        const source = new Source('{{ foo }}', 'foo.twig');

        let node = new ModuleNode(
            'eval',
            body,
            extendsNode,
            blocks,
            macros,
            traits,
            [new TwigNode([])],
            source
        );
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            `/* foo.twig */
(class ${getTemplateClass(compiler, node)} {
    constructor(env, prototypes) {
        this._env = env;
        this._prototypes = prototypes;
        this._extensions = env.getExtensions();
        let macros = this._macros = {};
        this._internalVars = {};
        this._outputStack = [];

        this._source = this.getSourceContext();

        this._parent = null;
        this._parents = {};
        this._parent = false;

        this._traits = {};
        this._blocks = {
        };

        return this;
    }

    _doRender(context, blocks = {}) {
        let macros = { ...this._macros };
        // line 1
        this.print(\`foo\`);
        return this._output;
    }

    getTemplateName() {
        return \`foo.twig\`;
    }

    getDebugInfo() {
        return  {    29: 1,};
    }

    getSourceContext() {
        return new this._prototypes.Source(\`\`, \`foo.twig\`, \`\`);
    }
})`
        );
        node = new ModuleNode(
            'export',
            body,
            extendsNode,
            blocks,
            macros,
            traits,
            [new TwigNode([])],
            source
        );
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            `/* foo.twig */
module.exports.default = class ${getTemplateClass(compiler, node)} {
    constructor(env, prototypes) {
        this._env = env;
        this._prototypes = prototypes;
        this._extensions = env.getExtensions();
        let macros = this._macros = {};
        this._internalVars = {};
        this._outputStack = [];

        this._source = this.getSourceContext();

        this._parent = null;
        this._parents = {};
        this._parent = false;

        this._traits = {};
        this._blocks = {
        };

        return this;
    }

    _doRender(context, blocks = {}) {
        let macros = { ...this._macros };
        // line 1
        this.print(\`foo\`);
        return this._output;
    }

    getTemplateName() {
        return \`foo.twig\`;
    }

    getDebugInfo() {
        return  {    29: 1,};
    }

    getSourceContext() {
        return new this._prototypes.Source(\`\`, \`foo.twig\`, \`\`);
    }
}`
        );

        const importNode = new ImportNode(
            new ConstantExpression('foo.twig', 1),
            new AssignNameExpression('macro', 1),
            2
        );

        body = new TwigNode([importNode]);
        extendsNode = new ConstantExpression('layout.twig', 1);

        node = new ModuleNode(
            'eval',
            body,
            extendsNode,
            blocks,
            macros,
            traits,
            [new TwigNode([])],
            source
        );
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            `/* foo.twig */
(class ${getTemplateClass(compiler, node)} {
    constructor(env, prototypes) {
        this._env = env;
        this._prototypes = prototypes;
        this._extensions = env.getExtensions();
        let macros = this._macros = {};
        this._internalVars = {};
        this._outputStack = [];

        this._source = this.getSourceContext();

        this._parent = null;
        this._parents = {};
        this._traits = {};
        this._blocks = {
        };

        return this;
    }

    _doGetParent(context) {
        // line 1
        return \`layout.twig\`;
    }

    _doRender(context, blocks = {}) {
        let macros = { ...this._macros };
        // line 2
        macros[\`macro\`] = this._macros[\`macro\`] = this._loadTemplate(\`foo.twig\`, \`foo.twig\`, 2).unwrap();
        // line 1
        this._parent = this._loadTemplate(\`layout.twig\`, \`foo.twig\`, 1);
        this.print(this._parent.render(context, { ...this._blocks, ...blocks }));
        return this._output;
    }

    getTemplateName() {
        return \`foo.twig\`;
    }

    isTraitable() {
        return false;
    }

    getDebugInfo() {
        return  {    26: 1,    32: 2,    34: 1,};
    }

    getSourceContext() {
        return new this._prototypes.Source(\`\`, \`foo.twig\`, \`\`);
    }
})`
        );

        const set = new SetNode(
            false,
            new TwigNode([new AssignNameExpression('foo', 4)]),
            new TwigNode([new ConstantExpression('foo', 4)]),
            4
        );
        body = new TwigNode([set]);
        extendsNode = new ConditionalExpression(
            new ConstantExpression(true, 2),
            new ConstantExpression('foo', 2),
            new ConstantExpression('foo', 2),
            2
        );

        env = new Environment(new RecordLoader({ 'foo.twig': '' }), {
            debug: true,
        });
        compiler = new Compiler(env);
        node = new ModuleNode(
            'eval',
            body,
            extendsNode,
            blocks,
            macros,
            traits,
            [new TwigNode([])],
            source
        );
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            `/* foo.twig */
(class ${getTemplateClass(compiler, node)} {
    constructor(env, prototypes) {
        this._env = env;
        this._prototypes = prototypes;
        this._extensions = env.getExtensions();
        let macros = this._macros = {};
        this._internalVars = {};
        this._outputStack = [];

        this._source = this.getSourceContext();

        this._parent = null;
        this._parents = {};
        this._traits = {};
        this._blocks = {
        };

        return this;
    }

    _doGetParent(context) {
        // line 2
        return this._loadTemplate(((true) ? (\`foo\`) : (\`foo\`)), \`foo.twig\`, 2);
    }

    _doRender(context, blocks = {}) {
        let macros = { ...this._macros };
        // line 4
        let __internal_compile_0 = '';
        context[\`foo\`] = \`foo\`;
        // line 2
        this.print(this.getParent(context).render(context, { ...this._blocks, ...blocks }));
        return this._output;
    }

    getTemplateName() {
        return \`foo.twig\`;
    }

    isTraitable() {
        return false;
    }

    getDebugInfo() {
        return  {    26: 2,    32: 4,    35: 2,};
    }

    getSourceContext() {
        return new this._prototypes.Source(\`{{ foo }}\`, \`foo.twig\`, \`\`);
    }
})`
        );
    });
});
