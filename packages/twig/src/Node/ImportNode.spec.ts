/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../Compiler';
import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';
import { AssignNameExpression } from './Expression/AssignNameExpression';
import { ConstantExpression } from './Expression/ConstantExpression';
import { ImportNode } from './ImportNode';

describe('@kephajs/twig/Node/ImportNode', () => {
    it('Should construct', () => {
        const macro = new ConstantExpression('foo.twig', 1);
        const variable = new AssignNameExpression('macro', 1);
        const node = new ImportNode(macro, variable, 1);

        expect(node.getNode('expr')).to.be.equal(macro);
        expect(node.getNode('var')).to.be.equal(variable);
    });

    it('Should compile the node', () => {
        const macro = new ConstantExpression('foo.twig', 1);
        const variable = new AssignNameExpression('macro', 1);
        const node = new ImportNode(macro, variable, 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);

        expect(compiler.getSource().trim()).to.be.equal(
            '// line 1\nmacros[`macro`] = this._macros[`macro`] = this._loadTemplate(`foo.twig`, null, 1).unwrap();'
        );
    });
});
