/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { Token } from '../Token';
import { TwigNode } from './TwigNode';

export class WithNode extends TwigNode {
    constructor(
        body: TwigNode,
        variables: TwigNode | null,
        only: boolean | Token,
        lineno: number,
        tag: null | string = null
    ) {
        const nodes: Record<string, TwigNode> = { body: body };
        if (variables !== null) nodes.variables = variables;

        super(nodes, { only }, lineno, tag);
    }

    compile(compiler: Compiler) {
        compiler.addDebugInfo(this);

        const parentContextName = compiler.getVarName();

        compiler.write('const ' + parentContextName + ' = { ...context };\n');

        if (this.hasNode('variables')) {
            const node = this.getNode('variables');
            const varsName = compiler.getVarName();
            compiler
                .write('let ' + varsName + ' = ')
                .subcompile(node)
                .raw(';\n')
                .write(
                    'if (' +
                        varsName +
                        ' === null || Array.isArray(' +
                        varsName +
                        ') || typeof ' +
                        varsName +
                        " !== 'object') {\n"
                )
                .indent()
                .write(
                    'throw new this._prototypes.RuntimeError(\'Variables passed to the "with" tag must be a hash.\', '
                )
                .repr(node.getTemplateLine())
                .raw(', this.getSourceContext());\n')
                .outdent()
                .write('}\n');

            if (this.getAttribute('only')) compiler.write('context = {};\n');

            compiler.write(
                'context = this._env.mergeGlobals({ ...context, ...' +
                    varsName +
                    '});\n'
            );
        }

        compiler
            .subcompile(this.getNode('body'))
            .write('context = ' + parentContextName + ';\n');
    }
}
