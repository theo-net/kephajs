/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { NodeOutputInterface } from './NodeOutputInterface';

export class TextNode extends NodeOutputInterface {
    constructor(data: string, lineno: number) {
        super({}, { data }, lineno);
    }

    compile(compiler: Compiler) {
        compiler
            .addDebugInfo(this)
            .write('this.print(')
            .string(this.getAttribute('data') as string)
            .raw(');\n');
    }
}
