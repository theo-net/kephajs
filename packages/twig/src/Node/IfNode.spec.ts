/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../Compiler';
import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';
import { ConstantExpression } from './Expression/ConstantExpression';
import { NameExpression } from './Expression/NameExpression';
import { IfNode } from './IfNode';
import { PrintNode } from './PrintNode';
import { TwigNode } from './TwigNode';

describe('@kephajs/twig/Node/IfNode', () => {
    it('Should construct', () => {
        const t = new TwigNode(
            [
                new ConstantExpression(true, 1),
                new PrintNode(new NameExpression('foo', 1), 1),
            ],
            {},
            1
        );
        let elseNode: TwigNode | null = null;
        let node = new IfNode(t, elseNode, 1);

        expect(node.getNode('tests')).to.be.equal(t);
        expect(node.hasNode('else')).to.be.equal(false);

        elseNode = new PrintNode(new NameExpression('bar', 1), 1);
        node = new IfNode(t, elseNode, 1);
        expect(node.getNode('else')).to.be.equal(elseNode);
    });

    it('Should compile the node', () => {
        const compiler = new Compiler(new Environment(new RecordLoader()));

        let t = new TwigNode(
            [
                new ConstantExpression(true, 1),
                new PrintNode(new NameExpression('foo', 1), 1),
            ],
            {},
            1
        );
        let elseNode: TwigNode | null = null;
        let node = new IfNode(t, elseNode, 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            '// line 1\nif (true) {\n    this.print((context[`foo`] ?? undefined));\n}'
        );

        t = new TwigNode(
            [
                new ConstantExpression(true, 1),
                new PrintNode(new NameExpression('foo', 1), 1),
                new ConstantExpression(false, 1),
                new PrintNode(new NameExpression('bar', 1), 1),
            ],
            {},
            1
        );
        elseNode = null;
        node = new IfNode(t, elseNode, 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            '// line 1\nif (true) {\n    this.print((context[`foo`] ?? undefined));\n} else if (false) {\n    this.print((context[`bar`] ?? undefined));\n}'
        );

        t = new TwigNode(
            [
                new ConstantExpression(true, 1),
                new PrintNode(new NameExpression('foo', 1), 1),
            ],
            {},
            1
        );
        elseNode = new PrintNode(new NameExpression('bar', 1), 1);
        node = new IfNode(t, elseNode, 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            '// line 1\nif (true) {\n    this.print((context[`foo`] ?? undefined));\n} else {\n    this.print((context[`bar`] ?? undefined));\n}'
        );
    });
});
