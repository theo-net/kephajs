/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { TwigNode } from './TwigNode';

export class ForLoopNode extends TwigNode {
    constructor(lineno: number, tag: null | string = null) {
        super([], { withLoop: false, ifexpr: false, else: false }, lineno, tag);
    }

    compile(compiler: Compiler) {
        if (this.getAttribute('else'))
            compiler.write("context['_iterated'] = true;\n");

        if (this.getAttribute('withLoop'))
            compiler
                .write('context.loop.index0++;\n')
                .write('context.loop.index++;\n')
                .write('context.loop.first = false;\n')
                .write('if (context.loop.length !== undefined) {\n')
                .indent()
                .write('context.loop.revindex0--;\n')
                .write('context.loop.revindex--;\n')
                .write('context.loop.last = 0 === context.loop.revindex0;\n')
                .outdent()
                .write('}\n');
    }
}
