/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../Compiler';
import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';
import { AutoEscapeNode } from './AutoEscapeNode';
import { TextNode } from './TextNode';
import { TwigNode } from './TwigNode';

describe('@kephajs/twig/Node/AutoEscapeNode', () => {
    it('Should construct', () => {
        const body = new TwigNode([new TextNode('foo', 1)]);
        const node = new AutoEscapeNode(true, body, 1);

        expect(node.getNode('body')).to.be.equal(body);
        expect(node.getAttribute('value')).to.be.equal(true);
    });

    it('Should compile the node', () => {
        const body = new TwigNode([new TextNode('foo', 1)]);
        const node = new AutoEscapeNode(true, body, 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);

        expect(compiler.getSource()).to.be.equal(
            '// line 1\nthis.print(`foo`);\n'
        );
    });
});
