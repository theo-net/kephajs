/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { AbstractExpression } from './Expression/AbstractExpression';
import { NodeOutputInterface } from './NodeOutputInterface';

export class IncludeNode extends NodeOutputInterface {
    constructor(
        expr: AbstractExpression,
        variables: AbstractExpression | null,
        only: boolean,
        ignoreMissing: boolean,
        lineno: number,
        tag: null | string = null
    ) {
        const nodes: Record<string, AbstractExpression> = { expr };
        if (variables !== null) nodes.variables = variables;

        super(nodes, { only, ignoreMissing }, lineno, tag);
    }

    compile(compiler: Compiler) {
        compiler.addDebugInfo(this);

        if (this.getAttribute('ignoreMissing')) {
            const template = compiler.getVarName();

            compiler
                .write('let ' + template + ' = null;\n')
                .write('try {\n')
                .indent()
                .write(template + ' = ');

            this._addGetTemplate(compiler);

            compiler
                .raw(';\n')
                .outdent()
                .write('} catch (error) {\n')
                .indent()
                .write('// ignore missing template\n')
                .outdent()
                .write('}\n')
                .write(`if (${template}) {\n`)
                .indent()
                .write('this.print(' + template + '.render(');
            this._addTemplateArguments(compiler);
            compiler.raw('));\n').outdent().write('}\n');
        } else {
            compiler.write('this.print(');
            this._addGetTemplate(compiler);
            compiler.raw('.render(');
            this._addTemplateArguments(compiler);
            compiler.raw('));\n');
        }
    }

    protected _addGetTemplate(compiler: Compiler) {
        compiler
            .raw('this._loadTemplate(')
            .subcompile(this.getNode('expr'))
            .raw(', ')
            .repr(this.getTemplateName())
            .raw(', ')
            .repr(this.getTemplateLine())
            .raw(')');
    }

    protected _addTemplateArguments(compiler: Compiler) {
        if (!this.hasNode('variables'))
            compiler.raw(
                false === this.getAttribute('only') ? 'context' : '{}'
            );
        else if (false === this.getAttribute('only')) {
            compiler
                .raw("this._env.getFilter('merge').getCallable()(context, ")
                .subcompile(this.getNode('variables'))
                .raw(')');
        } else compiler.subcompile(this.getNode('variables'));
    }
}
