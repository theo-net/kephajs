/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { ConstantExpression } from './Expression/ConstantExpression';
import { TempNameExpression } from './Expression/TempNameExpression';
import { NodeCaptureInterface } from './NodeCaptureInterface';
import { TextNode } from './TextNode';
import { TwigNode } from './TwigNode';

export class SetNode extends NodeCaptureInterface {
    constructor(
        capture: boolean,
        names: TwigNode,
        values: TwigNode,
        lineno: number,
        tag: null | string = null
    ) {
        super({ names, values }, { capture, safe: false }, lineno, tag);

        /*
         * Optimizes the node when capture is used for a large block of text.
         *
         * {% set foo %}foo{% endset %} is compiled to context['foo'] = new Markup("foo");
         */
        if (this.getAttribute('capture')) {
            this.setAttribute('safe', true);

            values = this.getNode('values');
            if (values instanceof TextNode) {
                this.setNode(
                    'values',
                    new ConstantExpression(
                        values.getAttribute('data'),
                        values.getTemplateLine()
                    )
                );
                this.setAttribute('capture', false);
            }
        }
    }

    compile(compiler: Compiler) {
        compiler.addDebugInfo(this);

        const tmpVar = compiler.getVarName();
        compiler.write('let ' + tmpVar + " = '';\n");

        if (this.getNode('names').length > 1) {
            const names: TwigNode[] = [],
                values: TwigNode[] = [];
            for (const node of this.getNode('names')) names.push(node);
            for (const value of this.getNode('values')) values.push(value);

            names.forEach((node, index) => {
                compiler
                    .subcompile(node)
                    .raw(' = ')
                    .subcompile(values[index])
                    .raw(';\n');
            });
        } else {
            if (this.getAttribute('capture')) {
                compiler.write('this.stash();\n');
                compiler.subcompile(this.getNode('values'));
                compiler.write(tmpVar + ' = this.pop();\n');
            }

            if (this.getNode('names') instanceof TempNameExpression)
                compiler.write('let ');
            else compiler.write('');
            compiler.subcompile(this.getNode('names'), true);

            if (this.getAttribute('capture'))
                compiler.raw(
                    ' = ' +
                        tmpVar +
                        " === '' ? '' : new this._prototypes.Markup(" +
                        tmpVar +
                        ')'
                );
            if (!this.getAttribute('capture')) {
                compiler.raw(' = ');

                if (this.getNode('names').length > 1) {
                    compiler.write('[');
                    let first = true;
                    const values = this.getNode('values');
                    for (const value of values) {
                        if (first) first = false;
                        else compiler.raw(', ');
                        compiler.subcompile(value);
                    }
                    compiler.raw(']');
                } else {
                    if (this.getAttribute('safe')) {
                        compiler
                            .raw('((' + tmpVar + ' = ')
                            .subcompile(this.getNode('values'))
                            .raw(
                                ") === '') ? '' : new this._prototypes.Markup(" +
                                    tmpVar +
                                    ')'
                            );
                    } else {
                        compiler.subcompile(this.getNode('values'));
                    }
                }
            }
            compiler.raw(';\n');
        }
    }
}
