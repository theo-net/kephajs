/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { ConstantExpression } from './Expression/ConstantExpression';
import { IncludeNode } from './IncludeNode';
import { RecordExpression } from './Expression/RecordExpression';
import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';
import { ConditionalExpression } from './Expression/ConditionalExpression';
import { Compiler } from '../Compiler';

describe('@kephajs/twig/Node/IncludeNode', () => {
    it('Test constructor', () => {
        const expr = new ConstantExpression('foo.twig', 1);
        let node = new IncludeNode(expr, null, false, false, 1);

        expect(node.hasNode('variables')).to.be.equal(false);
        expect(node.getNode('expr')).to.be.equal(expr);
        expect(node.getAttribute('only')).to.be.equal(false);

        const vars = new RecordExpression(
            [new ConstantExpression('foo', 1), new ConstantExpression(true, 1)],
            1
        );
        node = new IncludeNode(expr, vars, true, false, 1);
        expect(node.getNode('variables')).to.be.equal(vars);
        expect(node.getAttribute('only')).to.be.equal(true);
    });

    it('Should compile the node', () => {
        const env = new Environment(new RecordLoader());
        const compiler = new Compiler(env);

        let expr = new ConstantExpression('foo.twig', 1);
        let node = new IncludeNode(expr, null, false, false, 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            '// line 1\nthis.print(this._loadTemplate(`foo.twig`, null, 1).render(context));'
        );

        expr = new ConditionalExpression(
            new ConstantExpression(true, 1),
            new ConstantExpression('foo', 1),
            new ConstantExpression('foo', 1),
            0
        );
        node = new IncludeNode(expr, null, false, false, 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            '// line 1\nthis.print(this._loadTemplate(((true) ? (`foo`) : (`foo`)), null, 1).render(context));'
        );

        expr = new ConstantExpression('foo.twig', 1);
        const vars = new RecordExpression(
            [new ConstantExpression('foo', 1), new ConstantExpression(true, 1)],
            1
        );
        node = new IncludeNode(expr, vars, false, false, 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            "// line 1\nthis.print(this._loadTemplate(`foo.twig`, null, 1).render(this._env.getFilter('merge').getCallable()(context, {['foo']: true})));"
        );

        node = new IncludeNode(expr, vars, true, false, 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            "// line 1\nthis.print(this._loadTemplate(`foo.twig`, null, 1).render({['foo']: true}));"
        );

        node = new IncludeNode(expr, vars, true, true, 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(`// line 1
let __internal_compile_0 = null;
try {
    __internal_compile_0 = this._loadTemplate(\`foo.twig\`, null, 1);
} catch (error) {
    // ignore missing template
}
if (__internal_compile_0) {
    this.print(__internal_compile_0.render({['foo']: true}));
}`);
    });
});
