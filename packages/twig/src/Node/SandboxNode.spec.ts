/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../Compiler';
import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';
import { SandboxNode } from './SandboxNode';
import { TextNode } from './TextNode';

describe('@kephajs/twig/Node/SandboxNode', () => {
    it('Should construct', () => {
        const body = new TextNode('foo', 1);
        const node = new SandboxNode(body, 1);

        expect(node.getNode('body')).to.be.equal(body);
    });

    it('Should compile the node', () => {
        const body = new TextNode('foo', 1);
        const node = new SandboxNode(body, 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);

        expect(compiler.getSource().trim().split('\n')).to.be.deep.equal([
            '// line 1',
            'let alreadySandboxed = false;',
            'if (!alreadySandboxed = this.sandbox.isSandboxed())',
            '    this.sandbox.enableSandbox();',
            'try {',
            '    this.print(`foo`);',
            '} finally {',
            '    if (!alreadySandboxed)',
            '        this.sandbox.disableSandbox();',
            '}',
        ]);
    });
});
