/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../Compiler';
import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';
import { ConstantExpression } from './Expression/ConstantExpression';
import { NameExpression } from './Expression/NameExpression';
import { MacroNode } from './MacroNode';
import { TextNode } from './TextNode';
import { TwigNode } from './TwigNode';

describe('@kephajs/twig/Node/MacroNode', () => {
    it('Should construct', () => {
        const body = new TextNode('foo', 1);
        const args = new TwigNode([new NameExpression('foo', 1)], {}, 1);
        const node = new MacroNode('foo', body, args, 1);

        expect(node.getNode('body')).to.be.equal(body);
        expect(node.getNode('arguments')).to.be.equal(args);
        expect(node.getAttribute('name')).to.be.equal('foo');
    });

    it('Should compile the node', () => {
        const body = new TextNode('foo', 1);
        const args = new TwigNode(
            {
                foo: new ConstantExpression(null, 1),
                bar: new ConstantExpression('Foo', 1),
            },
            {},
            1
        );
        const node = new MacroNode('foo', body, args, 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);

        expect(compiler.getSource().trim().split('\n')).to.be.deep.equal([
            '// line 1',
            'macro_foo(__foo__ = null, __bar__ = `Foo`, ...__varargs__) {',
            '    let macros = { ...this._macros };',
            '    let context = this._env.mergeGlobals({',
            '        "foo": __foo__,',
            '        "bar": __bar__,',
            '        "varargs": __varargs__,',
            '    });',
            '',
            '    let blocks = [];',
            '',
            '    this.stash();',
            '    try {',
            '        this.print(`foo`);',
            '',
            '        let tmp = this.getOutput();',
            "        return tmp === '' ? '' : new this._prototypes.Markup(tmp);",
            '    } finally {',
            '        this.pop();',
            '    }',
            '}',
        ]);
    });
});
