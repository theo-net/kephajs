/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../Compiler';
import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';
import { AssignNameExpression } from './Expression/AssignNameExpression';
import { ConstantExpression } from './Expression/ConstantExpression';
import { NameExpression } from './Expression/NameExpression';
import { PrintNode } from './PrintNode';
import { SetNode } from './SetNode';
import { TextNode } from './TextNode';
import { TwigNode } from './TwigNode';

describe('@kephajs/twig/Node/SetNode', () => {
    it('Should compile the constructor', () => {
        const names = new TwigNode([new AssignNameExpression('foo', 1)], {}, 1);
        const values = new TwigNode([new ConstantExpression('foo', 1)], {}, 1);
        const node = new SetNode(false, names, values, 1);

        expect(node.getNode('names')).to.be.equal(names);
        expect(node.getNode('values')).to.be.equal(values);
        expect(node.getAttribute('capture')).to.be.equal(false);
    });

    it('Should compile the node', () => {
        const compiler = new Compiler(new Environment(new RecordLoader()));

        let names = new TwigNode([new AssignNameExpression('foo', 1)], {}, 1);
        let values = new TwigNode([new ConstantExpression('foo', 1)], {}, 1);
        let node = new SetNode(false, names, values, 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            "// line 1\nlet __internal_compile_0 = '';\ncontext[`foo`] = `foo`;"
        );

        names = new TwigNode([new AssignNameExpression('foo', 1)], {}, 1);
        values = new TwigNode(
            [new PrintNode(new ConstantExpression('foo', 1), 1)],
            {},
            1
        );
        node = new SetNode(true, names, values, 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            "// line 1\nlet __internal_compile_0 = '';\nthis.stash();\nthis.print(`foo`);\n__internal_compile_0 = this.pop();\ncontext[`foo`] = __internal_compile_0 === '' ? '' : new this._prototypes.Markup(__internal_compile_0);"
        );

        names = new TwigNode([new AssignNameExpression('foo', 1)], {}, 1);
        values = new TextNode('foo', 1);
        node = new SetNode(true, names, values, 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            "// line 1\nlet __internal_compile_0 = '';\ncontext[`foo`] = ((__internal_compile_0 = `foo`) === '') ? '' : new this._prototypes.Markup(__internal_compile_0);"
        );

        names = new TwigNode(
            [
                new AssignNameExpression('foo', 1),
                new AssignNameExpression('bar', 1),
            ],
            {},
            1
        );
        values = new TwigNode(
            [new ConstantExpression('foo', 1), new NameExpression('bar', 1)],
            {},
            1
        );
        node = new SetNode(false, names, values, 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            "// line 1\nlet __internal_compile_0 = '';\ncontext[`foo`] = `foo`;\ncontext[`bar`] = (context[`bar`] ?? undefined);"
        );
    });
});
