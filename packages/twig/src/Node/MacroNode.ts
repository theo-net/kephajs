/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { TwigNode } from './TwigNode';
import { SyntaxError } from '../Error/SyntaxError';
import { Compiler } from '../Compiler';
import { objectKeys } from '@kephajs/core/Helpers/Helpers';
import { BlockNode } from './BlockNode';

export class MacroNode extends TwigNode {
    static VARARGS_NAME = 'varargs';

    constructor(
        macroName: string,
        body: TwigNode,
        args: TwigNode,
        lineno: number,
        tag: null | string = null
    ) {
        for (const argName in args.getNodes()) {
            if (argName === MacroNode.VARARGS_NAME)
                throw new SyntaxError(
                    `The argument "${MacroNode.VARARGS_NAME}" in macro "${macroName}" cannot be defined because the variable "${MacroNode.VARARGS_NAME}" is reserved for arbitrary arguments.`,
                    args.getNodes()[argName].getTemplateLine(),
                    args.getNodes()[argName].getSourceContext()
                );
        }

        super({ body, arguments: args }, { name: macroName }, lineno, tag);
    }

    compile(compiler: Compiler) {
        compiler
            .addDebugInfo(this)
            .write(
                'macro_' +
                    BlockNode.escapeBlockName(
                        this.getAttribute('name') as string
                    ) +
                    '('
            );

        const argsNodes = this.getNode('arguments').getNodes();
        const count = this.getNode('arguments').length;
        let pos = 0;
        objectKeys(argsNodes).forEach(argName => {
            compiler
                .raw('__' + argName + '__ = ')
                .subcompile(argsNodes[argName]);

            if (++pos < count) compiler.raw(', ');
        });

        if (count) compiler.raw(', ');

        compiler
            .raw('...__varargs__')
            .raw(') {\n')
            .indent()
            .write('let macros = { ...this._macros };\n')
            .write('let context = this._env.mergeGlobals({\n')
            .indent();

        objectKeys(argsNodes).forEach(argName => {
            compiler
                .write('')
                .string(argName, '"')
                .raw(': __' + argName + '__')
                .raw(',\n');
        });

        compiler.write('').string(MacroNode.VARARGS_NAME, '"').raw(': ');

        compiler
            .raw('__varargs__,\n')
            .outdent()
            .write('});\n\n')
            .write('let blocks = [];\n\n');

        compiler.write('this.stash();\n');
        compiler
            .write('try {\n')
            .indent()
            .subcompile(this.getNode('body'))
            .raw('\n')
            .write('let tmp = this.getOutput();\n')
            .write(
                "return tmp === '' ? '' : new this._prototypes.Markup(tmp);\n"
            )
            .outdent()
            .write('} finally {\n')
            .indent()
            .write('this.pop();\n')
            .outdent()
            .write('}\n')
            .outdent()
            .write('}\n\n');
    }
}
