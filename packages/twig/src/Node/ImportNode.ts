/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { AbstractExpression } from './Expression/AbstractExpression';
import { NameExpression } from './Expression/NameExpression';
import { TwigNode } from './TwigNode';

export class ImportNode extends TwigNode {
    constructor(
        expr: AbstractExpression,
        importVar: AbstractExpression,
        lineno: number,
        tag: null | string = null,
        global = true
    ) {
        super({ expr, var: importVar }, { global }, lineno, tag);
    }

    compile(compiler: Compiler) {
        compiler
            .addDebugInfo(this)
            .write('macros[')
            .repr(this.getNode('var').getAttribute('name'))
            .raw('] = ');

        if (this.getAttribute('global')) {
            compiler
                .raw('this._macros[')
                .repr(this.getNode('var').getAttribute('name'))
                .raw('] = ');
        }

        if (
            this.getNode('expr') instanceof NameExpression &&
            '_self' === this.getNode('expr').getAttribute('name')
        )
            compiler.raw('this');
        else {
            compiler
                .raw('this._loadTemplate(')
                .subcompile(this.getNode('expr'))
                .raw(', ')
                .repr(this.getTemplateName())
                .raw(', ')
                .repr(this.getTemplateLine())
                .raw(').unwrap()');
        }

        compiler.raw(';\n');
    }
}
