/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { NodeOutputInterface } from './NodeOutputInterface';

export class BlockReferenceNode extends NodeOutputInterface {
    constructor(blockName: string, lineno: number, tag: null | string = null) {
        super({}, { name: blockName }, lineno, tag);
    }

    compile(compiler: Compiler) {
        compiler
            .addDebugInfo(this)
            .write(
                "this.print(this.renderBlock('" +
                    (this.getAttribute('name') as string) +
                    "', context, blocks));\n"
            );
    }
}
