/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { TwigNode } from '../TwigNode';
import { AbstractExpression } from './AbstractExpression';

export class ArrayExpression extends AbstractExpression {
    constructor(elements: TwigNode[], lineno: number) {
        super(elements, {}, lineno);
    }

    addElement(value: AbstractExpression) {
        this.push(value);
    }

    compile(compiler: Compiler) {
        compiler.raw('[');
        let first = true;
        for (const element of this) {
            if (!first) compiler.raw(', ');
            first = false;
            compiler.subcompile(element);
        }
        compiler.raw(']');
    }
}
