/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { AbstractExpression } from './AbstractExpression';

export class ConditionalExpression extends AbstractExpression {
    constructor(
        expr1: AbstractExpression,
        expr2: AbstractExpression,
        expr3: AbstractExpression,
        lineno: number
    ) {
        super({ expr1, expr2, expr3 }, {}, lineno);
    }

    compile(compiler: Compiler) {
        compiler
            .raw('((')
            .subcompile(this.getNode('expr1'))
            .raw(') ? (')
            .subcompile(this.getNode('expr2'))
            .raw(') : (')
            .subcompile(this.getNode('expr3'))
            .raw('))');
    }
}
