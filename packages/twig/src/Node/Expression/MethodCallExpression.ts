/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { BlockNode } from '../BlockNode';
import { AbstractExpression } from './AbstractExpression';
import { ArrayExpression } from './ArrayExpression';
import { NameExpression } from './NameExpression';

export class MethodCallExpression extends AbstractExpression {
    constructor(
        node: AbstractExpression,
        method: string,
        args: ArrayExpression,
        lineno: number
    ) {
        super(
            { node, arguments: args },
            { method, safe: false, isDefinedTest: false },
            lineno
        );

        if (node instanceof NameExpression)
            node.setAttribute('alwaysDefined', true);
    }

    compile(compiler: Compiler) {
        if (this.getAttribute('isDefinedTest')) {
            compiler
                .raw('(macros[')
                .repr(this.getNode('node').getAttribute('name'))
                .raw('] !== undefined && typeof macros[')
                .repr(this.getNode('node').getAttribute('name'))
                .raw('][')
                .repr(this.getAttribute('method'))
                .raw("] === 'function')");

            return;
        }

        compiler
            .raw('this._env.getFunction("twigCallMacro").getCallable()(macros[')
            .repr(this.getNode('node').getAttribute('name'))
            .raw('], ')
            .repr(
                BlockNode.escapeBlockName(this.getAttribute('method') as string)
            )
            .raw(', [');
        let first = true;
        for (const arg of this.getNode('arguments')) {
            if (!first) compiler.raw(', ');
            first = false;

            compiler.subcompile(arg);
        }
        compiler
            .raw('], ')
            .repr(this.getTemplateLine())
            .raw(', context, this.getSourceContext())');
    }
}
