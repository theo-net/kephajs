/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { Template } from '../../Template';
import { TwigNode } from '../TwigNode';
import { AbstractExpression } from './AbstractExpression';

export class GetAttrExpression extends AbstractExpression {
    constructor(
        node: AbstractExpression,
        attribute: AbstractExpression,
        args: null | AbstractExpression,
        type: string,
        lineno: number
    ) {
        const nodes: Record<string, TwigNode> = { node, attribute };
        if (args !== null) nodes.arguments = args;

        super(
            nodes,
            {
                type,
                isDefinedTest: false,
                ignoreStrictCheck: false,
                optimizable: true,
            },
            lineno
        );
    }

    compile(compiler: Compiler) {
        const env = compiler.getEnvironment();

        // optimize array calls
        if (
            this.getAttribute('optimizable') &&
            (!env.isStrictVariables() ||
                this.getAttribute('ignoreStrictCheck')) &&
            !this.getAttribute('isDefinedTest') &&
            Template.ARRAY_CALL === this.getAttribute('type')
        ) {
            const varName = 'this._internalVars.' + compiler.getVarName();
            compiler
                .raw('((' + varName + ' = ')
                .subcompile(this.getNode('node'))
                .raw(') !== undefined && (Array.isArray(' + varName)
                .raw(
                    ') || (' +
                        varName +
                        ' !== null && typeof ' +
                        varName +
                        " === 'object')) ? (" +
                        varName +
                        '['
                )
                .subcompile(this.getNode('attribute'))
                .raw('] !== undefined ? (typeof ' + varName + '[')
                .subcompile(this.getNode('attribute'))
                .raw("] === 'function' ? " + varName + '[')
                .subcompile(this.getNode('attribute'))
                .raw('](');
            if (this.hasNode('arguments'))
                compiler.raw('...').subcompile(this.getNode('arguments'));
            compiler
                .raw(') : ' + varName + '[')
                .subcompile(this.getNode('attribute'))
                .raw(']) : undefined) : undefined)');

            return;
        }

        compiler.raw(
            'this._env.getFunction("attribute").getCallable()(this._env, this._source, '
        );

        if (this.getAttribute('ignoreStrictCheck'))
            this.getNode('node').setAttribute('ignoreStrictCheck', true);

        compiler
            .subcompile(this.getNode('node'))
            .raw(', ')
            .subcompile(this.getNode('attribute'));

        if (this.hasNode('arguments'))
            compiler.raw(', ').subcompile(this.getNode('arguments'));
        else compiler.raw(', []');

        compiler
            .raw(', ')
            .repr(this.getAttribute('type'))
            .raw(', ')
            .repr(this.getAttribute('isDefinedTest'))
            .raw(', ')
            .repr(this.getAttribute('ignoreStrictCheck'))
            .raw(', ')
            .repr(env.hasExtension('SandboxExtension'))
            .raw(', ')
            .repr(this.getNode('node').getTemplateLine())
            .raw(')');
    }
}
