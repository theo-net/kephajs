/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../../Compiler';
import { Environment } from '../../Environment';
import { RecordLoader } from '../../Loader/RecordLoader';
import { TwigTest } from '../../TwigTest';
import { TwigNode } from '../TwigNode';
import { ConstantExpression } from './ConstantExpression';
import { NullTest } from './Test/NullTest';
import { TestExpression } from './TestExpression';

describe('@kephajs/twig/Node/Expression/TestExpression', () => {
    it('Test constructor', () => {
        const expr = new ConstantExpression('foo', 1);
        const testName = new ConstantExpression('null', 1);
        const args = new TwigNode();
        const node = new TestExpression(expr, testName, args, 1);

        expect(node.getNode('node')).to.be.equal(expr);
        expect(node.getNode('arguments')).to.be.equal(args);
        expect(node.getAttribute('name')).to.be.equal(testName);
    });

    it('Should compile the node', () => {
        function twigTestsTestBarbar(
            _str: string,
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            _arg1: unknown = null,
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            _arg2: unknown = null,
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            _args: unknown[] = []
        ) {}

        const env = new Environment(new RecordLoader());
        env.addTest(
            new TwigTest('anonymous', function () {} as (
                ...args: unknown[]
            ) => boolean)
        );
        env.addTest(
            new TwigTest(
                'barbar',
                twigTestsTestBarbar as (...args: unknown[]) => boolean,
                { isVariadic: true, needContext: true }
            )
        );
        const compiler = new Compiler(env);

        const expr = new ConstantExpression('foo', 1);
        let node = new NullTest(expr, 'null', new TwigNode([]), 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal('(`foo` === null)');

        // test as an anonymous function
        node = new TestExpression(
            new ConstantExpression('foo', 1),
            'anonymous',
            new TwigNode([new ConstantExpression('foo', 1)]),
            1
        );
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getTest(`anonymous`).getCallable()(`foo`, `foo`)'
        );

        // arbitrary named arguments
        const str = new ConstantExpression('abc', 1);
        node = new TestExpression(str, 'barbar', new TwigNode([]), 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getTest(`barbar`).getCallable()(`abc`)'
        );

        node = new TestExpression(
            str,
            'barbar',
            new TwigNode({ foo: new ConstantExpression('bar', 1) }),
            1
        );
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getTest(`barbar`).getCallable()(`abc`, `bar`)'
        );

        node = new TestExpression(
            str,
            'barbar',
            new TwigNode({ arg2: new ConstantExpression('bar', 1) }),
            1
        );
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getTest(`barbar`).getCallable()(`abc`, `bar`)'
        );

        node = new TestExpression(
            str,
            'barbar',
            new TwigNode([
                new ConstantExpression('3', 1),
                new ConstantExpression('bar', 1),
            ]),
            1
        );
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getTest(`barbar`).getCallable()(`abc`, `3`, `bar`)'
        );
    });
});
