/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../../Compiler';
import { Environment } from '../../Environment';
import { RecordLoader } from '../../Loader/RecordLoader';
import { ArrayExpression } from './ArrayExpression';
import { ConstantExpression } from './ConstantExpression';

describe('@kephajs/twig/Node/Expression/ArrayExpression', () => {
    it('Test constructor', () => {
        const foo = new ConstantExpression('bar', 1);
        const elements = [new ConstantExpression('foo', 1), foo];
        const node = new ArrayExpression(elements, 1);

        expect(node.getNode(1)).to.be.equal(foo);
    });

    it('Should compile the node', () => {
        const elements = [
            new ConstantExpression('foo', 1),
            new ConstantExpression('bar', 1),
        ];
        const node = new ArrayExpression(elements, 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);

        expect(compiler.getSource().trim()).to.be.equal('[`foo`, `bar`]');
    });
});
