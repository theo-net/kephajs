/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { AbstractExpression } from './AbstractExpression';

export class ConstantExpression extends AbstractExpression {
    constructor(value: unknown, lineno: number, quote?: string) {
        super({}, { value }, lineno);
        if (quote) this.setAttribute('quote', "'");
    }

    compile(compiler: Compiler) {
        if (this.hasAttribute('quote'))
            compiler.repr(
                this.getAttribute('value'),
                this.getAttribute('quote') as string
            );
        else compiler.repr(this.getAttribute('value'));
    }
}
