/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../../Compiler';
import { Environment } from '../../Environment';
import { RecordLoader } from '../../Loader/RecordLoader';
import { ConstantExpression } from './ConstantExpression';
import { ConditionalExpression } from './ConditionalExpression';

describe('@kephajs/twig/Node/Expression/ConditionalExpression', () => {
    it('Test constructor', () => {
        const expr1 = new ConstantExpression(1, 1);
        const expr2 = new ConstantExpression(2, 1);
        const expr3 = new ConstantExpression(3, 1);
        const node = new ConditionalExpression(expr1, expr2, expr3, 1);

        expect(node.getNode('expr1')).to.be.equal(expr1);
        expect(node.getNode('expr2')).to.be.equal(expr2);
        expect(node.getNode('expr3')).to.be.equal(expr3);
    });

    it('Should compile the node', () => {
        const expr1 = new ConstantExpression(1, 1);
        const expr2 = new ConstantExpression(2, 1);
        const expr3 = new ConstantExpression(3, 1);
        const node = new ConditionalExpression(expr1, expr2, expr3, 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);

        expect(compiler.getSource().trim()).to.be.equal('((1) ? (2) : (3))');
    });
});
