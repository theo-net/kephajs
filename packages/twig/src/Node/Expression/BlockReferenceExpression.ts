/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { TwigNode } from '../TwigNode';
import { AbstractExpression } from './AbstractExpression';

export class BlockReferenceExpression extends AbstractExpression {
    constructor(
        name: TwigNode,
        template: null | TwigNode = null,
        lineno = 0,
        tag: null | string = null
    ) {
        const nodes: Record<string, TwigNode> = { name };
        if (template !== null) nodes.template = template;

        super(nodes, { isDefinedTest: false }, lineno, tag);
    }

    compile(compiler: Compiler) {
        if (this.getAttribute('isDefinedTest') === true)
            this._compileTemplateCall(compiler, 'hasBlock');
        else {
            compiler.addDebugInfo(this);
            this._compileTemplateCall(compiler, 'renderBlock');
        }
    }

    private _compileTemplateCall(compiler: Compiler, method: string) {
        if (!this.hasNode('template')) compiler.write('this');
        else {
            compiler
                .write('this._loadTemplate(')
                .subcompile(this.getNode('template'))
                .raw(', ')
                .repr(this.getTemplateName())
                .raw(', ')
                .repr(this.getTemplateLine())
                .raw(')');
        }

        compiler.raw('.' + method);

        return this._compileBlockArguments(compiler);
    }

    private _compileBlockArguments(compiler: Compiler) {
        compiler.raw('(').subcompile(this.getNode('name')).raw(', context');

        if (!this.hasNode('template')) compiler.raw(', blocks');

        return compiler.raw(')');
    }
}
