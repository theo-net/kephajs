/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../../Compiler';
import { Environment } from '../../Environment';
import { AbstractExtension } from '../../Extension/AbstractExtension';
import { RecordLoader } from '../../Loader/RecordLoader';
import { TwigFilter } from '../../TwigFilter';
import { TwigNode } from '../TwigNode';
import { ConstantExpression } from './ConstantExpression';
import { FilterExpression } from './FilterExpression';

describe('@kephajs/twig/Node/Expression/FilterExpression', () => {
    it('Test constructor', () => {
        const expr = new ConstantExpression('foo', 1);
        const name = new ConstantExpression('upper', 1);
        const args = new TwigNode();
        const node = new FilterExpression(expr, name, args, 1);

        expect(node.getNode('node')).to.be.equal(expr);
        expect(node.getNode('filter')).to.be.equal(name);
        expect(node.getNode('arguments')).to.be.equal(args);
    });

    function createFilter(node: TwigNode, name: string, args: TwigNode[] = []) {
        return new FilterExpression(
            node,
            new ConstantExpression(name, 1),
            new TwigNode(args),
            1
        );
    }

    function twigTestsFilterDummy() {}

    function getTests() {
        const environment = new Environment(new RecordLoader());
        environment.addFilter(
            new TwigFilter('bar', twigTestsFilterDummy, {
                needsEnvironment: true,
            })
        );
        environment.addFilter(
            new TwigFilter('bar_closure', () => twigTestsFilterDummy(), {
                needsEnvironment: true,
            })
        );

        const extension = new (class TestExtension extends AbstractExtension {
            getFilters() {
                return {
                    foo: new TwigFilter('foo', this.foo),
                    foobar: new TwigFilter('foobar', this._foobar),
                };
            }

            foo() {}

            protected _foobar() {}
        })();
        environment.addExtension(extension);

        const tests: [FilterExpression, string, Environment | null][] = [];

        const expr = new ConstantExpression('foo', 1);
        let node = createFilter(expr, 'upper');
        node = createFilter(node, 'number_format', [
            new ConstantExpression(2, 1),
            new ConstantExpression('.', 1),
            new ConstantExpression(',', 1),
        ]);

        tests.push([
            node,
            'this._env.getFilter(`number_format`).getCallable()(this._env, this._env.getFilter(`upper`).getCallable()(this._env, `foo`), 2, `.`, `,`)',
            null,
        ]);

        // named arguments
        let date = new ConstantExpression(0, 1);
        node = createFilter(date, 'date', [
            new ConstantExpression('d/m/Y H:i:s P', 1),
            new ConstantExpression('America/Chicago', 1),
        ]);
        tests.push([
            node,
            'this._env.getFilter(`date`).getCallable()(this._env, 0, `d/m/Y H:i:s P`, `America/Chicago`)',
            null,
        ]);

        // skip an optional argument
        date = new ConstantExpression(0, 1);
        node = createFilter(date, 'date', [
            new ConstantExpression(null, 1),
            new ConstantExpression('America/Chicago', 1),
        ]);
        tests.push([
            node,
            'this._env.getFilter(`date`).getCallable()(this._env, 0, null, `America/Chicago`)',
            null,
        ]);

        // underscores vs camelCase for named arguments
        const string = new ConstantExpression('abc', 1);
        node = createFilter(string, 'reverse', [
            new ConstantExpression(true, 1),
        ]);
        tests.push([
            node,
            'this._env.getFilter(`reverse`).getCallable()(`abc`, true)',
            null,
        ]);
        node = createFilter(string, 'reverse', [
            new ConstantExpression(true, 1),
        ]);

        // filter as an anonymous function
        node = createFilter(new ConstantExpression('foo', 1), 'anonymous');
        tests.push([
            node,
            'this._env.getFilter(`anonymous`).getCallable()(`foo`)',
            null,
        ]);

        // needs environment
        node = createFilter(string, 'bar');
        tests.push([
            node,
            'this._env.getFilter(`bar`).getCallable()(this._env, `abc`)',
            environment,
        ]);

        node = createFilter(string, 'bar_closure');
        tests.push([
            node,
            'this._env.getFilter(`bar_closure`).getCallable()(this._env, `abc`)',
            environment,
        ]);

        node = createFilter(string, 'bar', [new ConstantExpression('bar', 1)]);
        tests.push([
            node,
            'this._env.getFilter(`bar`).getCallable()(this._env, `abc`, `bar`)',
            environment,
        ]);

        // from extension
        node = createFilter(string, 'foo');
        tests.push([
            node,
            'this._env.getFilter(`foo`).getCallable()(`abc`)',
            environment,
        ]);

        node = createFilter(string, 'foobar');
        tests.push([
            node,
            'this._env.getFilter(`foobar`).getCallable()(`abc`)',
            environment,
        ]);

        return tests;
    }

    function getEnvironment() {
        const env = new Environment(new RecordLoader({}));
        env.addFilter(new TwigFilter('anonymous', function () {}));

        return env;
    }

    it('Should compile the node', () => {
        getTests().forEach(test => {
            const compiler = new Compiler(test[2] ?? getEnvironment());
            compiler.compile(test[0]);

            expect(compiler.getSource().trim()).to.be.equal(test[1]);
        });
    });
});
