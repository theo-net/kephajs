/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { AbstractExpression } from './AbstractExpression';

export class ParentExpression extends AbstractExpression {
    constructor(parentName: string, lineno: number, tag: null | string = null) {
        super({}, { output: false, name: parentName }, lineno, tag);
    }

    compile(compiler: Compiler) {
        compiler
            .raw('this.renderParentBlock(')
            .string(this.getAttribute('name') as string)
            .raw(', context, blocks)');
    }
}
