/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { arrayChunk } from '@kephajs/core/Helpers/Helpers';
import { Compiler } from '../../Compiler';
import { TwigNode } from '../TwigNode';
import { AbstractExpression } from './AbstractExpression';
import { ConstantExpression } from './ConstantExpression';

export class RecordExpression extends AbstractExpression {
    private _arrayindex = -1;

    constructor(elements: TwigNode[], lineno: number) {
        super(elements, {}, lineno);

        this.getKeyValuePairs().forEach(pair => {
            if (
                pair.key instanceof ConstantExpression &&
                /\d+/g.exec(pair.key.getAttribute('value') + '') &&
                (pair.key.getAttribute('value') as number) > this._arrayindex
            )
                this._arrayindex = pair.key.getAttribute('value') as number;
        });
    }

    getKeyValuePairs() {
        const pairs: { key: TwigNode; value: TwigNode }[] = [];
        const nodes: TwigNode[] = [];
        for (const nodeName in this._nodes) nodes.push(this._nodes[nodeName]);

        arrayChunk(nodes, 2).forEach(pair => {
            pairs.push({
                key: pair[0],
                value: pair[1],
            });
        });

        return pairs;
    }

    hasElement(key: AbstractExpression) {
        let has = false;
        this.getKeyValuePairs().forEach(pair => {
            // we compare the string representation of the keys
            // to avoid comparing the line numbers which are not relevant here.
            if (key.toString() === pair.key.toString()) has = true;
        });

        return has;
    }

    addElement(
        value: AbstractExpression,
        key: null | AbstractExpression = null
    ) {
        if (key === null)
            key = new ConstantExpression(
                ++this._arrayindex,
                value.getTemplateLine()
            );

        this.push(key, value);
    }

    compile(compiler: Compiler) {
        compiler.raw('{');
        let first = true;
        this.getKeyValuePairs().forEach(pair => {
            if (pair.key instanceof ConstantExpression)
                pair.key.setAttribute('quote', "'");
            if (!first) compiler.raw(', ');
            first = false;
            compiler
                .raw('[')
                .subcompile(pair.key)
                .raw(']: ')
                .subcompile(pair.value);
        });
        compiler.raw('}');
    }
}
