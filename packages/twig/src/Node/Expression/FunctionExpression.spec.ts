/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../../Compiler';
import { Environment } from '../../Environment';
import { RecordLoader } from '../../Loader/RecordLoader';
import { TwigFunction } from '../../TwigFunction';
import { TwigNode } from '../TwigNode';
import { ConstantExpression } from './ConstantExpression';
import { FunctionExpression } from './FunctionExpression';

describe('@kephajs/twig/Node/Expression/FunctionExpression', () => {
    it('Test constructor', () => {
        const name = 'function';
        const args = new TwigNode();
        const node = new FunctionExpression(name, args, 1);

        expect(node.getAttribute('name')).to.be.equal(name);
        expect(node.getNode('arguments')).to.be.equal(args);
    });

    function twigTestsFunction() {}

    function createFunction(
        name: string,
        args: Record<string, TwigNode> | TwigNode[] = []
    ) {
        return new FunctionExpression(name, new TwigNode(args), 1);
    }

    it('Should compile the node', () => {
        const environment = new Environment(new RecordLoader());
        environment.addFunction(new TwigFunction('foo', twigTestsFunction, {}));
        environment.addFunction(
            new TwigFunction('bar', twigTestsFunction, {
                needsEnvironment: true,
            })
        );
        environment.addFunction(
            new TwigFunction('foofoo', twigTestsFunction, {
                needsContext: true,
            })
        );
        environment.addFunction(
            new TwigFunction('foobar', twigTestsFunction, {
                needsEnvironment: true,
                needsContext: true,
            })
        );
        environment.addFunction(
            new TwigFunction('barbar', twigTestsFunction, {
                isVariadic: true,
            })
        );
        const compiler = new Compiler(environment);

        let node = createFunction('foo');
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction(`foo`).getCallable()()'
        );

        node = createFunction('foo', [
            new ConstantExpression('bar', 1),
            new ConstantExpression('foobar', 1),
        ]);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction(`foo`).getCallable()(`bar`, `foobar`)'
        );

        node = createFunction('bar');
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction(`bar`).getCallable()(this._env)'
        );

        node = createFunction('bar', [new ConstantExpression('bar', 1)]);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction(`bar`).getCallable()(this._env, `bar`)'
        );

        node = createFunction('foofoo');
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction(`foofoo`).getCallable()(context)'
        );

        node = createFunction('foofoo', [new ConstantExpression('bar', 1)]);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction(`foofoo`).getCallable()(context, `bar`)'
        );

        node = createFunction('foobar');
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction(`foobar`).getCallable()(this._env, context)'
        );

        node = createFunction('foobar', [new ConstantExpression('bar', 1)]);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction(`foobar`).getCallable()(this._env, context, `bar`)'
        );

        // named arguments
        node = createFunction('date', {
            timezone: new ConstantExpression('America/Chicago', 1),
            date: new ConstantExpression(0, 1),
        });
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction(`date`).getCallable()(this._env, `America/Chicago`, 0)'
        );

        // arbitrary named arguments
        node = createFunction('barbar');
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction(`barbar`).getCallable()()'
        );

        node = createFunction('barbar', {
            foo: new ConstantExpression('bar', 1),
        });
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction(`barbar`).getCallable()(`bar`)'
        );

        node = createFunction('barbar', {
            arg2: new ConstantExpression('bar', 1),
        });
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction(`barbar`).getCallable()(`bar`)'
        );

        node = createFunction('barbar', [
            new ConstantExpression('1', 1),
            new ConstantExpression('2', 1),
            new ConstantExpression('3', 1),
            new ConstantExpression('bar', 1),
        ]);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction(`barbar`).getCallable()(`1`, `2`, `3`, `bar`)'
        );
    });
});
