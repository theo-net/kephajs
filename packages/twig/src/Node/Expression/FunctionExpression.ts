/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { RuntimeError } from '../../Error/RuntimeError';
import { TwigFunction } from '../../TwigFunction';
import { TwigNode } from '../TwigNode';
import { CallExpression } from './CallExpression';

export class FunctionExpression extends CallExpression {
    constructor(name: string, args: TwigNode, lineno: number) {
        super({ arguments: args }, { name, isDefinedTest: false }, lineno);
    }

    compile(compiler: Compiler) {
        const fctNname = this.getAttribute('name') as string;
        const fct = compiler
            .getEnvironment()
            .getFunction(fctNname) as TwigFunction;

        if (fct === null)
            throw new RuntimeError(`Cannot find "${fctNname}" function.`);

        this.setAttribute('name', fctNname);
        this.setAttribute('type', 'function');
        this.setAttribute('needsEnvironment', fct.needsEnvironment());
        this.setAttribute('needsContext', fct.needsContext());
        this.setAttribute('arguments', fct.getArguments());
        const callable = fct.getCallable();
        this.setAttribute('callable', callable);
        this.setAttribute('isVariadic', fct.isVariadic());

        compiler.raw('this._env.getFunction(').string(fctNname).raw(')');
        this._compileCallable(compiler);
    }
}
