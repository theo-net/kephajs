/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../../Compiler';
import { AbstractBinary } from './AbstractBinary';

export class RangeBinary extends AbstractBinary {
    compile(compiler: Compiler): void {
        compiler
            .raw("this._env.getFunction('range').getCallable()(")
            .subcompile(this.getNode('left'))
            .raw(', ')
            .subcompile(this.getNode('right'))
            .raw(')');
    }

    operator(compiler: Compiler) {
        return compiler.raw('..');
    }
}
