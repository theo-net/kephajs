/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../../Compiler';
import { AbstractBinary } from './AbstractBinary';

export class MatchesBinary extends AbstractBinary {
    compile(compiler: Compiler) {
        compiler
            .raw(
                "(new RegExp(this._env.getFunction('twigStrToRegExp').getCallable()('' + "
            ) // force string conversion
            .subcompile(this.getNode('right'))
            .raw(')).test(')
            .subcompile(this.getNode('left'))
            .raw('))');
    }

    operator(compiler: Compiler) {
        return compiler.raw('');
    }
}
