/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../../Compiler';
import { AbstractBinary } from './AbstractBinary';

export class EndsWithBinary extends AbstractBinary {
    compile(compiler: Compiler) {
        const left = "this._internalVars['" + compiler.getVarName() + "']";
        const right = "this._internalVars['" + compiler.getVarName() + "']";
        compiler
            .raw('(typeof (' + left + ' = ')
            .subcompile(this.getNode('left'))
            .raw(") === 'string' && typeof (" + right + ' = ')
            .subcompile(this.getNode('right'))
            .raw(
                ") === 'string' && (" +
                    right +
                    " === '' || " +
                    left +
                    '.substring(' +
                    left +
                    '.length - ' +
                    right +
                    '.length) === ' +
                    right +
                    '))'
            );
    }

    operator(compiler: Compiler) {
        return compiler.raw('');
    }
}
