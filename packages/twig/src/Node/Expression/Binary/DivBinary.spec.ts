/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../../../Compiler';
import { Environment } from '../../../Environment';
import { RecordLoader } from '../../../Loader/RecordLoader';
import { ConstantExpression } from '../ConstantExpression';
import { DivBinary } from './DivBinary';

describe('@kephajs/twig/Node/Expression/Binary/DivBinary.spec.ts', () => {
    it('Test constructor', () => {
        const left = new ConstantExpression(1, 1);
        const right = new ConstantExpression(2, 1);
        const node = new DivBinary(left, right, 1);

        expect(node.getNode('left')).to.be.equal(left);
        expect(node.getNode('right')).to.be.equal(right);
    });

    it('Should compile the node', () => {
        const left = new ConstantExpression(1, 1);
        const right = new ConstantExpression(2, 1);
        const node = new DivBinary(left, right, 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);

        expect(compiler.getSource().trim()).to.be.equal('(1 / 2)');
    });
});
