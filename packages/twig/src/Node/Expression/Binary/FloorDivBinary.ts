/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../../Compiler';
import { AbstractBinary } from './AbstractBinary';

export class FloorDivBinary extends AbstractBinary {
    compile(compiler: Compiler) {
        compiler.raw('Math.floor(Number(');
        super.compile(compiler);
        compiler.raw('))');
    }

    operator(compiler: Compiler) {
        return compiler.raw('/');
    }
}
