/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../../Compiler';
import { TwigNode } from '../../TwigNode';
import { AbstractExpression } from '../AbstractExpression';

export class AbstractBinary extends AbstractExpression {
    constructor(left: TwigNode, right: TwigNode, lineno: number) {
        super({ left, right }, {}, lineno);
    }

    compile(compiler: Compiler) {
        compiler.raw('(').subcompile(this.getNode('left')).raw(' ');
        this.operator(compiler);
        compiler.raw(' ').subcompile(this.getNode('right')).raw(')');
    }

    operator(compiler: Compiler) {
        return compiler;
    }
}
