/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../../Compiler';
import { Environment } from '../../Environment';
//import { Lexer } from '../../Lexer';
import { RecordLoader } from '../../Loader/RecordLoader';
//import { Parser } from '../../Parser';
//import { Source } from '../../Source';
import { ArrayExpression } from './ArrayExpression';
import { ConstantExpression } from './ConstantExpression';
import { GetAttrExpression } from './GetAttrExpression';
import { NameExpression } from './NameExpression';
import { NullCoalesceExpression } from './NullCoalesceExpression';

describe('@kephajs/twig/Node/Expression/NullCoalesceExpression', () => {
    it('Should clone the left node', () => {
        const left = new NameExpression('foo', 1);
        const right = new ConstantExpression(2, 1);
        const node = new NullCoalesceExpression(left, right, 1);

        expect(
            node.getNode('expr1').getNode('left').getNode('node')
        ).to.be.not.equal(node.getNode('expr2'));
    });

    it('Should clone the left expression', () => {
        const left = new GetAttrExpression(
            new GetAttrExpression(
                new GetAttrExpression(
                    new NameExpression('foo', 1),
                    new ConstantExpression('missing', 1),
                    new ArrayExpression([], 1),
                    'any',
                    1
                ),
                new ConstantExpression('bar', 1),
                new ArrayExpression([], 1),
                'any',
                1
            ),
            new ConstantExpression('foo', 1),
            new ArrayExpression([], 1),
            'any',
            1
        );
        const right = new ConstantExpression('OK', 1);
        const node = new NullCoalesceExpression(left, right, 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);
        expect(true).to.be.equal(true);
    });

    it('Should compile the node', () => {
        const left = new NameExpression('foo', 1);
        const right = new ConstantExpression(2, 1);
        const node = new NullCoalesceExpression(left, right, 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);

        expect(compiler.getSource().trim()).to.be.equal(
            '((// line 1\ncontext[`foo`]) ?? (2))'
        );
    });
});
