/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../../Compiler';
import { Environment } from '../../Environment';
import { RecordLoader } from '../../Loader/RecordLoader';
import { Template } from '../../Template';
import { ArrayExpression } from './ArrayExpression';
import { ConstantExpression } from './ConstantExpression';
import { GetAttrExpression } from './GetAttrExpression';
import { NameExpression } from './NameExpression';

describe('@kephajs/twig/Node/Expression/GetAttrExpression', () => {
    it('Test constructor', () => {
        const expr = new NameExpression('foo', 1);
        const attr = new ConstantExpression('bar', 1);
        const args = new ArrayExpression([], 1);
        args.addElement(new NameExpression('foo', 1));
        args.addElement(new ConstantExpression('bar', 1));
        const node = new GetAttrExpression(
            expr,
            attr,
            args,
            Template.ARRAY_CALL,
            1
        );

        expect(node.getNode('node')).to.be.equal(expr);
        expect(node.getNode('attribute')).to.be.equal(attr);
        expect(node.getNode('arguments')).to.be.equal(args);
        expect(node.getAttribute('type')).to.be.equal(Template.ARRAY_CALL);
    });

    it('Should compile the node', () => {
        const compiler = new Compiler(new Environment(new RecordLoader()));

        const expr = new NameExpression('foo', 1);
        const attr = new ConstantExpression('bar', 1);
        let args = new ArrayExpression([], 1);

        let node = new GetAttrExpression(
            expr,
            attr,
            args,
            Template.ANY_CALL,
            1
        );
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction("attribute").getCallable()(this._env, this._source, ' +
                '// line 1\n(context[`foo`] ?? undefined)' +
                ', `bar`, [], `any`, false, false, false, 1)'
        );

        node = new GetAttrExpression(expr, attr, args, Template.ARRAY_CALL, 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            '((this._internalVars.__internal_compile_0 = // line 1\n' +
                "(context[`foo`] ?? undefined)) !== undefined && (Array.isArray(this._internalVars.__internal_compile_0) || (this._internalVars.__internal_compile_0 !== null && typeof this._internalVars.__internal_compile_0 === 'object')) ? (this._internalVars.__internal_compile_0[`bar`] !== undefined ? (typeof this._internalVars.__internal_compile_0[`bar`] === 'function' ? this._internalVars.__internal_compile_0[`bar`](...[]) : this._internalVars.__internal_compile_0[`bar`]) : undefined) : undefined)"
        );

        args = new ArrayExpression([], 1);
        args.addElement(new NameExpression('foo', 1));
        args.addElement(new ConstantExpression('bar', 1));
        node = new GetAttrExpression(expr, attr, args, Template.METHOD_CALL, 1);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            'this._env.getFunction("attribute").getCallable()(this._env, this._source, ' +
                '// line 1\n(context[`foo`] ?? undefined)' +
                ', `bar`, [' +
                '(context[`foo`] ?? undefined)' +
                ', `bar`], `method`, false, false, false, 1)'
        );
    });
});
