/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { NameExpression } from './NameExpression';

export class AssignNameExpression extends NameExpression {
    compile(compiler: Compiler) {
        compiler
            .raw('context[')
            .string(this.getAttribute('name') as string)
            .raw(']');
    }
}
