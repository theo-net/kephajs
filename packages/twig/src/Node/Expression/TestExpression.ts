/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { TwigTest } from '../../TwigTest';
import { TwigNode } from '../TwigNode';
import { CallExpression } from './CallExpression';

export class TestExpression extends CallExpression {
    constructor(
        node: TwigNode,
        testName: string | TwigNode,
        args: TwigNode | null,
        lineno: number
    ) {
        const nodes: Record<string, TwigNode> = { node };
        if (args !== null) nodes.arguments = args;

        super(nodes, { name: testName }, lineno);
    }

    compile(compiler: Compiler) {
        const testName = this.getAttribute('name') as string;
        const test = compiler.getEnvironment().getTest(testName) as TwigTest;

        this.setAttribute('name', testName);
        this.setAttribute('type', 'test');
        this.setAttribute('arguments', test.getArguments());
        this.setAttribute('callable', test.getCallable());
        this.setAttribute('isVariadic', test.isVariadic());

        compiler.raw('this._env.getTest(').string(testName).raw(')');

        this._compileCallable(compiler);
    }
}
