/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { TwigNode } from '../TwigNode';
import { CallExpression } from './CallExpression';
import { ConstantExpression } from './ConstantExpression';

class NodeExpressionCall extends CallExpression {
    getArguments(args: TwigNode) {
        return super._getArguments(args);
    }
}

describe('@kephajs/twig/Node/Expression/CallExpression', () => {
    it('Should get arguments', () => {
        const node = new NodeExpressionCall([], {
            type: 'function',
            name: 'date',
        });
        const arg = new TwigNode();
        arg.push(
            new ConstantExpression('U', 1),
            new ConstantExpression(null, 1)
        );
        expect(node.getArguments(arg)).to.be.deep.equal([
            new ConstantExpression('U', 1),
            new ConstantExpression(null, 1),
        ]);
    });
});
