/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { AbstractExpression } from './AbstractExpression';

export class NameExpression extends AbstractExpression {
    private _specialVars = {
        _self: 'this.getTemplateName()',
        _context: 'context',
        _locale: 'this._env.getLocale()',
    };

    constructor(name: string, lineno: number) {
        super(
            {},
            {
                name,
                isDefinedTest: false,
                ignoreStrictCheck: false,
                alwaysDefined: false,
            },
            lineno
        );
    }

    compile(compiler: Compiler) {
        const nameAttr = this.getAttribute('name') as string;

        compiler.addDebugInfo(this);

        if (this.getAttribute('isDefinedTest') === true) {
            if (this.isSpecial()) compiler.repr(true);
            compiler.raw('(context[').string(nameAttr).raw('] !== undefined)');
        } else if (this.isSpecial())
            compiler.raw(
                this._specialVars[nameAttr as '_self' | '_context' | '_locale']
            );
        else if (this.getAttribute('alwaysDefined') === true) {
            compiler.raw('context[').string(nameAttr).raw(']');
        } else {
            if (
                this.getAttribute('ignoreStrictCheck') ||
                !compiler.getEnvironment().isStrictVariables()
            ) {
                compiler
                    .raw('(context[')
                    .string(nameAttr)
                    .raw('] ?? undefined)');
            } else {
                compiler
                    .raw('(context[')
                    .string(nameAttr)
                    .raw('] !== undefined ? context[')
                    .string(nameAttr)
                    .raw(
                        "] : (() => { throw new this._prototypes.RuntimeError('Variable "
                    )
                    .string(nameAttr, '"')
                    .raw(" does not exist.', ")
                    .repr(this._lineno)
                    .raw(', this._source); })()')
                    .raw(')');
            }
        }
    }

    isSpecial() {
        return (
            this._specialVars[
                this.getAttribute('name') as '_self' | '_context' | '_locale'
            ] !== undefined
        );
    }
}
