/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { TwigNode } from '../TwigNode';
import { AbstractExpression } from './AbstractExpression';

export class ArrowFunctionExpression extends AbstractExpression {
    constructor(
        expr: AbstractExpression,
        names: TwigNode,
        lineno: number,
        tag: string | null = null
    ) {
        super({ expr, names }, {}, lineno, tag);
    }

    compile(compiler: Compiler): void {
        compiler.addDebugInfo(this).raw('(');

        let first = true;
        for (const node of this.getNode('names')) {
            if (!first) compiler.raw(', ');
            first = false;

            compiler
                .raw('__')
                .raw(node.getAttribute('name') as string)
                .raw('__');
        }
        compiler.raw(') => { ');
        for (const node of this.getNode('names')) {
            compiler
                .raw('context["')
                .raw(node.getAttribute('name') as string)
                .raw('"] = __')
                .raw(node.getAttribute('name') as string)
                .raw('__; ');
        }
        compiler.raw('return ').subcompile(this.getNode('expr')).raw('; }');
    }
}
