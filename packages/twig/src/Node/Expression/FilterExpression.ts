/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { RuntimeError } from '../../Error/RuntimeError';
import { TwigNode } from '../TwigNode';
import { CallExpression } from './CallExpression';
import { ConstantExpression } from './ConstantExpression';

export class FilterExpression extends CallExpression {
    constructor(
        node: TwigNode,
        filterName: ConstantExpression,
        args: TwigNode,
        lineno: number,
        tag: null | string = null
    ) {
        super({ node, filter: filterName, arguments: args }, {}, lineno, tag);
    }

    compile(compiler: Compiler) {
        const name = this.getNode('filter').getAttribute('value') as string;
        const filter = compiler.getEnvironment().getFilter(name);

        if (filter === null)
            throw new RuntimeError(`Cannot find "${name}" filter.`);

        this.setAttribute('name', name);
        this.setAttribute('type', 'filter');
        this.setAttribute('needsEnvironment', filter.needsEnvironment());
        this.setAttribute('needsContext', filter.needsContext());
        this.setAttribute('arguments', filter.getArguments());
        this.setAttribute('callable', filter.getCallable());
        this.setAttribute('isVariadic', filter.isVariadic());

        compiler.raw('this._env.getFilter(').string(name).raw(')');

        this._compileCallable(compiler);
    }
}
