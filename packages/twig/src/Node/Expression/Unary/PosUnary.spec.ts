/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../../../Compiler';
import { Environment } from '../../../Environment';
import { RecordLoader } from '../../../Loader/RecordLoader';
import { ConstantExpression } from '../ConstantExpression';
import { PosUnary } from './PosUnary';

describe('@kephajs/twig/Node/Expression/Unary/PosUnary', () => {
    it('Test constructor', () => {
        const expr = new ConstantExpression(1, 1);
        const node = new PosUnary(expr, 1);

        expect(node.getNode('node')).to.be.equal(expr);
    });

    it('Should compile the node', () => {
        const expr = new ConstantExpression(1, 1);
        const node = new PosUnary(expr, 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);

        expect(compiler.getSource().trim()).to.be.equal('+1');
    });
});
