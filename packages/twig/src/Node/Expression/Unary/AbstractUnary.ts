/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../../Compiler';
import { TwigNode } from '../../TwigNode';
import { AbstractExpression } from '../AbstractExpression';

export class AbstractUnary extends AbstractExpression {
    constructor(node: TwigNode, lineno: number) {
        super({ node }, {}, lineno);
    }

    compile(compiler: Compiler) {
        compiler.raw(' ');
        this.operator(compiler);
        compiler.subcompile(this.getNode('node'));
    }

    operator(compiler: Compiler) {
        return compiler;
    }
}
