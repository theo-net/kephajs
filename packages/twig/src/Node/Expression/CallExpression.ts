/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { TwigNode } from '../TwigNode';
import { AbstractExpression } from './AbstractExpression';

export abstract class CallExpression extends AbstractExpression {
    protected _compileCallable(compiler: Compiler) {
        compiler.raw('.getCallable()');

        this._compileArguments(compiler);
    }

    protected _compileArguments(compiler: Compiler, isArray = false) {
        compiler.raw(isArray ? '[' : '(');

        let first = true;

        if (
            this.hasAttribute('needsEnvironment') &&
            this.getAttribute('needsEnvironment')
        ) {
            compiler.raw('this._env');
            first = false;
        }

        if (
            this.hasAttribute('needsContext') &&
            (this.getAttribute('needsContext') as boolean)
        ) {
            if (!first) compiler.raw(', ');
            compiler.raw('context');
            first = false;
        }

        if (this.hasAttribute('arguments')) {
            (this.getAttribute('arguments') as string[]).forEach(arg => {
                if (!first) compiler.raw(', ');
                compiler.string(arg);
                first = false;
            });
        }

        if (this.hasNode('node')) {
            if (!first) compiler.raw(', ');
            compiler.subcompile(this.getNode('node'));
            first = false;
        }

        if (this.hasNode('arguments')) {
            this._getArguments(this.getNode('arguments')).forEach(arg => {
                if (!first) compiler.raw(', ');
                compiler.subcompile(arg);
                first = false;
            });
        }

        compiler.raw(isArray ? ']' : ')');
    }

    protected _getArguments(args: TwigNode) {
        const parameters: TwigNode[] = [];
        for (const node of args) {
            parameters.push(node);
        }

        return parameters;
    }
}
