/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { AbstractExpression } from './AbstractExpression';

export class TempNameExpression extends AbstractExpression {
    constructor(name: string, lineno: number) {
        super([], { name }, lineno);
    }

    compile(compiler: Compiler) {
        compiler
            .raw('_')
            .raw(this.getAttribute('name') as string)
            .raw('_');
    }
}
