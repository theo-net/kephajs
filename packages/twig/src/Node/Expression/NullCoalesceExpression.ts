/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../Compiler';
import { TwigNode } from '../TwigNode';
import { AbstractExpression } from './AbstractExpression';
import { AndBinary } from './Binary/AndBinary';
import { BlockReferenceExpression } from './BlockReferenceExpression';
import { ConditionalExpression } from './ConditionalExpression';
import { NameExpression } from './NameExpression';
import { DefinedTest } from './Test/DefinedTest';
import { NullTest } from './Test/NullTest';
import { NotUnary } from './Unary/NotUnary';

export class NullCoalesceExpression extends ConditionalExpression {
    constructor(left: TwigNode, right: TwigNode, lineno: number) {
        let test: AbstractExpression = new DefinedTest(
            TwigNode.clone(left),
            'defined',
            new TwigNode(),
            left.getTemplateLine()
        );
        // for "block()", we don't need the null test as the return value is always a string
        if (!(left instanceof BlockReferenceExpression)) {
            test = new AndBinary(
                test,
                new NotUnary(
                    new NullTest(
                        left,
                        'null',
                        new TwigNode(),
                        left.getTemplateLine()
                    ),
                    left.getTemplateLine()
                ),
                left.getTemplateLine()
            );
        }

        super(test, left, right, lineno);
    }

    compile(compiler: Compiler) {
        if (this.getNode('expr2') instanceof NameExpression) {
            this.getNode('expr2').setAttribute('alwaysDefined', true);
            compiler
                .raw('((')
                .subcompile(this.getNode('expr2'))
                .raw(') ?? (')
                .subcompile(this.getNode('expr3'))
                .raw('))');
        } else super.compile(compiler);
    }
}
