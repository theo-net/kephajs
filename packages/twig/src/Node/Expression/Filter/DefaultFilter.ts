/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../../Compiler';
import { TwigNode } from '../../TwigNode';
import { ConditionalExpression } from '../ConditionalExpression';
import { ConstantExpression } from '../ConstantExpression';
import { FilterExpression } from '../FilterExpression';
import { GetAttrExpression } from '../GetAttrExpression';
import { NameExpression } from '../NameExpression';
import { DefinedTest } from '../Test/DefinedTest';

export class DefaultFilter extends FilterExpression {
    constructor(
        node: TwigNode,
        filterName: ConstantExpression,
        args: TwigNode,
        lineno: number,
        tag: null | string = null
    ) {
        const dflt = new FilterExpression(
            node,
            new ConstantExpression('default', node.getTemplateLine()),
            args,
            node.getTemplateLine()
        );

        if (
            filterName.getAttribute('value') === 'default' &&
            (node instanceof NameExpression ||
                node instanceof GetAttrExpression)
        ) {
            const test = new DefinedTest(
                TwigNode.clone(node),
                'defined',
                new TwigNode(),
                node.getTemplateLine()
            );
            const falseNode = args.length
                ? args.getNode(0)
                : new ConstantExpression('', node.getTemplateLine());

            node = new ConditionalExpression(
                test,
                dflt,
                falseNode,
                node.getTemplateLine()
            );
        } else node = dflt;

        super(node, filterName, args, lineno, tag);
    }

    compile(compiler: Compiler) {
        compiler.subcompile(this.getNode('node'));
    }
}
