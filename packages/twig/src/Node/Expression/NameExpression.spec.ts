/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../../Compiler';
import { Environment } from '../../Environment';
import { RecordLoader } from '../../Loader/RecordLoader';
import { NameExpression } from './NameExpression';

describe('@kephajs/twig/Node/Expression/NameExpression', () => {
    it('Test constructor', () => {
        const node = new NameExpression('foo', 1);

        expect(node.getAttribute('name')).to.be.equal('foo');
    });

    it('Should compile the node', () => {
        const node = new NameExpression('foo', 1);
        const self = new NameExpression('_self', 1);
        const context = new NameExpression('_context', 1);

        const env = new Environment(new RecordLoader(), {
            strictVariables: true,
        });
        const env1 = new Environment(new RecordLoader(), {
            strictVariables: false,
        });
        const compiler = new Compiler(env);
        const compiler1 = new Compiler(env1);

        const output =
            '(context[`foo`] !== undefined ? context[`foo`] : (() => { throw new this._prototypes.RuntimeError(\'Variable "foo" does not exist.\', 1, this._source); })())';

        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal('// line 1\n' + output);

        compiler1.compile(node);
        expect(compiler1.getSource().trim()).to.be.equal(
            '// line 1\n(context[`foo`] ?? undefined)'
        );

        compiler.compile(self);
        expect(compiler.getSource().trim()).to.be.equal(
            '// line 1\nthis.getTemplateName()'
        );

        compiler.compile(context);
        expect(compiler.getSource().trim()).to.be.equal('// line 1\ncontext');
    });
});
