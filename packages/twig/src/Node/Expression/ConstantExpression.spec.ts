/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../../Compiler';
import { Environment } from '../../Environment';
import { RecordLoader } from '../../Loader/RecordLoader';
import { ConstantExpression } from './ConstantExpression';

describe('@kephajs/twig/Node/Expression/ConstantExpression', () => {
    it('Test constructor', () => {
        const node = new ConstantExpression('foo', 1);

        expect(node.getAttribute('value')).to.be.equal('foo');
    });

    it('Should compile the node', () => {
        const node = new ConstantExpression('foo', 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);

        expect(compiler.getSource().trim()).to.be.equal('`foo`');
    });
});
