/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { TwigNode } from '../../TwigNode';
import { GetAttrExpression } from '../GetAttrExpression';
import { SyntaxError } from '../../../Error/SyntaxError';
import { TestExpression } from '../TestExpression';
import { NameExpression } from '../NameExpression';
import { BlockReferenceExpression } from '../BlockReferenceExpression';
import { ConstantExpression } from '../ConstantExpression';
import { ArrayExpression } from '../ArrayExpression';
import { MethodCallExpression } from '../MethodCallExpression';
import { FunctionExpression } from '../FunctionExpression';
import { Compiler } from '../../../Compiler';
import { RecordExpression } from '../RecordExpression';

export class DefinedTest extends TestExpression {
    constructor(
        node: TwigNode,
        testName: string,
        args: TwigNode | null,
        lineno: number
    ) {
        if (node instanceof NameExpression)
            node.setAttribute('isDefinedTest', true);
        else if (node instanceof GetAttrExpression) {
            node.setAttribute('isDefinedTest', true);
            DefinedTest._changeIgnoreStrictCheck(node);
        } else if (node instanceof BlockReferenceExpression)
            node.setAttribute('isDefinedTest', true);
        else if (
            node instanceof FunctionExpression &&
            'constant' === node.getAttribute('name')
        )
            node.setAttribute('isDefinedTest', true);
        else if (
            node instanceof ConstantExpression ||
            node instanceof ArrayExpression ||
            node instanceof RecordExpression
        )
            node = new ConstantExpression(true, node.getTemplateLine());
        else if (node instanceof MethodCallExpression)
            node.setAttribute('isDefinedTest', true);
        else
            throw new SyntaxError(
                'The "defined" test only works with simple variables.',
                lineno
            );

        super(node, testName, args, lineno);
    }

    static _changeIgnoreStrictCheck(node: GetAttrExpression) {
        node.setAttribute('optimizable', false);
        node.setAttribute('ignoreStrictCheck', true);

        if (node.getNode('node') instanceof GetAttrExpression)
            DefinedTest._changeIgnoreStrictCheck(node.getNode('node'));
    }

    compile(compiler: Compiler) {
        compiler.subcompile(this.getNode('node'));
    }
}
