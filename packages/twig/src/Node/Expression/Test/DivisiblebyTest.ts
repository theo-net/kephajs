/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../../../Compiler';
import { TestExpression } from '../TestExpression';

export class DivisiblebyTest extends TestExpression {
    compile(compiler: Compiler) {
        compiler
            .raw('(')
            .subcompile(this.getNode('node'))
            .raw(' % ')
            .subcompile(this.getNode('arguments').getNode(0))
            .raw(' === 0)');
    }
}
