/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { AbstractExpression } from './Expression/AbstractExpression';
import { TwigNode } from './TwigNode';

export class DoNode extends TwigNode {
    constructor(
        expr: AbstractExpression,
        lineno: number,
        tag: null | string = null
    ) {
        super({ expr }, {}, lineno, tag);
    }

    compile(compiler: Compiler) {
        compiler
            .addDebugInfo(this)
            .write('')
            .subcompile(this.getNode('expr'))
            .raw(';\n');
    }
}
