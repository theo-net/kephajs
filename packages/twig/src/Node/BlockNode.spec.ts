/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../Compiler';
import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';
import { BlockNode } from './BlockNode';
import { TextNode } from './TextNode';

describe('@kephajs/twig/Node/BlockNode', () => {
    it('Should construct', () => {
        const body = new TextNode('foo', 1);
        const node = new BlockNode('foo', body, 1);

        expect(node.getNode('body')).to.be.equal(body);
        expect(node.getAttribute('name')).to.be.equal('foo');
    });

    it('Should compile the node', () => {
        const body = new TextNode('foo', 1);
        const node = new BlockNode('foo', body, 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);

        expect(compiler.getSource().trim()).to.be.equal(
            '// line 1\nblock_foo(context, blocks = {}) {\n    let macros = { ...this._macros };\n    this.print(`foo`);\n}'
        );
    });

    it('Sould escape special char in block name', () => {
        const body = new TextNode('foo', 1);
        const node = new BlockNode('§', body, 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);

        expect(compiler.getSource().trim()).to.be.equal(
            '// line 1\nblock_167(context, blocks = {}) {\n    let macros = { ...this._macros };\n    this.print(`foo`);\n}'
        );
    });

    it('Should escape special char', () => {
        expect(BlockNode.escapeBlockName('foobar')).to.be.equal('foobar');
        expect(BlockNode.escapeBlockName('$foo_bar')).to.be.equal('$foo_bar');
        expect(BlockNode.escapeBlockName('f123oo_bar')).to.be.equal(
            'f123oo_bar'
        );
        expect(BlockNode.escapeBlockName('f§§')).to.be.equal('f167167');
        expect(BlockNode.escapeBlockName('f§_@')).to.be.equal('f167_64');
        expect(BlockNode.escapeBlockName('123Foobar')).to.be.equal(
            '$123Foobar'
        );
    });
});
