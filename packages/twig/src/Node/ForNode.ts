/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { AbstractExpression } from './Expression/AbstractExpression';
import { AssignNameExpression } from './Expression/AssignNameExpression';
import { ForLoopNode } from './ForLoopNode';
import { TwigNode } from './TwigNode';

export class ForNode extends TwigNode {
    private _loop: ForLoopNode;

    constructor(
        keyTarget: AssignNameExpression,
        valueTarget: AssignNameExpression,
        seq: AbstractExpression,
        _ifexpr: null | TwigNode,
        body: TwigNode,
        elseNode: null | TwigNode,
        lineno: number,
        tag: null | string = null
    ) {
        const loop = new ForLoopNode(lineno, tag);
        body = new TwigNode([body, loop]);

        const nodes: Record<string, AbstractExpression> = {
            keyTarget,
            valueTarget,
            seq,
            body,
        };
        if (elseNode !== null) nodes.else = elseNode;

        super(nodes, { withLoop: true }, lineno, tag);
        this._loop = loop;
    }

    compile(compiler: Compiler) {
        compiler
            .addDebugInfo(this)
            .write('context = { ...context };\n')
            .write('context._parent = structuredClone(context);\n')
            .write('context._seq = ')
            .subcompile(this.getNode('seq'))
            .raw(';\n');

        if (this.hasNode('else'))
            compiler.write('context._iterated = false;\n');

        if (this.getAttribute('withLoop') === true) {
            compiler
                .write('context.loop = {\n')
                .write("    'parent': context._parent,\n")
                .write("    'index0': 0,\n")
                .write("    'index': 1,\n")
                .write("    'first': true,\n")
                .write('};\n')
                .write('if (\n')
                .indent()
                .write('Array.isArray(context._seq) ||\n')
                .write(
                    "(context._seq !== null && context._seq !== undefined && typeof context._seq.length === 'number') ||\n"
                )
                .write(
                    "(context._seq !== null && typeof context._seq === 'object' && typeof context._seq[Symbol.iterator] !== 'function')\n"
                )
                .outdent()
                .write(') {\n')
                .indent()
                .write(
                    'let length = context._seq.length ?? Object.keys(context._seq).length;\n'
                )
                .write('context.loop.revindex0 = length - 1;\n')
                .write('context.loop.revindex = length;\n')
                .write('context.loop.length = length;\n')
                .write('context.loop.last = 1 === length;\n')
                .outdent()
                .write('}\n');
        }

        this._loop.setAttribute('else', this.hasNode('else'));
        this._loop.setAttribute('withLoop', this.getAttribute('withLoop'));

        compiler
            .write(
                "this._env.getFunction('twigForEach').getCallable()(context['_seq'], ("
            )
            .raw(this.getNode('valueTarget').getAttribute('name') as string)
            .raw(', ')
            .raw(this.getNode('keyTarget').getAttribute('name') as string)
            .raw(') => {\n')
            .indent()
            .subcompile(this.getNode('valueTarget'))
            .raw(
                ' = ' + this.getNode('valueTarget').getAttribute('name') + ';\n'
            )
            .subcompile(this.getNode('keyTarget'))
            .raw(' = ' + this.getNode('keyTarget').getAttribute('name') + ';\n')
            .subcompile(this.getNode('body'))
            .outdent()
            .write('});\n');

        if (this.hasNode('else')) {
            compiler
                .write('if (!context._iterated) {\n')
                .indent()
                .subcompile(this.getNode('else'))
                .outdent()
                .write('}\n');
        }

        const parentVarName = compiler.getVarName();
        const newContextVarName = compiler.getVarName();
        compiler.write('const ' + parentVarName + ' = context._parent;\n');

        // remove some "private" loop variables (needed for nested loops)
        compiler
            .write('delete context.seq;\n')
            .write('delete context.iterated;\n')
            .write(
                "delete context['" +
                    this.getNode('keyTarget').getAttribute('name') +
                    "'];\n"
            )
            .write(
                "delete context['" +
                    this.getNode('valueTarget').getAttribute('name') +
                    "'];\n"
            )
            .write('delete context._parent;\n')
            .write('delete context.loop;\n');

        // keep the values set in the inner context for variables defined in the outer context
        compiler
            .write('const ' + newContextVarName + ' = {};\n')
            .write('for (const property in ' + parentVarName + ') {\n')
            .indent()
            .write(
                'if (context[property] !== undefined) ' +
                    newContextVarName +
                    '[property] = context[property];\n'
            )
            .write(
                'else ' +
                    newContextVarName +
                    '[property] = ' +
                    parentVarName +
                    '[property];\n'
            )
            .outdent()
            .write('}\n')
            .write('context = ' + newContextVarName + ';\n');

        return compiler;
    }
}
