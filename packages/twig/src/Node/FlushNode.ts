/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { TwigNode } from './TwigNode';

export class FlushNode extends TwigNode {
    constructor(lineno: number, tag: string) {
        super([], {}, lineno, tag);
    }

    compile(compiler: Compiler) {
        compiler.addDebugInfo(this).write('flush();\n');
    }
}
