/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../Compiler';
import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';
import { AssignNameExpression } from './Expression/AssignNameExpression';
import { NameExpression } from './Expression/NameExpression';
import { ForNode } from './ForNode';
import { PrintNode } from './PrintNode';
import { TwigNode } from './TwigNode';

describe('@kephajs/twig/Node/ForNode', () => {
    it('Should construct', () => {
        const keyTarget = new AssignNameExpression('key', 1);
        const valueTarget = new AssignNameExpression('item', 1);
        const seq = new NameExpression('items', 1);
        const body = new TwigNode(
            [new PrintNode(new NameExpression('foo', 1), 1)],
            {},
            1
        );
        let elseNode: TwigNode | null = null;
        let node = new ForNode(
            keyTarget,
            valueTarget,
            seq,
            null,
            body,
            elseNode,
            1
        );
        node.setAttribute('withLoop', false);

        expect(node.getNode('keyTarget')).to.be.equal(keyTarget);
        expect(node.getNode('valueTarget')).to.be.equal(valueTarget);
        expect(node.getNode('seq')).to.be.equal(seq);
        //expect(node.getNode('body')).to.be.equal(body);
        expect(node.hasNode('else')).to.be.equal(false);

        elseNode = new PrintNode(new NameExpression('foo', 1), 1);
        node = new ForNode(
            keyTarget,
            valueTarget,
            seq,
            null,
            body,
            elseNode,
            1
        );
        node.setAttribute('withLoop', false);
        expect(node.getNode('else')).to.be.equal(elseNode);
    });

    it('Should compile the node', () => {
        const compiler = new Compiler(new Environment(new RecordLoader()));

        let keyTarget = new AssignNameExpression('key', 1);
        let valueTarget = new AssignNameExpression('item', 1);
        let seq = new NameExpression('items', 1);
        let body = new TwigNode(
            [new PrintNode(new NameExpression('foo', 1), 1)],
            {},
            1
        );
        let elseNode: TwigNode | null = null;
        let node = new ForNode(
            keyTarget,
            valueTarget,
            seq,
            null,
            body,
            elseNode,
            1
        );
        node.setAttribute('withLoop', false);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(
            `// line 1
context = { ...context };
context._parent = structuredClone(context);
context._seq = (context[\`items\`] ?? undefined);
this._env.getFunction('twigForEach').getCallable()(context['_seq'], (item, key) => {
context[\`item\`] = item;
context[\`key\`] = key;
    this.print((context[\`foo\`] ?? undefined));
});
const __internal_compile_0 = context._parent;
delete context.seq;
delete context.iterated;
delete context['key'];
delete context['item'];
delete context._parent;
delete context.loop;
const __internal_compile_1 = {};
for (const property in __internal_compile_0) {
    if (context[property] !== undefined) __internal_compile_1[property] = context[property];
    else __internal_compile_1[property] = __internal_compile_0[property];
}
context = __internal_compile_1;`
        );

        keyTarget = new AssignNameExpression('k', 1);
        valueTarget = new AssignNameExpression('v', 1);
        seq = new NameExpression('values', 1);
        body = new TwigNode(
            [new PrintNode(new NameExpression('foo', 1), 1)],
            {},
            1
        );
        elseNode = null;
        node = new ForNode(
            keyTarget,
            valueTarget,
            seq,
            null,
            body,
            elseNode,
            1
        );
        node.setAttribute('withLoop', true);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(`// line 1
context = { ...context };
context._parent = structuredClone(context);
context._seq = (context[\`values\`] ?? undefined);
context.loop = {
    'parent': context._parent,
    'index0': 0,
    'index': 1,
    'first': true,
};
if (
    Array.isArray(context._seq) ||
    (context._seq !== null && context._seq !== undefined && typeof context._seq.length === 'number') ||
    (context._seq !== null && typeof context._seq === 'object' && typeof context._seq[Symbol.iterator] !== 'function')
) {
    let length = context._seq.length ?? Object.keys(context._seq).length;
    context.loop.revindex0 = length - 1;
    context.loop.revindex = length;
    context.loop.length = length;
    context.loop.last = 1 === length;
}
this._env.getFunction('twigForEach').getCallable()(context['_seq'], (v, k) => {
context[\`v\`] = v;
context[\`k\`] = k;
    this.print((context[\`foo\`] ?? undefined));
    context.loop.index0++;
    context.loop.index++;
    context.loop.first = false;
    if (context.loop.length !== undefined) {
        context.loop.revindex0--;
        context.loop.revindex--;
        context.loop.last = 0 === context.loop.revindex0;
    }
});
const __internal_compile_0 = context._parent;
delete context.seq;
delete context.iterated;
delete context['k'];
delete context['v'];
delete context._parent;
delete context.loop;
const __internal_compile_1 = {};
for (const property in __internal_compile_0) {
    if (context[property] !== undefined) __internal_compile_1[property] = context[property];
    else __internal_compile_1[property] = __internal_compile_0[property];
}
context = __internal_compile_1;`);

        keyTarget = new AssignNameExpression('k', 1);
        valueTarget = new AssignNameExpression('v', 1);
        seq = new NameExpression('values', 1);
        body = new TwigNode(
            [new PrintNode(new NameExpression('foo', 1), 1)],
            {},
            1
        );
        elseNode = null;
        node = new ForNode(
            keyTarget,
            valueTarget,
            seq,
            null,
            body,
            elseNode,
            1
        );
        node.setAttribute('withLoop', true);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(`// line 1
context = { ...context };
context._parent = structuredClone(context);
context._seq = (context[\`values\`] ?? undefined);
context.loop = {
    'parent': context._parent,
    'index0': 0,
    'index': 1,
    'first': true,
};
if (
    Array.isArray(context._seq) ||
    (context._seq !== null && context._seq !== undefined && typeof context._seq.length === 'number') ||
    (context._seq !== null && typeof context._seq === 'object' && typeof context._seq[Symbol.iterator] !== 'function')
) {
    let length = context._seq.length ?? Object.keys(context._seq).length;
    context.loop.revindex0 = length - 1;
    context.loop.revindex = length;
    context.loop.length = length;
    context.loop.last = 1 === length;
}
this._env.getFunction('twigForEach').getCallable()(context['_seq'], (v, k) => {
context[\`v\`] = v;
context[\`k\`] = k;
    this.print((context[\`foo\`] ?? undefined));
    context.loop.index0++;
    context.loop.index++;
    context.loop.first = false;
    if (context.loop.length !== undefined) {
        context.loop.revindex0--;
        context.loop.revindex--;
        context.loop.last = 0 === context.loop.revindex0;
    }
});
const __internal_compile_0 = context._parent;
delete context.seq;
delete context.iterated;
delete context['k'];
delete context['v'];
delete context._parent;
delete context.loop;
const __internal_compile_1 = {};
for (const property in __internal_compile_0) {
    if (context[property] !== undefined) __internal_compile_1[property] = context[property];
    else __internal_compile_1[property] = __internal_compile_0[property];
}
context = __internal_compile_1;`);

        keyTarget = new AssignNameExpression('k', 1);
        valueTarget = new AssignNameExpression('v', 1);
        seq = new NameExpression('values', 1);
        body = new TwigNode(
            [new PrintNode(new NameExpression('foo', 1), 1)],
            {},
            1
        );
        elseNode = new PrintNode(new NameExpression('foo', 1), 1);
        node = new ForNode(
            keyTarget,
            valueTarget,
            seq,
            null,
            body,
            elseNode,
            1
        );
        node.setAttribute('withLoop', true);
        compiler.compile(node);
        expect(compiler.getSource().trim()).to.be.equal(`// line 1
context = { ...context };
context._parent = structuredClone(context);
context._seq = (context[\`values\`] ?? undefined);
context._iterated = false;
context.loop = {
    'parent': context._parent,
    'index0': 0,
    'index': 1,
    'first': true,
};
if (
    Array.isArray(context._seq) ||
    (context._seq !== null && context._seq !== undefined && typeof context._seq.length === 'number') ||
    (context._seq !== null && typeof context._seq === 'object' && typeof context._seq[Symbol.iterator] !== 'function')
) {
    let length = context._seq.length ?? Object.keys(context._seq).length;
    context.loop.revindex0 = length - 1;
    context.loop.revindex = length;
    context.loop.length = length;
    context.loop.last = 1 === length;
}
this._env.getFunction('twigForEach').getCallable()(context['_seq'], (v, k) => {
context[\`v\`] = v;
context[\`k\`] = k;
    this.print((context[\`foo\`] ?? undefined));
    context['_iterated'] = true;
    context.loop.index0++;
    context.loop.index++;
    context.loop.first = false;
    if (context.loop.length !== undefined) {
        context.loop.revindex0--;
        context.loop.revindex--;
        context.loop.last = 0 === context.loop.revindex0;
    }
});
if (!context._iterated) {
    this.print((context[\`foo\`] ?? undefined));
}
const __internal_compile_0 = context._parent;
delete context.seq;
delete context.iterated;
delete context['k'];
delete context['v'];
delete context._parent;
delete context.loop;
const __internal_compile_1 = {};
for (const property in __internal_compile_0) {
    if (context[property] !== undefined) __internal_compile_1[property] = context[property];
    else __internal_compile_1[property] = __internal_compile_0[property];
}
context = __internal_compile_1;`);
    });
});
