/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { TwigNode } from './TwigNode';

export class SandboxNode extends TwigNode {
    constructor(body: TwigNode, lineno: number, tag: null | string = null) {
        super({ body }, {}, lineno, tag);
    }

    compile(compiler: Compiler) {
        compiler
            .addDebugInfo(this)
            .write('let alreadySandboxed = false;\n')
            .write('if (!alreadySandboxed = this.sandbox.isSandboxed())\n')
            .indent()
            .write('this.sandbox.enableSandbox();\n')
            .outdent()
            .write('try {\n')
            .indent()
            .subcompile(this.getNode('body'))
            .outdent()
            .write('} finally {\n')
            .indent()
            .write('if (!alreadySandboxed)\n')
            .indent()
            .write('this.sandbox.disableSandbox();\n')
            .outdent()
            .outdent()
            .write('}\n');
    }
}
