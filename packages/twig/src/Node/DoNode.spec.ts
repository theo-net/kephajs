/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../Compiler';
import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';
import { DoNode } from './DoNode';
import { ConstantExpression } from './Expression/ConstantExpression';

describe('@kephajs/twig/Node/DoNode', () => {
    it('Should construct', () => {
        const expr = new ConstantExpression('foo', 1);
        const node = new DoNode(expr, 1);

        expect(node.getNode('expr')).to.be.equal(expr);
    });

    it('Should compile the node', () => {
        const expr = new ConstantExpression('foo', 1);
        const node = new DoNode(expr, 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);

        expect(compiler.getSource().trim()).to.be.equal('// line 1\n`foo`;');
    });
});
