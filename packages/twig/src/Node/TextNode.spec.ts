/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Compiler } from '../Compiler';
import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';
import { TextNode } from './TextNode';

describe('@kephajs/twig/Node/TextNode', () => {
    it('Should construct', () => {
        const node = new TextNode('foo', 1);

        expect(node.getAttribute('data')).to.be.equal('foo');
    });

    it('Should compile the node', () => {
        const node = new TextNode('foo', 1);

        const compiler = new Compiler(new Environment(new RecordLoader()));
        compiler.compile(node);

        expect(compiler.getSource()).to.be.equal(
            '// line 1\nthis.print(`foo`);\n'
        );
    });
});
