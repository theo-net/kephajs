/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { objectKeys, varExport } from '@kephajs/core/Helpers/Helpers';
import { Compiler } from '../Compiler';
import { Source } from '../Source';

export class TwigNode implements Iterable<TwigNode> {
    protected _index = -1;

    protected _nodes: Record<string, TwigNode> = {};

    private _sourceContext: null | Source = null;

    constructor(
        nodes: Record<string, TwigNode> | TwigNode[] = {},
        protected _attributes: Record<string, unknown> = {},
        protected _lineno = 0,
        protected _tag: null | string = null
    ) {
        if (Array.isArray(nodes)) this.push(...nodes);
        else this._nodes = nodes;
    }

    *[Symbol.iterator](): IterableIterator<TwigNode> {
        const keys = Object.keys(this._nodes);
        for (let i = 0; i < keys.length; ++i) {
            yield this._nodes[keys[i]];
        }
    }

    push(...items: TwigNode[]) {
        items.forEach(item => {
            this._nodes[++this._index] = item;
        });
    }

    addNode(node: TwigNode, nodeName?: string) {
        if (nodeName !== undefined) this._nodes[nodeName] = node;
        else this._nodes[++this._index] = node;
    }

    get length() {
        return objectKeys(this._nodes).length;
    }

    toString() {
        const attributes: string[] = [];
        for (const nodeName in this._attributes) {
            attributes.push(
                nodeName +
                    ': ' +
                    varExport(this._attributes[nodeName]).replace(/\n/g, '')
            );
        }

        const repr = [this.constructor.name + '(' + attributes.join(', ')];

        if (this._nodes.length) {
            for (const nodeName in this._nodes) {
                const len = nodeName.length + 4;
                const noderepr: string[] = [];
                this._nodes[nodeName]
                    .toString()
                    .split('\n')
                    .forEach(line => {
                        noderepr.push(' '.repeat(len) + line);
                    });

                repr.push(
                    '  ' + nodeName + ': ' + noderepr.join('\n').trimStart()
                );
            }

            repr.push(')');
        } else repr[0] += ')';

        return repr.join('\n');
    }

    compile(compiler: Compiler) {
        objectKeys(this._nodes).forEach(key => {
            this._nodes[key].compile(compiler);
        });
    }

    getNodes() {
        return this._nodes;
    }

    getTemplateLine() {
        return this._lineno;
    }

    getNodeTag() {
        return this._tag;
    }

    hasAttribute(name: string) {
        return this._attributes[name] !== undefined;
    }

    getAttribute(attrName: string) {
        if (this._attributes[attrName] === undefined)
            throw new Error(
                `Attribute "${attrName}" does not exist for Node "${this.constructor.name}".`
            );

        return this._attributes[attrName];
    }

    setAttribute(attrName: string, value: unknown) {
        this._attributes[attrName] = value;
    }

    /*public function removeAttribute(string $name): void
    {
        unset(this._attributes[$name]);
    }*/

    hasNode(nodeName: string | number) {
        return this._nodes[nodeName] !== undefined;
    }

    getNode(nodeName: string | number) {
        if (this._nodes[nodeName] === undefined)
            throw new Error(
                `Node "${nodeName}" does not exist for Node "${this.constructor.name}".`
            );

        return this._nodes[nodeName];
    }

    setNode(nodeName: string | number, node: TwigNode) {
        this._nodes[nodeName] = node;
    }

    removeNode(nodeName: string | number) {
        delete this._nodes[nodeName];
    }

    getTemplateName() {
        return this._sourceContext ? this._sourceContext.name : null;
    }

    setSourceContext(source: Source) {
        this._sourceContext = source;
        for (const nodeName in this._nodes)
            this._nodes[nodeName].setSourceContext(source);
    }

    getSourceContext() {
        return this._sourceContext;
    }

    static clone(node: TwigNode) {
        const cloneNode = structuredClone(node);
        Object.setPrototypeOf(cloneNode, Object.getPrototypeOf(node));
        TwigNode.cloneInnerNodes(cloneNode, node);

        return cloneNode;
    }

    static cloneInnerNodes(node: TwigNode, orginalNode: TwigNode) {
        const nodeList = objectKeys(orginalNode.getNodes());
        nodeList.forEach(nodeName => {
            const newNode = structuredClone(orginalNode.getNode(nodeName));
            Object.setPrototypeOf(
                newNode,
                Object.getPrototypeOf(orginalNode.getNode(nodeName))
            );
            node.addNode(newNode, nodeName);
        });
        nodeList.forEach(nodeName => {
            TwigNode.cloneInnerNodes(
                node.getNode(nodeName),
                orginalNode.getNode(nodeName)
            );
        });
    }
}
