/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { AbstractExpression } from './Expression/AbstractExpression';
import { ConstantExpression } from './Expression/ConstantExpression';
import { TwigNode } from './TwigNode';

export class DeprecatedNode extends TwigNode {
    constructor(
        expr: AbstractExpression,
        lineno: number,
        tag: null | string = null
    ) {
        super({ expr }, {}, lineno, tag);
    }

    compile(compiler: Compiler) {
        compiler.addDebugInfo(this);

        const expr = this.getNode('expr');

        if (expr instanceof ConstantExpression) {
            compiler.write('@trigger_error(').subcompile(expr);
        } else {
            const varName = compiler.getVarName();
            compiler
                .write(varName + ' = ')
                .subcompile(expr)
                .raw(';\n')
                .write('@trigger_error(' + varName);
        }

        compiler
            .raw('.')
            .string(
                ` ("${this.getTemplateName()}" at line ${this.getTemplateLine()}).`
            )
            .raw(', E_USER_DEPRECATED);\n');
    }
}
