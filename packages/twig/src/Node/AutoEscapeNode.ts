/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { TwigNode } from './TwigNode';

export class AutoEscapeNode extends TwigNode {
    constructor(
        value: true | string,
        body: TwigNode,
        lineno: number,
        tag = 'autoescape'
    ) {
        super({ body }, { value }, lineno, tag);
    }

    compile(compiler: Compiler) {
        compiler.subcompile(this.getNode('body'));
    }
}
