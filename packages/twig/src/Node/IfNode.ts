/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { TwigNode } from './TwigNode';

export class IfNode extends TwigNode {
    constructor(
        tests: TwigNode,
        elseNode: null | TwigNode,
        lineno: number,
        tag: null | string = null
    ) {
        const nodes: Record<string, TwigNode> = { tests };
        if (elseNode !== null) {
            nodes.else = elseNode;
        }

        super(nodes, {}, lineno, tag);
    }

    compile(compiler: Compiler) {
        compiler.addDebugInfo(this);
        for (
            let i = 0, count = this.getNode('tests').length;
            i < count;
            i += 2
        ) {
            if (i > 0) {
                compiler.outdent().write('} else if (');
            } else compiler.write('if (');

            compiler
                .subcompile(this.getNode('tests').getNode(i))
                .raw(') {\n')
                .indent()
                .subcompile(this.getNode('tests').getNode(i + 1));
        }

        if (this.hasNode('else')) {
            compiler
                .outdent()
                .write('} else {\n')
                .indent()
                .subcompile(this.getNode('else'));
        }

        compiler.outdent().write('}\n');
    }
}
