/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Compiler } from '../Compiler';
import { TwigNode } from './TwigNode';

export class BlockNode extends TwigNode {
    constructor(
        blockName: string,
        body: TwigNode,
        lineno: number,
        tag: null | string = null
    ) {
        super({ body }, { name: blockName }, lineno, tag);
    }

    compile(compiler: Compiler) {
        compiler
            .addDebugInfo(this)
            .write(
                `block_${BlockNode.escapeBlockName(
                    this.getAttribute('name') as string
                )}(context, blocks = {}) {\n`
            )
            .indent()
            .write('let macros = { ...this._macros };\n');

        compiler.subcompile(this.getNode('body')).outdent().write('}\n\n');
    }

    static escapeBlockName(blockName: string) {
        if (/^[A-Za-z_$][\w$]*$/.test(blockName)) return blockName;
        else {
            let escapedBlockName = '';
            let first = true;
            for (let i = 0; i < blockName.length; i++) {
                if (first && /[0-9]/.test(blockName[i]))
                    escapedBlockName += '$' + blockName[i];
                else {
                    escapedBlockName += /[\w$]/.test(blockName[i])
                        ? blockName[i]
                        : blockName.charCodeAt(i);
                }
                first = false;
            }
            return escapedBlockName;
        }
    }
}
