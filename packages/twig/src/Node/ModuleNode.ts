/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { objectKeys, varExport } from '@kephajs/core/Helpers/Helpers';
import { CacheExportType } from '../Cache/CacheInterface';
import { Compiler } from '../Compiler';
import { Source } from '../Source';
import { BlockNode } from './BlockNode';
import { BlockReferenceNode } from './BlockReferenceNode';
import { BodyNode } from './BodyNode';
import { AbstractExpression } from './Expression/AbstractExpression';
import { ConstantExpression } from './Expression/ConstantExpression';
import { TextNode } from './TextNode';
import { TwigNode } from './TwigNode';

export class ModuleNode extends TwigNode {
    constructor(
        private _exportType: CacheExportType,
        body: TwigNode,
        parent: null | AbstractExpression,
        blocks: TwigNode,
        macros: TwigNode,
        traits: TwigNode,
        embeddedTemplates: TwigNode[],
        source: Source
    ) {
        const nodes: Record<string, TwigNode> = {
            body: body,
            blocks: blocks,
            macros: macros,
            traits: traits,
            renderStart: new TwigNode(),
            renderEnd: new TwigNode(),
            constructorStart: new TwigNode(),
            constructorEnd: new TwigNode(),
            classEnd: new TwigNode(),
        };
        if (parent !== null) nodes.parent = parent;

        // embedded templates are set as attributes so that they are only visited once by the visitors
        super(
            nodes,
            {
                index: null,
                embeddedTemplates,
            },
            1
        );

        // populate the template name of all node children
        this.setSourceContext(source);
    }

    setIndex(index: number) {
        this.setAttribute('index', index);
    }

    compile(compiler: Compiler) {
        this._compileTemplate(compiler);

        const env = compiler.getEnvironment();
        const cache = env.getCache(false);
        (this.getAttribute('embeddedTemplates') as ModuleNode[]).forEach(
            template => {
                const embedCompiler = new Compiler(env);
                cache.write(
                    cache.generateKey(
                        (this.getSourceContext() as Source).name.replace(
                            /\*\//g,
                            '* /'
                        ),
                        env.getTemplateClass(
                            (this.getSourceContext() as Source).name,
                            template.hasAttribute('index')
                                ? (template.getAttribute('index') as number)
                                : null
                        )
                    ),
                    embedCompiler.compile(template).getSource()
                );
            }
        );
    }

    _compileTemplate(compiler: Compiler) {
        this._compileClassHeader(compiler);

        this._compileConstructor(compiler);

        this._compileGetParent(compiler);

        this._compileRender(compiler);

        compiler.subcompile(this.getNode('blocks'));

        this._compileMacros(compiler);

        this._compileGetTemplateName(compiler);

        this._compileIsTraitable(compiler);

        this._compileDebugInfo(compiler);

        this._compileGetSourceContext(compiler);

        this._compileClassFooter(compiler);
    }

    _compileGetParent(compiler: Compiler) {
        if (!this.hasNode('parent')) return;
        const parent = this.getNode('parent');

        compiler
            .write('_doGetParent(context) {\n')
            .indent()
            .addDebugInfo(parent)
            .write('return ');

        if (parent instanceof ConstantExpression) compiler.subcompile(parent);
        else {
            compiler
                .raw('this._loadTemplate(')
                .subcompile(parent)
                .raw(', ')
                .repr((this.getSourceContext() as Source).name)
                .raw(', ')
                .repr(parent.getTemplateLine())
                .raw(')');
        }

        compiler.raw(';\n').outdent().write('}\n\n');
    }

    _compileClassHeader(compiler: Compiler) {
        compiler.write('\n\n');
        compiler
            // if the template name contains */, add a blank to avoid a Javascript parse error
            .write(
                '/* ' +
                    (this.getSourceContext() as Source).name.replace(
                        /\*\//g,
                        '* /'
                    ) +
                    ' */\n'
            )
            .write(
                (this._exportType === 'export'
                    ? 'module.exports.default = '
                    : '(') +
                    'class ' +
                    compiler
                        .getEnvironment()
                        .getTemplateClass(
                            (this.getSourceContext() as Source).name,
                            this.getAttribute('index') as number
                        )
            )
            .write(' {\n')
            .indent();
    }

    _compileConstructor(compiler: Compiler) {
        compiler
            .write('constructor(env, prototypes) {\n')
            .indent()
            .write('this._env = env;\n', 'this._prototypes = prototypes;\n')
            .write('this._extensions = env.getExtensions();\n')
            .subcompile(this.getNode('constructorStart'))
            .write('let macros = this._macros = {};\n')
            .write('this._internalVars = {};\n')
            .write('this._outputStack = [];\n\n')
            .write('this._source = this.getSourceContext();\n\n');

        this._compileConstructorPropertiesInit(compiler);

        // parent
        if (!this.hasNode('parent'))
            compiler.write('this._parent = false;\n\n');

        const countTraits = this.getNode('traits').length;
        if (countTraits) {
            const traits = this.getNode('traits').getNodes();
            objectKeys(traits).forEach(i => {
                const node = traits[i].getNode('template');

                compiler
                    .addDebugInfo(node)
                    .write('let _trait_' + i + ' = this._loadTemplate(')
                    .subcompile(node)
                    .raw(', ')
                    .repr(node.getTemplateName())
                    .raw(', ')
                    .repr(node.getTemplateLine())
                    .raw(');\n')
                    .write('if (!_trait_' + i + '.isTraitable()) {\n')
                    .indent()
                    .write(
                        'throw new this._prototypes.RuntimeError("Template " +'
                    )
                    .subcompile(traits[i].getNode('template'))
                    .raw(' + " cannot be used as a trait.", ')
                    .repr(node.getTemplateLine())
                    .raw(', this._source);\n')
                    .outdent()
                    .write('}\n')
                    .write(
                        'let _trait_' +
                            i +
                            '_blocks = _trait_' +
                            i +
                            '.getBlocks();\n\n'
                    );

                const targets = traits[i].getNode('targets').getNodes();
                objectKeys(targets).forEach(key => {
                    compiler
                        .write('if (_trait_' + i + '_blocks[')
                        .string(key)
                        .raw('] === undefined) {\n')
                        .indent()
                        .write(
                            "throw new this._prototypes.RuntimeError('Block "
                        )
                        .string(key, '"')
                        .raw(' is not defined in trait ')
                        .subcompile(traits[i].getNode('template'))
                        .raw(".', ")
                        .repr(node.getTemplateLine())
                        .raw(', this._source);\n')
                        .outdent()
                        .write('}\n\n')

                        .write('_trait_' + i + '_blocks[')
                        .subcompile(targets[key])
                        .raw('] = _trait_' + i + '_blocks[')
                        .string(key)
                        .raw(']; delete _trait_' + i + '_blocks[')
                        .string(key)
                        .raw('];\n\n');
                });
            });

            if (countTraits > 1) {
                compiler.write('this._traits = {\n').indent();

                for (let i = 0; i < countTraits; ++i) {
                    compiler.write(
                        '..._trait_' +
                            i +
                            '_blocks' +
                            (i === countTraits - 1 ? '' : ',') +
                            '\n'
                    );
                }

                compiler.outdent().write('};\n\n');
            } else compiler.write('this._traits = _trait_0_blocks;\n\n');

            compiler
                .write('this._blocks = {\n')
                .indent()
                .write('...this._traits,\n')
                .write('...{\n');
        } else {
            compiler.write('this._traits = {};\n').write('this._blocks = {\n');
        }

        // blocks
        compiler.indent();

        const blocks = this.getNode('blocks').getNodes();
        objectKeys(blocks).forEach(blockName => {
            compiler.write(
                "'" +
                    blockName +
                    "': [this, 'block_" +
                    BlockNode.escapeBlockName(blockName) +
                    "'],\n"
            );
        });

        if (countTraits)
            compiler.outdent().write('}\n').outdent().write('};\n');
        else compiler.outdent().write('};\n');

        compiler
            .subcompile(this.getNode('constructorEnd'))
            .raw('\n')
            .write('return this;\n')
            .outdent()
            .write('}\n\n');
    }

    _compileConstructorPropertiesInit(compiler: Compiler) {
        compiler.write('this._parent = null;\n').write('this._parents = {};\n');
    }

    _compileRender(compiler: Compiler) {
        compiler
            .write('_doRender(context, blocks = {}) {\n')
            .indent()
            .write('let macros = { ...this._macros };\n')
            .subcompile(this.getNode('renderStart'))
            .subcompile(this.getNode('body'));

        if (this.hasNode('parent')) {
            const parent = this.getNode('parent');

            compiler.addDebugInfo(parent);
            if (parent instanceof ConstantExpression) {
                compiler
                    .write('this._parent = this._loadTemplate(')
                    .subcompile(parent)
                    .raw(', ')
                    .repr((this.getSourceContext() as Source).name)
                    .raw(', ')
                    .repr(parent.getTemplateLine())
                    .raw(');\n');
                compiler.write('this.print(this._parent');
            } else {
                compiler.write('this.print(this.getParent(context)');
            }
            compiler.raw(
                '.render(context, { ...this._blocks, ...blocks }));\n'
            );
        }

        compiler
            .subcompile(this.getNode('renderEnd'))
            .write('return this._output;\n')
            .outdent()
            .write('}\n\n');
    }

    _compileClassFooter(compiler: Compiler) {
        compiler
            .subcompile(this.getNode('classEnd'))
            .outdent()
            .write('}' + (this._exportType === 'export' ? '' : ')') + '\n');
    }

    _compileMacros(compiler: Compiler) {
        compiler.subcompile(this.getNode('macros'));
    }

    _compileGetTemplateName(compiler: Compiler) {
        compiler
            .write('getTemplateName() {\n')
            .indent()
            .write('return ')
            .repr((this.getSourceContext() as Source).name)
            .raw(';\n')
            .outdent()
            .write('}\n\n');
    }

    _compileIsTraitable(compiler: Compiler) {
        // A template can be used as a trait if:
        //   * it has no parent
        //   * it has no macros
        //   * it has no body
        //
        // Put another way, a template can be used as a trait if it
        // only contains blocks and use statements.
        let traitable =
            !this.hasNode('parent') && 0 === this.getNode('macros').length;
        let nodes: TwigNode;
        if (traitable) {
            if (this.getNode('body') instanceof BodyNode)
                nodes = this.getNode('body').getNode(0);
            else nodes = this.getNode('body');

            if (!nodes.length) nodes = new TwigNode([nodes]);

            const nodes2 = nodes.getNodes();
            objectKeys(nodes2).forEach(index => {
                if (!nodes2[index].length) return;

                if (
                    nodes2[index] instanceof TextNode &&
                    /^\s+$/.test(nodes2[index].getAttribute('data') as string)
                )
                    return;

                if (nodes2[index] instanceof BlockReferenceNode) return;

                traitable = false;
            });
        }

        if (traitable) return;

        compiler
            .write('isTraitable() {\n')
            .indent()
            .write('return ' + (traitable ? 'true' : 'false') + ';\n')
            .outdent()
            .write('}\n\n');
    }

    _compileDebugInfo(compiler: Compiler) {
        const debugInfo: Record<number, number> = {};
        objectKeys(compiler.getDebugInfo())
            .sort()
            .forEach(line => {
                debugInfo[Number(line)] = compiler.getDebugInfo()[Number(line)];
            });
        compiler
            .write('getDebugInfo() {\n')
            .indent()
            .write(
                'return ' +
                    varExport(debugInfo)
                        .replace('Object', '')
                        .replace(/\n/g, '') +
                    ';\n'
            )
            .outdent()
            .write('}\n\n');
    }

    _compileGetSourceContext(compiler: Compiler) {
        compiler
            .write('getSourceContext() {\n')
            .indent()
            .write('return new this._prototypes.Source(')
            .string(
                compiler.getEnvironment().isDebug()
                    ? (this.getSourceContext() as Source).code
                    : ''
            )
            .raw(', ')
            .string((this.getSourceContext() as Source).name)
            .raw(', ')
            .string((this.getSourceContext() as Source).path)
            .raw(');\n')
            .outdent()
            .write('}\n');
    }

    _compileLoadTemplate(compiler: Compiler, node: TwigNode, template: string) {
        if (node instanceof ConstantExpression) {
            compiler
                .write(template + ' = this._loadTemplate(')
                .subcompile(node)
                .raw(', ')
                .repr(node.getTemplateName())
                .raw(', ')
                .repr(node.getTemplateLine())
                .raw(');\n');
        } else throw new Error('Trait templates can only be constant nodes.');
    }
}
