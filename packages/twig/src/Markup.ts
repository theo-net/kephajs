/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

export class Markup {
    private _content: string;

    constructor(content: string) {
        this._content = '' + content;
    }

    toString() {
        return this._content;
    }

    toJSON() {
        return this._content;
    }

    get length() {
        return this._content.length;
    }
}
