/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import fs from 'node:fs';
import os from 'os';
import path from 'node:path';

import { expect } from 'chai';

import { rmrf } from '@kephajs/console/Helpers/Filesystem';
import { FilesystemCache } from './FilesystemCache';
import { CacheError } from './CacheError';
import { Template } from '../Template';
import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';

describe('@kephajs/twig/Cache/FilesystemCache', () => {
    let directory: string;
    let cache: FilesystemCache;
    let generatedSource: string;

    beforeEach(() => {
        generatedSource = 'module.exports.default = class MyTemplate {}\n';
        directory = os.tmpdir + path.sep + 'twig-cache-filesystemcache-tests';
        fs.mkdirSync(directory);
        cache = new FilesystemCache(directory);
    });

    afterEach(async () => {
        await rmrf(directory);
    });

    describe('generateKey', () => {
        it('Should generate a key', () => {
            expect(
                RegExp(
                    '^' +
                        directory +
                        '\\/(?<subdir>[a-f0-9]{2})\\/\\k<subdir>[a-f0-9]{30}\\.js$'
                ).test(cache.generateKey('foo', 'bar'))
            ).to.be.equal(true);
        });

        it('Should generate the same key for the same name', () => {
            expect(cache.generateKey('foo', 'bar')).to.be.equal(
                cache.generateKey('foo', 'bar')
            );
        });

        it('Should generate different keys for the different names and class name', () => {
            expect(cache.generateKey('foo', 'foo')).to.be.not.equal(
                cache.generateKey('bar', 'foo')
            );
            expect(cache.generateKey('foo', 'bar')).to.be.not.equal(
                cache.generateKey('foo', 'foo')
            );
            expect(cache.generateKey('foo', 'bar')).to.be.not.equal(
                cache.generateKey('bar', 'foo')
            );
        });
    });

    describe('load', () => {
        it('Should return the template class from the cache file', () => {
            const key = path.join(directory, 'cache', 'cachefile.js');
            fs.mkdirSync(path.join(directory, 'cache'), { recursive: true });
            fs.writeFileSync(key, generatedSource);

            const template = cache.load(key);

            expect(template).to.be.not.equal(null);
            expect(
                new (template as typeof Template)(
                    new Environment(new RecordLoader())
                )
            ).to.be.instanceOf(Template);
        });

        it('Should return null if the cache file is missing', () => {
            const key = path.join(directory, 'cache', 'cachefile.js');
            fs.mkdirSync(path.join(directory, 'cache'), { recursive: true });

            expect(cache.load(key)).to.be.equal(null);
        });
    });

    describe('write', () => {
        it('Should write a cache file', () => {
            const key = path.join(directory, 'cache', 'cachefile.js');

            expect(fs.existsSync(key)).to.be.equal(false);

            cache.write(key, generatedSource);

            expect(fs.existsSync(key)).to.be.equal(true);
            expect(fs.readFileSync(key).toString()).to.be.equal(
                generatedSource
            );
        });

        it('Should throw Error if fail mkdir', () => {
            const key = path.join(directory, 'cache', 'cachefile.js');
            fs.chmodSync(directory, 0o555);

            try {
                cache.write(key, generatedSource);
            } catch (error) {
                expect(error).to.be.instanceOf(Error);
                expect((error as Error).message).to.contain(
                    'Unable to create the cache directory'
                );
                return;
            }
            try {
                fs.accessSync(directory, fs.constants.R_OK | fs.constants.W_OK);
            } catch (error) {
                expect.fail('Should have thrown');
            }
        });

        it('Should throw Error if fail write inside a directory', () => {
            const key = path.join(directory, 'cache', 'cachefile.js');
            fs.mkdirSync(path.join(directory, 'cache'), 0o555);

            try {
                cache.write(key, generatedSource);
            } catch (error) {
                expect(error).to.be.instanceOf(CacheError);
                expect((error as CacheError).message).to.contain(
                    'Unable to write in the cache directory'
                );
                return;
            }
            try {
                fs.accessSync(directory, fs.constants.R_OK | fs.constants.W_OK);
            } catch (error) {
                expect.fail('Should have thrown');
            }
        });

        it('Should throw Error if fail write inside a writable directory', () => {
            const key = path.join(directory, 'cache', 'cachefile.js');
            fs.mkdirSync(path.join(directory, 'cache'), { recursive: true });
            fs.writeFileSync(key, 'foobar', { mode: 0o555 });

            try {
                cache.write(key, generatedSource);
            } catch (error) {
                expect(error).to.be.instanceOf(CacheError);
                expect((error as CacheError).message).to.contain(
                    'Failed to write cache file'
                );
                return;
            }
            try {
                fs.accessSync(key, fs.constants.R_OK | fs.constants.W_OK);
            } catch (error) {
                expect.fail('Should have thrown');
            }
        });
    });

    describe('getTimestamp', () => {
        it('Should get the timestamp', () => {
            const key = path.join(directory, 'cache', 'cachefile.js');
            fs.mkdirSync(path.join(directory, 'cache'), { recursive: true });
            fs.writeFileSync(key, 'foobar');
            fs.utimesSync(key, 9876543210, 1234567890);
            expect(cache.getTimestamp(key)).to.be.equal(1234567890000);
        });

        it('Should return 0 if the cache file is missing', () => {
            const key = path.join(directory, 'cache', 'cachefile.js');
            expect(cache.getTimestamp(key)).to.be.equal(0);
        });
    });
});
