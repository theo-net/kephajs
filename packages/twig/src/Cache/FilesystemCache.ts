/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import fs from 'fs';
import path from 'path';

import { XXHash128 } from 'xxhash-addon';

import { CacheError } from './CacheError';
import { CacheExportType, CacheInterface } from './CacheInterface';
import { Template } from '../Template';

/**
 * Implements a cache on the filesystem.
 */
export class FilesystemCache implements CacheInterface {
    public readonly exportType: CacheExportType = 'export';

    private _directory;

    constructor(directory: string) {
        this._directory = directory.replace(/\/+$/, '') + '/';
    }

    getDirectory() {
        return this._directory;
    }

    generateKey(name: string, className: string) {
        const hash =
            XXHash128.hash(Buffer.from(name + className)).toString('hex') + '';

        return this._directory + hash[0] + hash[1] + '/' + hash + '.js';
    }

    load(key: string) {
        try {
            const stat = fs.statSync(key);
            if (stat.isFile()) {
                // eslint-disable-next-line @typescript-eslint/no-var-requires
                const template = require(key).default;
                Object.setPrototypeOf(template.prototype, Template.prototype);
                Object.setPrototypeOf(template, Template);
                return template;
            } else throw Error();
        } catch (error) {
            return null;
        }
    }

    write(key: string, content: string) {
        const dir = path.dirname(key);

        try {
            if (!fs.existsSync(dir)) fs.mkdirSync(dir);
            else if (!fs.statSync(dir).isDirectory()) throw new CacheError();

            try {
                fs.accessSync(dir, fs.constants.W_OK);
                try {
                    fs.writeFileSync(key, content);
                } catch {
                    throw new CacheError(
                        `Failed to write cache file "${key}".`
                    );
                }
            } catch (error) {
                if (!(error instanceof CacheError))
                    throw new CacheError(
                        `Unable to write in the cache directory (${dir}).`
                    );
                else throw error;
            }
        } catch (error) {
            if (!(error instanceof CacheError))
                throw new CacheError(
                    `Twig: Unable to create the cache directory (${dir}).`
                );
            else throw error;
        }
    }

    has(key: string) {
        return fs.existsSync(key);
    }

    getTimestamp(key: string) {
        try {
            const stat = fs.statSync(key);
            if (stat.isFile()) {
                return stat.mtime.getTime();
            } else return 0;
        } catch (error) {
            return 0;
        }
    }
}
