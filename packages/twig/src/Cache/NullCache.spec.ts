/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Template } from '../Template';
import { Environment } from '../Environment';
import { RecordLoader } from '../Loader/RecordLoader';
import { NullCache } from './NullCache';

describe('@kephajs/twig/Cache/NullCache', () => {
    let cache: NullCache;
    let generatedSource: string;

    beforeEach(() => {
        generatedSource = '(class MyTemplate {})\n';
        cache = new NullCache();
    });

    describe('generateKey', () => {
        it('Should generate the same key for the same name', () => {
            expect(cache.generateKey('foo', 'bar')).to.be.equal(
                cache.generateKey('foo', 'bar')
            );
        });

        it('Should generate different keys for the different names and class name', () => {
            expect(cache.generateKey('foo', 'foo')).to.be.not.equal(
                cache.generateKey('bar', 'foo')
            );
            expect(cache.generateKey('foo', 'bar')).to.be.not.equal(
                cache.generateKey('foo', 'foo')
            );
            expect(cache.generateKey('foo', 'bar')).to.be.not.equal(
                cache.generateKey('bar', 'foo')
            );
        });
    });

    describe('load', () => {
        it('Should return the template class from the cache file', () => {
            const key = 'foobar';
            cache.write(key, generatedSource);
            const template = cache.load(key);

            expect(template).to.be.not.equal(null);
            expect(
                new (template as typeof Template)(
                    new Environment(new RecordLoader())
                )
            ).to.be.instanceOf(Template);
        });

        it('Should return null if the cache file is missing', () => {
            expect(cache.load('foobar')).to.be.equal(null);
        });
    });

    describe('getTimestamp', () => {
        it('Should return 0', () => {
            expect(cache.getTimestamp('foobar')).to.be.equal(0);
        });
    });
});
