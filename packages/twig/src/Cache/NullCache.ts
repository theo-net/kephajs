/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Template } from '../Template';
import { CacheExportType, CacheInterface } from './CacheInterface';

/**
 * Implements a no-cache strategy.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
export class NullCache implements CacheInterface {
    public readonly exportType: CacheExportType = 'eval';

    private _generatedTemplates: Record<string, string> = {};

    generateKey(name: string, className: string) {
        return name + '_' + className;
    }

    write(key: string, content: string) {
        this._generatedTemplates[key] = content;
    }

    load(key: string) {
        try {
            if (this._generatedTemplates[key] === undefined) throw new Error();
            const template = eval(this._generatedTemplates[key]);
            Object.setPrototypeOf(template.prototype, Template.prototype);
            Object.setPrototypeOf(template, Template);
            return template;
        } catch (error) {
            return null;
        }
    }

    has(key: string) {
        return this._generatedTemplates[key] !== undefined;
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    getTimestamp(_key: string) {
        return 0;
    }
}
