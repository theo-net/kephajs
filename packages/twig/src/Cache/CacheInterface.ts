/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Template } from '../Template';

export type CacheExportType = 'export' | 'eval';

export interface CacheInterface {
    readonly exportType: CacheExportType;

    generateKey(name: string, className: string): string;

    /**
     * Writes the compiled template to cache.
     *
     * @param string $content The template representation as a PHP class
     */
    write(key: string, content: string): void;

    /**
     * Loads a template from the cache.
     */
    load(key: string): null | typeof Template;

    has(key: string): boolean;

    /**
     * Returns the modification timestamp of a key.
     */
    getTimestamp(key: string): number;
}
