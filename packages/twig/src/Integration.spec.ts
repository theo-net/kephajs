/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

//import { realpathSync } from 'fs';
//import { sep } from 'path';

import { doIntegrationTest, getTests } from './IntegrationHelpers';

//const FIXTURES_DIR = realpathSync(__dirname + sep + '..' + sep + 'Fixtures');

describe('@kephajs/twig/Integration', () => {
    describe('Fixtures test', () => {
        getTests().forEach(test => {
            if (!test[5]) it.skip(test[0] + ' ' + test[1]);
            if (test[2]) {
                const ret = eval(test[2]);
                if (!ret)
                    it.skip(test[0] + ' ' + test[1] + ' (' + test[2] + ')');
            }

            it(test[0] + ' ' + test[1], () => {
                doIntegrationTest(...test);
            });
        });
    });

    /*describe('One test', () => {
        getTests(
            false,
            //FIXTURES_DIR + sep + 'tags/embed/nested.test'
            FIXTURES_DIR + sep + 'exceptions/unclosed_tag.test'
        ).forEach(test => {
            console.log(test);
            if (!test[5]) it.skip(test[0] + ' ' + test[1]);
            if (test[2]) {
                const ret = eval(test[2]);
                if (!ret)
                    it.skip(test[0] + ' ' + test[1] + ' (' + test[2] + ')');
            }

            it(test[0] + ' ' + test[1], () => {
                doIntegrationTest(...test);
            });
        });
    });*/
});
