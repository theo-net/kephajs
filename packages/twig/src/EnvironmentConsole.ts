/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { CacheInterface } from './Cache/CacheInterface';
import { FilesystemCache } from './Cache/FilesystemCache';
import { Environment } from './Environment';

export class EnvironmentConsole extends Environment {
    setCache(cache: string | boolean | CacheInterface) {
        this._originalCache = cache;
        if (typeof cache === 'string') this._cache = new FilesystemCache(cache);
        else super.setCache(cache);
    }
}
