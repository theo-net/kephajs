/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

// eslint-disable-next-line import/no-extraneous-dependencies
import { expect } from 'chai';

import { readdirSync, readFileSync, realpathSync, statSync } from 'fs';
import { sep } from 'path';

import { objectKeys } from '@kephajs/core/Helpers/Helpers';
import { Environment } from './Environment';
import { AbstractExtension } from './Extension/AbstractExtension';
import { SandboxExtension } from './Extension/SandboxExtension';
import { RecordLoader } from './Loader/RecordLoader';
import { SecurityPolicy } from './Sandbox/SecurityPolicy';
import { AbstractTokenParser } from './TokenParser/AbstractTokenParser';
import { Token } from './Token';
import { PrintNode } from './Node/PrintNode';
import { ConstantExpression } from './Node/Expression/ConstantExpression';
import { TwigFilter, TwigFilterType } from './TwigFilter';
import { TwigFunction, TwigFunctionType } from './TwigFunction';
import { TwigTest } from './TwigTest';
import { TemplateWrapper } from './TemplateWrapper';
import { Template } from './Template';
import { RuntimeError } from './Error/RuntimeError';
import { Markup as MarkupInclude } from './Markup';
const FIXTURES_DIR = realpathSync(__dirname + sep + '..' + sep + 'Fixtures');

class TwigTestTokenParser extends AbstractTokenParser {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    parse(_token: Token) {
        this._parser.getStream().expect(Token.BLOCK_END_TYPE);

        return new PrintNode(new ConstantExpression('§', -1), -1);
    }

    getTag() {
        return '§';
    }
}

class TwigTestExtension extends AbstractExtension {
    getTokenParsers() {
        return { 0: new TwigTestTokenParser() };
    }

    getFilters() {
        return {
            '§': new TwigFilter('§', this.parFilter as TwigFilterType),
            //new TwigFilter('escape_and_nl2br', this.escape_and_nl2br, { needsEnvironment: true, isSafe: ['html'] }),
            nl2br: new TwigFilter('nl2br', this.nl2br as TwigFilterType, {
                preEscape: 'html',
                isSafe: ['html'],
            }),
            escape_something: new TwigFilter(
                'escape_something',
                this.escape_something as TwigFilterType,
                {
                    isSafe: ['something'],
                }
            ),
            preserves_safety: new TwigFilter(
                'preserves_safety',
                this.preserves_safety as TwigFilterType,
                {
                    preservesSafety: ['html'],
                }
            ),
            static_call_string: new TwigFilter(
                'static_call_string',
                TwigTestExtension.staticCall as TwigFilterType
            ),
            '*_path': new TwigFilter(
                '*_path',
                this.dynamic_path as TwigFilterType
            ),
            '*_foo_*_bar': new TwigFilter(
                '*_foo_*_bar',
                this.dynamic_foo as TwigFilterType
            ),
            not: new TwigFilter('not', this.notFilter as TwigFilterType),
            anon_foo: new TwigFilter('anon_foo', function (name) {
                return '*' + name + '*';
            }),
        };
    }

    getFunctions() {
        return {
            '§': new TwigFunction('§', this.parFunction as TwigFunctionType),
            safe_br: new TwigFunction('safe_br', this.br, {
                isSafe: ['html'],
            }),
            unsafe_br: new TwigFunction('unsafe_br', this.br),
            static_call_string: new TwigFunction(
                'static_call_string',
                TwigTestExtension.staticCall as TwigFunctionType
            ),
            '*_path': new TwigFunction(
                '*_path',
                this.dynamic_path as TwigFunctionType
            ),
            '*_foo_*_bar': new TwigFunction(
                '*_foo_*_bar',
                this.dynamic_foo as TwigFunctionType
            ),
            anon_foo: new TwigFunction('anon_foo', function (name: string) {
                return '*' + name + '*';
            } as TwigFunctionType),
        };
    }

    getTests() {
        return {
            'multi word': new TwigTest(
                'multi word',
                this.is_multi_word as (...args: unknown[]) => boolean
            ),
            'test_*': new TwigTest('test_*', this.dynamic_test),
        };
    }

    notFilter(value: string) {
        return 'not ' + value;
    }

    parFilter(value: string) {
        return `§${value}§`;
    }

    parFunction(value: string) {
        return `§${value}§`;
    }

    /**
     * nl2br which also escapes, for testing escaper filters.
     */
    /*escape_and_nl2br(env, value, separator = '<br />') {
            return this.nl2br(twig_escape_filter(env, value, 'html'), separator);
        }*/

    /**
     * nl2br only, for testing filters with pre_escape.
     */
    nl2br(value: string, separator = '<br />') {
        // not secure if value contains html tags (not only entities)
        // don't use
        return (value ?? '').replace(/\n/g, separator + '\n');
    }

    dynamic_path(element: string, item: string) {
        return element + '/' + item;
    }

    dynamic_foo(foo: string, bar: string, item: string) {
        return foo + '/' + bar + '/' + item;
    }

    dynamic_test(element: unknown, item: unknown) {
        return element === item;
    }

    escape_something(value: string) {
        return value.toUpperCase();
    }

    preserves_safety(value: string) {
        return value.toUpperCase();
    }

    static staticCall(value: string) {
        return `*${value}*`;
    }

    br() {
        return '<br />';
    }

    is_multi_word(value: string) {
        return value.indexOf(' ') > -1;
    }
}

class TwigTestFoo implements Iterable<number> {
    public BAR_NAME = 'bar';

    public position = 0;

    public array = [1, 2];

    [Symbol.iterator]() {
        let counter = 0;
        const values = this.array;

        return {
            next() {
                return {
                    done: counter >= values.length,
                    value: values[counter++],
                };
            },
        };
    }

    bar(param1: string | null = null, param2: string | null = null) {
        return (
            'bar' + (param1 ? '_' + param1 : '') + (param2 ? '-' + param2 : '')
        );
    }

    getFoo() {
        return 'foo';
    }

    getSelf() {
        return this;
    }

    is() {
        return 'is';
    }

    in() {
        return 'in';
    }

    not() {
        return 'not';
    }

    strToLower(value: string) {
        return value.toLowerCase();
    }

    key() {
        return 'a';
    }

    valid() {
        return this.array[this.position] !== undefined;
    }
}

class ArrayIterator<T> implements Iterable<T> {
    constructor(private _values: T[]) {}

    [Symbol.iterator]() {
        let counter = 0;
        const values = this._values;

        return {
            next() {
                return {
                    done: counter >= values.length,
                    value: values[counter++],
                };
            },
        };
    }
}

class ToStringStub {
    constructor(private _string: string) {}

    toString() {
        console.log('*' + '*');
        return this._string;
    }
}

function parseTemplates(test: string) {
    const templates: Record<string, string> = {};

    const matches = test.matchAll(
        /--TEMPLATE(?:\((.*?)\))?--(.*?)(?=--TEMPLATE|$)/gs
    );
    for (const match of matches) {
        templates[match[1] ?? 'index.twig'] = match[2];
    }

    return templates;
}

function getFixturesFiles(dir = FIXTURES_DIR) {
    const files: string[] = [];

    if (!statSync(dir).isDirectory()) return [dir];

    readdirSync(dir).forEach(file => {
        const stat = statSync(dir + sep + file);
        if (stat.isDirectory())
            files.push(...getFixturesFiles(dir + sep + file));
        else files.push(dir + sep + file);
    });

    return files;
}

export function getTests(legacyTests = false, fixture?: string) {
    const tests: [
        string,
        string,
        string,
        Record<string, string>,
        string | false,
        (string | undefined)[][],
        string?
    ][] = [];

    getFixturesFiles(fixture).forEach(file => {
        if (!/\.test$/.test(file)) return;

        if (
            (legacyTests && file.indexOf('.legacy.test') < 0) ||
            (!legacyTests && file.indexOf('.legacy.test') > -1)
        )
            return;

        const test = readFileSync(file).toString();

        let message: string,
            condition: string,
            deprecation: string,
            templates: Record<string, string>,
            exception: string | false,
            outputs: (string | undefined)[][] = [],
            match: RegExpMatchArray | null;
        if (
            (match = test.match(
                /--TEST--\s*(.*?)\s*(?:--CONDITION--\s*(.*))?\s*(?:--DEPRECATION--\s*(.*?))?\s*((?:--TEMPLATE(?:\(.*?\))?--(?:.*?))+)\s*(?:--DATA--\s*(.*))?\s*--EXCEPTION--\s*(.*)/s
            ))
        ) {
            message = match[1];
            condition = match[2];
            deprecation = match[3];
            templates = parseTemplates(match[4]);
            exception = match[6];
            outputs = [[undefined, match[5], undefined, '']];
        } else if (
            (match = test.match(
                /--TEST--\s*(.*?)\s*(?:--CONDITION--\s*(.*))?\s*(?:--DEPRECATION--\s*(.*?))?\s*((?:--TEMPLATE(?:\(.*?\))?--(?:.*?))+)--DATA--.*?--EXPECT--.*/s
            ))
        ) {
            message = match[1];
            condition = match[2];
            deprecation = match[3];
            templates = parseTemplates(match[4]);
            exception = false;
            const matchesOutput = test.matchAll(
                /--DATA--(.*?)(?:--CONFIG--(.*?))?--EXPECT--(.*?)(?=--DATA--|$)/gs
            );
            for (const output of matchesOutput) outputs.push([...output]);
        } else
            throw new TypeError(
                `Test "${file.replace(FIXTURES_DIR + '/', '')}" is not valid.`
            );

        tests.push([
            file.replace(FIXTURES_DIR + '/', ''),
            message,
            condition,
            templates,
            exception,
            outputs,
            deprecation,
        ]);
    });

    if (legacyTests && tests.length === 0) {
        // add a dummy test
        return [['not', '-', '', {}, '', []]] as [
            string,
            string,
            string,
            Record<string, string>,
            string,
            []
        ][];
    }

    return tests;
}

export function doIntegrationTest(
    file: string,
    message: string,
    _condition: string,
    templates: Record<string, string>,
    exception: false | string,
    outputs: (string | undefined)[][],
    deprecation = ''
) {
    const loader = new RecordLoader(templates);

    outputs.forEach((match, i) => {
        const config = {
            cache: false,
            strictVariables: true,
            ...(match[2]
                ? (eval('(' + match[2] + ')') as Record<string, unknown>)
                : {}),
        };
        const twig = new Environment(loader, config);
        twig.addGlobal('global', 'global');
        //twig.addExtension(new DebugExtension());
        twig.addExtension(
            new SandboxExtension(
                new SecurityPolicy([], [], {}, {}, ['dump']),
                false
            )
        );
        //twig.addExtension(new StringLoaderExtension());
        twig.addExtension(new TwigTestExtension());

        let template: Template | TemplateWrapper = new TemplateWrapper(
            twig,
            new Template(twig)
        );
        const deprecations: string[] = [];
        try {
            /*prevHandler = set_error_handler(function (type, msg, file, line, context = []) use (&deprecations, &prevHandler) {
                    if (\E_USER_DEPRECATED === type) {
                        deprecations[] = msg;

                        return true;
                    }

                    return prevHandler ? prevHandler(type, msg, file, line, context) : false;
                });*/

            template = twig.load('index.twig');
        } catch (error) {
            if (exception !== false && exception !== undefined) {
                message = (error as Error).message;
                expect(
                    (error as Error).constructor.name + ': ' + message
                ).to.be.equal(exception.trim());
                const last = message.substring(message.length - 1);
                expect(
                    last === '.' || last === '?',
                    'Exception message must end with a dot or a question mark.'
                ).to.be.equal(true);
            } else {
                (error as Error).message =
                    (error as Error).constructor.name +
                    ': ' +
                    (error as Error).message;
                throw error;
            }
        } /* finally {
                restore_error_handler();
            }*/

        expect(deprecations.join('\n')).to.be.equal(deprecation);

        let output = '';
        try {
            // Fake declaration for eval
            expect(TwigTestFoo).to.be.not.equal(false);
            expect(RuntimeError).to.be.not.equal(false);
            // eslint-disable-next-line @typescript-eslint/no-shadow
            const Markup = MarkupInclude;
            expect(Markup).to.be.not.equal(false);
            expect(ArrayIterator).to.be.not.equal(false);
            expect(ToStringStub).to.be.not.equal(false);
            // Render
            output = template.render(eval('(' + match[1] + ')')).trim();
        } catch (error) {
            if (exception !== false) {
                expect(
                    (
                        (error as Error).constructor.name +
                        ': ' +
                        (error as Error).message
                    ).trim()
                ).to.be.equal(exception.trim());
                return;
            }

            const errorMessage =
                (error as Error).constructor.name +
                ': ' +
                (error as Error).message;
            output = errorMessage;
        }

        /*if (exception !== false) {
                list($class) = explode(':', exception);
                $constraintClass = class_exists(
                    'PHPUnitFrameworkConstraintException'
                )
                    ? 'PHPUnitFrameworkConstraintException'
                    : 'PHPUnit_Framework_Constraint_Exception';
                this.assertThat(null, new $constraintClass($class));
            }*/

        const expected = match[3]?.trim();

        if (expected !== output) {
            console.error(`Compiled templates that failed on case ${i + 1}:\n`);

            objectKeys(templates).forEach(name => {
                console.error('Template: ' + name + '\n');
                console.error(
                    twig.compile(
                        twig.parse(
                            twig.tokenize(
                                twig.getLoader().getSourceContext(name)
                            )
                        )
                    )
                );
            });
        }
        expect(output, message + ' (in ' + file + ')').to.be.equal(expected);
    });
}

/*
// This function is defined to check that escaping strategies
// like html works even if a function with the same name is defined.
function html()
{
    return 'foo';
}

function test_foo($value = 'foo')
{
    return $value;
}


/**
 * This class is used in tests for the "length" filter and "empty" test. It asserts that __call is not
 * used to convert such objects to strings.
 */
/*class MagicCallStub
{
    public function __call($name, $args)
    {
        throw new \Exception('__call shall not be called');
    }
}

class SimpleIteratorForTesting implements \Iterator
{
    private $data = [1, 2, 3, 4, 5, 6, 7];
    private $key = 0;

    public function current()
    {
        return this.key;
    }

    public function next(): void
    {
        ++this.key;
    }

    public function key()
    {
        return this.key;
    }

    public function valid(): bool
    {
        return isset(this.data[this.key]);
    }

    public function rewind(): void
    {
        this.key = 0;
    }

    public function toString()
    {
        // for testing, make sure string length returned is not the same as the `iterator_count`
        return str_repeat('X', iterator_count(this) + 10);
    }
}*/
