/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Environment } from './Environment';
import { Operator } from './Extension/ExtensionInterface';
import { Lexer } from './Lexer';
import { ArrayExpression } from './Node/Expression/ArrayExpression';
import { ArrowFunctionExpression } from './Node/Expression/ArrowFunctionExpression';
import { AssignNameExpression } from './Node/Expression/AssignNameExpression';
import { ConstantExpression } from './Node/Expression/ConstantExpression';
import { TwigNode } from './Node/TwigNode';
import { Parser } from './Parser';
import { SyntaxError } from './Error/SyntaxError';
import { Template } from './Template';
import { Token } from './Token';
import { NameExpression } from './Node/Expression/NameExpression';
import { AbstractExpression } from './Node/Expression/AbstractExpression';
import { ParentExpression } from './Node/Expression/ParentExpression';
import { BlockReferenceExpression } from './Node/Expression/BlockReferenceExpression';
import { GetAttrExpression } from './Node/Expression/GetAttrExpression';
import { MethodCallExpression } from './Node/Expression/MethodCallExpression';
import { NotUnary } from './Node/Expression/Unary/NotUnary';
import { ConcatBinary } from './Node/Expression/Binary/ConcatBinary';
import { TwigTest } from './TwigTest';
import { ConditionalExpression } from './Node/Expression/ConditionalExpression';
import { NegUnary } from './Node/Expression/Unary/NegUnary';
import { PosUnary } from './Node/Expression/Unary/PosUnary';
import { TestExpression } from './Node/Expression/TestExpression';
import { objectKeys } from '@kephajs/core/Helpers/Helpers';
import { AbstractBinary } from './Node/Expression/Binary/AbstractBinary';
import { AbstractUnary } from './Node/Expression/Unary/AbstractUnary';
import { RecordExpression } from './Node/Expression/RecordExpression';

export class ExpressionParser {
    static OPERATOR_LEFT = 1;

    static OPERATOR_RIGHT = 2;

    private _unaryOperators: Record<string, Operator>;

    private _binaryOperators: Record<string, Operator>;

    constructor(private _parser: Parser, private _env: Environment) {
        this._unaryOperators = this._env.getUnaryOperators();
        this._binaryOperators = this._env.getBinaryOperators();
    }

    parseExpression(precedence = 0, allowArrow = false) {
        const arrow = this._parseArrow();
        if (allowArrow && arrow !== null) return arrow;

        let expr: AbstractExpression = this._getPrimary();
        let token = this._parser.getCurrentToken();
        while (
            this._isBinary(token) &&
            this._binaryOperators[token.value + ''].precedence >= precedence
        ) {
            const op = this._binaryOperators[token.value + ''];
            this._parser.getStream().next();

            if ('is not' === token.value)
                expr = this._parseNotTestExpression(expr);
            else if ('is' === token.value)
                expr = this._parseTestExpression(expr);
            else if (op.callable !== undefined)
                expr = op.callable(this._parser, expr);
            else {
                const expr1 = this.parseExpression(
                    ExpressionParser.OPERATOR_LEFT === op.associativity
                        ? op.precedence + 1
                        : op.precedence
                );
                const classDeclaration =
                    op.class as unknown as typeof AbstractBinary;
                expr = new classDeclaration(expr, expr1, token.line);
            }

            token = this._parser.getCurrentToken();
        }

        if (precedence === 0) return this.parseConditionalExpression(expr);

        return expr;
    }

    private _parseArrow(): ArrowFunctionExpression | null {
        const stream = this._parser.getStream();

        // short array syntax (one argument, no parentheses)?
        if (stream.look(1).test(Token.ARROW_TYPE)) {
            const line = stream.getCurrent().line;
            const token = stream.expect(Token.NAME_TYPE);
            const names = [
                new AssignNameExpression(token.value + '', token.line),
            ];
            stream.expect(Token.ARROW_TYPE);

            return new ArrowFunctionExpression(
                this.parseExpression(0),
                new TwigNode(names),
                line
            );
        }

        // first, determine if we are parsing an arrow function by finding => (long form)
        let i = 0;
        if (!stream.look(i).test(Token.PUNCTUATION_TYPE, '(')) return null;
        ++i;
        // eslint-disable-next-line no-constant-condition
        while (true) {
            // variable name
            ++i;
            if (!stream.look(i).test(Token.PUNCTUATION_TYPE, ',')) break;

            ++i;
        }
        if (!stream.look(i).test(Token.PUNCTUATION_TYPE, ')')) return null;
        ++i;
        if (!stream.look(i).test(Token.ARROW_TYPE)) return null;

        // yes, let's parse it properly
        let token = stream.expect(Token.PUNCTUATION_TYPE, '(');
        const line = token.line;

        const names: AssignNameExpression[] = [];
        // eslint-disable-next-line no-constant-condition
        while (true) {
            token = stream.expect(Token.NAME_TYPE);
            names.push(new AssignNameExpression(token.value + '', token.line));

            if (!stream.nextIf(Token.PUNCTUATION_TYPE, ',')) {
                break;
            }
        }
        stream.expect(Token.PUNCTUATION_TYPE, ')');
        stream.expect(Token.ARROW_TYPE);

        return new ArrowFunctionExpression(
            this.parseExpression(0),
            new TwigNode(names),
            line
        );
    }

    private _getPrimary() {
        const token = this._parser.getCurrentToken();

        if (this._isUnary(token)) {
            const operator = this._unaryOperators[token.value + ''];
            this._parser.getStream().next();
            const expr = this.parseExpression(operator.precedence);
            const classDeclaration =
                operator.class as unknown as typeof AbstractUnary;

            return this.parsePostfixExpression(
                new classDeclaration(expr, token.line)
            );
        } else if (token.test(Token.PUNCTUATION_TYPE, '(')) {
            this._parser.getStream().next();
            const expr = this.parseExpression();
            this._parser
                .getStream()
                .expect(
                    Token.PUNCTUATION_TYPE,
                    ')',
                    'An opened parenthesis is not properly closed'
                );

            return this.parsePostfixExpression(expr);
        }

        return this.parsePrimaryExpression();
    }

    parseConditionalExpression(expr: AbstractExpression) {
        while (this._parser.getStream().nextIf(Token.PUNCTUATION_TYPE, '?')) {
            let expr2, expr3: AbstractExpression;
            if (!this._parser.getStream().nextIf(Token.PUNCTUATION_TYPE, ':')) {
                expr2 = this.parseExpression();
                if (
                    this._parser.getStream().nextIf(Token.PUNCTUATION_TYPE, ':')
                )
                    expr3 = this.parseExpression();
                else {
                    expr3 = new ConstantExpression(
                        '',
                        this._parser.getCurrentToken().line
                    );
                }
            } else {
                expr2 = expr;
                expr3 = this.parseExpression();
            }

            expr = new ConditionalExpression(
                expr,
                expr2,
                expr3,
                this._parser.getCurrentToken().line
            );
        }

        return expr;
    }

    private _isUnary(token: Token) {
        return (
            token.test(Token.OPERATOR_TYPE) &&
            this._unaryOperators[token.value as string] !== undefined
        );
    }

    private _isBinary(token: Token) {
        return (
            token.test(Token.OPERATOR_TYPE) &&
            this._binaryOperators[token.value as string] !== undefined
        );
    }

    parsePrimaryExpression() {
        const token = this._parser.getCurrentToken();
        let node: AbstractExpression;
        switch (token.type) {
            case Token.NAME_TYPE:
                this._parser.getStream().next();
                switch (token.value) {
                    case 'true':
                    case 'TRUE':
                        node = new ConstantExpression(true, token.line);
                        break;

                    case 'false':
                    case 'FALSE':
                        node = new ConstantExpression(false, token.line);
                        break;

                    case 'none':
                    case 'NONE':
                    case 'null':
                    case 'NULL':
                        node = new ConstantExpression(null, token.line);
                        break;

                    default:
                        if ('(' === this._parser.getCurrentToken().value)
                            node = this.getFunctionNode(
                                token.value as string,
                                token.line
                            );
                        else
                            node = new NameExpression(
                                token.value as string,
                                token.line
                            );
                }
                break;

            case Token.NUMBER_TYPE:
                this._parser.getStream().next();
                node = new ConstantExpression(token.value, token.line);
                break;

            case Token.STRING_TYPE:
            case Token.INTERPOLATION_START_TYPE:
                node = this.parseStringExpression();
                break;

            case Token.OPERATOR_TYPE:
                // eslint-disable-next-line no-case-declarations
                const matches = (token.value as string).match(Lexer.REGEX_NAME);
                if (matches && matches[0] == token.value) {
                    // in this context, string operators are variable names
                    this._parser.getStream().next();
                    node = new NameExpression(
                        token.value as string,
                        token.line
                    );
                    break;
                }

                if (this._unaryOperators[token.value + ''] !== undefined) {
                    const classDeclaration = this._unaryOperators[
                        token.value as string
                    ].class as unknown as typeof AbstractUnary;
                    if (
                        (
                            [
                                NegUnary,
                                PosUnary,
                            ] as unknown as typeof AbstractUnary[]
                        ).indexOf(classDeclaration) === -1
                    )
                        throw new SyntaxError(
                            `Unexpected unary operator "${token.value}".`,
                            token.line,
                            this._parser.getStream().getSourceContext()
                        );

                    this._parser.getStream().next();
                    const expr = this.parsePrimaryExpression();

                    node = new classDeclaration(expr, token.line);
                    break;
                }

            // no break
            // eslint-disable-next-line no-fallthrough
            default:
                if (token.test(Token.PUNCTUATION_TYPE, '['))
                    node = this.parseArrayExpression();
                else if (token.test(Token.PUNCTUATION_TYPE, '{'))
                    node = this.parseHashExpression();
                else if (
                    token.test(Token.OPERATOR_TYPE, '=') &&
                    ('==' === this._parser.getStream().look(-1).value ||
                        '!=' === this._parser.getStream().look(-1).value)
                )
                    throw new SyntaxError(
                        `Unexpected operator of value "${token.value}". Did you try to use "===" or "!==" for strict comparison? Use "is same as(value)" instead.`,
                        token.line,
                        this._parser.getStream().getSourceContext()
                    );
                else
                    throw new SyntaxError(
                        `Unexpected token "${Token.typeToEnglish(
                            token.type
                        )}" of value "${token.value}".`,
                        token.line,
                        this._parser.getStream().getSourceContext()
                    );
        }

        return this.parsePostfixExpression(node);
    }

    parseStringExpression() {
        const stream = this._parser.getStream();

        const nodes: AbstractExpression[] = [];
        // a string cannot be followed by another string in a single expression
        let nextCanBeString = true;
        // eslint-disable-next-line no-constant-condition
        while (true) {
            let token: null | Token;
            if (nextCanBeString && (token = stream.nextIf(Token.STRING_TYPE))) {
                nodes.push(new ConstantExpression(token.value, token.line));
                nextCanBeString = false;
            } else if (stream.nextIf(Token.INTERPOLATION_START_TYPE)) {
                nodes.push(this.parseExpression());
                stream.expect(Token.INTERPOLATION_END_TYPE);
                nextCanBeString = true;
            } else break;
        }

        let expr = nodes.shift() as AbstractExpression;
        nodes.forEach(node => {
            expr = new ConcatBinary(expr, node, node.getTemplateLine());
        });

        return expr;
    }

    parseArrayExpression() {
        const stream = this._parser.getStream();
        stream.expect(
            Token.PUNCTUATION_TYPE,
            '[',
            'An array element was expected'
        );

        const node = new ArrayExpression([], stream.getCurrent().line);
        let first = true;
        while (!stream.test(Token.PUNCTUATION_TYPE, ']')) {
            if (!first) {
                stream.expect(
                    Token.PUNCTUATION_TYPE,
                    ',',
                    'An array element must be followed by a comma'
                );

                // trailing ,?
                if (stream.test(Token.PUNCTUATION_TYPE, ']')) break;
            }
            first = false;

            node.addElement(this.parseExpression());
        }
        stream.expect(
            Token.PUNCTUATION_TYPE,
            ']',
            'An opened array is not properly closed'
        );

        return node;
    }

    parseHashExpression() {
        const stream = this._parser.getStream();
        stream.expect(
            Token.PUNCTUATION_TYPE,
            '{',
            'A hash element was expected'
        );

        const node = new RecordExpression([], stream.getCurrent().line);
        let first = true;
        while (!stream.test(Token.PUNCTUATION_TYPE, '}')) {
            if (!first) {
                stream.expect(
                    Token.PUNCTUATION_TYPE,
                    ',',
                    'A hash value must be followed by a comma'
                );

                // trailing ,?
                if (stream.test(Token.PUNCTUATION_TYPE, '}')) break;
            }
            first = false;

            // a hash key can be:
            //
            //  * a number -- 12
            //  * a string -- 'a'
            //  * a name, which is equivalent to a string -- a
            //  * an expression, which must be enclosed in parentheses -- (1 + 2)
            let token: Token | null;
            let key, value: AbstractExpression;
            if ((token = stream.nextIf(Token.NAME_TYPE))) {
                key = new ConstantExpression(token.value, token.line);

                // {a} is a shortcut for {a:a}
                if (stream.test(Token.PUNCTUATION_TYPE, [',', '}'])) {
                    value = new NameExpression(
                        key.getAttribute('value') as string,
                        key.getTemplateLine()
                    );
                    node.addElement(value, key);
                    continue;
                }
            } else if (
                (token = stream.nextIf(Token.STRING_TYPE)) ||
                (token = stream.nextIf(Token.NUMBER_TYPE))
            )
                key = new ConstantExpression(token.value, token.line);
            else if (stream.test(Token.PUNCTUATION_TYPE, '('))
                key = this.parseExpression();
            else {
                const current = stream.getCurrent();

                throw new SyntaxError(
                    `A hash key must be a quoted string, a number, a name, or an expression enclosed in parentheses (unexpected token "${Token.typeToEnglish(
                        current.type
                    )}" of value "${current.value}".`,
                    current.line,
                    stream.getSourceContext()
                );
            }

            stream.expect(
                Token.PUNCTUATION_TYPE,
                ':',
                'A hash key must be followed by a colon (:)'
            );
            value = this.parseExpression();

            node.addElement(value, key);
        }
        stream.expect(
            Token.PUNCTUATION_TYPE,
            '}',
            'An opened hash is not properly closed'
        );

        return node;
    }

    parsePostfixExpression(node: TwigNode) {
        // eslint-disable-next-line no-constant-condition
        while (true) {
            const token = this._parser.getCurrentToken();
            if (Token.PUNCTUATION_TYPE === token.type) {
                if ('.' === token.value || '[' === token.value)
                    node = this.parseSubscriptExpression(node);
                else if ('|' === token.value)
                    node = this.parseFilterExpression(node);
                else break;
            } else break;
        }

        return node;
    }

    getFunctionNode(fctName: string, line: number) {
        let args: TwigNode | ArrayExpression;
        switch (fctName) {
            case 'parent':
                this.parseArguments();
                if (this._parser.getBlockStack().length === 0)
                    throw new SyntaxError(
                        'Calling "parent" outside a block is forbidden.',
                        line,
                        this._parser.getStream().getSourceContext()
                    );

                if (!this._parser.getParent() && !this._parser.hasTraits())
                    throw new SyntaxError(
                        'Calling "parent" on a template that does not extend nor "use" another template is forbidden.',
                        line,
                        this._parser.getStream().getSourceContext()
                    );

                return new ParentExpression(
                    this._parser.peekBlockStack(),
                    line
                );
            case 'block':
                args = this.parseArguments();
                if (args.length < 1)
                    throw new SyntaxError(
                        'The "block" function takes one argument (the block name).',
                        line,
                        this._parser.getStream().getSourceContext()
                    );

                return new BlockReferenceExpression(
                    args.getNode(0),
                    args.length > 1 ? args.getNode(1) : null,
                    line
                );
            case 'attribute':
                args = this.parseArguments();
                if (args.length < 2)
                    throw new SyntaxError(
                        'The "attribute" function takes at least two arguments (the variable and the attributes).',
                        line,
                        this._parser.getStream().getSourceContext()
                    );

                return new GetAttrExpression(
                    args.getNode(0),
                    args.getNode(1),
                    args.length > 2 ? args.getNode(2) : null,
                    Template.ANY_CALL,
                    line
                );
            default:
                // eslint-disable-next-line no-case-declarations
                const alias = this._parser.getImportedSymbol(
                    'function',
                    fctName
                );
                if (alias !== null && alias.node !== null && alias.name) {
                    args = new ArrayExpression([], line);
                    for (const n of this.parseArguments())
                        (args as ArrayExpression).addElement(n);

                    const node = new MethodCallExpression(
                        alias.node,
                        alias.name,
                        args as ArrayExpression,
                        line
                    );
                    node.setAttribute('safe', true);

                    return node;
                }

                args = this.parseArguments(true);
                // eslint-disable-next-line no-case-declarations
                const className = this._getFunctionNodeClass(fctName, line);

                return new className(fctName, args, line);
        }
    }

    parseSubscriptExpression(node: TwigNode) {
        const stream = this._parser.getStream();
        let token = stream.next();
        const lineno = token.line;
        let args: TwigNode | ArrayExpression = new ArrayExpression([], lineno);
        let arg: TwigNode;
        let type = Template.ANY_CALL;
        if (token.value === '.') {
            token = stream.next();
            if (
                Token.NAME_TYPE == token.type ||
                Token.NUMBER_TYPE == token.type ||
                (Token.OPERATOR_TYPE == token.type &&
                    Lexer.REGEX_NAME.test(token.value as string))
            ) {
                arg = new ConstantExpression(token.value, lineno);

                if (stream.test(Token.PUNCTUATION_TYPE, '(')) {
                    type = Template.METHOD_CALL;
                    for (const n of this.parseArguments())
                        (args as ArrayExpression).addElement(n);
                }
            } else {
                throw new SyntaxError(
                    'Expected name or number, got value "' +
                        token.value +
                        '" of type ' +
                        Token.typeToEnglish(token.type) +
                        '.',
                    lineno,
                    stream.getSourceContext()
                );
            }

            if (
                node instanceof NameExpression &&
                null !==
                    this._parser.getImportedSymbol(
                        'template',
                        node.getAttribute('name') as string
                    )
            ) {
                if (!(arg instanceof ConstantExpression)) {
                    throw new SyntaxError(
                        `Dynamic macro names are not supported (called on "${node.getAttribute(
                            'name'
                        )}").`,
                        token.line,
                        stream.getSourceContext()
                    );
                }

                const nameValue = arg.getAttribute('value');

                node = new MethodCallExpression(
                    node,
                    'macro_' + nameValue,
                    args as ArrayExpression,
                    lineno
                );
                node.setAttribute('safe', true);

                return node;
            }
        } else {
            type = Template.ARRAY_CALL;

            // slice?
            let slice = false;
            if (stream.test(Token.PUNCTUATION_TYPE, ':')) {
                slice = true;
                arg = new ConstantExpression(0, token.line);
            } else arg = this.parseExpression();

            if (stream.nextIf(Token.PUNCTUATION_TYPE, ':')) slice = true;

            let length: AbstractExpression;
            if (slice) {
                if (stream.test(Token.PUNCTUATION_TYPE, ']'))
                    length = new ConstantExpression(null, token.line);
                else length = this.parseExpression();

                const className = this._getFilterNodeClass('slice', token.line);
                args = new TwigNode([arg, length]);
                const filter = new className(
                    node,
                    new ConstantExpression('slice', token.line),
                    args,
                    token.line
                );

                stream.expect(Token.PUNCTUATION_TYPE, ']');

                return filter;
            }

            stream.expect(Token.PUNCTUATION_TYPE, ']');
        }

        return new GetAttrExpression(node, arg, args, type, lineno);
    }

    parseFilterExpression(node: TwigNode) {
        this._parser.getStream().next();

        return this.parseFilterExpressionRaw(node);
    }

    parseFilterExpressionRaw(node: TwigNode, tag: string | null = null) {
        // eslint-disable-next-line no-constant-condition
        while (true) {
            let args: TwigNode;
            const token = this._parser.getStream().expect(Token.NAME_TYPE);

            const filterName = new ConstantExpression(token.value, token.line);
            if (!this._parser.getStream().test(Token.PUNCTUATION_TYPE, '('))
                args = new TwigNode();
            else args = this.parseArguments(true, false, true);

            const className = this._getFilterNodeClass(
                filterName.getAttribute('value') as string,
                token.line
            );

            node = new className(node, filterName, args, token.line, tag);

            if (!this._parser.getStream().test(Token.PUNCTUATION_TYPE, '|'))
                break;

            this._parser.getStream().next();
        }

        return node;
    }

    parseArguments(
        namedArguments = false,
        definition = false,
        allowArrow = false
    ) {
        const args: Record<string, AbstractExpression> = {};
        const stream = this._parser.getStream();

        stream.expect(
            Token.PUNCTUATION_TYPE,
            '(',
            'A list of arguments must begin with an opening parenthesis'
        );
        let index = 0;
        while (!stream.test(Token.PUNCTUATION_TYPE, ')')) {
            if (objectKeys(args).length > 0) {
                stream.expect(
                    Token.PUNCTUATION_TYPE,
                    ',',
                    'Arguments must be separated by a comma'
                );

                // if the comma above was a trailing comma, early exit the argument parse loop
                if (stream.test(Token.PUNCTUATION_TYPE, ')')) break;
            }

            let value: AbstractExpression;
            if (definition) {
                const token = stream.expect(
                    Token.NAME_TYPE,
                    null,
                    'An argument must be a name'
                );
                value = new NameExpression(
                    token.value as string,
                    this._parser.getCurrentToken().line
                );
            } else value = this.parseExpression(0, allowArrow);

            let argName: null | string = null;
            let token: null | Token;
            if (
                namedArguments &&
                (token = stream.nextIf(Token.OPERATOR_TYPE, '='))
            ) {
                if (!(value instanceof NameExpression))
                    throw new SyntaxError(
                        `A parameter name must be a string, "${value.constructor.name}" given.`,
                        token.line,
                        stream.getSourceContext()
                    );
                argName = value.getAttribute('name') as string;

                if (definition) {
                    value = this.parsePrimaryExpression();

                    if (!this._checkConstantExpression(value))
                        throw new SyntaxError(
                            'A default value for an argument must be a constant (a boolean, a string, a number, an array, or a record).',
                            token.line,
                            stream.getSourceContext()
                        );
                } else value = this.parseExpression(0, allowArrow);
            }

            if (definition) {
                if (argName === null) {
                    argName = value.getAttribute('name') as string;
                    value = new ConstantExpression(
                        null,
                        this._parser.getCurrentToken().line
                    );
                }
                args[argName as string] = value;
            } else {
                if (argName === null) args[index++] = value;
                else args[argName] = value;
            }
        }
        stream.expect(
            Token.PUNCTUATION_TYPE,
            ')',
            'A list of arguments must be closed by a parenthesis'
        );

        return new TwigNode(args);
    }

    parseAssignmentExpression() {
        const stream = this._parser.getStream();
        const targets: AssignNameExpression[] = [];
        // eslint-disable-next-line no-constant-condition
        while (true) {
            const token = this._parser.getCurrentToken();
            if (
                stream.test(Token.OPERATOR_TYPE) &&
                Lexer.REGEX_NAME.test(token.value as string)
            ) {
                // in this context, string operators are variable names
                this._parser.getStream().next();
            } else
                stream.expect(
                    Token.NAME_TYPE,
                    null,
                    'Only variables can be assigned to'
                );
            const value = token.value as string;
            if (
                ['true', 'false', 'none', 'null'].indexOf(value.toLowerCase()) >
                -1
            )
                throw new SyntaxError(
                    `You cannot assign a value to "${value}".`,
                    token.line,
                    stream.getSourceContext()
                );
            targets.push(new AssignNameExpression(value, token.line));

            if (!stream.nextIf(Token.PUNCTUATION_TYPE, ',')) {
                break;
            }
        }

        return new TwigNode(targets);
    }

    parseMultitargetExpression() {
        const targets: AbstractExpression[] = [];
        // eslint-disable-next-line no-constant-condition
        while (true) {
            targets.push(this.parseExpression());
            if (!this._parser.getStream().nextIf(Token.PUNCTUATION_TYPE, ','))
                break;
        }

        return new TwigNode(targets);
    }

    private _parseNotTestExpression(node: TwigNode) {
        return new NotUnary(
            this._parseTestExpression(node),
            this._parser.getCurrentToken().line
        );
    }

    private _parseTestExpression(node: TwigNode): TestExpression {
        const stream = this._parser.getStream();
        const { testName, test } = this._getTest(node.getTemplateLine());

        const testNodeClass = this._getTestNodeClass(test);
        let args: null | TwigNode = null;
        if (stream.test(Token.PUNCTUATION_TYPE, '('))
            args = this.parseArguments(true);
        else if (test.hasOneMandatoryArgument())
            args = new TwigNode({ 0: this.parsePrimaryExpression() });

        if ('defined' === testName && node instanceof NameExpression) {
            const alias = this._parser.getImportedSymbol(
                'function',
                node.getAttribute('name') as string
            );
            if (alias !== null && alias.node !== null && alias.name !== null) {
                node = new MethodCallExpression(
                    alias.node,
                    alias.name,
                    new ArrayExpression([], node.getTemplateLine()),
                    node.getTemplateLine()
                );
                node.setAttribute('safe', true);
            }
        }

        return new testNodeClass(
            node,
            testName,
            args,
            this._parser.getCurrentToken().line
        );
    }

    private _getTest(line: number) {
        const stream = this._parser.getStream();
        let testName = stream.expect(Token.NAME_TYPE).value as string;

        let test = this._env.getTest(testName);
        if (test) return { testName, test };

        if (stream.test(Token.NAME_TYPE)) {
            // try 2-words tests
            testName = testName + ' ' + this._parser.getCurrentToken().value;

            test = this._env.getTest(testName);
            if (test) {
                stream.next();

                return { testName, test };
            }
        }

        const error = new SyntaxError(
            `Unknown "${testName}" test.`,
            line,
            stream.getSourceContext()
        );
        error.addSuggestions(testName, objectKeys(this._env.getTests()));

        throw error;
    }

    private _getTestNodeClass(test: TwigTest) {
        if (test.isDeprecated()) {
            const stream = this._parser.getStream();
            let message = `Twig Test "${test.getName}" is deprecated`;

            if (test.getDeprecatedVersion())
                message += ' since version ' + test.getDeprecatedVersion();
            if (test.getAlternative())
                message += `. Use "${test.getAlternative()}" instead`;
            const src = stream.getSourceContext();
            message += ` in ${src.path ?? src.name} at line ${
                stream.getCurrent().line
            }.`;

            throw new Error(message);
            //@trigger_error($message, \E_USER_DEPRECATED);
        }

        return test.getNodeClass();
    }

    private _getFunctionNodeClass(fctName: string, line: number) {
        const fct = this._env.getFunction(fctName);
        if (fct === null) {
            const error = new SyntaxError(
                `Unknown "${fctName}" function.`,
                line,
                this._parser.getStream().getSourceContext()
            );
            error.addSuggestions(fctName, objectKeys(this._env.getFunctions()));

            throw error;
        }

        if (fct.isDeprecated()) {
            let message = `Twig Function "${fct.getName()}" is deprecated`;
            if (fct.getDeprecatedVersion())
                message += ` since version ${fct.getDeprecatedVersion()}`;
            if (fct.getAlternative())
                message += `. Use "${fct.getAlternative()}" instead`;
            const src = this._parser.getStream().getSourceContext();
            message += ` in ${src.path ?? src.name} at line ${line}.`;

            throw new Error(message);
            //@trigger_error($message, \E_USER_DEPRECATED);
        }

        return fct.getNodeClass();
    }

    _getFilterNodeClass(filterName: string, line: number) {
        const filter = this._env.getFilter(filterName);
        if (!filter) {
            const error = new SyntaxError(
                `Unknown "${filterName}" filter.`,
                line,
                this._parser.getStream().getSourceContext()
            );
            error.addSuggestions(
                filterName,
                Object.keys(this._env.getFilters())
            );

            throw error;
        }

        if (filter.isDeprecated()) {
            let message = `Twig Filter "${filter.getName()}" is deprecated`;
            if (filter.getDeprecatedVersion())
                message += ' since version ' + filter.getDeprecatedVersion();
            if (filter.getAlternative())
                message += `. Use "${filter.getAlternative()}" instead`;
            const src = this._parser.getStream().getSourceContext();
            message += ` in ${src.path ?? src.name} at line ${line}.`;

            throw new Error(message);
            //@trigger_error($message, \E_USER_DEPRECATED);
        }

        return filter.getNodeClass();
    }

    // checks that the node only contains "constant" elements
    private _checkConstantExpression(node: TwigNode): boolean {
        if (
            !(
                node instanceof ConstantExpression ||
                node instanceof ArrayExpression ||
                node instanceof RecordExpression ||
                node instanceof NegUnary ||
                node instanceof PosUnary
            )
        )
            return false;

        for (const n of node)
            if (!this._checkConstantExpression(n)) return false;

        return true;
    }
}
