/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

export interface SecurityPolicyInterface {
    checkSecurity(tags: string[], filters: string[], functions: string[]): void;

    checkMethodAllowed(obj: object, method: string): void;

    checkPropertyAllowed(obj: object, property: string): void;
}
