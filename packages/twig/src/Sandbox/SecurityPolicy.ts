/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { objectKeys } from '@kephajs/core/Helpers/Helpers';
import { Markup } from '../Markup';
import { Template } from '../Template';
import { SecurityNotAllowedFilterError } from './SecurityNotAllowedFilterError';
import { SecurityNotAllowedFunctionError } from './SecurityNotAllowedFunctionError';
import { SecurityNotAllowedMethodError } from './SecurityNotAllowedMethodError';
import { SecurityNotAllowedPropertyError } from './SecurityNotAllowedPropertyError';
import { SecurityNotAllowedTagError } from './SecurityNotAllowedTagError';
import { SecurityPolicyInterface } from './SecurityPolicyInterface';

export class SecurityPolicy implements SecurityPolicyInterface {
    private _allowedMethods: Record<string, string[]> = {};

    constructor(
        private _allowedTags: string[] = [],
        private _allowedFilters: string[] = [],
        allowedMethods: Record<string, string[] | string> = {},
        private _allowedProperties: Record<string, string | string[]> = {},
        private _allowedFunctions: string[] = []
    ) {
        this.setAllowedMethods(allowedMethods);
    }

    setAllowedTags(tags: string[]) {
        this._allowedTags = tags;
    }

    setAllowedFilters(filters: string[]) {
        this._allowedFilters = filters;
    }

    setAllowedMethods(methods: Record<string, string[] | string>) {
        this._allowedMethods = {};

        objectKeys(methods).forEach(clss => {
            this._allowedMethods[clss] = (
                (Array.isArray(methods[clss])
                    ? methods[clss]
                    : [methods[clss]]) as string[]
            ).map(value => {
                return value.toLowerCase();
            });
        });
    }

    setAllowedProperties(properties: Record<string, string | string[]>) {
        this._allowedProperties = properties;
    }

    setAllowedFunctions(functions: string[]) {
        this._allowedFunctions = functions;
    }

    checkSecurity(tags: string[], filters: string[], functions: string[]) {
        tags.forEach(tag => {
            if (this._allowedTags.indexOf(tag) === -1)
                throw new SecurityNotAllowedTagError(
                    `Tag "${tag}" is not allowed.`,
                    tag
                );
        });

        filters.forEach(filter => {
            if (this._allowedFilters.indexOf(filter) === -1)
                throw new SecurityNotAllowedFilterError(
                    `Filter "${filter}" is not allowed.`,
                    filter
                );
        });

        functions.forEach(fct => {
            if (this._allowedFunctions.indexOf(fct) === -1)
                throw new SecurityNotAllowedFunctionError(
                    `Function "${fct}" is not allowed.`,
                    fct
                );
        });
    }

    checkMethodAllowed(obj: object, method: string) {
        if (obj instanceof Template || obj instanceof Markup) return;

        let allowed = false;
        method = method.toLowerCase();
        objectKeys(this._allowedMethods).every(clss => {
            if (obj.constructor.name === clss) {
                allowed = this._allowedMethods[clss].indexOf(method) > -1;
                return false;
            }
            return true;
        });

        if (!allowed)
            throw new SecurityNotAllowedMethodError(
                `Calling "${method}" method on a "${obj.constructor.name}" object is not allowed.`,
                obj.constructor.name,
                method
            );
    }

    checkPropertyAllowed(obj: object, property: string) {
        let allowed = false;
        objectKeys(this._allowedProperties).every(clss => {
            if (obj.constructor.name === clss) {
                const properties = Array.isArray(this._allowedProperties[clss])
                    ? this._allowedProperties[clss]
                    : [this._allowedProperties[clss]];
                allowed = properties.indexOf(property) > -1;
                return false;
            }
            return true;
        });

        if (!allowed)
            throw new SecurityNotAllowedPropertyError(
                `Calling "${property}" property on a "${obj.constructor.name}" object is not allowed.`,
                obj.constructor.name,
                property
            );
    }
}
