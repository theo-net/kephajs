/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { SecurityError } from './SecurityError';

export class SecurityNotAllowedTagError extends SecurityError {
    constructor(message: string, private _tagName: string) {
        super(message);
    }

    getTagName() {
        return this._tagName;
    }
}
