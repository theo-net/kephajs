/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { SecurityError } from './SecurityError';

export class SecurityNotAllowedFilterError extends SecurityError {
    constructor(message: string, private _filterName: string) {
        super(message);
    }

    getFilterName() {
        return this._filterName;
    }
}
