/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Source } from '../Source';

export interface LoaderInterface {
    getSourceContext(name: string): Source;
    getCacheKey(name: string): string;
    isFresh(name: string, time: number): boolean;
    exists(name: string): boolean;
}
