/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';
import fs from 'fs';
import path from 'path';

import { RecordLoader } from './RecordLoader';
import { ChainLoader } from './ChainLoader';
import { FilesystemLoader } from './FilesystemLoader';
import { LoaderError } from '../Error/LoaderError';

describe('@kephajs/twig/Loader/FilesystemLoader', () => {
    const fixturesPath =
        __dirname + path.sep + '..' + path.sep + '..' + path.sep + 'Fixtures';
    const fixturesRealPath = fs.realpathSync(fixturesPath);

    describe('constructor', () => {
        it('Should not have paths if empty', () => {
            const loader = new FilesystemLoader();
            expect(loader.getPaths()).to.be.deep.equal([]);
        });

        it('Should register the real root path', () => {
            const loader = new FilesystemLoader([], fixturesPath);
            loader.addPath('normal');
            expect(loader.getSourceContext('index.html').path).to.be.equal(
                fixturesRealPath + path.sep + 'normal' + path.sep + 'index.html'
            );
        });

        it('Should use the process.cwd value if root path not defined', () => {
            const loader = new FilesystemLoader([]);
            loader.addPath(
                'packages' +
                    path.sep +
                    'twig' +
                    path.sep +
                    'Fixtures' +
                    path.sep +
                    'normal'
            );
            expect(loader.getSourceContext('index.html').path).to.be.equal(
                fixturesRealPath + path.sep + 'normal' + path.sep + 'index.html'
            );
        });

        it('Should register the paths in the main namespace', () => {
            const loader = new FilesystemLoader(
                ['normal', 'errors'],
                fixturesPath
            );
            expect(
                loader.getPaths(FilesystemLoader.MAIN_NAMESPACE)
            ).to.be.deep.equal(['normal', 'errors']);
        });
    });

    describe('getNamespace', () => {
        it('Should return the main namespace', () => {
            const loader = new FilesystemLoader([], fixturesPath);
            expect(loader.getNamespaces()).to.be.deep.equal([
                FilesystemLoader.MAIN_NAMESPACE,
            ]);
        });

        it('Should return all registered namespaces', () => {
            const loader = new FilesystemLoader([], fixturesPath);
            loader.addPath('normal', 'named');
            expect(loader.getNamespaces()).to.be.deep.equal([
                FilesystemLoader.MAIN_NAMESPACE,
                'named',
            ]);
        });
    });

    describe('addPath', () => {
        let loader: FilesystemLoader;

        beforeEach(() => {
            loader = new FilesystemLoader([], fixturesPath);
        });

        it('Should add a path in the main namespace', () => {
            loader.addPath('normal');
            expect(
                loader.getPaths(FilesystemLoader.MAIN_NAMESPACE)
            ).to.be.deep.equal(['normal']);
        });

        it('Should add a path in a specific namespace', () => {
            loader.addPath('normal', 'foobar');
            expect(loader.getPaths('foobar')).to.be.deep.equal(['normal']);
        });

        it('Should add an absolute path', () => {
            loader.addPath(fixturesRealPath + path.sep + 'normal');
            expect(loader.getPaths()).to.be.deep.equal([
                fixturesRealPath + path.sep + 'normal',
            ]);
        });

        it("Should throw a LoaderError if the path don't exits", () => {
            try {
                loader.addPath('foobar');
            } catch (error) {
                expect(error).to.be.instanceOf(LoaderError);
                expect((error as LoaderError).message).to.contain(
                    'directory does not exist'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it("Should throw a LoaderError if the path isn't a directory", () => {
            try {
                loader.addPath('normal/index.html');
            } catch (error) {
                expect(error).to.be.instanceOf(LoaderError);
                expect((error as LoaderError).message).to.contain(
                    'directory does not exist'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should remove the final /', () => {
            loader.addPath('normal' + path.sep);
            expect(loader.getPaths()).to.be.deep.equal(['normal']);
        });
    });

    describe('prependPath', () => {
        function testPath(
            basePath: string,
            cacheKey: string,
            rootPath: string
        ) {
            const loader = new FilesystemLoader(
                [basePath + '/normal', basePath + '/normal_bis'],
                rootPath
            );
            loader.setPaths(
                [basePath + '/named', basePath + '/named_bis'],
                'named'
            );
            loader.addPath(basePath + '/named_ter', 'named');
            loader.addPath(basePath + '/normal_ter');
            loader.prependPath(basePath + '/normal_final');
            loader.prependPath(basePath + '/named/../named_quater', 'named');
            loader.prependPath(basePath + '/named_final', 'named');

            expect(loader.getPaths()).to.be.deep.equal([
                basePath + '/normal_final',
                basePath + '/normal',
                basePath + '/normal_bis',
                basePath + '/normal_ter',
            ]);
            expect(loader.getPaths('named')).to.be.deep.equal([
                basePath + '/named_final',
                basePath + '/named/../named_quater',
                basePath + '/named',
                basePath + '/named_bis',
                basePath + '/named_ter',
            ]);

            expect(
                loader
                    .getCacheKey('@named/named_absolute.html')
                    .replace(/\\/g, '/')
            ).to.be.equal(cacheKey);
            expect(loader.getSourceContext('index.html').code).to.be.equal(
                'path (final)\n'
            );
            expect(
                loader.getSourceContext('@__main__/index.html').code
            ).to.be.equal('path (final)\n');
            expect(
                loader.getSourceContext('@named/index.html').code
            ).to.be.equal('named path (final)\n');
        }

        it('Should add path and preprend path', () => {
            [
                [
                    __dirname + '/../../Fixtures',
                    'named_quater/named_absolute.html',
                    fixturesRealPath,
                ],
                [
                    __dirname + '/../../Fixtures/../Fixtures',
                    'named_quater/named_absolute.html',
                    fixturesRealPath,
                ],
                [
                    'packages/twig/Fixtures',
                    'Fixtures/named_quater/named_absolute.html',
                    fixturesRealPath + '..' + path.sep + '..' + path.sep + '..',
                ],
                [
                    'Fixtures',
                    'Fixtures/named_quater/named_absolute.html',
                    process.cwd() + '/packages/twig',
                ],
                [
                    'Fixtures',
                    'Fixtures/named_quater/named_absolute.html',
                    process.cwd() + '/packages/../packages/twig',
                ],
            ].every(pathContext =>
                testPath(
                    pathContext[0] as string,
                    pathContext[1] as string,
                    pathContext[2] as string
                )
            );
        });
    });

    describe('getSourceContext', () => {
        let loader: FilesystemLoader;

        beforeEach(() => {
            loader = new FilesystemLoader([], fixturesPath);
        });

        it('Should return the source context', () => {
            loader.addPath('normal');
            const source = loader.getSourceContext('index.html');
            expect(source.code).to.be.equal('path\n');
            expect(source.name).to.be.equal('index.html');
            expect(source.path).to.be.equal(
                fixturesRealPath + path.sep + 'normal' + path.sep + 'index.html'
            );
        });

        it('Should return the source context (with namespace)', () => {
            loader.addPath('named', 'named');
            const source = loader.getSourceContext('@named/index.html');
            expect(source.code).to.be.equal('named path\n');
            expect(source.name).to.be.equal('@named/index.html');
            expect(source.path).to.be.equal(
                fixturesRealPath + path.sep + 'named' + path.sep + 'index.html'
            );
        });

        it("Should throw ErrorLoader if template don't exist", () => {
            try {
                loader.getSourceContext('normal/index.html');
            } catch (error) {
                expect(error).to.be.instanceOf(LoaderError);
                expect((error as LoaderError).message).to.contain(
                    'Unable to find template'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it("Should throw LoaderError if template don't exist (with namespace)", () => {
            loader.addPath('named', 'named');
            try {
                loader.getSourceContext('@named/nowhere.html');
            } catch (error) {
                expect(error).to.be.instanceOf(LoaderError);
                expect((error as LoaderError).message).to.contain(
                    'Unable to find template "@named/nowhere.html"'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should find template with cache', () => {
            loader.addPath('normal');
            loader.addPath('named', 'named');
            const namedSource =
                loader.getSourceContext('@named/index.html').code;
            expect(namedSource).to.be.equal('named path\n');
            expect(loader.getSourceContext('index.html').code).to.be.equal(
                'path\n'
            );
        });
    });

    describe('getCacheKey', () => {
        it('Should return the cache key', () => {
            const loader = new FilesystemLoader([], fixturesPath);
            loader.addPath('normal');
            expect(loader.getCacheKey('index.html')).to.be.equal(
                'normal' + path.sep + 'index.html'
            );
        });

        it("Should throw ErrorLoader if template don't exist", () => {
            const loader = new FilesystemLoader([], fixturesPath);
            try {
                loader.getCacheKey('index.html');
            } catch (error) {
                expect(error).to.be.instanceOf(LoaderError);
                expect((error as LoaderError).message).to.contain(
                    'Unable to find template'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should remove the rootPath part if necessary', () => {
            const loader = new FilesystemLoader(
                [],
                fixturesPath + path.sep + '..'
            );
            loader.addPath('Fixtures' + path.sep + 'normal');
            expect(loader.getCacheKey('index.html')).to.be.equal(
                'Fixtures' + path.sep + 'normal' + path.sep + 'index.html'
            );
        });
    });

    describe('exists', () => {
        it('Should return true if template exist', () => {
            const loader = new FilesystemLoader([], fixturesPath);
            loader.addPath('normal');
            expect(loader.exists('index.html')).to.be.equal(true);
        });

        it("Should return false if template don't exist", () => {
            const loader = new FilesystemLoader([], fixturesPath);
            loader.addPath('normal');
            expect(loader.exists('foobar.html')).to.be.equal(false);
        });

        it('Should always return boolean', () => {
            const loader = new FilesystemLoader([], fixturesPath);
            expect(loader.exists('foo\0.twig')).to.be.equal(false);
            expect(loader.exists('../foo.twig')).to.be.equal(false);
            expect(loader.exists('@foo')).to.be.equal(false);
            expect(loader.exists('foo')).to.be.equal(false);
            expect(loader.exists('@foo/bar.twig')).to.be.equal(false);

            loader.addPath('normal');
            expect(loader.exists('index.html')).to.be.equal(true);
            loader.addPath('normal', 'foo');
            expect(loader.exists('@foo/index.html')).to.be.equal(true);
        });
    });

    describe('isFresh', () => {
        it('Should return true if mtime of template < time', () => {
            const loader = new FilesystemLoader([], fixturesPath);
            loader.addPath('normal');
            expect(loader.isFresh('index.html', Date.now() + 100)).to.be.equal(
                true
            );
        });

        it('Should return false if mtime of template > time', () => {
            const loader = new FilesystemLoader([], fixturesPath);
            loader.addPath('normal');
            expect(loader.isFresh('index.html', 100)).to.be.equal(false);
        });

        it("Should throw ErrorLoader if template don't exist", () => {
            const loader = new FilesystemLoader([], fixturesPath);
            try {
                loader.isFresh('index.html', 100);
            } catch (error) {
                expect(error).to.be.instanceOf(LoaderError);
                expect((error as LoaderError).message).to.contain(
                    'Unable to find template'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    describe('security', () => {
        let loader: FilesystemLoader;

        beforeEach(() => {
            loader = new FilesystemLoader([fixturesRealPath]);
            loader.addPath(fixturesRealPath, 'foo');
        });

        it('Should throw LoaderError if NUL bytes in template name (getSourceContext)', () => {
            try {
                loader.getSourceContext('template\0.twig');
            } catch (error) {
                expect(error).to.be.instanceOf(LoaderError);
                expect((error as LoaderError).message).to.be.equal(
                    'A template name cannot contain NUL bytes.'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should throw LoaderError if NUL bytes in template name (getCacheKey)', () => {
            try {
                loader.getCacheKey('template\0.twig');
            } catch (error) {
                expect(error).to.be.instanceOf(LoaderError);
                expect((error as LoaderError).message).to.be.equal(
                    'A template name cannot contain NUL bytes.'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        function securityTest(template: string) {
            try {
                loader.getCacheKey(template);
            } catch (error) {
                expect(error).to.be.instanceOf(LoaderError);
                expect((error as LoaderError).message).to.be.contain(
                    'Looks like you try to load a template outside configured directories'
                );
                return;
            }
            expect.fail('Should have thrown');
        }

        it('Should not access to a file outside of the path', () => {
            [
                '..\\index.html',
                '..\\\\\\index.html',
                '../index.html',
                '..////index.html',
                './../index.html',
                '.\\..\\index.html',
                '././././././../index.html',
                '.\\./.\\./.\\./../index.html',
                'foo/../../index.html',
                'foo\\..\\..\\index.html',
                'foo/../bar/../../index.html',
                'foo/bar/../../../index.html',
                'filters/../../index.html',
                'filters//..//..//index.html',
                'filters\\..\\..\\index.html',
                'filters\\\\..\\\\..\\\\index.html',
                'filters\\//../\\/\\..\\index.html',
                '/../index.html',
                '@__main__/../index.html',
                '@foo/../index.html',
                '@__main__/../../index.html',
                '@foo/../../index.html',
            ].every(template => securityTest(template));
        });
    });

    describe('ChainLoader', () => {
        it('Should return the source context', () => {
            const loader = new ChainLoader([
                new RecordLoader({ foo: 'bar' }),
                new RecordLoader({ 'errors/index.html': 'baz' }),
                new FilesystemLoader([fixturesRealPath]),
            ]);

            expect(loader.getSourceContext('foo').name).to.be.equal('foo');
            expect(loader.getSourceContext('foo').path).to.be.equal('');

            expect(
                loader.getSourceContext('errors/index.html').name
            ).to.be.equal('errors/index.html');
            expect(
                loader.getSourceContext('errors/index.html').path
            ).to.be.equal('');
            expect(
                loader.getSourceContext('errors/index.html').code
            ).to.be.equal('baz');
        });
    });
});
