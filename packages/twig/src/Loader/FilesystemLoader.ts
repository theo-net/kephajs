/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import fs from 'fs';
import path from 'path';

import { LoaderError } from '../Error/LoaderError';
import { LoaderInterface } from './LoaderInterface';
import { Source } from '../Source';

export class FilesystemLoader implements LoaderInterface {
    public static readonly MAIN_NAMESPACE = '__main__';

    protected _cache: Record<string, string> = {};

    protected _errorCache: Record<string, string> = {};

    protected _paths: Record<string, string[]> = {};

    private _rootPath = '';

    constructor(paths: string[] = [], rootPath: string | null = null) {
        this._rootPath =
            fs.realpathSync(rootPath === null ? process.cwd() : rootPath) +
            path.sep;

        if (paths) this.setPaths(paths);
    }

    getPaths(namespace = FilesystemLoader.MAIN_NAMESPACE) {
        return this._paths[namespace] ?? [];
    }

    getNamespaces() {
        return Object.keys(this._paths);
    }

    setPaths(
        paths: string | string[],
        namespace = FilesystemLoader.MAIN_NAMESPACE
    ) {
        if (!Array.isArray(paths)) paths = [paths];

        this._paths[namespace] = [];
        paths.forEach(pathTemplate => {
            this.addPath(pathTemplate, namespace);
        });
    }

    addPath(pathTemplate: string, namespace = FilesystemLoader.MAIN_NAMESPACE) {
        // invalidate the cache
        this._cache = {};
        this._errorCache = {};

        const checkPath = this._isAbsolutePath(pathTemplate)
            ? pathTemplate
            : this._rootPath + pathTemplate;

        try {
            const stat = fs.statSync(checkPath);
            if (!stat.isDirectory()) throw new Error();
        } catch (error) {
            throw new LoaderError(
                `The "${pathTemplate}" directory does not exist ("${checkPath}").`
            );
        }

        if (this._paths[namespace] === undefined) this._paths[namespace] = [];
        this._paths[namespace].push(pathTemplate.replace(/(\\|\/)+$/, ''));
    }

    prependPath(
        pathToAdd: string,
        namespace = FilesystemLoader.MAIN_NAMESPACE
    ) {
        // invalidate the cache
        this._cache = {};
        this._errorCache = {};

        const checkPath = this._isAbsolutePath(pathToAdd)
            ? pathToAdd
            : this._rootPath + pathToAdd;
        if (!fs.statSync(checkPath).isDirectory())
            throw new LoaderError(
                `The "${path}" directory does not exist ("${checkPath}").`
            );

        pathToAdd = pathToAdd.replace(/(\\|\/)+$/, '');

        if (this._paths[namespace] === undefined)
            this._paths[namespace] = [pathToAdd];
        else this._paths[namespace] = [pathToAdd, ...this._paths[namespace]];
    }

    getSourceContext(name: string) {
        const templatePath = this._findTemplate(name);
        if (templatePath === null) return new Source('', name, '');

        return new Source(
            fs.readFileSync(templatePath, 'utf-8'),
            name,
            templatePath
        );
    }

    getCacheKey(name: string) {
        const pathTemplate = this._findTemplate(name);
        if (pathTemplate === null) return '';

        if (pathTemplate.startsWith(this._rootPath))
            return pathTemplate.substring(this._rootPath.length);

        return pathTemplate;
    }

    exists(name: string) {
        name = this._normalizeName(name);

        if (this._cache[name] !== undefined) return true;

        return this._findTemplate(name, false) !== null;
    }

    isFresh(name: string, time: number) {
        const pathTemplate = this._findTemplate(name);
        if (pathTemplate === null) return false;

        return fs.statSync(pathTemplate).mtime.getTime() / 1000 < time;
    }

    protected _findTemplate(name: string, toThrow = true) {
        name = this._normalizeName(name);

        if (this._cache[name] !== undefined) return this._cache[name];

        if (this._errorCache[name] !== undefined) {
            if (!toThrow) return null;

            throw new LoaderError(this._errorCache[name]);
        }

        let namespaceParsed = '';
        let shortnameParsed = '';

        try {
            const { namespace, shortname } = this._parseName(name);
            namespaceParsed = namespace;
            shortnameParsed = shortname;

            this._validateName(shortname);
        } catch (error) {
            if (!toThrow) return null;
            throw error;
        }

        if (this._paths[namespaceParsed] === undefined) {
            this._errorCache[
                name
            ] = `There are no registered paths for namespace "${namespaceParsed}".`;

            if (!toThrow) return null;

            throw new LoaderError(this._errorCache[name]);
        }

        let findTemplate = '';
        this._paths[namespaceParsed].forEach(pathTemplate => {
            if (findTemplate !== '') return;

            if (!this._isAbsolutePath(pathTemplate))
                pathTemplate = this._rootPath + pathTemplate;

            if (
                fs.existsSync(pathTemplate + '/' + shortnameParsed) &&
                fs.statSync(pathTemplate + '/' + shortnameParsed).isFile()
            ) {
                const realpath = fs.realpathSync(
                    pathTemplate + '/' + shortnameParsed
                );
                findTemplate = this._cache[name] = realpath;
            }
        });
        if (findTemplate !== '') return findTemplate;

        this._errorCache[
            name
        ] = `Unable to find template "${name}" (looked into: ${this._paths[
            namespaceParsed
        ].join(', ')}).`;

        if (!toThrow) return null;
        throw new LoaderError(this._errorCache[name]);
    }

    private _normalizeName(name: string) {
        return name.replace(/\\/g, '/').replace(/\/{2,}/g, '/');
    }

    private _parseName(
        name: string,
        defaultNamespace = FilesystemLoader.MAIN_NAMESPACE
    ) {
        if (name[0] !== undefined && name[0] === '@') {
            const pos = name.indexOf('/');
            if (pos === -1)
                throw new LoaderError(
                    `Malformed namespaced template name "${name}" (expecting "@namespace/template_name").`
                );

            const namespace = name.substring(1, pos);
            const shortname = name.substring(pos + 1);

            return { namespace, shortname };
        }

        return { namespace: defaultNamespace, shortname: name };
    }

    private _validateName(name: string) {
        if (name.indexOf('\0') !== -1)
            throw new LoaderError('A template name cannot contain NUL bytes.');

        name = name.replace(/\\+$/, '');
        const parts = name.split('/');
        let level = 0;
        parts.forEach(part => {
            if (part === '..') --level;
            else if (part === '.') ++level;

            if (level < 0)
                throw new LoaderError(
                    `Looks like you try to load a template outside configured directories (${name}).`
                );
        });
    }

    private _isAbsolutePath(file: string) {
        return (
            file[0] === '/' ||
            file[0] === '\\' ||
            (file.length > 3 &&
                /[A-Za-z]/.test(file[0]) &&
                file[1] === ':' &&
                (file[2] === '/' || file[2] === '\\'))
        );
    }
}
