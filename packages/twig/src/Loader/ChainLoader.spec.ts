/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { RecordLoader } from './RecordLoader';
import { ChainLoader } from './ChainLoader';
import { LoaderError } from '../Error/LoaderError';

describe('@kephajs/twig/Loader/ChainLoader', () => {
    describe('getSourceContext', () => {
        it('Should return the source context', () => {
            const loader = new ChainLoader([
                new RecordLoader({ foo: 'bar' }),
                new RecordLoader({ 'errors/index.html': 'baz' }),
            ]);

            expect(loader.getSourceContext('foo').name).to.be.equal('foo');
            expect(loader.getSourceContext('foo').path).to.be.equal('');

            expect(
                loader.getSourceContext('errors/index.html').name
            ).to.be.equal('errors/index.html');
            expect(
                loader.getSourceContext('errors/index.html').path
            ).to.be.equal('');
            expect(
                loader.getSourceContext('errors/index.html').code
            ).to.be.equal('baz');
        });

        it("Should throw an LoaderError if template don't exist", () => {
            const loader = new ChainLoader([]);
            try {
                loader.getSourceContext('foobar');
            } catch (error) {
                expect(error).to.be.instanceOf(LoaderError);
                expect((error as LoaderError).message).to.contain(
                    'Template "foobar" is not defined'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    describe('getCacheKey', () => {
        it('Should return the correct cache key', () => {
            const loader = new ChainLoader([
                new RecordLoader({ foo: 'bar' }),
                new RecordLoader({ foo: 'foobar', bar: 'foo' }),
            ]);
            expect(loader.getCacheKey('foo')).to.be.equal('foo:bar');
            expect(loader.getCacheKey('bar')).to.be.equal('bar:foo');
        });

        it("Should throw an LoaderError if template don't exist", () => {
            const loader = new ChainLoader([]);
            try {
                loader.getCacheKey('foo');
            } catch (error) {
                expect(error).to.be.instanceOf(LoaderError);
                expect((error as LoaderError).message).to.contain(
                    'Template "foo" is not defined'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    describe('addLoader', () => {
        it('Should add a loader', () => {
            const loader = new ChainLoader([]);
            loader.addLoader(new RecordLoader({ foo: 'bar' }));
            expect(loader.getSourceContext('foo').code).to.be.equal('bar');
        });
    });

    describe('exists', () => {
        it('Should return if the template extist or not', () => {
            const loader = new ChainLoader([
                new RecordLoader({ bar: 'foo' }),
                new RecordLoader({ foo: 'foobar' }),
            ]);
            expect(loader.exists('foo')).to.be.equal(true);
            expect(loader.exists('bar')).to.be.equal(true);
            expect(loader.exists('foobar')).to.be.equal(false);
        });
    });
});
