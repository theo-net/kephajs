/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Source } from '../Source';
import { LoaderError } from '../Error/LoaderError';
import { LoaderInterface } from './LoaderInterface';

export class ChainLoader implements LoaderInterface {
    private _hasSourceCache: Record<string, boolean> = {};

    private _loaders: LoaderInterface[] = [];

    constructor(loaders: LoaderInterface[] = []) {
        loaders.forEach(loader => {
            this.addLoader(loader);
        });
    }

    addLoader(loader: LoaderInterface) {
        this._loaders.push(loader);
        this._hasSourceCache = {};
    }

    getLoaders() {
        return this._loaders;
    }

    getSourceContext(name: string): Source {
        const exceptions: string[] = [];
        let finded: Source | undefined;
        this._loaders.forEach(loader => {
            if (finded !== undefined || !loader.exists(name)) return;

            try {
                finded = loader.getSourceContext(name);
            } catch (error) {
                exceptions.push((error as Error).message);
            }
        });

        if (finded === undefined)
            throw new LoaderError(
                `Template "${name}" is not defined${
                    exceptions ? ' (' + exceptions.join(', ') + ')' : ''
                }.`
            );

        return finded;
    }

    exists(name: string) {
        if (this._hasSourceCache[name] !== undefined)
            return this._hasSourceCache[name];

        let finded = false;
        this._loaders.forEach(loader => {
            if (!finded && loader.exists(name)) finded = true;
        });

        return (this._hasSourceCache[name] = finded);
    }

    getCacheKey(name: string) {
        const exceptions: string[] = [];
        let finded = '';
        this._loaders.forEach(loader => {
            if (finded !== '' || !loader.exists(name)) return;

            try {
                finded = loader.getCacheKey(name);
            } catch (error) {
                exceptions.push(
                    loader.constructor.name +
                        ': ' +
                        (error as LoaderError).message
                );
            }
        });

        if (finded === '')
            throw new LoaderError(
                `Template "${name}" is not defined${
                    exceptions ? ' (' + exceptions.join(', ') + ')' : ''
                }.`
            );

        return finded;
    }

    isFresh(name: string, time: number) {
        const exceptions: string[] = [];
        let finded = false;
        let fresh = false;
        this._loaders.forEach(loader => {
            if (finded || !loader.exists(name)) return;

            try {
                fresh = loader.isFresh(name, time);
                finded = true;
            } catch (error) {
                exceptions.push(
                    loader.constructor.name +
                        ': ' +
                        (error as LoaderError).message
                );
            }
        });

        if (!finded)
            throw new LoaderError(
                `Template "${name}" is not defined${
                    exceptions ? ' (' + exceptions.join(', ') + ')' : ''
                }.`
            );

        return fresh;
    }
}
