/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { RecordLoader } from './RecordLoader';
import { LoaderError } from '../Error/LoaderError';

describe('@kephajs/twig/Loader/RecordLoader', () => {
    describe('getSourceContext', () => {
        it('Should throw LoaderError if name not registered', () => {
            const loader = new RecordLoader();
            try {
                loader.getSourceContext('foobar');
            } catch (error) {
                expect(error).to.be.instanceOf(LoaderError);
                expect((error as LoaderError).message).to.be.equal(
                    'Template "foobar" is not defined.'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Sould get the source context', () => {
            const loader = new RecordLoader();
            loader.setTemplate('foobar', 'My template');
            const source = loader.getSourceContext('foobar');
            expect(source.code).to.be.equal('My template');
            expect(source.name).to.be.equal('foobar');
            expect(source.path).to.be.equal('');
        });
    });

    describe('setTemplate', () => {
        it('Should set a template', () => {
            const loader = new RecordLoader();
            loader.setTemplate('foo', 'bar');
            expect(loader.getSourceContext('foo').code).to.be.equal('bar');
        });
    });

    describe('getCacheKey', () => {
        it('Should throw LoaderError if name not cached', () => {
            const loader = new RecordLoader();
            try {
                loader.getCacheKey('foobar');
            } catch (error) {
                expect(error).to.be.instanceOf(LoaderError);
                expect((error as LoaderError).message).to.be.equal(
                    'Template "foobar" is not defined.'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should return the cache key', () => {
            const loader = new RecordLoader({ foo: 'bar' });
            expect(loader.getCacheKey('foo')).to.be.equal('foo:bar');
        });

        it('Should work with duplicate content', () => {
            const loader = new RecordLoader({ foo: 'bar', bar: 'bar' });
            expect(loader.getCacheKey('foo')).to.be.equal('foo:bar');
            expect(loader.getCacheKey('bar')).to.be.equal('bar:bar');
        });

        it('Should be protected from edge collisions', () => {
            const loader = new RecordLoader({ foo__: 'bar', foo: '__bar' });
            expect(loader.getCacheKey('foo__')).to.be.equal('foo__:bar');
            expect(loader.getCacheKey('foo')).to.be.equal('foo:__bar');
        });
    });

    describe('isFresh', () => {
        it('Should throw LoaderError if template not registered', () => {
            const loader = new RecordLoader();
            try {
                loader.isFresh('foobar', Date.now());
            } catch (error) {
                expect(error).to.be.instanceOf(LoaderError);
                expect((error as LoaderError).message).to.be.equal(
                    'Template "foobar" is not defined.'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should always return true', () => {
            const loader = new RecordLoader({ foo: 'bar' });
            expect(loader.isFresh('foo', Date.now())).to.be.equal(true);
        });
    });
});
