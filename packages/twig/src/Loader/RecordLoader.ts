/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { Source } from '../Source';
import { LoaderError } from '../Error/LoaderError';
import { LoaderInterface } from './LoaderInterface';

export class RecordLoader implements LoaderInterface {
    constructor(private _templates: Record<string, string> = {}) {}

    setTemplate(name: string, template: string) {
        this._templates[name] = template;
    }

    getSourceContext(name: string) {
        if (this._templates[name] === undefined)
            throw new LoaderError(`Template "${name}" is not defined.`);

        return new Source(this._templates[name], name);
    }

    exists(name: string) {
        return this._templates[name] !== undefined;
    }

    getCacheKey(name: string) {
        if (this._templates[name] === undefined)
            throw new LoaderError(`Template "${name}" is not defined.`);

        return name + ':' + this._templates[name];
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    isFresh(name: string, _time: number) {
        if (this._templates[name] === undefined)
            throw new LoaderError(`Template "${name}" is not defined.`);

        return true;
    }
}
