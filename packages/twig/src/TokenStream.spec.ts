/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { SyntaxError } from './Error/SyntaxError';
import { Token } from './Token';
import { TokenStream } from './TokenStream';

describe('@kephajs/twig/TokenStream', () => {
    const tokens = [
        new Token(Token.TEXT_TYPE, 1, 1),
        new Token(Token.TEXT_TYPE, 2, 1),
        new Token(Token.TEXT_TYPE, 3, 1),
        new Token(Token.TEXT_TYPE, 4, 1),
        new Token(Token.TEXT_TYPE, 5, 1),
        new Token(Token.TEXT_TYPE, 6, 1),
        new Token(Token.TEXT_TYPE, 7, 1),
        new Token(Token.EOF_TYPE, 0, 1),
    ];

    it('Should go to the next token', () => {
        const stream = new TokenStream(tokens);
        const repr: (string | bigint | number)[] = [];
        while (!stream.isEOF()) {
            const token = stream.next();

            repr.push(token.value);
        }
        expect(repr.join(', ')).to.be.equal('1, 2, 3, 4, 5, 6, 7');
    });

    it('Should throw SyntaxError if unexpected end of template after next', () => {
        const stream = new TokenStream([
            new Token(Token.BLOCK_START_TYPE, 1, 1),
        ]);

        try {
            while (!stream.isEOF()) {
                stream.next();
            }
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            expect((error as SyntaxError).message).to.contain(
                'Unexpected end of template'
            );
            return;
        }
        expect.fail('Should have thrown');
    });

    it('Should throw SyntaxError if unexpected end of template after look', () => {
        const stream = new TokenStream([
            new Token(Token.BLOCK_START_TYPE, 1, 1),
        ]);

        try {
            while (!stream.isEOF()) {
                stream.look();
                stream.next();
            }
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            expect((error as SyntaxError).message).to.contain(
                'Unexpected end of template'
            );
            return;
        }
        expect.fail('Should have thrown');
    });
});
