/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { objectKeys, varExport } from '@kephajs/core/Helpers/Helpers';
import { Environment } from './Environment';
import { TwigNode } from './Node/TwigNode';

export class Compiler {
    private _lastLine: null | number = null;

    private _source = '';

    private _indentation = 0;

    private _debugInfo: Record<number, number> = {};

    private _sourceOffset = 0;

    private _sourceLine = 1;

    private _varNameSalt = 0;

    constructor(private _env: Environment) {}

    getEnvironment() {
        return this._env;
    }

    getSource() {
        return this._source;
    }

    compile(node: TwigNode, indentation = 0) {
        this._lastLine = null;
        this._source = '';
        this._debugInfo = {};
        this._sourceOffset = 0;
        // source code starts at 1 (as we then increment it when we encounter new lines)
        this._sourceLine = 1;
        this._indentation = indentation;
        this._varNameSalt = 0;

        node.compile(this);

        return this;
    }

    subcompile(node: TwigNode, raw = true) {
        if (raw === false) this._source += ' '.repeat(this._indentation * 4);

        node.compile(this);

        return this;
    }

    raw(str: string) {
        this._source += str;

        return this;
    }

    write(...strings: string[]) {
        strings.forEach(str => {
            this._source += ' '.repeat(this._indentation * 4) + str;
        });

        return this;
    }

    string(value: string, quote = '`') {
        this._source +=
            quote +
            value
                .replace(/\\/g, '\\')
                .replace(new RegExp(quote, 'g'), '\\' + quote)
                .replace(/\$\{/g, '\\${')
                .replace(/\t/g, '\\t') +
            quote;

        return this;
    }

    repr(value: unknown, quote?: string) {
        if (typeof value === 'number') this.raw(varExport(value));
        else if (value === null) this.raw('null');
        else if (typeof value === 'boolean') this.raw(value ? 'true' : 'false');
        else if (Array.isArray(value)) {
            this.raw('[');
            let first = true;
            (value as unknown[]).forEach(val => {
                if (!first) this.raw(', ');
                first = false;
                this.repr(val);
            });
            this.raw(')');
        } else if (typeof value === 'object') {
            this.raw('{ ');
            let first = true;
            objectKeys(value).forEach(key => {
                if (!first) this.raw(', ');
                first = false;
                this.repr(key);
                this.raw(' => ');
                this.repr((value as Record<string, unknown>)[key]);
            });
            this.raw(' }');
        } else this.string(value as string, quote);

        return this;
    }

    addDebugInfo(node: TwigNode) {
        if (node.getTemplateLine() !== this._lastLine) {
            this.write('// line ' + node.getTemplateLine() + '\n');

            this._sourceLine +=
                this._source.substring(this._sourceOffset).split('\n').length -
                1;
            this._sourceOffset = this._source.length;
            this._debugInfo[this._sourceLine] = node.getTemplateLine();

            this._lastLine = node.getTemplateLine();
        }

        return this;
    }

    getDebugInfo() {
        const acc: Record<number, number> = {};
        objectKeys(this._debugInfo)
            .sort()
            .forEach(key => {
                acc[Number(key)] = this._debugInfo[Number(key)];
            });

        return this._debugInfo;
    }

    indent(step = 1) {
        this._indentation += step;

        return this;
    }

    outdent(step = 1) {
        // can't outdent by more steps than the current indentation level
        if (this._indentation < step)
            throw new Error(
                'Unable to call outdent() as the indentation would become negative.'
            );

        this._indentation -= step;

        return this;
    }

    getVarName() {
        return '__internal_compile_' + this._varNameSalt++;
    }
}
