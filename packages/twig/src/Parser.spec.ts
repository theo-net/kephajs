/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Parser } from './Parser';
import { SyntaxError } from './Error/SyntaxError';
import { TokenStream } from './TokenStream';
import { Token } from './Token';
import { Environment } from './Environment';
import { RecordLoader } from './Loader/RecordLoader';
import { TwigNode } from './Node/TwigNode';
import { TextNode } from './Node/TextNode';
import { AbstractTokenParser } from './TokenParser/AbstractTokenParser';
import { Source } from './Source';
import { SetNode } from './Node/SetNode';

describe('@kephajs/twig/Parser', () => {
    let env: Environment;
    let parser: Parser;

    beforeEach(() => {
        env = new Environment(new RecordLoader());
        parser = new Parser(env);
    });

    it('Should trhow SyntaxError if unknown tag', () => {
        const stream = new TokenStream([
            new Token(Token.BLOCK_START_TYPE, '', 1),
            new Token(Token.NAME_TYPE, 'foo', 1),
            new Token(Token.BLOCK_END_TYPE, '', 1),
            new Token(Token.EOF_TYPE, '', 1),
        ]);

        try {
            parser.parse(stream);
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            expect((error as SyntaxError).message).to.contain(
                'Unknown "foo" tag. Did you mean "for" at line 1?'
            );
            return;
        }
        expect.fail('Should have thrown');
    });

    it('Should throw SyntaxError if unknown tag (without suggestions)', () => {
        const stream = new TokenStream([
            new Token(Token.BLOCK_START_TYPE, '', 1),
            new Token(Token.NAME_TYPE, 'foobar', 1),
            new Token(Token.BLOCK_END_TYPE, '', 1),
            new Token(Token.EOF_TYPE, '', 1),
        ]);
        try {
            parser.parse(stream);
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            expect((error as SyntaxError).message).to.contain(
                'Unknown "foobar" tag at line 1.'
            );
            return;
        }
        expect.fail('Should have thrown');
    });

    it('Should call the method _filterBodyNodes', () => {
        // to have the goo value of _index
        const emptyText = new TwigNode([new TwigNode([])]);
        emptyText.removeNode(0);

        let input: TwigNode;
        [
            [new TwigNode([new TextNode('   ', 1)]), emptyText],
            [
                (input = new TwigNode([
                    new SetNode(false, new TwigNode(), new TwigNode(), 1),
                ])),
                input,
            ],
            [
                (input = new TwigNode([
                    new SetNode(
                        true,
                        new TwigNode(),
                        new TwigNode([new TwigNode([new TextNode('foo', 1)])]),
                        1
                    ),
                ])),
                input,
            ],
        ].forEach(test => {
            expect(
                Object.getPrototypeOf(parser)._filterBodyNodes.call(
                    parser,
                    test[0]
                )
            ).to.be.deep.equal(test[1]);
        });
    });

    it('Should throw SyntaxError in filterBodyNodes', () => {
        [
            new TextNode('foo', 1),
            new TwigNode([new TwigNode([new TextNode('foo', 1)])]),
        ].forEach(node => {
            try {
                Object.getPrototypeOf(parser)._filterBodyNodes.call(
                    parser,
                    node
                );
            } catch (error) {
                expect(error).to.be.instanceOf(SyntaxError);
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    it('Should parse filter body nodes with emptyNode', () => {
        [' ', '\t', '\n', '\n\t\n   '].forEach(emptyNode => {
            Object.getPrototypeOf(parser)._filterBodyNodes.call(
                parser,
                new TextNode(emptyNode, 1)
            );
        });
    });

    it('Should be reentrant', () => {
        class TestTokenParser extends AbstractTokenParser {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            parse(_token: Token) {
                // simulate the parsing of another template right in the middle of the parsing of the current template
                this._parser.parse(
                    new TokenStream([
                        new Token(Token.BLOCK_START_TYPE, '', 1),
                        new Token(Token.NAME_TYPE, 'extends', 1),
                        new Token(Token.STRING_TYPE, 'base', 1),
                        new Token(Token.BLOCK_END_TYPE, '', 1),
                        new Token(Token.EOF_TYPE, '', 1),
                    ])
                );

                this._parser.getStream().expect(Token.BLOCK_END_TYPE);

                return new TwigNode([]);
            }

            getTag() {
                return 'test';
            }
        }

        const twig = new Environment(new RecordLoader(), {
            autoescape: false,
            optimizations: 0,
        });
        twig.addTokenParser(new TestTokenParser());

        parser = new Parser(twig);

        parser.parse(
            new TokenStream([
                new Token(Token.BLOCK_START_TYPE, '', 1),
                new Token(Token.NAME_TYPE, 'test', 1),
                new Token(Token.BLOCK_END_TYPE, '', 1),
                new Token(Token.VAR_START_TYPE, '', 1),
                new Token(Token.NAME_TYPE, 'foo', 1),
                new Token(Token.VAR_END_TYPE, '', 1),
                new Token(Token.EOF_TYPE, '', 1),
            ])
        );

        expect(parser.getParent()).to.be.equal(null);
    });

    it('Should get var name', () => {
        env.parse(
            env.tokenize(
                new Source(
                    '{% from _self import foo %}\n{% macro foo() %}\n    {{ foo }}\n{% endmacro %}\n',
                    'index'
                )
            )
        );

        // The getVarName() must not depend on the template loaders,
        // If this test does not throw any exception, that's good.
        expect(true).to.be.equal(true);
    });
});
