/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { Environment } from './Environment';
import { SyntaxError } from './Error/SyntaxError';
import { Lexer } from './Lexer';
import { RecordLoader } from './Loader/RecordLoader';
import { Source } from './Source';
import { Token } from './Token';

describe('@kephajs/twig/Lexer', () => {
    let env: Environment, lexer: Lexer;

    beforeEach(() => {
        env = new Environment(new RecordLoader());
        lexer = new Lexer(env);
    });

    function countToken(
        template: string,
        type: number,
        value: string | null = null
    ) {
        env = new Environment(new RecordLoader());
        lexer = new Lexer(env);
        const stream = lexer.tokenize(new Source(template, 'index'));

        let count = 0;
        while (!stream.isEOF()) {
            const token = stream.next();
            if (token.type === type) {
                if (null === value || value === token.value) ++count;
            }
        }

        return count;
    }

    it('Should find name label for tag', () => {
        const template = '{% § %}';
        const stream = lexer.tokenize(new Source(template, 'index'));
        stream.expect(Token.BLOCK_START_TYPE);
        expect(stream.expect(Token.NAME_TYPE).value).to.be.equal('§');
    });

    it('Should find name label for function', () => {
        const template = '{{ §() }}';
        const stream = lexer.tokenize(new Source(template, 'index'));
        stream.expect(Token.VAR_START_TYPE);
        expect(stream.expect(Token.NAME_TYPE).value).to.be.equal('§');
    });

    it('Should work with brackets nesting', () => {
        const template = '{{ {"a":{"b":"c"}} }}';
        expect(countToken(template, Token.PUNCTUATION_TYPE, '{')).to.be.equal(
            2
        );
        expect(countToken(template, Token.PUNCTUATION_TYPE, '}')).to.be.equal(
            2
        );
    });

    it('Should add TEXT_TYPE token for verbatim', () => {
        const template = 'foo{% verbatim %}b{{ a }}r{% endverbatim %}';
        const stream = lexer.tokenize(new Source(template, 'index'));

        stream.expect(Token.TEXT_TYPE, 'foo');
        stream.expect(Token.TEXT_TYPE, 'b{{ a }}r');
        // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
        // can be executed without throwing any exceptions
        expect(true).to.be.equal(true);
    });

    it('Should work with the line number directive', () => {
        const template = 'foo\nbar\n{% line 10 %}\n{{\nbaz\n}}\n';
        const stream = lexer.tokenize(new Source(template, 'index'));

        // foo\nbar\n
        expect(stream.expect(Token.TEXT_TYPE).line).to.be.equal(1);
        // \n (after {% line %})
        expect(stream.expect(Token.TEXT_TYPE).line).to.be.equal(10);
        // {{
        expect(stream.expect(Token.VAR_START_TYPE).line).to.be.equal(11);
        // baz
        expect(stream.expect(Token.NAME_TYPE).line).to.be.equal(12);
    });

    it('Should not throw error with long comments', () => {
        const template = '{# ' + '*'.repeat(100000) + ' #}';
        lexer.tokenize(new Source(template, 'index'));
        // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
        // can be executed without throwing any exceptions
        expect(true).to.be.equal(true);
    });

    it('Should not throw error with long verbatim', () => {
        const template =
            '{* verbatim *} ' + '*'.repeat(100000) + '{* endverbatim *}';
        lexer.tokenize(new Source(template, 'index'));
        // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
        // can be executed without throwing any exceptions
        expect(true).to.be.equal(true);
    });

    it('Should not throw error with long var', () => {
        const template = '{{ ' + 'a'.repeat(100000) + ' }}';
        lexer.tokenize(new Source(template, 'index'));
        // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
        // can be executed without throwing any exceptions
        expect(true).to.be.equal(true);
    });

    it('Should not throw error with long block', () => {
        const template = '{% ' + 'a'.repeat(100000) + ' %}';
        lexer.tokenize(new Source(template, 'index'));
        // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
        // can be executed without throwing any exceptions
        expect(true).to.be.equal(true);
    });

    it('Should not throw error with big number', () => {
        const template = '{{ 922337203685477580700 }}';
        const stream = lexer.tokenize(new Source(template, 'index'));
        stream.next();
        expect(stream.next().value).to.be.equal(922337203685477580800n);
    });

    it('Should not throw error with string with escaped delimiter', () => {
        const tests = [
            ["{{ 'foo \\' bar' }}", "foo ' bar"],
            ['{{ "foo \\" bar" }}', 'foo " bar'],
        ];
        tests.forEach(test => {
            const stream = lexer.tokenize(new Source(test[0], 'index'));
            stream.expect(Token.VAR_START_TYPE);
            stream.expect(Token.STRING_TYPE, test[1]);
        });
        // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
        // can be executed without throwing any exceptions
        expect(true).to.be.equal(true);
    });

    it('Should not throw error with string with interpolation', () => {
        const template = 'foo {{ "bar #{ baz + 1 }" }}';
        const stream = lexer.tokenize(new Source(template, 'index'));

        stream.expect(Token.TEXT_TYPE, 'foo ');
        stream.expect(Token.VAR_START_TYPE);
        stream.expect(Token.STRING_TYPE, 'bar ');
        stream.expect(Token.INTERPOLATION_START_TYPE);
        stream.expect(Token.NAME_TYPE, 'baz');
        stream.expect(Token.OPERATOR_TYPE, '+');
        stream.expect(Token.NUMBER_TYPE, '1');
        stream.expect(Token.INTERPOLATION_END_TYPE);
        stream.expect(Token.VAR_END_TYPE);

        // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
        // can be executed without throwing any exceptions
        expect(true).to.be.equal(true);
    });

    it('Should not throw error with string with escaped interpolation', () => {
        const template = '{{ "bar \\#{baz+1}" }}';
        const stream = lexer.tokenize(new Source(template, 'index'));

        stream.expect(Token.VAR_START_TYPE);
        stream.expect(Token.STRING_TYPE, 'bar #{baz+1}');
        stream.expect(Token.VAR_END_TYPE);

        // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
        // can be executed without throwing any exceptions
        expect(true).to.be.equal(true);
    });

    it('Should not throw error with string with hash', () => {
        const template = '{{ "bar # baz" }}';
        const stream = lexer.tokenize(new Source(template, 'index'));

        stream.expect(Token.VAR_START_TYPE);
        stream.expect(Token.STRING_TYPE, 'bar # baz');
        stream.expect(Token.VAR_END_TYPE);

        // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
        // can be executed without throwing any exceptions
        expect(true).to.be.equal(true);
    });

    it('Should throw error if string with unterminated interpolation', () => {
        const template = '{{ "bar #{x" }}';

        try {
            lexer.tokenize(new Source(template, 'index'));
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            expect((error as SyntaxError).message).to.contain('Unclosed """');
            return;
        }
        expect.fail('Should have thrown');
    });

    it('Should not throw error with string with nested interpolation', () => {
        const template = '{{ "bar #{ "foo#{bar}" }" }}';
        const stream = lexer.tokenize(new Source(template, 'index'));

        stream.expect(Token.VAR_START_TYPE);
        stream.expect(Token.STRING_TYPE, 'bar ');
        stream.expect(Token.INTERPOLATION_START_TYPE);
        stream.expect(Token.STRING_TYPE, 'foo');
        stream.expect(Token.INTERPOLATION_START_TYPE);
        stream.expect(Token.NAME_TYPE, 'bar');
        stream.expect(Token.INTERPOLATION_END_TYPE);
        stream.expect(Token.INTERPOLATION_END_TYPE);
        stream.expect(Token.VAR_END_TYPE);

        // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
        // can be executed without throwing any exceptions
        expect(true).to.be.equal(true);
    });

    it('Should not throw error with string with nested inerpolation in block', () => {
        const template = '{% foo "bar #{ "foo#{bar}" }" %}';
        const stream = lexer.tokenize(new Source(template, 'index'));

        stream.expect(Token.BLOCK_START_TYPE);
        stream.expect(Token.NAME_TYPE, 'foo');
        stream.expect(Token.STRING_TYPE, 'bar ');
        stream.expect(Token.INTERPOLATION_START_TYPE);
        stream.expect(Token.STRING_TYPE, 'foo');
        stream.expect(Token.INTERPOLATION_START_TYPE);
        stream.expect(Token.NAME_TYPE, 'bar');
        stream.expect(Token.INTERPOLATION_END_TYPE);
        stream.expect(Token.INTERPOLATION_END_TYPE);
        stream.expect(Token.BLOCK_END_TYPE);

        // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
        // can be executed without throwing any exceptions
        expect(true).to.be.equal(true);
    });

    it('Should not throw error with operator ending with a letter at the end of a line', () => {
        const template = '{{ 1 and\n0}}';
        const stream = lexer.tokenize(new Source(template, 'index'));

        stream.expect(Token.VAR_START_TYPE);
        stream.expect(Token.NUMBER_TYPE, 1);
        stream.expect(Token.OPERATOR_TYPE, 'and');

        // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
        // can be executed without throwing any exceptions
        expect(true).to.be.equal(true);
    });

    it('Should throw error if unterminated variable', () => {
        const template = '\n\n{{\n\nbar\n\n\n';

        try {
            lexer.tokenize(new Source(template, 'index'));
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            expect((error as SyntaxError).message).to.contain(
                'Unclosed "variable" in "index" at line 3'
            );
            return;
        }
        expect.fail('Should have thrown');
    });

    it('Should throw error if unterminated block', () => {
        const template = '\n\n{%\n\nbar\n\n\n';

        try {
            lexer.tokenize(new Source(template, 'index'));
        } catch (error) {
            expect(error).to.be.instanceOf(SyntaxError);
            expect((error as SyntaxError).message).to.contain(
                'Unclosed "block" in "index" at line 3'
            );
            return;
        }
        expect.fail('Should have thrown');
    });

    it('Should not throw error with complex syntax', () => {
        const template =
            '{# comment #}{{ variable }}{% if true %}true{% endif %}';
        const stream = lexer.tokenize(new Source(template, 'index'));

        stream.expect(Token.VAR_START_TYPE);
        stream.expect(Token.NAME_TYPE, 'variable');
        stream.expect(Token.VAR_END_TYPE);
        stream.expect(Token.BLOCK_START_TYPE);
        stream.expect(Token.NAME_TYPE, 'if');
        stream.expect(Token.NAME_TYPE, 'true');
        stream.expect(Token.BLOCK_END_TYPE);
        stream.expect(Token.TEXT_TYPE, 'true');
        stream.expect(Token.BLOCK_START_TYPE);
        stream.expect(Token.NAME_TYPE, 'endif');
        stream.expect(Token.BLOCK_END_TYPE);

        // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
        // can be executed without throwing any exceptions
        expect(true).to.be.equal(true);
    });

    it('Should not throw error with overriding syntax', () => {
        const template =
            '[# comment #]{# variable #}/# if true #/true/# endif #/';

        const lexer2 = new Lexer(env, {
            tagComment: ['[#', '#]'],
            tagBlock: ['/#', '#/'],
            tagVariable: ['{#', '#}'],
        });
        const stream = lexer2.tokenize(new Source(template, 'index'));

        stream.expect(Token.VAR_START_TYPE);
        stream.expect(Token.NAME_TYPE, 'variable');
        stream.expect(Token.VAR_END_TYPE);
        stream.expect(Token.BLOCK_START_TYPE);
        stream.expect(Token.NAME_TYPE, 'if');
        stream.expect(Token.NAME_TYPE, 'true');
        stream.expect(Token.BLOCK_END_TYPE);
        stream.expect(Token.TEXT_TYPE, 'true');
        stream.expect(Token.BLOCK_START_TYPE);
        stream.expect(Token.NAME_TYPE, 'endif');
        stream.expect(Token.BLOCK_END_TYPE);

        // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
        // can be executed without throwing any exceptions
        expect(true).to.be.equal(true);
    });
});
