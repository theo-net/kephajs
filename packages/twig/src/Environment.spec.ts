/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { expect } from 'chai';

import { NullCache } from './Cache/NullCache';
import { Environment } from './Environment';
import { AbstractExtension } from './Extension/AbstractExtension';
import { CoreExtension } from './Extension/CoreExtension';
import { ExtensionInterface, Operator } from './Extension/ExtensionInterface';
import { RecordLoader } from './Loader/RecordLoader';
import { Source } from './Source';
import { Token } from './Token';

describe('@kephajs/twig/Environment', () => {
    describe('constructor', () => {
        const loader = new RecordLoader();

        it('Should register loader', () => {
            const environment = new Environment(loader);
            expect(environment.getLoader()).to.be.equal(loader);
        });

        it('Should have default options', () => {
            const environment = new Environment(loader);
            expect(environment.isDebug()).to.be.equal(false);
            expect(environment.getLocale()).to.be.equal('en-EN');
            expect(environment.isStrictVariables()).to.be.equal(false);
            expect(environment.getAutoescape()).to.be.equal('html');
            expect(environment.getCache()).to.be.equal(false);
            expect(environment.isAutoReload()).to.be.equal(false);
            expect(environment.getOptimizations()).to.be.equal(-1);
        });

        it('Should set debug', () => {
            const environment = new Environment(loader, { debug: true });
            expect(environment.isDebug()).to.be.equal(true);
        });

        it('Should set charset', () => {
            const environment = new Environment(loader, { locale: 'fr-FR' });
            expect(environment.getLocale()).to.be.equal('fr-FR');
        });

        it('Should set stricVariables', () => {
            const environment = new Environment(loader, {
                strictVariables: true,
            });
            expect(environment.isStrictVariables()).to.be.equal(true);
        });

        it('Should set autoescape', () => {
            const environment = new Environment(loader, { autoescape: 'html' });
            expect(environment.getAutoescape()).to.be.equal('html');
        });

        it('Should set cache', () => {
            const cache = new NullCache();
            const environment = new Environment(loader, { cache: cache });
            expect(environment.getCache()).to.be.equal(cache);
        });

        it('Should set autoreload', () => {
            const environment = new Environment(loader, { autoReload: true });
            expect(environment.isAutoReload()).to.be.equal(true);
        });

        it('Should set autoreload as debug value if null', () => {
            let environment = new Environment(loader, {
                debug: false,
                autoReload: null,
            });
            expect(environment.isAutoReload()).to.be.equal(false);
            environment = new Environment(loader, {
                debug: true,
                autoReload: null,
            });
            expect(environment.isAutoReload()).to.be.equal(true);
        });

        it('Should set optimizations', () => {
            const environment = new Environment(loader, { optimizations: 2 });
            expect(environment.getOptimizations()).to.be.equal(2);
        });

        it('Sould add CoreExtension', () => {
            const environment = new Environment(loader, { debug: true });
            expect(environment.getExtensions().CoreExtension).to.be.instanceOf(
                CoreExtension
            );
        });
    });

    describe('tokenize', () => {
        it('Should tokenize the source', () => {
            const template =
                '{# comment #}{{ variable }}{% if true %}true{% endif %}';
            const stream = new Environment(new RecordLoader()).tokenize(
                new Source(template, 'index')
            );

            stream.expect(Token.VAR_START_TYPE);
            stream.expect(Token.NAME_TYPE, 'variable');
            stream.expect(Token.VAR_END_TYPE);
            stream.expect(Token.BLOCK_START_TYPE);
            stream.expect(Token.NAME_TYPE, 'if');
            stream.expect(Token.NAME_TYPE, 'true');
            stream.expect(Token.BLOCK_END_TYPE);
            stream.expect(Token.TEXT_TYPE, 'true');
            stream.expect(Token.BLOCK_START_TYPE);
            stream.expect(Token.NAME_TYPE, 'endif');
            stream.expect(Token.BLOCK_END_TYPE);

            // add a dummy assertion here to satisfy chai, the only thing we want to test is that the code above
            // can be executed without throwing any exceptions
            expect(true).to.be.equal(true);
        });
    });

    describe('setCache', () => {
        it('Should register a NullCache if false', () => {
            const environment = new Environment(new RecordLoader(), {
                cache: false,
            });
            expect(environment.getCache(false)).to.be.instanceOf(NullCache);
        });

        it('Should register the cache if CacheInterface', () => {
            const cache = new NullCache();
            const environment = new Environment(new RecordLoader(), { cache });
            expect(environment.getCache(false)).to.be.equal(cache);
        });

        it('Should throw an TypeError if typeof string', () => {
            const environment = new Environment(new RecordLoader());
            try {
                environment.setCache('foobar');
            } catch (error) {
                expect(error).to.be.instanceOf(TypeError);
                expect((error as Error).message).to.contain(
                    'Use EnvironmentConsole to use a string to set the cache'
                );
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should throw an TypeError if true', () => {
            const environment = new Environment(new RecordLoader());
            try {
                environment.setCache(true);
            } catch (error) {
                expect(error).to.be.instanceOf(TypeError);
                expect((error as Error).message).to.contain(
                    'Cache can only be a string, false, or a CacheInterface implementation.'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    describe('add,set,get Extensions', () => {
        class MyExtension1 extends AbstractExtension {}
        class MyExtension2 extends AbstractExtension {}

        it('Should add an extension', () => {
            const environment = new Environment(new RecordLoader());
            const myExtension1 = new MyExtension1();
            environment.addExtension(myExtension1);
            expect(environment.getExtensions()).to.be.deep.contain({
                MyExtension1: myExtension1,
            });
        });

        it('Should set extensions', () => {
            const environment = new Environment(new RecordLoader());
            const myExtension1 = new MyExtension1();
            const myExtension2 = new MyExtension2();
            environment.setExtensions([myExtension1, myExtension2]);
            expect(environment.getExtensions()).to.be.deep.contain({
                MyExtension1: myExtension1,
                MyExtension2: myExtension2,
            });
        });
    });
    /*
    class EnvironmentTest extends TestCase
{
    public function testAutoescapeOption()
    {
        $loader = new ArrayLoader([
            'html' => '{{ foo }} {{ foo }}',
            'js' => '{{ bar }} {{ bar }}',
        ]);

        $twig = new Environment($loader, [
            'debug' => true,
            'cache' => false,
            'autoescape' => [$this, 'escapingStrategyCallback'],
        ]);

        $this->assertEquals('foo&lt;br/ &gt; foo&lt;br/ &gt;', $twig->render('html', ['foo' => 'foo<br/ >']));
        $this->assertEquals('foo\u003Cbr\/\u0020\u003E foo\u003Cbr\/\u0020\u003E', $twig->render('js', ['bar' => 'foo<br/ >']));
    }

    public function escapingStrategyCallback($name)
    {
        return $name;
    }

    public function testGlobals()
    {
        $loader = $this->createMock(LoaderInterface::class);
        $loader->expects($this->any())->method('getSourceContext')->willReturn(new Source('', ''));

        // globals can be added after calling getGlobals
        $twig = new Environment($loader);
        $twig->addGlobal('foo', 'foo');
        $twig->getGlobals();
        $twig->addGlobal('foo', 'bar');
        $globals = $twig->getGlobals();
        $this->assertEquals('bar', $globals['foo']);

        // globals can be modified after a template has been loaded
        $twig = new Environment($loader);
        $twig->addGlobal('foo', 'foo');
        $twig->getGlobals();
        $twig->load('index');
        $twig->addGlobal('foo', 'bar');
        $globals = $twig->getGlobals();
        $this->assertEquals('bar', $globals['foo']);

        // globals can be modified after extensions init
        $twig = new Environment($loader);
        $twig->addGlobal('foo', 'foo');
        $twig->getGlobals();
        $twig->getFunctions();
        $twig->addGlobal('foo', 'bar');
        $globals = $twig->getGlobals();
        $this->assertEquals('bar', $globals['foo']);

        // globals can be modified after extensions and a template has been loaded
        $arrayLoader = new ArrayLoader(['index' => '{{foo}}']);
        $twig = new Environment($arrayLoader);
        $twig->addGlobal('foo', 'foo');
        $twig->getGlobals();
        $twig->getFunctions();
        $twig->load('index');
        $twig->addGlobal('foo', 'bar');
        $globals = $twig->getGlobals();
        $this->assertEquals('bar', $globals['foo']);

        $twig = new Environment($arrayLoader);
        $twig->getGlobals();
        $twig->addGlobal('foo', 'bar');
        $template = $twig->load('index');
        $this->assertEquals('bar', $template->render([]));

        // globals cannot be added after a template has been loaded
        $twig = new Environment($loader);
        $twig->addGlobal('foo', 'foo');
        $twig->getGlobals();
        $twig->load('index');
        try {
            $twig->addGlobal('bar', 'bar');
            $this->fail();
        } catch (\LogicException $e) {
            $this->assertArrayNotHasKey('bar', $twig->getGlobals());
        }

        // globals cannot be added after extensions init
        $twig = new Environment($loader);
        $twig->addGlobal('foo', 'foo');
        $twig->getGlobals();
        $twig->getFunctions();
        try {
            $twig->addGlobal('bar', 'bar');
            $this->fail();
        } catch (\LogicException $e) {
            $this->assertArrayNotHasKey('bar', $twig->getGlobals());
        }

        // globals cannot be added after extensions and a template has been loaded
        $twig = new Environment($loader);
        $twig->addGlobal('foo', 'foo');
        $twig->getGlobals();
        $twig->getFunctions();
        $twig->load('index');
        try {
            $twig->addGlobal('bar', 'bar');
            $this->fail();
        } catch (\LogicException $e) {
            $this->assertArrayNotHasKey('bar', $twig->getGlobals());
        }

        // test adding globals after a template has been loaded without call to getGlobals
        $twig = new Environment($loader);
        $twig->load('index');
        try {
            $twig->addGlobal('bar', 'bar');
            $this->fail();
        } catch (\LogicException $e) {
            $this->assertArrayNotHasKey('bar', $twig->getGlobals());
        }
    }

    public function testExtensionsAreNotInitializedWhenRenderingACompiledTemplate()
    {
        $cache = new FilesystemCache($dir = sys_get_temp_dir().'/twig');
        $options = ['cache' => $cache, 'auto_reload' => false, 'debug' => false];

        // force compilation
        $twig = new Environment($loader = new ArrayLoader(['index' => '{{ foo }}']), $options);

        $key = $cache->generateKey('index', $twig->getTemplateClass('index'));
        $cache->write($key, $twig->compileSource(new Source('{{ foo }}', 'index')));

        // check that extensions won't be initialized when rendering a template that is already in the cache
        $twig = $this
            ->getMockBuilder(Environment::class)
            ->setConstructorArgs([$loader, $options])
            ->setMethods(['initExtensions'])
            ->getMock()
        ;

        $twig->expects($this->never())->method('initExtensions');

        // render template
        $output = $twig->render('index', ['foo' => 'bar']);
        $this->assertEquals('bar', $output);

        FilesystemHelper::removeDir($dir);
    }

    public function testAutoReloadCacheMiss()
    {
        $templateName = __FUNCTION__;
        $templateContent = __FUNCTION__;

        $cache = $this->createMock(CacheInterface::class);
        $loader = $this->getMockLoader($templateName, $templateContent);
        $twig = new Environment($loader, ['cache' => $cache, 'auto_reload' => true, 'debug' => false]);

        // Cache miss: getTimestamp returns 0 and as a result the load() is
        // skipped.
        $cache->expects($this->once())
            ->method('generateKey')
            ->willReturn('key');
        $cache->expects($this->once())
            ->method('getTimestamp')
            ->willReturn(0);
        $loader->expects($this->never())
            ->method('isFresh');
        $cache->expects($this->once())
            ->method('write');
        $cache->expects($this->once())
            ->method('load');

        $twig->load($templateName);
    }

    public function testAutoReloadCacheHit()
    {
        $templateName = __FUNCTION__;
        $templateContent = __FUNCTION__;

        $cache = $this->createMock(CacheInterface::class);
        $loader = $this->getMockLoader($templateName, $templateContent);
        $twig = new Environment($loader, ['cache' => $cache, 'auto_reload' => true, 'debug' => false]);

        $now = time();

        // Cache hit: getTimestamp returns something > extension timestamps and
        // the loader returns true for isFresh().
        $cache->expects($this->once())
            ->method('generateKey')
            ->willReturn('key');
        $cache->expects($this->once())
            ->method('getTimestamp')
            ->willReturn($now);
        $loader->expects($this->once())
            ->method('isFresh')
            ->willReturn(true);
        $cache->expects($this->atLeastOnce())
            ->method('load');

        $twig->load($templateName);
    }

    public function testAutoReloadOutdatedCacheHit()
    {
        $templateName = __FUNCTION__;
        $templateContent = __FUNCTION__;

        $cache = $this->createMock(CacheInterface::class);
        $loader = $this->getMockLoader($templateName, $templateContent);
        $twig = new Environment($loader, ['cache' => $cache, 'auto_reload' => true, 'debug' => false]);

        $now = time();

        $cache->expects($this->once())
            ->method('generateKey')
            ->willReturn('key');
        $cache->expects($this->once())
            ->method('getTimestamp')
            ->willReturn($now);
        $loader->expects($this->once())
            ->method('isFresh')
            ->willReturn(false);
        $cache->expects($this->once())
            ->method('write');
        $cache->expects($this->once())
            ->method('load');

        $twig->load($templateName);
    }

    public function testHasGetExtensionByClassName()
    {
        $twig = new Environment($this->createMock(LoaderInterface::class));
        $twig->addExtension($ext = new EnvironmentTest_Extension());
        $this->assertSame($ext, $twig->getExtension('Twig\Tests\EnvironmentTest_Extension'));
        $this->assertSame($ext, $twig->getExtension('\Twig\Tests\EnvironmentTest_Extension'));
    }

    public function testAddExtension()
    {
        $twig = new Environment($this->createMock(LoaderInterface::class));
        $twig->addExtension(new EnvironmentTest_Extension());

        $this->assertArrayHasKey('test', $twig->getTokenParsers());
        $this->assertArrayHasKey('foo_filter', $twig->getFilters());
        $this->assertArrayHasKey('foo_function', $twig->getFunctions());
        $this->assertArrayHasKey('foo_test', $twig->getTests());
        $this->assertArrayHasKey('foo_unary', $twig->getUnaryOperators());
        $this->assertArrayHasKey('foo_binary', $twig->getBinaryOperators());
        $this->assertArrayHasKey('foo_global', $twig->getGlobals());
        $visitors = $twig->getNodeVisitors();
        $found = false;
        foreach ($visitors as $visitor) {
            if ($visitor instanceof EnvironmentTest_NodeVisitor) {
                $found = true;
            }
        }
        $this->assertTrue($found);
    }

    public function testAddMockExtension()
    {
        $extension = $this->createMock(ExtensionInterface::class);
        $loader = new ArrayLoader(['page' => 'hey']);

        $twig = new Environment($loader);
        $twig->addExtension($extension);

        $this->assertInstanceOf(ExtensionInterface::class, $twig->getExtension(\get_class($extension)));
        $this->assertTrue($twig->isTemplateFresh('page', time()));
    }

    public function testOverrideExtension()
    {
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage('Unable to register extension "Twig\Tests\EnvironmentTest_Extension" as it is already registered.');

        $twig = new Environment($this->createMock(LoaderInterface::class));

        $twig->addExtension(new EnvironmentTest_Extension());
        $twig->addExtension(new EnvironmentTest_Extension());
    }

    public function testAddRuntimeLoader()
    {
        $runtimeLoader = $this->createMock(RuntimeLoaderInterface::class);
        $runtimeLoader->expects($this->any())->method('load')->willReturn(new EnvironmentTest_Runtime());

        $loader = new ArrayLoader([
            'func_array' => '{{ from_runtime_array("foo") }}',
            'func_array_default' => '{{ from_runtime_array() }}',
            'func_array_named_args' => '{{ from_runtime_array(name="foo") }}',
            'func_string' => '{{ from_runtime_string("foo") }}',
            'func_string_default' => '{{ from_runtime_string() }}',
            'func_string_named_args' => '{{ from_runtime_string(name="foo") }}',
        ]);

        $twig = new Environment($loader);
        $twig->addExtension(new EnvironmentTest_ExtensionWithoutRuntime());
        $twig->addRuntimeLoader($runtimeLoader);

        $this->assertEquals('foo', $twig->render('func_array'));
        $this->assertEquals('bar', $twig->render('func_array_default'));
        $this->assertEquals('foo', $twig->render('func_array_named_args'));
        $this->assertEquals('foo', $twig->render('func_string'));
        $this->assertEquals('bar', $twig->render('func_string_default'));
        $this->assertEquals('foo', $twig->render('func_string_named_args'));
    }

    public function testFailLoadTemplate()
    {
        $this->expectException(RuntimeError::class);
        $this->expectExceptionMessage('Failed to load Twig template "testFailLoadTemplate.twig", index "112233": cache might be corrupted in "testFailLoadTemplate.twig".');

        $template = 'testFailLoadTemplate.twig';
        $twig = new Environment(new ArrayLoader([$template => false]));
        $twig->loadTemplate($twig->getTemplateClass($template), $template, 112233);
    }

    public function testUndefinedFunctionCallback()
    {
        $twig = new Environment($this->createMock(LoaderInterface::class));
        $twig->registerUndefinedFunctionCallback(function (string $name) {
            if ('dynamic' === $name) {
                return new TwigFunction('dynamic', function () { return 'dynamic'; });
            }

            return false;
        });

        $this->assertNull($twig->getFunction('does_not_exist'));
        $this->assertInstanceOf(TwigFunction::class, $function = $twig->getFunction('dynamic'));
        $this->assertSame('dynamic', $function->getName());
    }

    public function testUndefinedFilterCallback()
    {
        $twig = new Environment($this->createMock(LoaderInterface::class));
        $twig->registerUndefinedFilterCallback(function (string $name) {
            if ('dynamic' === $name) {
                return new TwigFilter('dynamic', function () { return 'dynamic'; });
            }

            return false;
        });

        $this->assertNull($twig->getFilter('does_not_exist'));
        $this->assertInstanceOf(TwigFilter::class, $filter = $twig->getFilter('dynamic'));
        $this->assertSame('dynamic', $filter->getName());
    }

    public function testUndefinedTokenParserCallback()
    {
        $twig = new Environment($this->createMock(LoaderInterface::class));
        $twig->registerUndefinedTokenParserCallback(function (string $name) {
            if ('dynamic' === $name) {
                $parser = $this->createMock(TokenParserInterface::class);
                $parser->expects($this->once())->method('getTag')->willReturn('dynamic');

                return $parser;
            }

            return false;
        });

        $this->assertNull($twig->getTokenParser('does_not_exist'));
        $this->assertInstanceOf(TokenParserInterface::class, $parser = $twig->getTokenParser('dynamic'));
        $this->assertSame('dynamic', $parser->getTag());
    }

    protected function getMockLoader($templateName, $templateContent)
    {
        $loader = $this->createMock(LoaderInterface::class);
        $loader->expects($this->any())
          ->method('getSourceContext')
          ->with($templateName)
          ->willReturn(new Source($templateContent, $templateName));
        $loader->expects($this->any())
          ->method('getCacheKey')
          ->with($templateName)
          ->willReturn($templateName);

        return $loader;
    }
}

class EnvironmentTest_Extension_WithGlobals extends AbstractExtension
{
    public function getGlobals()
    {
        return [
            'foo_global' => 'foo_global',
        ];
    }
}

class EnvironmentTest_Extension extends AbstractExtension implements GlobalsInterface
{
    public function getTokenParsers(): array
    {
        return [
            new EnvironmentTest_TokenParser(),
        ];
    }

    public function getNodeVisitors(): array
    {
        return [
            new EnvironmentTest_NodeVisitor(),
        ];
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('foo_filter'),
        ];
    }

    public function getTests(): array
    {
        return [
            new TwigTest('foo_test'),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('foo_function'),
        ];
    }

    public function getOperators(): array
    {
        return [
            ['foo_unary' => []],
            ['foo_binary' => []],
        ];
    }

    public function getGlobals(): array
    {
        return [
            'foo_global' => 'foo_global',
        ];
    }
}

class EnvironmentTest_TokenParser extends AbstractTokenParser
{
    public function parse(Token $token): Node
    {
    }

    public function getTag(): string
    {
        return 'test';
    }
}

class EnvironmentTest_NodeVisitor implements NodeVisitorInterface
{
    public function enterNode(Node $node, Environment $env): Node
    {
        return $node;
    }

    public function leaveNode(Node $node, Environment $env): ?Node
    {
        return $node;
    }

    public function getPriority(): int
    {
        return 0;
    }
}

class EnvironmentTest_ExtensionWithoutRuntime extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('from_runtime_array', ['Twig\Tests\EnvironmentTest_Runtime', 'fromRuntime']),
            new TwigFunction('from_runtime_string', 'Twig\Tests\EnvironmentTest_Runtime::fromRuntime'),
        ];
    }
}

class EnvironmentTest_Runtime
{
    public function fromRuntime($name = 'bar')
    {
        return $name;
    }
}*/

    it('Should throw an TypeError if get invalid operators from extension', () => {
        class InvalidOperatorExtension implements ExtensionInterface {
            constructor(private _operators: Record<string, Operator>[]) {}

            getTokenParsers() {
                return {};
            }

            getNodeVisitors() {
                return [];
            }

            getFilters() {
                return {};
            }

            getTests() {
                return {};
            }

            getFunctions() {
                return {};
            }

            getOperators() {
                return this._operators;
            }
        }
        try {
            const env = new Environment(new RecordLoader());
            env.addExtension(
                new InvalidOperatorExtension([1, 2, 3] as unknown as Record<
                    string,
                    Operator
                >[])
            );
            env.getUnaryOperators();
        } catch (error) {
            expect(error).to.be.instanceOf(TypeError);
            expect((error as TypeError).message).to.be.equal(
                '"InvalidOperatorExtension.getOperators()" must return an array of 2 elements, got 3.'
            );
            return;
        }
        expect.fail('Should have thrown');
    });
});
