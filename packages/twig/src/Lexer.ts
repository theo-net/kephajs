/*
 * @kephajs/twig
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rewrited from Twig, Fabien Potencier <fabien@symfony.com>
 */

import { objectKeys } from '@kephajs/core/Helpers/Helpers';
import * as RegExpHelpers from '@kephajs/core/Helpers/RegExpHelpers';

import { Environment } from './Environment';
import { Source } from './Source';
import { SyntaxError } from './Error/SyntaxError';
import { TokenStream } from './TokenStream';
import { Token } from './Token';

function strtr(str: string, from: string, to: string) {
    const length = from.length >= to.length ? to.length : from.length;
    for (let i = 0; i < length; i++) str = str.replace(from[i], to[i]);
    return str;
}

export type LexerOptions = {
    tagComment: [string, string];
    tagBlock: [string, string];
    tagVariable: [string, string];
    whitespaceTrim: string;
    whitespaceLineTrim: string;
    whitespaceLineChars: string;
    interpolation: [string, string];
};

export class Lexer {
    public static readonly STATE_DATA = 0;

    public static readonly STATE_BLOCK = 1;

    public static readonly STATE_VAR = 2;

    public static readonly STATE_STRING = 3;

    public static readonly STATE_INTERPOLATION = 4;

    public static readonly REGEX_NAME =
        /^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*/;

    public static readonly REGEX_NUMBER =
        /^[0-9]+(?:\.[0-9]+)?([Ee][+-][0-9]+)?/;

    public static readonly REGEX_STRING =
        /^"([^#"\\]*(?:\\.[^#"\\]*)*)"|'([^'\\]*(?:\\.[^'\\]*)*)'/g;

    public static readonly REGEX_DQ_STRING_DELIM = /"/;

    public static readonly REGEX_DQ_STRING_PART =
        /^[^#"\\\\]*(?:(?:\\\\.|#(?!{))[^#"\\\\]*)*/g;

    public static readonly PUNCTUATION = '()[]{}?:.,|';

    private _brackets: [string, number][] = [];

    private _code = '';

    private _currentVarBlockLine = -1;

    private _cursor = 0;

    private _end = 0;

    private _lineno = 0;

    private _options: LexerOptions;

    private _position = -1;

    private _positions: [string, string, string, number][] = [];

    private _regexes: {
        lexVar: RegExp;
        lexBlock: RegExp;
        lexRawData: RegExp;
        operator: RegExp;
        lexComment: RegExp;
        lexBlockRaw: RegExp;
        lexBlockLine: RegExp;
        lexTokensStart: RegExp;
        interpolationStart: RegExp;
        interpolationEnd: RegExp;
    };

    private _source: Source | undefined;

    private _state = -1;

    private _states: number[] = [];

    private _tokens: Token[] = [];

    constructor(
        private _env: Environment,
        options: Partial<LexerOptions> = {}
    ) {
        this._options = {
            tagComment: ['{#', '#}'],
            tagBlock: ['{%', '%}'],
            tagVariable: ['{{', '}}'],
            whitespaceTrim: '-',
            whitespaceLineTrim: '~',
            whitespaceLineChars: ' \t\0\x0B',
            interpolation: ['#{', '}'],
            ...options,
        };

        this._regexes = {
            // }}
            lexVar: new RegExp(
                '^\\s*(?:' +
                    RegExpHelpers.escape(
                        this._options.whitespaceTrim +
                            this._options.tagVariable[1]
                    ) +
                    '\\s*' + // -}}\s*
                    '|' +
                    RegExpHelpers.escape(
                        this._options.whitespaceLineTrim +
                            this._options.tagVariable[1]
                    ) +
                    '[' +
                    this._options.whitespaceLineChars +
                    ']*' + // ~}}[ \t\0\x0B]*
                    '|' +
                    RegExpHelpers.escape(this._options.tagVariable[1]) +
                    ')' // }}
            ),

            // %}
            lexBlock: new RegExp(
                '^\\s*(?:' +
                    RegExpHelpers.escape(
                        this._options.whitespaceTrim + this._options.tagBlock[1]
                    ) +
                    '\\s*\n?' + // -%}\s*\n?
                    '|' +
                    RegExpHelpers.escape(
                        this._options.whitespaceLineTrim +
                            this._options.tagBlock[1]
                    ) +
                    '[' +
                    this._options.whitespaceLineChars +
                    ']*' + // ~%}[ \t\0\x0B]*
                    '|' +
                    RegExpHelpers.escape(this._options.tagBlock[1]) +
                    '\n?)' // %}\n?
            ),

            // {% endverbatim %}
            lexRawData: new RegExp(
                RegExpHelpers.escape(this._options.tagBlock[0]) + // {%
                    '(' +
                    this._options.whitespaceTrim + // -
                    '|' +
                    this._options.whitespaceLineTrim + // ~
                    ')?\\s*endverbatim\\s*' +
                    '(?:' +
                    RegExpHelpers.escape(
                        this._options.whitespaceTrim + this._options.tagBlock[1]
                    ) +
                    '\\s*' + // -%}
                    '|' +
                    RegExpHelpers.escape(
                        this._options.whitespaceLineTrim +
                            this._options.tagBlock[1]
                    ) +
                    '[' +
                    this._options.whitespaceLineChars +
                    ']*' + // ~%}[ \t\0\x0B]*
                    '|' +
                    RegExpHelpers.escape(this._options.tagBlock[1]) +
                    ')' // %}
            ),

            operator: this._getOperatorRegex(),

            // #}
            lexComment: new RegExp(
                '(?:' +
                    RegExpHelpers.escape(
                        this._options.whitespaceTrim +
                            this._options.tagComment[1]
                    ) +
                    '\\s*\n?' + // -#}\s*\n?
                    '|' +
                    RegExpHelpers.escape(
                        this._options.whitespaceLineTrim +
                            this._options.tagComment[1]
                    ) +
                    '[' +
                    this._options.whitespaceLineChars +
                    ']*' + // ~#}[ \t\0\x0B]*
                    '|' +
                    RegExpHelpers.escape(this._options.tagComment[1]) +
                    '\n?)' // #}\n?
            ),

            // verbatim %}
            lexBlockRaw: new RegExp(
                '^\\s*verbatim\\s*(?:' +
                    RegExpHelpers.escape(
                        this._options.whitespaceTrim + this._options.tagBlock[1]
                    ) +
                    '\\s*' + // -%}\s*
                    '|' +
                    RegExpHelpers.escape(
                        this._options.whitespaceLineTrim +
                            this._options.tagBlock[1]
                    ) +
                    '[' +
                    this._options.whitespaceLineChars +
                    ']*' + // ~%}[ \t\0\x0B]*
                    '|' +
                    RegExpHelpers.escape(this._options.tagBlock[1]) +
                    ')' // %}
            ),

            lexBlockLine: new RegExp(
                '^\\s*line\\s+(\\d+)\\s*' +
                    RegExpHelpers.escape(this._options.tagBlock[1])
            ),

            // {{ or {% or {#
            lexTokensStart: new RegExp(
                '(' +
                    RegExpHelpers.escape(this._options.tagVariable[0]) + // {{
                    '|' +
                    RegExpHelpers.escape(this._options.tagBlock[0]) + // {%
                    '|' +
                    RegExpHelpers.escape(this._options.tagComment[0]) + // {#
                    ')(' +
                    RegExpHelpers.escape(this._options.whitespaceTrim) + // -
                    '|' +
                    RegExpHelpers.escape(this._options.whitespaceLineTrim) +
                    ')?', // ~
                'g'
            ),
            interpolationStart: new RegExp(
                '^' +
                    RegExpHelpers.escape(this._options.interpolation[0]) +
                    '\\s*'
            ),
            interpolationEnd: new RegExp(
                '^\\s*' + RegExpHelpers.escape(this._options.interpolation[1])
            ),
        };
    }

    tokenize(source: Source): TokenStream {
        this._source = source;
        this._code = source.code.replace(/\r\n/g, '\n').replace(/\r/g, '\n');
        this._cursor = 0;
        this._lineno = 1;
        this._end = this._code.length;
        this._tokens = [];
        this._state = Lexer.STATE_DATA;
        this._states = [];
        this._brackets = [];
        this._position = -1;
        this._positions = [];

        // find all token starts in one go
        let result: RegExpExecArray | null = null;
        while ((result = this._regexes.lexTokensStart.exec(this._code))) {
            this._positions.push([
                result[0],
                result[1],
                result[2],
                result.index,
            ]);
        }

        while (this._cursor < this._end) {
            // dispatch to the lexing functions depending
            // on the current state
            switch (this._state) {
                case Lexer.STATE_DATA:
                    this._lexData();
                    break;

                case Lexer.STATE_BLOCK:
                    this._lexBlock();
                    break;

                case Lexer.STATE_VAR:
                    this._lexVar();
                    break;

                case Lexer.STATE_STRING:
                    this._lexString();
                    break;

                case Lexer.STATE_INTERPOLATION:
                    this._lexInterpolation();
                    break;
            }
        }
        this._pushToken(Token.EOF_TYPE);
        if (this._brackets.length > 0) {
            const bracketPoped = this._brackets.pop() as [string, number];
            throw new SyntaxError(
                `Unclosed "${bracketPoped[0]}".`,
                bracketPoped[1],
                this._source
            );
        }

        return new TokenStream(this._tokens, this._source);
    }

    private _lexData() {
        // if no matches are left we return the rest of the template as simple text token
        if (this._position === this._positions.length - 1) {
            this._pushToken(
                Token.TEXT_TYPE,
                this._code.substring(this._cursor)
            );
            this._cursor = this._end;

            return;
        }

        // Find the first token after the current cursor
        let position = this._positions[++this._position];
        while (position[3] < this._cursor) {
            if (this._position === this._positions.length - 1) return;
            position = this._positions[++this._position];
        }

        // push the template text first
        const textContent = this._code.substring(this._cursor, position[3]);
        let text = textContent;

        // trim?
        if (this._positions[this._position][2] !== undefined) {
            if (
                this._positions[this._position][2] ===
                this._options.whitespaceTrim
            ) {
                // whitespaceTrim detected ({%-, {{- or {#-)
                text = text.trimEnd();
            } else if (
                this._positions[this._position][2] ===
                this._options.whitespaceLineTrim
            ) {
                // whitespaceLineTrim detected ({%~, {{~ or {#~) don't trim \r and \n
                text = text.replace(/( |\t|\0)+$/, '');
            }
        }
        this._pushToken(Token.TEXT_TYPE, text);
        this._moveCursor(textContent + position[0]);

        let match;
        switch (this._positions[this._position][1]) {
            case this._options.tagComment[0]:
                this._lexComment();
                break;

            case this._options.tagBlock[0]:
                // raw data?
                if (
                    (match = this._code
                        .substring(this._cursor)
                        .match(this._regexes.lexBlockRaw))
                ) {
                    this._moveCursor(match[0]);
                    this._lexRawData();
                }
                // {% line \d+ %}
                else if (
                    (match = this._code
                        .substring(this._cursor)
                        .match(this._regexes.lexBlockLine))
                ) {
                    this._moveCursor(match[0]);
                    this._lineno = Number(match[1]);
                } else {
                    this._pushToken(Token.BLOCK_START_TYPE);
                    this._pushState(Lexer.STATE_BLOCK);
                    this._currentVarBlockLine = this._lineno;
                }
                break;

            case this._options.tagVariable[0]:
                this._pushToken(Token.VAR_START_TYPE);
                this._pushState(Lexer.STATE_VAR);
                this._currentVarBlockLine = this._lineno;
                break;
        }
    }

    private _lexBlock() {
        let match;
        if (
            this._brackets.length === 0 &&
            (match = this._code
                .substring(this._cursor)
                .match(this._regexes.lexBlock))
        ) {
            this._pushToken(Token.BLOCK_END_TYPE);
            this._moveCursor(match[0]);
            this._popState();
        } else this._lexExpression();
    }

    private _lexVar() {
        let match;
        if (
            this._brackets.length === 0 &&
            (match = this._code
                .substring(this._cursor)
                .match(this._regexes.lexVar))
        ) {
            this._pushToken(Token.VAR_END_TYPE);
            this._moveCursor(match[0]);
            this._popState();
        } else this._lexExpression();
    }

    private _lexExpression() {
        let match;
        // whitespace
        if ((match = this._code.substring(this._cursor).match(/^\s+/))) {
            this._moveCursor(match[0]);

            if (this._cursor >= this._end)
                throw new SyntaxError(
                    `Unclosed "${
                        Lexer.STATE_BLOCK === this._state ? 'block' : 'variable'
                    }".`,
                    this._currentVarBlockLine,
                    this._source
                );
        }

        // arrow function
        if (
            '=' === this._code[this._cursor] &&
            '>' === this._code[this._cursor + 1]
        ) {
            this._pushToken(Token.ARROW_TYPE, '=>');
            this._moveCursor('=>');
        }
        // operators
        else if (
            (match = this._code
                .substring(this._cursor)
                .match(this._regexes.operator))
        ) {
            this._pushToken(Token.OPERATOR_TYPE, match[0].replace(/\s+/g, ' '));
            this._moveCursor(match[0]);
        }
        // names
        else if (
            (match = this._code.substring(this._cursor).match(Lexer.REGEX_NAME))
        ) {
            this._pushToken(Token.NAME_TYPE, match[0]);
            this._moveCursor(match[0]);
        }
        // numbers
        else if (
            (match = this._code
                .substring(this._cursor)
                .match(Lexer.REGEX_NUMBER))
        ) {
            let number = Number(match[0]);
            if (/\+|-/.test(match[0])) number = eval(match[0]) as number;
            this._pushToken(
                Token.NUMBER_TYPE,
                number > Number.MAX_SAFE_INTEGER
                    ? BigInt(number.toFixed())
                    : number
            );
            this._moveCursor(match[0]);
        }
        // punctuation
        else if (Lexer.PUNCTUATION.indexOf(this._code[this._cursor]) > -1) {
            // opening bracket
            if ('([{'.indexOf(this._code[this._cursor]) > -1)
                this._brackets.push([this._code[this._cursor], this._lineno]);
            // closing bracket
            else if (')]}'.indexOf(this._code[this._cursor]) > -1) {
                if (this._brackets.length === 0)
                    throw new SyntaxError(
                        `Unexpected "${this._code[this._cursor]}".`,
                        this._lineno,
                        this._source
                    );

                const bracket = this._brackets.pop() as [string, number];
                if (
                    this._code[this._cursor] !== strtr(bracket[0], '([{', ')]}')
                )
                    throw new SyntaxError(
                        `Unclosed "${bracket[0]}".`,
                        bracket[1],
                        this._source
                    );
            }
            this._pushToken(Token.PUNCTUATION_TYPE, this._code[this._cursor]);
            ++this._cursor;
        }
        // strings
        else if (
            (match = this._code
                .substring(this._cursor)
                .match(Lexer.REGEX_STRING))
        ) {
            this._pushToken(
                Token.STRING_TYPE,
                match[0]
                    .substring(1, match[0].length - 1)
                    .replace(/\\'/g, "'")
                    .replace(/\\"/g, '"')
                    .replace(/\\#{/g, '#{')
            );
            this._moveCursor(match[0]);
        }
        // opening double quoted string
        else if (
            (match = this._code
                .substring(this._cursor)
                .match(Lexer.REGEX_DQ_STRING_DELIM))
        ) {
            this._brackets.push(['"', this._lineno]);
            this._pushState(Lexer.STATE_STRING);
            this._moveCursor(match[0]);
        }
        // unlexable
        else {
            throw new SyntaxError(
                `Unexpected character "${this._code[this._cursor]}".`,
                this._lineno,
                this._source
            );
        }
    }

    private _lexRawData() {
        let match;
        if (
            !(match = this._code
                .substring(this._cursor)
                .match(this._regexes.lexRawData))
        )
            throw new SyntaxError(
                'Unexpected end of file: Unclosed "verbatim" block.',
                this._lineno,
                this._source
            );

        let text = this._code.substring(
            this._cursor,
            this._cursor + (match.index ?? 0)
        );
        this._moveCursor(text + match[0]);

        // trim?
        if (match[1] !== undefined) {
            if (this._options.whitespaceTrim === match[1]) {
                // whitespace_trim detected ({%-, {{- or {#-)
                text = text.trimEnd();
            } else {
                // whitespace_line_trim detected ({%~, {{~ or {#~)
                // don't trim \r and \n
                text = text.replace(/( |\t|\0)+$/, '');
            }
        }
        this._pushToken(Token.TEXT_TYPE, text);
    }

    private _lexComment() {
        let match;
        if (
            !(match = this._code
                .substring(this._cursor)
                .match(this._regexes.lexComment))
        )
            throw new SyntaxError(
                'Unclosed comment.',
                this._lineno,
                this._source
            );

        this._moveCursor(
            this._code.substring(
                this._cursor,
                this._cursor + (match.index ?? 0)
            ) + match[0]
        );
    }

    private _lexString() {
        let match;
        if (
            (match = this._code
                .substring(this._cursor)
                .match(this._regexes.interpolationStart))
        ) {
            this._brackets.push([this._options.interpolation[0], this._lineno]);
            this._pushToken(Token.INTERPOLATION_START_TYPE);
            this._moveCursor(match[0]);
            this._pushState(Lexer.STATE_INTERPOLATION);
        } else if (
            (match = this._code
                .substring(this._cursor)
                .match(Lexer.REGEX_DQ_STRING_PART)) &&
            match[0].length > 0
        ) {
            this._pushToken(
                Token.STRING_TYPE,
                match[0].replace(/\\'/g, "'").replace(/\\"/g, '"')
            );
            this._moveCursor(match[0]);
        } else if (
            (match = this._code
                .substring(this._cursor)
                .match(Lexer.REGEX_DQ_STRING_DELIM))
        ) {
            const bracket = this._brackets.pop() as [string, number];
            if (this._code[this._cursor] !== '"')
                throw new SyntaxError(
                    `Unclosed "${bracket[0]}".`,
                    bracket[1],
                    this._source
                );

            this._popState();
            ++this._cursor;
        } else {
            // unlexable
            throw new SyntaxError(
                `Unexpected character "${this._code[this._cursor]}".`,
                this._lineno,
                this._source
            );
        }
    }

    private _lexInterpolation() {
        const bracket = this._brackets[this._brackets.length - 1];
        let match;
        if (
            this._options.interpolation[0] === bracket[0] &&
            (match = this._code
                .substring(this._cursor)
                .match(this._regexes.interpolationEnd))
        ) {
            this._brackets.pop();
            this._pushToken(Token.INTERPOLATION_END_TYPE);
            this._moveCursor(match[0]);
            this._popState();
        } else this._lexExpression();
    }

    private _pushToken(type: number, value: string | number | bigint = '') {
        // do not push empty text tokens
        if (type === Token.TEXT_TYPE && value === '') return;

        this._tokens.push(new Token(type, value, this._lineno));
    }

    private _moveCursor(text: string) {
        this._cursor += text.length;
        this._lineno += text.split('\n').length - 1;
    }

    private _getOperatorRegex() {
        const operators: Record<string, number> = {};
        [
            '=',
            ...objectKeys(this._env.getUnaryOperators()),
            ...objectKeys(this._env.getBinaryOperators()),
        ]
            .sort((a, b) => b.length - a.length)
            .forEach(operator => {
                operators[operator] = operator.length;
            });

        const regex: string[] = [];
        objectKeys(operators).forEach(operator => {
            // an operator that ends with a character must be followed by
            // a whitespace, a parenthesis, an opening map [ or sequence {
            let r = RegExpHelpers.escape(operator);
            if (/[A-Za-z]/.test(operator[operator.length - 1])) {
                r += '(?=[\\s()[{])';
            }

            // an operator that begins with a character must not have a dot or pipe before
            if (/[A-Za-z]/.test(operator[0])) r = '(?<![.|])' + r;

            // an operator with a space can be any amount of whitespaces
            r = r.replace(/\s+/g, '\\s+');

            regex.push(r);
        });

        return new RegExp('^(?:' + regex.join('|') + ')');
    }

    private _pushState(state: number) {
        this._states.push(this._state);
        this._state = state;
    }

    private _popState() {
        if (this._states.length === 0)
            throw new Error('Cannot pop state without a previous state.');

        this._state = this._states.pop() as number;
    }
}
