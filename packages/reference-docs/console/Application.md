# Console application

To make an console application, you need to use the `@kephajs/console/Application` class:

```ts
const app = new Application(__dirname); // We give the root folder to the framework
```

Note that this class extends `@kephajs/core/Application`, as you can read the documentation relative to this class to know more.

## `package.json`

If a `package.json` is accessible, the framework will use the values of `description`, `name` and `version` to configure your application. You can also overwrite the value with the `options`.

## Methods for console application

-   `loadJsonConfig` that load an Json file to the config service. The file it's loaded from the `appRoot` and the keys can be prefixed.
-   `getJsonLoaded` returns the list of the json files loaded
-   `exit` exit the application (you can pass the return code as parameter)
-   `run` the application with the command passed as console argument or the default command

## Run the application

When you call the method `run`, the application will use the `$commandRouter` service to get a command. It will use the first argument of the command line to determine the command to launch. If no argument, the default command will be runned.

        $ node app.js myCommand otherArg --option

Launch the command `myCommand` with the argument `otherArg` and the option `option = true`.

All non cached error will be cached by the method `run` and show to the `stdout error`. The application will also return the return code of the `_execute` method (you need to return a `number` or a `Promise<number>`) of the commands or the value of property `exitCode` of the throwed error or `1` if this property don't exist.

For `UnknownCommand` and `UnknownOptionError` some suggestions has showed.

## Exit the application

To exit the application and return a code, you can call the `exit` method. It will also call the `shutdown` method to finish corectly your application.

The registered values in the profiler will be print if the environnement is `developpement` and if the verbosity is debug.
