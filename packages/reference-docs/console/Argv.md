# Argv parsing

The framework can parse the command line arguments. After, you can access to the arguments with the `$argv` service.

The numbers are detected and will be `number` after parsing and not `string`.

They are two type of options:

-   long option: they start with `--`
-   short option: they start with `-`

An option is set to `true` as value, but if it's followed by a value, the option will take a value:

```ts
// --foo
{ foo: true };

// -fb
{ f: true, b: true };

// --foo=bar
// --foo bar
{ foo: 'bar' };

// -fb=bar
// -fb bar
{ f: true, b: 'bar' };
```

All not reconized options are put in `argv._`:

```ts
// --name John Doe
{ name: 'John', _: ['Doe']};

// -a -- b
{ a: true, _: ['b'] }; // `--` is skipped
```

## Nested dotted objects

The arguments can be parse to an nested dotted object:

```
node myApp.js --foo.bar 3 --foo.baz 4 --foo.quux.quibble 5 --foo.quux.oO --beep.boop
```

give:

```ts
argv = {
    foo: {
        bar: 3,
        baz: 4,
        quux: {
            quibble: 5,
            oO: true,
        },
    },
    beep: {
        boop: true,
    },
};
```

## Stop early

If the `stopEarly` option is set to `true`, the parsing is stoped on the first non-option:

```ts
parseArgv.setOptions({ stopEarly: true });
// --aaa bbb ccc --ddd
{
    aaa: 'bbb',
    _: ['ccc', '--ddd'],
};
```

## `--` option

If `--` option is set to `true`, the arguments after `--` are put into `argv['--']`:

```ts
parseArgv.setOptions({ '--': true });
// --name John before -- after
{
    name: 'John',
    _: ['before'],
    '--': ['after'],
}
```

## `argv` application option

If they are no argument, the framework read the `argv` option.

```ts
new Application('name', {
    argv: ['-b', '--foo', 'bar'],
});
// {
//     b: true,
//     foo: bar,
//     _: [],
// }
```
