# Helpers

The framework give some helpers for console applications.

## `NodeVersion`

Return the node version.

## `strlenMaxLineStylized`

Like `@kephajs/core/Helpers/Helpers/strlenMaxLine` but remove the stylization (cf. `Output`).

## `wordWrap`

Like `@kephajs/core/Helpers/Helpers/wordWrap` but remove the stylization (cf. `Output`).

Two parameters:

-   `maxLength`: the max line length
-   `inputStr`: the string to wrap

## `truncateWidthStylized`

Truncate a string

Parameters:

-   `inputStr`: the string to truncate
-   `desiredLength`: the desired line length
-   `strlen: (str: string) => number`: function to get the string length (default: `strlenMaxLineStylized`)

## `truncateStylized`

Truncate a string and add a final char as `…` if needed.

Parameters:

-   `inputStr`: the string to truncate
-   `desiredLength`: the desired line length
-   `truncateChar`: the final char to add (default: `…`)
-   `strlen: (str: string) => number`: function to get the string length (default: `strlenMaxLineStylized`)
-   `truncateFct`: function for the truncation (default: `truncateWidthStylized`)

**Warning:** By default the function take the max line string length and not the string length (`Hel\nlo` will return 3 and not 6!)

## `Filesystem`

### `rmrf`

Delete a directory, the subdirectories and the files.
