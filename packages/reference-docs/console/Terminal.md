# Terminal

You can get the width and the height of your terminal with the methods `getWidth()` and `getHeight()` of the object `Terminal`.

If we can't read the value, the defaults are return:

-   width: 80
-   height: 50
