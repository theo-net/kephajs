# The console output

The framework give two services to print message on the console:

-   `$ouput` print in the standard output `stdout` as `console.log()`
-   `$outputErr` print ine the standard error output `stderr` as `console.error()`

The only difference is the stream the service will write to (`process.stdout` or `process.stderr`).

You can get the stream with the method `getStream`.

## Write in the console output

You have two methods:

-   `write` outputs a message without adding a "\n" at the end of the line
-   `writeln` outputs a message followed by a "\n"

You can also give an array to `writeln`: each element will be print in a new line.

```ts
$output.write('Hello ');
$output.write('World!\n');
$output.writeln('Do you like...');
$output.writeln([' - apple?', ' - orange?']);
```

will show:

```
Hello World!
Do you like...
 - apple?
 - orange?
```

`write` also accept messages as array and have an optional parameter to add new line.

```ts
$output.write(['Hello', ' World', '!']);
$output.write('...');
$output.write('Cool', true);
$output.writeln('End');
```

```
Hello World!...Cool
End
```

## Color the Console Output

By default, the framework determines whether or not coloring is available in the output.

You have two methods to known the colorization informations:

-   `$output.getColorDepth()` returns what colors the terminal supports (`ColorDepth.C16M` 16,777,216 colors supported, `ColorDepth.C256` 256, `ColorDepth.C16` 16, `ColorDepth.TWO` 2 as disable colors)
-   `$output.hasColorSupport()` returns `true` if the output supports colorization

You can use env variables to force the values:

-   `FORCE_COLOR`: 0 to disable colors, 1 to set to 16 colors, 2 to set to 256 colors, 3 to set to 16M colors
-   `NO_COLOR` and `NODE_DISABLE_COLORS` disables colorization

### Create a style

To use the object `Color`, you can set a foreground and a background color. You can also set options.

```ts
new Color('foreground-color', 'background-color', ['option1', 'option2']);
```

The color can be:

-   an empty string `''` to dont change the color
-   one of `black`, `red`, `green`, `yellow`, `blue`, `magenta`, `cyan`, `white`, `default`, `gray`, `bright-red`, `bright-green`, `bright-yellow`, `bright-blue`, `bright-magenta`, `bright-cyan`, `bright-white`.
-   an hexa code with 3 digits like `#ABC`
-   an hexa code with 6 digits like `#ABCDEF`

For the two last options, you need to set the parameter `trueColor` to `true`:

```ts
new Color('...', '...', [], true);
```

The option can be: `bold`, `underscore`, `blink`, `reverse`, `conceal`.

But usually, we use the `OuputFormatterStyle`;

```ts
const formatter = new OutputFormatterStyle('blue', 'yellow', ['bold']);
formatter.setHref('https://www.theo-net.org');
```

#### Service `$outputFormatterStyle`

This service register the styles and format and wrap a text.

```ts
$outputFormatter.setStyle('style-name', new OutputFormatterStyle('red'));
console.log($outputFormatter.format('Hello <style-name>World</style-name>!'));
```

They are some methods:

-   `setDecorated` set if the output is decorated ord not (stylised)
-   `setStyle`, `getStyle`, `hasStyle`
-   `format` format a text
-   `formatAndWrap` format and wrap a style

You can wrap a text with the second paramteter `width` of `formatAndWrap`: the text will wrapped with `width` as max line length.

You can also prefix the lines with the third parameter `prefix`:

```ts
$outputFormatter.format('text', 10, ' - '); // all line prefixed by ' - '
$outputFromatter.format('text', 10, {
    first: ' - ', // first line prefixed by ' -'
    other: ' * ', // other line prefixed by ' * '
});
```

Default registered styles:

-   `error`
-   `info`
-   `comment`
-   `question`

## Access with output

The framework give you an access to the formatter with the services `$output` and `$outputErr`.

Whenever you output text, you can surround the text with tags to color its output. For example:

```ts
// green text
$output.writeln('<info>foo</info>');

// yellow text
$output.writeln('<comment>foo</comment>');

// black text on a cyan background
$output.writeln('<question>foo</question>');

// white text on a red background
$output.writeln('<error>foo</error>');
```

The closing tag can be replaced by `</>`, which revokes all formatting options established by the last opened tag.

It is possible to define your own styles using the `OutputFormatterStyle` class:

```ts
// ...
$outputStyle = new OutputFormatterStyle('red', '#ff0', ['bold', 'blink']);
$output.getFormatter().setStyle('fire', $outputStyle);

$output.writeln('<fire>foo</>');
```

Any hex color is supported for foreground and background colors. Besides that, these named colors are supported: `black`, `red`, `green`, `yellow`, `blue`, `magenta`, `cyan`, `white`, `gray`, `bright-red`, `bright-green`, `bright-yellow`, `bright-blue`, `bright-magenta`, `bright-cyan` and `bright-white`.

If the terminal doesn't support true colors, the nearest named color is used. E.g. `#c0392b` is degraded to `red` or `#f1c40f` is degraded to `yellow`.

And available options are: `bold`, `underscore`, `blink`, `reverse` (enables the "reverse video" mode where the background and foreground colors are swapped) and conceal (sets the foreground color to transparent, making the typed text invisible - although it can be selected and copied; this option is commonly used when asking the user to type sensitive information).

You can also set these colors and options directly inside the tag name:

```ts
// using named colors
$output.writeln('<fg=green>foo</>');

// using hexadecimal colors
$output.writeln('<fg=#c0392b>foo</>');

// black text on a cyan background
$output.writeln('<fg=black;bg=cyan>foo</>');

// bold text on a yellow background
$output.writeln('<bg=yellow;options=bold>foo</>');

// bold text with underscore
$output.writeln('<options=bold,underscore>foo</>');
```

If you need to render a tag literally, escape it with a backslash: `\<info>`.

Commands can use the special `<href>` tag to display links similar to the `<a>` elements of web pages:

```ts
$output.writeln('<href=https://theo-net.org>Théo-Net Homepage</>');
```

If your terminal belongs to the list of terminal emulators that support links you can click on the "Théo-Net Homepage" text to open its URL in your default browser. Otherwise, you'll see "Théo-Net Homepage" as regular text and the URL will be lost.

## Output Sections

The regular console output can be divided into multiple independent regions called "output sections". Create one or more of these sections when you need to clear and overwrite the output information.

Sections are created with the `Output.section()` method, which returns an instance of `SectionOutput`:

```ts
section1 = $output.section();
section2 = $output.section();

section1.writeln('Hello');
section2.writeln('World!');
// Output displays "Hello\nWorld!\n"

// overwrite() replaces all the existing section contents with the given content
section1.overwrite('Goodbye');
// Output now displays "Goodbye\nWorld!\n"

// clear() deletes all the section contents...
section2.clear();
// Output now displays "Goodbye\n"

// ...but you can also delete a given number of lines
// (this example deletes the last two lines of the section)
section1.clear(2);
// Output is now completely empty!
```

A new line is appended automatically when displaying information in a section.

Output sections let you manipulate the Console output in advanced ways, such as displaying multiple progress bars which are updated independently and appending rows to tables that have already been rendered.

## `NullOutput`

The special `NullOutput` just display nothing. It will throw an `TypeError` if you want to set a stream.

Why use it? Just if you want to make some action very quiet without changing the general verbosity of your application: you just need to change witch output is used (the regular or the null).

## `escape`

Escapes "<" and ">" special chars in given text.

```ts
OutputFormatter.escape('');
// =>
```

## `OutputFormatter.stylizeLines`

This static method takes a `string[]` and returns a new `string[]` with each line independently styled.

```ts
OutputFormatter.stylizeLine(['<error>Hello', 'World!</error>']);
// => ['<error>Hello</>', '<error>World!</>']
```
