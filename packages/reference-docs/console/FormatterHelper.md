# Formatter Helper

The Formatter helper provides functions to format the output with colors.

The methods return a string, which you'll usually render to the console by passing it to the `Outputwriteln` method.

The Formatter helper can be instatiate directly or be accessible by the method `getHelper` of your commands.

## TabulateList

To generate tabulate lists.

### Basic usage

```ts
const tabulateList = new TabulateList();
tabulateList.push(
    ['Hello', 'world!'],
    ['Nice to', 'meet', 'you'],
    ["I'm", 'Gregory']
);
tabulateList.render();

// Hello    world!
// Nice to  meet     you
// I'm      Gregory
```

You can also get a new `TabulateList` with the method `tabulateList` of the `FormatterHelper` service.

### Margin

To define the length of the margin, use the options:

```ts
const tabulateList = new TabulateList({ margin: 4 });
tabulateList.push(
    ['Hello', 'world!'],
    ['Nice to', 'meet', 'you'],
    ["I'm", 'Gregory']
);
console.log(tabulateList.render());

// Hello      world!
// Nice to    meet       you
// I'm        Gregory
```

The content can be stylized:

```ts
const tabulateList = new TabulateList({ margin: 4 });
tabulateList.push(
    ['Hello', 'world!'],
    ['Nice <notice>to</>', 'meet', 'you'],
    ["I'm", 'Gregory']
);
console.log(tabulateList.render());

// Hello      world!
// Nice <notice>to</>    meet       you
// I'm        Gregory
```

If you give an `Output` instance to the `render` method, the tabulate list will be print on the output.

```ts
...
tabulateList($output);
```

### Prefix

You can define a prefix for each column. If you don't need it for the last columns, you don't need to define one.

```ts
const tabulateList = new TabulateList({ prefix: ['- ', '...'] });
tabulateList.push(
    ['Hello', 'world!', 'Men'],
    ['Nice to', 'meet', 'you'],
    ["I'm", 'Gregory']
);
tabulateList.render();

// - Hello    ...world!   Men
// - Nice to  ...meet     you
// - I'm      ...Gregory
```

You can also add a prefix for one element:

```ts
const tabulateList = new TabulateList({ prefix: ['- ', '...'] });
tabulateList.push(
    ['Hello', 'world!', 'Men'],
    ['Nice to', { content: 'meet', prefix: '->', style: '' }, 'you'],
    ["I'm", 'Gregory']
);
tabulateList.render();

// - Hello    ...world!   Men
// - Nice to  ->meet      you
// - I'm      ...Gregory
```

### Styles

You can define a style for each line as the prefix:

```ts
const tabulateList = new TabulateList({
    prefix: ['- ', '...'],
    styles: ['error', 'bg=blue'],
});
tabulateList.push(
    ['Hello', 'world!', 'Men'],
    ['Nice to', 'meet', 'you'],
    ["I'm", 'Gregory']
);
tabulateList.render();

// <error>- Hello</>    <bg=blue>...world!</>   Men
// <error>- Nice to</>  <bg=blue>...meet</>     you
// <error>- I'm</>      <bg=blue>...Gregory</>
```

The prefix will be also stylized.

You can also add a style for one element:

```ts
const tabulateList = new TabulateList({
    prefix: ['- ', '...'],
    styles: ['error', 'bg=blue'],
});
tabulateList.push(
    ['Hello', 'world!', 'Men'],
    ['Nice to', { content: 'meet', prefix: '', style: 'bg=green' }, 'you'],
    ["I'm", 'Gregory']
);
tabulateList.render();

// <error>- Hello</>    <bg=blue>...world!</>   Men
// <error>- Nice to</>  <bg=green>...meet</>     you
// <error>- I'm</>      <bg=blue>...Gregory</>
```

## Print Messages in a Section

KephaJs offers a defined style when printing a message that belongs to some "section". It prints the section in color and with brackets around it and the actual message to the right of this. Minus the color, it looks like this:

    [SomeSection] Here is some message related to that section

To reproduce this style, you can use the `formatSection` method:

```ts
formattedLine = formatter.formatSection(
    'SomeSection',
    'Here is some message related to that section'
);
$output.writeln(formattedLine);
```

You have also a third parameter to define the style (default: `info`).

## Print Messages in a Block

Sometimes you want to be able to print a whole block of text with a background color. KephaJs uses this when printing error messages.

If you print your error message on more than one line manually, you will notice that the background is only as long as each individual line. Use the formatBlock() to generate a block output:

```ts
errorMessages = ['Error!', 'Something went wrong'];
formattedBlock = formatter.formatBlock(errorMessages, 'error');
$output.writeln(formattedBlock);
```

As you can see, passing an array of messages to the `formatBlock` method creates the desired output. If you pass `true` as third parameter, the block will be formatted with more padding (one blank line above and below the messages and 2 spaces on the left and right).

The exact "style" you use in the block is up to you. In this case, you're using the pre-defined error style, but there are other styles, or you can create your own.
