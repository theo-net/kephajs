# Table

When building a console application it may be useful to display tabular data:

```
+------------+--------------+--------------------------+
| Date       | Author       | Message                  |
+------------+--------------+--------------------------+
| 2018-11-25 | John Doe     | Hello World!             |
| 2020-06-12 | Michael Bond | How are you?             |
| 2022-08-03 | George Peter | I'm Georges              |
| 2022-02-19 | William Away | Bryan is in the kitchen  |
+---------------+-----------+--------------------------+
```

## Basic usage

To generate a table:

```ts
const table = new Table({ head: ['a', 'b'], compact: true });

table.push(['c', 'd'], ['e', 'f']);
```

For the render:

```ts
table.render($output);
```

The result will be:

```
+---+---+
| a | b |
+---+---+
| c | d |
| e | f |
+---+---+
```

The `output` parameter of `render` is optional. In all case, the method returns the `string` result of the rendering:

```ts
console.log(table.render());
```

The cell content can be stylized and after showing with the `$output` service:

```ts
const table = new Table();
table.push([
    '<error>Hello\nhow\nare\nyou?</error>',
    '<info>I\nam\nfine\nthanks!</>',
]);
table.render($output);
```

With `width` you can get the width of the table:

```ts
const table = new Table({ head: ['a', 'b'], compact: true });
table.push(['c', 'd'], ['e', 'f']);
console.log(table.width); // 9
```

## Table options

When you create a new table, you can give some options:

-   `style: string | TableStyle`, the style of your table (cf `TableManager`)
-   `compact: boolean` if `true` disable the row separator (default: `false`)
-   `head: string[]` the content of the header
-   `colWidths: number[]` force the widths of the columns
-   `rowHeights: number[]` force the heights of the rows
-   `colAligns: ('left' | 'center' | 'right')[]` the alignement of the columns
-   `rowAligns: ('top' | 'middle' | 'bottom')[]` the alignements of the rows
-   `hAlign: 'left' | 'center' | 'right'` the default horizontal align (default: `left`)
-   `vAlign: 'top' | 'middle' | 'bottom'` the default vertical align (default: `top`)
-   `wordWrap: boolean` if the wordwrap is enabled or not (default: `false`)

```ts
const table = new Table({
    head: ['Foo', 'Bar'],
    compact: true,
    wordWrap: true,
});
```

## Cell content

When you push data, you have two way: add a `string | number | boolean | null | undefined`, add a cell descriptor:

```ts
table.push(['Its a string', { content: "It's a cell descriptor" }]);
```

If the content is a `boolean`, the final content will be `'true'` or `'false'`. If it's `null` or `undefined`, it will be `''`.

The cell descriptior car have this properties:

-   `content` a `string` with the content of the cell
-   `head: boolean` if the cell is a cell header or not (default: `false`)
-   `style: TableStyle | string` the cell style
-   `colSpan: number`
-   `rowSpan: number`
-   `hAlign: 'left' | 'center' | 'right'`
-   `vAlign: 'top' | 'middle' | 'bottom'`

## Table Manager

The framework declare a service `$tableManager` that is a instance of `TableManager`. This service is initialized with the `$output` service.

With the table manager, you can register a style or get it:

```ts
myStyle = new TableStyle();
$tableManager.addStyle('style-name', myStyle);
$tableManager.getStyle('style-name'); // return myStyle
```

Note: `addStyle` will erase a previous registered style with the same name.

Note: `getStyle` will throw `UnknownStyleError` if the manager isn't able to find a style with this name.

You can also get a new table with the method `table`: the first parameter is the style name and the second is the options for the table. If the style name is `undefined`, the table will have the default styles.

```ts
$tableManager.table(); // default styles, default options
$tableManager.table(undefined, { head: ['a', 'b'] }); // default styles, options
$tableManager.table('myStyle', { head: ['a', 'b'] }); // styles 'myStyle', options
```

You can also set the second parameter of the `Table` constructor to give the Table Manager:

```ts
const table = new Table(undefined, new TableManager());
```

Will also throw `UnknownStyleError` if the manager isn't able to find a style with this name.

### Default registered styles

They are different styles already registered:

For this examples, you have:

```ts
// 1
const table = new Table({ head: ['a', 'b'], compact: true });
table.push(['c', 'd'], ['e', 'f']);

// 2
const table = new Table({ head: ['', 'a', 'b'], compact: true });
table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });

// 3
const table = new Table({ head: ['a', 'b'] });
table.push(['c', 'd'], ['e', 'f']);

// 4
const table = new Table({ head: ['', 'a', 'b'] });
table.push({ c: ['d', 'e'] }, { f: ['g', 'h'] });
```

`default`

```
// compact: true            compact: false
+---+---+       +---+---+   +---+---+       +---+---+
| a | b |       | a | b |   | a | b |       | a | b |
+---+---+   +---+---+---+   +---+---+   +---+---+---+
| c | d |   | c | d | e |   | c | d |   | c | d | e |
| e | f |   | f | g | h |   +---+---+   +---+---+---+
+---+---+   +---+---+---+   | e | f |   | f | g | h |
                            +---+---+   +---+---+---+
```

`compact`

```
// compact: true  compact: false
a b      a b      a b      a b
c d    c d e      c d    c d e
e f    f g h      e f    f g h
```

`borderless`

```
// compact: true    compact: false
== ==      == ==    == ==      == ==
aa bb      aa bb    aa bb      aa bb
== ==   == == ==    == ==   == == ==
cc dd   cc dd ee    cc dd   cc dd ee
ee ff   ff gg hh    -- --   -- -- --
== ==   == == ==    ee ff   ff gg hh
                    == ==   == == ==
```

`box`

```
// compact: true            compact: false
┌───┬───┐       ┌───┬───┐   ┌───┬───┐       ┌───┬───┐
│ a │ b │       │ a │ b │   │ a │ b │       │ a │ b │
├───┼───┤   ┌───┼───┼───┤   ├───┼───┤   ┌───┼───┼───┤
│ c │ d │   │ c │ d │ e │   │ c │ d │   │ c │ d │ e │
│ e │ f │   │ f │ g │ h │   ├╌╌╌┼╌╌╌┤   ├╌╌╌┼╌╌╌┼╌╌╌┤
└───┴───┘   └───┴───┴───┘   │ e │ f │   │ f │ g │ h │
                            └───┴───┘   └───┴───┴───┘
```

`dashed`

```
// compact: true            compact: false
╭╌╌╌┬╌╌╌╮       ╭╌╌╌┬╌╌╌╮   ╭╌╌╌┬╌╌╌╮       ╭╌╌╌┬╌╌╌╮
╎ a ╎ b ╎       ╎ a ╎ b ╎   ╎ a ╎ b ╎       ╎ a ╎ b ╎
├╌╌╌┼╌╌╌┤   ╭╌╌╌┼╌╌╌┼╌╌╌┤   ├╌╌╌┼╌╌╌┤   ╭╌╌╌┼╌╌╌┼╌╌╌┤
╎ c ╎ d ╎   ╎ c ╎ d ╎ e ╎   ╎ c ╎ d ╎   ╎ c ╎ d ╎ e ╎
╎ e ╎ f ╎   ╎ f ╎ g ╎ h ╎   ├╌╌╌┼╌╌╌┤   ├╌╌╌┼╌╌╌┼╌╌╌┤
╰╌╌╌┴╌╌╌╯   ╰╌╌╌┴╌╌╌┴╌╌╌╯   ╎ e ╎ f ╎   ╎ f ╎ g ╎ h ╎
                            ╰╌╌╌┴╌╌╌╯   ╰╌╌╌┴╌╌╌┴╌╌╌╯
```

`box-heavy`

```
// compact: true            compact: false
┏━━━┯━━━┓       ┏━━━┯━━━┓   ┏━━━┯━━━┓       ┏━━━┯━━━┓
┃ a │ b ┃       ┃ a │ b ┃   ┃ a │ b ┃       ┃ a │ b ┃
┣━━━┿━━━┫   ┏━━━╋━━━┿━━━┫   ┣━━━┿━━━┫   ┏━━━╋━━━┿━━━┫
┃ c │ d ┃   ┃ c ┃ d │ e ┃   ┃ c │ d ┃   ┃ c ┃ d │ e ┃
┃ e │ f ┃   ┃ f ┃ g │ h ┃   ┠───┼───┨   ┠───╂───┼───┨
┗━━━┷━━━┛   ┗━━━┻━━━┷━━━┛   ┃ e │ f ┃   ┃ f ┃ g │ h ┃
                            ┗━━━┷━━━┛   ┗━━━┻━━━┷━━━┛
```

`box-double`

```
// compact: true            compact: false
╔═══╤═══╗       ╔═══╤═══╗   ╔═══╤═══╗       ╔═══╤═══╗
║ a │ b ║       ║ a │ b ║   ║ a │ b ║       ║ a │ b ║
╠═══╪═══╣   ╔═══╬═══╪═══╣   ╠═══╪═══╣   ╔═══╬═══╪═══╣
║ c │ d ║   ║ c ║ d │ e ║   ║ c │ d ║   ║ c ║ d │ e ║
║ e │ f ║   ║ f ║ g │ h ║   ╟───┼───╢   ╟───╫───┼───╢
╚═══╧═══╝   ╚═══╩═══╧═══╝   ║ e │ f ║   ║ f ║ g │ h ║
                            ╚═══╧═══╝   ╚═══╩═══╧═══╝
```

`rounded`

```
// compact: true            compact: false
╭───┬───╮       ╭───┬───╮   ╭───┬───╮       ╭───┬───╮
│ a │ b │       │ a │ b │   │ a │ b │       │ a │ b │
├───┼───┤   ╭───┼───┼───┤   ├───┼───┤   ╭───┼───┼───┤
│ c │ d │   │ c │ d │ e │   │ c │ d │   │ c │ d │ e │
│ e │ f │   │ f │ g │ h │   ├╌╌╌┼╌╌╌┤   ├╌╌╌┼╌╌╌┼╌╌╌┤
╰───┴───╯   ╰───┴───┴───╯   │ e │ f │   │ f │ g │ h │
                            ╰───┴───╯   ╰───┴───┴───╯
```

### Create a custom style

You can create a new style with TableStyle:

```ts
const myStyle = new TableStyle();
```

The constructor can have three parameters:

-   the chars
-   the styles
-   the padding
-   if the table is compact or not (display or not the horizontal border inside the table)

You have also these getters and setters:

-   `defineChars`
-   `getChars`
-   `setCompact`
-   `isCompact`
-   `defineStyles`
-   `getStyles`
-   `definePaddings`
-   `getPaddings`

For the chars:

```ts
/**
 * 6=====3=====5==============5============7
 * 1           2              2            1
 * 12====3=====11=============11==========13
 * 1           2              2            1
 * 15----4-----14-------------14----------16
 * 1           2              2            1
 * 15----4-----14-------------14----------16
 * 1           2              2            1
 * 9=====3=====8==============8===========10
 *
 *
 *             6=======3======5============7
 *             1              2            1
 * 6=====3=====17=============11==========13
 * 1           1              2            1
 * 15----4-----19-------------14----------16
 * 1           1              2            1
 * 15----4-----19-------------14----------16
 * 1           1              2            1
 * 9=====3=====18=============8===========10
 *
 */
export type TableStyleChars = {
    verticalOutside: string; // 1
    verticalInside: string; // 2
    horizontalOutside: string; // 3
    horizontalInside: string; // 4
    topMid: string; // 5
    topLeft: string; // 6
    topRight: string; // 7
    bottomMid: string; // 8
    bottomLeft: string; // 9
    bottomRight: string; // 10
    midMid: string; // 11
    midLeft: string; // 12
    midRight: string; // 13
    mid: string; // 14
    left: string; // 15
    right: string; // 16
    crossIntersection: string; // 17
    crossBottom: string; // 18
    crossMid: string; // 19
    truncate: string;
    padding: string;
};
```

For the styles:

```ts
export type TableStyleStyles = {
    head: string | undefined; // default: undefined
    cell: string | undefined; // default: undefined
    borderOutside: string | undefined; // default: undefined
    borderInside: string | undefined; // default: undefined
};
```

The `string` for the styles reffered to the `OutputFormatter` styles.

For the paddings:

```ts
export type TableStylePaddings = {
    left: number; // default: 1
    right: number; // default: 1
};
```

## Horizontal header, vertical header and cross table

The vertical header can be define with the property `head` of the table options. To create horizontal header, use an object to declare the row.

```ts
const table = Table();
table.push({ 'v0.1': ['Foobar'] }, { 'v0.2': ['Test'] });

//    +------+--------+
//    | v0.1 | Foobar |
//    +------+--------+
//    | v0.2 | Test   |
//    +------+--------+
```

You can also create cross table if you define vertical and horizontal headers.

```ts
const table = Table({ head: ['', 'Header 1', 'Header 2'] }); // Note the first empty header
table.push(
    { 'Header 3': ['v0.1', 'Foobar'] },
    { 'Header 4': ['v0.2', 'Test'] }
);

//               +----------+----------+
//               | Header 1 | Header 2 |
//    +----------+----------+----------+
//    | Header 3 | v0.1     | Foobar   |
//    +----------+-----------+---------+
//    | Header 4 | v0.2     | Test     |
//    +----------+----------+----------+
```

## Wordwrap

If you define `wordWrap` to `true`, you active the automatic word wrapping:

```ts
//    +-------+---------+
//    | Hello | I am    |
//    | how   | fine    |
//    | are   | thanks! |
//    | you?  |         |
//    +-------+---------+

const table = new Table({
    colWidths: [7, 9],
    wordWrap: true,
});
table.push(['Hello how are you?', 'I am fine thanks!']);
```

## ColSpan and rowSpan

In the cell declaration, you can define col and row spanning.

```ts
//    +-------+---------------+
//    | hello | greetings     |
//    +---------------+-------+
//    | greetings     | hello |
//    +-------+-------+-------+
//    | final | hello | howdy |
//    +-------+-------+-------+

const table = new Table();
table.push(
    ['hello', { colSpan: 2, content: 'greetings' }],
    [{ colSpan: 2, content: 'greetings' }, 'howdy'],
    ['final', 'hello', 'howdy']
);
```

```ts
//    +-------------------------------+
//    | greetings |           | hello |
//    |           | greetings +-------+
//    |           |           | howdy |
//    +-------------------------------+

const table = new Table();
table.push(
    [
        { rowSpan: 2, content: 'greetings' },
        { rowSpan: 2, content: 'greetings', vAlign: 'center' },
        'hello',
    ],
    ['howdy']
);
```

```ts
//    +-------+-----------+-----------+
//    | hello | greetings |           |
//    +-------+           |           |
//    | howdy |           | greetings |
//    +-------+-----------+-----------+

const table = new Table();
table.push(
    [
        'hello',
        { rowSpan: 2, content: 'greetings' },
        { rowSpan: 2, content: 'greetings', vAlign: 'bottom' },
    ],
    ['howdy']
);
```

Of course, you can mix colSpan and rowSpan:

```ts
//    +-------+-----+----+
//    | hello | sup | hi |
//    +-------+     |    |
//    | howdy |     |    |
//    +---+---+--+--+----+
//    | o | k |  |  |    |
//    +---+---+--+--+----+

const table = new Table();
table.push(
    [
        { content: 'hello', colSpan: 2 },
        { rowSpan: 2, colSpan: 2, content: 'sup' },
        { rowSpan: 3, content: 'hi' },
    ],
    [{ content: 'howdy', colSpan: 2 }],
    ['o', 'k', '', '']
);
```

If needed, the layout manager create the missing cells:

```ts
//    +---+---+
//    | a | b |
//    |   +---+
//    |   |   |
//    |   |   |
//    |   |   |
//    +---+---+

const table = new Table();

// Just 2 celss are created, but the layout manager will generate 3
table.push([{ content: 'a', rowSpan: 3, colSpan: 2 }, 'b'], [], []);
```

// TODO

## Row heights and col widths

By default, the width of the columns and the heights of the rows are calculated automatically based on their contents.

But you can specify the columns widths with `colWidths` and the row heights with `rowHeights`. The `truncate` will be used.

```ts
//    +-------+
//    | hello |
//    | hi…   |
//    +-------+

const table = new Table({ rowHeights: [2] });
table.push(['hello\nhi\nsup']);
```

If `colWidths` isn't defined, the layout manager ajust the content to align the cells.

You can define the cell width for the first col, the other will be automaticly adjusted.

```ts
//    +-------------+
//    | hello there |
//    +----+--------+
//    | hi |   hi   |
//    +----|--------+

const table = new Table({ colWidths: [4] });
table.push(
    [{ colSpan: 2, content: 'hello there' }],
    ['hi', { hAlign: 'center', content: 'hi' }]
);
```

```ts
A col with `null` as width, will be adjust by the layout manager

//    +-------------+
//    | hello there |
//    +--------+----+
//    |     hi | hi |
//    +--------+----+

const table = new Table({ colWidths: [null, 4] });
table.push(
    [{ colSpan: 2, content: 'hello there' }],
    [{ hAlign: 'right', content: 'hi' }, 'hi']
);
```

## Table separator

You can add a table separator anywhere in the output by passing an instance of `TableSeparator` as a row:

```ts
//    +------------+----------------+
//    | Date       | Event          |
//    +------------+----------------+
//    | 2022-06-26 | Mega party     |
//    | 2O22-07-14 | For the french |
//    +------------+----------------+
//    | 2022-09-01 | Back to school |
//    +------------+----------------+

const table = new Table({ head: ['Date', 'Event'], compact: true });
table.push(
    ['2022-06-26', 'Mega party'],
    ['2O22-07-14', 'For the french'],
    new TableSeparator(),
    ['2022-09-01', 'Back to school']
);
```

If the table is not compact:

```ts
//    +------------+----------------+
//    | Date       | Event          |
//    +------------+----------------+
//    | 2022-06-26 | Mega party     |
//    +------------+----------------+
//    | 2O22-07-14 | For the french |
//    +------------+----------------+
//    +------------+----------------+
//    | 2022-09-01 | Back to school |
//    +------------+----------------+

const table = new Table({ head: ['Date', 'Event'] });
...
```
