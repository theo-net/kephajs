# Console Commands

You can create command line interfaces with the framework. You can also create command-line commands. Your console commands can be used for any recurring task, such as cronjobs, imports, or other batch jobs.

## Command router

The service `$commandRouter` registers all command into your application. It will be used to run commands.

To register commands, you need to use the method `addCommands` who accept an array of commands:

```ts
$commandRouter.addCommands([HelpCommand, ListCommand, VersionCommand]);
```

You can also define aliases with `addAliases`:

```ts
$commandRouter.addAliases({
    theHelp: HelpCommand,
    foobar: MyCommand,
});
// HelpCommand accessible by 'help' and 'theHelp'
```

To get all registered command, use `getCommands` who return an `Record`:

```ts
{
    commandName: {
        command: CommandClass,
        aliases: ['firstAlias', 'secondAlias'],
        cached: undefined, // can contain the instantiate command
    }
}
```

To get a command with the name, use `getCommand`. This method will instantiate the command, with dependencies injection, put the object in cache and return it. If the router not find the command, an `UnknownCommand` will be throw.

## Default commands

By default, the CommandRouter execute the help command, but you can define an other command as default with `setDefaultCommand` with the command name or the Command class. But, this command need to be added before with `addCommands`. To get the default command, use `getDefaultCommand`.

## Create a command

Each command need to extends `commands`. You can define two property:

-   `commandName` the command name
-   `description` the description of the command for the help

Because the `CommandRouter` use the `ServiceContainer` to instantiate a command, you can use dependencies injection.

You need also to define the `execute` method with your main logic.

```ts
import { Command } from '@kephajs/console/Command/Command';
import { Output } from '@kephajs/console/Output/Output';
import { Validator } from '@kephajs/core/Validate/Validator';
import { MyService } from '../Service/MyService';

export class MainCommand extends Command {
    public static readonly commandName = 'main';

    public static $inject = ['$validator', '$output', '$outputErr'];

    constructor(
        private _validator: Validator,
        private _output: Output,
        private _outputErr: Output
    ) {
        super();
    }

    protected _execute() {
        // your logic

        // this method must return an integer number with the "exit status code"
        // of the command. You can also use these constants to make code more readable

        // return this if there was no problem running the command
        // (it's equivalent to returning 0)
        return Command.SUCCESS;

        // or return this if some error happened during the execution
        // (it's equivalent to returning 1)
        // return Command.FAILURE;

        // or return this to indicate incorrect command usage; e.g. invalid options
        // or missing arguments (it's equivalent to returning 2)
        // return Command.INVALID
    }
}
```

The `_execute` method can have a parameter with the type `CommandExecuteArgs`:

```ts
type CommandExecuteArgs = {
    output?: Output;
    outputErr?: Output;
    args: Record<string, any>;
    options: Record<string, any>;
};
```

You can also define more specific type:

```ts
_execute(
    { output, outputErr, args, options}: CommandExecuteArgs
) {
    // your code
}
```

But to run a command, don't use directly the method `_execute`, use the public method `run` that test the arguments and the options. `run` take the same parameter as `_execute` and will also return the result of `_execute`.

`_execute` need to return a `number` or a `Promise<number>`. This number will be used as exit code for the application.

### Command name

When you define the command name with the public property `commandName`, you can also define a namespace for this command.

-   `app:myCommand` command `myCommand` in the namespace `app`
-   `list` command `list` in the main namespace
-   ...

The name of the command is what users type after `node myApp.js`.

The name is define by a static property, as you need after the initialization of your command to set this name with `setName`. The `CommandRouter` set the name before returning the instantiated command. After you can access to the name with the protected method `getName`.

### Description

With the static property `description` of your command, you can define a short description. After the initialization with the `CommandRouter`, you can access to the description with the method `getDescription` of your command.

### Help

You can define a custom help for your command with the method `getHelp` that need returns a `string`.

This help will be print with the command `help`.

In your help, you can use two special variables:

-   `%command.name%` the command name (cf. `getName` method)
-   `%app.bin%` the bin for your application (replaced by `$app.bin`)

Then, use the method `getProcessedHelp` to access to the help with this two variables replaced.

### Arguments and options

To add an argument or an option, ou can use the methods `addArgument` and `addOption`. who take an `Argument` object or an `Option` object.

Generally, we use these method in the command constructor. `Command` has also two getters to give the arguments and options list:

```ts
command.arguments; // Argument[]
command.options; // Options[]
```

`Argument` ans `Option` have a method `hasDefault` who return `true` if a default value is defined or `false` if not.

A variadic option or argument can have a variadic number of values. You have a method `isVariadic` that returns if the argument / the option have variable number of values.

```bash
# myScript.js <foobar...>
node myScript.js foo bar
# foobar has two values: ['foo', 'bar']

# myScript.js -f [foobar...]
node myScript.js -f=foo -f=bar
node myScript.js -f foo -f bar
# f has two values ['foo', 'bar']
```

#### Arguments

Parameters:

-   `synopsis` (`string`) the synopsis
-   `description` (`string`) the description will be used for the help, `''` by default
-   `validator` (`Type`) the validator for the value(s), facultative
-   `defaultValue` (`number | string | (number |string)[]`), facultative

Methods:

-   `isRequired` return `true` if the argument is required (the synopsis start with `<`)
-   `isOptional` return `true` if the argument is optional (the synopsis start with `[`)
-   `getType` return `Argument.REQUIRED` or `Argument.OPTIONAL`

Examples of synopsis:

```ts
'<foobar>'; // required argument
'[foobar]'; // optional argument
'<foobar...>'; // required variadic argument
'[foobar...]'; // optional variadic argument
```

**Warning** you should add the required arguments before the facultative arguments and simple arument before variadic (you can have just one)

#### Options

Parameters:

-   `synopsis` (`string`) the synopsis
-   `description` (`string`) the description will be used for the help, `''` by default
-   `defaultValue` (`number | string | (number |string)[]`), facultative
-   `validator` (`Type`) the validator for the value(s), facultative
-   `required` (`boolean`) if the option is required or not, `false`by default

The framework analyse the synopsis, in the option constructor, to configure the option:

-   option name start with `-`: is the short option name
-   option name start with `--`: is the long option name
-   value in `[]` is an optional value
-   value in `<>` is a required value
-   value followed by `...` is a variadic option

Exemples of synopsis:

```ts
'--test [lol...]'; // long option name, optional variadic value
'--test [lol]'; // long option name, optional non variadic
'--test <lol>'; // long option name, required value
'-t'; // short option name
'-t, --test [lol]'; // short and long option name, optional value
```

If they are an error in the synopsis syntax, an `OptionSyntaxError` wiil throw.

Methods:

-   `isRequired` return `true` if the option is required
-   `getValueType` return `Option.REQUIRED_VALUE` or `Option.OPTIONAL_VALUE`
-   `getLongCleanName` return the long option name
-   `getShortCleanName` return the short option name
-   `getLongOrShortCleanName` return the long option name or the short name if the first is not defined

#### Validator

To valid the values of your arguments and your options, you can give a validator to the constructors.

You can make your valdiator with `new Validator()` and the methods (`string`, `number`, ...) or use the service `$validator` if you inject it in yur constructor. With the service you can use your personalized validators.

With the validator you can transform the value with `transform` and `preprocess`. The validation system will return the new value.

### `getSynopsis`

This method return the synopsis of your command.

The synopsis is composed with the command name and the arguments name.

## Execute a command

Normally you will execute the command from the command line interface with your application. But, if you need to execute this command directly in a controller, in an other command, you have to way:

-   use the service `CommandRouter` to get the command
-   directly instantiate your command

In the both case, you need to give to the `run` method the good parameter of type `CommandRunArgs`:

```ts
type CommandRunArgs = {
    output?: Output;
    outputErr?: Output;
    args: (string | number)[] | Record<string, any>;
    options: Record<string, string | number | boolean>;
};
```

As you can see, the arguments can be an `array` or a `Record` with the key as argument's name.

When the command is runed, it will check the options and the arguments and return it to the `_execute` method.

1. if arguments in array, check the number of arguments (throw `WrongNumberOfArgumentError` if invalid) and convert to a Record
2. validate arguments, take the default value if needed (throw `MissingArgumentError` if an argument missing and `InvalidArgumentValue` if a value is invalid)
3. check if all of the required opitions are present (throw `MissingOptionError` if invalid)
4. check if the options are reconized (throw `UnknownOptionError` if invalid), take default value if needed and validate the values
5. take the long option name if defined
6. transform the options name to camelCase

When you use the command line, it's recommanded to finish by the options (arguments first, then options).

## Already defined command

-   `help` show the help
-   `list` list all application's commands
-   `version` show the version of the application

## Errors

Different types of errors can be throw.

### `UnknownCommand`

Throw when you try to get a command that not exist. This error have a special method `getSuggestions` who return suggestions from the unknown name.

To instantiate this error, you need to declare 3 parameters:

-   `message` as all error
-   `_unknownCommand` the name of the unknown command
-   `_commandsList` a `string[]` with all registered command names

### `MissingOptionError`

Throw when an option is missing.

### `OptionSyntaxError`

Throw when an option has a syntax error in the synopsis.

### `UnknownOptionError`

Throw when an option isn't reconized. This error have a special method `getSuggestions` who return suggestions from the unknown name.

To instantiate this error, you need to declare two parameters:

-   `_unknownOption` the name of the unknown option
-   `_optionsList` a `string[]` with all registered option names

### `WrongNumberOfArgumentError`

Throw when the number of arguments is invalid.
