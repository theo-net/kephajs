# Verbosity Levels

Console commands have different verbosity levels, which determine the messages displayed in their output. By default, commands display only the most useful messages, but you can control their verbosity with the `-q` and `-v` options:

```sh
# do not output any message (not even the command result message)
node myApp.js some-command -q
node myApp.js some-command --quiet

# normal behavior, no option required (display only the useful messages)
node myApp.js some-command

# increase verbosity of messages
node myApp.js some-command -v

# display also the informative non essential messages
node myApp.js some-command -vv

# display all messages (useful to debug errors)
node myApp.js some-command -vvv
```

The verbosity level can also be controlled globally for all commands with the `SHELL_VERBOSITY` environment variable (the `-q` and `-v` options still have more precedence over the value of `SHELL_VERBOSITY`):

| Console option    | SHELL_VERBOSITY value | Equivalent TypeScript constant  |
| ----------------- | --------------------- | ------------------------------- |
| `-q` or `--quiet` | `-1`                  | `Output.VERBOSITY.QUIET`        |
| (none)            | `0`                   | `Output.VERBOSITY.NORMAL`       |
| `-v`              | `1`                   | `Output.VERBOSITY.VERBOSE`      |
| `-vv`             | `2`                   | `Output.VERBOSITY.VERY_VERBOSE` |
| `-vvv`            | `3`                   | `Output.VERBOSITY.DEBUG`        |

It is possible to print a message in a command for only a specific verbosity level. For example:

```ts
// available methods: .isQuiet(), .isVerbose(), .isVeryVerbose(), .isDebug()
if ($output.isVerbose()) {
    $output.writeln('My verbose message');
}

// alternatively you can pass the verbosity level to writeln()
$output.writeln(
    'Will only be printed in verbose mode or higher',
    Output.VERBOSITY.VERBOSE
);
```

When the quiet level is used, all output is suppressed as the default `write()` method returns without actually printing.

The full exception stacktrace is printed if the `VERBOSITY.VERBOSE` level or above is used.
