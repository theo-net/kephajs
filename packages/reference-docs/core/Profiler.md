# Profiler

A profiler is available to register the execution of your script. You can determine the time to execute for the differents part.

One instance is initialized in your `Application`.

## Register a timestamp

You can register a timestamp with the method `register`:

```ts
const profiler = new Profiler();
profiler.register('a description');
```

The default value of the timestamp will be `Date.now()`, but you can specify one:

```ts
profiler.register('defined timestamp', 123456789);
```

## Get the values

To get the values, use the method `getAll` who return an array with all registered timestamp. Each value is an object of `ProfilerElement`:

```ts
{
    timestamp: number;
    description: string;
    duration: number;
}
```

The duration is calculated from the previous entry.

## Get an element

You can find an element by the description:

```ts
profiler.getElement('my description');
```

If the element exists, the method return the `ProfilerElement`, else it returns undefined.

If more than one element have the same description, the last registered wiil be returned.
