# Helpers

The framework give some helpers.

## `arrayToEnum`

Convert `string[]` to an enum.

```ts
Helpers.arrayToEnum(['foo', 'bar']);
// {
//     foo: 'foo',
//     bar: 'bar',
// }
```

## `camelCase`

Convert a string to camelCase.

```ts
Helpers.camelCase('too legit 2 quit'); // 'toLegit2Quit'
Helpers.camelCase('safe HTML'); // 'safeHtml'
Helpers.camelCase('XMLHttpRequest'); // 'xmlHttpRequest'
```

## `joinValues`

Join the value of an array to a string.

```ts
Helpers.joinValues([1, 'foo', 'bar']);
// => "1 | 'foo' | 'bar'"
```

You have also to optional parameters:

-   `separator`: the separator for the joigned values (default `\`)
-   `escapeString`: the escape character for the string values (default `'`)

## `objectKeys`

Returns an array of a given object's own enumerable properties names, iterated in the same order that a normal loop would.

## `levenshtein`

Return the Levenshtein distance between two string.

## `strlenMaxLine`

Return the string length after removing control chars. Return the maximum line length if `str` is multiline.

## `wordWrap`

Wrap a multiline string and return a `string[]`. The wrapping will be on whitespace char.

Arguments:

-   `maxLength`: the max line length
-   `inputStr`: the string to wrap
-   `strlen: (str: string) => number`: function to get the string length (default: `strlenMaxLine`)

## `truncateWidth`

Truncate a string

Arguments:

-   `inputStr`: the string to truncate
-   `desiredLength`: the desired line length
-   `strlen: (str: string) => number`: function to get the string length (default: `strlenMaxLine`)

## `truncate`

Truncate a string and add a final char as `…` if needed.

Arguments:

-   `inputStr`: the string to truncate
-   `desiredLength`: the desired line length
-   `truncateChar`: the final char to add (default: `…`)
-   `strlen: (str: string) => number`: function to get the string length (default: `strlenMaxLine`)
-   `truncateFct`: function for the truncation (default: `truncateWidth`)

**Warning:** By default the function take the max line string length and not the string length (`Hel\nlo` will return 3 and not 6!)

## `varExport`

Return a string representation of a value.

Arguments:

-   `value`: the value to print
-   `indentation`: the root identation

## `getAllMethods`

Return all methods name of an object (public, protected, private). This function doesn't return the static methods.

If you use `#` to declare a private method, it will be not returned.

## `getAllStaticMethods`

Return all static methods name of an object.

## `arrayChunck`

Chunks an array into arrays with length elements. The last chunk may contain less than length elements.

```ts
Helpers.arrayChunk([1, 2, 3, 4], 3);
// [
//     [1, 2, 3],
//     [4],
// ]
```

Arguments:

-   `items`: The array to work on
-   `length`: The size of each chunk

## `range`

Return a new array of numbers. Can also return an array of string (they take juste the first char).

```twig
{% for letter in 'a'..'d' %}{{ letter }} {% endfor %}
{# a b c d #}
```

Arguments:

-   `start`
-   `end`
-   `step`

## `NumberHelpers`

### `numberFormat`

Formats a number with grouped thousands and optionally decimal digits.

Arguments:

-   `num`: The number being formatted.
-   `decimals`: Sets the number of decimal digits. If 0, the `decimalSeparator` is omitted from the return value (default 0). Negative number is considered as 0.
-   `decimalSeparator`: Sets the separator for the decimal point (default `.`).
-   `thousandsSeparator`: Sets the thousands separator (default `,`).

## `StringHelpers`

### `hash`

Return an hash from a string. For the same value, the function will return the same hash each time.

### `sprintf`

Return a string according to the formatting string `format`.

Arguments:

-   `format`

The format string is composed of zero or more directives: ordinary characters (excluding `%`) that are copied directly to the result and conversion specifications, each of which results in fetching its own parameter.

A conversion specification follows this prototype: `%[argnum$][flags][width][.precision]specifier`.

`Argnum`

An integer followed by a dollar sign `$`, to specify which number argument to treat in the conversion.

`Flags`

| Flag      | Description                                                                                            |
| --------- | ------------------------------------------------------------------------------------------------------ |
| `-`       | Left-justify within the given field width; Right justification is the default                          |
| `+`       | Prefix positive numbers with a plus sign `+`; Default only negative are prefixed with a negative sign. |
| (space)   | Pads the result with spaces. This is the default.                                                      |
| `0`       | Only left-pads numbers with zeros. With s specifiers this can also right-pad with zeros.               |
| `'`(char) | Pads the result with the character (char).                                                             |

`Width`

An integer that says how many characters (minimum) this conversion should result in.

`Precision`

A period `.` followed by an integer who's meaning depends on the specifier:

-   For `e`, `E`, `f` and `F` specifiers: this is the number of digits to be printed after the decimal point (by default, this is 6).
-   For `g`, `G`, `h` and `H` specifiers: this is the maximum number of significant digits to be printed.
-   For `s` specifier: it acts as a cutoff point, setting a maximum character limit to the string.

Note: If the period is specified without an explicit value for precision, 0 is assumed.

Note: Attempting to use a position specifier greater than PHP_INT_MAX will generate warnings.

`Specifiers`

| Specifier | Description                                                                                                                                                                                                                                                                                                                 |
| --------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `%`       | A literal percent character. No argument is required.                                                                                                                                                                                                                                                                       |
| `b`       | The argument is treated as an integer and presented as a binary number.                                                                                                                                                                                                                                                     |
| `c`       | The argument is treated as an integer and presented as the character with that ASCII.                                                                                                                                                                                                                                       |
| `d`       | The argument is treated as an integer and presented as a (signed) decimal number.                                                                                                                                                                                                                                           |
| `e`       | The argument is treated as scientific notation (e.g. 1.2e+2).                                                                                                                                                                                                                                                               |
| `E`       | Like the e specifier but uses uppercase letter (e.g. 1.2E+2).                                                                                                                                                                                                                                                               |
| `f`       | The argument is treated as a float and presented as a floating-point number (locale aware).                                                                                                                                                                                                                                 |
| `F`       | The argument is treated as a float and presented as a floating-point number (non-locale aware).                                                                                                                                                                                                                             |
| `g`       | General format. Let P equal the precision if nonzero, 6 if the precision is omitted, or 1 if the precision is zero. Then, if a conversion with style E would have an exponent of X: If P > X ≥ −4, the conversion is with style f and precision P − (X + 1). Otherwise, the conversion is with style e and precision P − 1. |
| `G`       | Like the g specifier but uses E and f.                                                                                                                                                                                                                                                                                      |
| `h`       | Like the g specifier but uses F.                                                                                                                                                                                                                                                                                            |
| `H`       | Like the g specifier but uses E and F.                                                                                                                                                                                                                                                                                      |
| `o`       | The argument is treated as an integer and presented as an octal number.                                                                                                                                                                                                                                                     |
| `s`       | The argument is treated and presented as a string.                                                                                                                                                                                                                                                                          |
| `u`       | The argument is treated as an integer and presented as an unsigned decimal number.                                                                                                                                                                                                                                          |
| `x`       | The argument is treated as an integer and presented as a hexadecimal number (with lowercase letters).                                                                                                                                                                                                                       |
| `X`       | The argument is treated as an integer and presented as a hexadecimal number (with uppercase letters).                                                                                                                                                                                                                       |

Warning: The `c` type specifier ignores padding and width

Warning: Attempting to use a combination of the string and width specifiers with character sets that require more than one byte per character may result in unexpected results

Variables will be co-erced to a suitable type for the specifier:

| Handling Type | Specifiers             |
| ------------- | ---------------------- |
| `string`      | s                      |
| `int`         | d, u, c, o, x, X, b    |
| `float`       | e, E, f, F, g, G, h, H |

-   `values`

### `stripTags`

This function tries to return a string with all HTML tags stripped from a given string.

Arguments:

-   `string` The input string.
-   `allowedTags` You can use the optional second parameter to specify tags which should not be stripped. These are either given as string, or as array. Refer to the example below regarding the format of this parameter.

Note: HTML comments are also stripped. This is hardcoded and can not be changed with allowedTags.

Note: Self-closing XHTML tags are ignored and only non-self-closing tags should be used in allowed_tags. For example, to allow both `<br>` and `<br/>`, you should use: `stripTags(input, '<br>')`.

```ts
const text =
    '<p>Test paragraph.</p><!-- Comment --> <a href="#fragment">Other text</a>';
console.log(stripTags(text)); // Test paragraph. Other text

// Allow <p> and <a>
console.log(stripTags(text, '<p><a>')); // <p>Test paragraph.</p> <a href="#fragment">Other text</a>

// the line above can be written as:
// console.log(stripTags(text, ['p', 'a']));
```

## `IterationHelpers`

### `forEach`

Call `fn` for each element of a collection, or an array, of an object implementing the `Iterable` interface.

If `fn` return `false` stop the `forEach`.

Arguments:

-   `collection`
-   `fn` callback
    `thisArg` context (optional)

The `fn` function have this arguments:

-   `v`: the current value
-   `k`: the current key
-   `iterable`: the collection

## `RegExpHelpers`

### `escape`

Escape a string for conversion to a `RegExp`.

### `str2RegExp`

Convert a string to a `RegExp`. Keep the flags:

```
"/test/g" => RegExp('test', 'g');
```

If the string isn't a `RegExp` (don't have '/'), return the `string`.
