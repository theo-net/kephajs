# The core Application

It's the base for all applications created with the framework.

When you want to initialize the new application, we can give some parameters:

-   the application root: will be used to find the path to config file for example
-   somes options

```ts
const app = new Application('test', {});
```

## Options

If this parameter is an string, it will contain the current environment.

The environment can be: `development`, `staging`, `production` or `testing`

The options can be:

-   `environment`
-   `description` the description of the application
-   `name` the name of the application
-   `version` the version of the application

## Direct access to services and values

The application object give you direct access to differents services and values:

-   `appRoot` to the value of the applciation root (cf constructor)
-   `config` the config service
-   `container` the service container
-   `environment` the current environment
-   `isDev` boolean to know if we are in the development environment or not
-   `isProd` boolean to know if we are in the production environment or not
-   `kephajsVersion` the version of the framework
-   `profiler` the profiler of your application
-   `validator` the validator service
-   `version` the version of your application

## Dynamic description, name and version

If you don't want to use the options to define `description`, `name`, and `version`, you can extend the methods `_getDescription`, `_getName` and `_getVersion`.

## Run the application

To run your application, use the method `run`. If it's not initialized, the method `init` will be excuted just before.

In many case, you can just launch `run`, you don't need to call `init` yourself.

## Register shutdown function

You can register shutdown functions. They will be called when the application is shutdown, just before the state become `shutdown`.

The last registered will be the first called.

```ts
app.init();
app.registerShutdownFunction(() => {
    console.log('First');
    return Promise.resolve();
});
app.registerShutdownFunction(() => {
    console.log('Second');
    return Promise.resolve();
});
app.registerShutdownFunction(() => {
    console.log('Third');
    return Promise.resolve();
});
app.shutdown();
// Third
// Second
// First
```
