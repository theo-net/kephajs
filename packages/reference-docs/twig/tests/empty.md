# `empty`

`empty` checks if a variable is an empty string, an empty array, an empty hash, exactly `false`, or exactly `null`.

For objects that implement the `Iterable` interface, `empty` will check the length of the values.

For objects that implement the `toString()` magic method, it will check if an empty string is returned.

```twig
{% if foo is empty %}
    ...
{% endif %}
```
