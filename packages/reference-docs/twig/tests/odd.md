# `odd`

`odd` returns `true` if the given number is odd:

```twig
{{ var is odd }}
```

See also: `even`
