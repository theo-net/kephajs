# `divisible by`

`divisible by` checks if a variable is divisible by a number:

```twig
{% if loop.index is divisible by(3) %}
    ...
{% endif %}
```
