# `null`

`null` returns `true` if the variable is `null`:

```twig
{{ var is null }}
```

Note: `none` is an alias for `null`.
