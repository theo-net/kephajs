# Introduction

## Basic API Usage

This section gives you a brief introduction to the API for Twig:

```ts
const loader = new RecordLoader({
    index: 'Hello {{ name }}!',
});
const twig = new Environment(loader);

console.log(twig.render('index', { name: 'world' }));
```

Twig uses a loader (`RecordLoader`) to locate templates, and an environment (`Environment`) to store its configuration.

The `render()` method loads the template passed as a first argument and renders it with the variables passed as a second argument.

As templates are generally stored on the filesystem, Twig also comes with a filesystem loader:

```ts
const loader = new FilesystemLoader('/path/to/templates');
const twig = new EnvironmentConsole(loader, {
    cache: '/path/to/compilation_cache',
});

console.log(twig.render('index.html', { name: 'world' }));
```

Note: when you want to use the filesystem, use the `EnvironmentConsole` class.
