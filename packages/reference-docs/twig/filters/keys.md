# `keys`

The `keys` filter returns the keys of an hash or an object. It is useful when you want to iterate over the keys:

```twig
{% for key in hash|keys %}
    ...
{% endfor %}
```
