# `abs`

The `abs` filter returns the absolute value.

```twig
{# number = -5 #}

{{ number|abs }}

{# outputs 5 #}
```

Note: Internally, Twig uses the Javascript `Math.abs` function.
