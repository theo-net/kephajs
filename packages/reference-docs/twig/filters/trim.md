# `trim`

The `trim` filter strips whitespace (or other characters) from the beginning
and end of a string:

```twig
{{ '  I like Twig.  '|trim }}

{# outputs 'I like Twig.' #}

{{ '  I like Twig.  '|trim('left') }}

{# outputs 'I like Twig.  ' #}

{{ '  I like Twig.'|trim('right') }}

{# outputs '  I like Twig.' #}
```

## Arguments

-   `side`: The default is to strip from the left and the right (`both`) sides, but `left` and `right` will strip from either the left side or right side only
