# `reverse`

The `reverse` filter reverses a sequence, a mapping, or a string:

```twig
{% for user in users|reverse %}
    ...
{% endfor %}

{{ '1234'|reverse }}

{# outputs 4321 #}
```
