# `sort`

The `sort` filter sorts an array:

``twig
{% for user in users|sort %}
...
{% endfor %}

````

You can pass an arrow function to sort the array:

```twig
{% set fruits = [
    { name: 'Apples', quantity: 5 },
    { name: 'Oranges', quantity: 2 },
    { name: 'Grapes', quantity: 4 },
] %}

{% for fruit in fruits|sort((a, b) => a.quantity <=> b.quantity)|column('name') %}
    {{ fruit }}
{% endfor %}

{# output in this order: Oranges, Grapes, Apples #}
````

Note the usage of the `spaceship` operator to simplify the comparison.

## Arguments

-   `arrow`: An arrow function
