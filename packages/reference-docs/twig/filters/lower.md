# `lower`

The `lower` filter converts a value to lowercase:

```twig
{{ 'WELCOME'|lower }}

{# outputs 'welcome' #}
```
