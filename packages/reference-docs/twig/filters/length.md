# `length`

The `length` filter returns the number of items of a sequence or mapping, or the length of a string.

For objects have a `length` property will use this value.

For objects that implement the `toString()` magic method it will return the length of the string provided by that method.

For objects that implement the `Iterable` interface, `length` will use the length of the values.

```twig
{% if users|length > 10 %}
    ...
{% endif %}
```
