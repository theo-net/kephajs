# `json_encode`

The `json_encode` filter returns the JSON representation of a value:

```twig
{{ data|json_encode() }}
```

Note: Internally, Twig uses the Javascript `JSON.stringify` function.
