# `upper`

The `upper` filter converts a value to uppercase:

```twig
{{ 'welcome'|upper }}

{# outputs 'WELCOME' #}
```
