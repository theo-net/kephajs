# `include`

The `include` function returns the rendered content of a template:

```twig
{{ include('template.html') }}
{{ include(some_var) }}
```

Included templates have access to the variables of the active context.

If you are using the filesystem loader, the templates are looked for in the paths defined by it.

The context is passed by default to the template but you can also pass additional variables:

```twig
{# template.html will have access to the variables from the current context and the additional ones provided #}
{{ include('template.html', {foo: 'bar'}) }}
```

You can disable access to the context by setting `withContext` to `false`:

```twig
{# only the foo variable will be accessible #}
{{ include('template.html', {foo: 'bar'}, false) }}
```

```twig
{# no variables will be accessible #}
{{ include('template.html', {}, false) }}
```

And if the expression evaluates to a `Template` or a `TemplateWrapper` instance, Twig will use it directly:

```ts
// {{ include(template) }}

const template = twig.load('some_template.twig');

twig.display('template.twig', { template: template });
```

When you set the `ignoreMissing` flag, Twig will return an empty string if the template does not exist:

```twig
{{ include('sidebar.html', {}, true, true) }}
```

You can also provide a list of templates that are checked for existence before inclusion. The first template that exists will be rendered:

```twig
{{ include(['page_detailed.html', 'page.html']) }}
```

If `ignoreMissing` is set, it will fall back to rendering nothing if none of the templates exist, otherwise it will throw an exception.

When including a template created by an end user, you should consider sandboxing it:

```twig
{{ include('page.html', {}, true, false, true) }}
```

## Arguments

-   `template`: The template to render
-   `variables`: The variables to pass to the template
-   `withContext`: Whether to pass the current context variables or not
-   `ignoreMissing`: Whether to ignore missing templates or not
-   `sandboxed`: Whether to sandbox the template or not
