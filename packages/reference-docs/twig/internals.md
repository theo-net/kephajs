# Twig Internals

Twig is very extensible and you can hack it. Keep in mind that you should probably try to create an extension before hacking the core, as most features and enhancements can be handled with extensions. This chapter is also useful for people who want to understand how Twig works under the hood.

## How does Twig work?

The rendering of a Twig template can be summarized into four key steps:

-   **Load** the template: If the template is already compiled, load it and go
    to the _evaluation_ step, otherwise:

    -   First, the **lexer** tokenizes the template source code into small pieces
        for easier processing;

    -   Then, the **parser** converts the token stream into a meaningful tree
        of nodes (the Abstract Syntax Tree);

    -   Finally, the **compiler** transforms the AST into Js code.

<!--* **Evaluate** the template: It means calling the ``display()``
  method of the compiled template and passing it the context. -->

## The Lexer

The lexer tokenizes a template source code into a token stream (each token is an instance of `Token`, and the stream is an instance of `TokenStream`). The default lexer recognizes 13 different token types:

-   `Token.BLOCK_START_TYPE`, `Token.BLOCK_END_TYPE`: Delimiters for blocks (`{% %}`)
-   `Token.VAR_START_TYPE`, `Token.VAR_END_TYPE`: Delimiters for variables (`{{ }}`)
-   `Token.TEXT_TYPE`: A text outside an expression;
-   `Token.NAME_TYPE`: A name in an expression;
-   `Token.NUMBER_TYPE`: A number in an expression;
-   `Token.STRING_TYPE`: A string in an expression;
-   `Token.OPERATOR_TYPE`: An operator;
-   `Token.PUNCTUATION_TYPE`: A punctuation sign;
-   `Token.INTERPOLATION_START_TYPE`, `Token.INTERPOLATION_END_TYPE`: Delimiters for string interpolation;
-   `Token.EOF_TYPE`: Ends of template.

You can manually convert a source code into a token stream by calling the
`tokenize()` method of an environment:

```ts
stream = twig->tokenize(new Source(source, identifier));
```

As the stream has a `toString()` method, you can have a textual representation of it by print the object:

```ts
console.log(stream + '\n');
```

Here is the output for the `Hello {{ name }}` template:

```
TEXT_TYPE(Hello )
VAR_START_TYPE()
NAME_TYPE(name)
VAR_END_TYPE()
EOF_TYPE()
```

Note: The default lexer (`Lexer`) can be changed by calling the `setLexer()` method:

```ts
twig.setLexer(lexer);
```

## The Parser

The parser converts the token stream into an AST (Abstract Syntax Tree), or a node tree (an instance of `Node/ModuleNode`). The core extension defines the basic nodes like: `for`, `if`, ... and the expression nodes.

You can manually convert a token stream into a node tree by calling the
`parse()` method of an environment:

```ts
const nodes = twig.parse(stream);
```

Print the node object gives you a nice representation of the tree:

```ts
console.log(nodes + '\n');
```

Here is the output for the `Hello {{ name }}` template:

```
ModuleNode(
    TextNode(Hello )
    PrintNode(
        NameExpression(name)
    )
)
```

Note: The default parser (`TokenParser/AbstractTokenParser`) can be changed by calling the `setParser()` method:

```ts
twig.setParser(parser);
```

## The Compiler

The last step is done by the compiler. It takes a node tree as an input and generates Javascript code usable for runtime execution of the template.

You can manually compile a node tree to PHP code with the `compile()` method of an environment:

```tq
const js = twig.compile(nodes);
```

The generated template for a `Hello {{ name }}` template reads as follows (the actual output can differ depending on the version of Twig you are using):

```js
/* Hello {{ name }} */
class __TwigTemplate_1121b6f109fe93ebe8c6e22e3712bceb extends Template {
    protected doDisplay(context, blocks = []) {
        let finalString = '';
        // line 1
        finalString += 'Hello ';
        finalString += twig_escape_filter(this._env, context.name !== undefined ? context.name : null, 'html', null, true);
        return finalString;
    }

    // some more code
}
```

Note: The default compiler (`Compiler`) can be changed by calling the `setCompiler()` method:

```ts
twig.setCompiler(compiler);
```
