# `import`

The `import` tag imports `macro` names in a local variable. The tag is documented in detail in the documentation for the `macro` tag.
