# `block`

Blocks are used for inheritance and act as placeholders and replacements at the same time. They are documented in detail in the documentation for the `extends` tag.

Block names must consist of alphanumeric characters, and underscores. The first char can't be a digit and dashes are not permitted.

See also: `functions/block`, `functions/parent`, `tags/use`, `tags/extends`
