# `include`

The `include` statement includes a template and outputs the rendered content of that file:

```twig
{% include 'header.html' %}
    Body
{% include 'footer.html' %}
```

Note: It is recommended to use the `include` function instead as it provides the same features with a bit more flexibility:

-   The `include` function is semantically more "correct" (including a template outputs its rendered contents in the current scope; a tag should not display anything);

-   The `include` function is more "composable":

```twig
{# Store a rendered template in a variable #}
{% set content %}
    {% include 'template.html' %}
{% endset %}
{# vs #}
{% set content = include('template.html') %}
{# Apply filter on a rendered template #}
{% apply upper %}
    {% include 'template.html' %}
{% endapply %}
{# vs #}
{{ include('template.html')|upper }}
```

Included templates have access to the variables of the active context.

If you are using the filesystem loader, the templates are looked for in the paths defined by it.

You can add additional variables by passing them after the `with` keyword:

```twig
{# template.html will have access to the variables from the current context and the additional ones provided #}
{% include 'template.html' with {'foo': 'bar'} %}

{% set vars = {'foo': 'bar'} %}
{% include 'template.html' with vars %}
```

You can disable access to the context by appending the `only` keyword:

```twig
{# only the foo variable will be accessible #}
{% include 'template.html' with {'foo': 'bar'} only %}
```

```twig
{# no variables will be accessible #}
{% include 'template.html' only %}
```

Tip: When including a template created by an end user, you should consider sandboxing it. More information in the `Twig for Developers` chapter and in the `sandbox` tag documentation.

The template name can be any valid Twig expression:

```twig
{% include some_var %}
{% include ajax ? 'ajax.html' : 'not_ajax.html' %}
```

And if the expression evaluates to a `Template` or a `TemplateWrapper` instance, Twig will use it directly:

```ts
// {% include template %}

const template = twig.load('some_template.twig');

twig.render('template.twig', { template: template });
```

You can mark an include with `ignore missing` in which case Twig will ignore the statement if the template to be included does not exist. It has to be placed just after the template name. Here some valid examples:

```twig
{% include 'sidebar.html' ignore missing %}
{% include 'sidebar.html' ignore missing with {'foo': 'bar'} %}
{% include 'sidebar.html' ignore missing only %}
```

You can also provide a list of templates that are checked for existence before inclusion. The first template that exists will be included:

```twig
{% include ['page_detailed.html', 'page.html'] %}
```

If `ignore missing` is given, it will fall back to rendering nothing if none of the templates exist, otherwise it will throw an exception.
