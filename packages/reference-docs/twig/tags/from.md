# `from`

The `from` tag imports `macro` names into the current namespace. The tag is documented in detail in the documentation for the
`macro` tag.
