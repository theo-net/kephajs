# Extending Twig

Twig can be extended in many ways; you can add extra tags, filters, tests, operators, global variables, and functions. You can even extend the parser itself with node visitors.

Note: The first section of this chapter describes how to extend Twig. If you want to reuse your changes in different projects or if you want to share them with others, you should then create an extension as described in the following section.

Caution: Twig won't be able to recompile your templates when the Javascript code is updated. To see your changes in real-time, either disable template caching.

Before extending Twig, you must understand the differences between all the different possible extension points and when to use them.

First, remember that Twig has two main language constructs:

-   `{{ }}`: used to print the result of an expression evaluation;

-   `{% %}`: used to execute statements.

To understand why Twig exposes so many extension points, let's see how to implement a _Lorem ipsum_ generator (it needs to know the number of words to generate).

You can use a `lipsum` _tag_:

```twig
{% lipsum 40 %}
```

That works, but using a tag for `lipsum` is not a good idea for at least
three main reasons:

-   `lipsum` is not a language construct;
-   The tag outputs something;
-   The tag is not flexible as you cannot use it in an expression:

```twig
{{ 'some text' ~ {% lipsum 40 %} ~ 'some more text' }}
```

In fact, you rarely need to create tags; and that's good news because tags are, the most complex extension point.

Now, let's use a `lipsum` _filter_:

```twig
{{ 40|lipsum }}
```

Again, it works. But a filter should transform the passed value to something else. Here, we use the value to indicate the number of words to generate (so, `40` is an argument of the filter, not the value we want to transform).

Next, let's use a `lipsum` _function_:

```twig
{{ lipsum(40) }}
```

Here we go. For this specific example, the creation of a function is the extension point to use. And you can use it anywhere an expression is accepted:

```twig
{{ 'some text' ~ lipsum(40) ~ 'some more text' }}

{% set lipsum = lipsum(40) %}
```

Lastly, you can also use a _global_ object with a method able to generate lorem ipsum text:

```twig
{{ text.lipsum(40) }}
```

As a rule of thumb, use functions for frequently used features and global objects for everything else.

Keep in mind the following when you want to extend Twig:

| What?      | Implementation difficulty? | How often? | When?                  |
| ---------- | -------------------------- | ---------- | ---------------------- |
| _macro_    | simple                     | frequent   | Content generation     |
| _global_   | simple                     | frequent   | Helper object          |
| _function_ | simple                     | frequent   | Content generation     |
| _filter_   | simple                     | frequent   | Value transformation   |
| _tag_      | complex                    | rare       | DSL language construct |
| _test_     | simple                     | rare       | Boolean decision       |
| _operator_ | simple                     | rare       | Values transformation  |

## Globals

A global variable is like any other template variable, except that it's available in all templates and macros::

```ts
const twig = new Environment(loader);
twig.addGlobal('text', new Text());
```

You can then use the `text` variable anywhere in a template:

```twig
{{ text.lipsum(40) }}
```

## Filters

Creating a filter consists of associating a name with a Javascript function:

```ts
// an anonymous function
filter = new TwigFilter('splitLn', function (string) {
    return string.split('\n');
});

// or a simple Js function
filter = new TwigFilter('log', console.log);

// or a class static method
filter = new TwigFilter('rot13', SomeClass.rot13Filter);

// or a class method
filter = new TwigFilter('rot13', this.rot13Filter);
```

The first argument passed to the `TwigFilter` constructor is the name of the filter you will use in templates and the second one is the Javascript function to associate with it.

Then, add the filter to the Twig environment::

```ts
const twig = new Environment(loader);
twig.addFilter(filter);
```

And here is how to use it in a template:

```twig
{{ 'Twig'|rot13 }}

{# will output Gjvt #}
```

When called by Twig, the function receives the left side of the filter (before the pipe `|`) as the first argument and the extra arguments passed to the filter (within parentheses `()`) as extra arguments.

For instance, the following code:

```twig
{{ 'TWIG'|lower }}
{{ now|date('d/m/Y') }}
```

is compiled to something like the following:

```ts
this.print('TWIG'.toLower);
this.print(twigDateFormatFilter(now, 'd/m/Y');
```

The `TwigFilter` class takes an array of options as its last argument:

```ts
const filter = new TwigFilter('log', console.log, options);
```

### Environment-aware Filters

If you want to access the current environment instance in your filter, set the `needsEnvironment` option to `true`; Twig will pass the current environment as the first argument to the filter call:

```ts
const filter = new TwigFilter(
    'myFilter',
    (env: Environment, string: string) => {
        // the logic
    },
    { needsEnvironment: true }
);
```

### Context-aware Filters

If you want to access the current context in your filter, set the `needsContext` option to `true`; Twig will pass the current context as the first argument to the filter call (or the second one if `needsEnvironment` is also set to `true`):

```ts
const filter = new TwigFilter(
    'myFilter',
    (context: Record<string, unknown>, string: string) => {
        // ...
    },
    { needsContext: true }
);

const filter = new TwigFilter(
    'myFilter',
    (env: Environment, context: Record<string, unknown>, string: string) => {
        // ...
    },
    { needsContext: true, needsEnvironment: true }
);
```

### Automatic Escaping

If automatic escaping is enabled, the output of the filter may be escaped before printing. If your filter acts as an escaper (or explicitly outputs HTML or JavaScript code), you will want the raw output to be printed. In such a case, set the `isSafe` option:

```ts
const filter = new TwigFilter('myFilter', myFct, { isSafe: ['html'] });
```

Some filters may need to work on input that is already escaped or safe, for example when adding (safe) HTML tags to originally unsafe output. In such a case, set the `preEscape` option to escape the input data before it is run through your filter:

```ts
const filter = new TwigFilter('somefilter', somefilter, {
    preEscape: 'html',
    isSafe: ['html'],
});
```

### Variadic Filters

When a filter should accept an arbitrary number of arguments, set the `isVariadic` option to `true`; Twig will pass the extra arguments as the last argument to the filter call as an array:

```ts
const filter = new TwigFilter(
    'thumbnail',
    (file: string, options: unknown[] = []) => {
        // ...
    },
    { isVariadic: true }
);
```

### Dynamic Filters

A filter name containing the special `*` character is a dynamic filter and the `*` part will match any string:

```ts
const filter = new TwigFilter(
    '*_path',
    (name: string, arguments: unknown[]) => {
        // ...
    }
);
```

The following filters are matched by the above defined dynamic filter:

-   `product_path`
-   `category_path`

A dynamic filter can define more than one dynamic parts::

```ts
const filter = new TwigFilter(
    '*_path_*',
    (name: string, suffix: string, arguments: unknown[]) => {
        // ...
    }
);
```

The filter receives all dynamic part values before the normal filter arguments, but after the environment and the context. For instance, a call to `'foo'|a_path_b()` will result in the following arguments to be passed to the filter: `('a', 'b', 'foo')`.

### Deprecated Filters

You can mark a filter as being deprecated by setting the `deprecated` option to `true`. You can also give an alternative filter that replaces the deprecated one when that makes sense:

```ts
const filter = new TwigFilter(
    'obsolete',
    () => {
        // ...
    },
    { deprecated: true, alternative: 'new_one' }
);
```

When a filter is deprecated, Twig emits a deprecation notice when compiling a template using it. See `deprecation-notices` for more information.

## Functions

Functions are defined in the exact same way as filters, but you need to create an instance of `TwigFunction`:

```ts
const twig = new Environment(loader);
const fct = new TwigFunction('function_name', () => {
    // ...
});
twig.addFunction(fct);
```

Functions support the same features as filters, except for the `preEscape` and `preservesSafety` options.

## Tests

Tests are defined in the exact same way as filters and functions, but you need to create an instance of `TwigTest`:

```ts
const twig = new Environment(loader);
const test = new TwigTest('test_name', () => {
    // ...
});
twig.addTest(test);
```

Tests allow you to create custom application specific logic for evaluating boolean conditions. As a simple example, let's create a Twig test that checks if objects are 'red':

```ts
const twig = new Environment(loader);
const test = new TwigTest('red', (value: unknown) {
    if (typeof value === 'object' && value.color !== undefined && value.color === 'red') {
        return true;
    }
    if (typeof value === 'object' && value.paint !== undefined && value.paint === 'red') {
        return true;
    }
    return false;
});
twig.addTest(test);
```

Test functions must always return `true`/`false`.

When creating tests you can use the `nodeClass` option to provide custom test compilation. This is useful if your test can be compiled into Javascript primitives. This is used by many of the tests built into Twig:

```ts
const twig = new Environment(loader);
const test = new TwigTest('odd', null, { nodeClass: OddTestExpression });
twig.addTest(test);

class OddTestExpression extends TestExpression {
    compile(compiler: Compiler) {
        compiler
            .raw('(')
            .subcompile(this.getNode('node'))
            .raw(' % 2 != 0')
            .raw(')');
    }
}
```

The above example shows how you can create tests that use a node class. The node class has access to one sub-node called `node`. This sub-node contains the value that is being tested. When the `odd` filter is used in code such as:

```twig
{% if my_value is odd %}
```

The `node` sub-node will contain an expression of `my_value`. Node-based tests also have access to the `arguments` node. This node will contain the various other arguments that have been provided to your test.

If you want to pass a variable number of positional or named arguments to the test, set the `isVariadic` option to `true`. Tests support dynamic names (see dynamic filters for the syntax).

## Tags

One of the most exciting features of a template engine like Twig is the possibility to define new **language constructs**. This is also the most complex feature as you need to understand how Twig's internals work.

Most of the time though, a tag is not needed:

-   If your tag generates some output, use a **function** instead.

-   If your tag modifies some content and returns it, use a **filter** instead.

For instance, if you want to create a tag that converts a Markdown formatted text to HTML, create a `markdown` filter instead:

```twig
{{ '**markdown** text'|markdown }}
```

If you want use this filter on large amounts of text, wrap it with the `apply` tag:

```twig
{% apply markdown %}
Title
=====

Much better than creating a tag as you can **compose** filters.
{% endapply %}
```

-   If your tag does not output anything, but only exists because of a side effect, create a **function** that returns nothing and call it via the `do` tag.

For instance, if you want to create a tag that logs text, create a `log` function instead and call it via the `do` tag:

```twig
{% do log('Log some things') %}
```

If you still want to create a tag for a new language construct, great!

Let's create a `set` tag that allows the definition of simple variables from within a template. The tag can be used like follows:

```twig
{% set name = "value" %}

{{ name }}

{# should output value #}
```

Note: The `set` tag is part of the Core extension and as such is always available. The built-in version is slightly more powerful and supports multiple assignments by default.

Three steps are needed to define a new tag:

-   Defining a Token Parser class (responsible for parsing the template code);

-   Defining a Node class (responsible for converting the parsed code to Javascript);

-   Registering the tag.

### Registering a new tag

Add a tag by calling the `addTokenParser` method on the `Environment` instance:

```ts
const twig = new Environment(loader);
twig.addTokenParser(new Project_Set_TokenParser());
```

### Defining a Token Parser

Now, let's see the actual code of this class::

```ts
class Project_Set_TokenParser extends AbstractTokenParser {
    parse(token: Token) {
        const parser = this.parser;
        const stream = parser.getStream();

        const name = stream.expect(Token.NAME_TYPE).getValue();
        stream.expect(Token.OPERATOR_TYPE, '=');
        const value = parser.getExpressionParser().parseExpression();
        stream.expect(Token.BLOCK_END_TYPE);

        return new Project_Set_Node(name, value, token.line, this.getTag());
    }

    getTag() {
        return 'set';
    }
}
```

The `getTag()` method must return the tag we want to parse, here `set`.

The `parse()` method is invoked whenever the parser encounters a `set` tag. It should return a `TwigNode` instance that represents the node (the `Project_Set_Node` calls creating is explained in the next section).

The parsing process is simplified thanks to a bunch of methods you can call from the token stream (`this.parser.getStream()`):

-   `getCurrent()`: Gets the current token in the stream.

-   `next()`: Moves to the next token in the stream, _but returns the old one_.

-   `test(type)`, `test(value)` or `test(type, value)`: Determines whether the current token is of a particular type or value (or both). The value may be an array of several possible values.

-   `expect(type[, value[, message]])`: If the current token isn't of the given type/value a syntax error is thrown. Otherwise, if the type and value are correct, the token is returned and the stream moves to the next token.

-   `look()`: Looks at the next token without consuming it.

Parsing expressions is done by calling the `parseExpression()` like we did for the `set` tag.

Tip: Reading the existing `TokenParser` classes is the best way to learn all the nitty-gritty details of the parsing process.

### Defining a Node

The `Project_Set_Node` class itself is quite short:

```ts
class Project_Set_Node extends TwigNode {
    constructor(
        name: string,
        value: AbstractExpression,
        line: number,
        tag: string | null = null
    ) {
        super({ value }, { name }, line, tag);
    }

    compile(compiler: Compiler) {
        compiler
            .addDebugInfo(this)
            .write(
                'context["' + (this.getAttribute('name') as string) + '"] = '
            )
            .subcompile(this.getNode('value'))
            .raw(';\n');
    }
}
```

The compiler implements a fluid interface and provides methods that help the developer generate beautiful and readable Javascript code:

-   `subcompile()`: Compiles a node.

-   `raw()`: Writes the given string as is.

-   `write()`: Writes the given string by adding indentation at the beginning of each line.

-   `string()`: Writes a quoted string.

-   `repr()`: Writes a Javascript representation of a given value (see `ForNode` for a usage example).

-   `addDebugInfo()`: Adds the line of the original template file related to the current node as a comment.

-   `indent()`: Indents the generated code (see `BlockNode` for a usage example).

-   `outdent()`: Outdents the generated code (see `BlockNode` for a usage example).

## Creating an Extension

The main motivation for writing an extension is to move often used code into a reusable class like adding support for internationalization. An extension can define tags, filters, tests, operators, functions, and node visitors.

Most of the time, it is useful to create a single extension for your project, to host all the specific tags and filters you want to add to Twig.

Tip: When packaging your code into an extension, Twig is smart enough to recompile your templates whenever you make a change to it (when `autoReload` is enabled).

An extension is a class that implements the following interface:

```ts
interface ExtensionInterface {
    // Returns the token parser instances to add to the existing list.
    getTokenParsers(): Record<string, TokenParserInterface>;

    // Returns the node visitor instances to add to the existing list.
    getNodeVisitors(): NodeVisitorInterface[];

    // Returns a list of filters to add to the existing list.
    getFilters(): Record<string, TwigFilter>;

    // Returns a list of tests to add to the existing list.
    getTests(): Record<string, TwigTest>;

    // Returns a list of functions to add to the existing list.
    getFunctions(): Record<string, TwigFunction>;

    // Returns a list of operators to add to the existing list.
    getOperators(): Record<string, Operator>[];
}
```

To keep your extension class clean and lean, inherit from the built-in `Extension/AbstractExtension` class instead of implementing the interface as it provides empty implementations for all methods:

```ts
class Project_Twig_Extension extends AbstractExtension {}
```

This extension does nothing for now. We will customize it in the next sections.

You can save your extension anywhere on the filesystem, as all extensions must be registered explicitly to be available in your templates.

You can register an extension by using the `addExtension()` method on your main `Environment` object:

```ts
const twig = new Environment(loader);
twig.addExtension(new Project_Twig_Extension());
```

Tip: The Twig core extensions are great examples of how extensions work.

### Globals

Global variables can be registered in an extension via the `getGlobals()` method:

```ts
class Project_Twig_Extension
    extends AbstractExtension
    implements GlobalsInterface
{
    getGlobals(): Record<string, unknown> {
        return {
            text: new Text(),
        };
    }

    // ...
}
```

### Functions

Functions can be registered in an extension via the `getFunctions()` method:

```ts
class Project_Twig_Extension extends AbstractExtension {
    getFunctions() {
        return {
            lipsum: new TwigFunction('lipsum', generate_lipsum),
        };
    }
    // ...
}
```

### Filters

To add a filter to an extension, you need to override the `getFilters()` method. This method must return an array of filters to add to the Twig environment:

```ts
class Project_Twig_Extension extends AbstractExtension {
    getFilters() {
        return {
            rot13: new TwigFilter('rot13', str_rot13),
        };
    }
    // ...
}
```

### Tags

Adding a tag in an extension can be done by overriding the `getTokenParsers()` method. This method must return an array of tags to add
to the Twig environment:

```ts
class Project_Twig_Extension extends AbstractExtension {
    getTokenParsers() {
        return { projectSet: new Project_Set_TokenParser() };
    }
    // ...
}
```

In the above code, we have added a single new tag, defined by the `Project_Set_TokenParser` class. The `Project_Set_TokenParser` class is responsible for parsing the tag and compiling it to Javascript.

### Operators

The `getOperators()` methods lets you add new operators. Here is how to add the `!`, `||`, and `&&` operators:

```ts
class Project_Twig_Extension extends AbstractExtension {
    getOperators() {
        return [
            {
                '!': { precedence: 50, class: NotUnary },
            },
            {
                '||': {
                    precedence: 10,
                    class: OrBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
                '&&': {
                    precedence: 15,
                    class: AndBinary,
                    associativity: ExpressionParser.OPERATOR_LEFT,
                },
            },
        ];
    }
    // ...
}
```

### Tests

The `getTests()` method lets you add new test functions:

```ts
class Project_Twig_Extension extends AbstractExtension {
    getTests() {
        return {
            even: new TwigTest('even', twig_test_even),
        };
    }
    // ...
}
```

### Definition vs Runtime

Twig filters, functions, and tests runtime implementations can be defined as any valid Javascript function:

-   **functions/static methods**: Simple to implement and fast (used by all Twig core extensions); but it is hard for the runtime to depend on external objects;

-   **anonymous**: Simple to implement;

-   **object methods**: More flexible and required if your runtime code depends on external objects.

The simplest way to use methods is to define them on the extension itself:

```ts
class Project_Twig_Extension extends AbstractExtension {
    constructor(private _rot13Provider) {}

    getFunctions() {
        return {
            rot13: new TwigFunction('rot13', this.rot13),
        };
    }

    rot13(value) {
        return this._rot13Provider.rot13(value);
    }
}
```

<!--
This is very convenient but not recommended as it makes template compilation
depend on runtime dependencies even if they are not needed (think for instance
as a dependency that connects to a database engine).

You can decouple the extension definitions from their runtime implementations by
registering a `RuntimeLoaderInterface` instance on the
environment that knows how to instantiate such runtime classes (runtime classes
must be autoload-able):

```ts
class RuntimeLoader implements RuntimeLoaderInterface
{
    load($class)
    {
        // implement the logic to create an instance of $class
        // and inject its dependencies
        // most of the time, it means using your dependency injection container
        if ('Project_Twig_RuntimeExtension' === $class) {
            return new $class(new Rot13Provider());
        } else {
            // ...
        }
    }
}
$twig->addRuntimeLoader(new RuntimeLoader());
```

Note: Twig comes with a PSR-11 compatible runtime loader (`ContainerRuntimeLoader`).

It is now possible to move the runtime logic to a new `Project_Twig_RuntimeExtension` class and use it directly in the extension:

```ts
class Project_Twig_RuntimeExtension
{
    constructor(private _rot13Provider) {}

    static rot13(value) {
        return this._rot13Provider.rot13(value);
    }
}
class Project_Twig_Extension extends AbstractExtension
{
    getFunctions() {
        return {
            rot13: new TwigFunction('rot13', Project_Twig_RuntimeExtension.rot13),
        };
    }
}
```
-->
