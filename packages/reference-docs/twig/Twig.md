# Twig

The framework give a pure TypeScript implementation of the Twig PHP templating language (<https://twig.symfony.com/>)

The goal is to provide a library that is compatible with both browsers and server side JavaScript.

## Feature Support

### Differences / Implementation Notes

-   named arguments (e.g. `|filter(param = value)`) are not supported.
-   currently it's does not have the same auto-escaping that Twig does.

### Built-in Tags

Docs: [Twig 3.x tags]<https://twig.symfony.com/doc/3.x/tags/index.html>

Example syntax: `{% tagName %}`

-   `apply`: _No_
-   `autoescape`: _No_
-   `block`: _No_
-   `cache`: _No_
-   `deprecated`: _No_
-   `do`: _No_
-   `embed`: _No_
-   `extends`: _No_
-   `flush`: _No_
-   `for`: _No_
-   `from`: _No_
-   `if`: _No_
-   `import`: _No_
-   `include`: _No_
-   `macro`: _No_
-   `sandbox`: _No_
-   `set`: _No_
-   `use`: _No_
-   `verbatim`: _No_
-   `with`: _No_

### Built-in Filters

Docs: [Twig 3.x filters]<https://twig.symfony.com/doc/3.x/filters/index.html>

Example syntax: `{{ variable|filterName }}`

-   `abs`: _Supported_
-   `batch`: _Supported_
-   `capitalize`: _Supported_
-   `column`: _Supported_
-   `convert_encoding`: _No_
-   `country_name`: _No_
-   `currency_name`: _No_
-   `currency_symbol`: _No_
-   `data_uri`: _No_
-   `date`: _No_
-   `date_modify`: _No_
-   `default`: _Supported_
-   `escape`: _No_
-   `filter`: _Supported_
-   `first`: _Supported_
-   `format`: _Supported_
-   `format_currency`: _No_
-   `format_date`: _No_
-   `format_datetime`: _No_
-   `format_number`: _No_
-   `format_time`: _No_
-   `html_to_markdown`: _No_
-   `inky_to_html`: _No_
-   `inline_css`: _No_
-   `join`: _Supported_
-   `json_encode`: _Supported_
-   `keys`: _Supported_
-   `language_name`: _No_
-   `last`: _Supported_
-   `length`: _Supported_
-   `locale_name`: _No_
-   `lower`: _Supported_
-   `map`: _Supported_
-   `markdown_to_html`: _No_
-   `merge`: _Supported_
-   `nl2br`: _Supported_
-   `number_format`: _Supported_
-   `raw`: _No_
-   `reduce`: _Supported_
-   `replace`: _Supported_
-   `reverse`: _Supported_
-   `round`: _Supported_
-   `slice`: _Supported_
-   `sort`: _Supported_
-   `spaceless`: _Supported_
-   `split`: _Supported_
-   `striptags`: _Supported_
-   `timezone_name`: _No_
-   `title`: _Supported_
-   `trim`: _Supported_
-   `u`: _No_
-   `upper`: _Supported_
-   `url_encode`: _Supported_

### Built-in Functions

Docs: [Twig 3.x functions]<https://twig.symfony.com/doc/3.x/functions/index.html>

Example syntax: `{{ functionName(arguments) }}`

-   `attribute`: _Supported_
-   `block`: _Supported_
-   `constant`: _No_ <!-- work as PHP constant function -->
-   `cycle`: _Supported_
-   `date`: _No_
-   `dump`: _No_
-   `html_classes`: _No_
-   `include`: _Supported_
-   `max`: _Supported_
-   `min`: _Supported_
-   `parent`: _Supported_
-   `random`: _Supported_
-   `range`: _Supported_
-   `source`: _Supported_
-   `country_timezones`: _No_
-   `template_from_string`: _No_

### Built-in Tests

Docs: [Twig 3.x tests]<https://twig.symfony.com/doc/3.x/tests/index.html>

Example syntax: `{{ expression is testName }}`

-   `constant`: _No_ <!-- work with PHP constant function -->
-   `defined`: _Supported_
-   `divisibleby`: _Supported_
-   `empty`: _Supported_
-   `even`: _Supported_
-   `iterable`: _Supported_
-   `null` / `none`: _Supported_
-   `odd`: _Supported_
-   `sameas`: _Supported_

### Built-in Operators

Docs: [Twig 3.x operators]<https://twig.symfony.com/doc/3.x/templates.html#expressions>

Example syntax: `{{ expression operator expression }}`

-   `in`: _Supported_
-   `is`: _Supported_
-   Math (`+`, `-`, `/`, `%`, `*`, `**`): _Supported_
-   Logic (`and`, `or`, `not`, `()`): _Supported_
-   Bitwise (`b-and`, `b-or`, `b-xor`): _Supported_
-   Comparisons (`==`, `!=`, `<`, `>`, `>=`, `<=`, `===`): _Supported_
-   Others (`..`, `|`, `~`, `.`, `[]`, `?:`): _Supported_
-   Null-coalescing (`??`): _Supported_
