# Environment variables

Instead of maintaining mutiple config file, one for each environment, KephaJs use environment variables for values that often change between your local and the production environment. For example: the database credentials, a boolean flag to toggle caching, ...

## Access environment variables

Node.js natively allows you to access the environment variables using the `process.env` object. For example:

```ts
process.env.NODE_ENV;
process.env.HOST;
process.env.PORT;
```

However, we recommend using the KephaJs `$env` service, as it further improves the API to work with environment variables by adding support for validations and provides static type information.

```ts
$env.get('NODE_ENV');

$env.get('HOST');
$env.get('PORT');
```

You can also type the return:

```ts
const host = $env.get<string>('HOST'); // string
```

## Use environment variables in values

Use environment variables in values by prefixing variables with `$`:

```
DB_USER=root
DB_PASS=${DB_USER}pass # Include the user as a password prefix -> 'rootpass'
```

## Why validate environment variables?

Environment variables are injected from outside-in to your application, and you have little or no control over them within your codebase.

For example, a section of your codebase relies on the existence of the `SESSION_DRIVER` environment variable.

There is no guarantee that at the time of running the program, the SESSION_DRIVER env variable exists and has the correct value. Therefore you must validate it vs. getting an error later in the program lifecycle complaining about the `undefined` value.

You begin by defining the validation rules inside the `env.ts` file in the root folder.

```ts
import { ObjectType } from '@kephajs/core/Validate/Type/Object';
import { Validator } from '@kephajs/core/Validate/Validator';

export function envValidator(
    defaultValidator: ObjectType<any>,
    $validator: Validator
) {
    return defaultValidator.extend({
        FOO: $validator.string(),
        OPTIONAL: $validator.number().optional(),
        A_NUMBER: $validator.preprocess(
            value => Number(value),
            $validator.number()
        ),
        BASIC: $validator.string(),
    });
}
```

Also, you can have type information and provides IntelliSense.

```ts
// ./contracts/EnvType.ts
declare module '@kephajs/console/Env/EnvType' {
    interface EnvType {
        BASIC: string;
    }
}

// ./index.ts
import './contracts/EnvType';

// when you type `get(` you will get choice between 'FOO', 'OPTIONAL', 'A_NUMBER', 'BASIC'
const basic = $env.get<string>('BASIC'); // string
```

## Usefull method

### `getAll`

Return all registered values.

### `has`

Get if a parameter is registered.

## Conection with the Config service

All resgistered values as also register in the Config Service as readonly parameter. The framework prefix all parameter name with `'ENV.'`.

```ts
$env.get('FOOBAR') === $config.get('ENV.FOOBAR');
```

## `.env` file

The framework take the parameters from different files :

-   `{ rootDir }/.env`
-   `{ rootDir }/.env.local`
-   `{ rootDir }/.env.{environment}` with `environment` the current application environment ('production', 'testing', ...)

The priority of the parameters is:

1. key from `process.env`
2. key from `{ rootDir }/.env.{environment}`
3. key from `{ rootDir }/.env.local`
4. key from `{ rootDir }/.env`

You can get the list of the loaded files (`true` if loaded, `false` if the file don't exist):

```ts
$env.getLoadedFiles();
/*
    {
        '.env': true,
        '.env.local': false,
        '.env.production': true,
    }
*/
```

You can pass the list of the file to load:

```ts
//$env.init(files: string[]);
$env.init(['file1', 'file2']);
```

### What rules does the parsing engine follow?

The parsing engine currently supports the following rules:

-   `BASIC=basic` becomes `{BASIC: 'basic'}`
-   empty lines are skipped
-   lines beginning with `#` are treated as comments
-   `#` marks the beginning of a comment (unless when the value is wrapped in quotes)
-   empty values become empty strings (`EMPTY=`becomes`{EMPTY: ''}`)
-   inner quotes are maintained (think JSON) (`JSON={"foo": "bar"}` becomes `{JSON:"{\"foo\": \"bar\"}"`)
-   whitespace is removed from both ends of unquoted values (`FOO= some value` becomes `{FOO: 'some value'}`)
-   single and double quoted values are escaped (`SINGLE_QUOTE='quoted'` becomes `{SINGLE_QUOTE: "quoted"}`)
-   single and double quoted values maintain whitespace from both ends (`FOO=" some value "` becomes `{FOO: ' some value '}`)
-   support mutline value with double quoted

```
PRIVATE_KEY="-----BEGIN RSA PRIVATE KEY-----
...
Kh9NV...
...
-----END DSA PRIVATE KEY-----"
```

-   double quoted values expand new lines (`MULTILINE="new\nline"` becomes

```
{MULTILINE: 'new
line'}
```

-   backticks are supported (`` BACKTICK_KEY=`This has 'single' and "double" quotes inside of it.` ``)
