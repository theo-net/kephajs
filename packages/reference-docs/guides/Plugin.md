# Extend KephaJs

You can create some plugins to extend the framework.

When you create a plugin, you need to implement the `PluginInterface`.

After you can register your plugin in your application:

```ts
class MyPlugin implement PluginInterface {}

const app = new Application('test');
app.registerPlugin(new MyPlugin());
app.init();
```

As you can see, you need to register your plugins before the initialization of your application.

During the `app.init()`, the method `boot` of each plugin will be called between the states `registered` and `booted`. Then, when the application is in the `booted` state, the method `ready` will be called. Finally, the application will be in the `ready` state.

The `boot` and `ready` methods need to return a `Promise`.

The `boot` method can receive the `app` instance. With this instance, you can register some services, validator, commands, ...
