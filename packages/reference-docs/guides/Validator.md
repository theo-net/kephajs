# Validator

## Validator service

Basic usage:

### Creating a simple string schema

```ts
const validator = new Validator();

// creating a schema for strings
const mySchema = validator.string();

// parsing
mySchema.parse('tuna'); // => 'tuna'
mySchema.parse(12); // => throws ErrorValidator

// 'safe' parsing (doesn't throw error if validation fails)
mySchema.safeParse('tuna'); // => { success: true; data: 'tuna' }
mySchema.safeParse(12); // => { success: false; error: ErrorValidator }
```

### Creating an object schema

```ts
const validator = new Validator();

const User = validator.object({
    username: validator.string(),
});

User.parse({ username: 'Ludwig' });

// extract the inferred type
type User = inferValidator<typeof User>; // You can get this in the root of core
// { username: string }
```

### Writing generic functions

When attempting to write a functions that accepts a schemas as an input, it's common to
try something like this:

```ts
function makeSchemaOptional<T>(schema: Type<T>) {
    return schema.optional();
}
```

This approach has some issues. The `schema` variable in this function is typed as an
instance of `Type`, which is an abstract class that all schemas inherit from. This
approach loses type information, namely which subclass the input actually is.

```ts
const arg = makeSchemaOptional(v.string());
arg.unwrap();
```

A better approach is for the generate parameter to refer to the schema as a whole.

```ts
function makeSchemaOptional<T extends TypeAny>(schema: T) {
    return schema.optional();
}
```

`TypeAny` is just a shorthand for `Type<any, any, any>`, a type that is broad enough
to match any schema.

As you can see, `schema` is now fully and properly typed.

```ts
const arg = makeSchemaOptional(v.string());
arg.unwrap(); // StringType
```

### Constraining allowable inputs

The `Type` class has three generic parameters.

```ts
class Type<
    Output = any,
    Def extends TypeDef = TypeDef,
    Input = Output
> { ... }

By constraining these in your generic input, you can limit what schemas are allowable
as inputs to your function:

function makeSchemaOptional<T extends Type<string>>(schema: T) {
    return schema.optional();
}

makeSchemaOptional(v.string());
// works fine

makeSchemaOptional(v.number());
// Error: 'NumberType' is not assignable to parameter of type 'Type<string, TypeDef, string>'
```

## Recursive types

You can define a recursive schema with the Validator, but because of a limitation of TypeScript,
their type can't be statically inferred. Instead you'll need to define the type definition
manually, and provide it to Validator as a "type hint".

```ts
interface Category {
    name: string;
    subcategories: Category[];
}

// cast to Type<Category>
const Category: Type<Category> = LazyType.create(() =>
    ObjectType.create({
        name: StringType.create(),
        subcategories: ArrayType.create(Category),
    })
);

Category.parse({
    name: 'People',
    subcategories: [
        {
            name: 'Politicians',
            subcategories: [{ name: 'Presidents', subcategories: [] }],
        },
    ],
}); // passes
```

Unfortunately this code is a bit duplicative, since you're declaring the types twice: once
in the interface and again in the schema definition.

### JSON type

If you want to validate any JSON value, you can use the snippet below.

```ts
const literalSchema = Union.create([
    StringType.create(),
    NumberType.create(),
    BooleanType.create(),
    NullType.create(),
]);
type Literal = TypeOf<typeof literalSchema>;
type Json = Literal | { [key: string]: Json } | Json[];
const jsonSchema: Type<Json> = LazyType.create(() =>
    Union.create([
        literalSchema,
        ArrayType.create(jsonSchema),
        RecordType(jsonSchema),
    ])
);

jsonSchema.parse(data);
```

### Cyclical objects

Despite supporting recursive schemas, passing cyclical data into Validator will cause an infinite loop.

## Types

### `Any`

Catch all type, accept any value.

### `Array`

Describe an Array.

You have two way to use this type:

```ts
const stringArray = ArrayType.create(StringType.create());
// or
const stringArray = StringType.create().array();
```

Be carefull with the `.array` method. It returns a new `ArrayType` instance. This
means the order in whitch you call methods matters.

```
StringType.create().optional().array(); // (string | undefined)[]
StringType.create().array().optional(); // string[] | undefined
```

Use `.element` method to access the schema for an element of the array.

If you want to ensure that an array contains at least one element, use `.nonEmpty` method.

Minimum lenght of the array with `.min` method, maximum with `.max` and exact length with `.length`.

### `BigInt`

Describe a `bigint`.

### `Boolean`

Describe an `boolean`.

### `Date`

Describe a `Date`.

Accepts a date, not a date string (like `"2022-05-24T00:00:00.000Z"`)

To allow for dates or date strings, you can use preprocess

```ts
const dateSchema = validator.preprocess(arg => {
    if (typeof arg == 'string' || arg instanceof Date) return new Date(arg);
    throw new Error();
}, validator.date());
type DateSchema = validator.infer<typeof dateSchema>;
// type DateSchema = Date

dateSchema.safeParse(new Date('1/12/22')); // success: true
dateSchema.safeParse('2022-01-12T00:00:00.000Z'); // success: true
```

Return also error if the date is invalid (like `new Date("invalid")`)

### `DiscriminatedUnion`

If the union consists of object schemas all identifiable by a common property, it
is possible to use this object.

The advantage is in more efficient evaluation and more human friendly errors. With
the basic union method the input is tested against each of the provided "options",
and in the case of invalidity, issues for all the "options" are shown in the
`ErrorValidator`. On the other hand, the discriminated union allows for selecting
just one of the "options", testing against it, and showing only the issues related
to this "option".

```ts
const item = DiscriminatedUnion.create('type', [
    ObjectType.create({
        type: LiteralType.create('a'),
        a: StringType.create(),
    }),
    ObjectType.create({
        type: LiteralType.create('b'),
        b: StringType.create(),
    }),
]).parse({ type: 'a', a: 'abc' });
```

### `Enum`

It's a framework-native way to declare a schema with a fixed set of allowable string
values. Pass the array of values directly into z.enum().

```ts
const FishEnum = v.enum(['Salmon', 'Tuna', 'Trout']);
type FishEnum = v.infer<typeof FishEnum>;
// 'Salmon' | 'Tuna' | 'Trout'
```

Alternatively, use as const to define your enum values as a tuple of strings.

```ts
const VALUES = ['Salmon', 'Tuna', 'Trout'] as const;
const FishEnum = v.enum(VALUES);
```

This is not allowed, since the framework isn't able to infer the exact values of each
elements.

```ts
const fish = ['Salmon', 'Tuna', 'Trout'];
const FishEnum = v.enum(fish);
```

#### Autocompletion

To get autocompletion with a framework enum, use the `.enum` property of your schema:

```ts
FishEnum.enum.Salmon; // => autocompletes

FishEnum.enum;
// => {
//     Salmon: "Salmon",
//     Tuna: "Tuna",
//     Trout: "Trout",
// }
```

You can also retrieve the list of options as a tuple with the `.options` property:

```ts
FishEnum.options; // ["Salmon", "Tuna", "Trout"]);
```

### `Function`

You can define "function schemas". This makes it easy to validate the inputs and outputs of a
function without intermixing your validation code and "business logic".

You can create a function schema with `FunctionType.create(args, returnType)`.

```ts
const myFunction = FunctionType.create();

type myFunction = TypeOf<typeof myFunction>;
// => ()=>unknown
```

#### Define inputs and outputs.

```ts
const myFunction = FunctionType.create()
    .args(StringType.create(), NumberType.create()) // accepts an arbitrary number of arguments
    .returns(BooleanType.create());
type myFunction = TypeOf<typeof myFunction>;
// => (arg0: string, arg1: number)=>boolean
```

Function schemas have an `.implement()` method which accepts a function and returns a new function
that automatically validates it's inputs and outputs.

```ts
const trimmedLength = FunctionType.create()
    .args(StringType.create()) // accepts an arbitrary number of arguments
    .returns(NumberType.create())
    .implement(x => {
        // TypeScript knows x is a string!
        return x.trim().length;
    });

trimmedLength('sandwich'); // => 8
trimmedLength(' asdf '); // => 4
```

If you only care about validating inputs, just don't call the `.returns()` method. The output type will
be inferred from the implementation.

You can use the special `VoidType.create()` option if your function doesn't return anything. This will
let the validator properly infer the type of void-returning functions. (Void-returning functions actually
return `undefined`.)

```ts
const myFunction = FunctionType.create()
    .args(StringType.create())
    .implement(arg => {
        return [arg.length]; //
    });
myFunction; // (arg: string)=>number[]
```

Extract the input and output schemas from a function schema.

```ts
myFunction.parameters();
// => TupleType<[StringType, NumberType]>

myFunction.returnType();
// => BooleanType
```

### `Literal`

To type litterals.

### `Map`

```ts
const stringNumberMap = MapType.create(
    StringType.create(),
    NumberType.create()
);
type StringNumberMap = TypeOf<typeof stringNumberMap>;
// type StringNumberMap = Map<string, number>
```

### `NaNType`

To type `NaN`.

### `NativeEnum`

The EnumType is the recommended approach to defining and validating enums. But if you
need to validate against an enum from a third-party libray (or you don't want to
rewrite your existing enums) you can use `NativeEnumType`.

Works with numeric enums:

```ts
enum Foobar {
    Foo,
    Bar,
}
const FoobarEnum = v.nativeEnum(Foobar);
type FoobarEnum = v.infer<typeof Foobar>; // Foobar
FobarEnum.pars(Foobar.Foo); // passes
FobarEnum.pars(Foobar.Bar); // passes
FobarEnum.pars(0); // passes
FobarEnum.pars(1); // passes
FobarEnum.pars(2); // fails
```

Works with string enums:

```ts
enum Foobar {
    Foo = 'foo',
    Bar = 'bar,
    Foobar, // you can mix numerical and string enums
}
const FoobarEnum = v.nativeEnum(Foobar);
type FoobarEnum = v.infer<typeof Foobar>; // Foobar
FobarEnum.pars(Foobar.Foo); // passes
FobarEnum.pars(Foobar.Foobar); // passes
FobarEnum.pars('foo'); // passes
FobarEnum.pars('bar'); // passes
FobarEnum.pars(0); // fails
FobarEnum.pars('Foobar'); // fails
```

Works with const enums:

```ts
const Foobar = {
    Foo = 'foo',
    Bar = 'bar,
    Foobar: 3,
} as const;
const FoobarEnum = v.nativeEnum(Foobar);
    type FoobarEnum = v.infer<typeof Foobar>; // 'foo' | 'bar' | 3
FobarEnum.pars('foo'); // passes
FobarEnum.pars('bar'); // passes
FobarEnum.pars(0); // fails
FobarEnum.pars('Foobar'); // fails
```

Tou can access the underlying object with the `.enum` property:

```ts
Foobar.enum.Foo; // 'foo'
```

### `Never`

Never type, allows no values.

### `Null`

Type `null` validation.

### `Nullable`

You can make any schema nullable with this type. This wrap the schema and
returns the results.

```ts
const schema = Nullable.create(StringType.create());
schema.parse(null); // => returns null
type A = TypeOf<typeof schema>; // string | null
```

For your convenience, you can also call the `nullable` method on an existing schema.

```ts
const schema = StringType.create().nullable();
```

Extract the wrapped schema:

```ts
const stringSchema = StringType.create();
const nullableString = stringSchema.nullable();
nullableString.unwrap() === stringSchema; // true
```

### `Number`

Number validation.

You can customize somme common errors messages when creating a number schema:

```ts
const age = NumberType.create({
    requiredError: 'Age is required',
    invalidTypeError: 'Age must be a string',
});
```

When using validation methods, you can pass in an additional argument to
provide a custom error message.

#### Methods:

-   `.min(minValue)` The number need to be more or equal than `minValue`
-   `.max(maxValue)` The number need to be less or equal than `maxValue`
-   `.gt(minValue)` The number need to be more than `minValue` (not include the value)
-   `.gte(minValue)` The number need to be more or equal than `minValue`
-   `.lt(maxValue)` The number need to be less than `maxValue` (not include the value)
-   `.lte(maxValue)` The number need to be less or equal than `maxValue`
-   `.int()` The number need to be an integer
-   `.positive()` The number need to be positive (not include 0)
-   `.nonNegative()` The number need to be negative or 0
-   `.negative()` The number need to be negative (not include 0)
-   `.nonPositive()` The number need to be positive or 0
-   `.multipleOf(value)` The number need to be an multiple of `value`

### `Object`

All property are required by default

```ts
const Person = ObjectType.create({
    name: StringType.create(),
    age: NumberType.create(),
});
```

Extract the inferred type

```ts
type PersonType = TypeOf<typeof Person>;
```

equivalent to:

```ts
type PersonType = {
    name: string;
    age: number;
};
```

By default, it's nonstrict and strip unknown keys

#### `.shape()`

Return the member of the object (give each validator for each key)

You can use it to access the schemas for a particular key:

```ts
Person.shape.name; // => string schema
```

#### Extend

You can add additional fields an object scheam with this method:

```ts
const Student = Person.extend({
    class: StringType.create(),
});
```

#### Merging

Merge is equivalent to `A.extend(B.shape)`

If the two schemas share keys, the properties of B ovverides the property
of A. The returned schema also inherits the "unKownKeys" policy and the
catchall schema of B.

```ts
A.merge(B);
```

#### Pick

To only keep certain keys

```ts
const JustName = Person.pick({ name: true });
type JustNameType = TypeOf<type of JustName>; // => { name: string }
```

#### Omit

To remove certain keys

```ts
const JustName = Person.omit({ age: true });
type JustNameType = TypeOf<type of JustName>; // => { name: string }
```

#### Partial

Makes all properties optional

You can also specify wich properties to male optional:

```ts
const optionalEmail = user.partial({ email: true });
```

#### Passthrought

By default objects scheamas strip out unrecognize keys during passing. Instead,
if you want to pass through unknown keys, use `passthrough()`.

#### Strict

By default objects schemas strip out unreconized keys during parsing. You can disallow
unknown keys with `.strict()`. If there are any unknown keys in the input, we will
throw an error.

#### Strip

You can use the `.strip` method to reset an object schema to the default behavior
(stripping unrecognized keys).

#### Catch all

You can pass a "catchall" schema into an object schema. All unknown keys will be validated
against it.

Using `.catchall()` obviates `.passthrough()`, `.strip()`, or `.strict()`. All keys are now
considered "known".

### `Optional`

You can make any schema optional with this type. This wrap the schema and
returns the results.

```ts
const schema = Optional.create(StringType.create());
schema.parse(undefined); // => returns undefined
type A = TypeOf<typeof schema>; // string | undefined
```

For your convenience, you can also call the `optional` method on an existing schema.

```ts
const schema = StringType.create().optional();
```

Extract the wrapped schema

```ts
const stringSchema = StringType.create();
const optionalString = stringSchema.optional();
optionalString.unwrap() === stringSchema; // true
```

### `Record`

Record schemas are used to validate types such as `{ [k: string]: number }`.

If you want to validate the values of an object against some schema but don't care
about the keys, use `RecordType.create(valueType)`:

```ts
const NumberCache = RecordType.create(NumberType.create());

type NumberCache = TypeOf<typeof NumberCache>;
// => { [k: string]: number }
```

This is particularly useful for storing or caching items by ID.

```ts
const userStore: UserStore = {};

userStore['77d2586b-9e8e-4ecf-8b21-ea7e0530eadd'] = {
    name: 'Carlotta',
}; // passes

userStore['77d2586b-9e8e-4ecf-8b21-ea7e0530eadd'] = {
    whatever: 'Ice cream sundae',
}; // TypeError
```

#### Record key type

If you want to validate both the keys and the values, use `RecordType.create(keyType, valueType)`:

```ts
const NoEmptyKeysSchema = RecordType.create(
    StringType.create().min(1),
    v.number()
);
NoEmptyKeysSchema.parse({ count: 1 }); // => { 'count': 1 }
NoEmptyKeysSchema.parse({ '': 1 }); // fails
```

(Notice how when passing two arguments, `valueType` is the second argument)

#### A note on numerical keys

While `RecordType.create(keyType, valueType)` is able to accept numerical key types and TypeScript's
built-in Record type is `Record<KeyType, ValueType>`, it's hard to represent the TypeScript type
`Record<number, any>` in the framework.

As it turns out, TypeScript's behavior surrounding `[k: number]` is a little unintuitive:

```ts
const testMap: { [k: number]: string } = {
    1: 'one',
};

for (const key in testMap) {
    console.log(`${key}: ${typeof key}`);
}
// prints: `1: string`
```

As you can see, JavaScript automatically casts all object keys to strings under the hood. Since the
framework is trying to bridge the gap between static and runtime types, it doesn't make sense to
provide a way of creating a record schema with numerical keys, since there's no such thing as a
numerical key in runtime JavaScript.

### `Set`

Type `Set` validation

```ts
const numberSet = SetType.create(NumberType.create());
type NumberSet = TypeOf<typeof numberSet>;
// type NumberSet = Set<number>
```

Set schemas can be further contrainted with the following utility methods.

```ts
SetType.create(StringType.create()).nonempty(); // must contain at least one item
SetType.create(StringType.create()).min(5); // must contain 5 or more items
SetType.create(StringType.create()).max(5); // must contain 5 or fewer items
SetType.create(StringType.create()).size(5); // must contain 5 items exactly
```

#### Methods

-   `.min(minSize)` Define minimum size
-   `.max(maxSize) `Define maximum size
-   `.size(size)` Define size
-   `.nonempty()` Cannot be empty

### `String`

You can customize somme common errors messages when creating a string schema:

```ts
const name = StringType.create({
    requiredError: 'Name is required',
    invalidTypeError: 'Name must be a string',
});
```

When using validation methods, you can pass in an additional argument to
provide a custom error message.

-   `.min(minLength)` The string need to have more than `minLength` characters
-   `.max(maxLength)` The string need to have less than `maxLength` characters
-   `.length(length)` The string need to have exactly `length` characters
-   `.email()` Check if the string is an valid email
-   `.url()` Check if the string is an valid Url
-   `.uuid()` Check if the string is an valid UUID
-   `.cuid()` Check if the string is an valid CUID
-   `.regexp(regexp)` Check if the string pass a RegExp
-   `.isEmail()` Return if this type is an email address
-   `.isUrl()` Return if this type is an URL
-   `.isUuid()` Return if this type is a UUID
-   `.isCuid()` Return if this type is a CUID
-   `.minLength()` Return the minimum length
-   `.maxLength()` Return the maximum length

### `Tuple`

Unlike arrays, tuples have a fixed number of elements and each element can have a different type.

```ts
const athleteSchema = TupleType.create([
    StringType.create(),
    NumberType.create(),
    ObjectType.create({
        pointsScored: NumberType.create(),
    }),
]);

type Athlete = v.infer<typeof athleteSchema>;
// type Athlete = [string, number, { pointsScored: number }]
```

#### `.rest(rest)`

Special method: `FunctionType` use it to contain a rest parameter as a TupleType.
The rest parameter syntax allow a function to accept an indefinite number of
arguments as an array, providing a way to represent a variadic functions.

```ts
function sum(...rest: number[]): number {
    return rest.reduce((previous, current) => {
        return previous + current;
    });
}
```

### `Undefined`

Type `undefined` validation.

### `Unknown`

Catch all type, accept any value.

### `Void`

Type `void` validation, accept also `undefined`.

### `Union`

The framework includes a built-in `v.union` method for composing "OR" types.

```ts
const stringOrNumber = Union.create([StringType.create(), NumberType.create()]);

stringOrNumber.parse('foo'); // passes
stringOrNumber.parse(14); // passes
```

We will test the input against each of the "options" in order and return the first value
that validates successfully.

For convenience, you can also use the `.or` method:

```ts
const stringOrNumber = StringType.create().or(Number.create());
```

## `Intersection`

Intersections are useful for creating "logical AND" types. This is useful for
intersecting two object types.

```ts
const Person = ObjectType.create({
    name: StringType.create(),
});
const Employee = ObjectType.create({
    role: StringType.create(),
});
const EmployedPerson = Intersection.create(Person, Employee);
// equivalent to:
const EmployedPerson = Person.and(Employee);
```

Though in many cases, it is recommended to use `A.merge(B)` to merge two objects.
The `.merge` method returns a new `ObjectType` instance, whereas `A.and(B)` returns
a less useful `Intersection` instance that lacks common object methods like pick and
omit.

```ts
const a = Union.create([NumberType.create(), StringType.create()]);
const b = Union.create([NumberType.create(), BooleanType.create()]);
const c = Intersection.create(a, b);
type c = TypeOf<typeof c>; // => number
```

## Extract the inferred type

```ts
const v = new Validator();
const User = v.object({
    username: v.string(),
});
type User = TypeOf<typeof User>; // import from Validate/Type/Type
// { username: string }
```

## Parse

Check the data with `parse` method. If it's valid, a value is returned with full type information.
Otherwise, an error is thrown.

IMPORTANT: The value returned by `.parse` is a deep clone of the variable you passed in.

```ts
const stringSchema = StringType.create();
stringSchema.parse('fish'); // => returns 'fish'
stringSchema.parse(12); // throws Error('Non-string type: number');
```

With `safeParse` method, you can parse the values, but fon'y throw errors when validation fails.

If you don't want Validator to throw errors when validation fails, use `.safeParse`.
This method returns an object containing either the successfully parsed data or a
`ErrorValidator` instance containing detailed information about the validation problems.

```ts
stringSchema.safeParse(12);
// => { success: false; error: ErrorValidator }

stringSchema.safeParse('billie');
// => { success: true; data: 'billie' }
```

The result is a discriminated union so you can handle errors very conveniently:

```ts
const result = stringSchema.safeParse('billie');
if (!result.success) {
    // handle error then return
    result.error;
} else {
    // do something
    result.data;
}
```

## Convenience methods

You have some conveniences method for your schemas:

-   `optional` A convenience method that returns an optional version of a schema
-   `nullable` A convenience method that returns an nullable version of a schema
-   `nullish` A convenience method that returns a "nullish" version of a schema. Nullish schemas will accept both `undefined` and `null`
-   `array` A convenience method that returns an array schema for the given type
-   `or` A convenience method for union types
-   `and` A convenience method for creating intersection types

## Default

You can use transforms to implement the concept of "default values".

```ts
const stringWithDefault = StringType.create().default('tuna');

stringWithDefault.parse(undefined); // => 'tuna'
```

Optionally, you can pass a function into `.default` that will be re-executed whenever a default
value needs to be generated:

```ts
const numberWithRandomDefault = NumberType.create().default(Math.random);

numberWithRandomDefault.parse(undefined); // => 0.4413456736055323
numberWithRandomDefault.parse(undefined); // => 0.1871840107401901
numberWithRandomDefault.parse(undefined); // => 0.7223408162401552
```

## Refine

`.refine(validator: (data:T) => any, params?: RefineParams)`

We let you provide custom validation logic via refinements. (For advanced features like
creating multiple issues and customizing error codes, see `.superRefine.`)

The validator was designed to mirror TypeScript as closely as possible. But there are many
so-called "refinement types" you may wish to check for that can't be represented in TypeScript's
type system. For instance: checking that a number is an integer or that a string is a valid email
address.

For example, you can define a custom validation check on any schema with `.refine`:

```ts
const myString = StringType.create().refine(val => val.length <= 255, {
    message: "String can't be more than 255 characters",
});
```

⚠️ Refinement functions should not throw. Instead they should return a falsy value to signal failure.

###Arguments

As you can see, `.refine` takes two arguments.

1.  The first is the validation function. This function takes one input (of type `T` — the
    inferred type of the schema) and returns `any`. Any truthy value will pass validation.
2.  The second argument accepts some options. You can use this to customize certain
    error-handling behavior:

```ts
type RefineParams = {
    // override error message
    message?: string;

    // appended to error path
    path?: (string | number)[];

    // params object you can use to customize message
    // in error map
    params?: object;
};
```

For advanced cases, the second argument can also be a function that returns `RefineParams`/`

```ts
StringType.create().refine(
    val => val.length > 10,
    val => ({ message: `${val} is not more than 10 characters` })
);
```

### Customize error path

```ts
const passwordForm = ObjectType.create({
    password: StringType.create(),
    confirm: StringType.create(),
})
    .refine(data => data.password === data.confirm, {
        message: "Passwords don't match",
        path: ['confirm'], // path of error
    })
    .parse({ password: 'asdf', confirm: 'qwer' });
```

Because you provided a path parameter, the resulting error will be:

```ts
ErrorValidator {
    errors: [{
        'code': 'custom',
        'path': [ 'confirm' ],
        'message': "Passwords don't match"
    }]
}
```

### Relationship to transforms

Transforms and refinements can be interleaved:

```ts
StringType.create()
    .transform(val => val.length)
    .refine(val => val > 25);
```

### Super refine

The `.refine` method is actually syntactic sugar atop a more versatile (and verbose) method
called `superRefine`. Here's an example:

```ts
const Strings = ArrayType.create(StringType.create()).superRefine(
    (val, ctx) => {
        if (val.length > 3) {
            ctx.addIssue({
                code: IssueCode.tooBig,
                maximum: 3,
                type: 'array',
                inclusive: true,
                message: 'Too many items 😡',
            });
        }

        if (val.length !== new Set(val).size) {
            ctx.addIssue({
                code: IssueCode.custom,
                message: `No duplicated allowed.`,
            });
        }
    }
);
```

You can add as many issues as you like. If `ctx.addIssue` is NOT called during the execution
of the function, validation passes.

Normally refinements always create issues with a `IssueCode.custom` error code, but with
`superRefine` you can create any issue of any code.

#### Abort early

By default, parsing will continue even after a refinement check fails. For instance, if you
chain together multiple refinements, they will all be executed. However, it may be desirable
to abort early to prevent later refinements from being executed. To achieve this, pass the
fatal flag to `ctx.addIssue`:

```ts
const Strings = NumberType.create()
    .superRefine((val, ctx) => {
        if (val < 10) {
            ctx.addIssue({
                code: IssueCode.custom,
                message: 'foo',
                fatal: true,
            });
        }
    })
    .superRefine((val, ctx) => {
        if (val !== ' ') {
            ctx.addIssue({
                code: IssueCode.custom,
                message: 'bar',
            });
        }
    });
```

## Transform

To transform data after parsing, use the `transform` method.

```ts
const stringToNumber = StringType.create().transform(val => myString.length);
stringToNumber.parse('string'); // => 6
```

⚠️ Transform functions must not throw. Make sure to use refinements before the transform or
addIssue within the transform to make sure the input can be parsed by the transform.

### Chaining order

Note that `stringToNumber` above is an instance of the `Effects` subclass. It is NOT an
instance of `StringType`. If you want to use the built-in methods of `StringType` (e.g.
`.email()`) you must apply those methods before any transforms.

```ts
const emailToDomain = StringType.create()
    .email()
    .transform(val => val.split('@')[1]);

emailToDomain.parse('colinhacks@example.com'); // => example.com
```

### Validating during transform

Similar to `superRefine`, `transform` can optionally take a `ctx`. This allows you to
simultaneously validate and transform the value, which can be simpler than chaining `refine`
and `validate`. When calling `ctx.addIssue` make sure to still return a value of the correct
type otherwise the inferred type will include `undefined`.

```ts
const Strings = StringType.create().transform((val, ctx) => {
    const parsed = parseInt(val);
    if (isNaN(parsed)) {
        ctx.addIssue({
            code: IssueCode.custom,
            message: 'Not a number',
        });
    }
    return parsed;
});
```

### Relationship to refinements

Transforms and refinements can be interleaved. These will be executed in the order they are declared.

```ts
StringType.create()
    .transform(val => val.toUpperCase())
    .refine(val => val.length > 15)
    .transform(val => `Hello ${val}`)
    .refine(val => val.indexOf('!') === -1);
```

## Error handler

The validator throw `ErrorValidator`.

You can get the list of the issues with `.errors`.

### Format

Format the error with `format` method.

With this method, you don't need to parse a list of Issue, you can just
read for each field, the list of errors.

```ts
{
    name: {
        _errors: ['Expected string, received null']
    },
    contactInfo: {
        email: {
            _errors: ['Invalid email']
        }
    }
}
```

To transform the result like:

```ts
result.error.flatten((issue: ZodIssue) => ({
    message: issue.message,
    errorCode: issue.code,
}));
```

You will have:

```ts
{
    formErrors: [],
    fieldErrors: {
        name: [
            {message: "Expected string, received null", errorCode: "invalid_type"}
        ]
        contactInfo: [
            {message: "Invalid email", errorCode: "invalid_string"}
        ]
    },
}
```

`format()` returns a deeply nested object. With `flatten` you have a
one level deep.

```ts
{
    formErrors: [],
    fieldErrors: {
        name: ['Expected string, received null'],
        contactInfo: ['Invalid email']
    },
}
```

`formErrors` is a list of issues that occurred on the root of the object schema.

### Customize error messages

You can customize all error messages by providing a custom "error map".

```ts
const customErrorMap: ErrorMap = (issue, ctx) => {
    if (issue.code === IssueCode.invalidType) {
        if (issue.expected === 'string') {
            return { message: 'Need to be a string!' };
        }
    }
    return { message: ctx.defaultError };
};
```

A custom error maps doesn't need to produce an error message for every kind of
issue. The `defaultError` are determined by this priority:

-   from `defaultErrorMap`
-   from the error map set in the Validator service
-   from schema-bound error map (`.string({ errorMap: myErrorMap })`)
-   from contextual error map (`.string().parse('foobar', { errorMap: myErrorMap })`)

Param `ctx`: `{ defaultError: string; data: any }` with `defaultError` as the message
error generated by the default error map; and `data` as the data that
passed into `.parse`.

## Register custom validator

You can register custom validator in you Validator service. As you will access to it in all of your application.

To declare a new Type:

```ts
// ./src/Validator/MyType.ts

import {
    FirstPartyTypeKind,
    RawCreateParams,
    Type,
    TypeDef,
} from '@kephajs/core/Validate/Type/Type';
import { IssueCode } from '@kephajs/core/Validate/Validator';

export type MyTypeDef = {
    typeName: FirstPartyTypeKind.StringType;
} & TypeDef;

export class MyType extends Type<string, MyTypeDef> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        // your logic
    }

    static create = (params?: RawCreateParams): MyType => {
        return new MyType({
            typeName: FirstPartyTypeKind.StringType,
            ...MyType.processCreateParams(params),
        });
    };
}
```

Then you register it:

```ts
// ./src/index.ts
import { Application } from '@kephajs/console/Application';
import { MyType } from './src/Validator/MyType';

const app = new Application(__dirname);
app.start();

app.container.get('$validator').register('myType', MyType.create());
```

You can also register directly a validator:

```ts
// ./src/index.ts
import { Application } from '@kephajs/console/Application';
import { StringType } from '@kephajs/core/Validate/Type/String';

const app = new Application(__dirname);
app.start();

app.container
    .get('$validator')
    .register('shortString', StringType.create().max(5));
```

After, you need to inform the TypeScript compiler:

```ts
// ./contracts/ApplicationValidators.ts
import { StringType } from '@kephajs/core/Validate/Type/String';
import { MyType } from '../src/Validator/MyType';

declare module '@kephajs/core/Validate/ApplicationValidators' {
    interface ApplicationValidators {
        myType: MyType;
        stringShort: StringType;
    }
}

// ./index.ts
import './contracts/ApplicationValidators';
```

To use your validators:

```ts
// $validator is your Validator Service
$validator.rules.myType.parse('testLol');
$validator.rules.stringShort.parse('testLol');
```

## Check instanceof

You can use `.instanceof` to check that the input is an instance of a class. This is
useful to validate inputs against classes that are exported from third-party libraries.

```ts
class Test {
    name: string;
}

const TestSchema = validator.instanceof(Test);

const blob: any = 'whatever';
TestSchema.parse(new Test()); // passes
TestSchema.parse(blob); // throws
```

## Preprocess

Typically the framework operates under a "parse then transform" paradigm. It validates the
input first, then passes it through a chain of transformation functions. (For more information
about transforms, read the `.transform` docs.)

But sometimes you want to apply some transform to the input before parsing happens. A common
use case: type coercion. The framework enables this with this `preprocess` method.

```ts
const castToString = v.preprocess(val => String(val), StringType.create());
```

This returns an `Effects` instance. `Effects` is a wrapper class that contains all logic
pertaining to preprocessing, refinements, and transforms.

## Error handler

### Define a new default `ErrorMap`

```ts
$validator->setErrorMap(MyErrorMap);
```
