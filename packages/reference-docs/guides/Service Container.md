# Service Container

Your application is _full_ of useful objects: a "Mailer" object might help you send emails while another object might help you save things to the database.

In KephaJs, these useful objects are called **services** and each service lives inside a very special object called the **service container**. The container allows you to centralize the way objects are constructed. It makes your life easier, promotes a strong architecture.

When a service is injected, the container constructs a new object and returns it. But if you never ask for the service, it's never constructed: saving memory and speed. As a bonus, the service is only created once: the same instance is returned each time you ask for it.

The service container is 100% type safe Dependency Injection: if your code compiles, it works!

```ts
import { ServiceContainer } from '@kephajs/core/ServiceContainer/ServiceContainer';

class Logger {
    info(message: string) {
        console.log(message);
    }
}

class HttpClient {
    constructor(private readonly log: Logger) {}

    static $inject = ['logger'] as const;
}

class MyService {
    constructor(
        private readonly http: HttpClient,
        private readonly log: Logger
    ) {}

    static $inject = ['httpClient', 'logger'] as const;
}

const serviceContainer = ServiceContainer.init()
    .provideValue('logger', new Logger())
    .provideClass('httpClien', HttpClient);

const myService = serviceContainer.injectClass(MyService);
```

## Declaring your dependencies

Declare a public static property `$inject`, as string array, with your dependencies. It was very important that the _exact order of the tokens in the `$inject` array matched the parameters in the constructor function_.

If the constructor parameters of an injectable don't mach it's declared tokens, an error will throw. The framework test the names and the type.

**To work, the property `$inject` need to be declare `as const`:**

```ts
class MyService {
    constructor(
        private readonly http: HttpClient,
        private readonly log: Logger
    ) {}

    static $inject = ['httpClient', 'logger'] as const; // look here
}

function myFunction() {
    return 'foobar';
}
fooDecorator.$inject = ['foo'] as const; // look here
```

## Child injector

You start with an injector that have just `$container` with the `ServiceContainer`. To do anything usefull with your injector, you'll need to create child injector. This what you do with the `provideXXX` methods.

But, when you initialize your service container with an `Application` instance, you will provide the first service: `$app` that is this instance.

## Decorate your dependencies

With the decorator design pattern you can dynamically add functionality to existing dependencies.

```ts
import { ServiceContainer } from '@kephajs/core/ServiceContainer/ServiceContainer';

class Foo {
    bar() {
        console.log('bar!');
    }
}

function fooDecorator(foo: Foo) {
    return {
        bar() {
            console.log('before call');
            foo.bar();
            console.log('after call');
        },
    };
}
fooDecorator.$inject = ['foo'] as const;

const fooProvider = ServiceContainer.init()
    .provideClass('foo', Foo)
    .provideFactory('foo', fooDecorator);
const foo = fooProvider.resolve('foo');

foo.bar();
// => "before call"
// => "bar!"
// => "after call"
```

## Lifecycle control

With the third `Scope` parameter of `provideFactory` and `provideClass` methods, you can determine the lifecycle dependencies.

A scope has two possible values:

-   `Scope.Singleton` (default value) to enable caching. Every time the dependency needs to be provided by the injector, the same instance is returned. Other injectors will still create their own instances, so it's only a Singleton for the specific injector (and child injectors created from it).
-   `Scope.Transient` to altogether disable caching. You'll always get fresh instances.

## Disposing

Any injector has a `dispose` method. Calling it will call `dispose` method on any instance that was ever provided from it, as well as any child injectors that were created from it.

```ts
import { ServiceContainer } from '@kephajs/core/ServiceContainer/ServiceContainer';

class Foo {
    constructor() {
        console.log('Foo created');
    }
    dispose() {
        console.log('Foo disposed');
    }
}
const rootInjector = ServiceContainer.init();
const fooProvider = rootInjector.provideClass('foo', Foo);
fooProvider.resolve('foo'); // => "Foo created"
await rootInjector.dispose(); // => "Foo disposed"
fooProvider.resolve('foo'); // Error: Injector already disposed
```

Note: Always dispose from the top down! In this example, the `rootInjector` is disposed, which in turn disposes everything that was ever provided from one if it's child injectors.

Dispose methods are typically async. For example, you might need to clean up some files or get rid of a child process. If you do so, your dependencies should return a promise from the dispose method. In turn, calling dispose on an Injector is always async. You are responsible for the correct handling of the async behavior of the dispose method. This means you should either await the result or attach then/catch handlers.

Disposing of provided values is done in order of child first. So they are disposed in the opposite order of respective `providedXXX` calls (like a stack).

Any instance created with `injectClass` or `injectFactory` will not be disposed when `dispose` is called. You were responsible for creating it, so you are also responsible for the disposing of it. In the same vain, anything provided as a value with `providedValue` will also not be disposed when `dispose` is called on it's injector.

## Magic token

Any injector can always provide the following tokens:

| Token name       | Token value  | Description                                                                                        |
| ---------------- | ------------ | -------------------------------------------------------------------------------------------------- |
| `INJECTOR_TOKEN` | `'$injector` | Injects the current injector                                                                       |
| `TARGET_TOKEN`   | `'$target'`  | The class or function in wich the current values are injected, or `undefined` if resolved directly |

## Error handling

If an error occurs during injection or dependency resolving, an `InjectionError` is thrown. It have to special properties:

-   `cause`: the original cause of the injection error.
-   `path`: contain the path that was taken to get the error.

## Providing

You can provide a value, a class or a factory:

-   `provideValue` (or `register`) create a child injector that can provide a value.
-   `provideClass` create a child injector that can provide a value using an instance of the `Class`.
-   `provideFactory` create a child injector that can provide a value using `factory`.

## Resolving

-   `injectClass` creates a new instance of a class by populating its constructor arguments from the injector and returns it.
-   `injectFunction` injects the function with the requested tokes from the injector, invokes it and returns the result.
-   `get` resolves tokens by hand and return the result

## Configure your Service Container

With your application you need to use the `$app.container` property to register and get your services. To have the good typing, you need to declare a file `./contracts/ApplicationRegistredServices.ts` and import it in your `index.ts`:

```ts
// ./contracts/ApplicationRegisteredServices.ts
import { MyService } from '../src/Service/MyService';

declare module '@kephajs/core/ApplicationRegisteredServices' {
    interface ApplicationRegisteredServices {
        myService: MyService;
    }
}

// ./index.ts
import { Application } from '@kephajs/core/Application';
import { MyService } from './src/Service/MyService';

import './contracts/ApplicationRegisteredServices';

const app = new Application(__dirname);
app.start();
app.container.provideClass('myService', MyService);
app.container.get('myService'); // MyService
```

## Get all registered services

With the method `getAll` you can get all registered services:

```ts
import { Application } from '@kephajs/core/Application';
import { MyService } from './src/Service/MyService';

import './contracts/ApplicationRegisteredServices';

const app = new Application(__dirname);
app.start();
app.container.provideClass('myService', MyService).provideValue('aValue', 42);
app.container.getAll(); // { myService: instanceof MyService, aValue: 42 }
```

Be carefull, it's from the last registered to the first!

## Thanks

This part of our framework is inspirated by https://github.com/nicojs/typed-inject
