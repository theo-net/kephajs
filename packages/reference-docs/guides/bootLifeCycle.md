# Boot life cycle

The `Application` have a property `state` who return the current state of your application.

The value can be:

-   `unknown`
-   `initiated`
-   `setup`
-   `registered`
-   `booted`
-   `ready`
-   `shutdown`

Each state have a protected method `_state*` who execute the logic. These methods need to return a `Promise`.

```
    unknown
      ↓
        _stateUnkown()
      ↓
    initiated
      ↓
        _stateInitiated()
      ↓
    setup
      ↓
        _stateSetup()
      ↓
    registered
      ↓
        _stateRegistered()
      ↓
    booted
      ↓
        _stateBooted()
      ↓
    ready
      ↓
    shutdown
```

When you extend the core application and want to use one of these methods, you need to call inside `super._state*`.

All of these methods need to return a `Promise` except `_stateUnknow`.

## Unknown

It's the first state when you execute your application. It's the begin boot process.

## Initiated

Load and validate the env variables. Load also the config. Configure the logger and the profiler and initialize the service container.

## Setup

Register the plugins.

## Registered

Boot the plugins.

## Booted

Execute the plugin `ready` method.

## Ready

Execute the main code: the method `run` of your application. This method need to return a Promise.

## Shutdown

Wait until the resolution of your `run` method.

When the `shutdown` method is called, each `shutdown` method of yours plugins are also called and also all shutdown function that you have previously registered.

If one of them return a `Promise`, the framework wait for the resolution of it.

After the state will be `shutdown`.
