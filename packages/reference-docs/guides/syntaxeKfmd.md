# Syntaxe Kepha Flavored Markdown

Markdown a été conçu pour être aussi facile à lire et à écrire que possible.
Ainsi, un texte rédigé avec cette syntaxe reste lisible et n'est pas polué
par trop d'éléments de styles.

_Cf. https://michelf.ca/projets/php-markdown/syntaxe/ pour la syntaxe
originale._

## HTML intercalé

L'objectif de cette syntaxe est d'être utilisée comme format d'écriture sur
le web. Elle s'occupe donc uniquement de ce qui concerne le texte, ainsi le
reste peut être directement formaté et inséré avec des balises HTML. Il y a
néanmoins quelques restrictions :

-   les éléments représentants des blocs doivent être séparés du contenu
    environnant par des lignes vides. sinon, ils seront intégrés dans un
    paragraphe (ce qui peut poser problème).
-   seules les balises suivantes seront autorisées : `a em strong small s cite q dfn abbr data time code var samp kbd sub sup i b u mark ruby rt rp bdi bdo span br wbr ins del img h1 h2 h3 h4 h5 h6 p strong em a span ol ul li table thead tbody tr th td figure caption figcaption div blockquote footer iframe dl dt dd`
-   à l'intérieur de ces balises, la syntaxe ne sera pas interprétée, on ne
    pourra donc pas, par exemple, insérer de l'emphase à l'aide de `*`

Les caractères : `&`, `<` et `>` sont automatiquement échapés par leurs
équivalents (`&amp;` `&lt;` et `&gt;`) sauf s'ils servent pour les balises
HTML ou pour débuter une entité HTML (on pourra bien écrire `&copy;`).

## Éléments de bloc

### Paragraphes et sauts de ligne

Un paragraphe est une ou plusieurs lignes de texte, séparées par une ou
plusieurs lignes vides (une ligne contenant uniquement des espaces ou des
tabulations est considérée comme vide).

Pour forcer un retour à la ligne au sein d'un paragraphe, il faut terminer la
ligne par deux espaces (ou plus) avant le retour à la ligne.

### Titres

Pour insérer des titres, il suffit de débuter la ligne avec des `#` selon le
niveau. Six sont possibles (entre `h1` et `h6`). On peut facultivement
terminer le titre par un ou plusieurs `#` (le nombre importe peu).

    # h1
    ## h2
    ### h3
    #### h4
    ##### h5
    ###### h6

Il existe deux alternatives pour `h1` et `h2` :

    Alt-h1
    ======

    Alt-h2
    ------

N'importe quelle longueur de soulignement avec des `=` ou des `-`, du moment
qu'elle soit supérieure ou égale à 2 fonctionne.

### Blocs de citation

Pour délimiter les blocs de citation, on débute les lignes avec des `>`. On
peut imbriquer plusieurs blocs, ceux-ci peuvent ne contenir un `>` qu'à la
première ligne et contenir plusieurs paragraphes. Enfin, la syntaxe
fonctionne à l'intérieur.

    > Ceci est un paragraphe cité. Ce paragraphe peut contenir plusieurs
    > lignes _sans_ problème.
    >
    > > Et même un bloc imbriqué !

    > Un autre texte cité qui est un vrai paragraphe
    sur plusieurs lignes. Ici, seule la première
    possède le caractère `>`

### Listes

On peut créer des listes ordonnées (numérotées) et non-ordonnées (à puces).

Les listes non-ordonnées utilisent `*`, `+`, ou `-` de façon tout à fait
interchangeable :

    *   Rouge
    *   Vert
    *   Bleu

est équivalent à :

    +   Rouge
    +   Vert
    +   Bleu

et :

    -   Rouge
    -   Vert
    -   Bleu

Les listes ordonnées utilisent un nombre suivit d’un point :

    1.  Bird
    2.  McHale
    3.  Parish

Il est important de noter que l’ordre des nombres utilisés n'a aucune
incidence : la numérotation débutera toujours par `1` et sera dans l'ordre
d'apparition des éléments.

Les marqueurs de liste sont normalement alignés sur la marge de gauche, mais
ils peuvent être indentés de la marge par trois espaces ou moins. Les
marqueurs de liste doivent être suivis par au moins un espace, ou par une
tabulation.

Pour écrire une liste qui se lit bien, on peut ajouter une indentation après
chaque saut de ligne manuel (ce n'est pas obligatoire) :

     *   Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
         Aliquam hendrerit mi posuere lectus. Vestibulum enim wisi,
         viverra nec, fringilla in, laoreet vitae, risus.
     *   Donec sit amet nisl. Aliquam semper ipsum sit amet velit.
         Suspendisse id sem consectetuer libero luctus adipiscing.

Les listes peuvent être imbriquées en augmentant l'indentation des sous éléments :

    - a
      - b
      - c
    - d

Il vaut la peine de noter qu’il est possible de déclencher une liste
ordonnée par accident, en écrivant quelque chose comme ça :

    2017. Première publication de KephaJS

Car on est en face d'une séquence nombre-point-espace au début d’une ligne.
Pour éviter cette situation, vous pouvez échapper le point à l’aide d’une
barre oblique inverse :

    2017\. Première publication de KephaJS

Enfin, on peut créer des listes de tâches en débutant les lignes par la
séquence `- []` ou `- [x]`. Cela affichera une checkbox dont on ne pourra
pas changer l'état. Mais il ne doit y avoir qu'un seul espace entre `-` et
`[`.

-   [] Non fini
-   [x] fini

### Blocs de code

Pour écrire du code, on a plusieurs solutions. Dans tous les cas, le code
ainsi écrit ne sera pas interprété et tous les `&`, `<` et `>` seront
convertis en entités HTML.

Pour écrire du code inline :

    Dans un texte j'insère `du code` inline

Pour des blocs de code, deux possibilités : l'entourer par trois \` ou le
faire précéder d'une indentation de 4 espaces.

    ```
    function test() {
      console.log("notice the blank line before this function?");
    }
    ```

        code car précédé de 4 espaces

### Tableaux

Pour tracer un tableau, il y a deux possibilités : utiliser les balises
HTML ou utiliser une syntaxe simplifiée :

    | First Header | Second Header |
    |--------------|---------------|
    | Content Cell | Content Cell  |
    | Content Cell | Content Cell  |

Le séparateur d'header doit faire au moins 1 `-`, on est pas obligé d'aligner
les colonnes. En revanche, il doit obligatoirement avoir un header dans le
tableau pour que la syntaxe fonctionne.

    | Command | Description |
    | --- | --- |
    | git status | List all new or modified files |
    | git diff | Show file differences that haven't been staged |

Bien-sûr, la syntaxe markdown est interprétée à l'intérieur.

    | Command | Description |
    | --- | --- |
    | `git status` | List all *new or modified* files |
    | `git diff` | Show file differences that **haven't been** staged |

Le séparateur d'header peut permettre de spécifier l'alignement des colones.

    | Left-aligned | Center-aligned | Right-aligned |
    | :---         |     :---:      |          ---: |
    | git status   | git status     | git status    |
    | git diff     | git diff       | git diff      |

<!-- block -->

## Élément de texte

### Liens

Il y a deux moyens de créer un lien avec notre syntaxe. La première est de
tout simplement d'en écrire dans le document : s'il débute par `http://` ou
`https://` ou par `www.`, il sera converti directement en lien cliquable. Idem
pour les adresses mails et en fait la majorité des choses ressemblant à un
lien.

Le second moyen est de l'incorporer à l'aide de la syntaxe suivante :

    [Texte du lien](url_du_lien "texte pour le titre, facultatif")

### Emphase

Pour indiquer l'emphase, on utilise `*` ou bien `_` : si le texte est placé
entre deux `*` ou `_` il sera mis en emphase faible (en italique), s'il est
entre des doubles `*` ou `_` il sera mis en emphase forte (en gras). De la
même manière, on peut utiliser `~` pour barrer un texte, mais dans ce cas il
devra être mis en double.

    **texte gras** ou __texte gras__
    *italique* ou _italique_
    ~~Barré~~
    **Les _choses_ peuvent être combinées**
    L'emphase peut être au mi*li*eu d'un mot

Si ``_` et `~~` sont placés entre deux espaces, ils seront littéralement
traités comme un underscore ou deux tildes.

### Notes de bas de page

Pour insérer une note de bas de page, il suffit de mettre la note entre double
parenthèses à l'emplacement de la note.

    Un texte((avec une _note_ de bas de page.)) que j'écrit.

Sera rendu :

    Un texte<a href="#note-1"><sup>1</sup></a> que j'écrit.

    <a id="note-1">1</a> avec une <em>note</em> de bas de page.

<!-- inline -->

## Divers

### Échappement par barre oblique inverse

Pour insérer des caractères spéciaux, ceux que notre syntaxe utilise, il faut
les faire précéder par une barre oblique inverse (`\`).

Voici la liste de ces caractères (certains n'ont pas besoins d'être
systématiquement échappés car leur position ne carespont pas à la syntaxe) :

-   `\\`
-   `\``
-   `*`
-   `_`
-   `(` ou `)`
-   `[` ou `]`
-   `#`
-   `+`
-   `-`
-   `.`

### Typographie

-   Les `'` sont remplacés par un vrai apostrophe.

<!-- divers -->
