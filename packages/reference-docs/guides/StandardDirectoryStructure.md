# Standard Directory Structure

When you want to create a new application with KephaJs, you need to use this directory structure:

```
 ├─ assets/
 ├─ bin/
 │   └─ console
 ├─ config/
 ├─ contracts/
 ├─ migrations/
 ├─ public/
 │   └─ index
 ├─ src/
 │   ├─ DataFixtures/
 │   ├─ Entity/
 │   ├─ EventListener/
 │   ├─ EventSubscriber/
 │   ├─ Form/
 │   ├─ Command/
 │   ├─ Controller/
 │   ├─ Mailer/
 │   ├─ Service/
 │   ├─ Repository/
 │   └─ Validator/
 ├─ templates/
 ├─ tests/
 ├─ translations/
 └─var/
    ├─ cache/
    └─ log/
```

The exact list depends on your needs and the target of your application (browser, console, web server, ...)
