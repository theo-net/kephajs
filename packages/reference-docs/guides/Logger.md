# Logger

KephaJs give a logger service.

To print messages, you have different methods:

| Method      | `console` method  |
| ----------- | ----------------- |
| `error()`   | `console.error()` |
| `warning()` | `console.warn()`  |
| `notice()`  | `console.info()`  |
| `info()`    | `console.info()`  |
| `debug()`   | `console.log()`   |

Each printed messages are prefixed by:

-   `error`: "ERROR:"
-   `warning`: "WARNING:"
-   `notice`: "NOTICE:"
-   `info`: "INFO:"
-   `debug`: "DEBUG"

## Save for latter

You can save the log info to print it later. For this, you can use the method `setSaveToLater` and the methods `getSavedLogs` to get the list or `printSavedLogs` to print it:

```ts
const logger = new Logger();
logger.setSaveToLater(true);
logger.error('An error');
logger.notice('An notice');
logger.getSavedLogs();
/*
[
    { type: Logger.ERROR; message: 'An error', datetime: new Datetime() },
    { type: Logger.NOTICE; message: 'An notice', datetime: new Datetime() }
]
*/
logger.printSavedLogs();
/*
ERROR: An error
NOTICE: An notice
*/
logger.getSavedLogs();
/*
{}
*/
```

After printing, the saved log list is empty.

You can also purge the list with the method `resetSavedLogs`.

You can also give saved logs with the method `addSavedLogs`:

```ts
const logger = new Logger();
logger.setSaveToLater(true);
logger.error('An error');
logger.notice('An notice');
const logger2 = logger();
logger2.addSavedLogs(logger.getSavedLogs());
logger2.printSavedLogs();
/*
ERROR: An error
NOTICE: An notice
*/
```

You have also the method `isSaveToLater` to know if the logs are saved or not.

## Do not print messages

To decide wich message are printing, the logger use the environment type:

| Type      | Environment                         |
| --------- | ----------------------------------- |
| `ERROR`   | all                                 |
| `WARNING` | all                                 |
| `NOTICE`  | `development`, `testing`, `staging` |
| `INFO`    | `development`, `testing`            |
| `DEBUG`   | `development`, `testing`            |

## Using from console package

The framework use a new logger what:

-   get the saved log and print it (the log are saved before the initialization of the new logger)
-   change the prefix
-   change the rules of printing messages

The new prefix are the same, but they are formated

The printing rules are:

| Type      | Verbosity                     |
| --------- | ----------------------------- |
| `ERROR`   | Output.VERBOSITY.QUIET        |
| `WARNING` | Output.VERBOSITY.NORMAL       |
| `NOTICE`  | Output.VERBOSITY.VERBOSE      |
| `INFO`    | Output.VERBOSITY.VERY_VERBOSE |
| `DEBUG`   | Output.VERBOSITY.DEBUG        |

The messages are printing with the `$output` service, but the errors are printing with the `$outputErr` service.

The log before the initialization of the console services are keeped and printed when the console logger service are loaded.
