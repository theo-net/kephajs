# Config Service

KephaJS provides a service to manage the configuration of your application. As you can inject it in your class and get the differents configurations.

## Basic usage

```ts
$config.add({
    param1: 'bar',
    param2: 'foo%param1%',
});
$config.get('param2'); // 'foobar'
```

### Register a parameter

To register an element, use the `set` method. By default, you cannot replace an element with the same id.

```ts
$config.set('myParam', 42);
$config.set('myParam', 43); // throw TypeError
```

If you want replace the value, use the third parameter:

```ts
$config.set('myParam', 42);
$config.set('myParam', 43, true);
$config.get('myParam'); // 42
```

If you want, you can define a parameter as readonly:

```ts
$config.setReadonly('const', 'foo');
$config.set('const', 'bar'); // throw TypeError
$config.set('const', 'bar', true); // throw TypeError
$config.setReadonly('const', 'bar'); // throw TypeError
```

### Check if a parameter exists

The method `has` return a `boolean`:

```ts
$config.set('myParam', 42);
$config.has('myParam'); // true
$config.has('myOtherParam'); // false
```

### Add a series of parameters

```ts
$config.add({
    Foo: 'foo',
    Bar: 'bar',
});
$config.get('Foo'); // 'foo'
$config.get('Bar'); // 'bar'
```

### Get the list of the reaonly properties

```ts
$config.set('foo', 1);
$config.setReadonly('bar', 2);
$config.setReadonly('foobar', 3);
$config.getReadonly(); // ['bar', 'foobar']
```

### Delete an element

You can delete an element:

```ts
$config.set('foobar', 42);
$config.delete('foobar');
$config.has('foobar'); // false
```

But you cannot delete a readonly property:

```ts
$config.setReadonly('param', 'hello');
$config.delete('param'); // throw TypeError
$config.has('param'); // true
```

### Reset the config

You can reset the config with the method `reset`.

## Advanced features

### Default value

When you want to get the value of a parameter, you can specify a default value. As if the parameter isn't registered, the method returns the default value:

```ts
$config.has('param'); // false
$config.get('param', 42); // 42
```

If the default value is a function, the Config Service returns the return of this function:

```ts
function dflt() {
    return 'Hello World!';
}
$config.get('param', dflt); // 'Hello World!'
```

If the parameter don't exist and no default value provided, the method returns `undefined`.

### Return a serie of values

You can register the values as a tree and return it after:

```ts
$config.add({
    'root': 0,
    'foo.bar': 1,
    'foo.test.a': 2,
    'foo.test.b': 3;
});
$config.getSerie('foo.*'); // {
                           //    'bar': 1,
                           //    'test.a': 2,
                           //    'test.b': 3,
                           // }
$config.getSerie('foo.test.*'); // {
                                //    'a': 2,
                                //    'b': 3,
                                // }

$config.getSerie('foo'); // throw TypeError
                         // because 'foo' is not a pattern to match a serie
```

### Caching

For the existing keys, KephaJs take in cache the value when you get it for the first time.

```ts
$config.set('foo', 'bar');
$config.set('foo', 'foo', true);
$config.get('foo'); // 'foo'

$config.set('foo', 'foobar', true);
$config.get('foo'); // 'foo'
$config.refresh('foo');
$config.get('foo'); // 'foobar'
```

You can also refresh all cache, when you call the method `refresh` whitout parameter. You can also refresh a serie of parameters with an array:

```ts
$config.refresh(); // refresh everything
$config.refresh(['a', 'b']); // refresh parameter 'a' and 'b'
```

It's work for simple value, but also when you want to get a serie.

```ts
$config.set('foo.a', 'bar');
$config.set('foo.b', 'foo');
$config.get('foo.*'); // { a: 'bar', b: 'foo' }

$config.set('foo.a', 'foobar', true);
$config.get('foo.*'); // { a: 'bar', b: 'foo' }
$config.refresh('foo.*');
$config.get('foo.*'); // { a: 'bar', b: 'foo' }
$config.refresh('foo.a', 'foo.*');
$config.get('foo.*'); // { a: 'foobar', b: 'foo' }
```

As, like you can see, it's hard to work with the cache! Is because, normally, the parameters don't need to change during the life of your application.

### Use parameter in other values

You can use a parameter in an other value:

```ts
$config.set('Foo', 'foo');
$config.set('Bar', 'bar');
$config.set('Foobar', 'Hello %Foo%%Bar%!');
$config.get('Foobar'); // 'Hello foobar!'
$config.get('Foobar', undefined, false); // 'Hello %Foo%%Bar%!' (disable include)
```

The result is cached, as if you update a value, you need to refresh the cache:

```ts
$config.set('Foo', 'foo');
$config.set('Bar', 'bar');
$config.set('Foobar', 'Hello %Foo%%Bar%!');
$config.get('Foobar'); // 'Hello foobar!'

// update a value
$config.set('Foo', 'the ', true);
$config.get('Foobar'); // 'Hello foobar!'
$config.refresh('Foobar');
$config.get('Foobar'); // 'Hello the bar!'

// refresh all cache
$config.refresh();
```

If a circular reference is detected, the service throw an `CircularRefereneError`.

## Validation and type

To help you, you can add type information for your editor and the type checking.

```ts
// ./contracts/ConfigType.ts
declare module '@kephajs/core/Config/ConfigType' {
    interface ConfigType {
        foobar: number;
    }
}

// ./myFile.ts
import './contracts/ConfigType';

// when you type `get(` you will get choice between 'environment', 'version', 'foobar', ...
const foobar = $config.get<number>('foobar'); // number
```

## Add a Json

You can add a JSON as serie of parameters : it will be flattened.

```ts
$config.addJson(
    JSON.parse(`{
        "myKey": "foobar",
        "myOtherKey": {
            "foo": 4,
            "bar": 2,
            "table": ['a', 'b', 'c'],
        },
    }`)
);

$config.getAll();
// {
//     myKey: 'foobar',
//     'myOtherKey.foo': 4,
//     'myOtherKey.bar': 2,
//     'myOtherKey.table': ['a', 'b', 'c'],
// }
```

You have a `prefix` parameter to prefix the keys:

```ts
$config.addJson(
    JSON.parse(`{
        "myKey": "foobar",
        "myOtherKey": {
            "foo": 4,
            "bar": 2,
            "table": ['a', 'b', 'c'],
        },
    }`),
    'test'
);

$config.getAll();
// {
//     'test.myKey': 'foobar',
//     'test.myOtherKey.foo': 4,
//     'test.myOtherKey.bar': 2,
//     'test.myOtherKey.table': ['a', 'b', 'c'],
// }
```

As the `set` method, you have a `replace: boolean` parameter.

## TODO :

-   validation
