/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Application } from '../Application';
import {
    InjectableClass,
    InjectableFunction,
    InjectionToken,
    Injector,
} from '../Validate/Injector/AbstractInjector';
import { InjectionError } from '../Validate/Injector/Error';
import { RootInjector } from '../Validate/Injector/RootInjector';
import { Scope } from '../Validate/Injector/Scope';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type GenericContext = { [key: string]: any };

/**
 * It's a wrapper to the dependency injection system
 */
export class ServiceContainer {
    private _injector: Injector<GenericContext>;

    /**
     * Returns a new empty injector or an injector with `$app` provided with `app` parameter
     */
    constructor(app?: Application) {
        // eslint-disable-next-line @typescript-eslint/ban-types
        const rootInjector: Injector<{}> = new RootInjector();

        if (app instanceof Application) {
            this._injector = rootInjector.provideValue('$app', app);
        } else {
            this._injector = rootInjector;
        }

        this.register('$container', this);
    }

    getInjector() {
        return this._injector;
    }

    /**
     * Return a service
     */
    get(token: string) {
        if (this._injector instanceof RootInjector) {
            throw new InjectionError(
                [token],
                new Error('No services registered')
            );
        }
        return this._injector.get(token);
    }

    /**
     * Return the services lists
     *
     * Be carefull, it's from the last registered to the first!
     */
    getAll() {
        return this._injector.getProviders();
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    register(token: string, value: any) {
        this._injector = this._injector.register(token, value);

        return this;
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    provideValue(token: string, value: any) {
        this._injector = this._injector.provideValue(token, value);

        return this;
    }

    provideClass<R>(
        token: string,
        Class: InjectableClass<
            GenericContext,
            R,
            InjectionToken<GenericContext>[]
        >,
        scope: Scope = Scope.Singleton
    ) {
        this._injector = this._injector.provideClass(token, Class, scope);

        return this;
    }

    provideFactory<R>(
        token: string,
        factory: InjectableFunction<
            GenericContext,
            R,
            InjectionToken<GenericContext>[]
        >,
        scope: Scope = Scope.Singleton
    ) {
        this._injector = this._injector.provideFactory(token, factory, scope);

        return this;
    }
}
