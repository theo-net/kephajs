/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/* eslint-disable @typescript-eslint/no-explicit-any */

import { expect } from 'chai';

import { Application } from '../Application';
import { ValueProvider } from '../Validate/Injector/ValueProvider';
import { ServiceContainer } from './ServiceContainer';

describe('@kephajs/core/ServicesContainer/ServiceContainer', () => {
    describe('constructor', () => {
        it('Should return a new Injector with the container as service', () => {
            const injector = new ServiceContainer().getInjector();
            expect(injector).to.be.instanceOf(ValueProvider);
            expect(injector.get('$container')).to.be.instanceOf(
                ServiceContainer
            );
        });

        it('Should provide $app as the Application instance', () => {
            const app = new Application('test');
            expect(new ServiceContainer(app).get('$app')).to.be.equal(app);
        });

        it('Should register $container service', () => {
            const injector = new ServiceContainer();
            expect(injector.get('$container')).to.be.equal(injector);
        });
    });

    describe('getAll', () => {
        it('Should return the services list', () => {
            const value1 = { a: 1 };
            const value2 = { b: 2 };
            const injector = new ServiceContainer()
                .register('test1', value1)
                .provideValue('test2', value2);
            expect(
                Object.getOwnPropertyNames(injector.getAll())
            ).to.be.deep.equal(['test2', 'test1', '$container']);
            expect(injector.getAll().test1).to.be.deep.equal(value1);
            expect(injector.getAll().test2).to.be.deep.equal(value2);
        });
    });

    describe('register & provideValue', () => {
        it('Should register a value', () => {
            const value = {};
            const injector = new ServiceContainer()
                .register('test', value)
                .provideValue('test2', value);
            expect(injector.get('test')).to.be.equal(value);
            expect(injector.get('test2')).to.be.equal(value);
        });
    });

    describe('provideClass', () => {
        it('Should register a class', () => {
            class MyService {}
            const injector = new ServiceContainer().provideClass(
                'test',
                MyService
            );
            expect(injector.get('test')).to.be.instanceOf(MyService);
        });
    });

    describe('provideFactory', () => {
        it('Should register a factory', () => {
            function myFactory() {
                return 'foobar';
            }
            const injector = new ServiceContainer().provideFactory(
                'test',
                myFactory
            );
            expect(injector.get('test')).to.be.equal('foobar');
        });
    });
});
