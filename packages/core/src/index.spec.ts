/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import * as CoreIndex from './index';
import * as Helpers from './Helpers/Helpers';

describe('@kephajs/core', () => {
    it('Should export Helpers', () => {
        expect(CoreIndex.Helpers).to.be.equal(Helpers);
    });
});
