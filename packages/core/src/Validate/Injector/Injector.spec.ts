/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import sinon from 'sinon';

import { Injector, INJECTOR_TOKEN, TARGET_TOKEN } from './AbstractInjector';
import { Disposable } from './Disposable';
import { InjectionError, InjectorDisposedError } from './Error';
import { RootInjector } from './RootInjector';
import { Scope } from './Scope';

describe('@kephajs/core/Validate/Injector/Injector', () => {
    // eslint-disable-next-line @typescript-eslint/ban-types
    let rootInjector: Injector<{}>;

    beforeEach(() => {
        rootInjector = new RootInjector();
    });

    describe('AbstractInjector', () => {
        it('Should be able to inject injector and target in a class', () => {
            class Injectable {
                constructor(
                    // eslint-disable-next-line @typescript-eslint/ban-types
                    public readonly target: Function | undefined,
                    // eslint-disable-next-line @typescript-eslint/ban-types
                    public readonly injector: Injector<{}>
                ) {}

                static $inject = [TARGET_TOKEN, INJECTOR_TOKEN] as const;
            }

            const actual = rootInjector.injectClass(Injectable);

            expect(actual.target).to.be.equal(undefined);
            expect(actual.injector).to.be.equal(rootInjector);
        });

        it('Should be able to inject injector and target in a function', () => {
            // eslint-disable-next-line @typescript-eslint/ban-types
            let actualTarget: Function | undefined;
            // eslint-disable-next-line @typescript-eslint/ban-types
            let actualInjector: Injector<{}> | undefined;
            const expectedResult = { result: 42 };
            // eslint-disable-next-line @typescript-eslint/ban-types
            function injectable(target: Function | undefined, i: Injector<{}>) {
                actualTarget = target;
                actualInjector = i;
                return expectedResult;
            }
            injectable.$inject = [TARGET_TOKEN, INJECTOR_TOKEN] as const;

            const actualResult: { result: number } =
                rootInjector.injectFunction(injectable);

            expect(actualTarget).to.be.equal(undefined);
            expect(actualInjector).to.be.equal(rootInjector);
            expect(actualResult).to.be.deep.equal(expectedResult);
        });

        it('Should be able to provide a target into a function', () => {
            // eslint-disable-next-line @typescript-eslint/ban-types
            function fooFactory(target: undefined | Function) {
                return `foo -> ${target && target.name}`;
            }
            fooFactory.$inject = [TARGET_TOKEN] as const;
            // eslint-disable-next-line @typescript-eslint/ban-types
            function barFactory(target: undefined | Function, fooName: string) {
                return `${fooName} -> bar -> ${target && target.name}`;
            }
            barFactory.$inject = [TARGET_TOKEN, 'fooName'] as const;
            class Foo {
                constructor(public name: string) {}

                static $inject = ['name'] as const;
            }

            const actualFoo = rootInjector
                .provideFactory('fooName', fooFactory)
                .provideFactory('name', barFactory)
                .injectClass(Foo);

            expect(actualFoo.name).to.be.equal(
                'foo -> barFactory -> bar -> Foo'
            );
        });

        it('Should be able to provide a target into a class', () => {
            class Foo {
                // eslint-disable-next-line @typescript-eslint/ban-types
                constructor(public target: undefined | Function) {}

                static $inject = [TARGET_TOKEN] as const;
            }
            class Bar {
                constructor(
                    // eslint-disable-next-line @typescript-eslint/ban-types
                    public target: undefined | Function,
                    public foo: Foo
                ) {}

                static $inject = [TARGET_TOKEN, 'foo'] as const;
            }

            class Baz {
                constructor(
                    public bar: Bar,
                    // eslint-disable-next-line @typescript-eslint/ban-types
                    public target: Function | undefined
                ) {}

                static $inject = ['bar', TARGET_TOKEN] as const;
            }

            const actualBaz = rootInjector
                .provideClass('foo', Foo)
                .provideClass('bar', Bar)
                .injectClass(Baz);

            expect(actualBaz.target).to.be.equal(undefined);
            expect(actualBaz.bar.target).to.be.equal(Baz);
            expect(actualBaz.bar.foo.target).to.be.equal(Bar);
        });

        it('Should throw when no provider was found for a class', () => {
            class FooInjectable {
                constructor(public foo: string) {}

                static $inject = ['foo'] as const;
            }
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            expect(() => rootInjector.injectClass(FooInjectable as any)).throws(
                InjectionError,
                'Could not inject [class FooInjectable]. Cause: No provider found for "foo"!'
            );
        });

        it('Should throw when no provider was found for a function', () => {
            function foo(bar: string) {
                return bar;
            }
            foo.$inject = ['bar'] as const;
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            expect(() => rootInjector.injectFunction(foo as any)).throws(
                InjectionError,
                'Could not inject [function foo]. Cause: No provider found for "bar"!'
            );
        });

        it('Should be able to provide an Injector for a partial context', () => {
            class Foo {
                constructor(public injector: Injector<{ bar: number }>) {}

                static $inject = [INJECTOR_TOKEN] as const;
            }
            const barBazInjector = rootInjector
                .provideValue('bar', 42)
                .provideValue('baz', 'qux');
            const actualFoo = barBazInjector.injectClass(Foo);
            expect(actualFoo.injector).to.be.equal(barBazInjector);
        });
    });

    describe('ChildInjector', () => {
        it('Should cache the value if scope = Singleton', () => {
            let n = 0;
            function count() {
                return n++;
            }
            count.$inject = [] as const;
            const countInjector = rootInjector.provideFactory('count', count);
            class Injectable {
                // eslint-disable-next-line @typescript-eslint/no-shadow
                constructor(public count: number) {}

                static $inject = ['count'] as const;
            }

            const first = countInjector.injectClass(Injectable);
            const second = countInjector.injectClass(Injectable);

            expect(first.count).to.be.equal(second.count);
        });

        it('Should _not_ cache the value if scope = Transient', () => {
            let n = 0;
            function count() {
                return n++;
            }
            count.$inject = [] as const;
            const countInjector = rootInjector.provideFactory(
                'count',
                count,
                Scope.Transient
            );
            class Injectable {
                // eslint-disable-next-line @typescript-eslint/no-shadow
                constructor(public count: number) {}

                static $inject = ['count'] as const;
            }

            const first = countInjector.injectClass(Injectable);
            const second = countInjector.injectClass(Injectable);

            expect(first.count).to.be.equal(0);
            expect(second.count).to.be.equal(1);
        });
    });

    describe('ValueProvider', () => {
        it('Should be able to provide a value', () => {
            const sut = rootInjector.provideValue('foo', 42);
            const actual = sut.injectClass(
                class {
                    constructor(public foo: number) {}

                    static $inject = ['foo'] as const;
                }
            );
            expect(actual.foo).to.be.equal(42);
        });

        it('Should be able to provide a value from the parent injector', () => {
            const sut = rootInjector
                .provideValue('foo', 42)
                .provideValue('bar', 'baz');
            expect(sut.get('bar')).to.be.equal('baz');
            expect(sut.get('foo')).to.be.equal(42);
        });

        it('Should throw after disposed', async () => {
            const sut = rootInjector.provideValue('foo', 42);
            await sut.dispose();
            expect(() => sut.get('foo'))
                .throws(InjectorDisposedError)
                .which.includes({
                    message:
                        'Injector is already disposed. Please don\'t use it anymore. Tried to resolve [token "foo"].',
                });
            expect(() => sut.injectClass(class Bar {}))
                .throws(InjectorDisposedError)
                .which.includes({
                    message:
                        "Injector is already disposed. Please don't use it anymore. Tried to inject [class Bar].",
                });
            expect(() => sut.injectFunction(function baz() {}))
                .throws(InjectorDisposedError)
                .which.includes({
                    message:
                        "Injector is already disposed. Please don't use it anymore. Tried to inject [function baz].",
                });
        });

        it('Should be able to change the type of a token', () => {
            const answerProvider = rootInjector
                .provideValue('answer', 42)
                .provideValue('answer', '42');
            expect(answerProvider.get('answer')).to.be.equal('42');
            expect(typeof answerProvider.get('answer')).to.be.equal('string');
        });
    });

    describe('FactoryProvider', () => {
        it('Should be able to provide the return value of the factoryMethod', () => {
            const expectedValue = { foo: 'bar' };
            function foobar() {
                return expectedValue;
            }

            const actual = rootInjector
                .provideFactory('foobar', foobar)
                .injectClass(
                    class {
                        // eslint-disable-next-line @typescript-eslint/no-shadow
                        constructor(public foobar: { foo: string }) {}

                        static $inject = ['foobar'] as const;
                    }
                );
            expect(actual.foobar).to.be.equal(expectedValue);
        });

        it('Should be able to provide parent injector values', () => {
            function answer() {
                return 42;
            }
            const factoryProvider = rootInjector.provideFactory(
                'answer',
                answer
            );
            const actual = factoryProvider.injectClass(
                class {
                    constructor(
                        public injector: Injector<{ answer: number }>,
                        // eslint-disable-next-line @typescript-eslint/no-shadow
                        public answer: number
                    ) {}

                    static $inject = [INJECTOR_TOKEN, 'answer'] as const;
                }
            );
            expect(actual.injector).to.be.equal(factoryProvider);
            expect(actual.answer).to.be.equal(42);
        });

        it('Should throw after disposed', async () => {
            const sut = rootInjector.provideFactory(
                'answer',
                function answer() {
                    return 42;
                }
            );
            await sut.dispose();
            expect(() => sut.get('answer')).throws(
                'Injector is already disposed. Please don\'t use it anymore. Tried to resolve [token "answer"].'
            );
            expect(() => sut.injectClass(class Bar {})).throws(
                "Injector is already disposed. Please don't use it anymore. Tried to inject [class Bar]."
            );
            expect(() => sut.injectFunction(function baz() {})).throws(
                "Injector is already disposed. Please don't use it anymore. Tried to inject [function baz]."
            );
        });

        it('Should be able to decorate an existing token', () => {
            function incrementDecorator(n: number) {
                return ++n;
            }
            incrementDecorator.$inject = ['answer'] as const;

            const answerProvider = rootInjector
                .provideValue('answer', 40)
                .provideFactory('answer', incrementDecorator)
                .provideFactory('answer', incrementDecorator);

            expect(answerProvider.get('answer')).to.be.equal(42);
            expect(answerProvider.get('answer')).to.be.equal(42);
        });
    });

    describe('ClassProvider', () => {
        it('Should throw after disposed', async () => {
            const sut = rootInjector.provideClass('foo', class Foo {});
            await sut.dispose();
            expect(() => sut.get('foo')).throws(
                'Injector is already disposed. Please don\'t use it anymore. Tried to resolve [token "foo"].'
            );
            expect(() => sut.injectClass(class Bar {})).throws(
                "Injector is already disposed. Please don't use it anymore. Tried to inject [class Bar]."
            );
            expect(() => sut.injectFunction(function baz() {})).throws(
                "Injector is already disposed. Please don't use it anymore. Tried to inject [function baz]."
            );
        });

        it('Should be able to decorate an existing token', () => {
            class Foo {
                static $inject = ['answer'] as const;

                constructor(innerFoo: { answer: number }) {
                    this.answer = innerFoo.answer + 1;
                }

                answer: number;
            }

            const answerProvider = rootInjector
                .provideValue('answer', { answer: 40 })
                .provideClass('answer', Foo)
                .provideClass('answer', Foo);

            expect(answerProvider.get('answer').answer).to.be.equal(42);
        });
    });

    describe('dispose', () => {
        it('Should dispose all disposable singleton dependencies', async () => {
            class Foo {
                dispose2 = sinon.stub();

                dispose = sinon.stub();
            }
            function barFactory(): Disposable & { dispose3(): void } {
                return { dispose: sinon.stub(), dispose3: sinon.stub() };
            }
            class Baz {
                constructor(
                    public readonly bar: Disposable & { dispose3(): void },
                    public readonly foo: Foo
                ) {}

                static $inject = ['bar', 'foo'] as const;
            }
            const baz = rootInjector
                .provideClass('foo', Foo)
                .provideFactory('bar', barFactory)
                .injectClass(Baz);

            await rootInjector.dispose();

            expect((baz.bar.dispose as sinon.SinonStub).called).to.be.equal(
                true
            );
            expect((baz.foo.dispose as sinon.SinonStub).called).to.be.equal(
                true
            );
            expect((baz.foo.dispose2 as sinon.SinonStub).called).to.be.equal(
                false
            );
            expect((baz.bar.dispose3 as sinon.SinonStub).called).to.be.equal(
                false
            );
        });

        it('Should also dispose transient dependencies', async () => {
            class Foo {
                dispose = sinon.stub();
            }
            function barFactory(): Disposable {
                return { dispose: sinon.stub() };
            }
            class Baz {
                constructor(
                    public readonly bar: Disposable,
                    public readonly foo: Foo
                ) {}

                static $inject = ['bar', 'foo'] as const;
            }
            const baz = rootInjector
                .provideClass('foo', Foo, Scope.Transient)
                .provideFactory('bar', barFactory, Scope.Transient)
                .injectClass(Baz);

            await rootInjector.dispose();

            expect((baz.bar.dispose as sinon.SinonStub).called).to.be.equal(
                true
            );
            expect((baz.foo.dispose as sinon.SinonStub).called).to.be.equal(
                true
            );
        });

        it('Should dispose dependencies in correct order (child first)', async () => {
            class Grandparent {
                dispose = sinon.stub();
            }
            class Parent {
                dispose = sinon.stub();
            }
            class Child {
                constructor(
                    public readonly parent: Parent,
                    public readonly grandparent: Grandparent
                ) {}

                static $inject = ['parent', 'grandparent'] as const;

                dispose = sinon.stub();
            }
            const bazProvider = rootInjector
                .provideClass('grandparent', Grandparent, Scope.Transient)
                .provideClass('parent', Parent)
                .provideClass('child', Child);
            const child = bazProvider.get('child');
            const newGrandparent = bazProvider.get('grandparent');

            await rootInjector.dispose();

            expect(
                (child.parent.dispose as sinon.SinonStub).calledBefore(
                    child.grandparent.dispose
                )
            ).to.be.equal(true);
            expect(
                (child.parent.dispose as sinon.SinonStub).calledBefore(
                    newGrandparent.dispose
                )
            ).to.be.equal(true);
            expect(
                (child.dispose as sinon.SinonStub).calledBefore(
                    child.parent.dispose
                )
            ).to.be.equal(true);
        });

        it('Should not dispose injected classes or functions', async () => {
            class Foo {
                dispose = sinon.stub();
            }
            function barFactory(): Disposable {
                return { dispose: sinon.stub() };
            }
            const foo = rootInjector.injectClass(Foo);
            const bar = rootInjector.injectFunction(barFactory);
            await rootInjector.dispose();
            expect((foo.dispose as sinon.SinonStub).called).to.be.equal(false);
            expect((bar.dispose as sinon.SinonStub).called).to.be.equal(false);
        });

        it('Should not dispose providedValues', async () => {
            const disposable: Disposable = { dispose: sinon.stub() };
            const disposableProvider = rootInjector.provideValue(
                'disposable',
                disposable
            );
            disposableProvider.get('disposable');
            await disposableProvider.dispose();
            expect((disposable.dispose as sinon.SinonStub).called).to.be.equal(
                false
            );
        });

        it('Should not break on non-disposable dependencies', async () => {
            class Foo {
                dispose = true;
            }
            function barFactory(): { dispose: string } {
                return { dispose: 'no-fn' };
            }
            class Baz {
                constructor(
                    public readonly bar: { dispose: string },
                    public readonly foo: Foo
                ) {}

                static $inject = ['bar', 'foo'] as const;
            }
            const bazInjector = rootInjector
                .provideClass('foo', Foo)
                .provideFactory('bar', barFactory);
            const baz = bazInjector.injectClass(Baz);

            await bazInjector.dispose();

            expect(baz.bar.dispose).to.be.equal('no-fn');
            expect(baz.foo.dispose).to.be.equal(true);
        });

        it('Should not dispose dependencies twice', async () => {
            const fooProvider = rootInjector.provideClass(
                'foo',
                class Foo implements Disposable {
                    dispose = sinon.stub();
                }
            );
            const foo = fooProvider.get('foo');
            await fooProvider.dispose();
            await fooProvider.dispose();
            expect((foo.dispose as sinon.SinonStub).calledOnce).to.be.equal(
                true
            );
        });

        it('Should await dispose()', async () => {
            class Task {
                resolve!: () => void;

                reject!: (reason?: Error) => void;

                promise: Promise<void>;

                constructor() {
                    this.promise = new Promise<void>((res, rej) => {
                        this.resolve = res;
                        this.reject = rej;
                    });
                }
            }

            function tick(): Promise<void> {
                return new Promise(res => setTimeout(res));
            }

            const fooStub = sinon.stub();
            class Foo {
                task = new Task();

                dispose() {
                    fooStub();
                    return this.task.promise;
                }
            }
            const fooProvider = rootInjector.provideClass('foo', Foo);
            const foo = fooProvider.get('foo');
            let resolved = false;

            const promise = fooProvider.dispose().then(() => {
                resolved = true;
            });
            await tick(); // make sure it has a chance to fail.

            expect(fooStub.called).to.be.equal(true);
            expect(resolved).to.be.equal(false);
            foo.task.resolve();
            await promise;
            expect(resolved).to.be.equal(true);
        });

        it("Should dispose it's child providers", async () => {
            const fooDisposeStub = sinon.stub();
            class Foo {
                dispose() {
                    fooDisposeStub();
                }
            }
            const fooProvider = rootInjector.provideClass('foo', Foo);
            fooProvider.get('foo');

            await rootInjector.dispose();

            expect(fooDisposeStub.called).to.be.equal(true);
        });

        it('Should be removed from parent on disposal', async () => {
            const root = new RootInjector();
            const child = root.provideValue('a', 'a');
            await child.dispose();
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            expect((root as any)._childInjectors.size).to.be.equal(0);
        });

        it("Should not dispose it's parent provider", async () => {
            class Grandparent {
                dispose = sinon.stub();
            }
            class Parent {
                dispose = sinon.stub();
            }
            class Child {
                constructor(
                    readonly parent: Parent,
                    readonly grandparent: Grandparent
                ) {}

                static $inject = ['parent', 'grandparent'] as const;

                dispose = sinon.stub();
            }
            const parentProvider = rootInjector
                .provideClass('grandparent', Grandparent, Scope.Transient)
                .provideClass('parent', Parent);
            const childProvider = parentProvider.provideClass('child', Child);
            const child = childProvider.get('child');

            await childProvider.dispose();

            expect((child.dispose as sinon.SinonStub).called).to.be.equal(true);
            expect(
                (child.parent.dispose as sinon.SinonStub).called
            ).to.be.equal(false);
        });
    });

    describe('dependency tree', () => {
        it('Should be able to inject a dependency tree', () => {
            // Arrange
            class Logger {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                info(_msg: string) {}
            }
            class GrandChild {
                baz = 'qux';

                constructor(public log: Logger) {}

                static $inject = ['logger'] as const;
            }
            class Child1 {
                bar = 'foo';

                constructor(
                    public log: Logger,
                    public grandchild: GrandChild
                ) {}

                static $inject = ['logger', 'grandChild'] as const;
            }
            class Child2 {
                foo = 'bar';

                constructor(public log: Logger) {}

                static $inject = ['logger'] as const;
            }
            class Parent {
                constructor(
                    public readonly child: Child1,
                    public readonly child2: Child2,
                    public readonly log: Logger
                ) {}

                static $inject = ['child1', 'child2', 'logger'] as const;
            }
            const expectedLogger = new Logger();

            const actual = rootInjector
                .provideValue('logger', expectedLogger)
                .provideClass('grandChild', GrandChild)
                .provideClass('child1', Child1)
                .provideClass('child2', Child2)
                .injectClass(Parent);

            expect(actual.child.bar).to.be.equal('foo');
            expect(actual.child2.foo).to.be.equal('bar');
            expect(actual.child.log).to.be.equal(expectedLogger);
            expect(actual.child2.log).to.be.equal(expectedLogger);
            expect(actual.child.grandchild.log).to.be.equal(expectedLogger);
            expect(actual.child.grandchild.baz).to.be.equal('qux');
            expect(actual.log).to.be.equal(expectedLogger);
        });

        it('Should throw an Injection error with correct message when injection failed with a runtime error', () => {
            const expectedCause = Error('Expected error');
            class GrandChild {
                baz = 'baz';

                constructor() {
                    throw expectedCause;
                }
            }
            class Child {
                bar = 'foo';

                constructor(public grandchild: GrandChild) {}

                static $inject = ['grandChild'] as const;
            }
            class Parent {
                constructor(public readonly child: Child) {}

                static $inject = ['child'] as const;
            }

            const act = () =>
                rootInjector
                    .provideClass('grandChild', GrandChild)
                    .provideClass('child', Child)
                    .injectClass(Parent);
            expect(act)
                .throws(InjectionError)
                .which.deep.includes({
                    message:
                        'Could not inject [class Parent] -> [token "child"] -> [class Child] -> [token "grandChild"] -> [class GrandChild]. Cause: Expected error',
                    path: [Parent, 'child', Child, 'grandChild', GrandChild],
                });
        });
    });

    describe('getParent', () => {
        it('Should return parent injector or null for the rootInjector', () => {
            const value = {};
            function factory() {}
            class MyClass {}
            const injector1 = rootInjector.provideValue('test1', value);
            const injector2 = injector1.provideFactory('test2', factory);
            const injector3 = injector2.provideClass('test3', MyClass);

            expect(injector3.getParent()).to.be.equal(injector2);
            expect(injector2.getParent()).to.be.equal(injector1);
            expect(injector1.getParent()).to.be.equal(rootInjector);
            expect(rootInjector.getParent()).to.be.equal(null);
        });
    });

    describe('getToken', () => {
        it('Should return the last token or null for the rootInjector', () => {
            const value = {};
            function factory() {}
            class MyClass {}
            const injector1 = rootInjector.provideValue('test1', value);
            const injector2 = injector1.provideFactory('test2', factory);
            const injector3 = injector2.provideClass('test3', MyClass);

            expect(injector3.getLastToken()).to.be.equal('test3');
            expect(injector2.getLastToken()).to.be.equal('test2');
            expect(injector1.getLastToken()).to.be.equal('test1');
            expect(rootInjector.getLastToken()).to.be.equal(null);
        });
    });

    describe('getProviders', () => {
        it('Should return all provider and child provider', () => {
            const value = {};
            function factory() {
                return 42;
            }
            class MyClass {}
            const injector = rootInjector
                .provideValue('test1', value)
                .provideFactory('test2', factory)
                .provideClass('test3', MyClass);

            const providers = injector.getProviders();
            expect(providers.test1).to.be.equal(value);
            expect(providers.test2).to.be.equal(factory());
            expect(providers.test3).to.be.instanceOf(MyClass);
        });
    });
});
