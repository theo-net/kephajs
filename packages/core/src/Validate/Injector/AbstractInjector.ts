/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InjectionError, InjectorDisposedError } from './Error';
import { Scope } from './Scope';

export const INJECTOR_TOKEN = '$injector' as const;
export const TARGET_TOKEN = '$target' as const;
export type InjectionToken<TContext> =
    | typeof INJECTOR_TOKEN
    | typeof TARGET_TOKEN
    | keyof TContext;

type CorrespondingType<
    TContext,
    T extends InjectionToken<TContext>
> = T extends typeof INJECTOR_TOKEN
    ? Injector<TContext>
    : T extends typeof TARGET_TOKEN
    ? // eslint-disable-next-line @typescript-eslint/ban-types
      Function | undefined
    : T extends keyof TContext
    ? TContext[T]
    : never;
type CorrespondingTypes<
    TContext,
    TS extends readonly InjectionToken<TContext>[]
> = {
    [key in keyof TS]: TS[key] extends InjectionToken<TContext>
        ? CorrespondingType<TContext, TS[key]>
        : never;
};

/**
 * Just usefull for the error generating
 */
// eslint-disable-next-line @typescript-eslint/ban-types
export type InjectionTarget = Function | number | symbol | string;

/**
 * Represents a class or a function that is injectable
 */
interface ClassWithInjections<
    TContext,
    R,
    Tokens extends readonly InjectionToken<TContext>[]
> {
    new (...args: CorrespondingTypes<TContext, Tokens>): R;
    readonly $inject: Tokens;
}
type ClassWithoutInjections<R> = new () => R;
export type InjectableClass<
    TContext,
    R,
    Tokens extends readonly InjectionToken<TContext>[]
> = ClassWithInjections<TContext, R, Tokens> | ClassWithoutInjections<R>;

interface InjectableFunctionWithInject<
    TContext,
    R,
    Tokens extends readonly InjectionToken<TContext>[]
> {
    (...args: CorrespondingTypes<TContext, Tokens>): R;
    readonly $inject: Tokens;
}
type InjectableFunctionWithoutInject<R> = () => R;
export type InjectableFunction<
    TContext,
    R,
    Tokens extends readonly InjectionToken<TContext>[]
> =
    | InjectableFunctionWithInject<TContext, R, Tokens>
    | InjectableFunctionWithoutInject<R>;

export type Injectable<
    TContext,
    R,
    Tokens extends readonly InjectionToken<TContext>[]
> =
    | InjectableClass<TContext, R, Tokens>
    | InjectableFunction<TContext, R, Tokens>;

/**
 * Represent the child's context
 */
export type TChildContext<
    TParentContext,
    TProvided,
    CurrentToken extends string
> = {
    [key in keyof (TParentContext & {
        [K in CurrentToken]: TProvided;
    })]: key extends CurrentToken
        ? TProvided
        : key extends keyof TParentContext
        ? TParentContext[key]
        : never;
};

/**
 * An injector
 */
// eslint-disable-next-line @typescript-eslint/ban-types
export interface Injector<TContext = {}> {
    injectClass<R, Tokens extends readonly InjectionToken<TContext>[]>(
        Class: InjectableClass<TContext, R, Tokens>
    ): R;
    injectFunction<R, Tokens extends readonly InjectionToken<TContext>[]>(
        fn: InjectableFunction<TContext, R, Tokens>
    ): R;
    get<Token extends keyof TContext>(token: Token): TContext[Token];
    getProviders(): TContext;
    register<Token extends string, R>(
        token: Token,
        value: R
    ): Injector<TChildContext<TContext, R, Token>>;
    provideValue<Token extends string, R>(
        token: Token,
        value: R
    ): Injector<TChildContext<TContext, R, Token>>;
    provideClass<
        Token extends string,
        R,
        Tokens extends readonly InjectionToken<TContext>[]
    >(
        token: Token,
        Class: InjectableClass<TContext, R, Tokens>,
        scope?: Scope
    ): Injector<TChildContext<TContext, R, Token>>;
    provideFactory<
        Token extends string,
        R,
        Tokens extends readonly InjectionToken<TContext>[]
    >(
        token: Token,
        factory: InjectableFunction<TContext, R, Tokens>,
        scope?: Scope
    ): Injector<TChildContext<TContext, R, Token>>;
    dispose(): Promise<void>;
    getParent(): null | AbstractInjector<TContext>;
    getLastToken(): null | string;
}

const DEFAULT_SCOPE = Scope.Singleton;

/**
 *        ┏━━━━━━━━━━━━━━━━━━┓
 *        ┃ AbstractInjector ┃
 *        ┗━━━━━━━━━━━━━━━━━━┛
 *                   ▲
 *                   ┃
 *           ┏━━━━━━━┻━━━━━━━━┓
 *           ┃                ┃
 *   ┏━━━━━━━┻━━━━━━┓ ┏━━━━━━━┻━━━━━━━┓
 *   ┃ RootInjector ┃ ┃ ChildInjector ┃
 *   ┗━━━━━━━━━━━━━━┛ ┗━━━━━━━━━━━━━━━┛
 *                            ▲
 *                            ┃
 *          ┏━━━━━━━━━━━━━━━━━┻┳━━━━━━━━━━━━━━━━━┓
 * ┏━━━━━━━━┻━━━━━━━━┓ ┏━━━━━━━┻━━━━━━━┓ ┏━━━━━━━┻━━━━━━━┓
 * ┃ FactoryInjector ┃ ┃ ClassInjector ┃ ┃ ValueInjector ┃
 * ┗━━━━━━━━━━━━━━━━━┛ ┗━━━━━━━━━━━━━━━┛ ┗━━━━━━━━━━━━━━━┛
 */
export abstract class AbstractInjector<TContext> implements Injector<TContext> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private _childInjectors: Set<Injector<any>> = new Set();

    private _isDisposed = false;

    /**
     * Register something is exactly provide a value
     */
    register<Token extends string, R>(
        token: Token,
        value: R
    ): AbstractInjector<TChildContext<TContext, R, Token>> {
        return this.provideValue(token, value);
    }

    /**
     * Provide a value
     */
    provideValue<Token extends string, R>(
        token: Token,
        value: R
    ): AbstractInjector<TChildContext<TContext, R, Token>> {
        this._throwIfDisposed(token);
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        const provider = new ValueProvider(this, token, value);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this._childInjectors.add(provider as unknown as Injector<any>);
        return provider;
    }

    provideClass<
        Token extends string,
        R,
        Tokens extends InjectionToken<TContext>[]
    >(
        token: Token,
        Class: InjectableClass<TContext, R, Tokens>,
        scope = DEFAULT_SCOPE
    ): AbstractInjector<TChildContext<TContext, R, Token>> {
        this._throwIfDisposed(token);
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        const provider = new ClassProvider(this, token, scope, Class);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this._childInjectors.add(provider as unknown as Injector<any>);
        return provider;
    }

    provideFactory<
        Token extends string,
        R,
        Tokens extends InjectionToken<TContext>[]
    >(
        token: Token,
        factory: InjectableFunction<TContext, R, Tokens>,
        scope = DEFAULT_SCOPE
    ): AbstractInjector<TChildContext<TContext, R, Token>> {
        this._throwIfDisposed(token);
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        const provider = new FactoryProvider(this, token, scope, factory);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this._childInjectors.add(provider as unknown as Injector<any>);
        return provider;
    }

    /**
     * Resolve the injection from a token name
     */
    get<Token extends keyof TContext>(
        token: Token,
        // eslint-disable-next-line @typescript-eslint/ban-types
        target?: Function
    ): TContext[Token] {
        this._throwIfDisposed(token);
        return this._resolveInternal(token, target);
    }

    /**
     * Return all provider and child providers
     *
     * Be carefull, it's from the last provided to the first!
     */
    getProviders(context: TContext = {} as TContext): TContext {
        const token = this.getLastToken();
        if (token !== null) {
            context[token as keyof TContext] = this.get(
                token as keyof TContext
            );
        }

        const parent = this.getParent();
        if (parent !== null) {
            context = parent.getProviders(context);
        }

        return context;
    }

    /**
     * Create a new instance of a class by populating its constructor arguments from the injector and returns it.
     */
    injectClass<R, Tokens extends readonly InjectionToken<TContext>[]>(
        Class: InjectableClass<TContext, R, Tokens>,
        // eslint-disable-next-line @typescript-eslint/ban-types
        provideIn?: Function
    ): R {
        this._throwIfDisposed(Class);

        try {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const args: any[] = this._resolveParametersToInject(
                Class,
                provideIn
            );
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            return new Class(...(args as any));
        } catch (error) {
            throw InjectionError.create(Class, error as Error);
        }
    }

    injectFunction<R, Tokens extends InjectionToken<TContext>[]>(
        fn: InjectableFunction<TContext, R, Tokens>,
        // eslint-disable-next-line @typescript-eslint/ban-types
        provideIn?: Function
    ): R {
        this._throwIfDisposed(fn);

        try {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const args: any[] = this._resolveParametersToInject(fn, provideIn);
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            return fn(...(args as any));
        } catch (error) {
            throw InjectionError.create(fn, error as Error);
        }
    }

    async dispose() {
        if (!this._isDisposed) {
            this._isDisposed = true; // be sure new disposables aren't added while we're disposing
            const promises: Promise<void>[] = [];
            for (const child of this._childInjectors) {
                promises.push(child.dispose());
            }
            await Promise.all(promises);
            await this._disposeInjectedValues();
        }
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    removeChild(child: Injector<any>): void {
        this._childInjectors.delete(child);
    }

    getParent(): null | AbstractInjector<TContext> {
        return null;
    }

    getLastToken(): null | string {
        return null;
    }

    /**
     * Throw an error if the injector is disposed
     */
    protected _throwIfDisposed(injectableOrToken: InjectionTarget) {
        if (this._isDisposed) {
            throw new InjectorDisposedError(injectableOrToken);
        }
    }

    private _resolveParametersToInject<
        Tokens extends InjectionToken<TContext>[]
    >(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        injectable: Injectable<TContext, any, Tokens>,
        // eslint-disable-next-line @typescript-eslint/ban-types
        target?: Function
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ): any[] {
        const tokens: InjectionToken<TContext>[] =
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            (injectable as any).$inject || [];
        return tokens.map(key => {
            switch (key) {
                case TARGET_TOKEN:
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    return target as any;
                case INJECTOR_TOKEN:
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    return this as any;
                default:
                    return this._resolveInternal(key, injectable);
            }
        });
    }

    protected abstract _disposeInjectedValues(): Promise<void>;

    /**
     * Resolve the injection from a token
     */
    protected abstract _resolveInternal<Token extends keyof TContext>(
        token: Token,
        // eslint-disable-next-line @typescript-eslint/ban-types
        target?: Function
    ): TContext[Token];
}

// End beacause recursive import
import { ClassProvider } from './ClassProvider';
import { FactoryProvider } from './FactoryProvider';
import { ValueProvider } from './ValueProvider';
