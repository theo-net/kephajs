/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export interface Disposable {
    dispose(): void | PromiseLike<void>;
}

export function isDisposable(
    maybeDisposable: unknown
): maybeDisposable is Disposable {
    return typeof (maybeDisposable as Disposable)?.dispose === 'function';
}
