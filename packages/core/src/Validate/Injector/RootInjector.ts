/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { AbstractInjector } from './AbstractInjector';

// eslint-disable-next-line @typescript-eslint/ban-types
export class RootInjector extends AbstractInjector<{}> {
    protected override _resolveInternal(token: never): never {
        throw new Error(`No provider found for "${token}"!`);
    }

    protected override _disposeInjectedValues() {
        return Promise.resolve();
    }
}
