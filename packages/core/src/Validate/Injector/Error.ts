/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InjectionTarget } from './AbstractInjector';

function describeInjectAction(target: InjectionTarget) {
    if (typeof target === 'function') {
        return 'inject';
    } else {
        return 'resolve';
    }
}

function name(target: InjectionTarget) {
    if (typeof target === 'function') {
        if (target.toString().startsWith('class')) {
            return `[class ${target.name || '<anonymous>'}]`;
        } else {
            return `[function ${target.name || '<anonymous>'}]`;
        }
    } else {
        return `[token "${String(target)}"]`;
    }
}

export class InjectorDisposedError extends Error {
    constructor(target: InjectionTarget) {
        super(
            `Injector is already disposed. Please don't use it anymore. Tried to ${describeInjectAction(
                target
            )} ${name(target)}.`
        );
    }
}

export class InjectionError extends Error {
    constructor(
        public readonly path: InjectionTarget[],
        public readonly cause: Error
    ) {
        super(
            `Could not ${describeInjectAction(path[0])} ${path
                .map(name)
                .join(' -> ')}. Cause: ${cause.message}`
        );
        this.stack =
            this.stack && cause.stack
                ? this.message + '\n\n' + cause.stack + '\n\n' + this.stack
                : '';
    }

    static create(target: InjectionTarget, error: Error): InjectionError {
        if (error instanceof InjectionError) {
            return new InjectionError([target, ...error.path], error.cause);
        } else {
            return new InjectionError([target], error);
        }
    }
}
