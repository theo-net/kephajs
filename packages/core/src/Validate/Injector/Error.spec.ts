/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/* eslint-disable @typescript-eslint/no-explicit-any */

import { expect } from 'chai';
import { InjectorDisposedError, InjectionError } from './Error';

describe('@kephajs/core/Services/Injector/Error', () => {
    class MyService {}
    function myFunction() {}

    describe('InjectorDisposedError', () => {
        it('Should reconize the action and the name', () => {
            expect(new InjectorDisposedError('myService').message).to.be.equal(
                'Injector is already disposed. Please don\'t use it anymore. Tried to resolve [token "myService"].'
            );
            expect(new InjectorDisposedError(MyService).message).to.be.equal(
                "Injector is already disposed. Please don't use it anymore. Tried to inject [class MyService]."
            );
            expect(new InjectorDisposedError(myFunction).message).to.be.equal(
                "Injector is already disposed. Please don't use it anymore. Tried to inject [function myFunction]."
            );
        });
    });

    describe('InjectorError', () => {
        it('Should reconize the action and the name', () => {
            expect(
                new InjectionError(['myService'], new Error('my error')).message
            ).to.be.equal(
                'Could not resolve [token "myService"]. Cause: my error'
            );
            expect(
                new InjectionError([MyService], new Error('my error')).message
            ).to.be.equal(
                'Could not inject [class MyService]. Cause: my error'
            );
            expect(
                new InjectionError([myFunction], new Error('my error')).message
            ).to.be.equal(
                'Could not inject [function myFunction]. Cause: my error'
            );
        });

        it('Should give the pathof the resolving injection', () => {
            expect(
                new InjectionError(
                    ['myService', MyService, myFunction],
                    new Error('my error')
                ).message
            ).to.be.equal(
                'Could not resolve [token "myService"] -> [class MyService] -> [function myFunction]. Cause: my error'
            );
            expect(
                new InjectionError(
                    [MyService, 'myService', myFunction],
                    new Error('my error')
                ).message
            ).to.be.equal(
                'Could not inject [class MyService] -> [token "myService"] -> [function myFunction]. Cause: my error'
            );
        });
    });
});
