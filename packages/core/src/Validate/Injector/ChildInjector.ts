/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { AbstractInjector, Injector, TChildContext } from './AbstractInjector';
import { Disposable, isDisposable } from './Disposable';
import { InjectionError } from './Error';
import { Scope } from './Scope';

export abstract class ChildInjector<
    TParentContext,
    TProvided,
    CurrentToken extends string
> extends AbstractInjector<
    TChildContext<TParentContext, TProvided, CurrentToken>
> {
    // Use an object, as also undefined value can be stored
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private _cached: { value?: any } | undefined;

    private readonly _disposables = new Set<Disposable>();

    constructor(
        protected readonly _parent: AbstractInjector<TParentContext>,
        protected readonly _token: CurrentToken,
        private readonly _scope: Scope
    ) {
        super();
    }

    // eslint-disable-next-line @typescript-eslint/ban-types
    protected abstract _result(target: Function | undefined): TProvided;

    override async dispose() {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this._parent.removeChild(this as Injector<any>);
        await super.dispose();
    }

    getParent() {
        return this._parent as AbstractInjector<
            TChildContext<TParentContext, TProvided, CurrentToken>
        >;
    }

    getLastToken(): string | null {
        return this._token;
    }

    protected override async _disposeInjectedValues() {
        const promisesToAwait = [...this._disposables.values()].map(
            disposable => disposable.dispose()
        );
        await Promise.all(promisesToAwait);
    }

    protected override _resolveInternal<
        SearchToken extends keyof TChildContext<
            TParentContext,
            TProvided,
            CurrentToken
        >
    >(
        token: SearchToken,
        // eslint-disable-next-line @typescript-eslint/ban-types
        target: Function | undefined
    ): TChildContext<TParentContext, TProvided, CurrentToken>[SearchToken] {
        if ((token as unknown as CurrentToken) === this._token) {
            if (this._cached) {
                return this._cached.value;
            } else {
                try {
                    const value = this._result(target);
                    this._addToCacheIfNeeded(value);
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    return value as any;
                } catch (error) {
                    throw InjectionError.create(token, error as Error);
                }
            }
        } else {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            return this._parent.get(token as any, target) as any;
        }
    }

    private _addToCacheIfNeeded(value: TProvided) {
        if (this._scope === Scope.Singleton) {
            this._cached = { value };
        }
    }

    protected _registerProvidedValue(value: TProvided): TProvided {
        if (isDisposable(value)) {
            this._disposables.add(value);
        }
        return value;
    }
}
