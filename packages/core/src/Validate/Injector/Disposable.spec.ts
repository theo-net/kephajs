/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import { isDisposable } from './Disposable';

describe('@kephajs/core/Validate/Injector/Disposable', () => {
    describe('isDisposable', () => {
        it('Should return false for undefined value', () => {
            expect(isDisposable(undefined)).to.be.equal(false);
        });

        it('Should return false for null value', () => {
            expect(isDisposable(null)).to.be.equal(false);
        });

        it("Should return false if the key dispose don't exist", () => {
            class A {}
            const a = new A();
            expect(isDisposable(a)).to.be.equal(false);
        });

        it('Should return false if the key dispose is not a function', () => {
            class A {
                dispose = 2;
            }
            const a = new A();
            expect(isDisposable(a)).to.be.equal(false);
        });

        it('Should return true if the method dispose exists', () => {
            class A {
                dispose() {}
            }
            const a = new A();
            expect(isDisposable(a)).to.be.equal(true);
        });
    });
});
