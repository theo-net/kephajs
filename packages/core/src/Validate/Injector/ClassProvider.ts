/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    AbstractInjector,
    InjectableClass,
    InjectionToken,
} from './AbstractInjector';
import { ChildInjector } from './ChildInjector';
import { Scope } from './Scope';

export class ClassProvider<
    TParentContext,
    TProvided,
    ProvidedToken extends string,
    Tokens extends InjectionToken<TParentContext>[]
> extends ChildInjector<TParentContext, TProvided, ProvidedToken> {
    constructor(
        parent: AbstractInjector<TParentContext>,
        token: ProvidedToken,
        scope: Scope,
        private readonly _injectable: InjectableClass<
            TParentContext,
            TProvided,
            Tokens
        >
    ) {
        super(parent, token, scope);
    }

    // eslint-disable-next-line @typescript-eslint/ban-types
    protected override _result(target: Function): TProvided {
        return this._registerProvidedValue(
            this._parent.injectClass(this._injectable, target)
        );
    }
}
