/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { AbstractInjector } from './AbstractInjector';
import { ChildInjector } from './ChildInjector';
import { Scope } from './Scope';

export class ValueProvider<
    TParentContext,
    TProvided,
    ProvidedToken extends string
> extends ChildInjector<TParentContext, TProvided, ProvidedToken> {
    constructor(
        parent: AbstractInjector<TParentContext>,
        token: ProvidedToken,
        private readonly _value: TProvided
    ) {
        super(parent, token, Scope.Transient);
    }

    protected _result(): TProvided {
        return this._value;
    }
}
