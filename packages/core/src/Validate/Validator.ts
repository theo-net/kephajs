/* eslint-disable @typescript-eslint/lines-between-class-members */
/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ApplicationValidators } from './ApplicationValidators';
import { ErrorMap, setErrorMap } from './Error/ErrorMap';
import { AnyType } from './Type/Any';
import { ArrayType } from './Type/Array';
import { BigIntType } from './Type/BigInt';
import { BooleanType } from './Type/Boolean';
import { DateType } from './Type/Date';
import { DiscriminatedUnion } from './Type/DiscriminatedUnion';
import { Effects } from './Type/Effects';
import { EnumType } from './Type/Enum';
import { FunctionType } from './Type/Function';
import { Intersection } from './Type/Intersection';
import { LazyType } from './Type/Lazy';
import { LiteralType } from './Type/Literal';
import { MapType } from './Type/Map';
import { NaNType } from './Type/NaNType';
import { NativeEnumType } from './Type/NativeEnum';
import { NeverType } from './Type/Never';
import { NullType } from './Type/Null';
import { Nullable } from './Type/Nullable';
import { NumberType } from './Type/Number';
import { ObjectType } from './Type/Object';
import { Optional } from './Type/Optional';
import { RecordType } from './Type/Record';
import { SetType } from './Type/Set';
import { StringType } from './Type/String';
import { TupleType } from './Type/Tuple';
import { Type, TypeAny } from './Type/Type';
import { UndefinedType } from './Type/Undefined';
import { Union } from './Type/Union';
import { UnknownType } from './Type/Unknown';
import { VoidType } from './Type/Void';

export * from './Error/Error';
export type ErrorMapValidatorType = ErrorMap;

export class Validator {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected readonly _rules: ApplicationValidators = {} as any;

    get rules() {
        return this._rules;
    }

    register(name: string, type: TypeAny) {
        this._rules[name] = type;
    }

    string = StringType.create;
    number = NumberType.create;
    bigint = BigIntType.create;
    boolean = BooleanType.create;
    date = DateType.create;
    undefined = UndefinedType.create;
    null = NullType.create;
    void = VoidType.create;
    any = AnyType.create;
    unknown = UnknownType.create;
    never = NeverType.create;

    literal = LiteralType.create;

    nan = NaNType.create;

    enum = EnumType.create;
    nativeEnum = NativeEnumType.create;

    object = ObjectType.create;
    array = ArrayType.create;
    tuple = TupleType.create;
    record = RecordType.create;
    map = MapType.create;
    set = SetType.create;

    function = FunctionType.create;

    optional = Optional.create;
    nullable = Nullable.create;

    union = Union.create;
    discriminatedUnion = DiscriminatedUnion.create;
    intersection = Intersection.create;

    lazy = LazyType.create;
    late = {
        object: ObjectType.lazycreate,
    };

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    instanceof<T extends new (...args: any[]) => any>(
        cls: T,
        params: Parameters<TypeAny['refine']>[1] = {
            message: `Input not instance of ${cls.name}`,
        }
    ) {
        return this._custom<InstanceType<T>>(
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            (data: any) => data instanceof cls,
            params
        );
    }

    preprocess = Effects.createWithPreprocess;

    /**
     * Define a new default ErrorMap
     */
    setErrorMap(map: ErrorMap) {
        setErrorMap(map);
    }

    protected _custom<T>(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        check?: (data: unknown) => any,
        params?: Parameters<TypeAny['refine']>[1]
    ): Type<T> {
        if (check) return AnyType.create().refine(check, params);
        return AnyType.create();
    }
}
