/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { Optional } from './Optional';
import { StringType } from './String';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Optional', () => {
    it('Should parse Optional string', () => {
        expect(
            Optional.create(StringType.create()).parse('foobar')
        ).to.be.equal('foobar');
    });

    it('Should parse Optional number', () => {
        const f = () => Optional.create(StringType.create()).parse(123);
        expect(f).to.throw();
    });

    it('Should parse Optional boolean', () => {
        const f = () => Optional.create(StringType.create()).parse(true);
        expect(f).to.throw();
    });

    it('Should parse Optional undefined', () => {
        expect(
            Optional.create(StringType.create()).parse(undefined)
        ).to.be.equal(undefined);
    });

    it('Should parse Optional null', () => {
        const f = () => Optional.create(StringType.create()).parse(null);
        expect(f).to.throw();
    });

    it('Should infer Optional', () => {
        const MyOptional = Optional.create(StringType.create());
        type MyOptionalType = TypeOf<typeof MyOptional>;
        const result: AssertEqual<MyOptionalType, string | undefined> = true;
        expect(result).to.be.equal(true);
    });

    it('Should extract the wrapped scheama', () => {
        const stringSchema = StringType.create();
        expect(Optional.create(stringSchema).unwrap()).to.be.deep.equal(
            stringSchema
        );
    });
});
