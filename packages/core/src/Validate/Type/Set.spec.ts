/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import { IssueCode } from '../Error/Error';

import { AssertEqual } from '../helpers/testUtils';
import { SetType } from './Set';
import { StringType } from './String';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Set', () => {
    const stringSet = SetType.create(StringType.create());
    type StringSetType = TypeOf<typeof stringSet>;

    const minTwo = SetType.create(StringType.create()).min(2);
    const maxTwo = SetType.create(StringType.create()).max(2);
    const justTwo = SetType.create(StringType.create()).size(2);
    const nonEmpty = SetType.create(StringType.create()).nonempty();
    const nonEmptyMax = SetType.create(StringType.create()).nonempty().max(2);

    it('Should type inference', () => {
        const f1: AssertEqual<StringSetType, Set<string>> = true;
        expect(f1).to.be.equal(true);
    });

    it('Should valid parse', () => {
        const result = stringSet.safeParse(new Set(['first', 'second']));
        expect(result.success).to.be.equal(true);
        if (result.success) {
            expect(result.data.has('first')).to.be.equal(true);
            expect(result.data.has('second')).to.be.equal(true);
            expect(result.data.has('third')).to.be.equal(false);
        }

        expect(() => {
            minTwo.parse(new Set(['a', 'b']));
            minTwo.parse(new Set(['a', 'b', 'c']));
            maxTwo.parse(new Set(['a', 'b']));
            maxTwo.parse(new Set(['a']));
            justTwo.parse(new Set(['a', 'b']));
            nonEmpty.parse(new Set(['a']));
            nonEmptyMax.parse(new Set(['a']));
        }).not.to.throw();
    });

    it('Should valid parse: size-related methods', () => {
        expect(() => {
            minTwo.parse(new Set(['a', 'b']));
            minTwo.parse(new Set(['a', 'b', 'c']));
            maxTwo.parse(new Set(['a', 'b']));
            maxTwo.parse(new Set(['a']));
            justTwo.parse(new Set(['a', 'b']));
            nonEmpty.parse(new Set(['a']));
            nonEmptyMax.parse(new Set(['a']));
        }).not.to.throw();

        const sizeZeroResult = stringSet.parse(new Set());
        expect(sizeZeroResult.size).to.be.equal(0);

        const sizeTwoResult = minTwo.parse(new Set(['a', 'b']));
        expect(sizeTwoResult.size).to.be.equal(2);
    });

    it('Should failing when parsing empty set in nonempty ', () => {
        const result = nonEmpty.safeParse(new Set());
        expect(result.success).to.be.equal(false);

        if (result.success === false) {
            expect(result.error.errors.length).to.be.equal(1);
            expect(result.error.errors[0].code).to.be.equal(IssueCode.tooSmall);
        }
    });

    it('Should failing when set is smaller than min() ', () => {
        const result = minTwo.safeParse(new Set(['just_one']));
        expect(result.success).to.be.equal(false);

        if (result.success === false) {
            expect(result.error.errors.length).to.be.equal(1);
            expect(result.error.errors[0].code).to.be.equal(IssueCode.tooSmall);
        }
    });

    it('Should failing when set is bigger than max() ', () => {
        const result = maxTwo.safeParse(new Set(['one', 'two', 'three']));
        expect(result.success).to.be.equal(false);

        if (result.success === false) {
            expect(result.error.errors.length).to.be.equal(1);
            expect(result.error.errors[0].code).to.be.equal(IssueCode.tooBig);
        }
    });

    it('Should doesn’t throw when an empty set is given', () => {
        const result = stringSet.safeParse(new Set([]));
        expect(result.success).to.be.equal(true);
    });

    it('Should throws when a Map is given', () => {
        const result = stringSet.safeParse(new Map([]));
        expect(result.success).to.be.equal(false);
        if (result.success === false) {
            expect(result.error.errors.length).to.be.equal(1);
            expect(result.error.errors[0].code).to.be.equal(
                IssueCode.invalidType
            );
        }
    });

    it('Should throws when the given set has invalid input', () => {
        const result = stringSet.safeParse(new Set([Symbol()]));
        expect(result.success).to.be.equal(false);
        if (result.success === false) {
            expect(result.error.errors.length).to.be.equal(1);
            expect(result.error.errors[0].code).to.be.equal(
                IssueCode.invalidType
            );
            expect(result.error.errors[0].path).to.be.deep.equal([0]);
        }
    });

    it('Should throws when the given set has multiple invalid entries', () => {
        const result = stringSet.safeParse(
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            new Set([1, 2] as any[]) as Set<any>
        );

        expect(result.success).to.be.equal(false);
        if (result.success === false) {
            expect(result.error.errors.length).to.be.equal(2);
            expect(result.error.errors[0].code).to.be.equal(
                IssueCode.invalidType
            );
            expect(result.error.errors[0].path).to.be.deep.equal([0]);
            expect(result.error.errors[1].code).to.be.equal(
                IssueCode.invalidType
            );
            expect(result.error.errors[1].path).to.be.deep.equal([1]);
        }
    });
});
