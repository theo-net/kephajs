/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueData } from '../Error/Error';
import {
    FirstPartyTypeKind,
    RawCreateParams,
    RefinementCtx,
    Type,
    TypeAny,
    TypeDef,
} from './Type';

import {
    INVALID,
    ParseInput,
    ParseReturnType,
    isValid,
} from '../helpers/parseUtils';

export type RefinementEffect<T> = {
    type: 'refinement';
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    refinement: (arg: T, ctx: RefinementCtx) => any;
};
type TransformEffect<T> = {
    type: 'transform';
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    transform: (arg: T, ctx: RefinementCtx) => any;
};
type PreprocessEffect<T> = {
    type: 'preprocess';
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    transform: (arg: T) => any;
};

type Effect<T> = RefinementEffect<T> | TransformEffect<T> | PreprocessEffect<T>;

export type EffectsDef<T extends TypeAny = TypeAny> = {
    typeName: FirstPartyTypeKind.Effects;
    schema: T;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    effect: Effect<any>;
} & TypeDef;

export class Effects<
    T extends TypeAny,
    Output = T['_output'],
    Input = T['_input']
> extends Type<Output, EffectsDef<T>, Input> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const { status, ctx } = this._processInputParams(input);

        const effect = this._def.effect || null;

        if (effect.type === 'preprocess') {
            const processed = effect.transform(ctx.data);

            return this._def.schema._parse({
                data: processed,
                path: ctx.path,
                parent: ctx,
            });
        }

        const checkCtx: RefinementCtx = {
            addIssue: (arg: IssueData) => {
                this._addIssueToContext(ctx, arg);
                if (arg.fatal) {
                    status.abort();
                } else {
                    status.dirty();
                }
            },
            get path() {
                return ctx.path;
            },
        };

        checkCtx.addIssue = checkCtx.addIssue.bind(checkCtx);
        if (effect.type === 'refinement') {
            const executeRefinement = (
                acc: unknown
                // effect: RefinementEffect<any>
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
            ): any => {
                const result = effect.refinement(acc, checkCtx);
                if (result instanceof Promise) {
                    throw new Error("Refinement don't work with async method.");
                }
                return acc;
            };

            const inner = this._def.schema._parse({
                data: ctx.data,
                path: ctx.path,
                parent: ctx,
            });
            if (inner.status === 'aborted') return INVALID;
            if (inner.status === 'dirty') status.dirty();

            // return value is ignored
            executeRefinement(inner.value);
            return { status: status.value, value: inner.value };
        } else if (effect.type === 'transform') {
            const base = this._def.schema._parse({
                data: ctx.data,
                path: ctx.path,
                parent: ctx,
            });
            if (!isValid(base)) return base;

            const result = effect.transform(base.value, checkCtx);
            if (result instanceof Promise) {
                throw new Error(
                    `Asynchronous transform encountered during synchronous parse operation. Refinement don't work with async method.`
                );
            }

            return { status: status.value, value: result };
        }

        return INVALID;
    }

    static create<I extends TypeAny>(
        schema: I,
        effect: Effect<I['_output']>,
        params?: RawCreateParams
    ): Effects<I, I['_output']> {
        return new Effects({
            typeName: FirstPartyTypeKind.Effects,
            schema,
            effect,
            ...Effects.processCreateParams(params),
        });
    }

    static createWithPreprocess<I extends TypeAny>(
        preprocess: (arg: unknown) => unknown,
        schema: I,
        params?: RawCreateParams
    ): Effects<I, I['_output']> {
        return new Effects({
            typeName: FirstPartyTypeKind.Effects,
            schema,
            effect: { type: 'preprocess', transform: preprocess },
            ...Effects.processCreateParams(params),
        });
    }

    innerType() {
        return this._def.schema;
    }
}
