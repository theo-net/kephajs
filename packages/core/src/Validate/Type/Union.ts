/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ErrorValidator, Issue, IssueCode } from '../Error/Error';
import {
    FirstPartyTypeKind,
    RawCreateParams,
    Type,
    TypeAny,
    TypeDef,
} from './Type';

import {
    INVALID,
    ParseInput,
    ParseReturnType,
    DIRTY,
    ParseContext,
} from '../helpers/parseUtils';

type UnionOptions = Readonly<[TypeAny, ...TypeAny[]]>;
export type UnionDef<
    T extends UnionOptions = Readonly<[TypeAny, TypeAny, ...TypeAny[]]>
> = {
    typeName: FirstPartyTypeKind.Union;
    options: T;
} & TypeDef;

/**
 * The framework includes a built-in `v.union` method for composing "OR" types.
 *
 *      const stringOrNumber = Union.create([StringType.create(), NumberType.create()]);
 *
 *      stringOrNumber.parse("foo"); // passes
 *      stringOrNumber.parse(14); // passes
 *
 * We will test the input against each of the "options" in order and return the first value
 * that validates successfully.
 *
 * For convenience, you can also use the `.or` method:
 *
 *      const stringOrNumber = StringType.create().or(Number.create());
 */
export class Union<T extends UnionOptions> extends Type<
    T[number]['_output'],
    UnionDef<T>,
    T[number]['_input']
> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const { ctx } = this._processInputParams(input);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        let dirty: undefined | { result: DIRTY<any>; ctx: ParseContext } =
            undefined;
        const issues: Issue[][] = [];

        for (const option of this._def.options) {
            const childCtx: ParseContext = {
                ...ctx,
                common: {
                    ...ctx.common,
                    issues: [],
                },
                parent: null,
            };

            const result = option._parse({
                data: ctx.data,
                path: ctx.path,
                parent: childCtx,
            });

            // Then we find the first valid schema, we stop the validation
            if (result.status === 'valid') {
                return result;
            } else if (result.status === 'dirty' && !dirty) {
                dirty = { result, ctx: childCtx };
            }

            if (childCtx.common.issues.length) {
                issues.push(childCtx.common.issues);
            }
        }

        if (dirty) {
            ctx.common.issues.push(...dirty.ctx.common.issues);
            return dirty.result;
        }

        this._addIssueToContext(ctx, {
            code: IssueCode.invalidUnion,
            unionErrors: issues.map(iss => new ErrorValidator(iss)),
        });

        return INVALID;
    }

    static create<U extends Readonly<[TypeAny, TypeAny, ...TypeAny[]]>>(
        types: U,
        params?: RawCreateParams
    ): Union<U> {
        return new Union({
            typeName: FirstPartyTypeKind.Union,
            options: types,
            ...Union.processCreateParams(params),
        });
    }

    /**
     * Return all schema
     */
    get options() {
        return this._def.options;
    }
}
