/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { ArrayType } from './Array';
import { BooleanType } from './Boolean';
import { NullType } from './Null';
import { NumberType } from './Number';
import { ObjectType } from './Object';
import { StringType } from './String';
import { TypeOf } from './Type';
import { UndefinedType } from './Undefined';

describe('@kephajs/core/Validate/Type/Array', () => {
    it('Should parse ArrayType string', () => {
        const f = () => ArrayType.create(StringType.create()).parse('foobar');
        expect(f).to.throw();
        expect(
            ArrayType.create(StringType.create()).parse(['foo', 'bar'])
        ).to.be.deep.equal(['foo', 'bar']);
    });

    it('Should parse ArrayType number', () => {
        const f = () => ArrayType.create(NumberType.create()).parse(123);
        expect(f).to.throw();
        expect(
            ArrayType.create(NumberType.create()).parse([1, 2])
        ).to.be.deep.equal([1, 2]);
    });

    it('Should parse ArrayType boolean', () => {
        const f = () => ArrayType.create(BooleanType.create()).parse(true);
        expect(f).to.throw();
        expect(
            ArrayType.create(BooleanType.create()).parse([true, false])
        ).to.be.deep.equal([true, false]);
    });

    it('Should parse ArrayType undefined', () => {
        const f = () =>
            ArrayType.create(UndefinedType.create()).parse(undefined);
        expect(f).to.throw();
        expect(
            ArrayType.create(UndefinedType.create()).parse([undefined])
        ).to.be.deep.equal([undefined]);
    });

    it('Should parse ArrayType null', () => {
        const f = () => ArrayType.create(NullType.create()).parse(null);
        expect(f).to.throw();
        expect(
            ArrayType.create(NullType.create()).parse([null, null])
        ).to.be.deep.equal([null, null]);
    });

    it('Should parse ArrayType object', () => {
        const f = () =>
            ArrayType.create(
                ObjectType.create({ name: StringType.create() })
            ).parse(null);
        expect(f).to.throw();
        expect(
            ArrayType.create(
                ObjectType.create({ name: StringType.create() })
            ).parse([{ name: 'foo' }, { name: 'bar' }])
        ).to.be.deep.equal([{ name: 'foo' }, { name: 'bar' }]);
    });

    it('Should parse inside the array', () => {
        const f = () => ArrayType.create(StringType.create()).parse(['foo', 1]);
        expect(f).to.throw();
    });

    it('Should infer ArrayType', () => {
        const MyArray = ArrayType.create(StringType.create());
        type MyArrayType = TypeOf<typeof MyArray>;
        const result: AssertEqual<MyArrayType, string[]> = true;
        expect(result).to.be.equal(true);
    });

    describe('element', () => {
        it('Should return the given schema', () => {
            const stringSchema = StringType.create();
            expect(stringSchema.array().element).to.be.equal(stringSchema);
        });
    });

    describe('nonEmpty', () => {
        it('Should success for empty array by default', () => {
            expect(
                ArrayType.create(StringType.create()).parse([])
            ).to.be.deep.equal([]);
        });

        it('Should fail for empty array if nonEmpty option', () => {
            expect(
                ArrayType.create(StringType.create()).nonEmpty().safeParse([])
                    .success
            ).to.be.equal(false);
        });
    });

    describe('min, max, length', () => {
        const arraySchema = ArrayType.create(StringType.create());
        it('Should have minimum length', () => {
            expect(arraySchema.min(2).safeParse(['foo']).success).to.be.equal(
                false
            );
            expect(
                arraySchema.min(2).safeParse(['foo', 'bar']).success
            ).to.be.equal(true);
            expect(
                arraySchema.min(2).safeParse(['foo', 'bar', '!']).success
            ).to.be.equal(true);
        });

        it('Should have maximum length', () => {
            expect(arraySchema.max(2).safeParse(['foo']).success).to.be.equal(
                true
            );
            expect(
                arraySchema.max(2).safeParse(['foo', 'bar']).success
            ).to.be.equal(true);
            expect(
                arraySchema.max(2).safeParse(['foo', 'bar', '!']).success
            ).to.be.equal(false);
        });

        it('Should have length', () => {
            expect(
                arraySchema.length(2).safeParse(['foo']).success
            ).to.be.equal(false);
            expect(
                arraySchema.length(2).safeParse(['foo', 'bar']).success
            ).to.be.equal(true);
            expect(
                arraySchema.length(2).safeParse(['foo', 'bar', '!']).success
            ).to.be.equal(false);
        });
    });

    describe('custom message', () => {
        it('Should nonEmpty have custom message', () => {
            const result = ArrayType.create(StringType.create())
                .nonEmpty('Can not be empty!')
                .safeParse([]);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.deep.equal(
                    'Can not be empty!'
                );
            }
        });

        it('Should min have custom message', () => {
            const result = ArrayType.create(StringType.create())
                .min(2, 'Can not be empty!')
                .safeParse(['a']);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.deep.equal(
                    'Can not be empty!'
                );
            }
        });

        it('Should max have custom message', () => {
            const result = ArrayType.create(StringType.create())
                .max(1, 'Can not be empty!')
                .safeParse(['a', 'b']);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.deep.equal(
                    'Can not be empty!'
                );
            }
        });

        it('Should length have custom message', () => {
            const result = ArrayType.create(StringType.create())
                .length(1, 'Can not be empty!')
                .safeParse(['a', 'b']);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.deep.equal(
                    'Can not be empty!'
                );
            }
        });
    });
});
