/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { Validator } from '../Validator';
import { ArrayType } from './Array';
import { ObjectType } from './Object';
import { StringType } from './String';
import { Type, TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Lazy', () => {
    const validator = new Validator();

    interface Category {
        name: string;
        subcategories: Category[];
    }

    const testCategory: Category = {
        name: 'I',
        subcategories: [
            {
                name: 'A',
                subcategories: [
                    {
                        name: '1',
                        subcategories: [
                            {
                                name: 'a',
                                subcategories: [],
                            },
                        ],
                    },
                ],
            },
        ],
    };

    it('Should recursion with Validator.late.object', () => {
        const Category: Type<Category> = validator.late.object(() => ({
            name: StringType.create(),
            subcategories: ArrayType.create(Category),
        }));
        expect(Category.parse(testCategory)).to.be.deep.equal(testCategory);
    });

    it('Should recursion with LazyType', () => {
        const Category: Type<Category> = validator.lazy(() =>
            ObjectType.create({
                name: StringType.create(),
                subcategories: ArrayType.create(Category),
            })
        );
        expect(Category.parse(testCategory)).to.be.deep.equal(testCategory);
    });

    it('Should schema getter', () => {
        expect(
            validator.lazy(() => StringType.create()).schema.parse('asdf')
        ).to.be.equal('asdf');
    });

    it('Should infer LazyType', () => {
        const Category: Type<Category> = validator.lazy(() =>
            ObjectType.create({
                name: StringType.create(),
                subcategories: ArrayType.create(Category),
            })
        );
        type CategoryType = TypeOf<typeof Category>;
        const result: AssertEqual<CategoryType, Category> = true;
        expect(result).to.be.equal(true);
    });
});
