/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { NumberType } from './Number';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Number', () => {
    it('Should parse NumberType string', () => {
        const f = () => NumberType.create().parse('foobar');
        expect(f).to.throw();
    });

    it('Should parse NumberType number', () => {
        expect(NumberType.create().parse(123)).to.be.equal(123);
        expect(NumberType.create().parse(12.3)).to.be.equal(12.3);
    });

    it('Should parse NumberType boolean', () => {
        const f = () => NumberType.create().parse(true);
        expect(f).to.throw();
    });

    it('Should parse NumberType undefined', () => {
        const f = () => NumberType.create().parse(undefined);
        expect(f).to.throw();
    });

    it('Should parse NumberType null', () => {
        const f = () => NumberType.create().parse(null);
        expect(f).to.throw();
    });

    it('Should parse NumberType NaN', () => {
        const f = () => NumberType.create().parse(NaN);
        expect(f).to.throw();
    });

    it('Should check more than one validator', () => {
        expect(
            NumberType.create().int().min(1).safeParse(2).success
        ).to.be.equal(true);
    });

    it('Should check more than one validator and return all errors', () => {
        const result = NumberType.create().int().max(5).safeParse(6.3);
        expect(result.success).to.be.equal(false);
        if (!result.success) {
            expect(result.error.errors[0].message).to.be.equal(
                'Expected integer, received float'
            );
            expect(result.error.errors[1].message).to.be.equal(
                'Number must be less than or equal to 5'
            );
        }
    });

    describe('min', () => {
        it('Should return failing validations', () => {
            expect(NumberType.create().min(2).safeParse(1).success).to.be.equal(
                false
            );
        });

        it('Should return successing validations', () => {
            expect(NumberType.create().min(2).safeParse(3).success).to.be.equal(
                true
            );
            expect(
                NumberType.create().min(2).safeParse(2.00000001).success
            ).to.be.equal(true);
            expect(NumberType.create().min(2).safeParse(2).success).to.be.equal(
                true
            );
        });

        it('Should can return a custom message', () => {
            const myType = NumberType.create().min(5, 'Not more than 5!');

            const result = myType.safeParse(0);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Not more than 5!'
                );
            }
        });
    });

    describe('max', () => {
        it('Should return failing validations', () => {
            expect(NumberType.create().max(4).safeParse(5).success).to.be.equal(
                false
            );
            expect(
                NumberType.create().max(4).safeParse(4.000001).success
            ).to.be.equal(false);
        });

        it('Should return successing validations', () => {
            expect(
                NumberType.create().max(4).safeParse(-3).success
            ).to.be.equal(true);
            expect(
                NumberType.create().max(4).safeParse(3.999999).success
            ).to.be.equal(true);
            expect(NumberType.create().max(4).safeParse(4).success).to.be.equal(
                true
            );
        });

        it('Should can return a custom message', () => {
            const myType = NumberType.create().max(2, 'Not less than 2');

            const result = myType.safeParse(3);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Not less than 2'
                );
            }
        });
    });

    describe('gt', () => {
        it('Should return failing validations', () => {
            expect(NumberType.create().gt(4).safeParse(1).success).to.be.equal(
                false
            );
            expect(
                NumberType.create().gt(4).safeParse(3.99999).success
            ).to.be.equal(false);
            expect(NumberType.create().gt(4).safeParse(4).success).to.be.equal(
                false
            );
        });

        it('Should return successing validations', () => {
            expect(
                NumberType.create().gt(4).safeParse(100).success
            ).to.be.equal(true);
            expect(
                NumberType.create().gt(4).safeParse(4.0000001).success
            ).to.be.equal(true);
        });

        it('Should can return a custom message', () => {
            const myType = NumberType.create().gt(2, 'Oups!');

            const result = myType.safeParse(1);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal('Oups!');
            }
        });
    });

    describe('gte', () => {
        it('Should return failing validations', () => {
            expect(NumberType.create().gte(4).safeParse(1).success).to.be.equal(
                false
            );
            expect(
                NumberType.create().gte(4).safeParse(3.99999).success
            ).to.be.equal(false);
        });

        it('Should return successing validations', () => {
            expect(
                NumberType.create().gte(4).safeParse(100).success
            ).to.be.equal(true);
            expect(
                NumberType.create().gte(4).safeParse(4.0000001).success
            ).to.be.equal(true);
            expect(NumberType.create().gte(4).safeParse(4).success).to.be.equal(
                true
            );
        });

        it('Should can return a custom message', () => {
            const myType = NumberType.create().gt(2, 'Oups!');

            const result = myType.safeParse(1);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal('Oups!');
            }
        });
    });

    describe('lt', () => {
        it('Should return failing validations', () => {
            expect(NumberType.create().lt(4).safeParse(5).success).to.be.equal(
                false
            );
            expect(
                NumberType.create().lt(4).safeParse(4.0000001).success
            ).to.be.equal(false);
            expect(NumberType.create().lt(4).safeParse(5).success).to.be.equal(
                false
            );
        });

        it('Should return successing validations', () => {
            expect(NumberType.create().lt(4).safeParse(3).success).to.be.equal(
                true
            );
            expect(
                NumberType.create().lt(4).safeParse(3.9999).success
            ).to.be.equal(true);
        });

        it('Should can return a custom message', () => {
            const myType = NumberType.create().lt(2, 'Oups!');

            const result = myType.safeParse(3);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal('Oups!');
            }
        });
    });

    describe('lte', () => {
        it('Should return failing validations', () => {
            expect(NumberType.create().lte(4).safeParse(5).success).to.be.equal(
                false
            );
            expect(
                NumberType.create().lte(4).safeParse(4.0000001).success
            ).to.be.equal(false);
        });

        it('Should return successing validations', () => {
            expect(NumberType.create().lte(4).safeParse(3).success).to.be.equal(
                true
            );
            expect(
                NumberType.create().lte(4).safeParse(3.9999).success
            ).to.be.equal(true);
            expect(NumberType.create().lte(4).safeParse(4).success).to.be.equal(
                true
            );
        });

        it('Should can return a custom message', () => {
            const myType = NumberType.create().lte(2, 'Oups!');

            const result = myType.safeParse(3);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal('Oups!');
            }
        });
    });

    describe('int', () => {
        it('Should return failing validations', () => {
            expect(
                NumberType.create().int().safeParse(1.1).success
            ).to.be.equal(false);
        });

        it('Should return successing validations', () => {
            expect(NumberType.create().int().safeParse(-3).success).to.be.equal(
                true
            );
            expect(NumberType.create().int().safeParse(2).success).to.be.equal(
                true
            );
        });

        it('Should can return a custom message', () => {
            const myType = NumberType.create().int('Not integer!');

            const result = myType.safeParse(0.1);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Not integer!'
                );
            }
        });
    });

    describe('positive', () => {
        it('Should return failing validations', () => {
            expect(
                NumberType.create().positive().safeParse(-1).success
            ).to.be.equal(false);
            expect(
                NumberType.create().positive().safeParse(-0.000001).success
            ).to.be.equal(false);
            expect(
                NumberType.create().positive().safeParse(0).success
            ).to.be.equal(false);
        });

        it('Should return successing validations', () => {
            expect(
                NumberType.create().positive().safeParse(1).success
            ).to.be.equal(true);
            expect(
                NumberType.create().positive().safeParse(0.0000001).success
            ).to.be.equal(true);
        });

        it('Should can return a custom message', () => {
            const myType = NumberType.create().positive('Not positive!');

            const result = myType.safeParse(-3);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Not positive!'
                );
            }
        });
    });

    describe('nonPositive', () => {
        it('Should return failing validations', () => {
            expect(
                NumberType.create().nonPositive().safeParse(-1).success
            ).to.be.equal(false);
            expect(
                NumberType.create().nonPositive().safeParse(-0.000001).success
            ).to.be.equal(false);
        });

        it('Should return successing validations', () => {
            expect(
                NumberType.create().nonPositive().safeParse(1).success
            ).to.be.equal(true);
            expect(
                NumberType.create().nonPositive().safeParse(0.0000001).success
            ).to.be.equal(true);
            expect(
                NumberType.create().nonPositive().safeParse(0).success
            ).to.be.equal(true);
        });

        it('Should can return a custom message', () => {
            const myType = NumberType.create().nonPositive('Not positive!');

            const result = myType.safeParse(-3);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Not positive!'
                );
            }
        });
    });

    describe('negative', () => {
        it('Should return failing validations', () => {
            expect(
                NumberType.create().negative().safeParse(1).success
            ).to.be.equal(false);
            expect(
                NumberType.create().negative().safeParse(0.000001).success
            ).to.be.equal(false);
            expect(
                NumberType.create().negative().safeParse(0).success
            ).to.be.equal(false);
        });

        it('Should return successing validations', () => {
            expect(
                NumberType.create().negative().safeParse(-1).success
            ).to.be.equal(true);
            expect(
                NumberType.create().negative().safeParse(-0.00001).success
            ).to.be.equal(true);
        });

        it('Should can return a custom message', () => {
            const myType = NumberType.create().negative('Not negative!');

            const result = myType.safeParse(3);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Not negative!'
                );
            }
        });
    });

    describe('nonNegative', () => {
        it('Should return failing validations', () => {
            expect(
                NumberType.create().nonNegative().safeParse(1).success
            ).to.be.equal(false);
            expect(
                NumberType.create().nonNegative().safeParse(0.000001).success
            ).to.be.equal(false);
        });

        it('Should return successing validations', () => {
            expect(
                NumberType.create().nonNegative().safeParse(-1).success
            ).to.be.equal(true);
            expect(
                NumberType.create().nonNegative().safeParse(-0.00001).success
            ).to.be.equal(true);
            expect(
                NumberType.create().nonNegative().safeParse(0).success
            ).to.be.equal(true);
        });

        it('Should can return a custom message', () => {
            const myType = NumberType.create().nonNegative('Not negative!');

            const result = myType.safeParse(3);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Not negative!'
                );
            }
        });
    });

    describe('multipleOf', () => {
        it('Should return failing validations', () => {
            expect(
                NumberType.create().multipleOf(4).safeParse(1).success
            ).to.be.equal(false);
            expect(
                NumberType.create().multipleOf(4).safeParse(13).success
            ).to.be.equal(false);
        });

        it('Should return successing validations', () => {
            expect(
                NumberType.create().multipleOf(4).safeParse(0).success
            ).to.be.equal(true);
            expect(
                NumberType.create().multipleOf(4).safeParse(4).success
            ).to.be.equal(true);
            expect(
                NumberType.create().multipleOf(4).safeParse(16).success
            ).to.be.equal(true);
            expect(
                NumberType.create().multipleOf(0.0001).safeParse(3.01).success
            ).to.be.equal(true);
        });

        it('Should can return a custom message', () => {
            const myType = NumberType.create().multipleOf(
                2,
                'Not multiple of 2!'
            );

            const result = myType.safeParse(3);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Not multiple of 2!'
                );
            }
        });
    });

    describe('Custom messages', () => {
        it('Should can return a custom message', () => {
            const age = NumberType.create({
                requiredError: 'Age is required',
                invalidTypeError: 'Age must be a number',
            });

            // TODO test requiredError

            const result = age.safeParse(null);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Age must be a number'
                );
            }
        });
    });

    describe('Getters', () => {
        it('Should have isInt getter', () => {
            expect(NumberType.create().int().isInt).to.be.equal(true);
            expect(NumberType.create().isInt).to.be.equal(false);
        });

        it('Should have minValue getter', () => {
            expect(NumberType.create().min(2).minValue).to.be.equal(2);
            expect(NumberType.create().min(2).min(4).minValue).to.be.equal(4);
        });

        it('Should have maxValue getter', () => {
            expect(NumberType.create().max(4).maxValue).to.be.equal(4);
            expect(NumberType.create().max(4).max(2).maxValue).to.be.equal(2);
        });
    });

    it('Should infer NumberType', () => {
        const MyNumber = NumberType.create();
        type MyNumberType = TypeOf<typeof MyNumber>;
        const result: AssertEqual<MyNumberType, number> = true;
        expect(result).to.be.equal(true);
    });
});
