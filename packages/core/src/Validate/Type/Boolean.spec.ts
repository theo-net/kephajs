/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { BooleanType } from './Boolean';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Boolean', () => {
    it('Should parse BooleanType string', () => {
        let f = () => BooleanType.create().parse('');
        expect(f).to.throw();
        f = () => BooleanType.create().parse('foobar');
        expect(f).to.throw();
    });

    it('Should parse BooleanType number', () => {
        let f = () => BooleanType.create().parse(0);
        expect(f).to.throw();
        f = () => BooleanType.create().parse(1);
        expect(f).to.throw();
    });

    it('Should parse BooleanType boolean', () => {
        expect(BooleanType.create().parse(true)).to.be.equal(true);
        expect(BooleanType.create().parse(false)).to.be.equal(false);
    });

    it('Should parse BooleanType undefined', () => {
        const f = () => BooleanType.create().parse(undefined);
        expect(f).to.throw();
    });

    it('Should parse BooleanType null', () => {
        const f = () => BooleanType.create().parse(null);
        expect(f).to.throw();
    });

    it('Should infer BooleanType', () => {
        const MyBoolean = BooleanType.create();
        type MyBooleanType = TypeOf<typeof MyBoolean>;
        const result: AssertEqual<MyBooleanType, boolean> = true;
        expect(result).to.be.equal(true);
    });
});
