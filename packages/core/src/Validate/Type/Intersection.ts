/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import {
    FirstPartyTypeKind,
    RawCreateParams,
    Type,
    TypeAny,
    TypeDef,
} from './Type';

import {
    INVALID,
    ParseInput,
    ParseReturnType,
    ParsedType,
    isAborted,
    isDirty,
    getParsedType,
} from '../helpers/parseUtils';
import { objectKeys } from '../../Helpers/Helpers';

export type IntersectionDef<
    T extends TypeAny = TypeAny,
    U extends TypeAny = TypeAny
> = {
    typeName: FirstPartyTypeKind.Intersection;
    left: T;
    right: U;
} & TypeDef;

export class Intersection<
    T extends TypeAny = TypeAny,
    U extends TypeAny = TypeAny
> extends Type<
    T['_output'] & U['_output'],
    IntersectionDef<T, U>,
    T['_input'] & U['_input']
> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const { status, ctx } = this._processInputParams(input);

        const parsedLeft = this._def.left._parse({
                data: ctx.data,
                path: ctx.path,
                parent: ctx,
            }),
            parsedRight = this._def.right._parse({
                data: ctx.data,
                path: ctx.path,
                parent: ctx,
            });

        if (isAborted(parsedLeft) || isAborted(parsedRight)) {
            return INVALID;
        }

        const merged = this._mergeValues(parsedLeft.value, parsedRight.value);

        if (!merged.valid) {
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidIntersectionTypes,
            });
            return INVALID;
        }

        if (isDirty(parsedLeft) || isDirty(parsedRight)) {
            status.dirty();
        }

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        return { status: status.value, value: merged.data as any };
    }

    static create<T extends TypeAny, U extends TypeAny>(
        left: T,
        right: U,
        params?: RawCreateParams
    ): Intersection<T, U> {
        return new Intersection({
            typeName: FirstPartyTypeKind.Intersection,
            left,
            right,
            ...Intersection.processCreateParams(params),
        });
    }

    get left() {
        return this._def.left;
    }

    get right() {
        return this._def.right;
    }

    protected _mergeValues(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        a: any,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        b: any
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ): { valid: true; data: any } | { valid: false } {
        const aType = getParsedType(a);
        const bType = getParsedType(b);

        if (a === b) {
            return { valid: true, data: a };
        } else if (aType === ParsedType.object && bType === ParsedType.object) {
            const bKeys = objectKeys(b);
            const sharedKeys = objectKeys(a).filter(
                key => bKeys.indexOf(key) !== -1
            );

            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const newObj: any = { ...a, ...b };
            for (const key of sharedKeys) {
                const sharedValue = this._mergeValues(a[key], b[key]);
                if (!sharedValue.valid) {
                    return { valid: false };
                }
                newObj[key] = sharedValue.data;
            }

            return { valid: true, data: newObj };
        } else if (aType === ParsedType.array && bType === ParsedType.array) {
            if (a.length !== b.length) {
                return { valid: false };
            }

            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const newArray: any[] = [];
            for (let index = 0; index < a.length; index++) {
                const itemA = a[index];
                const itemB = b[index];
                const sharedValue = this._mergeValues(itemA, itemB);

                if (!sharedValue.valid) {
                    return { valid: false };
                }

                newArray.push(sharedValue.data);
            }

            return { valid: true, data: newArray };
        } else if (
            aType === ParsedType.date &&
            bType === ParsedType.date &&
            +a === +b
        ) {
            return { valid: true, data: a };
        } else {
            return { valid: false };
        }
    }
}
