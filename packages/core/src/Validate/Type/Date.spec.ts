/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { DateType } from './Date';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Date', () => {
    it('Should parse DateType string', () => {
        const f = () => DateType.create().parse('foobar');
        expect(f).to.throw();
    });

    it('Should parse DateType number', () => {
        const f = () => DateType.create().parse(123);
        expect(f).to.throw();
    });

    it('Should parse DateType boolean', () => {
        const f = () => DateType.create().parse(true);
        expect(f).to.throw();
    });

    it('Should parse DateType undefined', () => {
        const f = () => DateType.create().parse(undefined);
        expect(f).to.throw();
    });

    it('Should parse DateType null', () => {
        const f = () => DateType.create().parse(null);
        expect(f).to.throw();
    });

    it('Should parse DateType Date', () => {
        const date = new Date();
        expect(DateType.create().parse(date)).to.be.equal(date);
    });

    it('Should not parse string date', () => {
        const f = () => DateType.create().parse('2022-05-24T00:00:00.000Z');
        expect(f).to.throw();
    });

    it('Should return invalid date error', () => {
        const result = DateType.create().safeParse(new Date('invalid'));
        expect(result.success).to.be.equal(false);
        if (!result.success) {
            expect(result.error.errors[0].code).to.be.equal('invalidDate');
        }
    });

    it('Should infer DateType', () => {
        const MyDate = DateType.create();
        type MyDateType = TypeOf<typeof MyDate>;
        const result: AssertEqual<MyDateType, Date> = true;
        expect(result).to.be.equal(true);
    });
});
