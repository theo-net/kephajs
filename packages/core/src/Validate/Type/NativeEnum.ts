/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import { FirstPartyTypeKind, RawCreateParams, Type, TypeDef } from './Type';
import { joinValues, objectKeys } from '../../Helpers/Helpers';

import {
    INVALID,
    OK,
    ParseInput,
    ParseReturnType,
    ParsedType,
} from '../helpers/parseUtils';

type EnumLike = { [k: string]: string | number; [nu: number]: string };
export type NativeEnumDef<T extends EnumLike = EnumLike> = {
    typeName: FirstPartyTypeKind.NativeEnum;
    values: T;
} & TypeDef;

export class NativeEnumType<T extends EnumLike> extends Type<
    T[keyof T],
    NativeEnumDef<T>
> {
    _parse(input: ParseInput): ParseReturnType<T[keyof T]> {
        const nativeEnumValues = this._getValidEnumValues(this._def.values);
        const ctx = this._getOrReturnCtx(input);

        if (
            ctx.parsedType !== ParsedType.string &&
            ctx.parsedType !== ParsedType.number
        ) {
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: joinValues(
                    this._objectValues(nativeEnumValues)
                ) as 'string',
                received: ctx.parsedType,
            });
            return INVALID;
        }

        if (nativeEnumValues.indexOf(input.data) === -1) {
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidEnumValue,
                options: this._objectValues(nativeEnumValues),
                received: ctx.data,
            });
            return INVALID;
        }

        return OK(input.data);
    }

    static create<U extends EnumLike>(values: U, params?: RawCreateParams) {
        return new NativeEnumType<U>({
            typeName: FirstPartyTypeKind.NativeEnum,
            values,
            ...this.processCreateParams(params),
        });
    }

    /**
     * Return the underlying object
     */
    get enum() {
        return this._def.values;
    }

    /**
     * Return valid enum values
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected _getValidEnumValues(obj: any) {
        const validKeys = objectKeys(obj).filter(
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            (k: any) => typeof obj[obj[k]] !== 'number'
        );
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const filtered: any = {};
        for (const k of validKeys) {
            filtered[k] = obj[k];
        }
        return this._objectValues(filtered);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected _objectValues(obj: any) {
        return objectKeys(obj).map(function (e) {
            return obj[e];
        });
    }
}
