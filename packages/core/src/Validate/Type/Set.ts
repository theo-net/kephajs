/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import {
    FirstPartyTypeKind,
    RawCreateParams,
    Type,
    TypeAny,
    TypeDef,
} from './Type';

import {
    INVALID,
    ParseInput,
    ParseReturnType,
    ParsedType,
    ParseInputLazyPath,
} from '../helpers/parseUtils';

export type SetDef<Value extends TypeAny = TypeAny> = {
    typeName: FirstPartyTypeKind.SetType;
    valueType: Value;
    minSize: { value: number; message?: string } | null;
    maxSize: { value: number; message?: string } | null;
} & TypeDef;

/**
 * Type `Set` validation
 */
export class SetType<Value extends TypeAny = TypeAny> extends Type<
    Set<Value['_output']>,
    SetDef<Value>,
    Set<Value['_input']>
> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const { status, ctx } = this._processInputParams(input);

        if (ctx.parsedType !== ParsedType.set) {
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: ParsedType.set,
                received: ctx.parsedType,
            });
            return INVALID;
        }

        if (this._def.minSize !== null) {
            if (ctx.data.size < this._def.minSize.value) {
                this._addIssueToContext(ctx, {
                    code: IssueCode.tooSmall,
                    minimum: this._def.minSize.value,
                    type: 'set',
                    inclusive: true,
                    message: this._def.minSize.message,
                });
                status.dirty();
            }
        }

        if (this._def.maxSize !== null) {
            if (ctx.data.size > this._def.maxSize.value) {
                this._addIssueToContext(ctx, {
                    code: IssueCode.tooBig,
                    maximum: this._def.maxSize.value,
                    type: 'set',
                    inclusive: true,
                    message: this._def.maxSize.message,
                });
                status.dirty();
            }
        }

        const elements = [...(ctx.data as Set<unknown>).values()].map(
            (item, i) =>
                this._def.valueType._parse(
                    new ParseInputLazyPath(ctx, item, ctx.path, i)
                )
        );

        const parsedSet = new Set();
        for (const element of elements) {
            if (element.status === 'aborted') return INVALID;
            if (element.status === 'dirty') status.dirty();
            parsedSet.add(element.value);
        }
        return { status: status.value, value: parsedSet };
    }

    static create<V extends TypeAny = TypeAny>(
        valueType: V,
        params?: RawCreateParams
    ): SetType<V> {
        return new SetType({
            typeName: FirstPartyTypeKind.SetType,
            valueType,
            minSize: null,
            maxSize: null,
            ...SetType.processCreateParams(params),
        });
    }

    /**
     * Define minimum size
     */
    min(minSize: number, message?: string | { message: string }): this {
        return new SetType({
            ...this._def,
            minSize: {
                value: minSize,
                message:
                    typeof message === 'string' ? message : message?.message,
            },
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        }) as any;
    }

    /**
     * Define maximum size
     */
    max(maxSize: number, message?: string | { message: string }): this {
        return new SetType({
            ...this._def,
            maxSize: {
                value: maxSize,
                message:
                    typeof message === 'string' ? message : message?.message,
            },
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        }) as any;
    }

    /**
     * Define size
     */
    size(size: number, message?: string | { message: string }) {
        return this.min(size, message).max(size, message);
    }

    /**
     * Cannot be empty
     */
    nonempty(message?: string | { message: string }) {
        return this.min(1, message);
    }
}
