/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { AnyType } from './Any';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Any', () => {
    it('Should parse AnyType string', () => {
        expect(AnyType.create().parse('foobar')).to.be.equal('foobar');
    });

    it('Should parse AnyType number', () => {
        expect(AnyType.create().parse(123)).to.be.equal(123);
    });

    it('Should parse AnyType boolean', () => {
        expect(AnyType.create().parse(true)).to.be.equal(true);
    });

    it('Should parse AnyType undefined', () => {
        expect(AnyType.create().parse(undefined)).to.be.equal(undefined);
    });

    it('Should parse AnyType null', () => {
        expect(AnyType.create().parse(null)).to.be.equal(null);
    });

    it('Should parse AnyType object', () => {
        const object = { a: 1, b: 2 };
        expect(AnyType.create().parse(object)).to.be.deep.equal(object);
    });

    it('Should infer AnyType', () => {
        const MyAny = AnyType.create();
        type MyAnyType = TypeOf<typeof MyAny>;
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const result: AssertEqual<MyAnyType, any> = true;
        expect(result).to.be.equal(true);
    });
});
