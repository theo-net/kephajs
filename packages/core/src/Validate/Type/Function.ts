/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ErrorValidator, Issue, IssueCode } from '../Error/Error';
import {
    FirstPartyTypeKind,
    RawCreateParams,
    Type,
    TypeAny,
    TypeDef,
} from './Type';

import {
    INVALID,
    OK,
    ParseInput,
    ParseReturnType,
    ParsedType,
    ParseContext,
} from '../helpers/parseUtils';
import { TupleType } from './Tuple';
import { UnknownType } from './Unknown';
import { defaultErrorMap, ErrorMap, overrideErrorMap } from '../Error/ErrorMap';

export type FunctionDef<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Args extends TupleType<any, any> = TupleType<any, any>,
    Returns extends TypeAny = TypeAny
> = {
    typeName: FirstPartyTypeKind.FunctionType;
    args: Args;
    returns: Returns;
} & TypeDef;

type OuterTypeOfFunction<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Args extends TupleType<any, any>,
    Returns extends TypeAny
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
> = Args['_input'] extends Array<any>
    ? (...args: Args['_input']) => Returns['_output']
    : never;

type InnerTypeOfFunction<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Args extends TupleType<any, any>,
    Returns extends TypeAny
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
> = Args['_output'] extends Array<any>
    ? (...args: Args['_output']) => Returns['_input']
    : never;

export class FunctionType<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Args extends TupleType<any, any>,
    Returns extends TypeAny
> extends Type<
    OuterTypeOfFunction<Args, Returns>,
    FunctionDef<Args, Returns>,
    InnerTypeOfFunction<Args, Returns>
> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    _parse(input: ParseInput): ParseReturnType<any> {
        const { ctx } = this._processInputParams(input);
        if (ctx.parsedType !== ParsedType.function) {
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: ParsedType.function,
                received: ctx.parsedType,
            });
            return INVALID;
        }

        const params = { errorMap: ctx.common.contextualErrorMap };
        const fn = ctx.data;

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        return OK((...args: any[]) => {
            const parsedArgs = this._def.args.safeParse(args, params);
            if (!parsedArgs.success) {
                throw new ErrorValidator([
                    this._makeArgsIssue(ctx, args, parsedArgs.error),
                ]);
            }
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const result = fn(...(parsedArgs.data as any));
            const parsedReturns = this._def.returns.safeParse(result, params);
            if (!parsedReturns.success) {
                throw new ErrorValidator([
                    this._makeReturnsIssue(ctx, result, parsedReturns.error),
                ]);
            }
            return parsedReturns.data;
        });
    }

    static create<
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        Args extends TupleType<any, any> = TupleType<[], UnknownType>,
        Returns extends TypeAny = UnknownType
    >(
        args?: Args,
        returns?: Returns,
        params?: RawCreateParams
    ): FunctionType<Args, Returns> {
        return new FunctionType({
            typeName: FirstPartyTypeKind.FunctionType,
            args: (args
                ? args.rest(UnknownType.create())
                : // eslint-disable-next-line @typescript-eslint/no-explicit-any
                  TupleType.create([]).rest(UnknownType.create())) as any,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            returns: (returns || UnknownType.create()) as any,
            ...FunctionType.processCreateParams(params),
        });
    }

    args<Items extends Parameters<typeof TupleType['create']>[0]>(
        ...items: Items
    ): FunctionType<TupleType<Items, UnknownType>, Returns> {
        return new FunctionType({
            ...this._def,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            args: TupleType.create(items).rest(UnknownType.create()) as any,
        });
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    returns<NewReturnType extends Type<any, any>>(
        returnType: NewReturnType
    ): FunctionType<Args, NewReturnType> {
        return new FunctionType({
            ...this._def,
            returns: returnType,
        });
    }

    implement<F extends InnerTypeOfFunction<Args, Returns>>(func: F): F {
        const validatedFunc = this.parse(func);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        return validatedFunc as any;
    }

    parameters() {
        return this._def.args;
    }

    returnType() {
        return this._def.returns;
    }

    protected _makeArgsIssue(
        ctx: ParseContext,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        args: any,
        error: ErrorValidator
    ): Issue {
        return this._makeIssue({
            data: args,
            path: ctx.path,
            errorMaps: [
                ctx.common.contextualErrorMap,
                ctx.schemaErrorMap,
                overrideErrorMap,
                defaultErrorMap,
            ].filter(x => !!x) as ErrorMap[],
            issueData: {
                code: IssueCode.invalidArguments,
                argumentsError: error,
            },
        });
    }

    protected _makeReturnsIssue(
        ctx: ParseContext,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        returns: any,
        error: ErrorValidator
    ): Issue {
        return this._makeIssue({
            data: returns,
            path: ctx.path,
            errorMaps: [
                ctx.common.contextualErrorMap,
                ctx.schemaErrorMap,
                overrideErrorMap,
                defaultErrorMap,
            ].filter(x => !!x) as ErrorMap[],
            issueData: {
                code: IssueCode.invalidReturnType,
                returnTypeError: error,
            },
        });
    }
}
