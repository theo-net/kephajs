/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { Default } from './Default';
import { Effects } from './Effects';
import { ObjectType } from './Object';
import { Optional } from './Optional';
import { StringType } from './String';
import { InputType, OutputType, Type } from './Type';

describe('@kephajs/core/Validate/Type/Default', () => {
    it('Should define basic defaults', () => {
        expect(
            StringType.create().default('default').parse(undefined)
        ).to.be.equal('default');
    });

    it('Should define default with transform', () => {
        const stringWithDefault = StringType.create()
            .transform(val => val.toUpperCase())
            .default('default');
        expect(stringWithDefault.parse(undefined)).to.be.equal('DEFAULT');
        expect(stringWithDefault).to.be.instanceOf(Default);
        expect(stringWithDefault._def.innerType).to.be.instanceOf(Effects);
        expect(stringWithDefault._def.innerType._def.schema).to.be.instanceOf(
            Type
        );

        type Inp = InputType<typeof stringWithDefault>;
        const f1: AssertEqual<Inp, string | undefined> = true;
        type Out = OutputType<typeof stringWithDefault>;
        const f2: AssertEqual<Out, string> = true;
        expect([f1, f2]).to.be.deep.equal([true, true]);
    });

    it('Should define default on existing optional', () => {
        const stringWithDefault = StringType.create()
            .optional()
            .default('asdf');
        expect(stringWithDefault.parse(undefined)).to.be.equal('asdf');
        expect(stringWithDefault).to.be.instanceOf(Default);
        expect(stringWithDefault._def.innerType).to.be.instanceOf(Optional);
        expect(
            stringWithDefault._def.innerType._def.innerType
        ).to.be.instanceOf(StringType);

        type Inp = InputType<typeof stringWithDefault>;
        const f1: AssertEqual<Inp, string | undefined> = true;
        type Out = OutputType<typeof stringWithDefault>;
        const f2: AssertEqual<Out, string> = true;
        expect([f1, f2]).to.be.deep.equal([true, true]);
    });

    it('Should define optional on default', () => {
        const stringWithDefault = StringType.create()
            .default('asdf')
            .optional();

        type Inp = InputType<typeof stringWithDefault>;
        const f1: AssertEqual<Inp, string | undefined> = true;
        type Out = OutputType<typeof stringWithDefault>;
        const f2: AssertEqual<Out, string | undefined> = true;
        expect([f1, f2]).to.be.deep.equal([true, true]);
    });

    it('Should complex chain example', () => {
        const complex = StringType.create()
            .default('asdf')
            .transform((val: string) => val.toUpperCase())
            .default('qwer')
            .removeDefault()
            .optional()
            .default('asdfasdf');

        expect(complex.parse(undefined)).to.be.equal('ASDFASDF');
    });

    it('Should define removeDefault', () => {
        const stringWithRemovedDefault = StringType.create()
            .default('asdf')
            .removeDefault();

        type Out = OutputType<typeof stringWithRemovedDefault>;
        const f2: AssertEqual<Out, string> = true;
        expect(f2).to.be.equal(true);
    });

    it('Should define nested', () => {
        const inner = StringType.create().default('asdf');
        const outer = ObjectType.create({ inner }).default({
            inner: undefined,
        });
        type Inp = InputType<typeof outer>;
        const f1: AssertEqual<Inp, { inner?: string | undefined } | undefined> =
            true;
        type Out = OutputType<typeof outer>;
        const f2: AssertEqual<Out, { inner: string }> = true;
        expect([f1, f2]).to.be.deep.equal([true, true]);
        expect(outer.parse(undefined)).to.be.deep.equal({ inner: 'asdf' });
        expect(outer.parse({})).to.be.deep.equal({ inner: 'asdf' });
        expect(outer.parse({ inner: undefined })).to.be.deep.equal({
            inner: 'asdf',
        });
    });

    it('Should define chained defaults', () => {
        const stringWithDefault = StringType.create()
            .default('inner')
            .default('outer');
        const result = stringWithDefault.parse(undefined);
        expect(result).to.be.equal('outer');
    });

    it('Should define factory', () => {
        Default.create(StringType.create()).parse(undefined);
    });
});
