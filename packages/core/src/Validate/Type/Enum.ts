/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import { FirstPartyTypeKind, Type, TypeDef } from './Type';
import { joinValues } from '../../Helpers/Helpers';

import {
    INVALID,
    OK,
    ParseInput,
    ParseReturnType,
    ParsedType,
} from '../helpers/parseUtils';

type EnumValues = [string, ...string[]];
type Values<T extends EnumValues> = {
    [k in T[number]]: k;
};

export type EnumDef<T extends EnumValues = EnumValues> = {
    typeName: FirstPartyTypeKind.Enum;
    values: T;
} & TypeDef;

type Writeable<T> = { -readonly [P in keyof T]: T[P] };

/**
 * It's a framework-native way to declare a schema with a fixed set of allowable string
 * values. Pass the array of values directly into validator.enum().
 */
export class EnumType<T extends EnumValues> extends Type<
    T[number],
    EnumDef<T>
> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const parsedType = this._getType(input);
        if (parsedType !== ParsedType.string) {
            const ctx = this._getOrReturnCtx(input);
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: joinValues(this.options) as 'string',
                received: ctx.parsedType,
            });
            return INVALID;
        }

        if (this.options.indexOf(input.data) === -1) {
            const ctx = this._getOrReturnCtx(input);
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidEnumValue,
                options: this.options,
                received: ctx.data,
            });
            return INVALID;
        }

        return OK(input.data);
    }

    static create<U extends string, T extends Readonly<[U, ...U[]]>>(
        values: T
    ): EnumType<Writeable<T>>;
    static create<U extends string, T extends [U, ...U[]]>(
        values: T
    ): EnumType<T>;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    static create(values: any) {
        return new EnumType({
            typeName: FirstPartyTypeKind.Enum,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            values: values as any,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        }) as any;
    }

    /**
     * Return the values
     */
    get enum(): Values<T> {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const values: any = {};
        for (const value of this._def.values) {
            values[value] = value;
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        return values as any;
    }

    /**
     * Return the list of options as a tuple
     */
    get options() {
        return this._def.values;
    }
}
