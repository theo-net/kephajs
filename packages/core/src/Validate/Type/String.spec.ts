/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { ErrorValidator } from '../Error/Error';
import { AssertEqual } from '../helpers/testUtils';
import { StringType } from './String';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/String', () => {
    it('Should parse StringType string', () => {
        expect(StringType.create().parse('foobar')).to.be.equal('foobar');
    });

    it('Should parse StringType number', () => {
        const f = () => StringType.create().parse(123);
        expect(f).to.throw();
    });

    it('Should parse StringType boolean', () => {
        const f = () => StringType.create().parse(true);
        expect(f).to.throw();
    });

    it('Should parse StringType undefined', () => {
        const f = () => StringType.create().parse(undefined);
        expect(f).to.throw();
    });

    it('Should parse StringType null', () => {
        const f = () => StringType.create().parse(null);
        expect(f).to.throw();
    });

    it('Should be valid for empty string', () => {
        expect(StringType.create().parse('')).to.be.equal('');
    });

    it('Should check more than one validator', () => {
        expect(
            StringType.create().email().min(1).safeParse('foo@bar.com').success
        ).to.be.equal(true);
    });

    it('Should check more than one validator and return all errors', () => {
        const result = StringType.create().email().max(5).safeParse('foobar');
        expect(result.success).to.be.equal(false);
        if (!result.success) {
            expect(result.error.errors[0].message).to.be.equal('Invalid email');
            expect(result.error.errors[1].message).to.be.equal(
                'String must contain at most 5 character(s)'
            );
        }
    });

    describe('min', () => {
        it('Should return failing validations', () => {
            expect(
                StringType.create().min(2).safeParse('').success
            ).to.be.equal(false);
            expect(
                StringType.create().min(2).safeParse('a').success
            ).to.be.equal(false);
        });

        it('Should return successing validations', () => {
            expect(
                StringType.create().min(2).safeParse('ab').success
            ).to.be.equal(true);
            expect(
                StringType.create().min(2).safeParse('abc').success
            ).to.be.equal(true);
        });

        it('Should can return a custom message', () => {
            const myType = StringType.create().min(
                5,
                'Must be 5 or more characters long'
            );

            const result = myType.safeParse('a');
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Must be 5 or more characters long'
                );
            }
        });
    });

    describe('max', () => {
        it('Should return failing validations', () => {
            expect(
                StringType.create().max(4).safeParse('12345').success
            ).to.be.equal(false);
            expect(
                StringType.create().max(4).safeParse('123456').success
            ).to.be.equal(false);
        });

        it('Should return successing validations', () => {
            expect(
                StringType.create().max(4).safeParse('123').success
            ).to.be.equal(true);
            expect(
                StringType.create().max(4).safeParse('1234').success
            ).to.be.equal(true);
        });

        it('Should can return a custom message', () => {
            const myType = StringType.create().max(
                2,
                'Must be 2 or fewer characters long'
            );

            const result = myType.safeParse('foobar');
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Must be 2 or fewer characters long'
                );
            }
        });
    });

    describe('length', () => {
        it('Should return failing validations', () => {
            expect(
                StringType.create().length(2).safeParse('1').success
            ).to.be.equal(false);
            expect(
                StringType.create().length(2).safeParse('123').success
            ).to.be.equal(false);
        });

        it('Should return successing validations', () => {
            expect(
                StringType.create().length(2).safeParse('12').success
            ).to.be.equal(true);
        });

        it('Should can return a custom message', () => {
            const myType = StringType.create().length(
                5,
                'Must be exactly 6 characters long'
            );

            const result = myType.safeParse('foobar');
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Must be exactly 6 characters long'
                );
            }
        });
    });

    describe('email validation', () => {
        it('Should return failing validations', () => {
            expect(() =>
                StringType.create().email().parse('foobar')
            ).to.throw();
            expect(() =>
                StringType.create().email().parse('@foobar.com')
            ).to.throw();
        });

        it('Should return successing validations', () => {
            const data = [
                `"josé.arrañoça"@domain.com`,
                `"сайт"@domain.com`,
                `"💩"@domain.com`,
                `"🍺🕺🎉"@domain.com`,
                `poop@💩.la`,
                `"🌮"@i❤️tacos.ws`,
            ];
            const email = StringType.create().email();
            for (const datum of data) {
                expect(email.parse(datum)).to.be.equal(datum);
            }
        });

        it('Should can return a custom message', () => {
            const myType = StringType.create().email('Invalid email address!');

            const result = myType.safeParse('foobar');
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Invalid email address!'
                );
            }
        });
    });

    describe('url validation', () => {
        it('Should return failing validations', () => {
            expect(() => StringType.create().url().parse('foobar')).to.throw();
            expect(() =>
                StringType.create().url().parse('https://')
            ).to.throw();
            expect(() =>
                StringType.create().url().parse('foo@bar.com')
            ).to.throw();
        });

        it('Should return successing validations', () => {
            const data = [
                'http://duckduckgo.com',
                'http://duckduckgo.com/asdf?asdf=ljk3lk4&asdf=234#asdf',
            ];
            const url = StringType.create().url();
            for (const datum of data) {
                expect(url.parse(datum)).to.be.equal(datum);
            }
        });

        it('Should override error', () => {
            try {
                StringType.create().url().parse('https');
            } catch (err) {
                expect((err as ErrorValidator).errors[0].message).to.be.equal(
                    'Invalid url'
                );
            }
            try {
                StringType.create().url('badurl').parse('https');
            } catch (err) {
                expect((err as ErrorValidator).errors[0].message).to.be.equal(
                    'badurl'
                );
            }
            try {
                StringType.create().url({ message: 'badurl' }).parse('https');
            } catch (err) {
                expect((err as ErrorValidator).errors[0].message).to.be.equal(
                    'badurl'
                );
            }
        });
    });

    describe('UUID validation', () => {
        it('Should return failing validations', () => {
            const uuid = StringType.create().uuid('custom error');

            const result = uuid.safeParse(
                '9491d710-3185-4e06-bea0-6a2f275345e0X'
            );
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'custom error'
                );
            }

            expect(uuid.safeParse('foobar').success).to.be.equal(false);
        });

        it('Should return successing validations', () => {
            const data = [
                '9491d710-3185-4e06-bea0-6a2f275345e0',
                '00000000-0000-0000-0000-000000000000',
                'b3ce60f8-e8b9-40f5-1150-172ede56ff74', // Variant 0 - RFC 4122: Reserved, NCS backward compatibility
                '92e76bf9-28b3-4730-cd7f-cb6bc51f8c09', // Variant 2 - RFC 4122: Reserved, Microsoft Corporation backward compatibility
            ];
            const uuid = StringType.create().uuid();
            for (const datum of data) {
                expect(uuid.parse(datum)).to.be.equal(datum);
            }
        });
    });

    describe('CUID validation', () => {
        it('Should return failing validations', () => {
            const result = StringType.create().cuid().safeParse('foobar');
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Invalid cuid'
                );
            }
        });

        it('Should return successing validations', () => {
            expect(
                StringType.create()
                    .cuid()
                    .safeParse('ckopqwooh000001la8mbi2im9').success
            ).to.be.equal(true);
        });

        it('Should can return a custom message', () => {
            const myType = StringType.create().cuid('Invalid cuid!');

            const result = myType.safeParse('foobar');
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Invalid cuid!'
                );
            }
        });
    });

    describe('RegExp validation', () => {
        it('Should return failing validations', () => {
            expect(
                StringType.create()
                    .regexp(/^moo+$/)
                    .safeParse('looooo').success
            ).to.be.equal(false);
        });

        it('Should return successing validations', () => {
            expect(
                StringType.create()
                    .regexp(/^moo+$/)
                    .safeParse('mooooo').success
            ).to.be.equal(true);
        });
    });

    describe('Custom messages', () => {
        it('Should can return a custom message', () => {
            const name = StringType.create({
                requiredError: 'Name is required',
                invalidTypeError: 'Name must be a string',
            });

            // TODO test requiredError

            const result = name.safeParse(null);
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].message).to.be.equal(
                    'Name must be a string'
                );
            }
        });
    });

    describe('Getters', () => {
        it('Should have isEmail getter', () => {
            expect(StringType.create().email().isEmail).to.be.equal(true);
            expect(StringType.create().url().isEmail).to.be.equal(false);
            expect(StringType.create().uuid().isEmail).to.be.equal(false);
            expect(StringType.create().cuid().isEmail).to.be.equal(false);
        });

        it('Should have isUrl getter', () => {
            expect(StringType.create().email().isUrl).to.be.equal(false);
            expect(StringType.create().url().isUrl).to.be.equal(true);
            expect(StringType.create().uuid().isUrl).to.be.equal(false);
            expect(StringType.create().cuid().isUrl).to.be.equal(false);
        });

        it('Should have isUuid getter', () => {
            expect(StringType.create().email().isUuid).to.be.equal(false);
            expect(StringType.create().url().isUuid).to.be.equal(false);
            expect(StringType.create().uuid().isUuid).to.be.equal(true);
            expect(StringType.create().cuid().isUuid).to.be.equal(false);
        });

        it('Should have isCuid getter', () => {
            expect(StringType.create().email().isCuid).to.be.equal(false);
            expect(StringType.create().url().isCuid).to.be.equal(false);
            expect(StringType.create().uuid().isCuid).to.be.equal(false);
            expect(StringType.create().cuid().isCuid).to.be.equal(true);
        });

        it('Should have minLength getter', () => {
            expect(StringType.create().min(2).minLength).to.be.equal(2);
            expect(StringType.create().min(2).min(4).minLength).to.be.equal(4);
        });

        it('Should have maxLength getter', () => {
            expect(StringType.create().max(4).maxLength).to.be.equal(4);
            expect(StringType.create().max(4).max(2).maxLength).to.be.equal(2);
        });
    });

    it('Should infer StringType', () => {
        const MyString = StringType.create();
        type MyStringType = TypeOf<typeof MyString>;
        const result: AssertEqual<MyStringType, string> = true;
        expect(result).to.be.equal(true);
    });
});
