/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { TypeOf } from './Type';
import { UnknownType } from './Unknown';

describe('@kephajs/core/Validate/Type/Unknown', () => {
    it('Should parse UnknownType string', () => {
        expect(UnknownType.create().parse('foobar')).to.be.equal('foobar');
    });

    it('Should parse UnknownType number', () => {
        expect(UnknownType.create().parse('123')).to.be.equal('123');
    });

    it('Should parse UnknownType boolean', () => {
        expect(UnknownType.create().parse(true)).to.be.equal(true);
    });

    it('Should parse UnknownType undefined', () => {
        expect(UnknownType.create().parse(undefined)).to.be.equal(undefined);
    });

    it('Should parse UnknownType null', () => {
        expect(UnknownType.create().parse(null)).to.be.equal(null);
    });

    it('Should parse UnknownType object', () => {
        const object = { a: 1, b: 2 };
        expect(UnknownType.create().parse(object)).to.be.deep.equal(object);
    });

    it('Should infer UnknownType', () => {
        const MyUnknown = UnknownType.create();
        type MyUnknownType = TypeOf<typeof MyUnknown>;
        const result: AssertEqual<MyUnknownType, unknown> = true;
        expect(result).to.be.equal(true);
    });
});
