/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import {
    FirstPartyTypeKind,
    RawCreateParams,
    Type,
    TypeAny,
    TypeDef,
} from './Type';

import {
    INVALID,
    ParseInput,
    ParseReturnType,
    ParsedType,
    ParseInputLazyPath,
    ParseStatus,
} from '../helpers/parseUtils';

export type ArrayDef<T extends TypeAny = TypeAny> = {
    typeName: FirstPartyTypeKind.ArrayType;
    type: T;
    minLength: { value: number; message?: string } | null;
    maxLength: { value: number; message?: string } | null;
} & TypeDef;

type ArrayCardinality = 'many' | 'atleastone';
type ArrayOutputType<
    T extends TypeAny,
    Cardinality extends ArrayCardinality = 'many'
> = Cardinality extends 'atleastone'
    ? [T['_output'], ...T['_output'][]]
    : T['_output'][];

/**
 * Describe an Array
 */
export class ArrayType<
    T extends TypeAny,
    Cardinality extends ArrayCardinality = 'many'
> extends Type<
    ArrayOutputType<T, Cardinality>,
    ArrayDef<T>,
    Cardinality extends 'atleastone'
        ? [T['_input'], ...T['_input'][]]
        : T['_input'][]
> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const { ctx, status } = this._processInputParams(input);

        if (ctx.parsedType !== ParsedType.array) {
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: ParsedType.array,
                received: ctx.parsedType,
            });
            return INVALID;
        }

        if (this._def.minLength !== null) {
            if (ctx.data.length < this._def.minLength.value) {
                this._addIssueToContext(ctx, {
                    code: IssueCode.tooSmall,
                    minimum: this._def.minLength.value,
                    type: 'array',
                    inclusive: true,
                    message: this._def.minLength.message,
                });
                status.dirty();
            }
        }

        if (this._def.maxLength !== null) {
            if (ctx.data.length > this._def.maxLength.value) {
                this._addIssueToContext(ctx, {
                    code: IssueCode.tooBig,
                    maximum: this._def.maxLength.value,
                    type: 'array',
                    inclusive: true,
                    message: this._def.maxLength.message,
                });
                status.dirty();
            }
        }

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const result = (ctx.data as any[]).map((item, i) => {
            return this._def.type._parse(
                new ParseInputLazyPath(ctx, item, ctx.path, i)
            );
        });

        return ParseStatus.mergeArray(status, result);
    }

    static create<U extends TypeAny>(
        schema: U,
        params?: RawCreateParams
    ): ArrayType<U> {
        return new ArrayType({
            typeName: FirstPartyTypeKind.ArrayType,
            type: schema,
            minLength: null,
            maxLength: null,
            ...ArrayType.processCreateParams(params),
        });
    }

    /**
     * Use `.element` to access the schema for an element of the array
     */
    get element() {
        return this._def.type;
    }

    /**
     * If you want to ensure that an array contains at least one element, use
     * `.nonEmpty()`
     */
    nonEmpty(message?: string | { message?: string }) {
        return this.min(1, message);
    }

    /**
     * Minimum lenght of the array
     */
    min(minLength: number, message?: string | { message?: string }) {
        return new ArrayType({
            ...this._def,
            minLength: {
                value: minLength,
                ...(typeof message === 'string' ? { message } : message || {}),
            },
        });
    }

    /**
     * Maximum length of the array
     */
    max(maxLength: number, message?: string | { message?: string }) {
        return new ArrayType({
            ...this._def,
            maxLength: {
                value: maxLength,
                ...(typeof message === 'string' ? { message } : message || {}),
            },
        });
    }

    /**
     * Length of the array
     */
    length(length: number, message?: string | { message?: string }) {
        return this.min(length, message).max(length, message);
    }
}
