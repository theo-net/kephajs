/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    FirstPartyTypeKind,
    NoUndefined,
    RawCreateParams,
    Type,
    TypeAny,
    TypeDef,
} from './Type';

import { ParseInput, ParseReturnType, ParsedType } from '../helpers/parseUtils';
import { Optional } from './Optional';

export type DefaultDef<T extends TypeAny = TypeAny> = {
    typeName: FirstPartyTypeKind.Default;
    innerType: T;
    defaultValue: () => NoUndefined<T['_input']>;
} & TypeDef;

export class Default<T extends TypeAny> extends Type<
    NoUndefined<T['_output']>,
    DefaultDef<T>,
    T['_input'] | undefined
> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const { ctx } = this._processInputParams(input);
        let data = ctx.data;
        if (ctx.parsedType === ParsedType.undefined) {
            data = this._def.defaultValue();
        }
        return this._def.innerType._parse({
            data,
            path: ctx.path,
            parent: ctx,
        });
    }

    /**
     * Remove the default value
     */
    removeDefault() {
        return this._def.innerType;
    }

    static create<T extends TypeAny>(
        type: T,
        params?: RawCreateParams
    ): Optional<T> {
        return new Optional({
            typeName: FirstPartyTypeKind.Optional,
            innerType: type,
            ...Default.processCreateParams(params),
        });
    }
}
