/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import {
    FirstPartyTypeKind,
    RawCreateParams,
    Type,
    TypeAny,
    TypeDef,
} from './Type';

import {
    INVALID,
    ParseInput,
    ParseReturnType,
    ParsedType,
    ParseStatus,
    ParseInputLazyPath,
} from '../helpers/parseUtils';

type TupleItems = [TypeAny, ...TypeAny[]];

export type TupleDef<
    T extends TupleItems | [] = TupleItems,
    Rest extends TypeAny | null = null
> = {
    typeName: FirstPartyTypeKind.Tuple;
    items: T;
    rest: Rest;
} & TypeDef;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type AssertArray<T> = T extends any[] ? T : never;
export type OutputTypeOfTuple<T extends TupleItems | []> = AssertArray<{
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [k in keyof T]: T[k] extends Type<any, any> ? T[k]['_output'] : never;
}>;

type OutputTypeOfTupleWithRest<
    T extends TupleItems | [],
    Rest extends TypeAny | null = null
> = Rest extends TypeAny
    ? [...OutputTypeOfTuple<T>, ...Rest['_output'][]]
    : OutputTypeOfTuple<T>;

export type InputTypeOfTuple<T extends TupleItems | []> = AssertArray<{
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [k in keyof T]: T[k] extends Type<any, any> ? T[k]['_input'] : never;
}>;
export type InputTypeOfTupleWithRest<
    T extends TupleItems | [],
    Rest extends TypeAny | null = null
> = Rest extends TypeAny
    ? [...InputTypeOfTuple<T>, ...Rest['_input'][]]
    : InputTypeOfTuple<T>;

/**
 * Unlike arrays, tuples have a fixed number of elements and each element can have a different type.
 */
export class TupleType<
    T extends [TypeAny, ...TypeAny[]] | [] = [TypeAny, ...TypeAny[]],
    Rest extends TypeAny | null = null
> extends Type<
    OutputTypeOfTupleWithRest<T, Rest>,
    TupleDef<T, Rest>,
    InputTypeOfTupleWithRest<T, Rest>
> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const { status, ctx } = this._processInputParams(input);

        if (ctx.parsedType !== ParsedType.array) {
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: ParsedType.array,
                received: ctx.parsedType,
            });
            return INVALID;
        }

        if (ctx.data.length < this._def.items.length) {
            this._addIssueToContext(ctx, {
                code: IssueCode.tooSmall,
                minimum: this._def.items.length,
                inclusive: true,
                type: 'array',
            });
            return INVALID;
        }

        if (!this._def.rest && ctx.data.length > this._def.items.length) {
            this._addIssueToContext(ctx, {
                code: IssueCode.tooBig,
                maximum: this._def.items.length,
                inclusive: true,
                type: 'array',
            });
            status.dirty();
        }

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const items = (ctx.data as any[])
            .map((item, itemIndex) => {
                const schema = this._def.items[itemIndex] || this._def.rest;
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                if (!schema) return null as any as ParseReturnType<any>;
                return schema._parse(
                    new ParseInputLazyPath(ctx, item, ctx.path, itemIndex)
                );
            })
            .filter(x => !!x); // filter nulls

        return ParseStatus.mergeArray(status, items as ParseReturnType[]);
    }

    /**
     * Special method: `FunctionType` use it to contain a rest parameter as a TupleType.
     */
    rest<U extends TypeAny>(rest: U): TupleType<T, U> {
        return new TupleType({
            ...this._def,
            rest,
        });
    }

    get items() {
        return this._def.items;
    }

    static create<U extends [TypeAny, ...TypeAny[]] | []>(
        schemas: U,
        params?: RawCreateParams
    ): TupleType<U, null> {
        return new TupleType({
            typeName: FirstPartyTypeKind.Tuple,
            items: schemas,
            rest: null,
            ...TupleType.processCreateParams(params),
        });
    }
}
