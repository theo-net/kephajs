/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import { IssueCode } from '../Error/Error';

import { AssertEqual } from '../helpers/testUtils';
import { MapType } from './Map';
import { StringType } from './String';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Map', () => {
    const stringMap = MapType.create(StringType.create(), StringType.create());
    type StringMapType = TypeOf<typeof stringMap>;

    it('Should type inference', () => {
        const f1: AssertEqual<StringMapType, Map<string, string>> = true;
        expect(f1).to.be.equal(true);
    });

    it('Should valid parse', () => {
        const result = stringMap.safeParse(
            new Map([
                ['first', 'foo'],
                ['second', 'bar'],
            ])
        );
        expect(result.success).to.be.equal(true);
        if (result.success) {
            expect(result.data.has('first')).to.be.equal(true);
            expect(result.data.has('second')).to.be.equal(true);
            expect(result.data.get('first')).to.be.equal('foo');
            expect(result.data.get('second')).to.be.equal('bar');
        }
    });

    it('Should throws when a Set is given', () => {
        const result = stringMap.safeParse(new Set([]));
        expect(result.success).to.be.equal(false);
        if (result.success === false) {
            expect(result.error.errors.length).to.be.equal(1);
            expect(result.error.errors[0].code).to.be.equal(
                IssueCode.invalidType
            );
        }
    });

    it('Should throws when the given map has invalid key and invalid input', () => {
        const result = stringMap.safeParse(new Map([[42, Symbol()]]));
        expect(result.success).to.be.equal(false);
        if (result.success === false) {
            expect(result.error.errors.length).to.be.equal(2);
            expect(result.error.errors[0].code).to.be.equal(
                IssueCode.invalidType
            );
            expect(result.error.errors[0].path).to.be.deep.equal([0, 'key']);
            expect(result.error.errors[1].code).to.be.equal(
                IssueCode.invalidType
            );
            expect(result.error.errors[1].path).to.be.deep.equal([0, 'value']);
        }
    });

    it('Should throws when the given map has multiple invalid entries', () => {
        // const result = stringMap.safeParse(new Map([[42, Symbol()]]));

        const result = stringMap.safeParse(
            new Map([
                [1, 'foo'],
                ['bar', 2],
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
            ] as [any, any][]) as Map<any, any>
        );

        // const result = stringMap.safeParse(new Map([[42, Symbol()]]));
        expect(result.success).to.be.equal(false);
        if (result.success === false) {
            expect(result.error.errors.length).to.be.equal(2);
            expect(result.error.errors[0].code).to.be.equal(
                IssueCode.invalidType
            );
            expect(result.error.errors[0].path).to.be.deep.equal([0, 'key']);
            expect(result.error.errors[1].code).to.be.equal(
                IssueCode.invalidType
            );
            expect(result.error.errors[1].path).to.be.deep.equal([1, 'value']);
        }
    });
});
