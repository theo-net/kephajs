/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import { ErrorValidator, IssueCode } from '../Error/Error';

import { AssertEqual } from '../helpers/testUtils';
import { ArrayType } from './Array';
import { BooleanType } from './Boolean';
import { FunctionType } from './Function';
import { NumberType } from './Number';
import { ObjectType } from './Object';
import { StringType } from './String';
import { TupleType } from './Tuple';
import { TypeOf } from './Type';
import { Union } from './Union';

describe('@kephajs/core/Validate/Type/Function', () => {
    const args1 = TupleType.create([StringType.create()]);
    const returns1 = NumberType.create();
    const func1 = FunctionType.create(args1, returns1);

    it('Should function parsing', () => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const parsed = func1.parse((arg: any) => arg.length);
        expect(parsed('asdf')).to.be.equal(4);
    });

    it('Should parsed function fail 1', () => {
        const parsed = func1.parse((x: string) => x);
        expect(() => parsed('asdf')).to.throw();
    });

    it('Should parsed function fail 2', () => {
        const parsed = func1.parse((x: string) => x);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        expect(() => parsed(13 as any)).to.throw();
    });

    it('Should function inference 1', () => {
        type Func1 = TypeOf<typeof func1>;
        const t1: AssertEqual<Func1, (k: string) => number> = true;
        expect(t1).to.be.equal(true);
    });

    it('Should args method', () => {
        const t1 = FunctionType.create();
        type T1 = TypeOf<typeof t1>;
        const f1: AssertEqual<T1, () => void> = true;

        const t2 = t1.args(StringType.create());
        type T2 = TypeOf<typeof t2>;
        const f2: AssertEqual<T2, (arg: string) => void> = true;

        const t3 = t2.returns(BooleanType.create());
        type T3 = TypeOf<typeof t3>;
        const f3: AssertEqual<T3, (arg: string) => boolean> = true;

        expect([f1, f2, f3]).to.be.deep.equal([true, true, true]);
    });

    const args2 = TupleType.create([
        ObjectType.create({
            f1: NumberType.create(),
            f2: StringType.create().nullable(),
            f3: ArrayType.create(BooleanType.create().optional()).optional(),
        }),
    ]);
    const returns2 = Union.create([StringType.create(), NumberType.create()]);

    const func2 = FunctionType.create(args2, returns2);

    it('Should function inference 2', () => {
        type Func2 = TypeOf<typeof func2>;
        const t2: AssertEqual<
            Func2,
            (arg: {
                f1: number;
                f2: string | null;
                f3?: (boolean | undefined)[] | undefined;
            }) => string | number
        > = true;
        expect(t2).to.be.equal(true);
    });

    it('Should valid function run', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const validFunc2Instance = func2.implement(_x => {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            return 'adf' as any;
        });

        const checker = () => {
            validFunc2Instance({
                f1: 21,
                f2: 'asdf',
                f3: [true, false],
            });
        };

        checker();
    });

    it('Should input validation error', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const invalidFuncInstance = func2.implement(_x => {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            return 'adf' as any;
        });

        const checker = () => {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            invalidFuncInstance('Invalid_input' as any);
        };

        expect(checker).to.throw();
    });

    it('Should output validation error', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const invalidFuncInstance = func2.implement(_x => {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            return ['this', 'is', 'not', 'valid', 'output'] as any;
        });

        const checker = () => {
            invalidFuncInstance({
                f1: 21,
                f2: 'asdf',
                f3: [true, false],
            });
        };

        expect(checker).to.throw();
    });

    it('Should special function error codes', () => {
        const checker = FunctionType.create(
            TupleType.create([StringType.create()]),
            BooleanType.create()
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        ).implement((...arg: [] | [any, ...any[]]) => {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            return arg.length as any;
        });
        try {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            checker('12' as any);
        } catch (err) {
            const zerr = err as ErrorValidator;
            const first = zerr.errors[0];
            if (first.code !== IssueCode.invalidReturnType) throw new Error();

            expect(first.returnTypeError).to.be.instanceof(ErrorValidator);
        }

        try {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            checker(12 as any);
        } catch (err) {
            const zerr = err as ErrorValidator;
            const first = zerr.errors[0];
            if (first.code !== IssueCode.invalidArguments) throw new Error();
            expect(first.argumentsError).to.be.instanceof(ErrorValidator);
        }
    });

    it('Should allow extra parameters', () => {
        const maxLength5 = FunctionType.create()
            .args(StringType.create())
            .returns(BooleanType.create())
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            .implement((str, _arg, _qewr) => {
                return str.length <= 5;
            });

        const filteredList = [
            'apple',
            'orange',
            'pear',
            'banana',
            'strawberry',
        ].filter(maxLength5);
        expect(filteredList.length).to.be.equal(2);
    });

    it('Should params and returnType getters', () => {
        const func = FunctionType.create()
            .args(StringType.create())
            .returns(StringType.create());

        func.parameters().items[0].parse('asdf');
        func.returnType().parse('asdf');
    });
});
