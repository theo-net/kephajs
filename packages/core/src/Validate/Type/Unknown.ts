/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { FirstPartyTypeKind, RawCreateParams, Type, TypeDef } from './Type';

import { OK, ParseInput, ParseReturnType } from '../helpers/parseUtils';

export type UnknownDef = {
    typeName: FirstPartyTypeKind.UnknownType;
} & TypeDef;

/**
 * Catch all type, accept any value
 */
export class UnknownType extends Type<unknown, UnknownDef> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        return OK(input.data);
    }

    static create = (params?: RawCreateParams): UnknownType => {
        return new UnknownType({
            typeName: FirstPartyTypeKind.UnknownType,
            ...UnknownType.processCreateParams(params),
        });
    };
}
