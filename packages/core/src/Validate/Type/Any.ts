/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { FirstPartyTypeKind, RawCreateParams, Type, TypeDef } from './Type';

import { OK, ParseInput, ParseReturnType } from '../helpers/parseUtils';

export type AnyDef = {
    typeName: FirstPartyTypeKind.AnyType;
} & TypeDef;

/**
 * Catch all type, accept any value
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export class AnyType extends Type<any, AnyDef> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        return OK(input.data);
    }

    static create = (params?: RawCreateParams): AnyType => {
        return new AnyType({
            typeName: FirstPartyTypeKind.AnyType,
            ...AnyType.processCreateParams(params),
        });
    };
}
