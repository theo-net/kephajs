/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import {
    FirstPartyTypeKind,
    Primitive,
    RawCreateParams,
    RawShape,
    Type,
    TypeDef,
} from './Type';

import {
    INVALID,
    ParseInput,
    ParseReturnType,
    ParsedType,
} from '../helpers/parseUtils';
import { ObjectType } from './Object';
import { LiteralType } from './Literal';

type DiscriminatedUnionOption<
    Discriminator extends string,
    DiscriminatorValue extends Primitive
> = ObjectType<
    { [key in Discriminator]: LiteralType<DiscriminatorValue> } & RawShape,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    any,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    any
>;

export type DiscriminatedUnionDef<
    Discriminator extends string,
    DiscriminatorValue extends Primitive,
    Option extends DiscriminatedUnionOption<Discriminator, DiscriminatorValue>
> = {
    typeName: FirstPartyTypeKind.DiscriminatedUnion;
    discriminator: Discriminator;
    options: Map<DiscriminatorValue, Option>;
} & TypeDef;

export class DiscriminatedUnion<
    Discriminator extends string,
    DiscriminatorValue extends Primitive,
    Option extends DiscriminatedUnionOption<Discriminator, DiscriminatorValue>
> extends Type<
    Option['_output'],
    DiscriminatedUnionDef<Discriminator, DiscriminatorValue, Option>,
    Option['_input']
> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const { ctx } = this._processInputParams(input);

        if (ctx.parsedType !== ParsedType.object) {
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: ParsedType.object,
                received: ctx.parsedType,
            });
            return INVALID;
        }

        const discriminator = this.discriminator;
        const discriminatorValue: DiscriminatorValue = ctx.data[discriminator];
        const option = this.options.get(discriminatorValue);

        if (!option) {
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidUnionDiscriminator,
                options: this.validDiscriminatorValues,
                path: [discriminator],
            });
            return INVALID;
        }

        return option._parse({
            data: ctx.data,
            path: ctx.path,
            parent: ctx,
        });
    }

    static create<
        Disc extends string,
        DiscValue extends Primitive,
        Types extends [
            DiscriminatedUnionOption<Disc, DiscValue>,
            DiscriminatedUnionOption<Disc, DiscValue>,
            ...DiscriminatedUnionOption<Disc, DiscValue>[]
        ]
    >(
        discriminator: Disc,
        types: Types,
        params?: RawCreateParams
    ): DiscriminatedUnion<Disc, DiscValue, Types[number]> {
        const options: Map<DiscValue, Types[number]> = new Map();

        try {
            types.forEach(type => {
                options.set(type.shape[discriminator].value, type);
            });
        } catch (e) {
            throw new Error(
                'The discriminator value could not be extracted from all the provided schemas'
            );
        }

        if (options.size !== types.length) {
            throw new Error('Some of the discriminator values are not unique');
        }

        return new DiscriminatedUnion<Disc, DiscValue, Types[number]>({
            typeName: FirstPartyTypeKind.DiscriminatedUnion,
            discriminator,
            options,
            ...DiscriminatedUnion.processCreateParams(params),
        });
    }

    get discriminator() {
        return this._def.discriminator;
    }

    get validDiscriminatorValues() {
        return Array.from(this.options.keys());
    }

    get options() {
        return this._def.options;
    }
}
