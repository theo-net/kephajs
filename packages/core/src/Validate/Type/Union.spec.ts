/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { Union } from './Union';
import { TypeOf } from './Type';
import { StringType } from './String';
import { NumberType } from './Number';

describe('@kephajs/core/Validate/Type/Union', () => {
    const stringOrNumber = Union.create([
        StringType.create(),
        NumberType.create(),
    ]);

    it('Should parse Union string', () => {
        expect(stringOrNumber.parse('foobar')).to.be.equal('foobar');
    });

    it('Should parse Union number', () => {
        expect(stringOrNumber.parse(123)).to.be.equal(123);
    });

    it('Should parse Union boolean', () => {
        const f = () => stringOrNumber.parse(true);
        expect(f).to.throw();
    });

    it('Should parse Union undefined', () => {
        const f = () => stringOrNumber.parse(undefined);
        expect(f).to.throw();
    });

    it('Should parse Union null', () => {
        const f = () => stringOrNumber.parse(null);
        expect(f).to.throw();
    });

    it('Should infer Union', () => {
        const MyUnion = stringOrNumber;
        type MyUnionType = TypeOf<typeof MyUnion>;
        const result: AssertEqual<MyUnionType, string | number> = true;
        expect(result).to.be.equal(true);
    });

    it('Should return all options', () => {
        const type1 = StringType.create(),
            type2 = NumberType.create(),
            type3 = StringType.create();
        expect(Union.create([type1, type2, type3]).options).to.be.deep.equal([
            type1,
            type2,
            type3,
        ]);
    });
});
