/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { Nullable } from './Nullable';
import { StringType } from './String';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Nullable', () => {
    it('Should parse Nullable string', () => {
        expect(
            Nullable.create(StringType.create()).parse('foobar')
        ).to.be.equal('foobar');
    });

    it('Should parse Nullable number', () => {
        const f = () => Nullable.create(StringType.create()).parse(123);
        expect(f).to.throw();
    });

    it('Should parse Nullable boolean', () => {
        const f = () => Nullable.create(StringType.create()).parse(true);
        expect(f).to.throw();
    });

    it('Should parse Nullable undefined', () => {
        const f = () => Nullable.create(StringType.create()).parse(undefined);
        expect(f).to.throw();
    });

    it('Should parse Nullable null', () => {
        expect(Nullable.create(StringType.create()).parse(null)).to.be.equal(
            null
        );
    });

    it('Should infer Nullable', () => {
        const MyNullable = Nullable.create(StringType.create());
        type MyNullableType = TypeOf<typeof MyNullable>;
        const result: AssertEqual<MyNullableType, string | null> = true;
        expect(result).to.be.equal(true);
    });

    it('Should extract the wrapped scheama', () => {
        const stringSchema = StringType.create();
        expect(Nullable.create(stringSchema).unwrap()).to.be.deep.equal(
            stringSchema
        );
    });
});
