/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    FirstPartyTypeKind,
    RawCreateParams,
    Type,
    TypeAny,
    TypeDef,
} from './Type';

import {
    OK,
    ParseInput,
    ParseReturnType,
    ParsedType,
} from '../helpers/parseUtils';

export type OptionalDef<T extends TypeAny = TypeAny> = {
    typeName: FirstPartyTypeKind.Optional;
    innerType: T;
} & TypeDef;

/**
 * You can make any schema optional with this type. This wrap the schema and
 * returns the results.
 */
export class Optional<T extends TypeAny> extends Type<
    T['_output'] | undefined,
    OptionalDef<T>,
    T['_input'] | undefined
> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        if (this._getType(input) === ParsedType.undefined) {
            return OK(undefined);
        }
        return this._def.innerType._parse(input);
    }

    static create<U extends TypeAny>(
        type: U,
        params?: RawCreateParams
    ): Optional<U> {
        return new Optional({
            typeName: FirstPartyTypeKind.Optional,
            innerType: type,
            ...Optional.processCreateParams(params),
        });
    }

    /**
     * Extract the wrapped schema
     */
    unwrap() {
        return this._def.innerType;
    }
}
