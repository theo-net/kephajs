/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import {
    FirstPartyTypeKind,
    RawCreateParams,
    RawShape,
    Type,
    TypeAny,
    TypeDef,
} from './Type';

import {
    INVALID,
    ParseInput,
    ParseReturnType,
    ParsedType,
    ParseStatus,
    ParseInputLazyPath,
} from '../helpers/parseUtils';
import { NeverType } from './Never';
import { Optional } from './Optional';
import { objectKeys } from '../../Helpers/Helpers';

export type ObjectDef<
    T extends RawShape = RawShape,
    UnknownKeys extends UnknownKeysParam = UnknownKeysParam,
    Catchall extends TypeAny = TypeAny
> = {
    typeName: FirstPartyTypeKind.ObjectType;
    shape: () => T;
    catchall: Catchall;
    unknownKeys: UnknownKeys;
} & TypeDef;

type UnknownKeysParam = 'passthrough' | 'strict' | 'strip';

type Identity<T> = T;
type Flatten<T extends object> = Identity<{ [k in keyof T]: T[k] }>;
type OptionalKeys<T extends object> = {
    [k in keyof T]: undefined extends T[k] ? k : never;
}[keyof T];
type RequiredKeys<T extends object> = {
    [k in keyof T]: undefined extends T[k] ? never : k;
}[keyof T];
type AddQuestionMarks<T extends object> = Partial<Pick<T, OptionalKeys<T>>> &
    Pick<T, RequiredKeys<T>>;

type BaseObjectOutputType<Shape extends RawShape> = Flatten<
    AddQuestionMarks<{
        [k in keyof Shape]: Shape[k]['_output'];
    }>
>;
type ObjectOutputType<
    Shape extends RawShape,
    Catchall extends TypeAny
> = TypeAny extends Catchall
    ? BaseObjectOutputType<Shape>
    : Flatten<
          BaseObjectOutputType<Shape> & { [k: string]: Catchall['_output'] }
      >;

type BaseObjectInputType<Shape extends RawShape> = Flatten<
    AddQuestionMarks<{
        [k in keyof Shape]: Shape[k]['_input'];
    }>
>;

type ObjectInputType<
    Shape extends RawShape,
    Catchall extends TypeAny
> = TypeAny extends Catchall
    ? BaseObjectInputType<Shape>
    : Flatten<BaseObjectInputType<Shape> & { [k: string]: Catchall['_input'] }>;

type ExtendShape<A, B> = Omit<A, keyof B> & B;

export type NoNeverKeys<T extends RawShape> = {
    [k in keyof T]: [T[k]] extends [never] ? never : k;
}[keyof T];
export type NoNever<T extends RawShape> = Identity<{
    [k in NoNeverKeys<T>]: k extends keyof T ? T[k] : never;
}>;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type AnyObjectType = ObjectType<any, any, any>;

export class ObjectType<
    T extends RawShape,
    UnknownKeys extends UnknownKeysParam = 'strip',
    Catchall extends TypeAny = TypeAny,
    Output = ObjectOutputType<T, Catchall>,
    Input = ObjectInputType<T, Catchall>
> extends Type<Output, ObjectDef<T, UnknownKeys, Catchall>, Input> {
    private _cached: { shape: T; keys: string[] } | null = null;

    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const parsedType = this._getType(input);
        if (parsedType !== ParsedType.object) {
            const ctx = this._getOrReturnCtx(input);
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: ParsedType.object,
                received: ctx.parsedType,
            });
            return INVALID;
        }

        const { status, ctx } = this._processInputParams(input);

        const { shape, keys: shapeKeys } = this._getCached();
        const extraKeys: string[] = [];
        for (const key in ctx.data) {
            if (!shapeKeys.includes(key)) {
                extraKeys.push(key);
            }
        }

        const pairs: {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            key: ParseReturnType<any>;
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            value: ParseReturnType<any>;
            alwaysSet?: boolean;
        }[] = [];
        for (const key of shapeKeys) {
            const keyValidator = shape[key];
            const value = ctx.data[key];
            pairs.push({
                key: { status: 'valid', value: key },
                value: keyValidator._parse(
                    new ParseInputLazyPath(ctx, value, ctx.path, key)
                ),
                alwaysSet: key in ctx.data,
            });
        }

        if (this._def.catchall instanceof NeverType) {
            if (this._def.unknownKeys === 'passthrough') {
                for (const key of extraKeys) {
                    pairs.push({
                        key: { status: 'valid', value: key },
                        value: { status: 'valid', value: ctx.data[key] },
                    });
                }
            } else if (this._def.unknownKeys === 'strict') {
                if (extraKeys.length > 0) {
                    this._addIssueToContext(ctx, {
                        code: IssueCode.unrecognizedKeys,
                        keys: extraKeys,
                    });
                    status.dirty();
                }
            }
        } else {
            // run cathall validation
            for (const key of extraKeys) {
                const value = ctx.data[key];
                pairs.push({
                    key: { status: 'valid', value: key },
                    value: this._def.catchall._parse(
                        new ParseInputLazyPath(ctx, value, ctx.path, key)
                    ),
                    alwaysSet: key in ctx.data,
                });
            }
        }

        return ParseStatus.mergeObject(status, pairs);
    }

    static create<U extends RawShape>(
        shape: U,
        params?: RawCreateParams
    ): ObjectType<U> {
        return new ObjectType({
            typeName: FirstPartyTypeKind.ObjectType,
            shape: () => shape,
            unknownKeys: 'strip',
            catchall: NeverType.create(),
            ...ObjectType.processCreateParams(params),
        });
    }

    static lazycreate<T extends RawShape>(
        shape: () => T,
        params?: RawCreateParams
    ): ObjectType<T> {
        return new ObjectType({
            shape,
            unknownKeys: 'strip',
            catchall: NeverType.create(),
            typeName: FirstPartyTypeKind.ObjectType,
            ...ObjectType.processCreateParams(params),
        });
    }

    /**
     * Return the member of the object (give each validator for each key)
     */
    get shape() {
        return this._def.shape();
    }

    /**
     * Return the behavior with the unknown keys
     */
    get unknownKeys() {
        return this._def.unknownKeys;
    }

    /**
     * You can add additional fields an object scheam with this method:
     */
    extend<
        Def extends ObjectDef<T, UnknownKeys, Catchall>,
        Augmentation extends RawShape
    >(
        augmentation: Augmentation
    ): ObjectType<
        ExtendShape<ReturnType<Def['shape']>, Augmentation>,
        Def['unknownKeys'],
        Def['catchall']
    > {
        return new ObjectType({
            ...this._def,
            shape: () => ({
                ...this._def.shape(),
                ...augmentation,
            }),
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        }) as any;
    }

    /**
     * Merge is equivalent to `A.extend(B.shape)`
     */
    merge<Incoming extends AnyObjectType>(
        merging: Incoming
    ): ObjectType<ExtendShape<T, Incoming['shape']>, UnknownKeys, Catchall> {
        return new ObjectType({
            typeName: FirstPartyTypeKind.ObjectType,
            shape: () =>
                this._mergeShapes(this._def.shape(), merging._def.shape()),
            unknownKeys: merging._def.unknownKeys,
            catchall: merging._def.catchall,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        }) as any;
    }

    /**
     * To only keep certain keys
     */
    pick<Mask extends { [k in keyof T]?: true }>(
        mask: Mask
    ): ObjectType<
        Pick<T, Extract<keyof T, keyof Mask>>,
        UnknownKeys,
        Catchall
    > {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const shape: any = {};
        objectKeys(mask).map(key => {
            shape[key] = this.shape[key];
        });
        return new ObjectType({
            ...this._def,
            shape: () => shape,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        }) as any;
    }

    /**
     * To remove certain keys
     */
    omit<Mask extends { [k in keyof T]?: true }>(
        mask: Mask
    ): ObjectType<Omit<T, keyof Mask>, UnknownKeys, Catchall> {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const shape: any = {};
        objectKeys(this.shape).map(key => {
            if (objectKeys(mask).indexOf(key) === -1) {
                shape[key] = this.shape[key];
            }
        });
        return new ObjectType({
            ...this._def,
            shape: () => shape,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        }) as any;
    }

    /**
     * Makes all properties optional
     */
    partial(): ObjectType<
        { [k in keyof T]: Optional<T[k]> },
        UnknownKeys,
        Catchall
    >;
    partial<Mask extends { [k in keyof T]?: true }>(
        mask: Mask
    ): ObjectType<
        NoNever<{
            [k in keyof T]: k extends keyof Mask ? Optional<T[k]> : T[k];
        }>,
        UnknownKeys,
        Catchall
    >;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    partial(mask?: any) {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const newShape: any = {};
        if (mask) {
            objectKeys(this.shape).map(key => {
                if (objectKeys(mask).indexOf(key) === -1) {
                    newShape[key] = this.shape[key];
                } else {
                    newShape[key] = this.shape[key].optional();
                }
            });
            return new ObjectType({
                ...this._def,
                shape: () => newShape,
            });
        } else {
            for (const key in this.shape) {
                const fieldSchema = this.shape[key];
                newShape[key] = fieldSchema.optional();
            }
        }

        return new ObjectType({
            ...this._def,
            shape: () => newShape,
        });
    }

    /**
     * By default objects scheamas strip out unrecognize keys during passing. Instead,
     * if you want to pass through unknown keys, use `passthrough()`.
     */
    passthrough(): ObjectType<T, 'passthrough', Catchall> {
        return new ObjectType({
            ...this._def,
            unknownKeys: 'passthrough',
        });
    }

    /**
     * By default objects schemas strip out unreconized keys during parsing. You can disallow
     * unknown keys with `.strict()`. If there are any unknown keys in the input, we will
     * throw an error.
     */
    strict(): ObjectType<T, 'strict', Catchall> {
        return new ObjectType({
            ...this._def,
            unknownKeys: 'strict',
        });
    }

    /**
     * You can use the `.strip` method to reset an object schema to the default behavior
     * (stripping unrecognized keys).
     */
    strip(): ObjectType<T, 'strip', Catchall> {
        return new ObjectType({
            ...this._def,
            unknownKeys: 'strip',
        });
    }

    /**
     * You can pass a "catchall" schema into an object schema. All unknown keys will be validated
     * against it.
     *
     * Using `.catchall()` obviates `.passthrough()`, `.strip()`, or `.strict()`. All keys are now
     * considered "known".
     */
    catchall<Schema extends TypeAny>(
        schema: Schema
    ): ObjectType<T, UnknownKeys, Schema> {
        return new ObjectType({
            ...this._def,
            catchall: schema,
        });
    }

    protected _getCached(): { shape: T; keys: string[] } {
        if (this._cached !== null) return this._cached;
        const shape = this._def.shape();
        const keys = objectKeys(shape);
        return (this._cached = { shape, keys });
    }

    /**
     * Merge shape
     */
    protected _mergeShapes<U extends RawShape, V extends RawShape>(
        first: U,
        second: V
    ): U & V {
        return {
            ...first,
            ...second,
        };
    }
}
