/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import { IssueCode } from '../Error/Error';

import { AssertEqual } from '../helpers/testUtils';
import { ArrayType } from './Array';
import { BooleanType } from './Boolean';
import { Intersection } from './Intersection';
import { NumberType } from './Number';
import { ObjectType } from './Object';
import { StringType } from './String';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Intersection', () => {
    it('Should object intersection', () => {
        const BaseTeacher = ObjectType.create({
            subjects: ArrayType.create(StringType.create()),
        });
        const HasID = ObjectType.create({ id: StringType.create() });

        const Teacher = Intersection.create(BaseTeacher.passthrough(), HasID); // BaseTeacher.merge(HasID);
        const data = {
            subjects: ['math'],
            id: 'asdfasdf',
        };
        expect(Teacher.parse(data)).to.be.deep.equal(data);
        expect(() => Teacher.parse({ subject: data.subjects })).to.throw();
        expect(Teacher.parse({ ...data, extra: 12 })).to.be.deep.equal({
            ...data,
            extra: 12,
        });

        expect(() =>
            Intersection.create(BaseTeacher.strict(), HasID).parse({
                ...data,
                extra: 12,
            })
        ).to.throw();
    });

    it('Should deep intersection', () => {
        const Animal = ObjectType.create({
            properties: ObjectType.create({
                is_animal: BooleanType.create(),
            }),
        });
        const Cat = ObjectType.create({
            properties: ObjectType.create({
                jumped: BooleanType.create(),
            }),
        }).and(Animal);

        const cat = Cat.parse({
            properties: { is_animal: true, jumped: true },
        });
        expect(cat.properties).to.be.deep.equal({
            is_animal: true,
            jumped: true,
        });
    });

    it('Should deep intersection of arrays', async () => {
        const Author = ObjectType.create({
            posts: ArrayType.create(
                ObjectType.create({
                    post_id: NumberType.create(),
                })
            ),
        });
        const Registry = ObjectType.create({
            posts: ArrayType.create(
                ObjectType.create({
                    title: StringType.create(),
                })
            ),
        }).and(Author);

        const posts = [
            { post_id: 1, title: 'Novels' },
            { post_id: 2, title: 'Fairy tales' },
        ];
        const cat = Registry.parse({ posts });
        expect(cat.posts).to.be.deep.equal(posts);
    });

    it('Should invalid intersection types', async () => {
        const numberIntersection = Intersection.create(
            NumberType.create(),
            NumberType.create().transform(x => x + 1)
        );

        const result = numberIntersection.safeParse(1234);
        expect(result.success).to.be.equal(false);
        if (!result.success) {
            expect(result.error.errors[0].code).to.be.equal(
                IssueCode.invalidIntersectionTypes
            );
        }
    });

    it('Should invalid array merge', async () => {
        const stringArrInt = Intersection.create(
            StringType.create().array(),
            StringType.create()
                .array()
                .transform(val => [...val, 'asdf'])
        );
        const result = stringArrInt.safeParse(['asdf', 'qwer']);
        expect(result.success).to.be.equal(false);
        if (!result.success) {
            expect(result.error.errors[0].code).to.be.equal(
                IssueCode.invalidIntersectionTypes
            );
        }
    });

    it('Should infer Intersection', () => {
        const MyIntersection = Intersection.create(
            ObjectType.create({
                id: NumberType.create(),
                name: StringType.create(),
            }),
            ObjectType.create({
                role: StringType.create().optional(),
            })
        );
        type MyIntersectionType = TypeOf<typeof MyIntersection>;
        const result: AssertEqual<
            MyIntersectionType,
            { id: number; name: string; role?: string }
        > = true;
        expect(result).to.be.equal(true);
    });
});
