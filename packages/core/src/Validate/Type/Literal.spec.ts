/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { LiteralType } from './Literal';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Literal', () => {
    it('Should parse LiteralType string', () => {
        const f = () => LiteralType.create('foobar').parse('foor');
        expect(f).to.throw();
        expect(LiteralType.create('foobar').parse('foobar')).to.be.equal(
            'foobar'
        );
    });

    it('Should parse LiteralType number', () => {
        const f = () => LiteralType.create(12).parse(123);
        expect(f).to.throw();
        expect(LiteralType.create(12).parse(12)).to.be.equal(12);
    });

    it('Should parse LiteralType boolean', () => {
        const f = () => LiteralType.create(false).parse(true);
        expect(f).to.throw();
        expect(LiteralType.create(false).parse(false)).to.be.equal(false);
    });

    it('Should parse LiteralType undefined', () => {
        const f = () => LiteralType.create(undefined).parse(null);
        expect(f).to.throw();
        expect(LiteralType.create(undefined).parse(undefined)).to.be.equal(
            undefined
        );
    });

    it('Should parse LiteralType null', () => {
        const f = () => LiteralType.create(null).parse(undefined);
        expect(f).to.throw();
        expect(LiteralType.create(null).parse(null)).to.be.equal(null);
    });

    it('Should return the literal value', () => {
        expect(LiteralType.create(12).value).to.be.equal(12);
    });

    it('Should infer LiteralType', () => {
        const MyLiteral = LiteralType.create('foobar');
        type MyLiteralType = TypeOf<typeof MyLiteral>;
        const result: AssertEqual<MyLiteralType, 'foobar'> = true;
        expect(result).to.be.equal(true);
    });
});
