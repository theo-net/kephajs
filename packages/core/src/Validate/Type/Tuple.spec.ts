/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { NumberType } from './Number';
import { ObjectType } from './Object';
import { StringType } from './String';
import { TupleType } from './Tuple';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Tuple', () => {
    const athleteSchema = TupleType.create([
        StringType.create(),
        NumberType.create(),
        ObjectType.create({
            pointsScored: NumberType.create(),
        }),
    ]);

    it('Should parse TupleType string', () => {
        const f = () => athleteSchema.parse('foobar');
        expect(f).to.throw();
    });

    it('Should parse TupleType number', () => {
        const f = () => athleteSchema.parse(123);
        expect(f).to.throw();
    });

    it('Should parse TupleType boolean', () => {
        const f = () => athleteSchema.parse(true);
        expect(f).to.throw();
    });

    it('Should parse TupleType undefined', () => {
        const f = () => athleteSchema.parse(undefined);
        expect(f).to.throw();
    });

    it('Should parse TupleType null', () => {
        const f = () => athleteSchema.parse(null);
        expect(f).to.throw();
    });

    it('Should parse TupleType tuple, fail if not enough items', () => {
        const f = () => athleteSchema.parse(['foo', 12]);
        expect(f).to.throw();
    });

    it('Should parse TupleType tuple, fail if they are to many items', () => {
        const f = () =>
            athleteSchema.parse(['foo', 12, { pointsScored: 'foobar ' }, 3]);
        expect(f).to.throw();
    });

    it("Should parse TupleType tuple, fail if item's validation failed", () => {
        const f = () => athleteSchema.parse(['foo', 12, 'bar']);
        expect(f).to.throw();
    });

    it("Should parse TupleType tuple, fail if deep item's validation failed", () => {
        const f = () =>
            athleteSchema.parse(['foo', 12, { pointsScored: 'foobar ' }]);
        expect(f).to.throw();
    });

    it("Should parse TupleType tuple, sucess if deep item's validation sucess", () => {
        const result = athleteSchema.safeParse([
            'foo',
            12,
            { pointsScored: 3 },
        ]);
        expect(result.success).to.be.equal(true);
    });

    it('Should infer TupleType', () => {
        const MyAthlete = athleteSchema;
        type MyTupleType = TypeOf<typeof MyAthlete>;
        const result: AssertEqual<
            MyTupleType,
            [string, number, { pointsScored: number }]
        > = true;
        expect(result).to.be.equal(true);
    });

    describe('rest', () => {
        it('Should not have max limit if rest defined', () => {
            expect(
                athleteSchema
                    .rest(NumberType.create())
                    .safeParse(['foo', 12, { pointsScored: 2 }, 3]).success
            ).to.be.equal(true);
        });

        it('Should parse the rest', () => {
            expect(
                athleteSchema
                    .rest(NumberType.create())
                    .safeParse(['foo', 12, { pointsScored: 2 }, 3, 3]).success
            ).to.be.equal(true);
            expect(
                athleteSchema
                    .rest(NumberType.create())
                    .safeParse(['foo', 12, { pointsScored: 2 }, 3, 'foo'])
                    .success
            ).to.be.equal(false);
        });

        it("Should ignore the rest if they don't have extra items", () => {
            expect(
                athleteSchema
                    .rest(NumberType.create())
                    .safeParse(['foo', 12, { pointsScored: 2 }]).success
            ).to.be.equal(true);
        });
    });
});
