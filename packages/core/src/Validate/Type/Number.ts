/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import { FirstPartyTypeKind, RawCreateParams, Type, TypeDef } from './Type';

import {
    INVALID,
    ParseInput,
    ParseReturnType,
    ParsedType,
    ParseStatus,
    ParseContext,
} from '../helpers/parseUtils';

type NumberCheck =
    | { kind: 'min'; value: number; inclusive: boolean; message?: string }
    | { kind: 'max'; value: number; inclusive: boolean; message?: string }
    | { kind: 'int'; message?: string }
    | { kind: 'multipleOf'; value: number; message?: string };

export type NumberDef = {
    typeName: FirstPartyTypeKind.NumberType;
    checks: NumberCheck[];
} & TypeDef;

/**
 * Number validation
 */
export class NumberType extends Type<number, NumberDef> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const parsedType = this._getType(input);
        if (parsedType !== ParsedType.number) {
            const ctx = this._getOrReturnCtx(input);
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: ParsedType.number,
                received: ctx.parsedType,
            });
            return INVALID;
        }

        let ctx: undefined | ParseContext = undefined;
        const status = new ParseStatus();

        for (const check of this._def.checks) {
            if (check.kind === 'int') {
                if (!Number.isInteger(input.data)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    this._addIssueToContext(ctx, {
                        code: IssueCode.invalidType,
                        expected: 'integer',
                        received: 'float',
                        message: check.message,
                    });
                    status.dirty();
                }
            } else if (check.kind === 'min') {
                const tooSmall = check.inclusive
                    ? input.data < check.value
                    : input.data <= check.value;
                if (tooSmall) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    this._addIssueToContext(ctx, {
                        code: IssueCode.tooSmall,
                        minimum: check.value,
                        type: 'number',
                        inclusive: check.inclusive,
                        message: check.message,
                    });
                    status.dirty();
                }
            } else if (check.kind === 'max') {
                const tooBig = check.inclusive
                    ? input.data > check.value
                    : input.data >= check.value;
                if (tooBig) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    this._addIssueToContext(ctx, {
                        code: IssueCode.tooBig,
                        maximum: check.value,
                        type: 'number',
                        inclusive: check.inclusive,
                        message: check.message,
                    });
                    status.dirty();
                }
            } else if (check.kind === 'multipleOf') {
                if (this._floatSafeRemainder(input.data, check.value) !== 0) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    this._addIssueToContext(ctx, {
                        code: IssueCode.notMultipleOf,
                        multipleOf: check.value,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
        }

        return { status: status.value, value: input.data };
    }

    static create = (params?: RawCreateParams): NumberType => {
        return new NumberType({
            typeName: FirstPartyTypeKind.NumberType,
            checks: [],
            ...NumberType.processCreateParams(params),
        });
    };

    /**
     * The number need to be more or equal than `minValue`
     */
    min(minValue: number, message?: string | { message: string }) {
        return this.gte(minValue, message);
    }

    /**
     * The number need to be less or equal than `maxValue`
     */
    max(maxValue: number, message?: string | { message: string }) {
        return this.lte(maxValue, message);
    }

    /**
     * The number need to be more than `minValue` (not include the value)
     */
    gt(minValue: number, message?: string | { message: string }) {
        return this._setLimit(
            'min',
            minValue,
            false,
            typeof message === 'string' ? message : message?.message
        );
    }

    /**
     * The number need to be more or equal than `minValue`
     */
    gte(minValue: number, message?: string | { message: string }) {
        return this._setLimit(
            'min',
            minValue,
            true,
            typeof message === 'string' ? message : message?.message
        );
    }

    /**
     * The number need to be less than `maxValue` (not include the value)
     */
    lt(maxValue: number, message?: string | { message: string }) {
        return this._setLimit(
            'max',
            maxValue,
            false,
            typeof message === 'string' ? message : message?.message
        );
    }

    /**
     * The number need to be less or equal than `maxValue`
     */
    lte(maxValue: number, message?: string | { message: string }) {
        return this._setLimit(
            'max',
            maxValue,
            true,
            typeof message === 'string' ? message : message?.message
        );
    }

    /**
     * The number need to be an integer
     */
    int(message?: string | { message: string }) {
        return this._addCheck({
            kind: 'int',
            ...(typeof message === 'string' ? { message } : message || {}),
        });
    }

    /**
     * The number need to be positive (not include 0)
     */
    positive(message?: string | { message: string }) {
        return this._setLimit(
            'min',
            0,
            false,
            typeof message === 'string' ? message : message?.message
        );
    }

    /**
     * The number need to be negative or 0
     */
    nonNegative(message?: string | { message: string }) {
        return this._setLimit(
            'max',
            0,
            true,
            typeof message === 'string' ? message : message?.message
        );
    }

    /**
     * The number need to be negative (not include 0)
     */
    negative(message?: string | { message: string }) {
        return this._setLimit(
            'max',
            0,
            false,
            typeof message === 'string' ? message : message?.message
        );
    }

    /**
     * The number need to be positive or 0
     */
    nonPositive(message?: string | { message: string }) {
        return this._setLimit(
            'min',
            0,
            true,
            typeof message === 'string' ? message : message?.message
        );
    }

    /**
     * The number need to be an multiple of `value`
     */
    multipleOf(value: number, message?: string | { message: string }) {
        return this._addCheck({
            kind: 'multipleOf',
            value: value,
            ...(typeof message === 'string' ? { message } : message || {}),
        });
    }

    /**
     * Return if this type is an email address
     */
    get isInt() {
        return !!this._def.checks.find(ch => ch.kind === 'int');
    }

    /**
     * Return if this type is an email address
     */
    get minValue() {
        let min: number | null = null;
        for (const ch of this._def.checks) {
            if (ch.kind === 'min') {
                if (min === null || ch.value > min) min = ch.value;
            }
        }
        return min;
    }

    /**
     * Return if this type is an email address
     */
    get maxValue() {
        let max: number | null = null;
        for (const ch of this._def.checks) {
            if (ch.kind === 'max') {
                if (max === null || ch.value < max) max = ch.value;
            }
        }
        return max;
    }

    /**
     * Set a limit (min or max)
     */
    protected _setLimit(
        kind: 'min' | 'max',
        value: number,
        inclusive: boolean,
        message?: string
    ) {
        return new NumberType({
            ...this._def,
            checks: [
                ...this._def.checks,
                {
                    kind,
                    value,
                    inclusive,
                    message,
                },
            ],
        });
    }

    /**
     * Add an verification
     */
    protected _addCheck(check: NumberCheck) {
        return new NumberType({
            ...this._def,
            checks: [...this._def.checks, check],
        });
    }

    /**
     * Safe division, modulo ?
     */
    protected _floatSafeRemainder(val: number, step: number) {
        const valDecCount = (val.toString().split('.')[1] || '').length;
        const stepDecCount = (step.toString().split('.')[1] || '').length;
        const decCount =
            valDecCount > stepDecCount ? valDecCount : stepDecCount;
        const valInt = parseInt(val.toFixed(decCount).replace('.', ''));
        const stepInt = parseInt(step.toFixed(decCount).replace('.', ''));
        return (valInt % stepInt) / Math.pow(10, decCount);
    }
}
