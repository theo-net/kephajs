/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { TypeOf } from './Type';
import { UndefinedType } from './Undefined';

describe('@kephajs/core/Validate/Type/Undefined', () => {
    it('Should parse UndefinedType string', () => {
        const f = () => UndefinedType.create().parse('foobar');
        expect(f).to.throw();
    });

    it('Should parse UndefinedType number', () => {
        const f = () => UndefinedType.create().parse(123);
        expect(f).to.throw();
    });

    it('Should parse UndefinedType boolean', () => {
        const f = () => UndefinedType.create().parse(true);
        expect(f).to.throw();
    });

    it('Should parse UndefinedType undefined', () => {
        expect(UndefinedType.create().parse(undefined)).to.be.equal(undefined);
    });

    it('Should parse UndefinedType null', () => {
        const f = () => UndefinedType.create().parse(null);
        expect(f).to.throw();
    });

    it('Should infer UndefinedType', () => {
        const MyUndefined = UndefinedType.create();
        type MyUndefinedType = TypeOf<typeof MyUndefined>;
        const result: AssertEqual<MyUndefinedType, undefined> = true;
        expect(result).to.be.equal(true);
    });
});
