/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { TypeOf } from './Type';
import { VoidType } from './Void';

describe('@kephajs/core/Validate/Type/Void', () => {
    it('Should parse VoidType string', () => {
        const f = () => VoidType.create().parse('foobar');
        expect(f).to.throw();
    });

    it('Should parse VoidType number', () => {
        const f = () => VoidType.create().parse(123);
        expect(f).to.throw();
    });

    it('Should parse VoidType boolean', () => {
        const f = () => VoidType.create().parse(true);
        expect(f).to.throw();
    });

    it('Should parse VoidType undefined', () => {
        expect(VoidType.create().parse(undefined)).to.be.equal(undefined);
    });

    it('Should parse VoidType null', () => {
        const f = () => VoidType.create().parse(null);
        expect(f).to.throw();
    });

    it('Should infer VoidType', () => {
        const MyVoid = VoidType.create();
        type MyVoidType = TypeOf<typeof MyVoid>;
        const result: AssertEqual<MyVoidType, void> = true;
        expect(result).to.be.equal(true);
    });
});
