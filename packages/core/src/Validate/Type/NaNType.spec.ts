/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { NaNType } from './NaNType';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/NaNType', () => {
    it('Should parse NaNType string', () => {
        const f = () => NaNType.create().parse('foobar');
        expect(f).to.throw();
    });

    it('Should parse NaNType number', () => {
        const f = () => NaNType.create().parse(123);
        expect(f).to.throw();
    });

    it('Should parse NaNType boolean', () => {
        const f = () => NaNType.create().parse(true);
        expect(f).to.throw();
    });

    it('Should parse NaNType undefined', () => {
        const f = () => NaNType.create().parse(undefined);
        expect(f).to.throw();
    });

    it('Should parse NaNType null', () => {
        const f = () => NaNType.create().parse(null);
        expect(f).to.throw();
    });

    it('Should parse NaNType NaN', () => {
        expect(NaNType.create().safeParse(NaN).success).to.be.equal(true);
        expect(
            NaNType.create().safeParse(Number('Not a number')).success
        ).to.be.equal(true);
    });

    it('Should infer NaNType', () => {
        const MyNaN = NaNType.create();
        type MyNaNType = TypeOf<typeof MyNaN>;
        const result: AssertEqual<MyNaNType, typeof NaN> = true;
        expect(result).to.be.equal(true);
    });
});
