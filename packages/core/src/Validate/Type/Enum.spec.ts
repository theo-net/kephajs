/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { EnumType } from './Enum';
import { IssueCode } from '../Validator';
import { TypeOf } from './Type';
import { AssertEqual } from '../helpers/testUtils';

describe('@kephajs/core/Validate/Type/Enum', () => {
    it('Should parse EnumType string', () => {
        const f = () => EnumType.create(['foo', 'bar']).parse('foobar');
        expect(f).to.throw();
    });

    it('Should parse EnumType number', () => {
        const f = () => EnumType.create(['foo', 'bar']).parse(123);
        expect(f).to.throw();
    });

    it('Should parse EnumType boolean', () => {
        const f = () => EnumType.create(['foo', 'bar']).parse(true);
        expect(f).to.throw();
    });

    it('Should parse EnumType undefined', () => {
        const f = () => EnumType.create(['foo', 'bar']).parse(undefined);
        expect(f).to.throw();
    });

    it('Should parse EnumType null', () => {
        const f = () => EnumType.create(['foo', 'bar']).parse(null);
        expect(f).to.throw();
    });

    it('Should success', () => {
        expect(EnumType.create(['foo', 'bar']).parse('foo')).to.be.equal('foo');
    });

    it('Should fail', () => {
        expect(
            EnumType.create(['foo', 'bar']).safeParse('foobar').success
        ).to.be.equal(false);
        const result = EnumType.create(['foo', 'bar']).safeParse(1);
        expect(result.success).to.be.equal(false);
        if (!result.success) {
            expect(result.error.errors[0].code).to.be.equal(
                IssueCode.invalidType
            );
            expect(result.error.errors[0].message).to.be.equal(
                "Expected 'foo' | 'bar', received number"
            );
        }
    });

    it('Should use as const to define your enum values as a tuple of strings', () => {
        const values = ['foo1', 'bar1'] as const;
        expect(EnumType.create(values).options).to.be.equal(values);
    });

    describe('enum', () => {
        it('Should get a value from the enum', () => {
            const MyEnum = EnumType.create(['foo', 'bar']);
            expect(MyEnum.enum.foo).to.be.equal('foo');
            expect(MyEnum.enum.bar).to.be.equal('bar');
        });
    });

    describe('options', () => {
        it('Should get a value from the enum', () => {
            expect(EnumType.create(['foo', 'bar']).options).to.be.deep.equal([
                'foo',
                'bar',
            ]);
        });
    });

    it('Should infer EnumType', () => {
        const MyEnum = EnumType.create(['foo', 'bar']);
        type MyEnumType = TypeOf<typeof MyEnum>;
        const result: AssertEqual<MyEnumType, 'foo' | 'bar'> = true;
        expect(result).to.be.equal(true);
    });
});
