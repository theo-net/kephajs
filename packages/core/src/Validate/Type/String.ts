/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import { FirstPartyTypeKind, RawCreateParams, Type, TypeDef } from './Type';

import {
    INVALID,
    ParseInput,
    ParseReturnType,
    ParsedType,
    ParseContext,
    ParseStatus,
} from '../helpers/parseUtils';

type StringCheck =
    | { kind: 'min'; value: number; message?: string }
    | { kind: 'max'; value: number; message?: string }
    | { kind: 'email'; message?: string }
    | { kind: 'url'; message?: string }
    | { kind: 'uuid'; message?: string }
    | { kind: 'cuid'; message?: string }
    | { kind: 'regex'; regex: RegExp; message?: string };

export type StringDef = {
    typeName: FirstPartyTypeKind.StringType;
    checks: StringCheck[];
} & TypeDef;

const emailRegex =
    // eslint-disable-next-line
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
const cuidRegex = /^c[^\s-]{8,}$/i;
const uuidRegex =
    /^([a-f0-9]{8}-[a-f0-9]{4}-[1-5][a-f0-9]{3}-[a-f0-9]{4}-[a-f0-9]{12}|00000000-0000-0000-0000-000000000000)$/i;

/**
 * String validation
 */
export class StringType extends Type<string, StringDef> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const parsedType = this._getType(input);
        if (parsedType !== ParsedType.string) {
            const ctx = this._getOrReturnCtx(input);
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: ParsedType.string,
                received: ctx.parsedType,
            });
            return INVALID;
        }

        const status = new ParseStatus();
        let ctx: undefined | ParseContext = undefined;

        for (const check of this._def.checks) {
            if (check.kind === 'min') {
                if (input.data.length < check.value) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    this._addIssueToContext(ctx, {
                        code: IssueCode.tooSmall,
                        minimum: check.value,
                        type: 'string',
                        inclusive: true,
                        message: check.message,
                    });
                    status.dirty();
                }
            } else if (check.kind === 'max') {
                if (input.data.length > check.value) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    this._addIssueToContext(ctx, {
                        code: IssueCode.tooBig,
                        maximum: check.value,
                        type: 'string',
                        inclusive: true,
                        message: check.message,
                    });
                    status.dirty();
                }
            } else if (check.kind === 'email') {
                if (!emailRegex.test(input.data)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    this._addIssueToContext(ctx, {
                        validation: 'email',
                        code: IssueCode.invalidString,
                        message: check.message,
                    });
                    status.dirty();
                }
            } else if (check.kind === 'uuid') {
                if (!uuidRegex.test(input.data)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    this._addIssueToContext(ctx, {
                        validation: 'uuid',
                        code: IssueCode.invalidString,
                        message: check.message,
                    });
                    status.dirty();
                }
            } else if (check.kind === 'cuid') {
                if (!cuidRegex.test(input.data)) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    this._addIssueToContext(ctx, {
                        validation: 'cuid',
                        code: IssueCode.invalidString,
                        message: check.message,
                    });
                    status.dirty();
                }
            } else if (check.kind === 'url') {
                try {
                    new URL(input.data);
                } catch {
                    ctx = this._getOrReturnCtx(input, ctx);
                    this._addIssueToContext(ctx, {
                        validation: 'url',
                        code: IssueCode.invalidString,
                        message: check.message,
                    });
                    status.dirty();
                }
            } else if (check.kind === 'regex') {
                check.regex.lastIndex = 0;
                const testResult = check.regex.test(input.data);
                if (!testResult) {
                    ctx = this._getOrReturnCtx(input, ctx);
                    this._addIssueToContext(ctx, {
                        validation: 'regex',
                        code: IssueCode.invalidString,
                        message: check.message,
                    });
                    status.dirty();
                }
            }
        }

        return { status: status.value, value: input.data };
    }

    static create = (params?: RawCreateParams): StringType => {
        return new StringType({
            typeName: FirstPartyTypeKind.StringType,
            checks: [],
            ...StringType.processCreateParams(params),
        });
    };

    /**
     * The string need to have more than `min` characters
     */
    min(minLength: number, message?: string | { message: string }) {
        return this._addCheck({
            kind: 'min',
            value: minLength,
            ...(typeof message === 'string' ? { message } : message || {}),
        });
    }

    /**
     * The string need to have less than `max` characters
     */
    max(maxLength: number, message?: string | { message: string }) {
        return this._addCheck({
            kind: 'max',
            value: maxLength,
            ...(typeof message === 'string' ? { message } : message || {}),
        });
    }

    /**
     * The string need to have exactly `length` characters
     */
    length(length: number, message?: string | { message: string }) {
        return this.min(length, message).max(length, message);
    }

    /**
     * Check if the string is an valid email
     */
    email(message?: string | { message: string }) {
        return this._addCheck({
            kind: 'email',
            ...(typeof message === 'string' ? { message } : message || {}),
        });
    }

    /**
     * Check if the string is an valid Url
     */
    url(message?: string | { message: string }) {
        return this._addCheck({
            kind: 'url',
            ...(typeof message === 'string' ? { message } : message || {}),
        });
    }

    /**
     * Check if the string is an valid UUID
     */
    uuid(message?: string | { message: string }) {
        return this._addCheck({
            kind: 'uuid',
            ...(typeof message === 'string' ? { message } : message || {}),
        });
    }

    /**
     * Check if the string is an valid CUID
     */
    cuid(message?: string | { message: string }) {
        return this._addCheck({
            kind: 'cuid',
            ...(typeof message === 'string' ? { message } : message || {}),
        });
    }

    /**
     * Check if the string pass a RegExp
     */
    regexp(regexp: RegExp, message?: string | { message: string }) {
        return this._addCheck({
            kind: 'regex',
            regex: regexp,
            ...(typeof message === 'string' ? { message } : message || {}),
        });
    }

    /**
     * Return if this type is an email address
     */
    get isEmail() {
        return !!this._def.checks.find(ch => ch.kind === 'email');
    }

    /**
     * Return if this type is an URL
     */
    get isUrl() {
        return !!this._def.checks.find(ch => ch.kind === 'url');
    }

    /**
     * Return if this type is a UUID
     */
    get isUuid() {
        return !!this._def.checks.find(ch => ch.kind === 'uuid');
    }

    /**
     * Return if this type is a CUID
     */
    get isCuid() {
        return !!this._def.checks.find(ch => ch.kind === 'cuid');
    }

    /**
     * Return the minimum length
     */
    get minLength() {
        let min: number | null = -Infinity;
        this._def.checks.map(ch => {
            if (ch.kind === 'min') {
                if (min === null || ch.value > min) {
                    min = ch.value;
                }
            }
        });
        return min;
    }

    /**
     * Return the maximum length
     */
    get maxLength() {
        let max: number | null = null;
        this._def.checks.map(ch => {
            if (ch.kind === 'max') {
                if (max === null || ch.value < max) {
                    max = ch.value;
                }
            }
        });
        return max;
    }

    /**
     * Add an verification
     */
    protected _addCheck(check: StringCheck) {
        return new StringType({
            ...this._def,
            checks: [...this._def.checks, check],
        });
    }
}
