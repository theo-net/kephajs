/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import { FirstPartyTypeKind, RawCreateParams, Type, TypeDef } from './Type';

import {
    INVALID,
    ParseInput,
    ParseReturnType,
    ParsedType,
} from '../helpers/parseUtils';

export type NeverDef = {
    typeName: FirstPartyTypeKind.NeverType;
} & TypeDef;

/**
 * Never type, allows no values
 */
export class NeverType extends Type<never, NeverDef> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const ctx = this._getOrReturnCtx(input);
        this._addIssueToContext(ctx, {
            code: IssueCode.invalidType,
            expected: ParsedType.never,
            received: ctx.parsedType,
        });
        return INVALID;
    }

    static create = (params?: RawCreateParams): NeverType => {
        return new NeverType({
            typeName: FirstPartyTypeKind.NeverType,
            ...NeverType.processCreateParams(params),
        });
    };
}
