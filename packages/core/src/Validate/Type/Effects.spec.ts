/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import { IssueCode } from '../Error/Error';

import { AssertEqual } from '../helpers/testUtils';
import { Validator } from '../Validator';
import { BooleanType } from './Boolean';
import { NumberType } from './Number';
import { ObjectType } from './Object';
import { StringType } from './String';
import { InputType, OutputType } from './Type';

describe('@kephajs/core/Validate/Type/Effects', () => {
    const stringToNumber = StringType.create().transform(arg =>
        parseFloat(arg)
    );

    it('transform ctx.addIssue with parse', () => {
        const strs = ['foo', 'bar'];

        expect(() => {
            StringType.create()
                .transform((data, ctx) => {
                    const i = strs.indexOf(data);
                    if (i === -1) {
                        ctx.addIssue({
                            code: 'custom',
                            message: `${data} is not one of our allowed strings`,
                        });
                    }
                    return data.length;
                })
                .parse('asdf');
        }).to.throw(
            JSON.stringify(
                [
                    {
                        code: 'custom',
                        message: 'asdf is not one of our allowed strings',
                        path: [],
                    },
                ],
                null,
                2
            )
        );
    });

    it('basic transformations', () => {
        const r1 = StringType.create()
            .transform(data => data.length)
            .parse('asdf');
        expect(r1).to.be.equal(4);
    });

    it('coercion', () => {
        const numToString = NumberType.create().transform(n => String(n));
        const data = ObjectType.create({
            id: numToString,
        }).parse({ id: 5 });

        expect(data).to.be.deep.equal({ id: '5' });
    });

    it('default', () => {
        const data = StringType.create().default('asdf').parse(undefined); // => "asdf"
        expect(data).to.be.equal('asdf');
    });

    it('dynamic default', () => {
        const data = StringType.create()
            .default(() => 'string')
            .parse(undefined); // => "asdf"
        expect(data).to.be.equal('string');
    });

    it('default when property is null or undefined', () => {
        const data = ObjectType.create({
            foo: BooleanType.create().nullable().default(true),
            bar: BooleanType.create().default(true),
        }).parse({ foo: null });

        expect(data).to.be.deep.equal({ foo: null, bar: true });
    });

    it('default with falsy values', () => {
        const schema = ObjectType.create({
            emptyStr: StringType.create().default('def'),
            zero: NumberType.create().default(5),
            falseBoolean: BooleanType.create().default(true),
        });
        const input = { emptyStr: '', zero: 0, falseBoolean: true };
        const output = schema.parse(input);
        // defaults are not supposed to be used
        expect(output).to.be.deep.equal(input);
    });

    it('object typing', () => {
        const t1 = ObjectType.create({
            stringToNumber,
        });

        type T1 = InputType<typeof t1>;
        type T2 = OutputType<typeof t1>;

        const f1: AssertEqual<T1, { stringToNumber: string }> = true;
        const f2: AssertEqual<T2, { stringToNumber: number }> = true;
        expect([f1, f2]).to.be.deep.equal([true, true]);
    });

    it('transform method overloads', () => {
        const t1 = StringType.create().transform(val => val.toUpperCase());
        expect(t1.parse('asdf')).to.be.equal('ASDF');

        const t2 = StringType.create().transform(val => val.length);
        expect(t2.parse('asdf')).to.be.equal(4);
    });

    it('multiple transformers', () => {
        const doubler = stringToNumber.transform(val => {
            return val * 2;
        });
        expect(doubler.parse('5')).to.be.equal(10);
    });

    it('preprocess', () => {
        const schema = new Validator().preprocess(
            data => [data],
            StringType.create().array()
        );

        const value = schema.parse('asdf');
        expect(value).to.be.deep.equal(['asdf']);
    });

    it('short circuit on dirty', () => {
        const schema = StringType.create()
            .refine(() => false)
            .transform(val => val.toUpperCase());
        const result = schema.safeParse('asdf');
        expect(result.success).to.be.equal(false);
        if (!result.success) {
            expect(result.error.errors[0].code).to.be.equal(IssueCode.custom);
        }

        const result2 = schema.safeParse(1234);
        expect(result2.success).to.be.equal(false);
        if (!result2.success) {
            expect(result2.error.errors[0].code).to.be.equal(
                IssueCode.invalidType
            );
        }
    });
});
