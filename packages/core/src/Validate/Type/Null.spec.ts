/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { NullType } from './Null';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Null', () => {
    it('Should parse NullType string', () => {
        const f = () => NullType.create().parse('foobar');
        expect(f).to.throw();
    });

    it('Should parse NullType number', () => {
        const f = () => NullType.create().parse(123);
        expect(f).to.throw();
    });

    it('Should parse NullType boolean', () => {
        const f = () => NullType.create().parse(true);
        expect(f).to.throw();
    });

    it('Should parse NullType undefined', () => {
        const f = () => NullType.create().parse(undefined);
        expect(f).to.throw();
    });

    it('Should parse NullType null', () => {
        expect(NullType.create().parse(null)).to.be.equal(null);
    });

    it('Should infer NullType', () => {
        const MyNull = NullType.create();
        type MyNullType = TypeOf<typeof MyNull>;
        const result: AssertEqual<MyNullType, null> = true;
        expect(result).to.be.equal(true);
    });
});
