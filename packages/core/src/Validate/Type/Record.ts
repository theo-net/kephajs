/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import {
    FirstPartyTypeKind,
    RawCreateParams,
    Type,
    TypeAny,
    TypeDef,
} from './Type';

import {
    INVALID,
    ParseInput,
    ParseReturnType,
    ParsedType,
    ParseStatus,
    ParseInputLazyPath,
} from '../helpers/parseUtils';
import { StringType } from './String';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type KeySchema = Type<string | number | symbol, any, any>;
type RecordTypeType<K extends string | number | symbol, V> = [string] extends [
    K
]
    ? Record<K, V>
    : [number] extends [K]
    ? Record<K, V>
    : [symbol] extends [K]
    ? Record<K, V>
    : Partial<Record<K, V>>;

export type RecordDef<
    Key extends KeySchema = StringType,
    Value extends TypeAny = TypeAny
> = {
    typeName: FirstPartyTypeKind.Record;
    keyType: Key;
    valueType: Value;
} & TypeDef;

/**
 * Record schemas are used to validate types such as `{ [k: string]: number }`.
 */
export class RecordType<
    Key extends KeySchema = StringType,
    Value extends TypeAny = TypeAny
> extends Type<
    RecordTypeType<Key['_output'], Value['_output']>,
    RecordDef<Key, Value>,
    RecordTypeType<Key['_input'], Value['_input']>
> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const { status, ctx } = this._processInputParams(input);

        if (ctx.parsedType !== ParsedType.object) {
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: ParsedType.object,
                received: ctx.parsedType,
            });
            return INVALID;
        }

        const pairs: {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            key: ParseReturnType<any>;
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            value: ParseReturnType<any>;
        }[] = [];

        for (const key in ctx.data) {
            pairs.push({
                key: this._def.keyType._parse(
                    new ParseInputLazyPath(ctx, key, ctx.path, key)
                ),
                value: this._def.valueType._parse(
                    new ParseInputLazyPath(ctx, ctx.data[key], ctx.path, key)
                ),
            });
        }

        return ParseStatus.mergeObject(status, pairs);
    }

    static create<V extends TypeAny>(
        valueType: V,
        params?: RawCreateParams
    ): RecordType<StringType, V>;
    static create<K extends KeySchema, V extends TypeAny>(
        keySchema: K,
        valueType: V,
        params?: RawCreateParams
    ): RecordType<K, V>;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    static create(first: any, second?: any, third?: any): RecordType<any, any> {
        if (second instanceof Type) {
            return new RecordType({
                typeName: FirstPartyTypeKind.Record,
                keyType: first,
                valueType: second,
                ...RecordType.processCreateParams(third),
            });
        }

        return new RecordType({
            typeName: FirstPartyTypeKind.Record,
            keyType: StringType.create(),
            valueType: first,
            ...RecordType.processCreateParams(second),
        });
    }

    get keySchema() {
        return this._def.keyType;
    }

    get valueSchema() {
        return this._def.valueType;
    }
}
