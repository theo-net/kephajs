/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { BooleanType } from './Boolean';
import { EnumType } from './Enum';
import { LiteralType } from './Literal';
import { NumberType } from './Number';
import { RecordType } from './Record';
import { StringType } from './String';
import { TypeOf } from './Type';
import { Union } from './Union';

describe('@kephajs/core/Validate/Type/Record', () => {
    const booleanRecord = RecordType.create(BooleanType.create());
    type BooleanRecordType = TypeOf<typeof booleanRecord>;

    const recordWithEnumKeys = RecordType.create(
        EnumType.create(['Tuna', 'Salmon']),
        StringType.create()
    );
    type RecordWithEnumKeysType = TypeOf<typeof recordWithEnumKeys>;

    const recordWithLiteralKeys = RecordType.create(
        Union.create([
            LiteralType.create('Tuna'),
            LiteralType.create('Salmon'),
        ]),
        StringType.create()
    );
    type RecordWithLiteralKeysType = TypeOf<typeof recordWithLiteralKeys>;

    it('Should type inference', () => {
        const f1: AssertEqual<
            BooleanRecordType,
            Record<string, boolean>
        > = true;
        expect(f1).to.be.equal(true);

        const f2: AssertEqual<
            RecordWithEnumKeysType,
            Partial<Record<'Tuna' | 'Salmon', string>>
        > = true;
        expect(f2).to.be.equal(true);
        const f3: AssertEqual<
            RecordWithLiteralKeysType,
            Partial<Record<'Tuna' | 'Salmon', string>>
        > = true;
        expect(f3).to.be.equal(true);
    });

    it('Should string record parse - pass', () => {
        expect(
            booleanRecord.parse({
                k1: true,
                k2: false,
                1234: false,
            })
        ).to.be.deep.equal({
            k1: true,
            k2: false,
            1234: false,
        });
    });

    it('Should string record parse - fail', () => {
        const badCheck = () =>
            booleanRecord.parse({
                asdf: 1234,
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
            } as any);
        expect(badCheck).to.throw();

        expect(() => booleanRecord.parse('asdf')).to.throw();
    });

    it('Should string record parse - fail', () => {
        const badCheck = () =>
            booleanRecord.parse({
                asdf: {},
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
            } as any);
        expect(badCheck).to.throw();
    });

    it('Should string record parse - fail', () => {
        const badCheck = () =>
            booleanRecord.parse({
                asdf: [],
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
            } as any);
        expect(badCheck).to.throw();
    });

    it('Should key schema', () => {
        const result1 = recordWithEnumKeys.parse({
            Tuna: 'asdf',
            Salmon: 'asdf',
        });
        expect(result1).to.be.deep.equal({
            Tuna: 'asdf',
            Salmon: 'asdf',
        });

        const result2 = recordWithLiteralKeys.parse({
            Tuna: 'asdf',
            Salmon: 'asdf',
        });
        expect(result2).to.be.deep.equal({
            Tuna: 'asdf',
            Salmon: 'asdf',
        });

        // shouldn't require us to specify all props in record
        const result3 = recordWithEnumKeys.parse({
            Tuna: 'abcd',
        });
        expect(result3).to.be.deep.equal({
            Tuna: 'abcd',
        });

        // shouldn't require us to specify all props in record
        const result4 = recordWithLiteralKeys.parse({
            Salmon: 'abcd',
        });
        expect(result4).to.be.deep.equal({
            Salmon: 'abcd',
        });

        expect(() =>
            recordWithEnumKeys.parse({
                Tuna: 'asdf',
                Salmon: 'asdf',
                Trout: 'asdf',
            })
        ).to.throw();

        expect(() =>
            recordWithLiteralKeys.parse({
                Tuna: 'asdf',
                Salmon: 'asdf',

                Trout: 'asdf',
            })
        ).to.throw();
    });

    it('Should key and value getters', () => {
        const rec = RecordType.create(StringType.create(), NumberType.create());

        expect(rec.keySchema.parse('asdf')).to.be.equal('asdf');
        expect(rec.valueSchema.parse(1234)).to.be.equal(1234);
    });
});
