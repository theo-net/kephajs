/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { BigIntType } from './BigInt';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/BigInt', () => {
    it('Should parse BigIntType string', () => {
        const f = () => BigIntType.create().parse('foobar');
        expect(f).to.throw();
    });

    it('Should parse BigIntType number', () => {
        const f = () => BigIntType.create().parse(123);
        expect(f).to.throw();
    });

    it('Should parse BigIntType boolean', () => {
        const f = () => BigIntType.create().parse(true);
        expect(f).to.throw();
    });

    it('Should parse BigIntType undefined', () => {
        const f = () => BigIntType.create().parse(undefined);
        expect(f).to.throw();
    });

    it('Should parse BigIntType null', () => {
        const f = () => BigIntType.create().parse(null);
        expect(f).to.throw();
    });

    it('Should parse BigIntType BigInt', () => {
        expect(BigIntType.create().parse(BigInt(123))).to.be.equal(BigInt(123));
    });

    it('Should infer BigIntType', () => {
        const MyBigInt = BigIntType.create();
        type MyBigIntType = TypeOf<typeof MyBigInt>;
        const result: AssertEqual<MyBigIntType, bigint> = true;
        expect(result).to.be.equal(true);
    });
});
