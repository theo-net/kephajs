/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    CustomIssue,
    ErrorValidator,
    Issue,
    IssueCode,
    IssueData,
} from './../Error/Error';
import {
    defaultErrorMap,
    ErrorMap,
    overrideErrorMap,
} from './../Error/ErrorMap';

import {
    getParsedType,
    isValid,
    ParseContext,
    ParsedType,
    ParseInput,
    ParseParams,
    ParseReturnType,
    ParseStatus,
} from '../helpers/parseUtils';

export type Primitive = string | number | bigint | boolean | null | undefined;
export type Scalars = Primitive | Primitive[];

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type TypeAny = Type<any, any, any>;
export type RawShape = { [k: string]: TypeAny };

export type RefinementCtx = {
    addIssue: (arg: IssueData) => void;
    path: (string | number)[];
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type TypeOf<T extends Type<any, any, any>> = T['_output'];

export enum FirstPartyTypeKind {
    StringType = 'StringType',
    NumberType = 'NumberType',
    NaNType = 'NaNType',
    BigIntType = 'BigIntType',
    BooleanType = 'BooleanType',
    DateType = 'DateType',
    UndefinedType = 'UndefinedType',
    NullType = 'NullType',
    AnyType = 'AnyType',
    UnknownType = 'UnknownType',
    NeverType = 'NeverType',
    VoidType = 'VoidType',
    ArrayType = 'ArrayType',
    ObjectType = 'ObjectType',
    Union = 'Union',
    DiscriminatedUnion = 'DiscriminatedUnion',
    Intersection = 'Intersection',
    Tuple = 'Tuple',
    Record = 'Record',
    MapType = 'MapType',
    SetType = 'SetType',
    FunctionType = 'FunctionType',
    Lazy = 'Lazy',
    Literal = 'Literal',
    Enum = 'Enum',
    Effects = 'Effects',
    NativeEnum = 'NativeEnum',
    Optional = 'Optional',
    Nullable = 'Nullable',
    Default = 'Default',
    PromiseType = 'PromiseType',
}

export type TypeDef = {
    typeName: FirstPartyTypeKind;
    errorMap?: ErrorMap;
};

export type RawCreateParams =
    | {
          errorMap?: ErrorMap;
          invalidTypeError?: string;
          requiredError?: string;
      }
    | undefined;

export type SafeParseSuccess<Output> = { success: true; data: Output };
export type SafeParseError<Input> = {
    success: false;
    error: ErrorValidator<Input>;
};
export type SafeParseReturnType<Input, Output> =
    | SafeParseSuccess<Output>
    | SafeParseError<Input>;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type InputType<T extends Type<any, any, any>> = T['_input'];
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type OutputType<T extends Type<any, any, any>> = T['_output'];

export type NoUndefined<T> = T extends undefined ? never : T;

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export type CustomErrorParams = Partial<Omit<CustomIssue, 'code'>>;

export abstract class Type<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Output = any,
    Def extends TypeDef = TypeDef,
    Input = Output
> {
    readonly _output!: Output;

    readonly _input!: Input;

    readonly _def!: Def;

    constructor(def: Def) {
        this._def = def;
    }

    /**
     * All Type object implements this method. You need to return the `INVALID`
     * constant or the result of the OK function (from `parseUtils`).
     */
    abstract _parse(input: ParseInput): ParseReturnType<Output>;

    /**
     * Parse the values, but fon'y throw errors when validation fails.
     *
     * If you don't want Validator to throw errors when validation fails, use `.safeParse`.
     * This method returns an object containing either the successfully parsed data or a
     * `ErrorValidator` instance containing detailed information about the validation problems.
     *
     *      stringSchema.safeParse(12);
     *      // => { success: false; error: ErrorValidator }
     *
     *      stringSchema.safeParse('billie');
     *      // => { success: true; data: 'billie' }
     *
     * The result is a discriminated union so you can handle errors very conveniently:
     *
     *      const result = stringSchema.safeParse('billie');
     *      if (!result.success) {
     *          // handle error then return
     *          result.error;
     *      } else {
     *          // do something
     *          result.data;
     *      }
     *
     * @returns An object containing either the successfully parsed data or a
     *          ErrorValidator instance containing detailed information about
     *          the validation problems.
     */
    safeParse(
        data: unknown,
        params?: Partial<ParseParams>
    ): SafeParseReturnType<Input, Output> {
        const ctx: ParseContext = {
            common: {
                issues: [],
                contextualErrorMap: params?.errorMap,
            },
            path: params?.path || [],
            schemaErrorMap: this._def.errorMap,
            parent: null,
            data,
            parsedType: getParsedType(data),
        };
        const result = this._parse({
            data,
            path: ctx.path,
            parent: ctx,
        });

        return this._handleResult(ctx, result);
    }

    /**
     * Check the data. If it's valid, a value is returned with full type information.
     * Otherwise, an error is thrown.
     *
     * IMPORTANT: The value returned by .parse is a deep clone of the variable you passed in.
     *
     *      const stringSchema = StringType.create();
     *      stringSchema.parse('fish'); // => returns 'fish'
     *      stringSchema.parse(12); // throws Error('Non-string type: number');
     */
    parse(data: unknown, params?: Partial<ParseParams>): Output {
        const result = this.safeParse(data, params);
        if (result.success) return result.data;
        throw result.error;
    }

    /**
     * Parse params to get the good Error Map
     * @param params Be carefull, you can't use "invalid" or "required" in conjunction with custom error map.
     */
    static processCreateParams(params: RawCreateParams): {
        errorMap?: ErrorMap;
    } {
        if (!params) return {};
        const { errorMap, invalidTypeError, requiredError } = params;
        if (errorMap && (invalidTypeError || requiredError)) {
            throw new Error(
                'Can\'t use "invalid" or "required" in conjunction with custom error map.'
            );
        }
        if (errorMap) return { errorMap };
        const customMap: ErrorMap = (issue, ctx) => {
            if (issue.code !== 'invalidType')
                return { message: ctx.defaultError };
            if (typeof ctx.data === 'undefined' && requiredError)
                return { message: requiredError };
            if (params.invalidTypeError)
                return { message: params.invalidTypeError };
            return { message: ctx.defaultError };
        };
        return { errorMap: customMap };
    }

    /**
     * A convenience method that returns an optional version of a schema
     */
    optional(): OptionalType<this> {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        return OptionalType.create(this);
    }

    /**
     * A convenience method that returns an nullable version of a schema
     */
    nullable(): Nullable<this> {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        return Nullable.create(this);
    }

    /**
     * A convenience method that returns a "nullish" version of a schema.
     * Nullish schemas will accept both `undefined` and null
     */
    nullish(): Nullable<OptionalType<this>> {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        return this.optional().nullable();
    }

    /**
     * A convenience method that returns an array schema for the given type
     */
    array(): ArrayType<this> {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        return ArrayType.create(this);
    }

    /**
     * A convenience method for union types.
     */
    or<T extends TypeAny>(option: T): Union<[this, T]> {
        // eslint-disable-next-line
        return Union.create([this, option]);
    }

    /**
     * A convenience method for creating intersection types.
     */
    and<T extends TypeAny>(incoming: T): Intersection<this, T> {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        return Intersection.create(this, incoming);
    }

    /**
     * You can use transforms to implement the concept of "default values".
     *
     *      const stringWithDefault = StringType.create().default('tuna');
     *
     *      stringWithDefault.parse(undefined); // => 'tuna'
     *
     * Optionally, you can pass a function into `.default` that will be re-executed whenever a default
     * value needs to be generated:
     *
     *      const numberWithRandomDefault = NumberType.create().default(Math.random);
     *
     *      numberWithRandomDefault.parse(undefined); // => 0.4413456736055323
     *      numberWithRandomDefault.parse(undefined); // => 0.1871840107401901
     *      numberWithRandomDefault.parse(undefined); // => 0.7223408162401552
     */
    default(def: NoUndefined<Input>): Default<this>;
    default(def: () => NoUndefined<Input>): Default<this>;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    default(def: any) {
        const defaultValueFunc = typeof def === 'function' ? def : () => def;

        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        return new Default({
            innerType: this,
            defaultValue: defaultValueFunc,
            typeName: FirstPartyTypeKind.Default,
        });
    }

    /**
     * `.refine(validator: (data:T) => any, params?: RefineParams)`
     *
     * We let you provide custom validation logic via refinements. (For advanced features like
     * creating multiple issues and customizing error codes, see `.superRefine.`)
     *
     * The validator was designed to mirror TypeScript as closely as possible. But there are many
     * so-called "refinement types" you may wish to check for that can't be represented in TypeScript's
     * type system. For instance: checking that a number is an integer or that a string is a valid email
     * address.
     *
     * For example, you can define a custom validation check on any schema with `.refine`:
     *
     *      const myString = StringType.create().refine((val) => val.length <= 255, {
     *          message: "String can't be more than 255 characters",
     *      });
     *
     *     ⚠️ Refinement functions should not throw. Instead they should return a falsy value to signal failure.
     *
     * Arguments
     *
     * As you can see, `.refine` takes two arguments.
     *
     *  1. The first is the validation function. This function takes one input (of type `T` — the
     *     inferred type of the schema) and returns `any`. Any truthy value will pass validation.
     *  2. The second argument accepts some options. You can use this to customize certain
     *     error-handling behavior:
     *
     *      type RefineParams = {
     *          // override error message
     *          message?: string;
     *
     *          // appended to error path
     *          path?: (string | number)[];
     *
     *          // params object you can use to customize message
     *          // in error map
     *          params?: object;
     *      };
     *
     * For advanced cases, the second argument can also be a function that returns `RefineParams`/`
     *
     *      StringType.create().refine(
     *          (val) => val.length > 10,
     *          (val) => ({ message: `${val} is not more than 10 characters` })
     *      );
     *
     * Customize error path
     *
     *      const passwordForm = ObjectType.create({
     *              password: StringType.create(),
     *              confirm: StringType.create(),
     *          }).refine((data) => data.password === data.confirm, {
     *              message: "Passwords don't match",
     *              path: ['confirm'], // path of error
     *          }).parse({ password: 'asdf', confirm: 'qwer' })
     *      ;
     *
     * Because you provided a path parameter, the resulting error will be:
     *
     *      ErrorValidator {
     *          issues: [{
     *              'code': 'custom',
     *              'path': [ 'confirm' ],
     *              'message': "Passwords don't match"
     *          }]
     *      }
     *
     * Relationship to transforms
     *
     * Transforms and refinements can be interleaved:
     *
     *      StringType.create()
     *          .transform((val) => val.length)
     *          .refine((val) => val > 25);
     *
     */
    refine<RefinedOutput extends Output>(
        check: (arg: Output) => arg is RefinedOutput,
        message?:
            | string
            | CustomErrorParams
            | ((arg: Output) => CustomErrorParams)
    ): Effects<this, RefinedOutput, RefinedOutput>;
    refine(
        check: (arg: Output) => unknown | Promise<unknown>,
        message?:
            | string
            | CustomErrorParams
            | ((arg: Output) => CustomErrorParams)
    ): Effects<this, Output, Input>;
    refine(
        check: (arg: Output) => unknown,
        message?:
            | string
            | CustomErrorParams
            | ((arg: Output) => CustomErrorParams)
    ): Effects<this, Output, Input> {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const getIssueProperties: any = (val: Output) => {
            if (typeof message === 'string' || typeof message === 'undefined') {
                return { message };
            } else if (typeof message === 'function') {
                return message(val);
            } else {
                return message;
            }
        };
        return this._refinement((val, ctx) => {
            const result = check(val);
            const setError = () =>
                ctx.addIssue({
                    code: IssueCode.custom,
                    ...getIssueProperties(val),
                });
            if (typeof Promise !== 'undefined' && result instanceof Promise) {
                return result.then(data => {
                    if (!data) {
                        setError();
                        return false;
                    } else {
                        return true;
                    }
                });
            }
            if (!result) {
                setError();
                return false;
            } else {
                return true;
            }
        });
    }

    /**
     *
     * The `.refine` method is actually syntactic sugar atop a more versatile (and verbose) method
     * called `superRefine`. Here's an example:
     *
     *      const Strings = ArrayType.create(StringType.create()).superRefine((val, ctx) => {
     *          if (val.length > 3) {
     *              ctx.addIssue({
     *                  code: IssueCode.tooBig,
     *                  maximum: 3,
     *                  type: 'array',
     *                  inclusive: true,
     *                  message: 'Too many items 😡',
     *              });
     *          }
     *
     *          if (val.length !== new Set(val).size) {
     *              ctx.addIssue({
     *                  code: IssueCode.custom,
     *                  message: `No duplicated allowed.`,
     *              });
     *          }
     *      });
     *
     * You can add as many issues as you like. If `ctx.addIssue` is NOT called during the execution
     * of the function, validation passes.
     *
     * Normally refinements always create issues with a `IssueCode.custom` error code, but with
     * `superRefine` you can create any issue of any code.
     *
     * Abort early
     *
     * By default, parsing will continue even after a refinement check fails. For instance, if you
     * chain together multiple refinements, they will all be executed. However, it may be desirable
     * to abort early to prevent later refinements from being executed. To achieve this, pass the
     * fatal flag to `ctx.addIssue`:
     *
     *      const Strings = NumberType.create()
     *          .superRefine((val, ctx) => {
     *              if (val < 10) {
     *                  ctx.addIssue({
     *                      code: IssueCode.custom,
     *                      message: 'foo',
     *                      fatal: true,
     *                  });
     *              }
     *          })
     *          .superRefine((val, ctx) => {
     *              if (val !== ' ') {
     *                  ctx.addIssue({
     *                      code: IssueCode.custom,
     *                      message: 'bar',
     *                  });
     *              }
     *          });
     */
    superRefine = this._refinement;

    /**
     * To transform data after parsing, use the `transform` method.
     *
     *      const stringToNumber = StringType.create().transform((val) => myString.length);
     *      stringToNumber.parse("string"); // => 6
     *
     * ⚠️ Transform functions must not throw. Make sure to use refinements before the transform or
     *   addIssue within the transform to make sure the input can be parsed by the transform.
     *
     * Chaining order
     *
     * Note that `stringToNumber` above is an instance of the `Effects` subclass. It is NOT an
     * instance of `StringType`. If you want to use the built-in methods of `StringType` (e.g.
     * `.email()`) you must apply those methods before any transforms.
     *
     *      const emailToDomain = StringType.create()
     *          .email()
     *          .transform((val) => val.split('@')[1]);
     *
     *      emailToDomain.parse('colinhacks@example.com'); // => example.com
     *
     * Validating during transform
     *
     * Similar to `superRefine`, `transform` can optionally take a `ctx`. This allows you to
     * simultaneously validate and transform the value, which can be simpler than chaining `refine`
     * and `validate`. When calling `ctx.addIssue` make sure to still return a value of the correct
     * type otherwise the inferred type will include `undefined`.
     *
     *      const Strings = StringType.create().transform((val, ctx) => {
     *          const parsed = parseInt(val);
     *          if (isNaN(parsed)) {
     *              ctx.addIssue({
     *                  code: IssueCode.custom,
     *                  message: 'Not a number',
     *              });
     *          }
     *          return parsed;
     *      });
     *
     * Relationship to refinements
     *
     * Transforms and refinements can be interleaved. These will be executed in the order they are declared.
     *
     *      StringType.create()
     *          .transform((val) => val.toUpperCase())
     *          .refine((val) => val.length > 15)
     *          .transform((val) => `Hello ${val}`)
     *          .refine((val) => val.indexOf("!") === -1);
     */
    transform<NewOut>(
        transform: (arg: Output, ctx: RefinementCtx) => NewOut | Promise<NewOut>
    ): Effects<this, NewOut> {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        return new Effects({
            schema: this,
            typeName: FirstPartyTypeKind.Effects,
            effect: { type: 'transform', transform },
        });
    }

    /**
     * Return the type of the data and return the string result
     */
    protected _getType(input: ParseInput): ParsedType {
        return getParsedType(input.data);
    }

    /**
     * Handle the result of the parsing to make the result object. Thrown an
     * `Error` if the validation failed, bu no issues detected.
     */
    protected _handleResult(
        ctx: ParseContext,
        result: ParseReturnType<Output>
    ):
        | { success: true; data: Output }
        | { success: false; error: ErrorValidator<Input> } {
        if (isValid(result)) {
            return { success: true, data: result.value };
        } else {
            if (!ctx.common.issues.length) {
                throw new Error('Validation failed but no issues detected.');
            }
            const error = new ErrorValidator(ctx.common.issues);
            return { success: false, error };
        }
    }

    /**
     * Return or construct the parse context
     */
    protected _getOrReturnCtx(
        input: ParseInput,
        ctx?: ParseContext | undefined
    ): ParseContext {
        return (
            ctx || {
                common: input.parent.common,
                data: input.data,

                parsedType: getParsedType(input.data),

                schemaErrorMap: this._def.errorMap,
                path: input.path,
                parent: input.parent,
            }
        );
    }

    /**
     * Add an issue to the parse context
     */
    protected _addIssueToContext(
        ctx: ParseContext,
        issueData: IssueData
    ): void {
        const issue = this._makeIssue({
            issueData: issueData,
            data: ctx.data,
            path: ctx.path,
            errorMaps: [
                ctx.common.contextualErrorMap, // contextual error map is first priority
                ctx.schemaErrorMap, // then schema-bound map if available
                overrideErrorMap, // then global override map
                defaultErrorMap, // then global default map
            ].filter(x => !!x) as ErrorMap[],
        });
        ctx.common.issues.push(issue);
    }

    /**
     * Make an issue
     */
    protected _makeIssue = (params: {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        data: any;
        path: (string | number)[];
        errorMaps: ErrorMap[];
        issueData: IssueData;
    }): Issue => {
        const { data, path, errorMaps, issueData } = params;
        const fullPath = [...path, ...(issueData.path || [])];
        const fullIssue = {
            ...issueData,
            path: fullPath,
        };

        let errorMessage = '';
        const maps = errorMaps
            .filter(m => !!m)
            .slice()
            .reverse() as ErrorMap[];
        for (const map of maps) {
            errorMessage = map(fullIssue, {
                data,
                defaultError: errorMessage,
            }).message;
        }

        return {
            ...issueData,
            path: fullPath,
            message: issueData.message || errorMessage,
        };
    };

    /**
     * Process input parameters
     */
    protected _processInputParams(input: ParseInput): {
        status: ParseStatus;
        ctx: ParseContext;
    } {
        return {
            status: new ParseStatus(),
            ctx: {
                common: input.parent.common,
                data: input.data,

                parsedType: getParsedType(input.data),

                schemaErrorMap: this._def.errorMap,
                path: input.path,
                parent: input.parent,
            },
        };
    }

    protected _refinement(
        refinement: RefinementEffect<Output>['refinement']
    ): Effects<this, Output, Input> {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        return new Effects({
            schema: this,
            typeName: FirstPartyTypeKind.Effects,
            effect: { type: 'refinement', refinement },
        });
    }

    /*promise(): ZodPromise<this> {
        return ZodPromise.create(this);
    }

    isOptional(): boolean {
        return this.safeParse(undefined).success;
    }
    isNullable(): boolean {
        return this.safeParse(null).success;
    }*/
}

// Need to be to the end of the file because recursive inclusion AnyType > Type > Optional > AnyType
import { Optional as OptionalType } from './Optional';
import { ArrayType } from './Array';
import { Nullable } from './Nullable';
import { Union } from './Union';
import { Intersection } from './Intersection';
import { Effects, RefinementEffect } from './Effects';
import { Default } from './Default';
