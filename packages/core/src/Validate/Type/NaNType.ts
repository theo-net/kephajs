/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import { FirstPartyTypeKind, RawCreateParams, Type, TypeDef } from './Type';

import {
    INVALID,
    OK,
    ParseInput,
    ParseReturnType,
    ParsedType,
} from '../helpers/parseUtils';

export type NaNDef = {
    typeName: FirstPartyTypeKind.NaNType;
} & TypeDef;

/**
 * NaN valdiation
 */
export class NaNType extends Type<number, NaNDef> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const parsedType = this._getType(input);
        if (parsedType !== ParsedType.nan) {
            const ctx = this._getOrReturnCtx(input);
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: ParsedType.nan,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        return OK(input.data);
    }

    static create = (params?: RawCreateParams): NaNType => {
        return new NaNType({
            typeName: FirstPartyTypeKind.NaNType,
            ...NaNType.processCreateParams(params),
        });
    };
}
