/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { IssueCode } from '../Error/Error';
import { defaultErrorMap, ErrorMap } from '../Error/ErrorMap';
import {
    INVALID,
    OK,
    ParsedType,
    ParseInput,
    ParseReturnType,
} from '../helpers/parseUtils';
import { AssertEqual } from '../helpers/testUtils';
import { ArrayType } from './Array';
import { NumberType } from './Number';
import { ObjectType } from './Object';
import { StringType } from './String';
import {
    FirstPartyTypeKind,
    RawCreateParams,
    SafeParseError,
    Type,
    TypeDef,
    TypeOf,
} from './Type';

describe('@kephajs/core/Validate/Type/Type', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    class ValidType extends Type<any, TypeDef> {
        _parse(input: ParseInput): ParseReturnType<this['_output']> {
            return OK(input.data);
        }

        static create = (params?: RawCreateParams): ValidType => {
            return new ValidType({
                typeName: FirstPartyTypeKind.AnyType,
                ...ValidType.processCreateParams(params),
            });
        };
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    class InvalidType extends Type<any, TypeDef> {
        _parse(input: ParseInput): ParseReturnType<this['_output']> {
            const ctx = this._getOrReturnCtx(input);
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: ParsedType.null,
                received: ctx.parsedType,
            });
            return INVALID;
        }

        static create = (params?: RawCreateParams): ValidType => {
            return new InvalidType({
                typeName: FirstPartyTypeKind.AnyType,
                ...InvalidType.processCreateParams({
                    ...params,
                    invalidTypeError: 'Error',
                }),
            });
        };
    }

    describe('safeParse', () => {
        it('Should return the result of valid test', () => {
            expect(ValidType.create().safeParse('foobar')).to.be.deep.equal({
                success: true,
                data: 'foobar',
            });
        });

        it('Should return errors validations of invalid test', () => {
            const result = InvalidType.create().safeParse(
                'foobar'
            ) as SafeParseError<string>;
            expect(result.success).to.be.equal(false);
            expect(result.error.errors).to.be.deep.equal([
                {
                    code: IssueCode.invalidType,
                    expected: ParsedType.null,
                    received: ParsedType.string,
                    path: [],
                    message: 'Error',
                },
            ]);
        });
    });

    describe('parse', () => {
        it('Should return the value for a valid test', () => {
            expect(ValidType.create().parse('foobar')).to.be.deep.equal(
                'foobar'
            );
        });

        it('Should thrown an error invalid test', () => {
            const f = () => InvalidType.create().parse('foobar');
            expect(f).to.throw();
        });
    });

    describe('processCreateParams', () => {
        it('Should return empty parameter if we put undefined', () => {
            expect(ValidType.processCreateParams(undefined)).to.be.deep.equal(
                {}
            );
        });

        it('Should throw an error if errorMap and invalidTypeError defined together', () => {
            const f = () =>
                ValidType.processCreateParams({
                    errorMap: defaultErrorMap,
                    invalidTypeError: 'foobar',
                });
            expect(f).to.throw();
        });

        it('Should throw an error if errorMap and requiredError defined together', () => {
            const f = () =>
                ValidType.processCreateParams({
                    errorMap: defaultErrorMap,
                    requiredError: 'foobar',
                });
            expect(f).to.throw();
        });

        it('Should throw an error if invalidTypeError and requiredError defined together', () => {
            const f = () =>
                ValidType.processCreateParams({
                    invalidTypeError: 'foo',
                    requiredError: 'bar',
                });
            expect(f).to.not.throw();
        });

        it('Should return the custom ErrorMap', () => {
            const customErrorMap: ErrorMap = (issue, ctx) => {
                if (issue.code === IssueCode.invalidType) {
                    if (issue.expected === 'string') {
                        return { message: 'Need to be a string!' };
                    }
                }
                return { message: ctx.defaultError };
            };
            expect(
                ValidType.processCreateParams({
                    errorMap: customErrorMap,
                })
            ).to.be.deep.equal({
                errorMap: customErrorMap,
            });
        });

        it('Should construct an custom errorMap with invalidTypeError and requiredError', () => {
            const constructedErrorMap = ValidType.processCreateParams({
                invalidTypeError: 'foo',
                requiredError: 'bar',
            });
            if (constructedErrorMap.errorMap) {
                expect(
                    constructedErrorMap.errorMap(
                        {
                            path: [],
                            code: IssueCode.invalidLiteral,
                            expected: { a: 1, b: '2' },
                        },
                        { defaultError: 'foobar', data: undefined }
                    )
                ).to.be.deep.equal({ message: 'foobar' });
                expect(
                    constructedErrorMap.errorMap(
                        {
                            path: [],
                            code: IssueCode.invalidType,
                            expected: 'string',
                            received: 'number',
                        },
                        { defaultError: 'foo', data: undefined }
                    )
                ).to.be.deep.equal({ message: 'bar' });
                expect(
                    constructedErrorMap.errorMap(
                        {
                            path: [],
                            code: IssueCode.invalidType,
                            expected: 'string',
                            received: 'number',
                        },
                        { defaultError: 'foo', data: 123 }
                    )
                ).to.be.deep.equal({ message: 'foo' });
            }
        });
    });

    describe('optional', () => {
        it('Should make optional a schema', () => {
            expect(StringType.create().optional().parse('foobar')).to.be.equal(
                'foobar'
            );
            const result = StringType.create().optional().safeParse(undefined);
            expect(result.success).to.be.equal(true);
            if (result.success) {
                expect(result.data).to.be.equal(undefined);
            }
        });
    });

    describe('nullable', () => {
        it('Should make nullable a schema', () => {
            expect(StringType.create().nullable().parse('foobar')).to.be.equal(
                'foobar'
            );
            const result = StringType.create().nullable().safeParse(null);
            expect(result.success).to.be.equal(true);
            if (result.success) {
                expect(result.data).to.be.equal(null);
            }
        });
    });

    describe('nullish', () => {
        it('Should make nullish a schema', () => {
            expect(StringType.create().nullish().parse('foobar')).to.be.equal(
                'foobar'
            );
            expect(StringType.create().nullish().parse(null)).to.be.equal(null);
            expect(StringType.create().nullish().parse(undefined)).to.be.equal(
                undefined
            );
        });
    });

    describe('array', () => {
        it('Should make an array for the given schema', () => {
            const stringSchema = StringType.create();
            expect(stringSchema.array().element).to.be.equal(stringSchema);
        });
    });

    describe('or', () => {
        it('Should make an union with a new schema', () => {
            const stringSchema = StringType.create(),
                numberSchema = NumberType.create(),
                otherSchema = StringType.create().email();
            expect(stringSchema.or(numberSchema).options).to.be.deep.equal([
                stringSchema,
                numberSchema,
            ]);
            const unionSchema = stringSchema.or(numberSchema);
            expect(unionSchema.options).to.be.deep.equal([
                stringSchema,
                numberSchema,
            ]);
            expect(unionSchema.or(otherSchema).options).to.be.deep.equal([
                unionSchema,
                otherSchema,
            ]);
        });
    });

    describe('and', () => {
        it('Should make an intersection with a new schema', () => {
            const baseSchema = ObjectType.create({
                id: NumberType.create(),
            });
            const newSchema = ObjectType.create({
                name: StringType.create(),
            });
            const andSchema = baseSchema.and(newSchema);
            expect(andSchema.left).to.be.equal(baseSchema);
            expect(andSchema.right).to.be.equal(newSchema);
        });
    });

    describe('refine/superRefine', () => {
        it('Should refinement', () => {
            const obj1 = ObjectType.create({
                first: StringType.create(),
                second: StringType.create(),
            });
            const obj2 = obj1.partial().strict();

            const obj3 = obj2.refine(
                data => data.first || data.second,
                'Either first or second should be filled in.'
            );

            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            expect(obj1 === (obj2 as any)).to.be.equal(false);
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            expect(obj2 === (obj3 as any)).to.be.equal(false);

            expect(() => obj1.parse({})).to.throw();
            expect(() => obj2.parse({ third: 'adsf' })).to.throw();
            expect(() => obj3.parse({})).to.throw();
            expect(obj3.parse({ first: 'a' })).to.be.deep.equal({ first: 'a' });
            expect(obj3.parse({ second: 'a' })).to.be.deep.equal({
                second: 'a',
            });
            expect(obj3.parse({ first: 'a', second: 'a' })).to.be.deep.equal({
                first: 'a',
                second: 'a',
            });
        });

        it('Should refinement 2', () => {
            const validationSchema = ObjectType.create({
                email: StringType.create().email(),
                password: StringType.create(),
                confirmPassword: StringType.create(),
            }).refine(
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                (data: any) => data.password === data.confirmPassword,
                'Both password and confirmation must match'
            );

            expect(() =>
                validationSchema.parse({
                    email: 'aaaa@gmail.com',
                    password: 'aaaaaaaa',
                    confirmPassword: 'bbbbbbbb',
                })
            ).to.throw();
        });

        it('Should refinement type guard', () => {
            const validationSchema = ObjectType.create({
                a: StringType.create().refine((s): s is 'a' => s === 'a'),
            });
            type Schema = TypeOf<typeof validationSchema>;

            const f1: AssertEqual<'a', Schema['a']> = true;
            const f2: AssertEqual<'string', Schema['a']> = false;
            expect([f1, f2]).to.be.deep.equal([true, false]);
        });

        it('Should custom path', async () => {
            const result = await ObjectType.create({
                password: StringType.create(),
                confirm: StringType.create(),
            })
                .refine(data => data.confirm === data.password, {
                    path: ['confirm'],
                })
                .safeParse({ password: 'asdf', confirm: 'qewr' });
            expect(result.success).to.be.equal(false);
            if (!result.success) {
                expect(result.error.errors[0].path).to.be.deep.equal([
                    'confirm',
                ]);
            }
        });

        it('Should use path in refinement context', async () => {
            const noNested = StringType.create().superRefine((_val, ctx) => {
                if (ctx.path.length > 0) {
                    ctx.addIssue({
                        code: IssueCode.custom,
                        message: `schema cannot be nested. path: ${ctx.path.join(
                            '.'
                        )}`,
                    });
                    return false;
                } else {
                    return true;
                }
            });

            const data = ObjectType.create({
                foo: noNested,
            });

            const t1 = noNested.safeParse('asdf');
            const t2 = data.safeParse({ foo: 'asdf' });

            expect(t1.success).to.be.equal(true);
            expect(t2.success).to.be.equal(false);
            if (t2.success === false) {
                expect(t2.error.errors[0].message).to.be.equal(
                    'schema cannot be nested. path: foo'
                );
            }
        });

        it('Should superRefine', () => {
            const Strings = ArrayType.create(StringType.create())
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                .superRefine((val: any, ctx: any) => {
                    if (val.length > 3) {
                        ctx.addIssue({
                            code: IssueCode.tooBig,
                            maximum: 3,
                            type: 'array',
                            inclusive: true,
                            message: 'Too many items 😡',
                        });
                    }

                    if (val.length !== new Set(val).size) {
                        ctx.addIssue({
                            code: IssueCode.custom,
                            message: `No duplicates allowed.`,
                        });
                    }
                });

            const result = Strings.safeParse(['asfd', 'asfd', 'asfd', 'asfd']);

            expect(result.success).to.be.equal(false);
            if (!result.success)
                expect(result.error.errors.length).to.be.equal(2);

            Strings.parse(['asfd', 'qwer']);
        });

        it('Should get inner type', () => {
            StringType.create()
                .refine(() => true)
                .innerType()
                .parse('asdf');
        });

        it('Should chained refinements', () => {
            const objectSchema = ObjectType.create({
                length: NumberType.create(),
                size: NumberType.create(),
            })
                .refine(({ length }) => length > 5, {
                    path: ['length'],
                    message: 'length greater than 5',
                })
                .refine(({ size }) => size > 7, {
                    path: ['size'],
                    message: 'size greater than 7',
                });
            const r1 = objectSchema.safeParse({
                length: 4,
                size: 9,
            });
            expect(r1.success).to.be.equal(false);
            if (!r1.success) expect(r1.error.errors.length).to.be.equal(1);

            const r2 = objectSchema.safeParse({
                length: 4,
                size: 3,
            });
            expect(r2.success).to.be.equal(false);
            if (!r2.success) expect(r2.error.errors.length).to.be.equal(2);
        });

        it('Should fatal superRefine', () => {
            const Strings = StringType.create()
                .superRefine((val, ctx) => {
                    if (val === '') {
                        ctx.addIssue({
                            code: IssueCode.custom,
                            message: 'foo',
                            fatal: true,
                        });
                    }
                })
                .superRefine((val, ctx) => {
                    if (val !== ' ') {
                        ctx.addIssue({
                            code: IssueCode.custom,
                            message: 'bar',
                        });
                    }
                });

            const result = Strings.safeParse('');

            expect(result.success).to.be.equal(false);
            if (!result.success)
                expect(result.error.errors.length).to.be.equal(1);
        });
    });
});
