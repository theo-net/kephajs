/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    FirstPartyTypeKind,
    InputType,
    OutputType,
    RawCreateParams,
    Type,
    TypeAny,
    TypeDef,
} from './Type';

import { ParseInput, ParseReturnType } from '../helpers/parseUtils';

export type LazyDef<T extends TypeAny = TypeAny> = {
    typeName: FirstPartyTypeKind.Lazy;
    getter: () => T;
} & TypeDef;

export class LazyType<T extends TypeAny> extends Type<
    OutputType<T>,
    LazyDef<T>,
    InputType<T>
> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const { ctx } = this._processInputParams(input);
        const lazySchema = this._def.getter();
        return lazySchema._parse({
            data: ctx.data,
            path: ctx.path,
            parent: ctx,
        });
    }

    static create<T extends TypeAny>(
        getter: () => T,
        params?: RawCreateParams
    ): LazyType<T> {
        return new LazyType({
            typeName: FirstPartyTypeKind.Lazy,
            getter,
            ...LazyType.processCreateParams(params),
        });
    }

    get schema(): T {
        return this._def.getter();
    }
}
