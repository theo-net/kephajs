/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import { FirstPartyTypeKind, RawCreateParams, Type, TypeDef } from './Type';

import {
    INVALID,
    OK,
    ParseInput,
    ParseReturnType,
    ParsedType,
} from '../helpers/parseUtils';

export type VoidDef = {
    typeName: FirstPartyTypeKind.VoidType;
} & TypeDef;

/**
 * Type `void` validation, accept also `undefined`
 */
export class VoidType extends Type<void, VoidDef> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const parsedType = this._getType(input);
        if (parsedType !== ParsedType.undefined) {
            const ctx = this._getOrReturnCtx(input);
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: ParsedType.void,
                received: ctx.parsedType,
            });
            return INVALID;
        }
        return OK(input.data);
    }

    static create = (params?: RawCreateParams): VoidType => {
        return new VoidType({
            typeName: FirstPartyTypeKind.VoidType,
            ...VoidType.processCreateParams(params),
        });
    };
}
