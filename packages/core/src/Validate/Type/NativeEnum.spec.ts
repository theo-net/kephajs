/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { NativeEnumType } from './NativeEnum';
import { TypeOf } from './Type';
import { AssertEqual } from '../helpers/testUtils';

describe('@kephajs/core/Validate/Type/NativeEnum', () => {
    it('Should test NativeEnum with consts', () => {
        const Fruits: { Apple: 'apple'; Banana: 'banana' } = {
            Apple: 'apple',
            Banana: 'banana',
        };
        const fruitEnum = NativeEnumType.create(Fruits);
        type FruitEnum = TypeOf<typeof fruitEnum>;
        expect(fruitEnum.parse('apple')).to.be.equal('apple');
        expect(fruitEnum.parse('banana')).to.be.equal('banana');
        expect(fruitEnum.parse(Fruits.Apple)).to.be.equal('apple');
        expect(fruitEnum.parse(Fruits.Banana)).to.be.equal('banana');
        const result1: AssertEqual<FruitEnum, 'apple' | 'banana'> = true;
        expect(result1).to.be.equal(true);
        const result2: AssertEqual<FruitEnum, FruitEnum> = true;
        expect(result2).to.be.equal(true);
    });

    it('Should test NativeEnum with real enum', () => {
        enum Fruits {
            Apple = 'apple',
            Banana = 'banana',
        }
        const fruitEnum = NativeEnumType.create(Fruits);
        type FruitEnum = TypeOf<typeof fruitEnum>;
        expect(fruitEnum.parse('apple')).to.be.equal('apple');
        expect(fruitEnum.parse('banana')).to.be.equal('banana');
        expect(fruitEnum.parse(Fruits.Apple)).to.be.equal('apple');
        expect(fruitEnum.parse(Fruits.Banana)).to.be.equal('banana');
        const result: AssertEqual<FruitEnum, Fruits> = true;
        expect(result).to.be.equal(true);
    });

    it('Should test NativeEnum with const with numeric keys', () => {
        const FruitValues = {
            Apple: 10,
            Banana: 20,
        } as const;
        const fruitEnum = NativeEnumType.create(FruitValues);
        type FruitEnum = TypeOf<typeof fruitEnum>;
        expect(fruitEnum.parse(10)).to.be.equal(10);
        expect(fruitEnum.parse(20)).to.be.equal(20);
        expect(fruitEnum.parse(FruitValues.Apple)).to.be.equal(10);
        expect(fruitEnum.parse(FruitValues.Banana)).to.be.equal(20);
        const result: AssertEqual<FruitEnum, 10 | 20> = true;
        expect(result).to.be.equal(true);
    });

    it('Should test from enum', () => {
        enum Fruits {
            Cantaloupe,
            Apple = 'apple',
            Banana = 'banana',
        }

        const fruitEnum = NativeEnumType.create(Fruits);
        expect(fruitEnum.parse(Fruits.Cantaloupe)).to.be.equal(0);
        expect(fruitEnum.parse(Fruits.Apple)).to.be.equal('apple');
        expect(fruitEnum.parse('apple')).to.be.equal('apple');
        expect(fruitEnum.parse(0)).to.be.equal(0);
        expect(() => fruitEnum.parse(1)).to.throw();
        expect(() => fruitEnum.parse('Apple')).to.throw();
        expect(() => fruitEnum.parse('Cantaloupe')).to.throw();
    });

    it('Should test Nrom const', () => {
        const Greek = {
            Alpha: 'a',
            Beta: 'b',
            Gamma: 3,
        } as const;

        const GreekEnum = NativeEnumType.create(Greek);
        expect(GreekEnum.parse('a')).to.be.equal('a');
        expect(GreekEnum.parse('b')).to.be.equal('b');
        expect(GreekEnum.parse(3)).to.be.equal(3);
        expect(() => GreekEnum.parse('v')).to.throw();
        expect(() => GreekEnum.parse('Alpha')).to.throw();
        expect(() => GreekEnum.parse(2)).to.throw();

        expect(GreekEnum.enum.Alpha).to.be.equal('a');
    });
});
