/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { NeverType } from './Never';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Never', () => {
    it('Should parse NeverType string', () => {
        const f = () => NeverType.create().parse('foobar');
        expect(f).to.throw();
    });

    it('Should parse NeverType number', () => {
        const f = () => NeverType.create().parse(123);
        expect(f).to.throw();
    });

    it('Should parse NeverType boolean', () => {
        const f = () => NeverType.create().parse(true);
        expect(f).to.throw();
    });

    it('Should parse NeverType undefined', () => {
        const f = () => NeverType.create().parse(undefined);
        expect(f).to.throw();
    });

    it('Should parse NeverType null', () => {
        const f = () => NeverType.create().parse(null);
        expect(f).to.throw();
    });

    it('Should infer NeverType', () => {
        const MyNever = NeverType.create();
        type MyNeverType = TypeOf<typeof MyNever>;
        const result: AssertEqual<MyNeverType, never> = true;
        expect(result).to.be.equal(true);
    });
});
