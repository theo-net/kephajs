/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { AssertEqual } from '../helpers/testUtils';
import { NumberType } from './Number';
import { ObjectType } from './Object';
import { StringType } from './String';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/Object', () => {
    const NameType = StringType.create();
    const AgeType = NumberType.create();

    const Person = ObjectType.create({
        name: NameType,
        age: AgeType,
    });

    it('Should parse ObjectType string', () => {
        const f = () => Person.parse('foobar');
        expect(f).to.throw();
    });

    it('Should parse ObjectType number', () => {
        const f = () => Person.parse(123);
        expect(f).to.throw();
    });

    it('Should parse ObjectType boolean', () => {
        const f = () => Person.parse(true);
        expect(f).to.throw();
    });

    it('Should parse ObjectType undefined', () => {
        const f = () => Person.parse(undefined);
        expect(f).to.throw();
    });

    it('Should parse ObjectType null', () => {
        const f = () => Person.parse(null);
        expect(f).to.throw();
    });

    it('Should success if every keys success', () => {
        expect(Person.parse({ name: 'foobar', age: 33 })).to.be.deep.equal({
            name: 'foobar',
            age: 33,
        });
    });

    it('Should fail if one key fail', () => {
        expect(
            Person.safeParse({ name: 'foobar', age: '33' }).success
        ).to.be.equal(false);
    });

    it('Should fail if one property is missing', () => {
        expect(Person.safeParse({ name: 'foobar' }).success).to.be.equal(false);
    });

    it('Should ignore and strip unknown key', () => {
        expect(
            Person.parse({ name: 'foobar', age: 33, foo: 'bar' })
        ).to.be.deep.equal({
            name: 'foobar',
            age: 33,
        });
        expect(Person.unknownKeys).to.be.equal('strip');
    });

    it('Should infer ObjectType', () => {
        type PersonType = TypeOf<typeof Person>;
        type ValidType = {
            name: string;
            age: number;
        };
        const result: AssertEqual<PersonType, ValidType> = true;
        expect(result).to.be.equal(true);
    });

    describe('shape', () => {
        it('Should return the schema of a particular key', () => {
            expect(Person.shape.name).to.be.equal(NameType);
            expect(Person.shape.age).to.be.equal(AgeType);
        });
    });

    describe('extend and merge', () => {
        it('Should extend an existing object schema', () => {
            const Class = StringType.create();
            const Student = Person.extend({
                class: Class,
            });
            expect(Student.shape.class).to.be.equal(Class);
        });

        it('Should merge two object schema', () => {
            const Class = StringType.create();
            const HasClass = ObjectType.create({
                class: Class,
            });
            const Student = Person.merge(HasClass);
            expect(Student.shape.class).to.be.equal(Class);
        });

        it('Should overrides previous keys', () => {
            const ShortName = StringType.create().max(5);
            const HasShortName = ObjectType.create({
                name: ShortName,
            });
            const Student = Person.merge(HasShortName);
            expect(Student.shape.name).to.be.equal(ShortName);
        });
    });

    describe('partial', () => {
        it('Should make all properties optional', () => {
            const partialPerson = Person.partial();
            expect(partialPerson.safeParse({}).success).to.be.equal(true);

            type PersonType = TypeOf<typeof partialPerson>;
            type ValidType = {
                name?: string;
                age?: number;
            };
            const result: AssertEqual<PersonType, ValidType> = true;
            expect(result).to.be.equal(true);
        });

        it('Should specify witch properties to make optional', () => {
            const optionalAge = Person.partial({ age: true });
            expect(optionalAge.safeParse({ age: 33 }).success).to.be.equal(
                false
            );
            expect(optionalAge.parse({ name: 'foobar' })).to.be.deep.equal({
                name: 'foobar',
            });

            type PersonType = TypeOf<typeof optionalAge>;
            type ValidType = {
                name: string;
                age?: number;
            };
            const result: AssertEqual<PersonType, ValidType> = true;
            expect(result).to.be.equal(true);
        });
    });

    describe('passthrough', () => {
        it('Should pass through unknown keys', () => {
            expect(
                Person.passthrough().parse({
                    name: 'foobar',
                    age: 33,
                    foo: 'bar',
                })
            ).to.be.deep.equal({
                name: 'foobar',
                age: 33,
                foo: 'bar',
            });

            expect(Person.passthrough().unknownKeys).to.be.equal('passthrough');
        });
    });

    describe('strict', () => {
        it('Should pass through unknown keys', () => {
            const result = Person.strict().safeParse({
                name: 'foobar',
                age: 33,
                foo: 'bar',
            });
            expect(result.success).to.be.equal(false);

            expect(Person.strict().unknownKeys).to.be.equal('strict');
        });
    });

    describe('strip', () => {
        it('Should restore default behavior', () => {
            const result = Person.strict().strip().safeParse({
                name: 'foobar',
                age: 33,
                foo: 'bar',
            });
            expect(result.success).to.be.equal(true);

            expect(Person.strict().strip().unknownKeys).to.be.equal('strip');
        });
    });

    describe('cathall', () => {
        it('Should catch all unknown keys', () => {
            const result1 = Person.catchall(StringType.create()).safeParse({
                name: 'foobar',
                age: 33,
                foo: 'bar',
            });
            expect(result1.success).to.be.equal(true);
            const result2 = Person.catchall(StringType.create()).safeParse({
                name: 'foobar',
                age: 33,
                foo: 12,
            });
            expect(result2.success).to.be.equal(false);
        });
    });
});
