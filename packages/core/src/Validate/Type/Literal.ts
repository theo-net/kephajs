/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import {
    FirstPartyTypeKind,
    Primitive,
    RawCreateParams,
    Type,
    TypeDef,
} from './Type';

import {
    INVALID,
    OK,
    ParseInput,
    ParseReturnType,
} from '../helpers/parseUtils';

export type LiteralDef<T> = {
    typeName: FirstPartyTypeKind.Literal;
    value: T;
} & TypeDef;

export class LiteralType<T> extends Type<T, LiteralDef<T>> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        if (input.data !== this._def.value) {
            const ctx = this._getOrReturnCtx(input);
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidLiteral,
                expected: this._def.value,
            });
            return INVALID;
        }
        return OK(input.data);
    }

    /**
     * The literal value
     */
    get value() {
        return this._def.value;
    }

    static create = <U extends Primitive>(
        value: U,
        params?: RawCreateParams
    ): LiteralType<U> => {
        return new LiteralType({
            typeName: FirstPartyTypeKind.Literal,
            value,
            ...LiteralType.processCreateParams(params),
        });
    };
}
