/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    FirstPartyTypeKind,
    RawCreateParams,
    Type,
    TypeAny,
    TypeDef,
} from './Type';

import {
    OK,
    ParseInput,
    ParseReturnType,
    ParsedType,
} from '../helpers/parseUtils';

export type NullableDef<T extends TypeAny = TypeAny> = {
    typeName: FirstPartyTypeKind.Nullable;
    innerType: T;
} & TypeDef;

/**
 * You can make any schema nullable with this type. This wrap the schema and
 * returns the results.
 */
export class Nullable<T extends TypeAny> extends Type<
    T['_output'] | null,
    NullableDef<T>,
    T['_input'] | null
> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        if (this._getType(input) === ParsedType.null) {
            return OK(null);
        }
        return this._def.innerType._parse(input);
    }

    static create<U extends TypeAny>(
        type: U,
        params?: RawCreateParams
    ): Nullable<U> {
        return new Nullable({
            typeName: FirstPartyTypeKind.Nullable,
            innerType: type,
            ...Nullable.processCreateParams(params),
        });
    }

    /**
     * Extract the wrapped schema
     */
    unwrap() {
        return this._def.innerType;
    }
}
