/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import { IssueCode } from '../Error/Error';
import { ParsedType } from '../helpers/parseUtils';

import { AssertEqual } from '../helpers/testUtils';
import { DiscriminatedUnion } from './DiscriminatedUnion';
import { LiteralType } from './Literal';
import { ObjectType } from './Object';
import { StringType } from './String';
import { TypeOf } from './Type';

describe('@kephajs/core/Validate/Type/DiscriminatedUnion', () => {
    it('Should success, same primitive type', () => {
        expect(
            DiscriminatedUnion.create('type', [
                ObjectType.create({
                    type: LiteralType.create('a'),
                    a: StringType.create(),
                }),
                ObjectType.create({
                    type: LiteralType.create('b'),
                    b: StringType.create(),
                }),
            ]).parse({ type: 'a', a: 'abc' })
        ).to.be.deep.equal({ type: 'a', a: 'abc' });
    });

    it('Should success, discriminator value of various primitive types', () => {
        const schema = DiscriminatedUnion.create('type', [
            ObjectType.create({
                type: LiteralType.create('1'),
                val: LiteralType.create(1),
            }),
            ObjectType.create({
                type: LiteralType.create(1),
                val: LiteralType.create(2),
            }),
            ObjectType.create({
                type: LiteralType.create(BigInt(1)),
                val: LiteralType.create(3),
            }),
            ObjectType.create({
                type: LiteralType.create('true'),
                val: LiteralType.create(4),
            }),
            ObjectType.create({
                type: LiteralType.create(true),
                val: LiteralType.create(5),
            }),
            ObjectType.create({
                type: LiteralType.create('null'),
                val: LiteralType.create(6),
            }),
            ObjectType.create({
                type: LiteralType.create(null),
                val: LiteralType.create(7),
            }),
            ObjectType.create({
                type: LiteralType.create('undefined'),
                val: LiteralType.create(8),
            }),
            ObjectType.create({
                type: LiteralType.create(undefined),
                val: LiteralType.create(9),
            }),
        ]);

        expect(schema.parse({ type: '1', val: 1 })).to.be.deep.equal({
            type: '1',
            val: 1,
        });
        expect(schema.parse({ type: 1, val: 2 })).to.be.deep.equal({
            type: 1,
            val: 2,
        });
        expect(schema.parse({ type: BigInt(1), val: 3 })).to.be.deep.equal({
            type: BigInt(1),
            val: 3,
        });
        expect(schema.parse({ type: 'true', val: 4 })).to.be.deep.equal({
            type: 'true',
            val: 4,
        });
        expect(schema.parse({ type: true, val: 5 })).to.be.deep.equal({
            type: true,
            val: 5,
        });
        expect(schema.parse({ type: 'null', val: 6 })).to.be.deep.equal({
            type: 'null',
            val: 6,
        });
        expect(schema.parse({ type: null, val: 7 })).to.be.deep.equal({
            type: null,
            val: 7,
        });
        expect(schema.parse({ type: 'undefined', val: 8 })).to.be.deep.equal({
            type: 'undefined',
            val: 8,
        });
        expect(schema.parse({ type: undefined, val: 9 })).to.be.deep.equal({
            type: undefined,
            val: 9,
        });
    });

    it('Should fail, invalid - null', () => {
        try {
            DiscriminatedUnion.create('type', [
                ObjectType.create({
                    type: LiteralType.create('a'),
                    a: StringType.create(),
                }),
                ObjectType.create({
                    type: LiteralType.create('b'),
                    b: StringType.create(),
                }),
            ]).parse(null);
            throw new Error();
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        } catch (e: any) {
            expect(JSON.parse(e.message)).to.be.deep.equal([
                {
                    code: IssueCode.invalidType,
                    expected: ParsedType.object,
                    message: 'Expected object, received null',
                    received: ParsedType.null,
                    path: [],
                },
            ]);
        }
    });

    it('Should fail, invalid discriminator value', () => {
        try {
            DiscriminatedUnion.create('type', [
                ObjectType.create({
                    type: LiteralType.create('a'),
                    a: StringType.create(),
                }),
                ObjectType.create({
                    type: LiteralType.create('b'),
                    b: StringType.create(),
                }),
            ]).parse({ type: 'x', a: 'abc' });
            throw new Error();
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        } catch (e: any) {
            expect(JSON.parse(e.message)).to.be.deep.equal([
                {
                    code: IssueCode.invalidUnionDiscriminator,
                    options: ['a', 'b'],
                    message: "Invalid discriminator value. Expected 'a' | 'b'",
                    path: ['type'],
                },
            ]);
        }
    });

    it('Should fail, valid discriminator value, invalid data', () => {
        try {
            DiscriminatedUnion.create('type', [
                ObjectType.create({
                    type: LiteralType.create('a'),
                    a: StringType.create(),
                }),
                ObjectType.create({
                    type: LiteralType.create('b'),
                    b: StringType.create(),
                }),
            ]).parse({ type: 'a', b: 'abc' });
            throw new Error();
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        } catch (e: any) {
            expect(JSON.parse(e.message)).to.be.deep.equal([
                {
                    code: IssueCode.invalidType,
                    expected: ParsedType.string,
                    message: 'Required',
                    path: ['a'],
                    received: ParsedType.undefined,
                },
            ]);
        }
    });

    it('Should fail, wrong schema - missing discriminator', () => {
        try {
            DiscriminatedUnion.create('type', [
                ObjectType.create({
                    type: LiteralType.create('a'),
                    a: StringType.create(),
                }),
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                ObjectType.create({ b: StringType.create() }) as any,
            ]);
            throw new Error();
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        } catch (e: any) {
            expect(e.message).to.be.deep.equal(
                'The discriminator value could not be extracted from all the provided schemas'
            );
        }
    });

    it('Should fail, wrong schema - duplicate discriminator values', () => {
        try {
            DiscriminatedUnion.create('type', [
                ObjectType.create({
                    type: LiteralType.create('a'),
                    a: StringType.create(),
                }),
                ObjectType.create({
                    type: LiteralType.create('a'),
                    b: StringType.create(),
                }),
            ]);
            throw new Error();
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
        } catch (e: any) {
            expect(e.message).to.be.deep.equal(
                'Some of the discriminator values are not unique'
            );
        }
    });

    it('Should infer DiscriminatedUnion', () => {
        const MySchema = DiscriminatedUnion.create('type', [
            ObjectType.create({
                type: LiteralType.create('a'),
                a: StringType.create(),
            }),
            ObjectType.create({
                type: LiteralType.create('b'),
                b: StringType.create(),
            }),
        ]);
        type MySchemaType = TypeOf<typeof MySchema>;
        const result: AssertEqual<
            MySchemaType,
            { type: 'a'; a: string } | { type: 'b'; b: string }
        > = true;
        expect(result).to.be.equal(true);
    });
});
