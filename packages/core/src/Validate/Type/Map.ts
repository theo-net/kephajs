/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { IssueCode } from '../Error/Error';
import {
    FirstPartyTypeKind,
    RawCreateParams,
    Type,
    TypeAny,
    TypeDef,
} from './Type';

import {
    INVALID,
    ParseInput,
    ParseReturnType,
    ParsedType,
    ParseInputLazyPath,
} from '../helpers/parseUtils';

export type MapDef<
    Key extends TypeAny = TypeAny,
    Value extends TypeAny = TypeAny
> = {
    typeName: FirstPartyTypeKind.MapType;
    keyType: Key;
    valueType: Value;
} & TypeDef;

/**
 * Type `Map` validation
 */
export class MapType<
    Key extends TypeAny = TypeAny,
    Value extends TypeAny = TypeAny
> extends Type<
    Map<Key['_output'], Value['_output']>,
    MapDef<Key, Value>,
    Map<Key['_input'], Value['_input']>
> {
    _parse(input: ParseInput): ParseReturnType<this['_output']> {
        const { status, ctx } = this._processInputParams(input);

        if (ctx.parsedType !== ParsedType.map) {
            this._addIssueToContext(ctx, {
                code: IssueCode.invalidType,
                expected: ParsedType.map,
                received: ctx.parsedType,
            });
            return INVALID;
        }

        const pairs = [...(ctx.data as Map<unknown, unknown>).entries()].map(
            ([key, value], index) => {
                return {
                    key: this._def.keyType._parse(
                        new ParseInputLazyPath(ctx, key, ctx.path, [
                            index,
                            'key',
                        ])
                    ),
                    value: this._def.valueType._parse(
                        new ParseInputLazyPath(ctx, value, ctx.path, [
                            index,
                            'value',
                        ])
                    ),
                };
            }
        );

        const finalMap = new Map();
        for (const pair of pairs) {
            const key = pair.key as ParseReturnType;
            const value = pair.value as ParseReturnType;
            if (key.status === 'aborted' || value.status === 'aborted') {
                return INVALID;
            }
            if (key.status === 'dirty' || value.status === 'dirty') {
                status.dirty();
            }

            finalMap.set(key.value, value.value);
        }

        return { status: status.value, value: finalMap };
    }

    static create<K extends TypeAny = TypeAny, V extends TypeAny = TypeAny>(
        keyType: K,
        valueType: V,
        params?: RawCreateParams
    ): MapType<K, V> {
        return new MapType({
            typeName: FirstPartyTypeKind.MapType,
            valueType,
            keyType,
            ...MapType.processCreateParams(params),
        });
    }
}
