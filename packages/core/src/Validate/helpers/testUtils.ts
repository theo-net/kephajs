/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export type AssertEqual<T, Expected> = [T] extends [Expected]
    ? [Expected] extends [T]
        ? true
        : false
    : false;
