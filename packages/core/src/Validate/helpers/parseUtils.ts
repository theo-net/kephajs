/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Issue } from '../Error/Error';
import { ErrorMap } from '../Error/ErrorMap';
import * as Helpers from '../../Helpers/Helpers';

export const ParsedType = Helpers.arrayToEnum([
    'string',
    'nan',
    'number',
    'integer',
    'float',
    'boolean',
    'date',
    'bigint',
    'symbol',
    'function',
    'undefined',
    'null',
    'array',
    'object',
    'unknown',
    'promise',
    'void',
    'never',
    'map',
    'set',
]);
// eslint-disable-next-line @typescript-eslint/no-redeclare
export type ParsedType = keyof typeof ParsedType;

export type ParsePathComponent = string | number;
export type ParsePath = ParsePathComponent[];

export type ParseContext = {
    readonly common: {
        readonly issues: Issue[];
        readonly contextualErrorMap?: ErrorMap;
    };
    readonly path: ParsePath;
    readonly schemaErrorMap?: ErrorMap;
    readonly parent: ParseContext | null;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    readonly data: any;
    readonly parsedType: ParsedType;
};

export type ParseInput = {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    data: any;
    path: (string | number)[];
    parent: ParseContext;
};

export type ParseParams = {
    path: (string | number)[];
    errorMap: ErrorMap;
};

export type INVALID = { status: 'aborted' };
// eslint-disable-next-line @typescript-eslint/no-redeclare
export const INVALID: INVALID = Object.freeze({
    status: 'aborted',
});

export type DIRTY<T> = { status: 'dirty'; value: T };
// eslint-disable-next-line @typescript-eslint/no-redeclare
export const DIRTY = <T>(value: T): DIRTY<T> => ({ status: 'dirty', value });

export type OK<T> = { status: 'valid'; value: T };
// eslint-disable-next-line @typescript-eslint/no-redeclare
export const OK = <T>(value: T): OK<T> => ({ status: 'valid', value });

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const isAborted = (x: ParseReturnType<any>): x is INVALID =>
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (x as any).status === 'aborted';

export const isDirty = <T>(x: ParseReturnType<T>): x is OK<T> =>
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (x as any).status === 'dirty';

export const isValid = <T>(x: ParseReturnType<T>): x is OK<T> =>
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (x as any).status === 'valid';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type ParseReturnType<T = any> = OK<T> | DIRTY<T> | INVALID;

/**
 * Parse the type of data
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getParsedType = (data: any): ParsedType => {
    const t = typeof data;

    switch (t) {
        case 'undefined':
            return ParsedType.undefined;

        case 'string':
            return ParsedType.string;

        case 'number':
            return isNaN(data) ? ParsedType.nan : ParsedType.number;

        case 'boolean':
            return ParsedType.boolean;

        case 'function':
            return ParsedType.function;

        case 'bigint':
            return ParsedType.bigint;

        case 'object':
            if (Array.isArray(data)) {
                return ParsedType.array;
            }
            if (data === null) {
                return ParsedType.null;
            }
            if (
                data.then &&
                typeof data.then === 'function' &&
                data.catch &&
                typeof data.catch === 'function'
            ) {
                return ParsedType.promise;
            }
            if (typeof Map !== 'undefined' && data instanceof Map) {
                return ParsedType.map;
            }
            if (typeof Set !== 'undefined' && data instanceof Set) {
                return ParsedType.set;
            }
            if (typeof Date !== 'undefined' && data instanceof Date) {
                return ParsedType.date;
            }
            return ParsedType.object;

        default:
            return ParsedType.unknown;
    }
};

/**
 * Manage the status of a validation
 */
export class ParseStatus {
    protected _value: 'aborted' | 'dirty' | 'valid' = 'valid';

    get value() {
        return this._value;
    }

    dirty() {
        if (this._value === 'valid') this._value = 'dirty';
    }

    abort() {
        this._value = 'aborted';
    }

    /*static mergeArray(
        status: ParseStatus,
        results: SyncParseReturnType<any>[]
    ): SyncParseReturnType {
        const arrayValue: any[] = [];
        for (const s of results) {
            if (s.status === 'aborted') return INVALID;
            if (s.status === 'dirty') status.dirty();
            arrayValue.push(s.value);
        }

        return { status: status.value, value: arrayValue };
    }*/

    /**
     * Merge the results of all pair
     */
    static mergeObject(
        status: ParseStatus,
        pairs: {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            key: ParseReturnType<any>;
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            value: ParseReturnType<any>;
            alwaysSet?: boolean;
        }[]
    ): ParseReturnType {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const finalObject: any = {};
        for (const pair of pairs) {
            const { key, value } = pair;
            if (key.status === 'aborted') return INVALID;
            if (value.status === 'aborted') return INVALID;
            if (key.status === 'dirty') status.dirty();
            if (value.status === 'dirty') status.dirty();

            if (typeof value.value !== 'undefined' || pair.alwaysSet) {
                finalObject[key.value] = value.value;
            }
        }

        return { status: status.value, value: finalObject };
    }

    /**
     * Merge the result of all element of an array
     */
    static mergeArray(
        status: ParseStatus,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        results: ParseReturnType<any>[]
    ): ParseReturnType {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const arrayValue: any[] = [];

        for (const result of results) {
            if (result.status === 'aborted') return INVALID;
            if (result.status === 'dirty') status.dirty();
            arrayValue.push(result.value);
        }

        return { status: status.value, value: arrayValue };
    }
}

/**
 * Give parse data and put the `key` inside the parent's path.
 */
export class ParseInputLazyPath implements ParseInput {
    parent: ParseContext;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    data: any;

    protected _path: ParsePath;

    protected _key: string | number | (string | number)[];

    constructor(
        parent: ParseContext,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        value: any,
        path: ParsePath,
        key: string | number | (string | number)[]
    ) {
        this.parent = parent;
        this.data = value;
        this._path = path;
        this._key = key;
    }

    get path() {
        return this._path.concat(this._key);
    }
}
