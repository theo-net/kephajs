/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import {
    isAborted,
    isDirty,
    isValid,
    getParsedType,
    ParsedType,
    ParseStatus,
    ParseInputLazyPath,
} from './parseUtils';

describe('@kephajs/core/Validate/helpers/parseUtils', () => {
    describe('getParsedType', () => {
        it('Should return ParsedType.undefined for undefined type', () => {
            expect(getParsedType(undefined)).to.be.equal(ParsedType.undefined);
        });

        it('Should return ParsedType.string for String type', () => {
            expect(getParsedType('foobar')).to.be.equal(ParsedType.string);
        });

        it('Should return ParsedType.nan for NaN type', () => {
            expect(getParsedType(NaN)).to.be.equal(ParsedType.nan);
        });

        it('Should return ParsedType.number for Number type', () => {
            expect(getParsedType(123)).to.be.equal(ParsedType.number);
        });

        it('Should return ParsedType.boolean for boolean type', () => {
            expect(getParsedType(true)).to.be.equal(ParsedType.boolean);
            expect(getParsedType(false)).to.be.equal(ParsedType.boolean);
        });

        it('Should return ParsedType.function for Function type', () => {
            expect(getParsedType((a: string) => a)).to.be.equal(
                ParsedType.function
            );
        });

        it('Should return ParsedType.bigint for BigInt type', () => {
            expect(getParsedType(BigInt(123))).to.be.equal(ParsedType.bigint);
        });

        it('Should return ParsedType.array for Array type', () => {
            expect(getParsedType([1, 2, 3])).to.be.equal(ParsedType.array);
        });

        it('Should return ParsedType.null for null type', () => {
            expect(getParsedType(null)).to.be.equal(ParsedType.null);
        });

        it('Should return ParsedType.promise for Promise type', () => {
            expect(
                getParsedType(Promise.resolve({ name: 'Foobar' }))
            ).to.be.equal(ParsedType.promise);
        });

        it('Should return ParsedType.map for Map type', () => {
            expect(getParsedType(new Map())).to.be.equal(ParsedType.map);
        });

        it('Should return ParsedType.set for Set type', () => {
            expect(getParsedType(new Set())).to.be.equal(ParsedType.set);
        });

        it('Should return ParsedType.date for Date type', () => {
            expect(getParsedType(new Date())).to.be.equal(ParsedType.date);
        });

        it('Should return ParsedType.object for Object type', () => {
            expect(getParsedType({ a: 1, b: 2 })).to.be.equal(
                ParsedType.object
            );
        });
    });

    describe('isValid', () => {
        it('Should return true if the status is "valid"', () => {
            expect(isValid({ status: 'valid', value: 'foobar' })).to.be.equal(
                true
            );
        });

        it('Should return false if the status isn\'t "valid"', () => {
            expect(isValid({ status: 'aborted' })).to.be.equal(false);
        });
    });

    describe('isDirty', () => {
        it('Should return true if the status is "dirty"', () => {
            expect(isDirty({ status: 'dirty', value: 'foobar' })).to.be.equal(
                true
            );
        });

        it('Should return false if the status isn\'t "dirty"', () => {
            expect(isDirty({ status: 'aborted' })).to.be.equal(false);
        });
    });

    describe('isAborted', () => {
        it('Should return true if the status is "aborted"', () => {
            expect(isAborted({ status: 'aborted' })).to.be.equal(true);
        });

        it('Should return false if the status isn\'t "aborted"', () => {
            expect(isAborted({ status: 'valid', value: 'foobar' })).to.be.equal(
                false
            );
        });
    });

    describe('ParseStatus', () => {
        it('Should be set by default to "valid" value', () => {
            expect(new ParseStatus().value).to.be.equal('valid');
        });

        describe('dirty', () => {
            it('Should set to "dirty" status if it was "valid" status', () => {
                const status = new ParseStatus();
                status.dirty();
                expect(status.value).to.be.equal('dirty');
            });

            it('Should not set to "dirty" status if it wasn\'t "valid" status', () => {
                const status = new ParseStatus();
                status.abort();
                status.dirty();
                expect(status.value).to.be.equal('aborted');
            });
        });

        describe('abort', () => {
            it('Should set to "aborted" status if it was "valid" status', () => {
                const status = new ParseStatus();
                status.abort();
                expect(status.value).to.be.equal('aborted');
            });

            it('Should set to "aborted" status if it wasn\'t "valid" status', () => {
                const status = new ParseStatus();
                status.dirty();
                status.abort();
                expect(status.value).to.be.equal('aborted');
            });
        });

        describe('mergeObject', () => {
            it('Should return "valid" status if all keys and all values valid', () => {
                expect(
                    ParseStatus.mergeObject(new ParseStatus(), [
                        {
                            key: { status: 'valid', value: 'foo' },
                            value: { status: 'valid', value: 123 },
                        },
                        {
                            key: { status: 'valid', value: 'bar' },
                            value: { status: 'valid', value: 321 },
                        },
                    ]).status
                ).to.be.equal('valid');
            });

            it('Should return "aborted" status if one of the keys are aborted', () => {
                expect(
                    ParseStatus.mergeObject(new ParseStatus(), [
                        {
                            key: { status: 'valid', value: 'foo' },
                            value: { status: 'valid', value: 123 },
                        },
                        {
                            key: { status: 'aborted' },
                            value: { status: 'valid', value: 321 },
                        },
                    ]).status
                ).to.be.equal('aborted');
            });

            it('Should return "aborted" status if one of the values are aborted', () => {
                expect(
                    ParseStatus.mergeObject(new ParseStatus(), [
                        {
                            key: { status: 'valid', value: 'foo' },
                            value: { status: 'valid', value: 123 },
                        },
                        {
                            key: { status: 'valid', value: 'bar' },
                            value: { status: 'aborted' },
                        },
                    ]).status
                ).to.be.equal('aborted');
            });

            it('Should return "dirty" status if one of the keys are dirty', () => {
                expect(
                    ParseStatus.mergeObject(new ParseStatus(), [
                        {
                            key: { status: 'valid', value: 'foo' },
                            value: { status: 'valid', value: 123 },
                        },
                        {
                            key: { status: 'dirty', value: 'bar' },
                            value: { status: 'valid', value: 321 },
                        },
                    ]).status
                ).to.be.equal('dirty');
            });

            it('Should return "dirty" status if one of the values are dirty', () => {
                expect(
                    ParseStatus.mergeObject(new ParseStatus(), [
                        {
                            key: { status: 'valid', value: 'foo' },
                            value: { status: 'valid', value: 123 },
                        },
                        {
                            key: { status: 'valid', value: 'bar' },
                            value: { status: 'dirty', value: 321 },
                        },
                    ]).status
                ).to.be.equal('dirty');
            });

            it('Should return the final object', () => {
                const result = ParseStatus.mergeObject(new ParseStatus(), [
                    {
                        key: { status: 'valid', value: 'foo' },
                        value: { status: 'valid', value: 123 },
                    },
                    {
                        key: { status: 'valid', value: 'bar' },
                        value: { status: 'valid', value: 321 },
                    },
                ]);
                expect(result.status).to.be.equal('valid');
                if (result.status === 'valid') {
                    expect(result.value).to.be.deep.equal({
                        foo: 123,
                        bar: 321,
                    });
                }
            });

            it('Should strip a value if is undefined', () => {
                const result = ParseStatus.mergeObject(new ParseStatus(), [
                    {
                        key: { status: 'valid', value: 'foo' },
                        value: { status: 'valid', value: 123 },
                    },
                    {
                        key: { status: 'valid', value: 'bar' },
                        value: { status: 'valid', value: undefined },
                    },
                ]);
                expect(result.status).to.be.equal('valid');
                if (result.status === 'valid') {
                    expect(result.value).to.be.deep.equal({ foo: 123 });
                }
            });

            it('Should not strip a value if is undefined and `alwaysSet` set to `true`', () => {
                const result = ParseStatus.mergeObject(new ParseStatus(), [
                    {
                        key: { status: 'valid', value: 'foo' },
                        value: { status: 'valid', value: 123 },
                    },
                    {
                        key: { status: 'valid', value: 'bar' },
                        value: { status: 'valid', value: undefined },
                        alwaysSet: true,
                    },
                ]);
                expect(result.status).to.be.equal('valid');
                if (result.status === 'valid') {
                    expect(result.value).to.be.deep.equal({
                        foo: 123,
                        bar: undefined,
                    });
                }
            });
        });

        describe('mergeArray', () => {
            it('Should return "valid" status if all values valid', () => {
                expect(
                    ParseStatus.mergeArray(new ParseStatus(), [
                        { status: 'valid', value: 'foo' },
                        { status: 'valid', value: 321 },
                    ]).status
                ).to.be.equal('valid');
            });

            it('Should return "aborted" status if one of the values are aborted', () => {
                expect(
                    ParseStatus.mergeArray(new ParseStatus(), [
                        { status: 'valid', value: 123 },
                        { status: 'aborted' },
                    ]).status
                ).to.be.equal('aborted');
            });

            it('Should return "dirty" status if one of the values are dirty', () => {
                expect(
                    ParseStatus.mergeArray(new ParseStatus(), [
                        { status: 'valid', value: 123 },
                        { status: 'dirty', value: 321 },
                    ]).status
                ).to.be.equal('dirty');
            });

            it('Should return the final aray', () => {
                const result = ParseStatus.mergeArray(new ParseStatus(), [
                    { status: 'valid', value: 'foo' },
                    { status: 'valid', value: 321 },
                ]);
                expect(result.status).to.be.equal('valid');
                if (result.status === 'valid') {
                    expect(result.value).to.be.deep.equal(['foo', 321]);
                }
            });
        });
    });

    describe('ParseInputLazyPath', () => {
        it('Should put the current key inside a path', () => {
            const lazy = new ParseInputLazyPath(
                {
                    common: { issues: [] },
                    path: ['foo', 1],
                    parent: null,
                    data: 'azerty',
                    parsedType: 'string',
                },
                123,
                ['bar'],
                'foobar'
            );
            expect(lazy.path).to.be.deep.equal(['bar', 'foobar']);
        });
    });
});
