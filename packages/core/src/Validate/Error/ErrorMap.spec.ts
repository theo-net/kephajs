/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { ErrorValidator, Issue, IssueCode } from './Error';
import {
    defaultErrorMap,
    ErrorMap,
    overrideErrorMap,
    setErrorMap,
} from './ErrorMap';

describe('@kephajs/core/Validate/Error/ErrorMap', () => {
    describe('defaultErrorMap', () => {
        it('Should return message for invalidType', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.invalidType,
                        expected: 'string',
                        received: 'array',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({ message: 'Expected string, received array' });
        });

        it('Should return message for invalidType, required', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.invalidType,
                        expected: 'string',
                        received: 'undefined',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({ message: 'Required' });
        });

        it('Should return message for invalidLiteral', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.invalidLiteral,
                        expected: 'foobar',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message: 'Invalid literal value, expected "foobar"',
            });
        });

        it('Should return message for invalidLiteral with complex types', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.invalidLiteral,
                        expected: { a: 1, b: '2' },
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message: 'Invalid literal value, expected {"a":1,"b":"2"}',
            });
        });

        it('Should return message for custom', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.custom,
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({ message: 'Invalid input' });
        });

        it('Should return message for invalidUnion', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.invalidUnion,
                        unionErrors: [],
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({ message: 'Invalid input' });
        });

        it('Should return message for invalidUnionDiscriminator', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.invalidUnionDiscriminator,
                        options: ['string', 'number'],
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message:
                    "Invalid discriminator value. Expected 'string' | 'number'",
            });
        });

        it('Should return message for invalidEnumValue', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.invalidEnumValue,
                        received: 'foobar',
                        options: ['lol', 1],
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message:
                    "Invalid enum value. Expected 'lol' | 1, received 'foobar'",
            });
        });

        it('Should return message for unrecognizedKeys', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.unrecognizedKeys,
                        keys: ['foo', 'bar'],
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message: "Unrecognized key(s) in object: 'foo', 'bar'",
            });
        });

        it('Should return message for invalidArguments', () => {
            const issue: Issue = {
                code: IssueCode.invalidDate,
                path: [],
                message: 'foobar',
            };
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.invalidArguments,
                        argumentsError: new ErrorValidator([issue]),
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({ message: 'Invalid function arguments' });
        });

        it('Should return message for invalidReturnType', () => {
            const issue: Issue = {
                code: IssueCode.invalidDate,
                path: [],
                message: 'foobar',
            };
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.invalidReturnType,
                        returnTypeError: new ErrorValidator([issue]),
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({ message: 'Invalid function return type' });
        });

        it('Should return message for invalidDate', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.invalidDate,
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({ message: 'Invalid date' });
        });

        it('Should return message for invalidString for regex', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.invalidString,
                        validation: 'regex',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({ message: 'Invalid' });
        });

        it('Should return message for invalidString for other', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.invalidString,
                        validation: 'email',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({ message: 'Invalid email' });
        });

        it('Should return message for tooSmall, array', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.tooSmall,
                        minimum: 1,
                        inclusive: false,
                        type: 'array',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message: 'Array must contain more than 1 element(s)',
            });
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.tooSmall,
                        minimum: 1,
                        inclusive: true,
                        type: 'array',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message: 'Array must contain at least 1 element(s)',
            });
        });

        it('Should return message for tooSmall, string', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.tooSmall,
                        minimum: 1,
                        inclusive: false,
                        type: 'string',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message: 'String must contain over 1 character(s)',
            });
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.tooSmall,
                        minimum: 1,
                        inclusive: true,
                        type: 'string',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message: 'String must contain at least 1 character(s)',
            });
        });

        it('Should return message for tooSmall, number', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.tooSmall,
                        minimum: 1,
                        inclusive: false,
                        type: 'number',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({ message: 'Number must be greater than 1' });
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.tooSmall,
                        minimum: 1,
                        inclusive: true,
                        type: 'number',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message: 'Number must be greater than or equal to 1',
            });
        });

        it('Should return message for tooSmall, other', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.tooSmall,
                        minimum: 1,
                        inclusive: false,
                        type: 'set',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({ message: 'Invalid input' });
        });

        it('Should return message for tooBig, array', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.tooBig,
                        maximum: 1,
                        inclusive: false,
                        type: 'array',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message: 'Array must contain less than 1 element(s)',
            });
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.tooBig,
                        maximum: 1,
                        inclusive: true,
                        type: 'array',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message: 'Array must contain at most 1 element(s)',
            });
        });

        it('Should return message for tooBig, string', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.tooBig,
                        maximum: 1,
                        inclusive: false,
                        type: 'string',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message: 'String must contain under 1 character(s)',
            });
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.tooBig,
                        maximum: 1,
                        inclusive: true,
                        type: 'string',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message: 'String must contain at most 1 character(s)',
            });
        });

        it('Should return message for tooBig, number', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.tooBig,
                        maximum: 1,
                        inclusive: false,
                        type: 'number',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({ message: 'Number must be less than 1' });
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.tooBig,
                        maximum: 1,
                        inclusive: true,
                        type: 'number',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message: 'Number must be less than or equal to 1',
            });
        });

        it('Should return message for tooBig, other', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.tooBig,
                        maximum: 1,
                        inclusive: false,
                        type: 'set',
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({ message: 'Invalid input' });
        });

        it('Should return message for invalidIntersectionTypes', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.invalidIntersectionTypes,
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({
                message: 'Intersection results could not be merged',
            });
        });

        it('Should return message for notMultipleOf', () => {
            expect(
                defaultErrorMap(
                    {
                        path: [],
                        code: IssueCode.notMultipleOf,
                        multipleOf: 3,
                    },
                    { defaultError: '', data: '' }
                )
            ).to.be.deep.equal({ message: 'Number must be a multiple of 3' });
        });
    });

    describe('setErrorMap', () => {
        afterEach(() => {
            setErrorMap(defaultErrorMap);
        });

        it('Should be defaultErrorMap by default', () => {
            expect(overrideErrorMap).to.be.equal(defaultErrorMap);
        });

        it('Should define a new ErrorMap', () => {
            const customErrorMap: ErrorMap = (issue, ctx) => {
                if (issue.code === IssueCode.invalidType) {
                    if (issue.expected === 'string') {
                        return { message: 'Need to be a string!' };
                    }
                }
                return { message: ctx.defaultError };
            };
            setErrorMap(customErrorMap);
            expect(overrideErrorMap).to.be.equal(customErrorMap);
        });
    });
});
