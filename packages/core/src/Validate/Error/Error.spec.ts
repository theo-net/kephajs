/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { ErrorValidator, Issue, IssueCode } from './Error';

describe('@kephajs/core/Validate/Error/Error', () => {
    const errorMaker = (message: string): Issue => {
        return {
            code: IssueCode.invalidType,
            path: ['contactInfo', 'email'],
            expected: 'null',
            received: 'string',
            message,
        };
    };

    describe('errors', () => {
        it('Should return all issues', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidDate,
                    path: [],
                    message: 'foobar',
                },
                {
                    code: IssueCode.invalidDate,
                    path: [],
                    message: 'foobar',
                },
            ];
            const error = new ErrorValidator(issues);
            expect(error.errors).to.be.deep.equal(issues);
        });
    });

    describe('toString and message', () => {
        it('Should return a string version of all issues', () => {
            const expected = `[
  {
    "code": "invalidDate",
    "path": [
      "name"
    ],
    "message": "foobar"
  }
]`;
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidDate,
                    path: ['name'],
                    message: 'foobar',
                },
            ];
            const error = new ErrorValidator(issues);
            expect(error.message).to.be.equal(expected);
            expect(error.toString()).to.be.equal(expected);
        });
    });

    describe('isEmpty', () => {
        it('Should be equal to false if they are issue(s)', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidDate,
                    path: [],
                    message: 'foobar',
                },
                {
                    code: IssueCode.invalidDate,
                    path: [],
                    message: 'foobar',
                },
            ];
            const error = new ErrorValidator(issues);
            expect(error.isEmpty).to.be.equal(false);
        });

        it('Should be equal to true if they are no issue', () => {
            const error = new ErrorValidator([]);
            expect(error.isEmpty).to.be.equal(true);
        });
    });

    describe('addIssue', () => {
        it('Should add an issue', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidDate,
                    path: [],
                    message: 'foobar',
                },
                {
                    code: IssueCode.invalidDate,
                    path: [],
                    message: 'foobar',
                },
            ];
            const error = new ErrorValidator([issues[0]]);
            error.addIssue(issues[1]);
            expect(error.errors).to.be.deep.equal(issues);
        });
    });

    describe('addIssues', () => {
        it('Should add issues', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidDate,
                    path: [],
                    message: 'foobar',
                },
                {
                    code: IssueCode.invalidDate,
                    path: [],
                    message: 'foobar',
                },
            ];
            const error = new ErrorValidator([]);
            error.addIssues(issues);
            expect(error.errors).to.be.deep.equal(issues);
        });
    });

    describe('format', () => {
        it('Should format one deep level without path', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidDate,
                    path: [],
                    message: 'foo',
                },
                {
                    code: IssueCode.invalidDate,
                    path: [],
                    message: 'bar',
                },
            ];
            const error = new ErrorValidator(issues);
            expect(error.format()).to.be.deep.equal({
                _errors: ['foo', 'bar'],
            });
        });

        it('Should format one deep level with path', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidDate,
                    path: [1],
                    message: 'foo',
                },
                {
                    code: IssueCode.invalidDate,
                    path: ['test'],
                    message: 'bar',
                },
            ];
            const error = new ErrorValidator(issues);
            expect(error.format()).to.be.deep.equal({
                _errors: [],
                '1': { _errors: ['foo'] },
                test: { _errors: ['bar'] },
            });
        });

        it('Should format multiple level', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidDate,
                    path: ['contactInfo', 'email'],
                    message: 'foobar',
                },
                {
                    code: IssueCode.invalidDate,
                    path: ['name'],
                    message: 'foo',
                },
                {
                    code: IssueCode.invalidDate,
                    path: ['contactInfo', 'email'],
                    message: 'bar',
                },
            ];
            const error = new ErrorValidator(issues);
            expect(error.format()).to.be.deep.equal({
                _errors: [],
                name: { _errors: ['foo'] },
                contactInfo: {
                    _errors: [],
                    email: { _errors: ['foobar', 'bar'] },
                },
            });
        });

        it('Should customize the map result', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidDate,
                    path: ['name'],
                    message: 'foo',
                },
                {
                    code: IssueCode.invalidDate,
                    path: ['contactInfo', 'email'],
                    message: 'bar',
                },
            ];
            const error = new ErrorValidator(issues);
            expect(
                error.format(issue => {
                    return { code: issue.code, message: issue.message };
                })
            ).to.be.deep.equal({
                _errors: [],
                name: {
                    _errors: [{ code: IssueCode.invalidDate, message: 'foo' }],
                },
                contactInfo: {
                    _errors: [],
                    email: {
                        _errors: [
                            { code: IssueCode.invalidDate, message: 'bar' },
                        ],
                    },
                },
            });
        });

        it('Should process invalidUnion errors', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidUnion,
                    path: ['contactInfo', 'email'],
                    message: 'foobar',
                    unionErrors: [
                        new ErrorValidator([errorMaker('error1')]),
                        new ErrorValidator([
                            errorMaker('error2'),
                            errorMaker('error3'),
                        ]),
                    ],
                },
                {
                    code: IssueCode.invalidDate,
                    path: ['name'],
                    message: 'foo',
                },
                {
                    code: IssueCode.invalidDate,
                    path: ['contactInfo', 'email'],
                    message: 'bar',
                },
            ];
            const error = new ErrorValidator(issues);
            expect(error.format()).to.be.deep.equal({
                _errors: [],
                name: { _errors: ['foo'] },
                contactInfo: {
                    _errors: [],
                    email: { _errors: ['error1', 'error2', 'error3', 'bar'] },
                },
            });
        });

        it('Should process invalidReturnType errors', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidReturnType,
                    path: ['contactInfo', 'email'],
                    message: 'foobar',
                    returnTypeError: new ErrorValidator([errorMaker('error1')]),
                },
                {
                    code: IssueCode.invalidDate,
                    path: ['name'],
                    message: 'foo',
                },
                {
                    code: IssueCode.invalidDate,
                    path: ['contactInfo', 'email'],
                    message: 'bar',
                },
            ];
            const error = new ErrorValidator(issues);
            expect(error.format()).to.be.deep.equal({
                _errors: [],
                name: { _errors: ['foo'] },
                contactInfo: {
                    _errors: [],
                    email: { _errors: ['error1', 'bar'] },
                },
            });
        });

        it('Should process invalidArguments errors', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidArguments,
                    path: ['contactInfo', 'email'],
                    message: 'foobar',
                    argumentsError: new ErrorValidator([errorMaker('error1')]),
                },
                {
                    code: IssueCode.invalidDate,
                    path: ['name'],
                    message: 'foo',
                },
                {
                    code: IssueCode.invalidDate,
                    path: ['contactInfo', 'email'],
                    message: 'bar',
                },
            ];
            const error = new ErrorValidator(issues);
            expect(error.format()).to.be.deep.equal({
                _errors: [],
                name: { _errors: ['foo'] },
                contactInfo: {
                    _errors: [],
                    email: { _errors: ['error1', 'bar'] },
                },
            });
        });
    });

    describe('flatten', () => {
        it('Should format one deep level without path', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidDate,
                    path: [],
                    message: 'foo',
                },
                {
                    code: IssueCode.invalidDate,
                    path: [],
                    message: 'bar',
                },
            ];
            const error = new ErrorValidator(issues);
            expect(error.flatten()).to.be.deep.equal({
                fieldErrors: {},
                formErrors: ['foo', 'bar'],
            });
        });

        it('Should format one deep level with path', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidDate,
                    path: [1],
                    message: 'foo',
                },
                {
                    code: IssueCode.invalidDate,
                    path: ['test'],
                    message: 'bar',
                },
            ];
            const error = new ErrorValidator(issues);
            expect(error.flatten()).to.be.deep.equal({
                fieldErrors: {
                    '1': ['foo'],
                    test: ['bar'],
                },
                formErrors: [],
            });
        });

        it('Should format multiple level', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidDate,
                    path: ['contactInfo', 'email'],
                    message: 'foobar',
                },
                {
                    code: IssueCode.invalidDate,
                    path: ['name'],
                    message: 'foo',
                },
                {
                    code: IssueCode.invalidDate,
                    path: ['contactInfo', 'email'],
                    message: 'bar',
                },
            ];
            const error = new ErrorValidator(issues);
            expect(error.flatten()).to.be.deep.equal({
                fieldErrors: {
                    name: ['foo'],
                    contactInfo: ['foobar', 'bar'],
                },
                formErrors: [],
            });
        });

        it('Should customize the map result', () => {
            const issues: Issue[] = [
                {
                    code: IssueCode.invalidDate,
                    path: ['name'],
                    message: 'foo',
                },
                {
                    code: IssueCode.invalidDate,
                    path: ['contactInfo', 'email'],
                    message: 'bar',
                },
            ];
            const error = new ErrorValidator(issues);
            expect(
                error.flatten(issue => {
                    return { code: issue.code, message: issue.message };
                })
            ).to.be.deep.equal({
                fieldErrors: {
                    name: [{ code: IssueCode.invalidDate, message: 'foo' }],
                    contactInfo: [
                        { code: IssueCode.invalidDate, message: 'bar' },
                    ],
                },
                formErrors: [],
            });
        });
    });
});
