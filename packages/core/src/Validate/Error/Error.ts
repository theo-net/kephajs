/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as Helpers from '../../Helpers/Helpers';
import { ParsedType } from '../helpers/parseUtils';
import { Primitive } from '../Type/Type';

/**
 * All validation errors thrown by the validator are instances of `ErrorValidator`.
 *
 * Each `ErrorValidator` has an `issues` property that is an array of `Issue`. Each issue
 * documents a problem that occured during validation.
 */

export const IssueCode = Helpers.arrayToEnum([
    'invalidType',
    'invalidLiteral',
    'custom',
    'invalidUnion',
    'invalidUnionDiscriminator',
    'invalidEnumValue',
    'unrecognizedKeys',
    'invalidArguments',
    'invalidReturnType',
    'invalidDate',
    'invalidString',
    'tooSmall',
    'tooBig',
    'invalidIntersectionTypes',
    'notMultipleOf',
]);
// eslint-disable-next-line @typescript-eslint/no-redeclare
export type IssueCode = keyof typeof IssueCode;

export type IssueBase = {
    path: (string | number)[];
    message?: string;
};

/**
 * The type is invalid.
 *
 *  - `expected`: the expected value
 *  - `received`: the receveid value by the validator
 */
export interface InvalidTypeIssue extends IssueBase {
    code: typeof IssueCode.invalidType;
    expected: ParsedType;
    received: ParsedType;
}

/**
 * The litteral type are invalid
 *
 *  - `expected`: the expected value
 */
export interface InvalidLiteralIssue extends IssueBase {
    code: typeof IssueCode.invalidLiteral;
    expected: unknown;
}

/**
 * Some keys are unrecognized by the validator
 *
 *  - `keys`: the list of unrecognized keys
 */
export interface UnrecognizedKeysIssue extends IssueBase {
    code: typeof IssueCode.unrecognizedKeys;
    keys: string[];
}

/**
 * Error in the union
 *
 *  - `unionErrors`: The errors thrown by each element of the union
 */
export interface InvalidUnionIssue extends IssueBase {
    code: typeof IssueCode.invalidUnion;
    unionErrors: ErrorValidator[];
}

/**
 * If the union consists of object schemas all identifiable by a common property,
 * it's possible to use the `discriminatedUnion` method.
 *
 * The errors will be more human friendly.
 *
 *  - `options`: the options of the union
 */
export interface InvalidUnionDiscriminatorIssue extends IssueBase {
    code: typeof IssueCode.invalidUnionDiscriminator;
    options: Primitive[];
}

/**
 *  - `received`: the receveid value by the validator
 *  - `options`: the possible values in the enum
 */
export interface InvalidEnumValueIssue extends IssueBase {
    received: string | number;
    code: typeof IssueCode.invalidEnumValue;
    options: (string | number)[];
}

/**
 * Special error code only thrown by a wrapped function returned by
 * `.function().implement()`.
 *
 *  - `argumentsError`: `ErrorValidator` containing the validation error details
 */
export interface InvalidArgumentsIssue extends IssueBase {
    code: typeof IssueCode.invalidArguments;
    argumentsError: ErrorValidator;
}

/**
 * Special error code only thrown by a wrapped function returned by
 * `.function().implement()`.
 *
 *  - `returnTypeError`: `ErrorValidator` containing the validation error details
 */
export interface InvalidReturnTypeIssue extends IssueBase {
    code: typeof IssueCode.invalidReturnType;
    returnTypeError: ErrorValidator;
}

/**
 * Need to be a `Date` object.
 */
export interface InvalidDateIssue extends IssueBase {
    code: typeof IssueCode.invalidDate;
}

export type StringValidation = 'email' | 'url' | 'uuid' | 'regex' | 'cuid';

/**
 * Validate a string
 *
 *  - `validation`: which built-in sring vlaidator failed
 */
export interface InvalidStringIssue extends IssueBase {
    code: typeof IssueCode.invalidString;
    validation: StringValidation;
}

/**
 *  - `type`: the type of the data failing validation
 *  - `minimum`: the minimum length (`array`, `string`, `set`) / value (`number`)
 *  - `inclusive`: whether the minimum included in the range of acceptable values
 */
export interface TooSmallIssue extends IssueBase {
    code: typeof IssueCode.tooSmall;
    minimum: number;
    inclusive: boolean;
    type: 'array' | 'string' | 'number' | 'set';
}

/**
 *  - `type`: the type of the data failing validation
 *  - `maximum`: the maximum length (`array`, `string`, `set`) / value (`number`)
 *  - `inclusive`: whether the maximum included in the range of acceptable values
 */
export interface TooBigIssue extends IssueBase {
    code: typeof IssueCode.tooBig;
    maximum: number;
    inclusive: boolean;
    type: 'array' | 'string' | 'number' | 'set';
}

/**
 * Intersection results could not be merged
 */
export interface InvalidIntersectionTypesIssue extends IssueBase {
    code: typeof IssueCode.invalidIntersectionTypes;
}

/**
 * If the `number` is not an multiple of `multipleOf`.
 */
export interface NotMultipleOfIssue extends IssueBase {
    code: typeof IssueCode.notMultipleOf;
    multipleOf: number;
}

/**
 * For all of the custom error
 */
export interface CustomIssue extends IssueBase {
    code: typeof IssueCode.custom;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    params?: { [k: string]: any };
}

export type IssueOptionalMessage =
    | InvalidTypeIssue
    | InvalidLiteralIssue
    | UnrecognizedKeysIssue
    | InvalidUnionIssue
    | InvalidUnionDiscriminatorIssue
    | InvalidEnumValueIssue
    | InvalidArgumentsIssue
    | InvalidReturnTypeIssue
    | InvalidDateIssue
    | InvalidStringIssue
    | TooSmallIssue
    | TooBigIssue
    | InvalidIntersectionTypesIssue
    | NotMultipleOfIssue
    | CustomIssue;

/**
 * Every issue has these fields:
 *
 *  - `code`: the code issue
 *  - `path`: the path to the field
 *  - `message`: the message of the issue
 */
export type Issue = IssueOptionalMessage & { message: string };

export type OmitKeys<T, K extends string> = Pick<T, Exclude<keyof T, K>>;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type StripPath<T extends object> = T extends any ? OmitKeys<T, 'path'> : never;
export type IssueData = StripPath<IssueOptionalMessage> & {
    path?: (string | number)[];
    fatal?: boolean;
};

export type FormattedError<T, U = string> = {
    _errors: U[];
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
} & (T extends [any, ...any[]]
    ? { [K in keyof T]?: FormattedError<T[K]> }
    : // eslint-disable-next-line @typescript-eslint/no-explicit-any
    T extends any[]
    ? { [k: number]: FormattedError<T[number]> }
    : T extends object
    ? { [K in keyof T]?: FormattedError<T[K]> }
    : unknown);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type AllKeys<T> = T extends any ? keyof T : never;

export type TypeToFlattenedError<T, U = string> = {
    formErrors: U[];
    fieldErrors: {
        [P in AllKeys<T>]?: U[];
    };
};

/**
 * The error generated by the validator
 *
 * You can get the list of the issues with `.errors`
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export class ErrorValidator<T = any> extends Error {
    protected _issues: Issue[] = [];

    /**
     * Return all issues
     */
    get errors() {
        return this._issues;
    }

    /**
     * Customize the default `Error.message` property
     */
    get message() {
        return this.toString();
    }

    toString() {
        return JSON.stringify(this._issues, null, 2);
    }

    get isEmpty(): boolean {
        return this._issues.length === 0;
    }

    constructor(issues: Issue[]) {
        super();

        this._issues = issues;
    }

    addIssue = (sub: Issue) => {
        this._issues = [...this._issues, sub];
    };

    addIssues = (subs: Issue[] = []) => {
        this._issues = [...this._issues, ...subs];
    };

    format(): FormattedError<T>;
    format<U>(mapper: (issue: Issue) => U): FormattedError<T, U>;

    /**
     * Format the error
     *
     * With this method, you don't need to parse a list of Issue, you can just
     * read for each field, the list of errors.
     *
     *      {
     *          name: {
     *              _errors: ['Expected string, received null']
     *          },
     *          contactInfo: {
     *              email: {
     *                  _errors: ['Invalid email']
     *              }
     *          }
     *      }
     *
     * @param mapper To transform the result like:
     *                  result.error.flatten((issue: ZodIssue) => ({
     *                      message: issue.message,
     *                      errorCode: issue.code,
     *                  }));
     *               You will have:
     *                  {
     *                      formErrors: [],
     *                      fieldErrors: {
     *                          name: [
     *                              {message: "Expected string, received null", errorCode: "invalid_type"}
     *                          ]
     *                          contactInfo: [
     *                              {message: "Invalid email", errorCode: "invalid_string"}
     *                          ]
     *                      },
     *                  }
     */
    format(
        mapper: (issue: Issue) => string = (issue: Issue) => issue.message
    ): FormattedError<T> {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const fieldErrors: FormattedError<T> = { _errors: [] } as any;
        const processError = (error: ErrorValidator) => {
            for (const issue of error.errors) {
                if (issue.code === 'invalidUnion') {
                    issue.unionErrors.map(processError);
                } else if (issue.code === 'invalidReturnType') {
                    processError(issue.returnTypeError);
                } else if (issue.code === 'invalidArguments') {
                    processError(issue.argumentsError);
                } else if (issue.path.length === 0) {
                    fieldErrors._errors.push(mapper(issue));
                } else {
                    // eslint-disable-next-line @typescript-eslint/no-explicit-any
                    let curr: any = fieldErrors;
                    let i = 0;
                    while (i < issue.path.length) {
                        const el = issue.path[i];
                        const terminal = i === issue.path.length - 1;

                        if (!terminal) {
                            curr[el] = curr[el] || { _errors: [] };
                        } else {
                            curr[el] = curr[el] || { _errors: [] };
                            curr[el]._errors.push(mapper(issue));
                        }

                        curr = curr[el];
                        i++;
                    }
                }
            }
        };

        processError(this);

        return fieldErrors;
    }

    flatten(): TypeToFlattenedError<T>;
    flatten<U>(mapper?: (issue: Issue) => U): TypeToFlattenedError<T, U>;

    /**
     * `format()` returns a deeply nested object. With `flatten` you have a
     * one level deep.
     *
     *      {
     *          formErrors: [],
     *          fieldErrors: {
     *              name: ['Expected string, received null'],
     *              contactInfo: ['Invalid email']
     *          },
     *      }
     *
     * `formErrors` is a list of issues that occurred on the root of the object schema.
     *
     * @param mapper To transform the result as `format` method
     */
    flatten<U = string>(
        mapper: (issue: Issue) => U = (issue: Issue) =>
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            issue.message as any
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ): any {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const fieldErrors: any = {};
        const formErrors: U[] = [];
        for (const sub of this._issues) {
            if (sub.path.length > 0) {
                fieldErrors[sub.path[0]] = fieldErrors[sub.path[0]] || [];
                fieldErrors[sub.path[0]].push(mapper(sub));
            } else {
                formErrors.push(mapper(sub));
            }
        }
        return { formErrors, fieldErrors };
    }
}
