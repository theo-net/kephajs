/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import * as Error from './Error/Error';
import {
    ErrorMap,
    defaultErrorMap,
    overrideErrorMap,
    setErrorMap,
} from './Error/ErrorMap';
import * as Validator from './Validator';

import { AnyType } from './Type/Any';
import { BigIntType } from './Type/BigInt';
import { BooleanType } from './Type/Boolean';
import { DateType } from './Type/Date';
import { LiteralType } from './Type/Literal';
import { NeverType } from './Type/Never';
import { NullType } from './Type/Null';
import { NumberType } from './Type/Number';
import { StringType } from './Type/String';
import { UndefinedType } from './Type/Undefined';
import { UnknownType } from './Type/Unknown';
import { VoidType } from './Type/Void';
import { NaNType } from './Type/NaNType';
import { EnumType } from './Type/Enum';
import { NativeEnumType } from './Type/NativeEnum';
import { ObjectType } from './Type/Object';
import { Optional } from './Type/Optional';
import { ArrayType } from './Type/Array';
import { Nullable } from './Type/Nullable';
import { TupleType } from './Type/Tuple';
import { Union } from './Type/Union';
import { DiscriminatedUnion } from './Type/DiscriminatedUnion';
import { RecordType } from './Type/Record';
import { MapType } from './Type/Map';
import { SetType } from './Type/Set';
import { Intersection } from './Type/Intersection';
import { Effects } from './Type/Effects';
import { LazyType } from './Type/Lazy';
import { TypeOf } from './Type/Type';
import { AssertEqual } from './helpers/testUtils';
import { FunctionType } from './Type/Function';

describe('@kephajs/core/Validate/Validator', () => {
    it('Should export Error', () => {
        expect(Validator).to.contain(Error);
    });

    describe('Primitives', () => {
        const validator = new Validator.Validator();

        it('Should have the primitive string', () => {
            expect(validator.string).to.be.equal(StringType.create);
        });

        it('Should have the primitive number', () => {
            expect(validator.number).to.be.equal(NumberType.create);
        });

        it('Should have the primitive bigint', () => {
            expect(validator.bigint).to.be.equal(BigIntType.create);
        });

        it('Should have the primitive boolean', () => {
            expect(validator.boolean).to.be.equal(BooleanType.create);
        });

        it('Should have the primitive date', () => {
            expect(validator.date).to.be.equal(DateType.create);
        });

        it('Should have the primitive undefined', () => {
            expect(validator.undefined).to.be.equal(UndefinedType.create);
        });

        it('Should have the primitive null', () => {
            expect(validator.null).to.be.equal(NullType.create);
        });

        it('Should have the primitive void', () => {
            expect(validator.void).to.be.equal(VoidType.create);
        });

        it('Should have the primitive any', () => {
            expect(validator.any).to.be.equal(AnyType.create);
        });

        it('Should have the primitive unknown', () => {
            expect(validator.unknown).to.be.equal(UnknownType.create);
        });

        it('Should have the primitive never', () => {
            expect(validator.never).to.be.equal(NeverType.create);
        });
    });

    it('Should have literals', () => {
        expect(new Validator.Validator().literal).to.be.equal(
            LiteralType.create
        );
    });

    it('Should have NaN', () => {
        expect(new Validator.Validator().nan).to.be.equal(NaNType.create);
    });

    it('Should have EnumType', () => {
        expect(new Validator.Validator().enum).to.be.equal(EnumType.create);
    });

    it('Should have NativeEnumType', () => {
        expect(new Validator.Validator().nativeEnum).to.be.equal(
            NativeEnumType.create
        );
    });

    it('Should have ObjectType', () => {
        expect(new Validator.Validator().object).to.be.equal(ObjectType.create);
    });

    it('Should have ArrayType', () => {
        expect(new Validator.Validator().array).to.be.equal(ArrayType.create);
    });

    it('Should have TupleType', () => {
        expect(new Validator.Validator().tuple).to.be.equal(TupleType.create);
    });

    it('Should have RecordType', () => {
        expect(new Validator.Validator().record).to.be.equal(RecordType.create);
    });

    it('Should have MapType', () => {
        expect(new Validator.Validator().map).to.be.equal(MapType.create);
    });

    it('Should have SetType', () => {
        expect(new Validator.Validator().set).to.be.equal(SetType.create);
    });

    it('Should have FunctionType', () => {
        expect(new Validator.Validator().function).to.be.equal(
            FunctionType.create
        );
    });

    it('Should have Optional type', () => {
        expect(new Validator.Validator().optional).to.be.equal(Optional.create);
    });

    it('Should have Nullable type', () => {
        expect(new Validator.Validator().nullable).to.be.equal(Nullable.create);
    });

    it('Should have Union type', () => {
        expect(new Validator.Validator().union).to.be.equal(Union.create);
    });

    it('Should have DiscriminatedUnion type', () => {
        expect(new Validator.Validator().discriminatedUnion).to.be.equal(
            DiscriminatedUnion.create
        );
    });

    it('Should have Intersection type', () => {
        expect(new Validator.Validator().intersection).to.be.equal(
            Intersection.create
        );
    });

    it('Should have preprocess method', () => {
        expect(new Validator.Validator().preprocess).to.be.equal(
            Effects.createWithPreprocess
        );
    });

    it('Should have LazyType type', () => {
        expect(new Validator.Validator().lazy).to.be.equal(LazyType.create);
    });

    it('Should have late', () => {
        expect(new Validator.Validator().late).to.be.deep.equal({
            object: ObjectType.lazycreate,
        });
    });

    describe('setErrorMap', () => {
        afterEach(() => {
            setErrorMap(defaultErrorMap);
        });

        it('Should be defaultErrorMap by default', () => {
            expect(overrideErrorMap).to.be.equal(defaultErrorMap);
        });

        it('Should define a new ErrorMap', () => {
            const customErrorMap: ErrorMap = (issue, ctx) => {
                if (issue.code === Error.IssueCode.invalidType) {
                    if (issue.expected === 'string') {
                        return { message: 'Need to be a string!' };
                    }
                }
                return { message: ctx.defaultError };
            };
            setErrorMap(customErrorMap);
            expect(overrideErrorMap).to.be.equal(customErrorMap);
        });
    });

    it('Should test instanceof', () => {
        class Test {}
        class Subtest extends Test {}

        const TestSchema = new Validator.Validator().instanceof(Test);
        const SubtestSchema = new Validator.Validator().instanceof(Subtest);

        expect(TestSchema.parse(new Test())).to.be.instanceof(Test);
        expect(TestSchema.parse(new Subtest())).to.be.instanceof(Subtest);
        expect(SubtestSchema.parse(new Subtest())).to.be.instanceof(Subtest);

        expect(() => SubtestSchema.parse(new Test())).to.throw(
            /Input not instance of Subtest/
        );
        expect(() => TestSchema.parse(12)).to.throw(
            /Input not instance of Test/
        );

        const f1: AssertEqual<Test, TypeOf<typeof TestSchema>> = true;
        expect(f1).to.be.equal(true);
    });
});
