/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { FormattedError, TypeToFlattenedError } from './Validator';
import { Type, TypeOf } from './Type/Type';

/**
 * Extract type signature of `.format()` from a schema validation error.
 */
export type InferFormattedError<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    T extends Type<any, any, any>,
    U = string
> = FormattedError<TypeOf<T>, U>;

/**
 * Extract type signature of `.flatten()` from a schema validation error.
 */
export type InferFlattenedErrors<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    T extends Type<any, any, any>,
    U = string
> = TypeToFlattenedError<TypeOf<T>, U>;
