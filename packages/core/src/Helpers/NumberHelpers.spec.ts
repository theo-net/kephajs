/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import * as NumberHelpers from './NumberHelpers';

describe('@kephajs/core/Helpers/NumberHelpers', () => {
    describe('numberFormat', () => {
        it('Should format a number', () => {
            expect(NumberHelpers.numberFormat(42)).to.be.equal('42');
            expect(NumberHelpers.numberFormat(42.2)).to.be.equal('42');
            expect(NumberHelpers.numberFormat(42.256)).to.be.equal('42');
            expect(NumberHelpers.numberFormat(42.25667)).to.be.equal('42');
            expect(NumberHelpers.numberFormat(1242.25667)).to.be.equal('1,242');
            expect(NumberHelpers.numberFormat(12345642.25667)).to.be.equal(
                '12,345,642'
            );
        });

        it('Should set decimals as 0 if negative number', () => {
            expect(NumberHelpers.numberFormat(42, -2)).to.be.equal('42');
            expect(NumberHelpers.numberFormat(42.2, -2)).to.be.equal('42');
            expect(NumberHelpers.numberFormat(42.256, -2)).to.be.equal('42');
            expect(NumberHelpers.numberFormat(42.25667, -2)).to.be.equal('42');
            expect(NumberHelpers.numberFormat(1242.25667, -2)).to.be.equal(
                '1,242'
            );
            expect(NumberHelpers.numberFormat(12345642.25667, -2)).to.be.equal(
                '12,345,642'
            );
        });

        it('Should define the decimals number', () => {
            expect(NumberHelpers.numberFormat(42, 2)).to.be.equal('42.00');
            expect(NumberHelpers.numberFormat(42.2, 2)).to.be.equal('42.20');
            expect(NumberHelpers.numberFormat(42.256, 2)).to.be.equal('42.26');
            expect(NumberHelpers.numberFormat(42.25667, 2)).to.be.equal(
                '42.26'
            );
            expect(NumberHelpers.numberFormat(1242.25667, 2)).to.be.equal(
                '1,242.26'
            );
            expect(NumberHelpers.numberFormat(12345642.25667, 2)).to.be.equal(
                '12,345,642.26'
            );
        });

        it('Should change the decimal separator', () => {
            expect(NumberHelpers.numberFormat(42, 2, '!')).to.be.equal('42!00');
            expect(NumberHelpers.numberFormat(42.2, 2, '!')).to.be.equal(
                '42!20'
            );
            expect(NumberHelpers.numberFormat(42.256, 2, '!')).to.be.equal(
                '42!26'
            );
            expect(NumberHelpers.numberFormat(42.25667, 4, '!')).to.be.equal(
                '42!2567'
            );
            expect(NumberHelpers.numberFormat(1242.25667, 2, '!')).to.be.equal(
                '1,242!26'
            );
            expect(
                NumberHelpers.numberFormat(12345642.25667, 2, '!')
            ).to.be.equal('12,345,642!26');
        });

        it('Should change the thousands separator', () => {
            expect(NumberHelpers.numberFormat(42, 4, '.', ' ')).to.be.equal(
                '42.0000'
            );
            expect(NumberHelpers.numberFormat(42.2, 4, '.', ' ')).to.be.equal(
                '42.2000'
            );
            expect(NumberHelpers.numberFormat(42.256, 4, '.', ' ')).to.be.equal(
                '42.2560'
            );
            expect(
                NumberHelpers.numberFormat(42.25667, 4, '.', ' ')
            ).to.be.equal('42.2567');
            expect(
                NumberHelpers.numberFormat(1242.25667, 4, '.', ' ')
            ).to.be.equal('1 242.2567');
            expect(
                NumberHelpers.numberFormat(12345642.25667, 4, '.', ' ')
            ).to.be.equal('12 345 642.2567');
        });
    });
});
