/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export function forEach<
    T extends unknown[] | Record<K, T> | Iterable<T>,
    K extends string | number | symbol
>(
    collection: T,
    fn: (v: T, k?: K, iterable?: T) => void | boolean,
    thisArg?: unknown
): T {
    if (typeof fn !== 'function')
        throw new TypeError(fn + ' is not a function');

    let k = -1;

    if (Array.isArray(collection)) {
        const length = collection.length;

        while (++k < length) {
            if (fn.call(thisArg, collection[k], k as K, collection) === false)
                break;
        }
    } else if (
        collection !== null &&
        typeof collection === 'object' &&
        typeof (collection as Record<symbol, unknown>)[Symbol.iterator] ===
            'function'
    ) {
        for (const value of collection as Iterable<T>) {
            if (fn.call(thisArg, value, ++k as K, collection) === false) break;
        }
    } else if (collection !== undefined && collection !== null) {
        const iterable =
                !!collection &&
                (typeof collection === 'object' ||
                    typeof collection === 'function')
                    ? collection
                    : Object(collection),
            props = Object.keys(collection),
            length = props.length;

        while (++k < length) {
            const key = props[k];
            if (fn.call(thisArg, iterable[key], key as K, iterable) === false)
                break;
        }
    }

    return collection;
}
