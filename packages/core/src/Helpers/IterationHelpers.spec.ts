/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { forEach } from './IterationHelpers';

describe('@kephajs/core/Helpers/IterationHelpers', () => {
    // eslint-disable-next-line @typescript-eslint/ban-types
    const IterationHelpers: Record<string, Function> = {
        forEach,
    };

    const methods: string[] = ['forEach'];
    const collectionMethods: string[] = ['forEach'];
    const iterationMethods: string[] = ['forEach'];
    const objectMethods: string[] = [];
    const rightMethods: string[] = [];
    const exitMethods: string[] = ['forEach'];
    const arrayIteratorMethods: string[] = ['forEach'];

    class ArrayIterator<T> implements Iterable<T> {
        constructor(private _values: T[]) {}

        [Symbol.iterator]() {
            let counter = 0;
            const values = this._values;

            return {
                next() {
                    return {
                        done: counter >= values.length,
                        value: values[counter++],
                    };
                },
            };
        }
    }

    methods.forEach(methodName => {
        let array = [1, 2, 3];
        const func = IterationHelpers[methodName];

        it(
            methodName + ' should provide the correct iteratee arguments',
            function () {
                const args: unknown[] = [],
                    expected = [
                        [1, 0, true],
                        [2, 1, true],
                        [3, 2, true],
                    ];

                func(
                    array,
                    (value: unknown, index: unknown, collection: unknown) => {
                        args.push([value, index, collection == array]);
                    }
                );

                if (rightMethods.indexOf(methodName) > -1) {
                    const tmp = expected[0];
                    expected[0] = expected[2];
                    expected[2] = tmp;
                }

                expect(args).to.be.deep.equal(expected);
            }
        );

        it(methodName + ' should treat sparse arrays as dense', () => {
            array = [1];
            array[2] = 3;

            const expected =
                objectMethods.indexOf(methodName) > -1
                    ? [
                          [1, '0', array],
                          [undefined, '1', array],
                          [3, '2', array],
                      ]
                    : [
                          [1, 0, array],
                          [undefined, 1, array],
                          [3, 2, array],
                      ];

            if (rightMethods.indexOf(methodName) > -1) expected.reverse();

            const argsList: unknown[] = [];
            func(
                array,
                (value: unknown, index: unknown, collection: unknown) => {
                    argsList.push([value, index, collection]);
                }
            );

            if (objectMethods.indexOf(methodName) === -1)
                expect(argsList).to.be.deep.equal(expected);
        });
    });

    methods
        .filter(method => objectMethods.indexOf(method) === -1)
        .forEach(methodName => {
            const array = [1, 2, 3],
                func = IterationHelpers[methodName];

            (array as unknown as Record<string, number>).a = 1;

            it(
                methodName + ' should not iterate custom properties on arrays',
                function () {
                    const keys: unknown[] = [];
                    func(array, (_value: unknown, key: unknown) => {
                        keys.push(key);
                    });

                    expect(keys.indexOf('a')).to.be.equal(-1);
                }
            );
        });

    methods.forEach(methodName => {
        const func = IterationHelpers[methodName];

        it(
            methodName +
                ' iterates over own string keyed properties of objects',
            function () {
                class Foo {
                    a = 1;
                }
                (Foo.prototype as unknown as Record<string, unknown>).b = 2;

                const values: unknown[] = [];
                func(new Foo(), (value: unknown) => {
                    values.push(value);
                });
                expect(values).to.be.deep.equal([1]);
            }
        );
    });

    iterationMethods.forEach(methodName => {
        const array = [1, 2, 3],
            func = IterationHelpers[methodName];

        it(methodName + ' should return the collection', () => {
            expect(func(array, Boolean)).to.be.equal(array);
        });
    });

    methods.forEach(methodName => {
        const func = IterationHelpers[methodName],
            isSome = methodName == 'some';

        it(
            methodName + ' should ignore changes to `array.length`',
            function () {
                let count = 0;
                const array = [1];

                func(
                    array,
                    () => {
                        if (++count == 1) array.push(2);
                        return !isSome;
                    },
                    null
                );

                expect(count).to.be.equal(1);
            }
        );
    });

    methods.concat(collectionMethods).forEach(methodName => {
        const func = IterationHelpers[methodName],
            isSome = methodName == 'some';

        it(
            methodName + ' should ignore added `object` properties',
            function () {
                let count = 0;
                const object: Record<string, unknown> = { a: 1 };

                func(
                    object,
                    function () {
                        if (++count == 1) object.b = 2;
                        return !isSome;
                    },
                    null
                );

                expect(count).to.be.equal(1);
            }
        );
    });

    exitMethods.forEach(methodName => {
        const func = IterationHelpers[methodName];

        it(methodName + ' can exit early when iterating arrays', () => {
            const array = [1, 2, 3],
                values: unknown[] = [];

            func(array, (value: unknown, other: unknown) => {
                values.push(Array.isArray(value) ? other : value);
                return false;
            });

            expect(values).to.be.deep.equal([
                rightMethods.indexOf(methodName) > -1 ? 3 : 1,
            ]);
        });

        it(methodName + ' can exit early when iterating objects', () => {
            const object = { a: 1, b: 2, c: 3 },
                values: unknown[] = [];

            func(object, (value: unknown, other: unknown) => {
                values.push(Array.isArray(value) ? other : value);
                return false;
            });

            expect(values.length).to.be.equal(1);
        });
    });

    arrayIteratorMethods.forEach(methodName => {
        const array = new ArrayIterator([1, 2, 3, 4]);
        const func = IterationHelpers[methodName];

        it(methodName + ' should work with Iterator interface', () => {
            const args: unknown[] = [],
                expected = [
                    [1, 0, true],
                    [2, 1, true],
                    [3, 2, true],
                    [4, 3, true],
                ];

            func(
                array,
                (value: unknown, index: unknown, collection: unknown) => {
                    args.push([value, index, collection == array]);
                }
            );

            if (rightMethods.indexOf(methodName) > -1) {
                const tmp = expected[0];
                expected[0] = expected[2];
                expected[2] = tmp;
            }

            expect(args).to.be.deep.equal(expected);
        });
    });
});
