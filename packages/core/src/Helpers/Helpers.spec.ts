/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import * as Helpers from './Helpers';
import { SemVer } from '../SemVer/SemVer';

describe('@kephajs/core/Helpers', () => {
    it('Should export SemVer', () => {
        expect(Helpers.SemVer).to.be.equal(SemVer);
    });

    describe('arrayToEnum', () => {
        it('Should convert array of string to enum', () => {
            expect(Helpers.arrayToEnum(['foo', 'bar'])).to.be.deep.equal({
                foo: 'foo',
                bar: 'bar',
            });
        });
    });

    describe('camelCase', () => {
        it('should work with numbers', () => {
            expect(Helpers.camelCase('12 feet')).to.equal('12Feet');
            expect(Helpers.camelCase('enable 6h format')).to.equal(
                'enable6HFormat'
            );
            expect(Helpers.camelCase('enable 24H format')).to.equal(
                'enable24HFormat'
            );
            expect(Helpers.camelCase('too legit 2 quit')).to.equal(
                'tooLegit2Quit'
            );
            expect(Helpers.camelCase('walk 500 miles')).to.equal(
                'walk500Miles'
            );
            expect(Helpers.camelCase('xhr2 request')).to.equal('xhr2Request');
        });

        it('should handle acronyms', () => {
            ['safe HTML', 'safeHTML'].forEach(function (string) {
                expect(Helpers.camelCase(string)).to.equal('safeHtml');
            });

            ['escape HTML entities', 'escapeHTMLEntities'].forEach(function (
                string
            ) {
                expect(Helpers.camelCase(string)).to.equal(
                    'escapeHtmlEntities'
                );
            });

            ['XMLHttpRequest', 'XmlHTTPRequest'].forEach(function (string) {
                expect(Helpers.camelCase(string)).to.equal('xmlHttpRequest');
            });
        });
    });

    describe('joinValues', () => {
        it('Should join the values of an array', () => {
            expect(Helpers.joinValues([1, 'foo', 'bar'])).to.be.equal(
                "1 | 'foo' | 'bar'"
            );
        });

        it('Should customize the separator', () => {
            expect(Helpers.joinValues([1, 'foo', 'bar'], '; ')).to.be.equal(
                "1; 'foo'; 'bar'"
            );
        });

        it('Should customize the escape char for string values', () => {
            expect(
                Helpers.joinValues([1, 'foo', 'bar'], ' | ', '"')
            ).to.be.equal('1 | "foo" | "bar"');
        });
    });

    describe('objectKeys', () => {
        it('Should return all enumerable keys of an object', () => {
            const a = { foo: 1, bar: 2, foobar: 3 };
            expect(Helpers.objectKeys(a)).to.be.deep.equal([
                'foo',
                'bar',
                'foobar',
            ]);
        });
    });

    describe('levenshtein', () => {
        it('Should return the str2 length if str1 empty', () => {
            expect(Helpers.levenshtein('', 'foo')).to.be.equal(3);
        });

        it('Should return the str1 length if str2 empty', () => {
            expect(Helpers.levenshtein('bar!', '')).to.be.equal(4);
        });

        it('Should return the levenshtein value', () => {
            expect(Helpers.levenshtein('niche', 'chien')).to.be.equal(4);
            expect(Helpers.levenshtein('chat', 'chien')).to.be.equal(3);
            expect(Helpers.levenshtein('chat', 'poule')).to.be.equal(5);
            expect(Helpers.levenshtein('poue', 'poule')).to.be.equal(1);
        });
    });

    describe('strlenMaxLine', () => {
        it('length of "hello" is 5', () => {
            expect(Helpers.strlenMaxLine('hello')).to.equal(5);
        });

        it('length of "hi" is 2', () => {
            expect(Helpers.strlenMaxLine('hi')).to.equal(2);
        });

        it('length of "hello\\nhi\\nheynow" is 6', () => {
            expect(Helpers.strlenMaxLine('hello\nhi\nheynow')).to.equal(6);
        });

        it('length of "中文字符" is 8', () => {
            expect(Helpers.strlenMaxLine('中文字符')).to.equal(8);
        });

        it('length of "日本語の文字" is 12', () => {
            expect(Helpers.strlenMaxLine('日本語の文字')).to.equal(12);
        });

        it('length of "한글" is 4', () => {
            expect(Helpers.strlenMaxLine('한글')).to.equal(4);
        });
    });

    describe('wordWrap', () => {
        it('Should wrap a string', () => {
            const input = 'Hello, how are you today? I am fine, thank you!';
            const expected =
                'Hello, how\nare you\ntoday? I\nam fine,\nthank you!';

            expect(Helpers.wordWrap(10, input).join('\n')).to.equal(expected);
        });

        it('Should not create an empty last line', () => {
            const input = 'Hello Hello ';
            const expected = 'Hello\nHello';

            expect(Helpers.wordWrap(5, input).join('\n')).to.equal(expected);
        });

        it('Should handle formatting reset', () => {
            const input = '\x1b[31mHello\x1b[0m Hello ';
            const expected = '\x1b[31mHello\x1b[0m\nHello';

            expect(Helpers.wordWrap(5, input).join('\n')).to.equal(expected);
        });

        it('Should handle formatting reset (EMPTY version)', () => {
            const input = '\x1b[31mHello\x1b[m Hello ';
            const expected = '\x1b[31mHello\x1b[m\nHello';

            expect(Helpers.wordWrap(5, input).join('\n')).to.equal(expected);
        });

        it('Should not create extra newlines with words longer than limit', () => {
            const input =
                'disestablishment is a multiplicity someotherlongword';
            const expected =
                'disestablishment\nis a\nmultiplicity\nsomeotherlongword';

            expect(Helpers.wordWrap(7, input).join('\n')).to.equal(expected);
        });

        it('Should wreap multiple line input', () => {
            const input = 'a\nb\nc d e d b duck\nm\nn\nr';
            const expected = [
                'a',
                'b',
                'c d',
                'e d',
                'b',
                'duck',
                'm',
                'n',
                'r',
            ];

            expect(Helpers.wordWrap(4, input)).to.eql(expected);
        });

        it('Should not start a line with whitespace', () => {
            const input = 'ab cd  ef gh  ij kl';
            const expected = ['ab cd', 'ef gh', 'ij kl'];
            expect(Helpers.wordWrap(7, input)).to.eql(expected);
        });

        it('Should wrap CJK chars', () => {
            const input = '漢字 漢\n字 漢字';
            const expected = ['漢字 漢', '字 漢字'];
            expect(Helpers.wordWrap(7, input)).to.eql(expected);
        });

        it('Should wrap CJK chars with formatting', () => {
            const input = '\x1b[31m漢字\x1b[0m\n 漢字';
            const expected = ['\x1b[31m漢字\x1b[0m', ' 漢字'];
            expect(Helpers.wordWrap(5, input)).to.eql(expected);
        });
    });

    describe('truncateWidth', () => {
        it('Should return the string if length <= desiredLength', () => {
            expect(Helpers.truncateWidth('Hello', 5)).to.be.equal('Hello');
            expect(Helpers.truncateWidth('Hello', 7)).to.be.equal('Hello');
        });

        it('Should truncate the string if length >= desiredLength', () => {
            expect(Helpers.truncateWidth('Hello', 3)).to.be.equal('Hel');
            expect(Helpers.truncateWidth('Hello', 2)).to.be.equal('He');
        });
    });

    describe('truncate', () => {
        it('truncate("hello", 5) === "hello"', () => {
            expect(Helpers.truncate('hello', 5)).to.equal('hello');
        });

        it('truncate("hello sir", 7, "…") == "hello …"', () => {
            expect(Helpers.truncate('hello sir', 7, '…')).to.equal('hello …');
        });

        it('truncate("hello sir", 6, "…") == "hello…"', () => {
            expect(Helpers.truncate('hello sir', 6, '…')).to.equal('hello…');
        });

        it('truncate("goodnight moon", 8, "…") == "goodnig…"', () => {
            expect(Helpers.truncate('goodnight moon', 8, '…')).to.equal(
                'goodnig…'
            );
        });

        it('truncateWidth("漢字テスト", 15) === "漢字テスト"', () => {
            expect(Helpers.truncate('漢字テスト', 15)).to.equal('漢字テスト');
        });

        it('truncateWidth("漢字テスト", 6) === "漢字…"', () => {
            expect(Helpers.truncate('漢字テスト', 6)).to.equal('漢字…');
        });

        it('truncateWidth("漢字テスト", 5) === "漢字…"', () => {
            expect(Helpers.truncate('漢字テスト', 5)).to.equal('漢字…');
        });

        it('truncateWidth("漢字testてすと", 12) === "漢字testて…"', () => {
            expect(Helpers.truncate('漢字testてすと', 12)).to.equal(
                '漢字testて…'
            );
        });
    });

    describe('varExport', () => {
        it('Should return the value for litterals', () => {
            expect(Helpers.varExport(null)).to.be.equal('null');
            expect(Helpers.varExport('foobar')).to.be.equal("'foobar'");
            expect(Helpers.varExport(123)).to.be.equal('123');
            expect(Helpers.varExport(true)).to.be.equal('true');
            expect(Helpers.varExport(false)).to.be.equal('false');
            expect(Helpers.varExport(undefined)).to.be.equal('undefined');
        });

        it('Should return values of array', () => {
            expect(
                Helpers.varExport(['foo', 'bar', 42, [4, 5, null]])
            ).to.be.equal(
                [
                    '[',
                    "    'foo',",
                    "    'bar',",
                    '    42,',
                    '    [',
                    '        4,',
                    '        5,',
                    '        null,',
                    '    ],',
                    ']',
                ].join('\n')
            );
        });

        it('Should return values of object', () => {
            expect(
                Helpers.varExport({ foo: 'bar', bar: 42, foobar: [4, 5, null] })
            ).to.be.equal(
                [
                    'Object {',
                    "    foo: 'bar',",
                    '    bar: 42,',
                    '    foobar: [',
                    '        4,',
                    '        5,',
                    '        null,',
                    '    ],',
                    '}',
                ].join('\n')
            );
        });

        it('Should return values for named function', () => {
            function hello(who: string) {
                return who;
            }
            expect(Helpers.varExport(hello)).to.be.equal('[Function: hello]');
        });

        it('Should return values for anonymous function', () => {
            expect(
                Helpers.varExport((who: string) => {
                    return who;
                })
            ).to.be.equal('[Function (anonymous)]');
        });

        it('Should return values for instantiate object', () => {
            class MyObject {
                hello = true;

                private lol = 2;

                print() {
                    console.log(this.hello);
                    this.by();
                }

                private by() {
                    console.log(this.lol);
                }
            }
            expect(Helpers.varExport(new MyObject())).to.be.equal(
                ['MyObject {', '    hello: true,', '    lol: 2,', '}'].join(
                    '\n'
                )
            );
        });
    });

    describe('getAllMethods', () => {
        it('Should return all methods of an object', () => {
            class A {
                methodA() {
                    return this.methodC();
                }

                methodB() {}

                private methodC() {}

                static methodD() {}
            }

            expect(Helpers.getAllMethods(new A())).to.be.deep.equal([
                '__defineGetter__',
                '__defineSetter__',
                '__lookupGetter__',
                '__lookupSetter__',
                'constructor',
                'hasOwnProperty',
                'isPrototypeOf',
                'methodA',
                'methodB',
                'methodC',
                'propertyIsEnumerable',
                'toLocaleString',
                'toString',
                'valueOf',
            ]);
        });
    });

    describe('getAllStaticMethods', () => {
        it('Should return all methods of an object', () => {
            class A {
                methodA() {
                    return this.methodC();
                }

                methodB() {}

                private methodC() {}

                static methodD() {}
            }

            expect(Helpers.getAllStaticMethods(new A())).to.be.deep.equal([
                'methodD',
            ]);
        });
    });

    describe('arrayChunck', () => {
        it('Should chunck an array', () => {
            expect(Helpers.arrayChunk([1, 2, 3, 4], 2)).to.be.deep.equal([
                [1, 2],
                [3, 4],
            ]);
            expect(Helpers.arrayChunk([1, 2, 3, 4], 3)).to.be.deep.equal([
                [1, 2, 3],
                [4],
            ]);
            expect(Helpers.arrayChunk([1, 2, 3, 4], 4)).to.be.deep.equal([
                [1, 2, 3, 4],
            ]);
            expect(Helpers.arrayChunk([1, 2, 3, 4], 5)).to.be.deep.equal([
                [1, 2, 3, 4],
            ]);
            expect(Helpers.arrayChunk([1, 2, 3, 4], 1)).to.be.deep.equal([
                [1],
                [2],
                [3],
                [4],
            ]);
        });
    });

    describe('range', () => {
        it('Should return an array from start to and with step 1', () => {
            expect(Helpers.range(1, 5)).to.be.deep.equal([1, 2, 3, 4, 5]);
            expect(Helpers.range(-2, 2)).to.be.deep.equal([-2, -1, 0, 1, 2]);
        });

        it('Should use custom step', () => {
            expect(Helpers.range(1, 5, 2)).to.be.deep.equal([1, 3, 5]);
            expect(Helpers.range(-2, 2, 0.5)).to.be.deep.equal([
                -2, -1.5, -1, -0.5, 0, 0.5, 1, 1.5, 2,
            ]);
        });

        it('Should not return more than end value', () => {
            expect(Helpers.range(1, 6, 2)).to.be.deep.equal([1, 3, 5]);
        });

        it('Work also with string', () => {
            expect(Helpers.range('b', 'e')).to.be.deep.equal([
                'b',
                'c',
                'd',
                'e',
            ]);
            expect(Helpers.range('V', 'Y')).to.be.deep.equal([
                'V',
                'W',
                'X',
                'Y',
            ]);
            expect(Helpers.range('bb', 'erty')).to.be.deep.equal([
                'b',
                'c',
                'd',
                'e',
            ]);
            expect(Helpers.range('b', 'f', 2)).to.be.deep.equal([
                'b',
                'd',
                'f',
            ]);
        });
    });
});
