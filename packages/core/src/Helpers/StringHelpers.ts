/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export function hash(str: string) {
    let hashed = 0,
        chr;
    if (str.length === 0) return hashed;
    for (let i = 0; i < str.length; i++) {
        chr = str.charCodeAt(i);
        hashed = (hashed << 5) - hashed + chr;
        hashed |= 0; // Convert to 32bit integer
    }

    return hashed > -1 ? hashed : -hashed;
}

/**
 * Inspired by https://github.com/alexei/sprintf.js
 */
type ParseTree = {
    placeholder: string;
    paramNo: number;
    sign: string;
    padChar: string;
    align: string;
    width: number;
    precision: number;
    type: string;
};
const re = {
    notString: /[^s]/,
    notBool: /[^t]/,
    notType: /[^T]/,
    notPrimitive: /[^v]/,
    number: /[diefg]/,
    numericArg: /[bcdiefguxX]/,
    json: /[j]/,
    notJson: /[^j]/,
    text: /^[^\x25]+/,
    modulo: /^\x25{2}/,
    placeholder:
        /^\x25(?:([1-9]\d*)\$)?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-gijostTuvxX])/,
    key: /^([a-z_][a-z_\d]*)/i,
    indexAccess: /^\[(\d+)\]/,
    sign: /^[+-]/,
};
function sprintfFormat(parseTree: (string | ParseTree)[], argv: unknown[]) {
    const treeLength = parseTree.length;
    let cursor = 0,
        arg,
        output = '',
        i,
        ph,
        pad,
        padCharacter,
        padLength,
        isPositive,
        sign;
    for (i = 0; i < treeLength; i++) {
        if (typeof parseTree[i] === 'string') output += parseTree[i];
        else if (typeof parseTree[i] === 'object') {
            ph = parseTree[i] as ParseTree; // convenience purposes only
            if (ph.paramNo)
                // positional argument (explicit)
                arg = argv[ph.paramNo - 1];
            // positional argument (implicit)
            else arg = argv[cursor++];

            if (
                re.notType.test(ph.type) &&
                re.notPrimitive.test(ph.type) &&
                arg instanceof Function
            )
                arg = arg();

            if (
                re.numericArg.test(ph.type) &&
                typeof arg !== 'number' &&
                isNaN(arg)
            )
                throw new TypeError(
                    '[sprintf] expecting number but found ' + typeof arg
                );

            if (re.number.test(ph.type)) isPositive = arg >= 0;

            switch (ph.type) {
                case 'b':
                    arg = parseInt(arg, 10).toString(2);
                    break;
                case 'c':
                    arg = String.fromCharCode(parseInt(arg, 10));
                    break;
                case 'd':
                case 'i':
                    arg = parseInt(arg, 10);
                    break;
                case 'j':
                    arg = JSON.stringify(arg, null, ph.width ? ph.width : 0);
                    break;
                case 'e':
                    arg = ph.precision
                        ? parseFloat(arg).toExponential(ph.precision)
                        : parseFloat(arg).toExponential();
                    break;
                case 'f':
                    arg = ph.precision
                        ? parseFloat(arg).toFixed(ph.precision)
                        : parseFloat(arg);
                    break;
                case 'g':
                    arg = ph.precision
                        ? String(Number(arg.toPrecision(ph.precision)))
                        : parseFloat(arg);
                    break;
                case 'o':
                    arg = (parseInt(arg, 10) >>> 0).toString(8);
                    break;
                case 's':
                    arg = String(arg);
                    arg = ph.precision ? arg.substring(0, ph.precision) : arg;
                    break;
                case 't':
                    arg = String(!!arg);
                    arg = ph.precision ? arg.substring(0, ph.precision) : arg;
                    break;
                case 'T':
                    arg = Object.prototype.toString
                        .call(arg)
                        .slice(8, -1)
                        .toLowerCase();
                    arg = ph.precision ? arg.substring(0, ph.precision) : arg;
                    break;
                case 'u':
                    arg = parseInt(arg, 10) >>> 0;
                    break;
                case 'v':
                    arg = arg.valueOf();
                    arg = ph.precision ? arg.substring(0, ph.precision) : arg;
                    break;
                case 'x':
                    arg = (parseInt(arg, 10) >>> 0).toString(16);
                    break;
                case 'X':
                    arg = (parseInt(arg, 10) >>> 0).toString(16).toUpperCase();
                    break;
            }
            if (re.json.test(ph.type)) {
                output += arg;
            } else {
                if (re.number.test(ph.type) && (!isPositive || ph.sign)) {
                    sign = isPositive ? '+' : '-';
                    arg = arg.toString().replace(re.sign, '');
                } else {
                    sign = '';
                }
                padCharacter = ph.padChar
                    ? ph.padChar === '0'
                        ? '0'
                        : ph.padChar.charAt(1)
                    : ' ';
                padLength = ph.width - (sign + arg).length;
                pad = ph.width
                    ? padLength > 0
                        ? padCharacter.repeat(padLength)
                        : ''
                    : '';
                output += ph.align
                    ? sign + arg + pad
                    : padCharacter === '0'
                    ? sign + pad + arg
                    : pad + sign + arg;
            }
        }
    }
    return output;
}
const sprintfCache: Record<string, (string | ParseTree)[]> = {};
function sprintfParse(fmt: string) {
    if (fmt === '') return [''];
    else if (sprintfCache[fmt] !== undefined) return sprintfCache[fmt];

    let match;
    const parseTree: (string | ParseTree)[] = [];
    while (fmt) {
        if ((match = re.text.exec(fmt)) !== null) parseTree.push(match[0]);
        else if ((match = re.modulo.exec(fmt)) !== null) parseTree.push('%');
        else if ((match = re.placeholder.exec(fmt)) !== null) {
            parseTree.push({
                placeholder: match[0],
                paramNo: parseInt(match[1]),
                sign: match[2],
                padChar: match[3],
                align: match[4],
                width: parseInt(match[5]),
                precision: parseInt(match[6]),
                type: match[7],
            });
        } else throw new SyntaxError('[sprintf] unexpected placeholder');
        fmt = fmt.substring(match[0].length);
    }
    return (sprintfCache[fmt] = parseTree);
}
export function sprintf(format: string, ...values: unknown[]) {
    return sprintfFormat(sprintfParse(format), values);
}

export function stripTags(input: unknown, allowed?: string | string[]) {
    // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    if (Array.isArray(allowed))
        allowed = allowed.reduce(
            (accumulator, current) => accumulator + '<' + current + '>',
            ''
        );
    allowed = (
        ((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []
    ).join('');
    const tags = /<\/?([a-z0-9]*)\b[^>]*>?/gi;
    const commentsTags = /<!--[\s\S]*?-->/gi;
    let after = String(input);
    // removes tha '<' char at the end of the string to replicate PHP's behaviour
    after =
        after.substring(after.length - 1) === '<'
            ? after.substring(0, after.length - 1)
            : after;
    // recursively remove tags to ensure that the returned string doesn't contain forbidden tags after previous passes (e.g. '<<bait/>switch/>')
    // eslint-disable-next-line no-constant-condition
    while (true) {
        const before = after;
        after = before
            .replace(commentsTags, '')
            .replace(tags, (substring, replacer) => {
                return (allowed as string).indexOf(
                    '<' + replacer.toLowerCase() + '>'
                ) > -1
                    ? substring
                    : '';
            });
        // return once no more tags are removed
        if (before === after) return after;
    }
}
