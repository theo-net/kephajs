/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import * as RegExpHelpers from './RegExpHelpers';

describe('@kephajs/core/Helpers/RegExpHelpers', () => {
    describe('escape', () => {
        it('Should escape special chars', () => {
            expect(RegExpHelpers.escape('.*+?^${}()|[]\\]')).to.be.equal(
                '\\.\\*\\+\\?\\^\\$\\{\\}\\(\\)\\|\\[\\]\\\\\\]'
            );
        });
    });

    describe('str2Regex', () => {
        it('Should return a string if no regexp', () => {
            expect(RegExpHelpers.str2RegExp('test')).to.be.equal('test');
        });

        it('Should convert a string to a RegExp', () => {
            expect(RegExpHelpers.str2RegExp('/test/')).to.be.deep.equal(/test/);
            expect(RegExpHelpers.str2RegExp('/te.+t/')).to.be.deep.equal(
                /te.+t/
            );
        });

        it('Should keep flags', () => {
            expect(RegExpHelpers.str2RegExp('/te.+t/gi')).to.be.deep.equal(
                /te.+t/gi
            );
        });
    });
});
