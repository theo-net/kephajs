/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export function escape(input: string) {
    return input.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

export function str2RegExp(str: string) {
    const rule = /\/(.*)\/([igm]*)/;
    const match = (str + '').match(rule);
    return match ? new RegExp(match[1], match[2]) : str;
}
