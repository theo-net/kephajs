/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export function numberFormat(
    num: number,
    decimals = 0,
    decimalSeparator = '.',
    thousandsSeparator = ','
) {
    if (decimals < 0) decimals = 0;

    const formatter = new Intl.NumberFormat('en-EN', {
        style: 'decimal',
        minimumFractionDigits: decimals,
        maximumFractionDigits: decimals,
    });

    const splited = formatter.format(num).split('.');
    return (
        splited[0].split(',').join(thousandsSeparator) +
        (splited.length > 1 ? decimalSeparator + splited[1] : '')
    );
}
