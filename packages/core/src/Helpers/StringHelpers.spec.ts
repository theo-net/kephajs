/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import * as StringHelpers from './StringHelpers';

describe('@kephajs/core/Helpers/StringHelpers', () => {
    describe('hash', () => {
        it('Should return an hash', () => {
            expect(typeof StringHelpers.hash('foobar')).to.be.equal('number');
        });

        it('Should be always positive', () => {
            expect(StringHelpers.hash('foobar') > 0).to.be.equal(true);
        });

        it('Should return same hash for same string', () => {
            expect(StringHelpers.hash('foobar')).to.be.equal(
                StringHelpers.hash('foobar')
            );
        });

        it('Should return different hash for different string', () => {
            expect(StringHelpers.hash('foo')).to.be.not.equal(
                StringHelpers.hash('bar')
            );
        });
    });

    describe('sprintf', () => {
        const pi = 3.141592653589793;

        it('Should return empty string for empty format', () => {
            expect(StringHelpers.sprintf('')).to.be.equal('');
            expect(StringHelpers.sprintf('', 2)).to.be.equal('');
        });

        it('Should return formated strings for simple placeholders', () => {
            expect(StringHelpers.sprintf('%%')).to.be.equal('%');
            expect(StringHelpers.sprintf('%b', 2)).to.be.equal('10');
            expect(StringHelpers.sprintf('%c', 65)).to.be.equal('A');
            expect(StringHelpers.sprintf('%d', 2)).to.be.equal('2');
            expect(StringHelpers.sprintf('%i', 2)).to.be.equal('2');
            expect(StringHelpers.sprintf('%d', '2')).to.be.equal('2');
            expect(StringHelpers.sprintf('%i', '2')).to.be.equal('2');
            expect(StringHelpers.sprintf('%j', { foo: 'bar' })).to.be.equal(
                '{"foo":"bar"}'
            );
            expect(StringHelpers.sprintf('%j', ['foo', 'bar'])).to.be.equal(
                '["foo","bar"]'
            );
            expect(StringHelpers.sprintf('%e', 2)).to.be.equal('2e+0');
            expect(StringHelpers.sprintf('%u', 2)).to.be.equal('2');
            expect(StringHelpers.sprintf('%u', -2)).to.be.equal('4294967294');
            expect(StringHelpers.sprintf('%f', 2.2)).to.be.equal('2.2');
            expect(StringHelpers.sprintf('%g', pi)).to.be.equal(
                '3.141592653589793'
            );
            expect(StringHelpers.sprintf('%o', 8)).to.be.equal('10');
            expect(StringHelpers.sprintf('%o', -8)).to.be.equal('37777777770');
            expect(StringHelpers.sprintf('%s', '%s')).to.be.equal('%s');
            expect(StringHelpers.sprintf('%x', 255)).to.be.equal('ff');
            expect(StringHelpers.sprintf('%x', -255)).to.be.equal('ffffff01');
            expect(StringHelpers.sprintf('%X', 255)).to.be.equal('FF');
            expect(StringHelpers.sprintf('%X', -255)).to.be.equal('FFFFFF01');
            expect(
                StringHelpers.sprintf(
                    '%2$s %3$s a %1$s',
                    'cracker',
                    'Polly',
                    'wants'
                )
            ).to.be.equal('Polly wants a cracker');
            expect(StringHelpers.sprintf('%t', true)).to.be.equal('true');
            expect(StringHelpers.sprintf('%.1t', true)).to.be.equal('t');
            expect(StringHelpers.sprintf('%t', 'true')).to.be.equal('true');
            expect(StringHelpers.sprintf('%t', 1)).to.be.equal('true');
            expect(StringHelpers.sprintf('%t', false)).to.be.equal('false');
            expect(StringHelpers.sprintf('%.1t', false)).to.be.equal('f');
            expect(StringHelpers.sprintf('%t', '')).to.be.equal('false');
            expect(StringHelpers.sprintf('%t', 0)).to.be.equal('false');

            expect(StringHelpers.sprintf('%T', undefined)).to.be.equal(
                'undefined'
            );
            expect(StringHelpers.sprintf('%T', null)).to.be.equal('null');
            expect(StringHelpers.sprintf('%T', true)).to.be.equal('boolean');
            expect(StringHelpers.sprintf('%T', 42)).to.be.equal('number');
            expect(StringHelpers.sprintf('%T', 'This is a string')).to.be.equal(
                'string'
            );
            expect(StringHelpers.sprintf('%T', Math.log)).to.be.equal(
                'function'
            );
            expect(StringHelpers.sprintf('%T', [1, 2, 3])).to.be.equal('array');
            expect(StringHelpers.sprintf('%T', { foo: 'bar' })).to.be.equal(
                'object'
            );
            expect(
                StringHelpers.sprintf('%T', /<('[^']*'|'[^']*'|[^''>])*>/)
            ).to.be.equal('regexp');

            expect(StringHelpers.sprintf('%v', true)).to.be.equal('true');
            expect(StringHelpers.sprintf('%v', 42)).to.be.equal('42');
            expect(StringHelpers.sprintf('%v', 'This is a string')).to.be.equal(
                'This is a string'
            );
            expect(StringHelpers.sprintf('%v', [1, 2, 3])).to.be.equal('1,2,3');
            expect(StringHelpers.sprintf('%v', { foo: 'bar' })).to.be.equal(
                '[object Object]'
            );
            expect(
                StringHelpers.sprintf('%v', /<("[^"]*"|'[^']*'|[^'">])*>/)
            ).to.be.equal('/<("[^"]*"|\'[^\']*\'|[^\'">])*>/');
        });

        it('Should return formated strings for complex placeholders', () => {
            // sign
            expect(StringHelpers.sprintf('%d', 2)).to.be.equal('2');
            expect(StringHelpers.sprintf('%d', -2)).to.be.equal('-2');
            expect(StringHelpers.sprintf('%+d', 2)).to.be.equal('+2');
            expect(StringHelpers.sprintf('%+d', -2)).to.be.equal('-2');
            expect(StringHelpers.sprintf('%i', 2)).to.be.equal('2');
            expect(StringHelpers.sprintf('%i', -2)).to.be.equal('-2');
            expect(StringHelpers.sprintf('%+i', 2)).to.be.equal('+2');
            expect(StringHelpers.sprintf('%+i', -2)).to.be.equal('-2');
            expect(StringHelpers.sprintf('%f', 2.2)).to.be.equal('2.2');
            expect(StringHelpers.sprintf('%f', -2.2)).to.be.equal('-2.2');
            expect(StringHelpers.sprintf('%+f', 2.2)).to.be.equal('+2.2');
            expect(StringHelpers.sprintf('%+f', -2.2)).to.be.equal('-2.2');
            expect(StringHelpers.sprintf('%+.1f', -2.34)).to.be.equal('-2.3');
            expect(StringHelpers.sprintf('%+.1f', -0.01)).to.be.equal('-0.0');
            expect(StringHelpers.sprintf('%.6g', pi)).to.be.equal('3.14159');
            expect(StringHelpers.sprintf('%.3g', pi)).to.be.equal('3.14');
            expect(StringHelpers.sprintf('%.1g', pi)).to.be.equal('3');
            expect(StringHelpers.sprintf('%+010d', -123)).to.be.equal(
                '-000000123'
            );
            expect(StringHelpers.sprintf("%+'_10d", -123)).to.be.equal(
                '______-123'
            );
            expect(StringHelpers.sprintf('%f %f', -234.34, 123.2)).to.be.equal(
                '-234.34 123.2'
            );

            // padding
            expect(StringHelpers.sprintf('%05d', -2)).to.be.equal('-0002');
            expect(StringHelpers.sprintf('%05i', -2)).to.be.equal('-0002');
            expect(StringHelpers.sprintf('%5s', '<')).to.be.equal('    <');
            expect(StringHelpers.sprintf('%05s', '<')).to.be.equal('0000<');
            expect(StringHelpers.sprintf("%'_5s", '<')).to.be.equal('____<');
            expect(StringHelpers.sprintf('%-5s', '>')).to.be.equal('>    ');
            expect(StringHelpers.sprintf('%0-5s', '>')).to.be.equal('>0000');
            expect(StringHelpers.sprintf("%'_-5s", '>')).to.be.equal('>____');
            expect(StringHelpers.sprintf('%5s', 'xxxxxx')).to.be.equal(
                'xxxxxx'
            );
            expect(StringHelpers.sprintf('%02u', 1234)).to.be.equal('1234');
            expect(StringHelpers.sprintf('%8.3f', -10.23456)).to.be.equal(
                ' -10.235'
            );
            expect(StringHelpers.sprintf('%f %s', -12.34, 'xxx')).to.be.equal(
                '-12.34 xxx'
            );
            expect(StringHelpers.sprintf('%2j', { foo: 'bar' })).to.be.equal(
                '{\n  "foo": "bar"\n}'
            );
            expect(StringHelpers.sprintf('%2j', ['foo', 'bar'])).to.be.equal(
                '[\n  "foo",\n  "bar"\n]'
            );

            // precision
            expect(StringHelpers.sprintf('%.1f', 2.345)).to.be.equal('2.3');
            expect(StringHelpers.sprintf('%5.5s', 'xxxxxx')).to.be.equal(
                'xxxxx'
            );
            expect(StringHelpers.sprintf('%5.1s', 'xxxxxx')).to.be.equal(
                '    x'
            );
        });

        it('Should format with argnum', () => {
            expect(
                StringHelpers.sprintf(
                    'The %2$s contains %1$d monkeys',
                    3,
                    'zoo'
                )
            ).to.be.equal('The zoo contains 3 monkeys');
        });

        it('Should return formated strings for callbacks', () => {
            expect(
                StringHelpers.sprintf('%s', function () {
                    return 'foobar';
                })
            ).to.be.equal('foobar');
        });

        function shouldThrow(
            format: string,
            args: unknown[],
            err: typeof Error
        ) {
            try {
                StringHelpers.sprintf(format, ...args);
            } catch (error) {
                expect(error).to.be.instanceOf(err);
                return;
            }
            expect.fail('Should have thrown');
        }

        function shouldNotThrow(format: string, args: unknown[]) {
            StringHelpers.sprintf(format, ...args);
            expect(true).to.be.equal(true);
        }

        it('Should not throw Error (cache consistency)', () => {
            // redefine object properties to ensure that is not affect to the cache
            StringHelpers.sprintf('hasOwnProperty');
            StringHelpers.sprintf('constructor');
            shouldNotThrow('%s', ['caching...']);
            shouldNotThrow('%s', ['crash?']);
        });

        it('Should throw SyntaxError for placeholders', () => {
            shouldThrow('%', [], SyntaxError);
            shouldThrow('%A', [], SyntaxError);
            shouldThrow('%s%', [], SyntaxError);
            shouldThrow('%(s', [], SyntaxError);
            shouldThrow('%)s', [], SyntaxError);
            shouldThrow('%$s', [], SyntaxError);
            shouldThrow('%()s', [], SyntaxError);
            shouldThrow('%(12)s', [], SyntaxError);
            shouldThrow('%(hello)s', [], SyntaxError);
        });

        const numeric = 'bcdiefguxX'.split('');
        numeric.forEach(specifier => {
            const fmt = StringHelpers.sprintf('%%%s', specifier);
            it(fmt + ' should throw TypeError for invalid numbers', () => {
                shouldThrow(fmt, [], TypeError);
                shouldThrow(fmt, ['str'], TypeError);
                shouldThrow(fmt, [{}], TypeError);
                shouldThrow(fmt, ['s'], TypeError);
            });

            it(
                fmt +
                    ' should not throw TypeError for something implicitly castable to number',
                () => {
                    shouldNotThrow(fmt, [1 / 0]);
                    shouldNotThrow(fmt, [true]);
                    shouldNotThrow(fmt, [[1]]);
                    shouldNotThrow(fmt, ['200']);
                    shouldNotThrow(fmt, [null]);
                }
            );
        });
    });

    describe('stripTags', () => {
        it('Should strip all tags and comments', () => {
            expect(
                StringHelpers.stripTags(
                    '<p>Test paragraph.\n</p><!-- Comment --> <a href="#fragment">Other text</a>'
                )
            ).to.be.equal('Test paragraph.\n Other text');
            expect(
                StringHelpers.stripTags(
                    '<P>Test paragraph.\n</P><!-- Comment --> <A href="#fragment">Other text</A>'
                )
            ).to.be.equal('Test paragraph.\n Other text');
            expect(StringHelpers.stripTags('1 < 5 5 > 1')).to.be.be.equal(
                '1 < 5 5 > 1'
            );
            expect(StringHelpers.stripTags('1 <br/> 1')).to.be.be.equal('1  1');
            expect(
                StringHelpers.stripTags(
                    '<i>hello</i> <<foo>script>world<</foo>/script>'
                )
            ).to.be.be.equal('hello world');
            expect(StringHelpers.stripTags(4)).to.be.be.equal('4');
        });

        it('Should keep allowed tags', () => {
            expect(
                StringHelpers.stripTags(
                    '<p>Hello</p> <br /><b>world</b> <i>!</i>',
                    '<i><b>'
                )
            ).to.be.equal('Hello <b>world</b> <i>!</i>');
            expect(
                StringHelpers.stripTags(
                    '<P>Hello</P> <BR /><B>world</B> <I>!</I>',
                    '<i><b>'
                )
            ).to.be.equal('Hello <B>world</B> <I>!</I>');
            expect(
                StringHelpers.stripTags(
                    '<p>Hello <img src="someimage.png" onmouseover="someFunction()">the <i>world</i></p>',
                    '<p>'
                )
            ).to.be.be.equal('<p>Hello the world</p>');
            expect(
                StringHelpers.stripTags(
                    "<a href='https://theo-net.org'>A website</a>",
                    '<a>'
                )
            ).to.be.be.equal("<a href='https://theo-net.org'>A website</a>");
            expect(StringHelpers.stripTags('1 <br/> 1', '<br>')).to.be.be.equal(
                '1 <br/> 1'
            );
            expect(
                StringHelpers.stripTags('1 <br/> 1', '<br><br/>')
            ).to.be.be.equal('1 <br/> 1');
        });

        it('Should keep allowed tags (array)', () => {
            expect(
                StringHelpers.stripTags(
                    '<p>Hello</p> <br /><b>world</b> <i>!</i>',
                    ['i', 'b']
                )
            ).to.be.equal('Hello <b>world</b> <i>!</i>');
            expect(
                StringHelpers.stripTags(
                    '<P>Hello</P> <BR /><B>world</B> <I>!</I>',
                    ['i', 'b']
                )
            ).to.be.equal('Hello <B>world</B> <I>!</I>');
            expect(
                StringHelpers.stripTags(
                    '<p>Hello <img src="someimage.png" onmouseover="someFunction()">the <i>world</i></p>',
                    ['p']
                )
            ).to.be.be.equal('<p>Hello the world</p>');
            expect(
                StringHelpers.stripTags(
                    "<a href='https://theo-net.org'>A website</a>",
                    ['a']
                )
            ).to.be.be.equal("<a href='https://theo-net.org'>A website</a>");
            expect(StringHelpers.stripTags('1 <br/> 1', ['br'])).to.be.be.equal(
                '1 <br/> 1'
            );
            expect(
                StringHelpers.stripTags('1 <br/> 1', ['br', 'br'])
            ).to.be.be.equal('1 <br/> 1');
        });
    });
});
