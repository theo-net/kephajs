/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export { SemVer } from '../SemVer/SemVer';

/**
 * Convert `string[]` to an enum
 */
export function arrayToEnum<T extends string, U extends [T, ...T[]]>(
    items: U
): { [k in U[number]]: k } {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const obj: any = {};
    for (const item of items) {
        obj[item] = item;
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return obj as any;
}

/**
 * Convert `string` to camelCase
 * @author Lodash
 */
export function camelCase(string: string): string {
    // String need to be a string...
    string = string === null ? '' : string + '';

    // Convert latin-1 char to classic latin and remove diacric char
    const reComboMark = /[\u0300-\u036f\ufe20-\ufe23]/g;
    const reLatin1 = /[\xc0-\xd6\xd8-\xde\xdf-\xf6\xf8-\xff]/g;

    function deburrLetter(letter: string): string {
        // map latin-1 extended char to regular char
        const deburredLetters: { [key: string]: string } = {
            '\xc0': 'A',
            '\xc1': 'A',
            '\xc2': 'A',
            '\xc3': 'A',
            '\xc4': 'A',
            '\xc5': 'A',
            '\xe0': 'a',
            '\xe1': 'a',
            '\xe2': 'a',
            '\xe3': 'a',
            '\xe4': 'a',
            '\xe5': 'a',
            '\xc7': 'C',
            '\xe7': 'c',
            '\xd0': 'D',
            '\xf0': 'd',
            '\xc8': 'E',
            '\xc9': 'E',
            '\xca': 'E',
            '\xcb': 'E',
            '\xe8': 'e',
            '\xe9': 'e',
            '\xea': 'e',
            '\xeb': 'e',
            '\xcC': 'I',
            '\xcd': 'I',
            '\xce': 'I',
            '\xcf': 'I',
            '\xeC': 'i',
            '\xed': 'i',
            '\xee': 'i',
            '\xef': 'i',
            '\xd1': 'N',
            '\xf1': 'n',
            '\xd2': 'O',
            '\xd3': 'O',
            '\xd4': 'O',
            '\xd5': 'O',
            '\xd6': 'O',
            '\xd8': 'O',
            '\xf2': 'o',
            '\xf3': 'o',
            '\xf4': 'o',
            '\xf5': 'o',
            '\xf6': 'o',
            '\xf8': 'o',
            '\xd9': 'U',
            '\xda': 'U',
            '\xdb': 'U',
            '\xdc': 'U',
            '\xf9': 'u',
            '\xfa': 'u',
            '\xfb': 'u',
            '\xfc': 'u',
            '\xdd': 'Y',
            '\xfd': 'y',
            '\xff': 'y',
            '\xc6': 'Ae',
            '\xe6': 'ae',
            '\xde': 'Th',
            '\xfe': 'th',
            '\xdf': 'ss',
        };

        return deburredLetters[letter];
    }
    string = string.replace(reLatin1, deburrLetter).replace(reComboMark, '');

    // create word array
    const reWords = (function (): RegExp {
        const upper = '[A-Z\\xc0-\\xd6\\xd8-\\xde]',
            lower = '[a-z\\xdf-\\xf6\\xf8-\\xff]+';
        return new RegExp(
            upper +
                '+(?=' +
                upper +
                lower +
                ')|' +
                upper +
                '?' +
                lower +
                '|' +
                upper +
                '+|[0-9]+',
            'g'
        );
    })();
    const array = string.match(reWords) || [];

    let result = '';
    array.forEach(function (word, index) {
        word = word.toLowerCase();
        result += index ? word.charAt(0).toUpperCase() + word.slice(1) : word;
    });

    return result;
}

/**
 * Join the value of an array to a string
 *
 *      joinValues([1, 'foo', 'bar']) // => "1 | 'foo' | 'bar'"
 *
 * @param separator The separator for the joined values
 * @param escapeString The escape character for string values
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function joinValues<T extends any[]>(
    array: T,
    separator = ' | ',
    escapeString = "'"
): string {
    return array
        .map(val =>
            typeof val === 'string'
                ? `${escapeString}${val}${escapeString}`
                : val
        )
        .join(separator);
}

/**
 * Returns an array of a given object's own enumerable properties names,
 * iterated in the same order that a normal loop would.
 */
export const objectKeys: ObjectConstructor['keys'] =
    typeof Object.keys === 'function'
        ? // eslint-disable-next-line @typescript-eslint/no-explicit-any
          (obj: any) => Object.keys(obj)
        : // eslint-disable-next-line @typescript-eslint/no-explicit-any
          (object: any) => {
              const keys: string[] = [];
              for (const key in object) {
                  if (Object.prototype.hasOwnProperty.call(object, key)) {
                      keys.push(key);
                  }
              }
              return keys;
          };

/**
 * Return the Levenshtein distance between two string
 */
export function levenshtein(str1: string, str2: string) {
    const str1Len = str1.length,
        str2Len = str2.length;

    // basics cases
    if (str1Len === 0) return str2Len;
    if (str2Len === 0) return str1Len;

    // init values
    const prevRow: number[] = [],
        str2Char: number[] = [];
    let curCol, i, j, tmp, strCmp;
    let nextCol = 0;

    // init of prevRow
    for (i = 0; i < str2Len; i++) {
        prevRow[i] = i;
        str2Char[i] = str2.charCodeAt(i);
    }
    prevRow[str2Len] = str2Len;

    // calcul the distance between the current line with the previous
    for (i = 0; i < str1Len; i++) {
        nextCol = i + 1;

        for (j = 0; j < str2Len; j++) {
            curCol = nextCol;

            // substitution
            strCmp = str1.charCodeAt(i) === str2Char[j];
            nextCol = prevRow[j] + (strCmp ? 0 : 1);

            // insertion
            tmp = curCol + 1;
            if (nextCol > tmp) nextCol = tmp;

            // deletion
            tmp = prevRow[j + 1] + 1;
            if (nextCol > tmp) nextCol = tmp;

            // preparation for the next iteration
            prevRow[j] = curCol;
        }

        // preparation for the next iteration
        prevRow[j] = nextCol;
    }

    return nextCol;
}

export function strlenMaxLine(str: string) {
    const split = str.split('\n');

    function stringWidth(s: string): number {
        let width = 0;

        for (let i = 0; i < s.length; i++) {
            const code = s.charCodeAt(i);
            if (code >= 0xd800 && code <= 0xdbff && s.length > i + 1) {
                const second = s.charCodeAt(i + 1);
                if (second >= 0xdc00 && second <= 0xdfff)
                    return (code - 0xd800) * 0x400 + second - 0xdc00 + 0x10000;
            }

            // ignore control characters
            if (code <= 0x1f || (code >= 0x7f && code <= 0x9f)) continue;

            // is fullwidth code point
            if (
                code >= 0x1100 &&
                (code <= 0x115f || // Hangul Jamo
                    0x2329 === code || // LEFT-POINTING ANGLE BRACKET
                    0x232a === code || // RIGHT-POINTING ANGLE BRACKET
                    // CJK Radicals Supplement .. Enclosed CJK Letters and Months
                    (0x2e80 <= code && code <= 0x3247 && code !== 0x303f) ||
                    // Enclosed CJK Letters and Months .. CJK Unified Ideographs Ext A
                    (0x3250 <= code && code <= 0x4dbf) ||
                    // CJK Unified Ideographs .. Yi Radicals
                    (0x4e00 <= code && code <= 0xa4c6) ||
                    // Hangul Jamo Extended-A
                    (0xa960 <= code && code <= 0xa97c) ||
                    // Hangul Syllables
                    (0xac00 <= code && code <= 0xd7a3) ||
                    // CJK Compatibility Ideographs
                    (0xf900 <= code && code <= 0xfaff) ||
                    // Vertical Forms
                    (0xfe10 <= code && code <= 0xfe19) ||
                    // CJK Compatibility Forms .. Small Form Variants
                    (0xfe30 <= code && code <= 0xfe6b) ||
                    // Halfwidth and Fullwidth Forms
                    (0xff01 <= code && code <= 0xff60) ||
                    (0xffe0 <= code && code <= 0xffe6) ||
                    // Kana Supplement
                    (0x1b000 <= code && code <= 0x1b001) ||
                    // Enclosed Ideographic Supplement
                    (0x1f200 <= code && code <= 0x1f251) ||
                    // CJK Unified Ideographs Extension B .. Tertiary Ideographic Plane
                    (0x20000 <= code && code <= 0x3fffd))
            )
                width += 2;
            else width++;
        }

        return width;
    }

    return split.reduce((memo, s) => {
        return stringWidth(s) > memo ? stringWidth(s) : memo;
    }, 0);
}

export function wordWrap(
    maxLength: number,
    inputStr: string,
    strlen = strlenMaxLine
) {
    const output: string[] = [];
    const input = inputStr.split('\n');

    input.forEach(inputLine => {
        const lines: string[] = [];
        const split = inputLine.split(/(\s+)/g);
        let line: string[] = [];
        let lineLength = 0;
        let whitespace;

        // Each word (each time we have: one word and one or many blank char
        for (let k = 0; k < split.length; k += 2) {
            const word = split[k];
            let newLength = lineLength + strlen(word);

            // Blank char before the word
            if (lineLength > 0 && whitespace) newLength += whitespace.length;

            // Wrap line
            if (newLength > maxLength) {
                if (lineLength !== 0) lines.push(line.join(''));
                line = [word];
                lineLength = strlen(word);
            } else {
                line.push(whitespace || '', word);
                lineLength = newLength;
            }

            // Save whitespace
            whitespace = split[k + 1];
        }

        if (lineLength) lines.push(line.join(''));

        output.push(...lines);
    });

    return output;
}

export function truncateWidth(
    inputStr: string,
    desiredLength: number,
    strlen = strlenMaxLine
) {
    if (inputStr.length === strlenMaxLine(inputStr))
        return inputStr.substring(0, desiredLength);

    while (strlen(inputStr) > desiredLength) inputStr = inputStr.slice(0, -1);

    return inputStr;
}

export function truncate(
    inputStr: string,
    desiredLength: number,
    truncateChar = '…',
    strlen = strlenMaxLine,
    truncateFct = truncateWidth
) {
    const lengthOfStr = strlen(inputStr);
    if (lengthOfStr <= desiredLength) return inputStr;

    desiredLength -= strlen(truncateChar);

    return truncateFct(inputStr, desiredLength, strlen) + truncateChar;
}

export function varExport(value: unknown, indentation = 0) {
    const getType = function (inp: unknown) {
        if (inp === null) return 'null';
        if (inp === undefined) return 'undefined';
        let type: string = typeof inp;
        if (type === 'function') return 'function';
        if (type === 'object') {
            if (!inp.constructor) return 'object';

            let cons = inp.constructor.toString();
            const match = cons.match(/(\w+)\(/);
            if (match) cons = match[1].toLowerCase();
            if (['boolean', 'number', 'string', 'array'].indexOf(cons) > -1)
                type = cons;
        }
        return type;
    };

    let retstr = '';
    const type = getType(value);
    if (type === 'null') retstr = type;
    else if (type === 'array') {
        const x: string[] = [];
        (value as unknown[]).forEach(subValue => {
            x.push(
                ' '.repeat(indentation + 4) +
                    varExport(subValue, indentation + 4) +
                    ','
            );
        });
        retstr = '[\n' + x.join('\n') + '\n' + ' '.repeat(indentation) + ']';
    } else if (type === 'object') {
        const x: string[] = [];
        objectKeys(value as Record<string, unknown>).forEach(key => {
            x.push(
                ' '.repeat(indentation + 4) +
                    key +
                    ': ' +
                    varExport(
                        (value as Record<string, unknown>)[key],
                        indentation + 4
                    ) +
                    ','
            );
        });
        retstr =
            (typeof value?.constructor?.name === 'string'
                ? value?.constructor?.name
                : 'Object') +
            ' {\n' +
            x.join('\n') +
            '\n' +
            ' '.repeat(indentation) +
            '}';
    } else if (type === 'function') {
        const funcParts = (value as () => void)
            .toString()
            .match(/^(?:function (.+))?\(.*\)/) as RegExpMatchArray;
        retstr =
            '[Function' +
            (funcParts[1] ? ': ' + funcParts[1] : ' (anonymous)') +
            ']';
    } else if (['number', 'boolean', 'undefined'].indexOf(type) > -1)
        retstr = value + '';
    else {
        retstr =
            typeof value !== 'string'
                ? value + ' '
                : "'" +
                  value.replace(/(["'])/g, '\\$1').replace(/\0/g, '\\0') +
                  "'";
    }

    return retstr;
}

export function getAllMethods(toCheck: object) {
    const props: string[] = [];
    let obj = toCheck;

    do {
        props.push(...Object.getOwnPropertyNames(obj));
    } while ((obj = Object.getPrototypeOf(obj)));
    props.push(...Object.getOwnPropertyNames(toCheck.constructor));

    return props.sort().filter((e, i, arr) => {
        if (
            e != arr[i + 1] &&
            typeof (toCheck as Record<string, unknown>)[e] === 'function'
        )
            return true;
        return false;
    });
}

export function getAllStaticMethods(toCheck: object) {
    const statics: string[] = [];

    // static methods
    if (toCheck.constructor !== undefined)
        statics.push(...Object.getOwnPropertyNames(toCheck.constructor));
    return statics.sort().filter((method, i, arr) => {
        return (
            method != arr[i + 1] &&
            typeof (toCheck.constructor as unknown as Record<string, unknown>)[
                method
            ] === 'function'
        );
    });
}

export function arrayChunk<T>(items: T[], length: number) {
    const final: T[][] = [];
    let index = 0;
    let current = 0;
    items.forEach(item => {
        if (current === length) {
            index++;
            final[index] = [];
            current = 0;
        } else if (index === 0 && final[index] === undefined) final[index] = [];
        final[index].push(item);
        current++;
    });

    return final;
}

export function range<T extends number | string>(
    start: T,
    end: T,
    step = 1
): T[] {
    const ans: T[] = [];
    const isString = typeof start === 'string' || typeof end === 'string';

    if (isString)
        for (
            let i = (start as string).charCodeAt(0);
            i <= (end as string).charCodeAt(0);
            i += step
        )
            ans.push(String.fromCharCode(i as number) as T);
    else for (let i = start as number; i <= end; i += step) ans.push(i as T);
    return ans;
}
