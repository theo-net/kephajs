/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Profiler } from './Profiler';

describe('@kephajs/core/Profiler', () => {
    describe('register', () => {
        it('Should register an element with the current timestamp', () => {
            const profiler = new Profiler();
            profiler.register('foobar');
            expect(profiler.getAll()[0].description).to.be.equal('foobar');
            expect(
                Date.now() - profiler.getAll()[0].timestamp < 10
            ).to.be.equal(true);
        });

        it('Should register an element with a custom timestamp', () => {
            const profiler = new Profiler();
            profiler.register('foobar', 123);
            expect(profiler.getAll()[0].description).to.be.equal('foobar');
            expect(profiler.getAll()[0].timestamp).to.be.equal(123);
        });

        it('Should set duration to 0 for the first element', () => {
            const profiler = new Profiler();
            profiler.register('foobar');
            expect(profiler.getAll()[0].duration).to.be.equal(0);
        });

        it('Should calculate the duration for the other elements', async () => {
            const profiler = new Profiler();
            profiler.register('foo');
            function delay(time: number) {
                return new Promise(resolve => setTimeout(resolve, time));
            }
            await delay(5);
            profiler.register('bar');
            expect(profiler.getAll()[1].duration > 0).to.be.equal(true);
        });
    });

    describe('getAll', () => {
        it('Should return an array with all registered timestamp', () => {
            const profiler = new Profiler();
            profiler.register('first', 123);
            profiler.register('second', 456);
            profiler.register('third', 789);
            expect(profiler.getAll()).to.be.deep.equal([
                { description: 'first', timestamp: 123, duration: 0 },
                { description: 'second', timestamp: 456, duration: 456 - 123 },
                { description: 'third', timestamp: 789, duration: 789 - 456 },
            ]);
        });
    });

    describe('getElement', () => {
        it('Should return a registered element', () => {
            const profiler = new Profiler();
            profiler.register('foobar', 123);
            expect(profiler.getElement('foobar')).to.be.deep.equal({
                description: 'foobar',
                timestamp: 123,
                duration: 0,
            });
        });

        it("Should return undefined if the element don't exist", () => {
            const profiler = new Profiler();
            profiler.register('foobar', 123);
            expect(profiler.getElement('FOOBAR')).to.be.equal(undefined);
        });

        it('Should return the last registered element', () => {
            const profiler = new Profiler();
            profiler.register('foobar', 123);
            profiler.register('foobar', 456);
            expect(profiler.getElement('foobar')).to.be.deep.equal({
                description: 'foobar',
                timestamp: 456,
                duration: 456 - 123,
            });
        });
    });
});
