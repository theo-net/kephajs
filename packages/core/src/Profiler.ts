/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export type ProfilerElement = {
    timestamp: number;
    description: string;
    duration: number;
};

export class Profiler {
    protected _elements: ProfilerElement[] = [];

    register(description: string, timestamp = Date.now()) {
        let duration = 0;
        if (this._elements.length > 0)
            duration =
                timestamp - this._elements[this._elements.length - 1].timestamp;

        this._elements.push({ description, timestamp, duration });
    }

    getAll() {
        return this._elements;
    }

    getElement(description: string) {
        let result: ProfilerElement | undefined;
        this._elements.forEach(element => {
            if (element.description === description) result = element;
        });

        return result;
    }
}
