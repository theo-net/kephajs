/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Application } from './Application';

export interface PluginInterface {
    boot?(app?: Application): Promise<void>;
    ready?(): Promise<void>;
}
