/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ApplicationOptions } from './ApplicationOptions';
import { ApplicationRegisteredServices } from './ApplicationRegisteredServices';
import { Config } from './Config/Config';
import { ConfigType } from './Config/ConfigType';
import { Logger } from './Logger/Logger';
import { PluginInterface } from './Plugin';
import { Profiler } from './Profiler';
import { SemVer } from './SemVer/SemVer';
import { ServiceContainer } from './ServiceContainer/ServiceContainer';
import { Validator } from './Validate/Validator';
import { version as kephajsVersion } from './version';

interface ApplicationInjector extends ServiceContainer {
    get<Token extends keyof TContext, TContext = ApplicationRegisteredServices>(
        token: Token
    ): TContext[Token];
}

export type TEnvironment = 'development' | 'staging' | 'production' | 'testing';

/**
 * The base for all application
 */
export class Application {
    // The root directory of the application
    public readonly appRoot: string;

    // The app config
    public readonly config: Config<ConfigType>;

    // The service container
    public container: ApplicationInjector;

    // The app description
    public readonly description: string | undefined;

    // The version of KephaJs/Core
    public readonly kephajsVersion: SemVer;

    // The app name
    public readonly name: string | undefined;

    public readonly profiler = new Profiler();

    // The validator service
    public readonly validator: Validator;

    // The app version
    public readonly version: SemVer;

    // Actual environment of the application
    protected _environment: TEnvironment = 'production';

    // `true` if we are in development environment
    protected _isDev = false;

    // `false` if wa are in production environment
    protected _isProduction = true;

    // Options passed to the constructor
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    protected readonly _optionsRaw: ApplicationOptions = {};

    protected _plugins: PluginInterface[] = [];

    protected _state:
        | 'unknown'
        | 'initiated'
        | 'setup'
        | 'registered'
        | 'booted'
        | 'ready'
        | 'shutdown' = 'unknown';

    protected _registeredShutdownFunctions: (() => Promise<void>)[] = [];

    /**
     * Initialize the application and register :
     *  - the `appRoot`
     *  - in wich environment we are.
     *
     * List of the options:
     *
     *  - `environment` the current environment (`development`, `staging`, `production` or `testing`)
     *  - `version` the application version
     *  - `validator` the class to instanciate for the validator service (as you can extend it before injection)
     *
     * @param appRoot The application root
     * @param options Can a string who define the environment (can be : `development`, `staging`, `production`
     *                or `testing`) or a list option.
     */
    constructor(
        appRoot: string,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        options: TEnvironment | ApplicationOptions = 'production'
    ) {
        this.profiler.register('begin', Date.now());

        this.appRoot = appRoot;
        this.kephajsVersion = new SemVer(kephajsVersion);

        this._stateUnknown();
        this._state = 'initiated';
        this.profiler.register('app state initiated');

        // Define the environment and register the raw options
        let environment = 'production';
        if (typeof options === 'string') {
            environment = options;
        } else {
            this._optionsRaw = options;
            if (
                options.environment &&
                typeof options.environment === 'string'
            ) {
                environment = options.environment;
            }
        }
        if (
            !['development', 'staging', 'production', 'testing'].includes(
                environment
            )
        ) {
            throw new TypeError(
                "environment need to be 'development', 'staging', 'production' or 'testing'"
            );
        }
        this._setEnvironment(environment as TEnvironment);

        // Logger service
        const logger = new Logger(this._environment);
        if (this._optionsRaw._savetoLaterLogs !== undefined) {
            logger.setSaveToLater(this._optionsRaw._savetoLaterLogs);
        }
        logger.info('Environment is ' + this.environment);

        // Config service
        this.config = new Config<ConfigType>();
        this.config.setReadonly('environment', this.environment);

        // Validator service
        this.validator = new Validator();

        // Initialize the service container
        this.container = new ServiceContainer(this)
            .register('$logger', logger)
            .register('$config', this.config)
            .register('$validator', this.validator) as ApplicationInjector;

        // Define the app description
        if (
            this._optionsRaw.description &&
            typeof this._optionsRaw.description === 'string'
        ) {
            this.description = this._optionsRaw.description;
        } else {
            const description = this._getDescription();
            if (description !== undefined) {
                this.description = description;
            }
        }

        // Define the app name
        if (
            this._optionsRaw.name &&
            typeof this._optionsRaw.name === 'string'
        ) {
            this.name = this._optionsRaw.name;
        } else {
            const name = this._getName();
            if (name !== undefined) {
                this.name = name;
            }
        }

        // Define the app version
        if (
            this._optionsRaw.version &&
            typeof this._optionsRaw.version === 'string'
        ) {
            this.version = new SemVer(this._optionsRaw.version);
        } else {
            const version = this._getVersion();
            if (version !== 'undefined') {
                this.version = new SemVer(version);
            } else {
                this.version = new SemVer('0.0.0');
            }
        }
        this.config.setReadonly('version', this.version);
    }

    get environment() {
        return this._environment;
    }

    get isDev() {
        return this._isDev;
    }

    get isProduction() {
        return this._isProduction;
    }

    get state() {
        return this._state;
    }

    async init() {
        await this._stateInitiated();
        this._state = 'setup';
        this.profiler.register('app state setup');
        await this._stateSetup();
        this._state = 'registered';
        this.profiler.register('app state registered');
        await this._stateRegistered();
        this._state = 'booted';
        this.profiler.register('app state booted');
        await this._stateBooted();
        this._state = 'ready';
        this.profiler.register('app state ready');
    }

    async run() {
        if (this._state !== 'ready' && this._state !== 'initiated')
            await this.init();
        return Promise.resolve();
    }

    registerShutdownFunction(fct: () => Promise<void>) {
        this._registeredShutdownFunctions.push(fct);
    }

    async shutdown() {
        await this._stateShutdown();

        this._registeredShutdownFunctions.reverse().forEach(async fct => {
            await fct();
        });

        this._state = 'shutdown';
        return Promise.resolve();
    }

    registerPlugin(plugin: PluginInterface) {
        this._plugins.push(plugin);
    }

    getRegisteredPlugins() {
        return this._plugins;
    }

    protected _stateUnknown() {}

    protected async _stateInitiated() {
        return Promise.resolve();
    }

    protected async _stateSetup() {
        // Register plugins
        return Promise.resolve();
    }

    protected async _stateRegistered() {
        // boot plugins
        this._plugins.forEach(async plugin => {
            if (plugin.boot) await plugin.boot(this);
        });

        return Promise.resolve();
    }

    protected async _stateBooted() {
        // ready plugins
        this._plugins.forEach(async plugin => {
            if (plugin.ready) await plugin.ready();
        });

        return Promise.resolve();
    }

    protected async _stateShutdown() {
        return Promise.resolve();
    }

    /**
     * Extend this method to set the app description
     */
    protected _getDescription(): string | undefined {
        return undefined;
    }

    /**
     * Extend this method to set the app name
     */
    protected _getName(): string | undefined {
        return undefined;
    }

    /**
     * Extend this method to set the app version
     */
    protected _getVersion(): string {
        return 'undefined';
    }

    /**
     * Register the environment
     */
    protected _setEnvironment(
        environment: 'development' | 'staging' | 'production' | 'testing'
    ) {
        this._environment = environment;
        this._isDev = environment === 'development';
        this._isProduction = environment === 'production';
    }
}
