/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import Sinon from 'sinon';

import { Logger } from './Logger';

describe('@kephajs/core/Logger/Logger', () => {
    let logger: Logger;
    let stubError: Sinon.SinonStub;
    let stubWarn: Sinon.SinonStub;
    let stubInfo: Sinon.SinonStub;
    let stubLog: Sinon.SinonStub;

    beforeEach(() => {
        logger = new Logger('development');
        stubError = Sinon.stub(console, 'error');
        stubWarn = Sinon.stub(console, 'warn');
        stubInfo = Sinon.stub(console, 'info');
        stubLog = Sinon.stub(console, 'log');
    });

    afterEach(() => {
        stubError.restore();
        stubWarn.restore();
        stubInfo.restore();
        stubLog.restore();
    });

    describe('error', () => {
        it('Should print the error with console.error', () => {
            logger.error('test');
            expect(stubError.getCalls()[0].args[0]).to.be.contain('test');
        });

        it('Should prefix the message by "ERROR:"', () => {
            logger.error('test');
            expect(stubError.getCalls()[0].args[0]).to.be.equal('ERROR: test');
        });

        it('Should print in production environment', () => {
            logger = new Logger('production');
            logger.error('test');
            expect(stubError.getCalls()[0].args[0]).to.be.equal('ERROR: test');
        });

        it('Should print in staging environment', () => {
            logger = new Logger('staging');
            logger.error('test');
            expect(stubError.getCalls()[0].args[0]).to.be.equal('ERROR: test');
        });

        it('Should print in testing environment', () => {
            logger = new Logger('testing');
            logger.error('test');
            expect(stubError.getCalls()[0].args[0]).to.be.equal('ERROR: test');
        });

        it('Should print in development environment', () => {
            logger = new Logger('development');
            logger.error('test');
            expect(stubError.getCalls()[0].args[0]).to.be.equal('ERROR: test');
        });
    });

    describe('warning', () => {
        it('Should print the warning with console.warn', () => {
            logger.warning('test');
            expect(stubWarn.getCalls()[0].args[0]).to.be.contain('test');
        });

        it('Should prefix the message by "WARNING:"', () => {
            logger.warning('test');
            expect(stubWarn.getCalls()[0].args[0]).to.be.equal('WARNING: test');
        });

        it('Should print in production environment', () => {
            logger = new Logger('production');
            logger.warning('test');
            expect(stubWarn.getCalls()[0].args[0]).to.be.equal('WARNING: test');
        });

        it('Should print in staging environment', () => {
            logger = new Logger('staging');
            logger.warning('test');
            expect(stubWarn.getCalls()[0].args[0]).to.be.equal('WARNING: test');
        });

        it('Should print in testing environment', () => {
            logger = new Logger('testing');
            logger.warning('test');
            expect(stubWarn.getCalls()[0].args[0]).to.be.equal('WARNING: test');
        });

        it('Should print in development environment', () => {
            logger = new Logger('development');
            logger.warning('test');
            expect(stubWarn.getCalls()[0].args[0]).to.be.equal('WARNING: test');
        });
    });

    describe('notice', () => {
        it('Should print the notice with console.info', () => {
            logger.notice('test');
            expect(stubInfo.getCalls()[0].args[0]).to.be.contain('test');
        });

        it('Should prefix the message by "NOTICE:"', () => {
            logger.notice('test');
            expect(stubInfo.getCalls()[0].args[0]).to.be.equal('NOTICE: test');
        });

        it('Should not print in production environment', () => {
            logger = new Logger('production');
            logger.notice('test');
            expect(stubInfo.getCalls().length).to.be.equal(0);
        });

        it('Should print in staging environment', () => {
            logger = new Logger('staging');
            logger.notice('test');
            expect(stubInfo.getCalls()[0].args[0]).to.be.equal('NOTICE: test');
        });

        it('Should print in testing environment', () => {
            logger = new Logger('testing');
            logger.notice('test');
            expect(stubInfo.getCalls()[0].args[0]).to.be.equal('NOTICE: test');
        });

        it('Should print in development environment', () => {
            logger = new Logger('development');
            logger.notice('test');
            expect(stubInfo.getCalls()[0].args[0]).to.be.equal('NOTICE: test');
        });
    });

    describe('info', () => {
        it('Should print the error with console.info', () => {
            logger.info('test');
            expect(stubInfo.getCalls()[0].args[0]).to.be.contain('test');
        });

        it('Should prefix the message by "INFO:"', () => {
            logger.info('test');
            expect(stubInfo.getCalls()[0].args[0]).to.be.equal('INFO: test');
        });

        it('Should not print in production environment', () => {
            logger = new Logger('production');
            logger.info('test');
            expect(stubInfo.getCalls().length).to.be.equal(0);
        });

        it('Should not print in staging environment', () => {
            logger = new Logger('staging');
            logger.info('test');
            expect(stubInfo.getCalls().length).to.be.equal(0);
        });

        it('Should print in testing environment', () => {
            logger = new Logger('testing');
            logger.info('test');
            expect(stubInfo.getCalls()[0].args[0]).to.be.equal('INFO: test');
        });

        it('Should print in development environment', () => {
            logger = new Logger('development');
            logger.info('test');
            expect(stubInfo.getCalls()[0].args[0]).to.be.equal('INFO: test');
        });
    });

    describe('debug', () => {
        it('Should print the debug with console.log', () => {
            logger.debug('test');
            expect(stubLog.getCalls()[0].args[0]).to.be.contain('test');
        });

        it('Should prefix the message by "DEBUG:"', () => {
            logger.debug('test');
            expect(stubLog.getCalls()[0].args[0]).to.be.equal('DEBUG: test');
        });

        it('Should not print in production environment', () => {
            logger = new Logger('production');
            logger.debug('test');
            expect(stubLog.getCalls().length).to.be.equal(0);
        });

        it('Should not print in staging environment', () => {
            logger = new Logger('staging');
            logger.debug('test');
            expect(stubLog.getCalls().length).to.be.equal(0);
        });

        it('Should print in testing environment', () => {
            logger = new Logger('testing');
            logger.debug('test');
            expect(stubLog.getCalls()[0].args[0]).to.be.equal('DEBUG: test');
        });

        it('Should print in development environment', () => {
            logger = new Logger('development');
            logger.debug('test');
            expect(stubLog.getCalls()[0].args[0]).to.be.equal('DEBUG: test');
        });
    });

    describe('save to later', () => {
        it('Should be false by default', () => {
            expect(logger.isSaveToLater()).to.be.equal(false);
        });

        it('Should set by setSaveToLater', () => {
            logger.setSaveToLater(true);
            expect(logger.isSaveToLater()).to.be.equal(true);
        });

        it('Should save the log', () => {
            logger.setSaveToLater(true);
            logger.error('An error');
            logger.notice('An notice');
            expect(logger.getSavedLogs()[0].type).to.be.equal(Logger.ERROR);
            expect(logger.getSavedLogs()[0].message).to.be.equal('An error');
            expect(logger.getSavedLogs()[0].datetime).to.be.instanceOf(Date);
            expect(logger.getSavedLogs()[1].type).to.be.equal(Logger.NOTICE);
            expect(logger.getSavedLogs()[1].message).to.be.equal('An notice');
            expect(logger.getSavedLogs()[1].datetime).to.be.instanceOf(Date);
        });

        it('Should print the saved logs', () => {
            logger.setSaveToLater(true);
            logger.error('An error');
            logger.notice('An notice');
            logger.printSavedLogs();
            expect(stubError.getCalls()[0].args[0]).to.be.equal(
                'ERROR: An error'
            );
            expect(stubInfo.getCalls()[0].args[0]).to.be.equal(
                'NOTICE: An notice'
            );
        });

        it('Should not have saved log after printing', () => {
            logger.setSaveToLater(true);
            logger.error('An error');
            logger.notice('An notice');
            logger.printSavedLogs();
            expect(logger.getSavedLogs()).to.be.deep.equal([]);
        });

        it('Should reset the list', () => {
            logger.setSaveToLater(true);
            logger.error('An error');
            logger.notice('An notice');
            logger.resetSavedLogs();
            expect(logger.getSavedLogs()).to.be.deep.equal([]);
        });

        it('Should give saved log', () => {
            const savedLogs = [
                { type: Logger.DEBUG, message: 'debug', datetime: new Date() },
                { type: Logger.INFO, message: 'info', datetime: new Date() },
            ];
            logger.addSavedLogs(savedLogs);
            expect(logger.getSavedLogs()).to.be.deep.equal(savedLogs);
        });
    });
});
