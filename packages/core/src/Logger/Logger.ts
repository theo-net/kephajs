/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TEnvironment } from '../Application';

export class Logger {
    public static readonly ERROR = 0;

    public static readonly WARNING = 1;

    public static readonly NOTICE = 2;

    public static readonly INFO = 3;

    public static readonly DEBUG = 4;

    protected _savedLog: {
        type: number;
        message: string;
        datetime: Date;
    }[] = [];

    protected _saveToLater = false;

    constructor(private _environment: TEnvironment = 'production') {}

    error(message: string) {
        if (!this._saveToLater) {
            this._printError(message);
        } else {
            this._saveLog(Logger.ERROR, message);
        }
    }

    warning(message: string) {
        if (!this._saveToLater) {
            this._printWarning(message);
        } else {
            this._saveLog(Logger.WARNING, message);
        }
    }

    notice(message: string) {
        if (!this._saveToLater) {
            this._printNotice(message);
        } else {
            this._saveLog(Logger.NOTICE, message);
        }
    }

    info(message: string) {
        if (!this._saveToLater) {
            this._printInfo(message);
        } else {
            this._saveLog(Logger.INFO, message);
        }
    }

    debug(message: string) {
        if (!this._saveToLater) {
            this._printDebug(message);
        } else {
            this._saveLog(Logger.DEBUG, message);
        }
    }

    setSaveToLater(saveToLater: boolean) {
        this._saveToLater = saveToLater;
    }

    isSaveToLater() {
        return this._saveToLater;
    }

    getSavedLogs() {
        return this._savedLog;
    }

    printSavedLogs() {
        this._savedLog.forEach(log => {
            switch (log.type) {
                case Logger.ERROR:
                    this._printError(log.message);
                    break;
                case Logger.WARNING:
                    this._printWarning(log.message);
                    break;
                case Logger.NOTICE:
                    this._printNotice(log.message);
                    break;
                case Logger.INFO:
                    this._printInfo(log.message);
                    break;
                case Logger.DEBUG:
                    this._printInfo(log.message);
                    break;
            }
        });
        this.resetSavedLogs();
    }

    addSavedLogs(
        savedLog: {
            type: number;
            message: string;
            datetime: Date;
        }[]
    ) {
        this._savedLog = [...savedLog];
    }

    resetSavedLogs() {
        this._savedLog = [];
    }

    protected _printError(message: string) {
        console.error('ERROR: ' + message);
    }

    protected _printWarning(message: string) {
        console.warn('WARNING: ' + message);
    }

    protected _printNotice(message: string) {
        if (
            ['development', 'testing', 'staging'].indexOf(this._environment) >
            -1
        ) {
            console.info('NOTICE: ' + message);
        }
    }

    protected _printInfo(message: string) {
        if (['development', 'testing'].indexOf(this._environment) > -1) {
            console.info('INFO: ' + message);
        }
    }

    protected _printDebug(message: string) {
        if (['development', 'testing'].indexOf(this._environment) > -1) {
            console.log('DEBUG: ' + message);
        }
    }

    protected _saveLog(
        type:
            | typeof Logger.ERROR
            | typeof Logger.WARNING
            | typeof Logger.NOTICE
            | typeof Logger.INFO
            | typeof Logger.DEBUG,
        message: string
    ) {
        this._savedLog.push({ type, message, datetime: new Date() });
    }
}
