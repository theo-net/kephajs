/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export class CircularRefereneError<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    TypeConfig extends { [key: string]: any } = { [key: string]: any }
> extends Error {
    constructor(references: (keyof TypeConfig)[], circular: keyof TypeConfig) {
        let path = '';
        references.forEach(id => {
            path +=
                (path === '' ? '' : ' → ') +
                ((id === circular ? '❗' + (id as string) : id) as string);
        });
        path += ' → ❗' + (circular as string);

        super(
            `Circular reference detected in [${
                references[0] as string
            }] (${path})`
        );
    }
}
