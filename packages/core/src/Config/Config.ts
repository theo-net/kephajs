/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { CircularRefereneError } from './CircularReferenceError';

/* eslint-disable @typescript-eslint/no-explicit-any */

/**
 * Register all app config
 */
export class Config<
    TypeConfig extends { [key: string]: any } = { [key: string]: any }
> {
    protected _cache: Record<string, any> = {} as Record<string, any>;

    protected _readonly: (keyof TypeConfig)[] = [];

    protected _vars: Record<keyof TypeConfig, any> = {} as Record<
        keyof TypeConfig,
        any
    >;

    /**
     * Register an element
     *
     * @param id Id of the element
     * @param value Value of the element
     * @param replace Replace or not an element with the same id
     */
    set(id: keyof TypeConfig, value: any, replace = false): void {
        if (this._readonly.indexOf(id as string) > -1) {
            throw new TypeError(`The key [${id as string}] is readonly`);
        }
        if (!replace && this._vars[id] !== undefined) return;

        this._vars[id] = value;
    }

    /**
     * Register an element as readonly
     *
     * @param id Id of the element
     * @param value Value of the element
     */
    setReadonly(id: keyof TypeConfig, value: any): void {
        if (this._readonly.indexOf(id) > -1) {
            throw new TypeError(`The key [${id as string}] is readonly`);
        }

        this._vars[id] = value;
        this._readonly.push(id);
    }

    /**
     * Return an element or the default value if the element don't exist.
     * Formating constists to replace a constant by the value.
     */
    get<T>(
        id: keyof TypeConfig | string,
        dflt: T | undefined = undefined,
        format = true,
        _references: (keyof TypeConfig)[] | null = null
    ): T | { [key: string]: any } | undefined {
        // Is in the cache
        if (this._cache[id as string]) {
            return this._cache[id as string];
        }

        if (this.has(id)) {
            if (format && typeof this._vars[id] === 'string') {
                let nbReplace = 0;
                const result = this._vars[id].replace(
                    /%([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff.]+)%/g,
                    (_match: string, p1: string): any => {
                        nbReplace++;
                        if (_references === null || nbReplace > 1) {
                            _references = [id];
                            nbReplace = 1;
                        }
                        if (_references.indexOf(p1) === -1) {
                            _references.push(p1);

                            return this.get(p1, undefined, true, _references);
                        } else {
                            throw new CircularRefereneError(_references, p1);
                        }
                    }
                );
                return (this._cache[id as string] = result);
            } else {
                return (this._cache[id as string] = this._vars[id]);
            }
        } else if (typeof dflt === 'function') {
            return dflt();
        } else {
            return dflt;
        }
    }

    /**
     * Return a serie of element (with `.*`)
     */
    getSerie(
        id: string,
        dflt: { [key: string]: any } | undefined = undefined
    ): { [key: string]: any } | undefined {
        // Is in the cache
        if (this._cache[id]) {
            return this._cache[id];
        }

        if (/.+\.\*$/.test(id)) {
            const root = id.substring(0, id.length - 1);
            const results: { [keys: string]: any } = {};

            Object.getOwnPropertyNames(this._vars).forEach(elmt => {
                if (elmt.substring(0, root.length) == root)
                    results[elmt.substring(root.length)] = this.get(elmt);
            });

            return (this._cache[id] = { ...dflt, ...results });
        } else {
            throw TypeError('The parameter need to reference a tree');
        }
    }

    /**
     * The element exists or not?
     */
    has(id: keyof TypeConfig): boolean {
        return Object.keys(this._vars).indexOf(id as string) > -1;
    }

    /**
     * Delete an element. To delete a series, we need to use `foo.bar.*` as id.
     * @param fct Deleting function (you don't need to use in our app)
     */
    delete(id: keyof TypeConfig, fct?: (elmt: string) => void): void {
        if (this._readonly.indexOf(id) > -1) {
            throw new TypeError(`The key [${id as string}] is readonly`);
        }

        if (typeof fct !== 'function')
            fct = (elmt: string): void => {
                delete this._vars[elmt];
            };
        if (/.+\.\*$/.test(id as string)) {
            const root = (id as string).substring(0, (id as string).length - 1);
            Object.getOwnPropertyNames(this._vars).forEach(elmt => {
                if (elmt.substring(0, root.length) == root)
                    (fct as (elmt: string) => void)(elmt);
            });
        } else fct(id as string);
    }

    /**
     * Initialize a series of element
     * @param replace Replace or not an existing element
     */
    add(parameters: Record<string, any>, replace = false): void {
        for (const id in parameters) {
            this.set(id, parameters[id], replace);
        }
    }

    /**
     * Add a JSON
     */
    addJson(parameters: any, prefix = '', replace = false) {
        if (parameters?.constructor === {}.constructor) {
            Object.getOwnPropertyNames(parameters).forEach(key => {
                if (parameters[key]?.constructor === {}.constructor) {
                    this.addJson(
                        parameters[key],
                        prefix !== '' ? prefix + '.' + key : key,
                        replace
                    );
                } else {
                    this.set(
                        prefix !== '' ? prefix + '.' + key : key,
                        parameters[key],
                        replace
                    );
                }
            });
        }
    }

    /**
     * Return all elements
     */
    getAll(): Record<string, any> {
        return this._vars;
    }

    /**
     * Return the list of the readonly properties
     */
    getReadonly() {
        return this._readonly;
    }

    /**
     * Refresh the cache
     */
    refresh(id: string | string[] | undefined = undefined) {
        if (typeof id === 'string') {
            delete this._cache[id];
        } else if (id === undefined) {
            this._cache = {};
        } else {
            id.forEach(key => {
                delete this._cache[key];
            });
        }
    }

    /**
     * Reset the config (delete all elements)
     */
    reset(): void {
        this._cache = {};
        this._readonly = [];
        this._vars = {} as Record<keyof TypeConfig, any>;
    }
}
