/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { CircularRefereneError } from './CircularReferenceError';
import { Config } from './Config';

describe('@kephajs/core/Config/Config', () => {
    let config: Config;

    beforeEach(() => {
        config = new Config();
    });

    describe('set', () => {
        it('Should register new config var', () => {
            config.set('test', 'aValue');

            expect(config.get('test')).to.be.equal('aValue');
        });

        it('Sould not replace an existing value', () => {
            config.set('test', 'aValue');
            config.set('test', 'otherValue');

            expect(config.get('test')).to.be.equal('aValue');
        });

        it('Should have a parameter to force replacing', () => {
            config.set('test', 'aValue');
            config.set('test', 'otherValue', true);

            expect(config.get('test')).to.be.equal('otherValue');
        });
    });

    describe('get', () => {
        it('Should return a config var', () => {
            config.set('param', 'foobar');

            expect(config.get('param')).to.be.equal('foobar');
        });

        it("Should return an default value if the config var don't exist", () => {
            config.set('param', 'foobar');

            expect(config.get('param2', 'lol')).to.be.equal('lol');
        });

        it('Can have a default value from a function', () => {
            config.set('param', 'foobar');

            expect(config.get('param2', () => 'lol')).to.be.equal('lol');
        });

        it("Should return undefined if the parameter don't exist and they are no default value", () => {
            expect(config.get('hello')).to.be.equal(undefined);
        });

        it('Should replace constants by the values', () => {
            config.add({
                param1: 'foo',
                param2: 'bar',
                param3: '%param1%',
                param4: '%param1%?',
                param5: '%param1%%param2%',
                'par.am': 'foobar',
                param6: 'a %par.am% person',
                param7: 'a %param5%',
            });
            const expected = {
                param1: 'foo',
                param2: 'bar',
                param3: 'foo',
                param4: 'foo?',
                param5: 'foobar',
                'par.am': 'foobar',
                param6: 'a foobar person',
                param7: 'a foobar',
            };

            expect(config.get('param3')).to.be.equal(expected.param3);
            expect(config.get('param4')).to.be.equal(expected.param4);
            expect(config.get('param5')).to.be.equal(expected.param5);
            expect(config.get('param6')).to.be.equal(expected.param6);
        });

        it('Should not replace the constant if format set to false', () => {
            config.add({
                param1: 'foo',
                param2: 'bar',
                param3: '%param1%',
                param4: '%param1%?',
                param5: '%param1%%param2%',
            });
            const expected = {
                param1: 'foo',
                param2: 'bar',
                param3: '%param1%',
                param4: '%param1%?',
                param5: '%param1%%param2%',
            };

            expect(config.get('param3', null, false)).to.be.equal(
                expected.param3
            );
            expect(config.get('param4', null, false)).to.be.equal(
                expected.param4
            );
            expect(config.get('param5', null, false)).to.be.equal(
                expected.param5
            );
        });

        it("Sould not format the value if it's not a string", () => {
            const obj = {};
            config.add({
                param1: 'foo',
                param2: '%param1%?',
                param3: obj,
            });
            const expected = {
                param1: 'foo',
                param2: 'foo?',
                param3: obj,
            };

            expect(config.get('param2')).to.be.equal(expected.param2);
            expect(config.get('param3')).to.be.equal(expected.param3);
        });

        it('Should throw if circular reference', () => {
            config.set('foo', 'Foo');
            config.set('bar', 'bar!');
            config.set('Bar', '%myBar%');
            config.set('myBar', '%param%');
            config.set('param', '%Bar%');
            config.set('FOOBAR', '%foo%%bar% %Bar%');
            try {
                config.get('FOOBAR');
            } catch (error) {
                expect(error).to.be.instanceOf(CircularRefereneError);
                expect((error as CircularRefereneError).message).to.be.equal(
                    'Circular reference detected in [FOOBAR] (FOOBAR → ❗Bar → myBar → param → ❗Bar)'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    describe('getSerie', () => {
        it('Should return a series of value with foo.bar.*', () => {
            config.add({
                'foo.truc': 1,
                'foo.bar.a': 'foo',
                'foo.bar.b': '%foo.bar.a%bar',
                'foo.bar.c.d': 1,
                'foo.bar.c.e': 1,
                'foo.bar.c.f.g': 1,
            });

            expect(config.getSerie('foo.bar.*')).to.be.deep.equal({
                a: 'foo',
                b: 'foobar',
                'c.d': 1,
                'c.e': 1,
                'c.f.g': 1,
            });
        });

        it('Should merge the default value', () => {
            config.add({
                'foo.bar.a': 1,
                'foo.bar.c.d': 1,
                'foo.bar.c.e': 0,
                'foo.bar.c.f.g': 1,
            });

            expect(
                config.getSerie('foo.bar.*', { b: 1, 'c.e': 1 })
            ).to.be.deep.equal({
                a: 1,
                b: 1,
                'c.d': 1,
                'c.e': 0,
                'c.f.g': 1,
            });
        });

        it('Should trhow an error if not serie', () => {
            try {
                config.getSerie('foobar');
            } catch (error) {
                expect(error).to.be.instanceOf(TypeError);
                expect((error as TypeError).message).to.be.equal(
                    'The parameter need to reference a tree'
                );
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    describe('has', () => {
        it('Should return true if config have the key', () => {
            config.set('test', true);

            expect(config.has('test')).to.be.equal(true);
        });

        it("Should return false if config haven't the key", () => {
            config.set('test', true);

            expect(config.has('test2')).to.be.equal(false);
        });
    });

    describe('delete', () => {
        it('Should delete a element', () => {
            config.set('test', true);
            expect(config.has('test')).to.be.equal(true);

            config.delete('test');
            expect(config.has('test')).to.be.equal(false);
        });

        it('Should delete element foo.bar.*', () => {
            config.add({
                'foo.truc': 1,
                'foo.bar.a': 1,
                'foo.bar.b': 1,
                'foo.bar.c.d': 1,
                'foo.bar.c.e': 1,
                'foo.bar.c.f.g': 1,
            });
            config.delete('foo.bar.*');

            expect(config.has('foo.truc')).to.be.equal(true);
            expect(config.has('foo.bar.a')).to.be.equal(false);
            expect(config.has('foo.bar.b')).to.be.equal(false);
            expect(config.has('foo.bar.c.d')).to.be.equal(false);
            expect(config.has('foo.bar.c.e')).to.be.equal(false);
            expect(config.has('foo.bar.c.f.g')).to.be.equal(false);
        });
    });

    describe('add', () => {
        it('Should add a series of elements', () => {
            config.set('test', 1);
            config.add({ a: 2, b: 3 });

            expect(config.getAll()).to.be.deep.equal({ test: 1, a: 2, b: 3 });
        });
    });

    describe('addJson', () => {
        it('Should add a Json configuration', () => {
            config.addJson(
                JSON.parse(`{
                    "myKey": "foobar",
                    "myOtherKey": {
                        "foo": 4,
                        "bar": 2,
                        "table": ["a", "b", "c"]
                    }
                }`)
            );
            expect(config.getAll()).to.be.deep.equal({
                myKey: 'foobar',
                'myOtherKey.foo': 4,
                'myOtherKey.bar': 2,
                'myOtherKey.table': ['a', 'b', 'c'],
            });
        });

        it('Should add a Json and prefix the values', () => {
            config.addJson(
                JSON.parse(`{
                    "myKey": "foobar",
                    "myOtherKey": {
                        "foo": 4,
                        "bar": 2,
                        "table": ["a", "b", "c"]
                    }
                }`),
                'test'
            );
            expect(config.getAll()).to.be.deep.equal({
                'test.myKey': 'foobar',
                'test.myOtherKey.foo': 4,
                'test.myOtherKey.bar': 2,
                'test.myOtherKey.table': ['a', 'b', 'c'],
            });
        });

        it('Should add a Json, but by default, not replace the values', () => {
            config.set('myKey', 'hello');
            config.set('myOtherKey.foo', 3);
            config.addJson(
                JSON.parse(`{
                    "myKey": "foobar",
                    "myOtherKey": {
                        "foo": 4,
                        "bar": 2,
                        "table": ["a", "b", "c"]
                    }
                }`)
            );
            expect(config.getAll()).to.be.deep.equal({
                myKey: 'hello',
                'myOtherKey.foo': 3,
                'myOtherKey.bar': 2,
                'myOtherKey.table': ['a', 'b', 'c'],
            });
        });

        it('Should add a Json, but can replace the values', () => {
            config.set('myKey', 'hello');
            config.set('myOtherKey.foo', 3);
            config.addJson(
                JSON.parse(`{
                    "myKey": "foobar",
                    "myOtherKey": {
                        "foo": 4,
                        "bar": 2,
                        "table": ["a", "b", "c"]
                    }
                }`),
                '',
                true
            );
            expect(config.getAll()).to.be.deep.equal({
                myKey: 'foobar',
                'myOtherKey.foo': 4,
                'myOtherKey.bar': 2,
                'myOtherKey.table': ['a', 'b', 'c'],
            });
        });
    });

    describe('getAll', () => {
        it('Should return all elements', () => {
            config.add({ a: 2, b: 3 });
            expect(config.getAll()).to.be.deep.equal({ a: 2, b: 3 });
        });
    });

    describe('reset', () => {
        it('Should delete all elements', () => {
            config.add({ a: 2, b: 3 });
            config.reset();

            expect(config.getAll()).to.be.deep.equal({});
        });

        it('Should delete readonly properties', () => {
            config.set('foobar', 42);
            config.setReadonly('foo', 4);
            config.setReadonly('bar', 2);
            config.reset();
            expect(config.getAll()).to.be.deep.equal({});
            expect(config.getReadonly()).to.be.deep.equal([]);
        });

        it('Should delete the cache', () => {
            config.set('foobar', 42);
            config.get('foobar');
            config.reset();
            config.set('foobar', 33);
            expect(config.get('foobar')).to.be.equal(33);
        });
    });

    describe('setReadonly', () => {
        it('Should register the value', () => {
            config.setReadonly('foobar', 42);
            expect(config.get('foobar')).to.be.equal(42);
        });

        it('Should throw if try to setReadonly a readonly value', () => {
            config.setReadonly('foobar', 42);

            try {
                config.setReadonly('foobar', 2);
            } catch (error) {
                expect(error).to.be.instanceOf(TypeError);
                expect((error as TypeError).message).to.be.equal(
                    'The key [foobar] is readonly'
                );
                expect(config.get('foobar')).to.be.equal(42);
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should throw if try to set a readonly value', () => {
            config.setReadonly('foobar', 42);

            try {
                config.set('foobar', 3);
            } catch (error) {
                console.log('ERROR');
                expect(error).to.be.instanceOf(TypeError);
                expect((error as TypeError).message).to.be.equal(
                    'The key [foobar] is readonly'
                );
                expect(config.get('foobar')).to.be.equal(42);
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should not delete a readonly value and throw', () => {
            config.setReadonly('foobar', 42);

            try {
                config.delete('foobar');
            } catch (error) {
                expect(error).to.be.instanceOf(TypeError);
                expect((error as TypeError).message).to.be.equal(
                    'The key [foobar] is readonly'
                );
                expect(config.has('foobar')).to.be.equal(true);
                return;
            }

            expect.fail('Should have thrown');
        });
    });

    describe('getReadonly', () => {
        it('Should return all readonly properties', () => {
            config.set('foobar', 42);
            config.setReadonly('foo', 4);
            config.setReadonly('bar', 2);
            expect(config.getReadonly()).to.be.deep.equal(['foo', 'bar']);
        });
    });

    describe('caching', () => {
        it('Should add a parameter in the cache after is getting', () => {
            config.set('foobar', 1);
            config.set('foobar', 2, true);
            expect(config.get('foobar')).to.be.equal(2);
            config.set('foobar', 3, true);
            expect(config.get('foobar')).to.be.equal(2);
        });

        it('Should refresh one element', () => {
            config.set('foo', 1);
            config.set('bar', 2, true);
            config.get('foo');
            config.get('bar');
            config.set('foo', 3, true);
            config.set('bar', 4, true);
            expect(config.get('foo')).to.be.equal(1);
            config.refresh('foo');
            expect(config.get('foo')).to.be.equal(3);
            expect(config.get('bar')).to.be.equal(2);
        });

        it('Should refresh all elements', () => {
            config.set('foo', 1);
            config.set('bar', 2, true);
            config.get('foo');
            config.get('bar');
            config.set('foo', 3, true);
            config.set('bar', 4, true);
            config.refresh();
            expect(config.get('foo')).to.be.equal(3);
            expect(config.get('bar')).to.be.equal(4);
        });

        it('Should work with included parameters', () => {
            config.set('Foo', 'foo');
            config.set('Bar', 'bar');
            config.set('Foobar', 'Hello %Foo%%Bar%!');

            expect(config.get('Foobar')).to.be.equal('Hello foobar!');

            // update a value
            config.set('Foo', 'the ', true);
            expect(config.get('Foobar')).to.be.equal('Hello foobar!');
            config.refresh(['Foobar', 'Foo']);
            expect(config.get('Foobar')).to.be.equal('Hello the bar!');
        });

        it('Should work with series', () => {
            config.set('foo.a', 'bar');
            config.set('foo.b', 'foo');
            expect(config.getSerie('foo.*')).to.be.deep.equal({
                a: 'bar',
                b: 'foo',
            });

            config.set('foo.a', 'foobar', true);
            expect(config.getSerie('foo.*')).to.be.deep.equal({
                a: 'bar',
                b: 'foo',
            });
            config.refresh('foo.*');
            expect(config.getSerie('foo.*')).to.be.deep.equal({
                a: 'bar',
                b: 'foo',
            });
            config.refresh(['foo.a', 'foo.*']);
            expect(config.getSerie('foo.*')).to.be.deep.equal({
                a: 'foobar',
                b: 'foo',
            });
        });
    });
});
