/*
 * @kephajs/console
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Application } from './Application';
import { Config } from './Config/Config';
import { Logger } from './Logger/Logger';
import { ServiceContainer } from './ServiceContainer/ServiceContainer';
import { Validator } from './Validate/Validator';

export interface ApplicationRegisteredServices {
    $app: Application;
    $config: Config;
    $container: ServiceContainer;
    $logger: Logger;
    $validator: Validator;
}
