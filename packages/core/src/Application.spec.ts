/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';
import Sinon from 'sinon';

import { Application } from './Application';
import { Config } from './Config/Config';
import { Logger } from './Logger/Logger';
import { SemVer } from './SemVer/SemVer';
import { version as kephajsVersion } from './version';
import { Validator } from './Validate/Validator';
import { ServiceContainer } from './ServiceContainer/ServiceContainer';
import { Profiler } from './Profiler';
import { PluginInterface } from './Plugin';

describe('@kephajs/core/Application', () => {
    it('Should register the appRoot and should return it', () => {
        const app = new Application('test');

        expect(app.appRoot).to.be.equal('test');
    });

    describe('kephajsVersion', () => {
        it('Should be a SemVer type', () => {
            expect(new Application('test').kephajsVersion).to.be.instanceOf(
                SemVer
            );
        });

        it('Should give the KephaJs core version', () => {
            expect(
                new Application('test').kephajsVersion.toString()
            ).to.be.equal(kephajsVersion);
        });
    });

    describe('state', () => {
        it('Should execute _stateUnknown in state unknown', () => {
            let state = '';
            class MyApp extends Application {
                protected _stateUnknown() {
                    super._stateUnknown();
                    state = this._state;
                    return Promise.resolve();
                }
            }
            new MyApp('test');
            expect(state).to.be.equal('unknown');
        });

        it('Should execute _stateInitiated in state initiated', async () => {
            let state = '';
            class MyApp extends Application {
                protected async _stateInitiated() {
                    await super._stateInitiated();
                    state = this._state;
                    return Promise.resolve();
                }
            }
            await new MyApp('test').init();
            expect(state).to.be.equal('initiated');
        });

        it('Should execute _stateSetup in state setup', async () => {
            let state = '';
            class MyApp extends Application {
                protected async _stateSetup() {
                    await super._stateSetup();
                    state = this._state;
                    return Promise.resolve();
                }
            }
            await new MyApp('test').init();
            expect(state).to.be.equal('setup');
        });

        it('Should execute _stateRegistered in state registered', async () => {
            let state = '';
            class MyApp extends Application {
                protected async _stateRegistered() {
                    await super._stateRegistered();
                    state = this._state;
                    return Promise.resolve();
                }
            }
            await new MyApp('test').init();
            expect(state).to.be.equal('registered');
        });

        it('Should execute _stateBooted in state booted', async () => {
            let state = '';
            class MyApp extends Application {
                protected async _stateBooted() {
                    await super._stateBooted();
                    state = this._state;
                    return Promise.resolve();
                }
            }
            await new MyApp('test').init();
            expect(state).to.be.equal('booted');
        });

        it('Should execute _stateReady in state ready', async () => {
            let state = '';
            class MyApp extends Application {
                async run() {
                    await super.run();
                    state = this._state;
                    return Promise.resolve();
                }
            }
            const app = new MyApp('test');
            await app.init();
            await app.run();
            expect(state).to.be.equal('ready');
        });

        it('Should execute _stateShutdown before state shutdown', async () => {
            let executed = false;
            let state = '';
            class MyApp extends Application {
                protected async _stateShutdown() {
                    await super._stateShutdown();
                    state = this._state;
                    executed = true;
                    return Promise.resolve();
                }
            }
            const app = new MyApp('test');
            await app.init();
            await app.shutdown();
            expect(app.state).to.be.equal('shutdown');
            expect(state).to.be.equal('ready');
            expect(executed).to.be.equal(true);
        });
    });

    describe('environment', () => {
        it('Should be set to production in default configuration', () => {
            const app = new Application('test');

            expect(app.isDev).to.be.equal(false);
            expect(app.isProduction).to.be.equal(true);
            expect(app.environment).to.be.equal('production');
        });

        it('Can be passed through the option argument', () => {
            expect(
                new Application('test', { environment: 'staging' }).environment
            ).to.be.equal('staging');
        });

        it('Should take default value if options is an record without environment key', () => {
            expect(new Application('test', {}).environment).to.be.equal(
                'production'
            );
        });

        it('Can be set to development', () => {
            const app = new Application('test', 'development');

            expect(app.isDev).to.be.equal(true);
            expect(app.isProduction).to.be.equal(false);
            expect(app.environment).to.be.equal('development');
        });

        it('Can be set to staging', () => {
            const app = new Application('test', 'staging');

            expect(app.isDev).to.be.equal(false);
            expect(app.isProduction).to.be.equal(false);
            expect(app.environment).to.be.equal('staging');
        });

        it('Can be set to production', () => {
            const app = new Application('test', 'production');

            expect(app.isDev).to.be.equal(false);
            expect(app.isProduction).to.be.equal(true);
            expect(app.environment).to.be.equal('production');
        });

        it('Can be set to testing', () => {
            const app = new Application('test', 'testing');

            expect(app.isDev).to.be.equal(false);
            expect(app.isProduction).to.be.equal(false);
            expect(app.environment).to.be.equal('testing');
        });

        it('Cannot be set to an other value', () => {
            try {
                new Application('test', 'foobar' as unknown as 'production');
            } catch (error) {
                expect(error).to.be.instanceOf(TypeError);
                return;
            }
            expect.fail('Should have thrown');
        });
    });

    describe('profiler', () => {
        it('Should have profiler instance', () => {
            expect(new Application('test').profiler).to.be.instanceOf(Profiler);
        });

        it('Should have the launch timestamp registered', () => {
            expect(
                new Application('test').profiler.getAll()[0].description
            ).to.be.equal('begin');
        });

        it('Should register begin', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.profiler.getElement('begin')).to.not.be.equal(undefined);
        });

        it('Should register initiated state', async () => {
            const app = new Application('test');
            await app.init();
            expect(
                app.profiler.getElement('app state initiated')
            ).to.not.be.equal(undefined);
        });

        it('Should register setup state', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.profiler.getElement('app state setup')).to.not.be.equal(
                undefined
            );
        });

        it('Should register registered state', async () => {
            const app = new Application('test');
            await app.init();
            expect(
                app.profiler.getElement('app state registered')
            ).to.not.be.equal(undefined);
        });

        it('Should register booted state', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.profiler.getElement('app state booted')).to.not.be.equal(
                undefined
            );
        });

        it('Should register ready state', async () => {
            const app = new Application('test');
            await app.init();
            expect(app.profiler.getElement('app state ready')).to.not.be.equal(
                undefined
            );
        });
    });

    describe('description', () => {
        it('Should set to undefined by default', () => {
            expect(new Application('test').description).to.be.equal(undefined);
        });

        it('Should call _getDescription()', () => {
            class MyApp extends Application {
                protected _getDescription(): string {
                    return 'Hello world!';
                }
            }

            expect(new MyApp('test').description).to.be.equal('Hello world!');
        });

        it('Should take the description from options.description if defined', () => {
            expect(
                new Application('test', { description: 'Hello' }).description
            ).to.be.equal('Hello');
        });
    });

    describe('name', () => {
        it('Should set to undefined by default', () => {
            expect(new Application('test').name).to.be.equal(undefined);
        });

        it('Should call _getName()', () => {
            class MyApp extends Application {
                protected _getName(): string {
                    return 'my application';
                }
            }

            expect(new MyApp('test').name).to.be.equal('my application');
        });

        it('Should take the name from options.name if defined', () => {
            expect(
                new Application('test', { name: 'foobar' }).name
            ).to.be.equal('foobar');
        });
    });

    describe('version', () => {
        it('Should be SemVer type', () => {
            expect(new Application('test').version).to.be.instanceOf(SemVer);
        });

        it("Should set to '0.0.0' by default", () => {
            expect(new Application('test').version.toString()).to.be.equal(
                '0.0.0'
            );
        });

        it('Should call _getVersion()', () => {
            class MyApp extends Application {
                protected _getVersion(): string {
                    return '3.2.1';
                }
            }

            expect(new MyApp('test').version.toString()).to.be.equal('3.2.1');
        });

        it('Should parse the result of _getVersion() if it\'s not "undefined"', () => {
            class MyApp extends Application {
                protected _getVersion(): string {
                    return '3.2.1';
                }
            }

            expect(new MyApp('test').version.toString()).to.be.equal('3.2.1');
        });

        it('Should take the default value if the result of _getVersion() if is "undefined"', () => {
            class MyApp extends Application {
                protected _getVersion(): string {
                    return 'undefined';
                }
            }

            expect(new MyApp('test').version.toString()).to.be.equal('0.0.0');
        });

        it('Should take the version from options.version if defined', () => {
            expect(
                new Application('test', { version: '4.5.6' }).version.toString()
            ).to.be.equal('4.5.6');
        });
    });

    describe('container', () => {
        it('Should register the service container', () => {
            expect(new Application('test').container).to.be.instanceOf(
                ServiceContainer
            );
        });

        it('Should register logger service', () => {
            expect(new Application('test').container.get('$logger')).instanceOf(
                Logger
            );
            expect(
                new Application('test').container.get('$logger').isSaveToLater()
            ).to.be.equal(false);
        });

        it('Should set saveToLater parameter of logger service', () => {
            expect(
                new Application('test', { _savetoLaterLogs: true }).container
                    .get('$logger')
                    .isSaveToLater()
            ).to.be.equal(true);
        });

        it('Should register config service', () => {
            expect(new Application('test').container.get('$config')).instanceOf(
                Config
            );
        });

        it('Should give a direct link to the config service', () => {
            const app = new Application('test');

            expect(app.config).to.be.equal(app.container.get('$config'));
        });

        it('Should register environment and version in config as readonly', () => {
            const app = new Application('test', {
                environment: 'staging',
                version: '1.2.3',
            });

            expect(app.config.get('environment')).to.be.equal('staging');
            expect(app.config.get<SemVer>('version')?.toString()).to.be.equal(
                '1.2.3'
            );
            expect(app.config.getReadonly()).to.contain('environment');
            expect(app.config.getReadonly()).to.contain('version');
        });

        it('Should register validator service', () => {
            expect(
                new Application('test').container.get('$validator')
            ).instanceOf(Validator);
        });

        it('Should give a direct link to the validator service', () => {
            const app = new Application('test');

            expect(app.validator).to.be.equal(app.container.get('$validator'));
        });
    });

    describe('registerShutdownFunction', () => {
        it('Should register a shutdown function', async () => {
            let called = false;
            const app = new Application('test');
            app.registerShutdownFunction(() => {
                called = true;
                return Promise.resolve();
            });
            await app.init();
            await app.shutdown();
            expect(called).to.be.equal(true);
        });

        it('Should call the last registered first', async () => {
            const called: string[] = [];
            const app = new Application('test');

            app.registerShutdownFunction(() => {
                called.push('First');
                return Promise.resolve();
            });
            app.registerShutdownFunction(() => {
                called.push('Second');
                return Promise.resolve();
            });
            app.registerShutdownFunction(() => {
                called.push('Third');
                return Promise.resolve();
            });

            await app.init();
            await app.shutdown();
            expect(called).to.be.deep.equal(['Third', 'Second', 'First']);
        });
    });

    describe('plugins', () => {
        class MyPlugin implements PluginInterface {
            boot(app: Application) {
                app.profiler.register('tick');
                return Promise.resolve();
            }

            ready() {
                return Promise.resolve();
            }
        }

        it('Should register a plugin', () => {
            const app = new Application('test');
            const plugin1 = new MyPlugin(),
                plugin2 = new MyPlugin(),
                plugin3 = new MyPlugin();
            app.registerPlugin(plugin1);
            app.registerPlugin(plugin2);
            app.registerPlugin(plugin3);
            expect(app.getRegisteredPlugins()).to.be.deep.equal([
                plugin1,
                plugin2,
                plugin3,
            ]);
        });

        it('Should call the boot function after the registered state', async () => {
            const plugin = new MyPlugin();
            const spy = Sinon.spy(plugin, 'boot');

            const app = new Application('test');
            app.registerPlugin(plugin);

            await Object.getPrototypeOf(app)._stateRegistered.call(app);
            expect(spy.called).to.be.equal(true);
        });

        it('Should call the boot function with the app instance', async () => {
            const plugin = new MyPlugin();
            const spy = Sinon.spy(plugin, 'boot');

            const app = new Application('test');
            app.registerPlugin(plugin);

            await Object.getPrototypeOf(app)._stateRegistered.call(app);
            expect(spy.getCalls()[0].args[0]).to.be.equal(app);
        });

        it('Should call the ready function after the booted state', async () => {
            const plugin = new MyPlugin();
            const spy = Sinon.spy(plugin, 'ready');

            const app = new Application('test');
            app.registerPlugin(plugin);

            await Object.getPrototypeOf(app)._stateBooted.call(app);
            expect(spy.called).to.be.equal(true);
        });
    });
});
