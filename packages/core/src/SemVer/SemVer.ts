/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Manipulate version with the syntax "Semantic Versioning 2.0.0" https://semver.org/
 */
export class SemVer {
    // String representation of the version
    private readonly version: string;

    // Number with the Major version (0 if undefined)
    public readonly major: number = 0;

    // Number with the Minor version (0 if undefined)
    public readonly minor: number = 0;

    // Number with the Patch version (0 if undefined)
    public readonly patch: number = 0;

    // String with the Pre-release version ("" if undefined)
    public readonly preRelease: string = '';

    // String with the Build version ("" if undefined)
    public readonly build: string = '';

    /**
     * Initialize the object
     * @param version The string representation of the version
     */
    constructor(version: string) {
        const versionParsed = version
            .trim()
            .match(/^(\d*)\.(\d*)\.?(\d*)(-((\w+)(\+(.+))?))?$/);

        if (versionParsed !== null) {
            this.major = +versionParsed[1];
            this.minor = +versionParsed[2];
            this.patch = +versionParsed[3];
            this.preRelease = versionParsed[6] ?? '';
            this.build = versionParsed[8] ?? '';

            this.version = this.major + '.' + this.minor + '.' + this.patch;

            if (this.preRelease !== '') {
                this.version = this.version + '-' + this.preRelease;
                if (this.build !== '') {
                    this.version = this.version + '+' + this.build;
                }
            }
        } else {
            throw new TypeError("version don't respect the semver convention");
        }
    }

    toString(): string {
        return this.version;
    }
}
