/*
 * @kephajs/core
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { SemVer } from './SemVer';

describe('@kephajs/core/SemVer/SemVer', () => {
    describe('constructor and toString()', () => {
        it('Should throw TypeError exception if not SemVer convention', () => {
            try {
                new SemVer('foobar');
            } catch (error) {
                expect(error).to.be.instanceOf(TypeError);
                return;
            }
            expect.fail('Should have thrown');
        });
        it('Should return the version', () => {
            const semver = new SemVer('1.2.3');

            expect(semver.toString()).to.be.equal('1.2.3');
        });

        it('Should trim the version', () => {
            const semver = new SemVer(' 1.2.3  ');

            expect(semver.toString()).to.be.equal('1.2.3');
        });

        it('Should take major, minor or patch number to 0 if undefined', () => {
            expect(new SemVer('.2.3').toString()).to.be.equal('0.2.3');
            expect(new SemVer('1..3').toString()).to.be.equal('1.0.3');
            expect(new SemVer('1.2').toString()).to.be.equal('1.2.0');
        });

        it('Should return pre-release identifier and build if definned', () => {
            expect(new SemVer('1.2.3').toString()).to.be.equal('1.2.3');
            expect(new SemVer('1.2.3-foobar').toString()).to.be.equal(
                '1.2.3-foobar'
            );
            expect(new SemVer('1.2.3-foo+bar').toString()).to.be.equal(
                '1.2.3-foo+bar'
            );
        });
    });

    describe('minor, major, patch', () => {
        it('Should return 0 if undefined', () => {
            expect(new SemVer('.2.3').major).to.be.equal(0);
            expect(new SemVer('1..3').minor).to.be.equal(0);
            expect(new SemVer('1.2').patch).to.be.equal(0);
        });

        it('Should return the correct number as number', () => {
            const semver = new SemVer('1.2.3');

            expect(semver.major).to.be.equal(1);
            expect(semver.minor).to.be.equal(2);
            expect(semver.patch).to.be.equal(3);
        });
    });

    describe('pre-release and buil', () => {
        it('Should not accept build identifier without pre-release', () => {
            try {
                new SemVer('1.2.3+foobar');
            } catch (error) {
                expect(error).to.be.instanceOf(TypeError);
                return;
            }
            expect.fail('Should have thrown');
        });

        it('Should return the correct pre-release identifier', () => {
            expect(new SemVer('1.2.3').preRelease).to.be.equal('');
            expect(new SemVer('1.2.3-foobar').preRelease).to.be.equal('foobar');
            expect(new SemVer('1.2.3-foo+bar').preRelease).to.be.equal('foo');
        });

        it('Should return the correct build identifier', () => {
            expect(new SemVer('1.2.3').build).to.be.equal('');
            expect(new SemVer('1.2.3-foobar').build).to.be.equal('');
            expect(new SemVer('1.2.3-foo+bar').build).to.be.equal('bar');
        });
    });
});
