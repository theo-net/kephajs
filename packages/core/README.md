# KephaJs `core`

All KephaJs applciation need to extend `@kephajs/core/Application`.

You can extends some method:

-   `_getVersion`: give the app version

The options can be:

-   `environment` the current environment (`development`, `staging`, `production` or `testing`)
-   `version` the app version

Registered services are:

-   `$config` the app configuration
