/*
 * @kephajs/browser
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import { Application } from './Application';
import { Application as CoreApplication } from '@kephajs/core/Application';
import { objectKeys } from '@kephajs/core/Helpers/Helpers';

describe('@kephajs/browser/Application', () => {
    it('Should extend @kephajs/core/Application', () => {
        expect(objectKeys(new Application('test'))).to.include.members(
            objectKeys(new CoreApplication('test'))
        );
    });
});
