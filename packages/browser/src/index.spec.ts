/*
 * @kephajs/browser
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import * as BrowserIndex from './index';
import { Helpers as CoreHelpers } from '@kephajs/core';
import * as Helpers from './Helpers/Helpers';

describe('@kephajs/browser', () => {
    it('Should export console Helpers', () => {
        expect(BrowserIndex.Helpers).to.contain(Helpers);
    });

    it('Should export @kephajs/core/Helpers', () => {
        expect(BrowserIndex.Helpers).to.contain(CoreHelpers);
    });
});
