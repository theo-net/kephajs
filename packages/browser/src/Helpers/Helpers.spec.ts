/*
 * @kephajs/browser
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { expect } from 'chai';

import * as Helpers from './Helpers';

describe('@kephajs/browser/Helpers', () => {
    describe('NodeVersion', () => {
        it('Should export the current user Agent', () => {
            expect(Helpers.UserAgent).to.be.equal(navigator.userAgent);
        });
    });
});
