/*
 * @kephajs/browser
 *
 * (c) Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Application as CoreApplication } from '@kephajs/core/Application';

export class Application extends CoreApplication {
    /**
     * Launch the application
     */
    async run() {
        await super.run();

        console.log('Application start at ' + this.appRoot);
        console.log('KephaJs version: ' + this.kephajsVersion.toString());
        console.log('Application version: ' + this.version.toString());
    }
}
