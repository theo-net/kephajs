// Karma configuration
module.exports = config => {
    config.set({
        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: 'packages',

        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['mocha', 'karma-typescript'],

        // list of files / patterns to load in the browser
        files: ['core/**/*.ts', 'browser/**/*.ts', 'twig/**/*.ts'],

        // list of files to exclude
        exclude: [
            'twig/src/Loader/FilesystemLoader.*',
            'twig/src/Cache/FilesystemCache.*',
            'twig/src/EnvironmentConsole.*',
            'twig/src/cli.*',
            'twig/src/Integration.*',
            'twig/src/IntegrationHelpers.*',
        ],

        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        karmaTypescriptConfig: {
            compilerOptions: {
                module: 'commonjs',
                noEmit: false,
                noImplicitAny: false,
                noImplicitReturns: false,
                noImplicitThis: false,
                noUnusedLocals: false,
                noUnusedParameters: false,
                sourceMap: true,
            },
            tsconfig: '../tsconfig.json',
            bundlerOptions: {
                acornOptions: {
                    ecmaVersion: 11,
                },
            },
            exclude: [
                'packages/console/**/*.ts',
                'packages/demo-console/**/*.ts',
                'packages/twig/src/Cache/FilesystemCache.*',
                'packages/twig/src/EnvironmentConsole.*',
            ],
        },
        preprocessors: {
            '**/*.ts': 'karma-typescript',
        },

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['mocha', 'karma-typescript'],

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: [],

        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true,

        // Concurrency level
        // how many browser should be started simultaneous
        concurrency: Infinity,
    });
};
