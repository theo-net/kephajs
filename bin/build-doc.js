'use strict' ;

/**
 * bin/build-doc.js
 */

const docdown = require('docdown'),
      fs      = require('fs'),
      path    = require('path') ;

const cli   = require('../src/index').cli(),
      color = cli.color() ;

const basePath = path.join(__dirname, '..'),
      docPath  = path.join(basePath, 'doc/framework') ;

const pkg = require('../package.json'),
      version = pkg.version ;


/**
 * Configuration
 */
let config = {
  'path': path.join(basePath, 'dist', 'kephajs.js'),
  'title': '<a href="https://kepha.theo-net.org/kephajs">KephaJs</a> '
         + '<span>v' + version + '</span>',
  'url': 'https//framagit.com/theonet/kephajs/blob/' + version + '/kephajs.js',
  //'hash': 'default', // default or github
  toc: 'categories', // properties or categories
  //'tocLink': '#docs'
} ;


/**
 * Exécutée après chaque doc généré
 * @param {String} string Document généré
 * @returns {String}
 */
function postprocess (string) {

  // Fix docdown bugs.
  return string
    // Repair the default value of `chars`.
    // See https://github.com/eslint/doctrine/issues/157 for more details.
    .replace(/\bchars=''/g, "chars=' '")
    // Wrap symbol property identifiers in brackets.
    .replace(/\.(Symbol\.(?:[a-z]+[A-Z]?)+)/g, '[$1]') ;
}


/**
 * Génère la documentation
 * @param {String} file Fichier pour lequel on génère la documentation
 * @param {String} dist Chemin de destination de la documentation
 */
function build (file, dist) {

  const debug = cli.kernel().get('$config').get('logLevel') === 'debug' ;

  config.path =  file ;

  fs.writeFile(dist, postprocess(docdown(config)), err => {
    if (err)
      throw err ;
    if (debug) console.log(color.cyan('  ' + file)) ;
  }) ;
}


/**
 * Récupère la liste des fichiers pour lesquels on crééra une doc
 * @param {Array} acc Tableau qui contiendra la liste des fichiers
 * @param {String} pathDir Répertoire dont on veut la liste des fichiers
 */
function listFiles (acc, pathDir) {

  const debug = cli.kernel().get('$config').get('logLevel') === 'debug' ;

  if (debug) console.log(color.cyan('  parcours de ') + pathDir) ;

  let files = fs.readdirSync(pathDir) ;
  files.forEach(file => {

    let stat = fs.statSync(path.join(pathDir, file)) ;

    if (!/.test.js$/.test(file)) {
      if (stat.isFile() && /.js$/.test(file))
        acc.push(path.join(pathDir, file)) ;
      else if (stat.isDirectory()) {
        fs.mkdirSync(path.join(pathDir, file)
                         .replace(path.join(basePath, 'src'), docPath)) ;
        if (debug) {
          console.log(color.red('    création de ')
            + path.join(pathDir, file)
                  .replace(path.join(basePath, 'src'), docPath)) ;
        }
        listFiles(acc, path.join(pathDir, file)) ;
      }
    }
  }) ;
}


/**
 * Génération de la documentation en parcourant tous les fichiers
 */
cli.version(version).action(() => {

  const quiet = cli.kernel().get('$config').get('logLevel') === 'warn' ;

  if (!quiet)
    console.log(color.bold.green('Génération de la documentation')) ;

  if (!quiet)
    console.log(color.yellow('Vide le dossier de la documentation...')) ;
  cli.K.rmrf(docPath, () => {

    fs.mkdir(docPath, () => {

      if (!quiet) console.log(color.yellow('Listage des fichiers...')) ;
      let filesList = [] ;
      listFiles(filesList, path.join(basePath, 'src')) ;

      if (!quiet) console.log(color.yellow('Génération des documents...')) ;
      filesList.forEach(file => {
        build(file, file.replace(path.join(basePath, 'src'), docPath)
                    .replace(/js$/, 'md')) ;
      }) ;

      if (!quiet) {
        console.log(
          color.bold.green('Documentation générée dans ') + docPath) ;
      }
    }) ;
  }) ;
}) ;

cli.run() ;

