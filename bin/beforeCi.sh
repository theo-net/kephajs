#!/bin/sh

# Vérications obligatoires avant chaque commit

code=0

npm run validate
code=$(($? || $code))

npm run build
code=$(($? || $code))
npm run build:minified
code=$(($? || $code))
npm run doc
code=$(($? || $code))

exit $code

