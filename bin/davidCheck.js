'use strict' ;

/**
 * bin/davidCheck.js
 */

const david = require('david') ;
const cli = require('../src/index').cli() ;

const manifest = require('../package.json') ;
const color = cli.color() ;

function listDependencies (deps) {

  let table = cli.newTable({
    style: {head: []},
    head: [
      color.underline('Package'), color.underline('Required'),
      color.underline('Stable'), color.underline('Latest')
    ]
  }, 'noBorder') ;

  Object.keys(deps).forEach(depName => {

    let dName    = depName,
        required = deps[depName].required || '*',
        stable   = deps[depName].stable || 'None',
        latest = deps[depName].latest ;

    if (required.replace('^', '') != stable)
      dName = color.red(depName) ;
    else
      dName = color.green(depName) ;

    table.push([dName, required, stable, latest]) ;
  }) ;

  console.log(table.toString()) ;
}


cli.version('0.0.1').action(() => {

  david.getDependencies(manifest, (er, deps) => {

    console.log('latest dependencies information for '
                + color.italic(manifest.name)) ;
    listDependencies(deps) ;
  }) ;

  david.getDependencies(manifest, {dev: true}, (er, deps) => {

    console.log('\nlatest devDependencies information for '
                + color.italic(manifest.name)) ;
    listDependencies(deps) ;
  }) ;
}) ;

cli.run() ;

