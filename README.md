# KephaJs

KephaJs is a TypeScript framework for the browser, for NodeJs console application and for NodeJs server application.

## philosophy

Some will reproach several things, contrary to the philosophy of many open-source projects:

-   One application for one feature, while on the contrary we bring together many elements in a single project in order to have the greatest possible coherence. Especially since we put in a single project code for both NodeJs and both for the browser, because for us, one of the great advantages of choosing TypeScript is to be able to pool development. We thus have a great coherence of structure and available methods.
-   Not to reinvent the wheel, we could have contented ourselves with assembling elements already available, but for the purpose of learning, because we wanted to understand how these bricks are made, we have recoded everything or taken whole portions after the have decorticated.

## Structuring

The framework is split into four major parts:

-   _core_: all the libraries common to the different parts of the framework.
-   _console_: what concerns the command line (application running with NodeJS but being launched in a terminal)
-   _server_: concerning the server part (application running with NodeJS and providing an HTTP service)
-   _browser_: part running in an internet browser

We are inspired by AdonisJs and Symfony frameworks.

### Core

Some elements are inspired from AngularJS and the ebook "Build your own AngularJS".

For this part, we are inspired by:

-   `zod 3.16.0`, Colin McDonnell <colin@colinhacks.com>, for the validator
-   `typed-inject 3.0.1`, Nico Jansen <jansennico@gmail.com>, for the dependencies injection

### Console

For this part, we are inspired by:

-   `dotenv 16.0.1`
-   `cli-table2 0.2.0`, James Talmage <james.talmage@jrtechnical.com>, for table render
-   `minimist 1.20`, James Halliday <mail@substack.net>, for the argv parsing

### Browser

Mais pour la partie navigateur, nous,nous sommes inspirés de React et notamment
d'un tutoriel proposé par Grafikart nous proposant de recréer notre propre version
ultra-simplifiée (https://www.grafikart.fr/tutoriels/comprendre-react-1210).

### Server

Nous avons implémenté, from scratch, un système permettant de servir du
contenu via le portocole HTTP, idéal pour des API REST et des sites web. Nous
avons choisis un modèle MVC (Modèle - Vue - Controller).

### Other parts

They are other packages whore are not included in the all of the major part, as you can decide to include or not for your project.

-   `twig` a pure TypeScript re-implementation of the Twig PHP templating language (<https://twig.symfony.com/>) \_included in `server`

## Licence

Look the file [LICENCE](/LICENCE).

## Test the GitLab CI

The GitLab runner can be install just for LTS version. If your OS are not LTS, you can run:

        curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo os=ubuntu dist=jammy bash
        sudo apt install gitlab-runner

To test a job:

        gitlab-runner exec docker <my-job>

To debug a task, add before the tests in your `.gitlab-ci.yml`:

    sleep 5m

After, when the runner is paused, find the container id:

    docker ps

and enter inside:

    docker exec -it <container-id> bash

## Upgrade Js dependencies

        yarn upgrade-interactive
