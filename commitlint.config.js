module.exports = {
    extends: ['@commitlint/config-conventional'],
    rules: {
        'scope-enum': [
            0,
            'always',
            [
                'browser',
                'console',
                'core',
                'demo-browser',
                'demo-console',
                'reference-docs',
                'twig',
            ],
        ],
    },
};
